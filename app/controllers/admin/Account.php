<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 #------------------------------------    
    # Author: Bdtask Ltd
    # Author link: https://www.bdtask.com/
    # Dynamic style php file
    # Developed by :Isahaq
    #------------------------------------    

class Account extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('admin');
        }
        $this->lang->admin_load('accounts', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('account_model');
        $this->load->admin_model('shop_admin_model');
        $this->load->library('upload');
        $this->upload_path        = 'assets/uploads/';
        $this->thumbs_path        = 'assets/uploads/thumbs/';
        $this->image_types        = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size  = '1024';
          
    }
   
 //tree view show
    function coa() {
        $this->data['title']      = lang('c_o_a');
        $this->data['userList']   = $this->account_model->get_userlist();
        $this->data['parent']     = $this->account_model->get_parenthead();
        $this->data['userID']     = set_value('userID');
        $this->data['module']     = "account";
        $this->data['page']       = "treeview";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/coa'), 'page' => lang('coa')], ['link' => '#', 'page' => lang('coa')]];
        $meta               = ['page_title' => lang('c_o_a'), 'bc' => $bc];
        $this->page_construct('account/treeview', $meta, $this->data);
    }


    public function add_opening_balance(){
        $this->form_validation->set_rules('headcode', lang('account_head')  ,'max_length[100]');
        $this->form_validation->set_rules('dtpDate', lang('date')  ,'required|max_length[30]');
        $this->form_validation->set_rules('amount', lang('amount')  ,'required|max_length[30]');
         if ($this->form_validation->run()) {
          $createby   = $this->session->userdata('user_id');
          $createdate = date('Y-m-d H:i:s');
              $postData = array(
              'VNo'            => $this->input->post('txtVNo',true),
              'Vtype'          => 'Opening',
              'VDate'          => $this->input->post('dtpDate',true),
              'COAID'          => $this->input->post('headcode',true),
              'Narration'      => $this->input->post('txtRemarks',true),
              'Debit'          => $this->input->post('amount',true),
              'Credit'         => 0,
              'IsPosted'       => 1,
              'is_opening'     => 1,
              'CreateBy'       => $createby,
              'CreateDate'     => $createdate,
              'IsAppove'       => 1
      );

            if ($this->account_model->create_opening($postData)) {
            $this->session->set_flashdata('message', lang('save_successfully'));
            redirect('admin/account/opening_balance');
            }else{
             $this->session->set_flashdata('exception', lang('please_try_again'));
             redirect('admin/account/opening_balance');
            }
         }else{
           $this->session->set_flashdata('exception', validation_errors());
           redirect('admin/account/opening_balance');
         }
    }


    public function opening_balance(){
        $this->data['title']      = lang('opening_balance');
        $this->data['headss']     = $this->account_model->get_userlist();
        $this->data['voucher_no'] = $this->account_model->opeing_voucher();
        $this->data['module']     = "account";
        $this->data['page']       = "opening_balance";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/opening_balance'), 'page' => lang('opening_balance')], ['link' => '#', 'page' => lang('opening_balance')]];
        $meta               = ['page_title' => lang('opening_balance'), 'bc' => $bc];
        $this->page_construct('account/opening_balance', $meta, $this->data);
    }

  public function selectedform($id){

        $role_reult = $this->account_model->treeview_selectform($id);
        if ($role_reult){
            $html = "";
            $html .= form_open('','id="treeview_form" class="form-vertical"');
      $html .= "<div id=\"newData\" class=\"row\">
      <div class=\"col-sm-12\">
      <div class=\"row form-custom\">
        <label class=\"col-sm-3\"><b>Head Code</b></label>
        <div class=\"col-sm-9\"><input type=\"text\" name=\"txtHeadCode\" id=\"txtHeadCode\" class=\"form_input form-control\"  value=\"".$role_reult->HeadCode."\" readonly=\"readonly\"/></div>
      </div>
       </div>

     <div class=\"col-sm-12\">
      <div class=\"row form-custom\">
        <label class=\"col-sm-3\"><b>Head Name</b></label>
        <div class=\"col-sm-9\"><input type=\"text\" name=\"txtHeadName\" id=\"txtHeadName\" class=\"form_input form-control\" value=\"".$role_reult->HeadName."\"/>
<input type=\"hidden\" name=\"HeadName\" id=\"HeadName\" class=\"form_input\" value=\"".$role_reult->HeadName."\"/>
        </div>
      </div>
      </div>
     <div class=\"col-sm-12\">
      <div class=\"row form-custom\">
        <label class=\"col-sm-3\"><b>Parent Head</b></label>
        <div class=\"col-sm-9\"><input type=\"text\" name=\"txtPHead\" id=\"txtPHead\" class=\"form_input form-control\" readonly=\"readonly\" value=\"".$role_reult->PHeadName."\"/></div>
      </div>
      </div>
       <div class=\"col-sm-12\">
      <div class=\"row form-custom\">

        <label class=\"col-sm-3\"><b>Head Level</b></label>
        <div class=\"col-sm-9\"><input type=\"text\" name=\"txtHeadLevel\" id=\"txtHeadLevel\" class=\"form_input form-control\" readonly=\"readonly\" value=\"".$role_reult->HeadLevel."\"/></div>
      </div>
      </div>
       <div class=\"col-sm-12\">
      <div class=\"row form-custom\">
        <label class=\"col-sm-3\"><b>Head Type</b></label>
        <div class=\"col-sm-9\"><input type=\"text\" name=\"txtHeadType\" id=\"txtHeadType\" class=\"form_input form-control\" readonly=\"readonly\" value=\"".$role_reult->HeadType."\"/></div>
      </div>
      </div>

       <div class=\"col-sm-12\">
      <div class=\"row form-custom\">
         <div class=\"col-sm-9 col-sm-offset-3\">
         <div class=\"align-center\">
           <div class=\"mr-15\">
           <input type=\"checkbox\" name=\"IsTransaction\" value=\"1\" class=\"mr-5\" id=\"IsTransaction\" size=\"28\"  onchange=\"IsTransaction_change()\"";
           if($role_reult->IsTransaction==1){ $html .="checked";}
            $html .= "/><label for=\"IsTransaction\"> IsTransaction</label>
            </div>

            <div class=\"mr-15\">
           <input type=\"checkbox\" value=\"1\" name=\"IsActive\" class=\"mr-5\" id=\"IsActive\" size=\"28\"";
            if($role_reult->IsActive==1){ $html .="checked";}
            $html .= "/><label for=\"IsActive\"> IsActive</label>
            </div>

            <div class=\"mr-15\">
           <input type=\"checkbox\" value=\"1\" name=\"IsGL\" class=\"mr-5\" id=\"IsGL\" size=\"28\" onchange=\"IsGL_change();\"";
           if($role_reult->IsGL==1){ $html .="checked";}
            $html .= "/><label for=\"IsGL\"> IsGL</label>
            </div>
          </div>

        </div>";
      $html .= "</div>
      </div>
       <div class=\"col-sm-12\">
       <div class=\"row mx-0\">
                    <div class=\"col-sm-9 col-sm-offset-3\">";
                     $html .="<input type=\"button\" name=\"btnNew\" id=\"btnNew\" value=\"New\" onClick=\"newHeaddata(".$role_reult->HeadCode.")\" class=\"btn btn-sub btn-info\"/>
                      <input type=\"btn\" name=\"btnSave\" id=\"btnSave\" value=\"Save\" disabled=\"disabled\" class=\"btn btn-sub btn-success\" onclick=\"treeSubmit()\"/>";

          $html .=" <input type=\"button\" name=\"btnUpdate\" id=\"btnUpdate\" value=\"Update\" onclick=\"treeSubmit()\" class=\"btn btn-sub btn-primary\"/>  <button type=\"button\" class=\"btn btn-sub btn-danger\" data-dismiss=\"modal\">Close</button></div>";
    $html .= "</div></div>
 </form>
            ";
        }

        echo json_encode($html);
    }

    function insert_coa(){
    $headcode    = $this->input->post('txtHeadCode',TRUE);
    $HeadName    = $this->input->post('txtHeadName',TRUE);
    $PHeadName   = $this->input->post('txtPHead',TRUE);
    $HeadLevel   = $this->input->post('txtHeadLevel',TRUE);
    $txtHeadType = $this->input->post('txtHeadType',TRUE);
    $isact       = $this->input->post('IsActive',TRUE);
    $IsActive    = (!empty($isact)?$isact:0);
    $trns        = $this->input->post('IsTransaction',TRUE);
    $IsTransaction = (!empty($trns)?$trns:0);
    $isgl        = $this->input->post('IsGL',TRUE);
     $IsGL       = (!empty($isgl)?$isgl:0);
    $createby    = $this->session->userdata('user_id');
    $createdate  = date('Y-m-d H:i:s');
       $postData = array(
      'HeadCode'       =>  $headcode,
      'HeadName'       =>  $HeadName,
      'PHeadName'      =>  $PHeadName,
      'HeadLevel'      =>  $HeadLevel,
      'IsActive'       =>  $IsActive,
      'IsTransaction'  =>  $IsTransaction,
      'IsGL'           =>  $IsGL,
      'HeadType'       => $txtHeadType,
      'IsBudget'       => 0,
      'CreateBy'       => $createby,
      'CreateDate'     => $createdate,
    );
 $upinfo = $this->db->select('*')
            ->from('acc_coa')
            ->where('HeadCode',$headcode)
            ->get()
            ->row();
            if(empty($upinfo)){
  $this->db->insert('acc_coa',$postData);
  $this->data['status']  = true;
  $this->data['message'] = 'Successfully Saved';
}else{

$hname =$this->input->post('HeadName',TRUE);
$updata = array(
'PHeadName'      =>  $HeadName,
);


  $this->db->where('HeadCode',$headcode)
      ->update('acc_coa',$postData);
  $this->db->where('PHeadName',$hname)
      ->update('acc_coa',$updata);

      $this->data['status']  = true;
      $this->data['message'] = 'Successfully Updated';
}

        echo json_encode($this->data);
  }

      public function newform($id){

    $newdata = $this->db->select('*')
            ->from('acc_coa')
            ->where('HeadCode',$id)
            ->get()
            ->row();


  $newidsinfo = $this->db->select('*,max(HeadCode) as hc')
            ->from('acc_coa')
            ->where('PHeadName',$newdata->HeadName)
            ->get()
            ->row();

$nid  = $newidsinfo->hc;
if($nid){
  $n =$nid + 1;
  $HeadCode = $n;
}else{
  $HeadCode = $id .'00'. 1;
}

  $info['headcode']  =  $HeadCode;
  $info['rowdata']   =  $newdata;
  $info['headlabel'] =  $newdata->HeadLevel+1;
    echo json_encode($info);
  }


    public function supplier_payment() {
        $this->data['title']         = lang('supplier_payment');
        $this->data['supplier_list'] =  $this->account_model->get_supplier();
        $this->data['voucher_no']    = $this->account_model->Spayment();
        $this->data['module']        = "account";
        $this->data['page']          = "supplier_payment_form";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/supplier_payment'), 'page' => lang('supplier_payment')], ['link' => '#', 'page' => lang('supplier_payment')]];
        $meta               = ['page_title' => lang('supplier_payment'), 'bc' => $bc];
        $this->page_construct('account/supplier_payment_form', $meta, $this->data);
    }

    public function create_supplier_payment(){
        $this->form_validation->set_rules('txtCode', lang('txtCode')  ,'max_length[100]');
        $this->form_validation->set_rules('paytype', lang('paytype')  ,'required|max_length[2]');
         $this->form_validation->set_rules('txtCode', lang('code')  ,'required|max_length[30]');
          $this->form_validation->set_rules('txtAmount', lang('amount')  ,'required|max_length[30]');
         if ($this->form_validation->run()) {
        if ($this->account_model->supplier_payment_insert()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/supplier_payment');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/supplier_payment");
        }else{
          $this->session->set_flashdata('exception',   validation_errors());
          redirect("admin/account/supplier_payment");
         }

}

public function supplier_paymentreceipt($supplier_id,$voucher_no,$coaid){
    $supplier_id           = $this->uri->segment(2);
    $voucher_no            = $this->uri->segment(3);
    $coaid                 = $this->uri->segment(4);
    $this->data['supplier_info'] = $this->account_model->supplierinfo($supplier_id);
    $this->data['payment_info']  = $this->account_model->supplierpaymentinfo($voucher_no,$coaid);
    $this->data['title']         = lang('supplier_payment_receipt');
    $this->data['module']        = "account";
    $this->data['page']          = "supplier_payment_receipt";
    $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
    $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/supplier_payment_receipt'), 'page' => lang('supplier_payment_receipt')], ['link' => '#', 'page' => lang('supplier_payment_receipt')]];
    $meta               = ['page_title' => lang('supplier_payment_receipt'), 'bc' => $bc];
    $this->page_construct('account/supplier_payment_receipt', $meta, $this->data);
}

    public function supplier_headcode($id){
    $supplier_info = $this->db->select('name')->from('companies')->where('id',$id)->get()->row();
    $head_name     = $id.'-'.$supplier_info->name;
    $supplierhcode = $this->db->select('*')
            ->from('acc_coa')
            ->where('HeadName',$head_name)
            ->get()
            ->row();
      $code = $supplierhcode->HeadCode;
    echo json_encode($code);

   }

   //Customer Receive
public function customer_receive(){
    $this->data['customer_list'] = $this->account_model->get_customer();
    $this->data['voucher_no']    = $this->account_model->Creceive();
    $this->data['title']         = lang('customer_receive');
    $this->data['module']        = "account";
    $this->data['page']          = "customer_receive_form";
    $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
    $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/customer_receive'), 'page' => lang('customer_receive')], ['link' => '#', 'page' => lang('customer_receive')]];
    $meta               = ['page_title' => lang('customer_receive'), 'bc' => $bc];
    $this->page_construct('account/customer_receive_form', $meta, $this->data);
}

  public function customer_headcode($id){
  $customer_info = $this->db->select('name')->from('companies')->where('id',$id)->get()->row();
    $head_name     = $id.'-'.$customer_info->name;
    $customerhcode = $this->db->select('*')
            ->from('acc_coa')
            ->where('HeadName',$head_name)
            ->get()
            ->row();
      $code = $customerhcode->HeadCode;
    echo json_encode($code);

   }


      public function create_customer_receive(){
     $this->form_validation->set_rules('paytype', lang('paytype')  ,'required|max_length[100]');
     $this->form_validation->set_rules('txtCode', lang('txtCode')  ,'required|max_length[100]');
     $this->form_validation->set_rules('txtAmount', lang('amount')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->customer_receive_insert()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/customer_receive');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/customer_receive");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/customer_receive");
     }

   }


    public function customer_receipt($customer_id,$voucher_no,$coaid){
    $customer_id           = $this->uri->segment(2);
    $voucher_no            = $this->uri->segment(3);
    $coaid                 = $this->uri->segment(4);
    $this->data['customer_info'] = $this->account_model->custoinfo($customer_id);
    $this->data['payment_info']  = $this->account_model->customerreceiptinfo($voucher_no,$coaid);
    $this->data['title']         = lang('customer_receive');
    $this->data['module']        = "account";
    $this->data['page']          = "customer_payment_receipt";
    $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
    $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/customer_payment_receipt'), 'page' => lang('customer_payment_receipt')], ['link' => '#', 'page' => lang('customer_payment_receipt')]];
    $meta               = ['page_title' => lang('customer_payment_receipt'), 'bc' => $bc];
    $this->page_construct('account/customer_payment_receipt', $meta, $this->data);
}


  public function cash_adjustment(){
      $this->data['title']      = lang('cash_adjustment');
      $this->data['voucher_no'] = $this->account_model->Cashvoucher();
      $this->data['module']     = "account";
      $this->data['page']       = "cash_adjustment";
      $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
      $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/cash_adjustment'), 'page' => lang('cash_adjustment')], ['link' => '#', 'page' => lang('cash_adjustment')]];
      $meta               = ['page_title' => lang('customer_receive'), 'bc' => $bc];
      $this->page_construct('account/cash_adjustment', $meta, $this->data);
  }

   public function create_cash_adjustment(){
    $this->form_validation->set_rules('txtAmount', lang('amount')  ,'required|max_length[100]');
    $this->form_validation->set_rules('type', lang('adjustment_type')  ,'required|max_length[10]');
      if ($this->form_validation->run()) {
        if ($this->account_model->insert_cashadjustment()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/cash_adjustment');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/cash_adjustment");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/cash_adjustment");
     }

}


    public function debit_voucher(){
    $this->data['title']      = lang('debit_voucher');
    $this->data['acc']        = $this->account_model->Transacc();
    $this->data['voucher_no'] = $this->account_model->voNO();
    $this->data['crcc']       = $this->account_model->Cracc();
    $this->data['module']     = "account";
    $this->data['page']       = "debit_voucher";
    $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
    $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/debit_voucher'), 'page' => lang('debit_voucher')], ['link' => '#', 'page' => lang('debit_voucher')]];
    $meta               = ['page_title' => lang('debit_voucher'), 'bc' => $bc];
    $this->page_construct('account/debit_voucher', $meta, $this->data);
  }

    public function debtvouchercode($id){

    $debitvcode = $this->db->select('*')
            ->from('acc_coa')
            ->where('HeadCode',$id)
            ->get()
            ->row();
      $code = $debitvcode->HeadCode;
     echo json_encode($code);

   }

    public function create_debit_voucher(){
     $this->form_validation->set_rules('cmbDebit', lang('credit_account_head')  ,'required|max_length[100]');
      $this->form_validation->set_rules('dtpDate', lang('date')  ,'required|max_length[100]');
      $this->form_validation->set_rules('cmbCode[]', lang('account_name')  ,'required|max_length[100]');
       $this->form_validation->set_rules('txtAmount[]', lang('amount')  ,'required|max_length[100]');
     $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->insert_debitvoucher()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/debit_voucher');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/debit_voucher");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/debit_voucher");
     }

}



    //Credit voucher
      public function credit_voucher(){
        $this->data['title']      = lang('credit_voucher');
        $this->data['acc']        = $this->account_model->Transacc();
        $this->data['voucher_no'] = $this->account_model->crVno();
        $this->data['crcc']       = $this->account_model->Cracc();
        $this->data['module']     = "account";
        $this->data['page']       = "credit_voucher";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/credit_voucher'), 'page' => lang('credit_voucher')], ['link' => '#', 'page' => lang('credit_voucher')]];
        $meta               = ['page_title' => lang('credit_voucher'), 'bc' => $bc];
        $this->page_construct('account/credit_voucher', $meta, $this->data);
  }

      //Create Credit Voucher
 public function create_credit_voucher(){
    $this->form_validation->set_rules('cmbDebit', lang('credit_account_head')  ,'required|max_length[100]');
      $this->form_validation->set_rules('dtpDate', lang('date')  ,'required|max_length[100]');
      $this->form_validation->set_rules('cmbCode[]', lang('account_name')  ,'required|max_length[100]');
       $this->form_validation->set_rules('txtAmount[]', lang('amount')  ,'required|max_length[100]');
     $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->insert_creditvoucher()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/credit_voucher');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/credit_voucher");
    }else{
      $this->session->set_flashdata('exception',  lang('please_try_again'));
      redirect("admin/account/credit_voucher");
     }

}

    // Contra Voucher form
 public function contra_voucher(){
     $this->data['title']      = lang('contra_voucher');
     $this->data['acc']        = $this->account_model->Transacc();
     $this->data['voucher_no'] = $this->account_model->contra();
     $this->data['module']     = "account";
     $this->data['page']       = "contra_voucher";
     $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
     $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/contra_voucher'), 'page' => lang('contra_voucher')], ['link' => '#', 'page' => lang('contra_voucher')]];
     $meta               = ['page_title' => lang('contra_voucher'), 'bc' => $bc];
     $this->page_construct('account/contra_voucher', $meta, $this->data);
  }


   public function create_contra_voucher(){
    $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->insert_contravoucher()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/contra_voucher');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
            redirect("admin/account/contra_voucher");
        }else{
          $this->session->set_flashdata('exception',  validation_errors());
          redirect("admin/account/contra_voucher");
         }

    }

     // Journal voucher
 public function journal_voucher(){
     $this->data['title']      = lang('journal_voucher');
     $this->data['acc']        = $this->account_model->Transacc();
     $this->data['voucher_no'] = $this->account_model->journal();
     $this->data['module']     = "account";
     $this->data['page']       = "journal_voucher";
     $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
     $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/journal_voucher'), 'page' => lang('journal_voucher')], ['link' => '#', 'page' => lang('journal_voucher')]];
     $meta               = ['page_title' => lang('contra_voucher'), 'bc' => $bc];
     $this->page_construct('account/journal_voucher', $meta, $this->data);
  }


        //Create Journal Voucher
    public function create_journal_voucher(){
    $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->insert_journalvoucher()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/journal_voucher');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/journal_voucher");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/journal_voucher");
     }

}

 //Aprove voucher
  public function voucher_list(){
        $this->data['title']   = lang('voucher_approval');
        $this->data['aprrove'] = $this->account_model->approve_voucher();
        $this->data['module']  = "account";
        $this->data['page']    = "voucher_approve";
        $this->data['error']   = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/voucher_approve'), 'page' => lang('voucher_approve')], ['link' => '#', 'page' => lang('voucher_approve')]];
        $meta               = ['page_title' => lang('voucher_approve'), 'bc' => $bc];
        $this->page_construct('account/voucher_approve', $meta, $this->data);
}

 // isApprove
 public function isactive($id = null, $action = null)
  {
    $action   = ($action=='active'?1:0);
    $postData = array(
      'VNo'      => $id,
      'IsAppove' => $action
    );

    if ($this->account_model->approved($postData)) {
      $this->session->set_flashdata('message', lang('successfully_approved'));
    } else {
      $this->session->set_flashdata('exception', lang('please_try_again'));
    }

    redirect($_SERVER['HTTP_REFERER']);
  }



     //Update voucher
  public function voucher_update($id= null){
    $vtype =$this->db->select('*')
                    ->from('acc_transaction')
                    ->where('VNo',$id)
                    ->get()
                    ->result_array();

                    if($vtype[0]['Vtype'] =="DV"){
    $this->data['title']          = lang('update_debit_voucher');
    $this->data['dbvoucher_info'] = $this->account_model->dbvoucher_updata($id);
    $this->data['credit_info']    = $this->account_model->crvoucher_updata($id);
    $this->data['page']           = "update_dbt_crtvoucher";

    }

     if($vtype[0]['Vtype'] =="JV"){
    $this->data['title']        = 'Update'.' '.lang('journal_voucher');
    $this->data['acc']          = $this->account_model->Transacc();
    $this->data['voucher_info'] = $this->account_model->journal_updata($id);
    $this->data['page']         = "update_journal_voucher";
    }


     if($vtype[0]['Vtype'] =="Contra"){
    $this->data['title']         = 'Update'.' '.lang('contra_voucher');
    $this->data['acc']           = $this->account_model->Transacc();
    $this->data['voucher_info']  = $this->account_model->journal_updata($id);
     $this->data['page']         = "update_contra_voucher";
    }

    if($vtype[0]['Vtype'] =="CV"){
    $this->data['title']          = lang('update_credit_voucher');
    $this->data['crvoucher_info'] = $this->account_model->crdtvoucher_updata($id);
    $this->data['debit_info']     = $this->account_model->debitvoucher_updata($id);
    $this->data['page']           = "update_credit_bdtvoucher";
    }
    $this->data['crcc']           = $this->account_model->Cracc();
    $this->data['acc']            = $this->account_model->Transacc();
    $this->data['module']         = "account";
  $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
  $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/'.$this->data['page']), 'page' => lang($this->data['page'])], ['link' => '#', 'page' => lang($this->data['page'])]];
  $meta               = ['page_title' => lang($this->data['page']), 'bc' => $bc];
  $this->page_construct('account/'.$this->data['page'], $meta, $this->data);
  }

  /*updates part*/
  public function update_contra_voucher(){
    $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->update_contravoucher()) {
          $this->session->set_flashdata('message', lang('successfully_updated'));
          redirect('admin/account/voucher_list');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/voucher_list");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/voucher_list");
     }

}

  public function update_credit_voucher(){
    $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->update_creditvoucher()) {
          $this->session->set_flashdata('message', lang('save_successfully'));
          redirect('admin/account/voucher_list/');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/voucher_list");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/voucher_list");
     }

}

    // Update Debit voucher
public function update_debit_voucher(){
    $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->update_debitvoucher()) {
          $this->session->set_flashdata('message', lang('update_successfully'));
          redirect('admin/account/voucher_list');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/voucher_list");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/voucher_list");
     }

}

 public function update_journal_voucher(){
    $this->form_validation->set_rules('cmbDebit', lang('cmbDebit')  ,'max_length[100]');
         if ($this->form_validation->run()) {
        if ($this->account_model->update_journalvoucher()) {
          $this->session->set_flashdata('message', lang('successfully_updated'));
          redirect('admin/account/voucher_list');
        }else{
          $this->session->set_flashdata('exception',  lang('please_try_again'));
        }
        redirect("admin/account/voucher_list");
    }else{
      $this->session->set_flashdata('exception',  validation_errors());
      redirect("admin/account/voucher_list");
     }

   }

    public function voucher_delete($voucher){
     if ($this->account_model->delete_voucher($voucher)) {
      $this->session->set_flashdata('message', lang('successfully_delete'));
    } else {
      $this->session->set_flashdata('exception', lang('please_try_again'));
    }

    redirect($_SERVER['HTTP_REFERER']);

  }

      public function cash_book(){
          $this->data['title']   = lang('cash_book');
          $this->data['module']  = "account";
          $this->data['page']    = "cash_book";
          $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
          $this->data['error']   = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
          $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/cash_book'), 'page' => lang('cash_book')], ['link' => '#', 'page' => lang('cash_book')]];
          $meta               = ['page_title' => lang('cash_book'), 'bc' => $bc];
          $this->page_construct('account/cash_book', $meta, $this->data);
    }


       // Inventory Report
     public function inventory_ledger(){
         $this->data['title']   = lang('Inventory_ledger');
         $this->data['module']  = "account";
         $this->data['page']    = "inventory_ledger";
         $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
         $this->data['error']   = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
         $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/inventory_ledger'), 'page' => lang('inventory_ledger')], ['link' => '#', 'page' => lang('inventory_ledger')]];
         $meta               = ['page_title' => lang('inventory_ledger'), 'bc' => $bc];
         $this->page_construct('account/inventory_ledger', $meta, $this->data);
    }

      public function bank_book(){
          $this->data['title']   = lang('bank_book');
          $this->data['module']  = "account";
          $this->data['page']    = "bank_book";
          $this->data['error']   = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
          $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/bank_book'), 'page' => lang('bank_book')], ['link' => '#', 'page' => lang('bank_book')]];
          $meta               = ['page_title' => lang('bank_book'), 'bc' => $bc];
          $this->page_construct('account/bank_book', $meta, $this->data);
     }

         public function general_ledger(){
        $this->data['title']          = lang('general_ledger');
        $this->data['general_ledger'] = $this->account_model->get_general_ledger();
        $this->data['module']         = "account";
        $this->data['page']           = "general_ledger";
        $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
         $this->data['error']   = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
         $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/general_ledger'), 'page' => lang('general_ledger')], ['link' => '#', 'page' => lang('general_ledger')]];
         $meta               = ['page_title' => lang('general_ledger'), 'bc' => $bc];
         $this->page_construct('account/general_ledger', $meta, $this->data);
    }


    public function general_led($Headid = NULL){
        $Headid   = $this->input->post('Headid',TRUE);
        $HeadName = $this->account_model->general_led_get($Headid);
        echo  "<option>Transaction Head</option>";
        $html = "";
        foreach($HeadName as $data){
            $html .="<option value='$data->HeadCode'>$data->HeadName</option>";

        }
        echo $html;
    }

        //general ledger working
    public function accounts_report_search(){
        $cmbGLCode       = $this->input->post('cmbGLCode',TRUE);
        $cmbCode         = $this->input->post('cmbCode',TRUE);
        $dtpFromDate     = $this->input->post('dtpFromDate',TRUE);
        $dtpToDate       = $this->input->post('dtpToDate',TRUE);
        $chkIsTransction = $this->input->post('chkIsTransction',TRUE);
        $HeadName        = $this->account_model->general_led_report_headname($cmbGLCode);
        $HeadName2       = $this->account_model->general_led_report_headname2($cmbGLCode,$cmbCode,$dtpFromDate,$dtpToDate,$chkIsTransction);
        $pre_balance     = $this->account_model->general_led_report_prebalance($cmbCode,$dtpFromDate);

         $this->data['title']          = lang('general_ledger_report');
         $this->data['dtpFromDate']    = $dtpFromDate;
         $this->data['dtpToDate']      = $dtpToDate;
         $this->data['HeadName']       = $HeadName;
         $this->data['HeadName2']      = $HeadName2;
         $this->data['prebalance']     =  $pre_balance;
         $this->data['chkIsTransction']= $chkIsTransction;
        $this->data['ledger']  = $this->db->select('*')->from('acc_coa')->where('HeadCode',$cmbCode)->get()->result_array();
        $this->data['module']  = "account";
        $this->data['page']    = "general_ledger_report";
        $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/general_ledger_report'), 'page' => lang('general_ledger_report')], ['link' => '#', 'page' => lang('general_ledger_report')]];
        $meta               = ['page_title' => lang('general_ledger_report'), 'bc' => $bc];
        $this->page_construct('account/general_ledger_report', $meta, $this->data);

    }

    //Trial Balannce
    public function trial_balance(){
        $this->data['title']   = lang('trial_balance');
        $this->data['module']  = "account";
        $this->data['page']    = "trial_balance";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/trial_balance'), 'page' => lang('trial_balance')], ['link' => '#', 'page' => lang('trial_balance')]];
        $meta               = ['page_title' => lang('trial_balance'), 'bc' => $bc];
        $this->page_construct('account/trial_balance', $meta, $this->data);
        }

          //Trial Balance Report
    public function trial_balance_report(){
       $dtpFromDate     = $this->input->post('dtpFromDate',TRUE);
       $dtpToDate       = $this->input->post('dtpToDate',TRUE);
       $chkWithOpening  = $this->input->post('chkWithOpening',TRUE);
       $results         = $this->account_model->trial_balance_report($dtpFromDate,$dtpToDate,$chkWithOpening);
        $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();

       if ($results['WithOpening'] == 1) {
           $this->data['oResultTr']    = $results['oResultTr'];
            $this->data['oResultInEx']  = $results['oResultInEx'];
            $this->data['dtpFromDate']  = $dtpFromDate;
            $this->data['dtpToDate']    = $dtpToDate;
            $this->data['title']        = lang('trial_balance');
            $this->data['module']       = "account";
            $this->data['page']         = "trial_balance_with_opening";
           $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
           $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/trial_balance_with_opening'), 'page' => lang('trial_balance_with_opening')], ['link' => '#', 'page' => lang('trial_balance_with_opening')]];
           $meta               = ['page_title' => lang('trial_balance_with_opening'), 'bc' => $bc];
           $this->page_construct('account/trial_balance_with_opening', $meta, $this->data);
       }else{

            $this->data['oResultTr']    = $results['oResultTr'];
            $this->data['oResultInEx']  = $results['oResultInEx'];
            $this->data['dtpFromDate']  = $dtpFromDate;
            $this->data['dtpToDate']    = $dtpToDate;
            $this->data['title']        = lang('trial_balance');
            $this->data['module']       = "account";
            $this->data['page']         = "trial_balance_without_opening";
           $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
           $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/trial_balance_without_opening'), 'page' => lang('trial_balance_without_opening')], ['link' => '#', 'page' => lang('trial_balance_without_opening')]];
           $meta               = ['page_title' => lang('trial_balance_without_opening'), 'bc' => $bc];
           $this->page_construct('account/trial_balance_without_opening', $meta, $this->data);
       }

    }

    public function voucher_report(){
        //$this->permission->method('accounts','read')->redirect();
       $get_cash = $this->account_model->get_cash();
        $get_vouchar= $this->account_model->get_vouchar();
        $this->data['get_cash']  = $get_cash;
        $this->data['get_vouchar']  = $get_vouchar;
        $this->data['title']  = lang('voucher_report');
        $this->data['module'] = "accounts";
        $this->data['page']   = "coa";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/voucher_report'), 'page' => lang('voucher_report')], ['link' => '#', 'page' => lang('voucher_report')]];
        $meta               = ['page_title' => lang('voucher_report'), 'bc' => $bc];
        $this->page_construct('account/coa', $meta, $this->data);
    }

          //Profit loss report page
    public function profit_loss_report(){
        $this->data['title']   = lang('profit_loss');
        $this->data['module']  = "account";
        $this->data['page']    = "profit_loss_report";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/profit_loss_report'), 'page' => lang('profit_loss_report')], ['link' => '#', 'page' => lang('profit_loss_report')]];
        $meta               = ['page_title' => lang('profit_loss_report'), 'bc' => $bc];
        $this->page_construct('account/profit_loss_report', $meta, $this->data);
    }

        //Profit loss serch result
    public function profit_loss_report_search(){
        $dtpFromDate              = $this->input->post('dtpFromDate',TRUE);
        $dtpToDate                = $this->input->post('dtpToDate',TRUE);
        $get_profit               = $this->account_model->profit_loss_serach();
        $this->data['oResultAsset']     = $get_profit['oResultAsset'];
        $this->data['oResultLiability'] = $get_profit['oResultLiability'];
        $this->data['dtpFromDate']      = $dtpFromDate;
        $this->data['dtpToDate']        = $dtpToDate;
        $this->data['pdf']              = 'assets/data/pdf/Statement of Comprehensive Income From '.$dtpFromDate.' To '.$dtpToDate.'.pdf';
        $this->data['title']            = lang('profit_loss');
        $this->data['module']           = "account";
        $this->data['page']             = "profit_loss_report_search";
        $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/profit_loss_report_search'), 'page' => lang('profit_loss_report_search')], ['link' => '#', 'page' => lang('profit_loss_report_search')]];
        $meta               = ['page_title' => lang('profit_loss_report_search'), 'bc' => $bc];
        $this->page_construct('account/profit_loss_report_search', $meta, $this->data);
    }

         //Cash flow page
    public function cash_flow_report(){
        $this->pdata['title']  = lang('cash_flow_report');
        $this->pdata['module'] = "account";
        $this->pdata['page']   = "cash_flow_report";
        $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/cash_flow_report'), 'page' => lang('cash_flow_report')], ['link' => '#', 'page' => lang('cash_flow_report')]];
        $meta               = ['page_title' => lang('cash_flow_report'), 'bc' => $bc];
        $this->page_construct('account/cash_flow_report', $meta, $this->data);
    }

         //Cash flow report search
    public function cash_flow_report_search(){
        $dtpFromDate          = $this->input->post('dtpFromDate',TRUE);
        $dtpToDate            = $this->input->post('dtpToDate',TRUE);
        $this->data['dtpFromDate']  = $dtpFromDate;
        $this->data['dtpToDate']    = $dtpToDate;
        $this->data['title']        = lang('cash_flow_report');
        $this->data['module']       = "account";
        $this->data['page']         = "cash_flow_report_search";
        $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/cash_flow_report_search'), 'page' => lang('cash_flow_report_search')], ['link' => '#', 'page' => lang('cash_flow_report_search')]];
        $meta               = ['page_title' => lang('cash_flow_report_search'), 'bc' => $bc];
        $this->page_construct('account/cash_flow_report_search', $meta, $this->data);
    }

     public function coa_print(){
         $this->data['title']        = lang('coa_print');
         $this->data['module']       = "account";
         $this->data['page']         = "coa_print";
         $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
         $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
         $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/coa_print'), 'page' => lang('coa_print')], ['link' => '#', 'page' => lang('coa_print')]];
         $meta               = ['page_title' => lang('coa_print'), 'bc' => $bc];
         $this->page_construct('account/coa_print', $meta, $this->data);
    }

    public function balance_sheet(){
    $this->data['title']       = lang('balance_sheet');
    $from_date           = (!empty($this->input->post('dtpFromDate'))?$this->input->post('dtpFromDate'):date('Y-m-d'));
    $to_date             = (!empty($this->input->post('dtpToDate'))?$this->input->post('dtpToDate'):date('Y-m-d'));
        $this->data['shop_settings'] = $this->shop_admin_model->getShopSettings();
        $this->data['from_date']   = $from_date;
        $this->data['to_date']     = $to_date;
        $this->data['fixed_assets']= $this->account_model->fixed_assets();
        $this->data['liabilities'] = $this->account_model->liabilities_data();
        $this->data['incomes']     = $this->account_model->income_fields();
        $this->data['expenses']    = $this->account_model->expense_fields();
        $this->data['module']      = "account";
        $this->data['page']        = "balance_sheet";
        $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('account/balance_sheet'), 'page' => lang('balance_sheet')], ['link' => '#', 'page' => lang('balance_sheet')]];
        $meta               = ['page_title' => lang('balance_sheet'), 'bc' => $bc];
        $this->page_construct('account/balance_sheet', $meta, $this->data);
    }

}


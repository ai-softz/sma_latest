<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Core_hr extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('corehr_model');
        $this->load->admin_model('employees_model');
        $this->load->admin_model('department_model');
        $this->load->admin_model('calendar_model');
        $this->load->admin_model('timesheet_model');
        $this->load->library('form_validation');
    }

    public function awards()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('timesheets/awards'), 'page' => lang('awards')], ['link' => '#', 'page' => lang('awards')]];
        $meta = ['page_title' => lang('awards'), 'bc' => $bc];
		$this->page_construct('core_hr/awards_list', $meta, $this->data);
     }

     public function getAwards()
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('awards.award_id as id, award_type.award_type, CONCAT_WS(" ", first_name, last_name), awards.gift_item, awards.award_month_year')
			->from('awards')
			->join('award_type', 'award_type.award_type_id = awards.award_type_id')
			->join('employees', 'employees.user_id = awards.employee_id')
			->group_by('awards.award_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_award/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_award') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_award/$1') . "' class='tip' title='" . lang('edit_award') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_award') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_award/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     } 

     public function view_award($award_id)
     {
        $awards = $this->corehr_model->read_award($award_id);
        $this->data['awards'] = $awards;
        $this->data['emp_name'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$awards->employee_id))->row();
        $this->data['award_type'] = $this->db->select('award_type_id, award_type')->get_where('award_type', array('award_type_id'=>$awards->award_type_id))->row()->award_type;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_award', $this->data);
     }

     public function delete_award($id='')
     {
        if ($this->corehr_model->delete_award($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('award_deleted')]);
        }
     }

     public function edit_award($award_id='')
     {
        $this->data['awards'] = $this->corehr_model->read_award($award_id);

		$this->data['award_type'] = $this->corehr_model->award_type();
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/awards'), 'page' => lang('awards')], ['link' => '#', 'page' => lang('awards')]];
        $meta = ['page_title' => lang('awards'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_award', $meta, $this->data);

     }

     public function add_award()
     {
     	$this->form_validation->set_rules('award_type_id', lang('award_type_id'), 'required');
        $this->form_validation->set_rules('created_at', lang('award_date'), 'required');
     	$this->form_validation->set_rules('award_month_year', lang('award_month_year'), 'required');
     	$this->form_validation->set_rules('gift_item', lang('gift_item'), 'required');
     	$this->form_validation->set_rules('cash_price', lang('cash_price'), 'required');

     	if ($this->form_validation->run() == true) {
            $award_month_year = $this->input->post('award_month_year');
            $award_month_year = date('Y-m', strtotime($award_month_year));
            $award_date = $this->input->post('award_date');
            $award_date = date('Y-m-d', strtotime($award_date));

            if(!empty($_FILES["award_photo"]["name"])) {
                $config['upload_path']          = 'assets/uploads/awards/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('award_photo')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
                $this->upload->initialize($config);
            } 

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'award_type_id' => $this->input->post('award_type_id'),
                'created_at' => $award_date,
                'award_month_year' => $award_month_year,
                'gift_item' => $this->input->post('gift_item'),
                'cash_price' => $this->input->post('cash_price'),
                'award_photo' => $file_name,
                'award_information' => $this->input->post('award_information'),
                'description' => $this->input->post('description'),
            );
           
     		
     	} elseif ($this->input->post('add_award')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_award');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_award($data)) {
            $this->session->set_flashdata('message', lang('award_added'));
            admin_redirect('core_hr/awards');
        }
        else {
	     	$this->data['award_type'] = $this->corehr_model->award_type();
            $this->data['all_employees'] = $this->employees_model->all_employees();
	     	$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/awards'), 'page' => lang('awards')], ['link' => '#', 'page' => lang('awards')]];
            $meta = ['page_title' => lang('awards'), 'bc' => $bc];
            $this->page_construct('core_hr/add_award', $meta, $this->data);
	    }
     }
     
     public function editAward()
     {
     	$this->form_validation->set_rules('award_type_id', lang('award_type_id'), 'required');
     	$this->form_validation->set_rules('created_at', lang('award_date'), 'required');
     	$this->form_validation->set_rules('award_month_year', lang('award_month_year'), 'required');
     	$this->form_validation->set_rules('gift_item', lang('gift_item'), 'required');
     	$this->form_validation->set_rules('cash_price', lang('cash_price'), 'required');
         
        $award_id = $this->input->post('award_id');

     	if ($this->form_validation->run() == true) {
            $award_month_year = $this->input->post('award_month_year');
            $award_month_year = date('Y-m', strtotime($award_month_year));
            $award_date = $this->input->post('award_date');
            $award_date = date('Y-m-d', strtotime($award_date));

            if(!empty($_FILES["award_photo"]["name"])) {
                $document = $this->db->select('award_photo')->get_where('awards', array('award_id'=>$award_id))->row();
                if(!empty($document->award_photo)) {
                    unlink('assets/uploads/awards/'.$document->award_photo);
                }

               $config['upload_path']          = 'assets/uploads/awards/';
               $config['allowed_types']        = 'gif|jpg|jpeg|png';
               $config['max_size']             = 5000;
               

               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               if ($this->upload->do_upload('award_photo')) {
                       $data = array('upload_data' => $this->upload->data());
                       $file_name = $data['upload_data']['file_name'];
                      
               }
               //unset($_FILES);
           } else{
                $document = $this->db->select('award_photo')->get_where('awards', array('award_id'=>$award_id))->row();
               $file_name = $document->award_photo;
           }

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'award_type_id' => $this->input->post('award_type_id'),
                'created_at' => $award_date,
                'award_month_year' => $award_month_year,
                'gift_item' => $this->input->post('gift_item'),
                'cash_price' => $this->input->post('cash_price'),
                'award_photo' => $file_name,
                'award_information' => $this->input->post('award_information'),
                'description' => $this->input->post('description'),
            );
           
     		
     	} elseif ($this->input->post('edit_award')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_award/'.$award_id);
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_award($data, $award_id)) {
            $this->session->set_flashdata('message', lang('award_edited'));
            admin_redirect('core_hr/awards');
        }
        else {
            $this->data['awards'] = $this->corehr_model->read_award($award_id);
	     	$this->data['award_type'] = $this->corehr_model->award_type();
            $this->data['all_employees'] = $this->employees_model->all_employees();
	     	$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/awards'), 'page' => lang('awards')], ['link' => '#', 'page' => lang('awards')]];
            $meta = ['page_title' => lang('awards'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_award/'.$award_id, $meta, $this->data);
	    }
     }

     public function transfer()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/transfer'), 'page' => lang('transfer')], ['link' => '#', 'page' => lang('transfer')]];
        $meta = ['page_title' => lang('awards'), 'bc' => $bc];
		$this->page_construct('core_hr/transfer_list', $meta, $this->data);
     }

     public function getTransfers()
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('employee_transfer.transfer_id as id, CONCAT_WS(" ", first_name, last_name), employee_transfer.transfer_date, employee_transfer.status')
			->from('employee_transfer')
			->join('employees', 'employees.user_id = employee_transfer.employee_id')
			->group_by('employee_transfer.transfer_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_transfer/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_transfer') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_transfer/$1') . "' class='tip' title='" . lang('edit_transfer') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_transfer') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_transfer/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     } 
     public function delete_transfer($id='')
     {
        if ($this->corehr_model->delete_transfer($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('transfer_deleted')]);
        }
     }

     public function add_transfer()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
        $this->form_validation->set_rules('transfer_date', lang('transfer_date'), 'required');
     	$this->form_validation->set_rules('transfer_department', lang('transfer_department'), 'required');

     	if ($this->form_validation->run() == true) {
            $transfer_date = $this->input->post('transfer_date');
            $transfer_date = date('Y-m-d', strtotime($transfer_date));

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'transfer_date' => $transfer_date,
                'transfer_department' => $this->input->post('transfer_department'),
                'description' => $this->input->post('description'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     		
     	} elseif ($this->input->post('add_transfer')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_transfer');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_transfer($data)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/transfer');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['all_departments'] = $this->department_model->all_departments();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/transfer'), 'page' => lang('transfers')], ['link' => '#', 'page' => lang('transfers')]];
            $meta = ['page_title' => lang('transfer'), 'bc' => $bc];
            $this->page_construct('core_hr/add_transfer', $meta, $this->data);
	    }
     } 
     public function view_transfer($id)
     {
        $transfer = $this->corehr_model->read_transfer($id);
        $this->data['transfer'] = $transfer;
        $this->data['emp_name'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$transfer->employee_id))->row();
        $this->data['dept_name'] = $this->db->select('department_name')->get_where('departments', array('department_id'=>$transfer->transfer_department))->row()->department_name;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_transfer', $this->data);
     }
     public function edit_transfer($id='')
     {
        $this->data['transfer'] = $this->corehr_model->read_transfer($id);

		$this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['all_departments'] = $this->department_model->all_departments();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/transfer'), 'page' => lang('transfers')], ['link' => '#', 'page' => lang('transfers')]];
            $meta = ['page_title' => lang('transfer'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_transfer', $meta, $this->data);

     }
     public function Editransfer($id='')
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
     	$this->form_validation->set_rules('transfer_date', lang('transfer_date'), 'required');
     	$this->form_validation->set_rules('transfer_department', lang('transfer_department'), 'required');
         $transfer_id = $this->input->post('transfer_id');
     	if ($this->form_validation->run() == true) {
            
            $transfer_date = $this->input->post('transfer_date');
            $transfer_date = date('Y-m-d', strtotime($transfer_date));

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'transfer_date' => $transfer_date,
                'transfer_department' => $this->input->post('transfer_department'),
                'status' => $this->input->post('status'),
                'description' => $this->input->post('description'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     		
     	} elseif ($this->input->post('edit_transfer')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_transfer/'.$transfer_id);
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_transfer($data, $transfer_id)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/transfer');
        }
        else {
            $this->data['transfer'] = $this->corehr_model->read_transfer($id);
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['all_departments'] = $this->department_model->all_departments();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/transfer'), 'page' => lang('transfers')], ['link' => '#', 'page' => lang('transfers')]];
            $meta = ['page_title' => lang('transfer'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_transfer/'.$transfer_id, $meta, $this->data);
	    }
     }
     public function resignations()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/resignations'), 'page' => lang('resignations')], ['link' => '#', 'page' => lang('resignations')]];
        $meta = ['page_title' => lang('resignations'), 'bc' => $bc];
		$this->page_construct('core_hr/resignation_list', $meta, $this->data);
     }
     public function getResignation()
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('employee_resignations.resignation_id as id, CONCAT_WS(" ", first_name, last_name), employee_resignations.notice_date, employee_resignations.resignation_date, employee_resignations.status')
			->from('employee_resignations')
			->join('employees', 'employees.user_id = employee_resignations.employee_id')
			->group_by('employee_resignations.resignation_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_resignation/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_resignation') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_resignation/$1') . "' class='tip' title='" . lang('edit_resignation') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_resignation') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_resignation/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     } 
     public function delete_resignation($id='')
     {
        if ($this->corehr_model->delete_resignation($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('resignation_deleted')]);
        }
     }
     public function add_resignation()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
        $this->form_validation->set_rules('notice_date', lang('notice_date'), 'required');
     	$this->form_validation->set_rules('resignation_date', lang('resignation_date'), 'required');
     	$this->form_validation->set_rules('reason', lang('reason'), 'required');

     	if ($this->form_validation->run() == true) {
            $notice_date = $this->input->post('notice_date');
            $notice_date = date('Y-m-d', strtotime($notice_date));
            $resignation_date = $this->input->post('resignation_date');
            $resignation_date = date('Y-m-d', strtotime($resignation_date));

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'notice_date' => $notice_date,
                'resignation_date' => $resignation_date,
                'reason' => $this->input->post('reason'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('add_resignation')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_resignation');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_resignation($data)) {
            $this->session->set_flashdata('message', lang('resignation_added'));
            admin_redirect('core_hr/resignations');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/resignations'), 'page' => lang('resignations')], ['link' => '#', 'page' => lang('resignations')]];
            $meta = ['page_title' => lang('resignations'), 'bc' => $bc];
            $this->page_construct('core_hr/add_resignation', $meta, $this->data);
	    }
     } 
     public function edit_resignation($resignation_id='')
     {
        $this->data['resignation'] = $this->corehr_model->read_resignation($resignation_id);

		$this->data['all_employees'] = $this->employees_model->all_employees();
        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/resignations'), 'page' => lang('resignations')], ['link' => '#', 'page' => lang('resignations')]];
        $meta = ['page_title' => lang('resignations'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_resignation', $meta, $this->data);

     }
     public function editResignation()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
     	$this->form_validation->set_rules('notice_date', lang('notice_date'), 'required');
     	$this->form_validation->set_rules('resignation_date', lang('resignation_date'), 'required');
     	$this->form_validation->set_rules('reason', lang('reason'), 'required');

         $resignation_id = $this->input->post('resignation_id');
     	if ($this->form_validation->run() == true) {
            
            $notice_date = $this->input->post('notice_date');
            $notice_date = date('Y-m-d', strtotime($notice_date));
            $resignation_date = $this->input->post('resignation_date');
            $resignation_date = date('Y-m-d', strtotime($resignation_date));

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'notice_date' => $notice_date,
                'resignation_date' => $resignation_date,
                'reason' => $this->input->post('reason'),
                'status' => $this->input->post('status'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('edit_resignation')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_resignation/'.$resignation_id);
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_resignation($data, $resignation_id)) {
            $this->session->set_flashdata('message', lang('resignation_edited'));
            admin_redirect('core_hr/resignations');
        }
        else {
            $this->data['resignation'] = $this->corehr_model->read_resignation($resignation_id);
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/resignations'), 'page' => lang('resignations')], ['link' => '#', 'page' => lang('resignations')]];
            $meta = ['page_title' => lang('resignations'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_resignation/'.$resignation_id, $meta, $this->data);
	    }
     } 

     public function promotions()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/promotions'), 'page' => lang('promotions')], ['link' => '#', 'page' => lang('promotions')]];
        $meta = ['page_title' => lang('promotions'), 'bc' => $bc];
		$this->page_construct('core_hr/promotion_list', $meta, $this->data);
     }
     public function getPromotions()
     {
        $this->load->library('datatables');
        $this->datatables
			->select('employee_promotions.promotion_id as id, CONCAT_WS(" ", first_name, last_name), employee_promotions.title, designations.designation_name, employee_promotions.promotion_date')
			->from('employee_promotions')
			->join('employees', 'employees.user_id = employee_promotions.employee_id')
            ->join('designations', 'designations.designation_id = employee_promotions.designation_id')
			->group_by('employee_promotions.promotion_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_promotion/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_promotion') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_promotion/$1') . "' class='tip' title='" . lang('edit_promotion') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_promotion') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_promotion/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }
     public function delete_promotion($id='')
     {
        if ($this->corehr_model->delete_promotion($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('promotion_deleted')]);
        }
     }
     public function view_promotion($promotion_id)
     {
        $promotion = $this->corehr_model->read_promotion($promotion_id);
        $this->data['promotion'] = $promotion;
        $this->data['emp_name'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$promotion->employee_id))->row();
        $this->data['designation'] = $this->db->select('designation_id, designation_name')->get_where('designations', array('designation_id'=>$promotion->designation_id))->row()->designation_name;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_promotion', $this->data);
     }
     public function add_promotion()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
         $this->form_validation->set_rules('designation_id', lang('designation_id'), 'required');
     	$this->form_validation->set_rules('title', lang('title'), 'required');
     	$this->form_validation->set_rules('promotion_date', lang('promotion_date'), 'required');

     	if ($this->form_validation->run() == true) {
            $promotion_date = $this->input->post('promotion_date');
            $promotion_date = date('Y-m-d', strtotime($promotion_date));
            
            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'designation_id' => $this->input->post('designation_id'),
                'title' => $this->input->post('title'),
                'promotion_date' => $promotion_date,
                'description' => $this->input->post('description'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('add_promotion')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_promotion');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_promotion($data)) {
            $this->session->set_flashdata('message', lang('promotion_added'));
            admin_redirect('core_hr/promotions');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
 
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/promotions'), 'page' => lang('promotions')], ['link' => '#', 'page' => lang('promotions')]];
            $meta = ['page_title' => lang('promotions'), 'bc' => $bc];
            $this->page_construct('core_hr/add_promotion', $meta, $this->data);
	    }
     } 

     public function getDesignation()
     {
         if($this->input->post()) {
             
            $emp_id = $this->input->post('empId');
            $emp = $this->db->select('designation_id')->get_where('employees', array('user_id'=>$emp_id))->row();
            $designation = $this->db->select('designation_id, designation_name')->get_where('designations', array('designation_id'=>$emp->designation_id))->row();
            $output = '';

            $output .= '
            <select name="designation_id" class="form-control select" id="designation_id">
                    <option value="">Select</option>
                    <option value="'.$designation->designation_id.'">'. $designation->designation_name .'</option>
            </select>
            ';
            echo $output;

         }
     }
     public function edit_promotion($promotion_id='')
     {
        $promotion = $this->corehr_model->read_promotion($promotion_id);
        $this->data['promotion'] = $promotion;
        $this->data['designation'] = $this->db->get_where('designations', array('designation_id'=>$promotion->designation_id))->row();
        $this->data['all_employees'] = $this->employees_model->all_employees();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/promotions'), 'page' => lang('promotions')], ['link' => '#', 'page' => lang('promotions')]];
        $meta = ['page_title' => lang('promotions'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_promotion', $meta, $this->data);

     }
     public function editPromotion()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
     	$this->form_validation->set_rules('designation_id', lang('designation_id'), 'required');
     	$this->form_validation->set_rules('title', lang('title'), 'required');
     	$this->form_validation->set_rules('promotion_date', lang('promotion_date'), 'required');
        $promotion_id = $this->input->post('promotion_id');
     	if ($this->form_validation->run() == true) {
            
            $promotion_date = $this->input->post('promotion_date');
            $promotion_date = date('Y-m-d', strtotime($promotion_date));
            
            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'designation_id' => $this->input->post('designation_id'),
                'title' => $this->input->post('title'),
                'promotion_date' => $promotion_date,
                'description' => $this->input->post('description'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('edit_promotion')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_promotion/'.$promotion_id);
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_promotion($data, $promotion_id)) {
            $this->session->set_flashdata('message', lang('promotion_added'));
            admin_redirect('core_hr/promotions');
        }
        else {
            $promotion = $this->corehr_model->read_promotion($promotion_id);
            $this->data['promotion'] = $promotion;
            $this->data['designation'] = $this->db->get_where('designations', array('designation_id'=>$promotion->designation_id))->row();
            $this->data['all_employees'] = $this->employees_model->all_employees(); 
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/promotions'), 'page' => lang('promotions')], ['link' => '#', 'page' => lang('promotions')]];
            $meta = ['page_title' => lang('promotions'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_promotion/'.$promotion_id, $meta, $this->data);
	    }
     } 

     public function getTravels()
     {
        $this->load->library('datatables');
        $this->datatables
			->select('employee_travels.travel_id as id, CONCAT_WS(" ", first_name, last_name), employee_travels.visit_place, employee_travels.start_date, employee_travels.end_date, employee_travels.actual_budget, employee_travels.status')
			->from('employee_travels')
			->join('employees', 'employees.user_id = employee_travels.employee_id')
			->group_by('employee_travels.travel_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_travel/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_travel') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_travel/$1') . "' class='tip' title='" . lang('edit_travel') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_travel') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_travel/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }
     public function delete_travel($id='')
     {
        if ($this->corehr_model->delete_travel($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('travel_deleted')]);
        }
     }
     public function view_travel($travel_id)
     {
        $travel = $this->corehr_model->read_travel($travel_id);
        $this->data['travel'] = $travel;
        $this->data['emp_name'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$travel->employee_id))->row();
        //$this->data['designation'] = $this->db->select('designation_id, designation_name')->get_where('designations', array('designation_id'=>$promotion->designation_id))->row()->designation_name;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_travel', $this->data);
     }
     public function travels()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/travels'), 'page' => lang('travels')], ['link' => '#', 'page' => lang('travels')]];
        $meta = ['page_title' => lang('travels'), 'bc' => $bc];
		$this->page_construct('core_hr/travels_list', $meta, $this->data);
     }

     public function add_travel()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
         $this->form_validation->set_rules('start_date', lang('start_date'), 'required');
     	$this->form_validation->set_rules('end_date', lang('end_date'), 'required');
     	$this->form_validation->set_rules('visit_place', lang('visit_place'), 'required');
     	$this->form_validation->set_rules('visit_purpose', lang('visit_purpose'), 'required');

     	if ($this->form_validation->run() == true) {
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $this->input->post('end_date');
            $end_date = date('Y-m-d', strtotime($end_date));
            
            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'visit_place' => $this->input->post('visit_place'),
                'visit_purpose' => $this->input->post('visit_purpose'),
                'start_date' => $start_date,
                'end_date' => $end_date,
                'actual_budget' => $this->input->post('actual_budget'),
                'expected_budget' => $this->input->post('expected_budget'),
                'travel_mode' => $this->input->post('travel_mode'),
                'arrangement_type' => $this->input->post('arrangement_type'),
                'description' => $this->input->post('description'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('add_travel')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_travel');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_travel($data)) {
            $this->session->set_flashdata('message', lang('travel_added'));
            admin_redirect('core_hr/travels');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
 
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/travels'), 'page' => lang('travels')], ['link' => '#', 'page' => lang('travels')]];
            $meta = ['page_title' => lang('travels'), 'bc' => $bc];
            $this->page_construct('core_hr/add_travel', $meta, $this->data);
	    }
     }
     public function add_travel_cal()
     {
         if($this->input->post()) {
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $this->input->post('end_date');
            $end_date = date('Y-m-d', strtotime($end_date));

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'visit_place' => $this->input->post('visit_place'),
                'visit_purpose' => $this->input->post('visit_purpose'),
                'start_date' => $start_date,
                'end_date' => $end_date,
                'actual_budget' => $this->input->post('actual_budget'),
                'expected_budget' => $this->input->post('expected_budget'),
                'travel_mode' => $this->input->post('travel_mode'),
                'arrangement_type' => $this->input->post('arrangement_type'),
                'description' => $this->input->post('description'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );

            $insert = $this->corehr_model->add_travel($data);
            if($insert) {
                return true;
            } else {
                return false;
            }
         }
     }

     public function edit_travel($travel_id='')
     {
        $travel = $this->corehr_model->read_travel($travel_id);
        $this->data['travel'] = $travel;
        $this->data['all_employees'] = $this->employees_model->all_employees();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/travels'), 'page' => lang('travels')], ['link' => '#', 'page' => lang('travels')]];
        $meta = ['page_title' => lang('travels'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_travel', $meta, $this->data);

     }
     public function editTravel()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
         $this->form_validation->set_rules('start_date', lang('start_date'), 'required');
     	$this->form_validation->set_rules('end_date', lang('end_date'), 'required');
     	$this->form_validation->set_rules('visit_place', lang('visit_place'), 'required');
     	$this->form_validation->set_rules('visit_purpose', lang('visit_purpose'), 'required');

     	if ($this->form_validation->run() == true) {

            $travel_id = $this->input->post('travel_id');
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $this->input->post('end_date');
            $end_date = date('Y-m-d', strtotime($end_date));
            
            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'visit_place' => $this->input->post('visit_place'),
                'visit_purpose' => $this->input->post('visit_purpose'),
                'start_date' => $start_date,
                'end_date' => $end_date,
                'actual_budget' => $this->input->post('actual_budget'),
                'expected_budget' => $this->input->post('expected_budget'),
                'travel_mode' => $this->input->post('travel_mode'),
                'arrangement_type' => $this->input->post('arrangement_type'),
                'description' => $this->input->post('description'),
                'status' => $this->input->post('status'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('edit_travel')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_travel/'.$travel_id);
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_travel($data, $travel_id)) {
            $this->session->set_flashdata('message', lang('travel_edited'));
            admin_redirect('core_hr/travels');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
 
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/travels'), 'page' => lang('travels')], ['link' => '#', 'page' => lang('travels')]];
            $meta = ['page_title' => lang('travels'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_travel/'.$travel_id, $meta, $this->data);
	    }
     }

     public function trainings()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('trainings')]];
        $meta = ['page_title' => lang('trainings'), 'bc' => $bc];
		$this->page_construct('core_hr/trainings_list', $meta, $this->data);
     }
     public function getTrainings()
     {
        $records = $this->corehr_model->get_trainings();

        $data = array();
        foreach($records as $value) {
            $nestedData = array();

            $names = '';
            if(!empty($value->employee_id)) {
                $employees = explode(',', $value->employee_id);
                $name_arr = array();
                foreach($employees as $row) {
                    $name = $this->employees_model->get_emp_name($row);
                    $name_arr[] = $name->first_name.' '.$name->last_name; 
                } 
                if(!empty($name_arr)) {
                        $names = implode(', ', $name_arr);
                }
            }
           
            $trainer = $this->db->get_where('trainers', array('trainer_id'=>$value->trainer_id))->row();

            $nestedData[] = $value->training_id;
            $nestedData[] = $value->training_title;
            $nestedData[] = $names;
            $nestedData[] = $trainer->first_name.' '.$trainer->last_name;
            $start_date = date('d-m-Y', strtotime($value->start_date));
            $finish_date = date('d-m-Y', strtotime($value->finish_date));
            $nestedData[] = $start_date. ' To '.$finish_date;
            $nestedData[] = $value->training_cost;

            $edit = "";
            $edit = "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_training/'.$value->training_id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_training') . "'><i class=\"fa fa-file-text\"></i></a>";
            $edit .= " <a href='" . admin_url('core_hr/edit_training/'.$value->training_id) . "' class='tip' title='" . lang('edit_training') . "'><i class=\"fa fa-edit\"></i></a>";
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_training') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_training/'.$value->training_id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
            $nestedData[] = $edit;

			$data[] = $nestedData;
		}
        $totalData = sizeof($records);
        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
     } 
     public function add_training()
     {
     	//$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
        $this->form_validation->set_rules('start_date', lang('start_date'), 'required');
     	$this->form_validation->set_rules('finish_date', lang('finish_date'), 'required');
     	$this->form_validation->set_rules('training_title', lang('training_title'), 'required');
     	
     	if ($this->form_validation->run() == true) {
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $finish_date = $this->input->post('finish_date');
            $finish_date = date('Y-m-d', strtotime($finish_date));
            $employee_ids = $this->input->post('user_ids');
            if(!empty($employee_ids)) {
                $employee_id = implode(',', $employee_ids);
            }
            
            $data = array(
                'employee_id' => $employee_id,
                'training_title' => $this->input->post('training_title'),
                'trainer_id' => $this->input->post('trainer_id'),
                'trainer_option' => $this->input->post('trainer_option'),
                'start_date' => $start_date,
                'finish_date' => $finish_date,
                'training_cost' => $this->input->post('training_cost'),
                'description' => $this->input->post('description'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('add_training')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_training');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_training($data)) {
            $this->session->set_flashdata('message', lang('training_added'));
            admin_redirect('core_hr/trainings');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['trainers'] = $this->db->get('trainers')->result();
 
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/trainings'), 'page' => lang('trainings')], ['link' => '#', 'page' => lang('add_training')]];
            $meta = ['page_title' => lang('add_training'), 'bc' => $bc];
            $this->page_construct('core_hr/add_training', $meta, $this->data);
	    }
     }
     public function add_training_cal()
     {
         if($this->input->post()) {
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $finish_date = $this->input->post('finish_date');
            $finish_date = date('Y-m-d', strtotime($finish_date));
            $employee_ids = $this->input->post('user_ids');
            if(!empty($employee_ids)) {
                $employee_id = implode(',', $employee_ids);
            }
            
            $data = array(
                'employee_id' => $employee_id,
                'training_title' => $this->input->post('training_title'),
                'trainer_id' => $this->input->post('trainer_id'),
                'trainer_option' => $this->input->post('trainer_option'),
                'start_date' => $start_date,
                'finish_date' => $finish_date,
                'training_cost' => $this->input->post('training_cost'),
                'description' => $this->input->post('description'),
		        'created_at' => date('d-m-Y'),
            );

            $insert = $this->corehr_model->add_training($data);
            if($insert) {
                return true;
            } else {
                return false;
            }
         }
     }
     
     public function edit_training($training_id='')
     {
        $training = $this->corehr_model->read_training($training_id);
        $this->data['training'] = $training;
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['trainers'] = $this->db->get('trainers')->result();

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/trainings'), 'page' => lang('trainings')], ['link' => '#', 'page' => lang('trainings')]];
        $meta = ['page_title' => lang('trainings'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_training', $meta, $this->data);

     }
     public function view_training($training_id)
     {
        $training = $this->corehr_model->read_training($training_id);
        $this->data['data'] = $training;
        $this->data['emp_name'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$training->employee_id))->row();

        $training_for_users = explode(',', $training->employee_id);
        $name_arr = array();
        foreach($training_for_users as $user) {
            $emp_name = $this->db->select('first_name, last_name')->get_where('employees', array('user_id'=>$user))->row();
            if(!empty($emp_name)) {
                $fullName = $emp_name->first_name.' '.$emp_name->last_name;
                array_push($name_arr, $fullName);
                $emp_names = implode(', ', $name_arr);
            }
        }
        $this->data['emp_names'] = $emp_names;
        
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_training', $this->data);
     }
     public function editTraining()
     {
     	//$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
        $this->form_validation->set_rules('start_date', lang('start_date'), 'required');
     	$this->form_validation->set_rules('finish_date', lang('finish_date'), 'required');
     	$this->form_validation->set_rules('training_title', lang('training_title'), 'required');
     	
     	if ($this->form_validation->run() == true) {

            $training_id = $this->input->post('training_id');
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $finish_date = $this->input->post('finish_date');
            $finish_date = date('Y-m-d', strtotime($finish_date));
            $employee_ids = $this->input->post('user_ids');
            if(!empty($employee_ids)) {
                $employee_id = implode(',', $employee_ids);
            }
            
            $data = array(
                'employee_id' => $employee_id,
                'training_title' => $this->input->post('training_title'),
                'trainer_id' => $this->input->post('trainer_id'),
                'trainer_option' => $this->input->post('trainer_option'),
                'start_date' => $start_date,
                'finish_date' => $finish_date,
                'training_cost' => $this->input->post('training_cost'),
                'description' => $this->input->post('description'),
		        'created_at' => date('d-m-Y'),
            );
           
     	} elseif ($this->input->post('edit_training')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_training');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_training($data, $training_id)) {
            $this->session->set_flashdata('message', lang('training_edited'));
            admin_redirect('core_hr/trainings');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['trainers'] = $this->db->get('trainers')->result();
 
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/trainings'), 'page' => lang('trainings')]];
            $meta = ['page_title' => lang('edit_training'), 'bc' => $bc];
            $this->page_construct('core_hr/trainings', $meta, $this->data);
	    }
     }
     public function delete_training($id='')
     {
        if ($this->corehr_model->delete_training($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('training_deleted')]);
        }
     }
    public function read_training()
    {
        // print_r($this->input->get());
        $training_id = $this->input->get('training_id');
        $data = $this->corehr_model->read_training($training_id);
         $this->data['data'] = $data;
        if($data) {
            $this->load->view($this->theme.'calendar_hr/dialog_training', $this->data);
        }
    }
    public function trainers()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('trainers')]];
        $meta = ['page_title' => lang('trainers'), 'bc' => $bc];
		$this->page_construct('core_hr/trainers_list', $meta, $this->data);
     }
     public function getTrainers()
     {
        $records = $this->db->get('trainers')->result();

        $data = array();
        foreach($records as $value) {
            $nestedData = array();
            
            $nestedData[] = $value->trainer_id;
            $nestedData[] = $value->first_name.' '.$value->last_name;
            $nestedData[] = $value->contact_number;
            $nestedData[] = $value->email;
            $nestedData[] = $value->expertise;
            $nestedData[] = $value->address;

            $edit = "";
            $edit .= " <a href='" . admin_url('core_hr/edit_trainer_form/'.$value->trainer_id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_trainer') . "'><i class=\"fa fa-edit\"></i></a>";
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_trainer') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_trainer/'.$value->trainer_id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
            $nestedData[] = $edit;

			$data[] = $nestedData;
		}
        $totalData = sizeof($records);
        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
     }  
     public function delete_trainer($id='')
     {
        if ($this->corehr_model->delete_trainer($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('trainer_deleted')]);
        }
     }
     public function add_trainer_form()
     {
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/add_trainer_form', $this->data);
     }
     public function add_trainer()
     {
         if($this->input->post()) {
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $finish_date = $this->input->post('finish_date');
            $finish_date = date('Y-m-d', strtotime($finish_date));
            $employee_ids = $this->input->post('user_ids');
            if(!empty($employee_ids)) {
                $employee_id = implode(',', $employee_ids);
            }
            
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'contact_number' => $this->input->post('contact_number'),
                'email' => $this->input->post('email'),
                'expertise' => $this->input->post('expertise'),
                'address' => $this->input->post('address'),
		        'created_at' => date('d-m-Y'),
            );

            $insert = $this->corehr_model->add_trainer($data);
            if($insert) {
                return true;
            } else {
                return false;
            }
         }
     }
     public function edit_trainer()
     {
         if($this->input->post()) {
            $trainer_id = $this->input->post('trainer_id');
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $finish_date = $this->input->post('finish_date');
            $finish_date = date('Y-m-d', strtotime($finish_date));
            $employee_ids = $this->input->post('user_ids');
            if(!empty($employee_ids)) {
                $employee_id = implode(',', $employee_ids);
            }
            
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'contact_number' => $this->input->post('contact_number'),
                'email' => $this->input->post('email'),
                'expertise' => $this->input->post('expertise'),
                'address' => $this->input->post('address'),
		        'created_at' => date('d-m-Y'),
            );

            $insert = $this->corehr_model->edit_trainer($data, $trainer_id);
            if($insert) {
                return true;
            } else {
                return false;
            }
         }
     }
     public function edit_trainer_form($id)
     {
        $this->data['trainer'] = $this->corehr_model->read_trainer($id);

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/edit_trainer_form', $this->data);
     }

     public function complaints()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/complaints'), 'page' => lang('complaints')], ['link' => '#', 'page' => lang('complaints')]];
        $meta = ['page_title' => lang('complaints'), 'bc' => $bc];
		$this->page_construct('core_hr/complaints_list', $meta, $this->data);
     }
     public function getComplaints()
     {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->db
            ->select('employee_complaints.complaint_id as id, CONCAT_WS(" ", first_name, last_name) as full_name, employee_complaints.complaint_against, employee_complaints.title, employee_complaints.complaint_date, employee_complaints.status')
            ->from('employee_complaints')
            ->join('employees', 'employees.user_id = employee_complaints.complaint_from')
            ->group_by('employee_complaints.complaint_id')
            ->get()
            ->result()
            ;

        foreach($records as $value) {
            $nestedData = array();
				$nestedData[] = $value->id;
				$nestedData[] = $value->full_name;

                $complaint_for_users = explode(',', $value->complaint_against);
                $name_arr = array();
                foreach($complaint_for_users as $user) {
                    $emp_name = $this->db->select('first_name, last_name')->get_where('employees', array('user_id'=>$user))->row();
                    if(!empty($emp_name)) {
                        $fullName = $emp_name->first_name.' '.$emp_name->last_name;
                        array_push($name_arr, $fullName);
                        $emp_names = implode(', ', $name_arr);
                    }
                }
                $nestedData[] = $emp_names;
				$nestedData[] = $value->title;
				$nestedData[] = $value->complaint_date;
                $nestedData[] = $value->status;

				$edit = '';
				$edit = "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_complaint/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_complaint') . "'><i class=\"fa fa-file-text\"></i></a>";
				$edit .= " <a href='" . admin_url('core_hr/edit_complaint/'.$value->id) . "' class='tip' title='" . lang('edit_complaint') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_complaint') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_complaint/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $totalData = sizeof($records);
        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);

     }
     public function delete_complaint($id='')
     {
        if ($this->corehr_model->delete_complaint($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('complaint_deleted')]);
        }
     }
     public function view_complaint($complaint_id)
     {
        $complaint = $this->corehr_model->read_complaint($complaint_id);
        $this->data['complaint'] = $complaint;
        $this->data['emp_name'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$complaint->complaint_from))->row();

        $complaint_for_users = explode(',', $complaint->complaint_against);
        $name_arr = array();
        foreach($complaint_for_users as $user) {
            $emp_name = $this->db->select('first_name, last_name')->get_where('employees', array('user_id'=>$user))->row();
            if(!empty($emp_name)) {
                $fullName = $emp_name->first_name.' '.$emp_name->last_name;
                array_push($name_arr, $fullName);
                $emp_names = implode(', ', $name_arr);
            }
        }
        $this->data['complaint_against'] = $emp_names;
        
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_complaint', $this->data);
     }
     public function edit_complaint($complaint_id='')
     {
        $complaint = $this->corehr_model->read_complaint($complaint_id);
        $this->data['complaint'] = $complaint;
        $this->data['all_employees'] = $this->employees_model->all_employees();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/complaints'), 'page' => lang('complaints')], ['link' => '#', 'page' => lang('complaints')]];
        $meta = ['page_title' => lang('complaints'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_complaint', $meta, $this->data);

     }
     public function add_complaint()
     {
     	$this->form_validation->set_rules('complaint_from', lang('complaint_from'), 'required');
       // $this->form_validation->set_rules('complaint_against', lang('complaint_against'), 'required');
     	$this->form_validation->set_rules('title', lang('title'), 'required');
     	$this->form_validation->set_rules('complaint_date', lang('title'), 'required');

     	if ($this->form_validation->run() == true) {
            $complaint_date = $this->input->post('complaint_date');
            $complaint_date = date('Y-m-d', strtotime($complaint_date));

            $complaint_againsts_input = $this->input->post('complaint_against');
            if(!empty($complaint_againsts_input)) {
                $complaint_againsts = implode(',', $complaint_againsts_input);
            }
            if(!empty($_FILES["attachment"]["name"])) {
                $config['upload_path']          = 'assets/uploads/complaints/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('attachment')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
                $this->upload->initialize($config);
            } 

            $data = array(
                'complaint_from' => $this->input->post('complaint_from'),
                'complaint_against' => $complaint_againsts,
                'complaint_date' => $complaint_date,
                'attachment' => $file_name,
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('add_complaint')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_complaint');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_complaint($data)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/complaints');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/complaints'), 'page' => lang('complaints')], ['link' => '#', 'page' => lang('complaints')]];
            $meta = ['page_title' => lang('complaints'), 'bc' => $bc];
            $this->page_construct('core_hr/add_complaint', $meta, $this->data);
	    }
     } 
     public function editComplaint()
     {
     	$this->form_validation->set_rules('complaint_from', lang('complaint_from'), 'required');
       // $this->form_validation->set_rules('complaint_against', lang('complaint_against'), 'required');
     	$this->form_validation->set_rules('title', lang('title'), 'required');
     	$this->form_validation->set_rules('complaint_date', lang('title'), 'required');

     	if ($this->form_validation->run() == true) {
            $complaint_id = $this->input->post('complaint_id');
            $complaint_date = $this->input->post('complaint_date');
            $complaint_date = date('Y-m-d', strtotime($complaint_date));

            $complaint_againsts_input = $this->input->post('complaint_against');
            if(!empty($complaint_againsts_input)) {
                $complaint_againsts = implode(',', $complaint_againsts_input);
            }
            if(!empty($_FILES["attachment"]["name"])) {
                $document = $this->db->select('attachment')->get_where('employee_complaints', array('complaint_id'=>$complaint_id))->row();
                if(!empty($document->attachment)) {
                    unlink('assets/uploads/complaints/'.$document->attachment);
                }

               $config['upload_path']          = 'assets/uploads/complaints/';
               $config['allowed_types']        = 'gif|jpg|jpeg|png';
               $config['max_size']             = 5000;
               

               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               if ($this->upload->do_upload('attachment')) {
                       $data = array('upload_data' => $this->upload->data());
                       $file_name = $data['upload_data']['file_name'];
                      
               }
               //unset($_FILES);
           } else{
                $document = $this->db->select('attachment')->get_where('employee_complaints', array('complaint_id'=>$complaint_id))->row();
               $file_name = $document->attachment;
           }

            $data = array(
                'complaint_from' => $this->input->post('complaint_from'),
                'complaint_against' => $complaint_againsts,
                'complaint_date' => $complaint_date,
                'attachment' => $file_name,
                'title' => $this->input->post('title'),
                'status' => $this->input->post('status'),
                'description' => $this->input->post('description'),
                
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('edit_complaint')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_complaint/'.$complaint_id);
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_complaint($data, $complaint_id)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/complaints');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/complaints'), 'page' => lang('complaints')], ['link' => '#', 'page' => lang('complaints')]];
            $meta = ['page_title' => lang('complaints'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_complaint/'.$complaint_id, $meta, $this->data);
	    }
     } 

     public function warnings()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/warnings'), 'page' => lang('warnings')], ['link' => '#', 'page' => lang('warnings')]];
        $meta = ['page_title' => lang('warnings'), 'bc' => $bc];
		$this->page_construct('core_hr/warnings_list', $meta, $this->data);
     }

     public function getWarnings()
     {
        $this->load->library('datatables');
        $this->datatables
			->select('employee_warnings.warning_id as id, CONCAT_WS(" ", first_name, last_name), warning_type.type, employee_warnings.subject, employee_warnings.warning_date, CONCAT_WS(" ", first_name, last_name) as warning_by, employee_warnings.status')
			->from('employee_warnings')
			->join('employees', 'employees.user_id = employee_warnings.warning_to')
            ->join('warning_type', 'warning_type.warning_type_id = employee_warnings.warning_type_id')
			->group_by('employee_warnings.warning_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_warning/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_warning') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_warning/$1') . "' class='tip' title='" . lang('edit_warning') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_warning') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_warning/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }
     public function delete_warning($id='')
     {
        if ($this->corehr_model->delete_warning($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('warning_deleted')]);
        }
     }
     public function add_warning()
     {
     	$this->form_validation->set_rules('warning_to', lang('warning_to'), 'required');
     	$this->form_validation->set_rules('warning_type_id', lang('warning_type_id'), 'required');
     	$this->form_validation->set_rules('subject', lang('subject'), 'required');
     	$this->form_validation->set_rules('warning_date', lang('warning_date'), 'required');
     	$this->form_validation->set_rules('warning_by', lang('warning_by'), 'required');

         $file_name = '';
     	if ($this->form_validation->run() == true) {
            $warning_date = $this->input->post('warning_date');
            $warning_date = date('Y-m-d', strtotime($warning_date));

            if(!empty($_FILES["attachment"]["name"])) {
                $config['upload_path']          = 'assets/uploads/warnings/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('attachment')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
                $this->upload->initialize($config);
            } 

            $data = array(
                'warning_to' => $this->input->post('warning_to'),
                'warning_by' => $this->input->post('warning_by'),
                'warning_type_id' => $this->input->post('warning_type_id'),
                'warning_date' => $warning_date,
                'attachment' => $file_name,
                'subject' => $this->input->post('subject'),
                'description' => $this->input->post('description'),
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('add_warning')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_warning');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_warning($data)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/warnings');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['warning_type'] = $this->db->get('warning_type')->result();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/warnings'), 'page' => lang('warnings')], ['link' => '#', 'page' => lang('warnings')]];
            $meta = ['page_title' => lang('warnings'), 'bc' => $bc];
            $this->page_construct('core_hr/add_warning', $meta, $this->data);
	    }
     } 
     public function edit_warning($warning_id='')
     {
        $warning = $this->corehr_model->read_warning($warning_id);
        $this->data['emp_warning'] = $warning;
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['warning_type'] = $this->db->get('warning_type')->result();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/warnings'), 'page' => lang('warnings')], ['link' => '#', 'page' => lang('warnings')]];
        $meta = ['page_title' => lang('warnings'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_warning', $meta, $this->data);

     }
     public function editWarning()
     {
     	$this->form_validation->set_rules('warning_to', lang('warning_to'), 'required');
     	$this->form_validation->set_rules('warning_type_id', lang('warning_type_id'), 'required');
     	$this->form_validation->set_rules('subject', lang('subject'), 'required');
     	$this->form_validation->set_rules('warning_date', lang('warning_date'), 'required');
     	$this->form_validation->set_rules('warning_by', lang('warning_by'), 'required');

         $file_name = '';
     	if ($this->form_validation->run() == true) {
            $warning_id = $this->input->post('warning_id');
            $warning_date = $this->input->post('warning_date');
            $warning_date = date('Y-m-d', strtotime($warning_date));

            if(!empty($_FILES["attachment"]["name"])) {
                $document = $this->db->select('attachment')->get_where('employee_warnings', array('warning_id'=>$warning_id))->row();
                if(!empty($document->attachment)) {
                    unlink('assets/uploads/warnings/'.$document->attachment);
                }

               $config['upload_path']          = 'assets/uploads/warnings/';
               $config['allowed_types']        = 'gif|jpg|jpeg|png';
               $config['max_size']             = 5000;
               
               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               if ($this->upload->do_upload('attachment')) {
                       $data = array('upload_data' => $this->upload->data());
                       $file_name = $data['upload_data']['file_name'];
                      
               }
               //unset($_FILES);
           } else{
                $document = $this->db->select('attachment')->get_where('employee_warnings', array('warning_id'=>$warning_id))->row();
               $file_name = $document->attachment;
           }

            $data = array(
                'warning_to' => $this->input->post('warning_to'),
                'warning_by' => $this->input->post('warning_by'),
                'warning_type_id' => $this->input->post('warning_type_id'),
                'warning_date' => $warning_date,
                'attachment' => $file_name,
                'subject' => $this->input->post('subject'),
                'description' => $this->input->post('description'),
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('add_warning')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_warning/'.$warning_id);
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_warning($data, $warning_id)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/warnings');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['warning_type'] = $this->db->get('warning_type')->result();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/warnings'), 'page' => lang('warnings')], ['link' => '#', 'page' => lang('warnings')]];
            $meta = ['page_title' => lang('warnings'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_warning/'.$warning_id, $meta, $this->data);
	    }
     } 
     public function view_warning($warning_id)
     {
        $warning = $this->corehr_model->read_warning($warning_id);
        $this->data['warning'] = $warning;
        $this->data['warning_to'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$warning->warning_to))->row();
        $this->data['warning_by'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$warning->warning_by))->row();
        
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_warning', $this->data);
     }

     public function terminations()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/terminations'), 'page' => lang('terminations')], ['link' => '#', 'page' => lang('terminations')]];
        $meta = ['page_title' => lang('terminations'), 'bc' => $bc];
		$this->page_construct('core_hr/termination_list', $meta, $this->data);
     }
     public function getTermination()
     {
        $this->load->library('datatables');
        $this->datatables
			->select('employee_terminations.termination_id as id, CONCAT_WS(" ", first_name, last_name), termination_type.type, employee_terminations.notice_date, employee_terminations.termination_date, employee_terminations.status')
			->from('employee_terminations')
			->join('employees', 'employees.user_id = employee_terminations.employee_id')
            ->join('termination_type', 'termination_type.termination_type_id = employee_terminations.termination_type_id')
			->group_by('employee_terminations.termination_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_termination/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_termination') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_termination/$1') . "' class='tip' title='" . lang('edit_termination') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_termination') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_termination/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }
     public function delete_termination($id='')
     {
        if ($this->corehr_model->delete_termination($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('termination_deleted')]);
        }
     }
     public function add_termination()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
     	$this->form_validation->set_rules('termination_type_id', lang('termination_type_id'), 'required');
     	$this->form_validation->set_rules('notice_date', lang('notice_date'), 'required');
     	$this->form_validation->set_rules('termination_date', lang('termination_date'), 'required');

        $file_name = '';
     	if ($this->form_validation->run() == true) {
            $notice_date = $this->input->post('notice_date');
            $notice_date = date('Y-m-d', strtotime($notice_date));
            $termination_date = $this->input->post('termination_date');
            $termination_date = date('Y-m-d', strtotime($termination_date));

            if(!empty($_FILES["attachment"]["name"])) {
                $config['upload_path']          = 'assets/uploads/terminations/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('attachment')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
                $this->upload->initialize($config);
            } 

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'termination_type_id' => $this->input->post('termination_type_id'),
                'notice_date' => $notice_date,
                'termination_date' => $termination_date,
                'attachment' => $file_name,
                'description' => $this->input->post('description'),
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('add_termination')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_termination');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_termination($data)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/terminations');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['termination_type'] = $this->db->get('termination_type')->result();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/terminations'), 'page' => lang('terminations')], ['link' => '#', 'page' => lang('terminations')]];
            $meta = ['page_title' => lang('terminations'), 'bc' => $bc];
            $this->page_construct('core_hr/add_termination', $meta, $this->data);
	    }
     } 
     public function edit_termination($termination_id='')
     {
        $termination = $this->corehr_model->read_termination($termination_id);
        $this->data['termination'] = $termination;
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['termination_type'] = $this->db->get('termination_type')->result();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/terminations'), 'page' => lang('terminations')], ['link' => '#', 'page' => lang('terminations')]];
        $meta = ['page_title' => lang('terminations'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_termination', $meta, $this->data);

     }
     public function editTermination()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
     	$this->form_validation->set_rules('termination_type_id', lang('termination_type_id'), 'required');
     	$this->form_validation->set_rules('notice_date', lang('notice_date'), 'required');
     	$this->form_validation->set_rules('termination_date', lang('termination_date'), 'required');

        $file_name = '';
     	if ($this->form_validation->run() == true) {
            $termination_id = $this->input->post('termination_id');
            $notice_date = $this->input->post('notice_date');
            $notice_date = date('Y-m-d', strtotime($notice_date));
            $termination_date = $this->input->post('termination_date');
            $termination_date = date('Y-m-d', strtotime($termination_date));

            if(!empty($_FILES["attachment"]["name"])) {
                $document = $this->db->select('attachment')->get_where('employee_terminations', array('termination_id'=>$termination_id))->row();
                if(!empty($document->attachment)) {
                    unlink('assets/uploads/terminations/'.$document->attachment);
                }

               $config['upload_path']          = 'assets/uploads/terminations/';
               $config['allowed_types']        = 'gif|jpg|jpeg|png';
               $config['max_size']             = 5000;
               
               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               if ($this->upload->do_upload('attachment')) {
                       $data = array('upload_data' => $this->upload->data());
                       $file_name = $data['upload_data']['file_name'];
                      
               }
               //unset($_FILES);
           } else{
                $document = $this->db->select('attachment')->get_where('employee_terminations', array('termination_id'=>$termination_id))->row();
               $file_name = $document->attachment;
           }

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'termination_type_id' => $this->input->post('termination_type_id'),
                'notice_date' => $notice_date,
                'termination_date' => $termination_date,
                'attachment' => $file_name,
                'description' => $this->input->post('description'),
                'status' => $this->input->post('status'),
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('edit_termination')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_termination');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_termination($data, $termination_id)) {
            $this->session->set_flashdata('message', lang('transfer_added'));
            admin_redirect('core_hr/terminations');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['termination_type'] = $this->db->get('termination_type')->result();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/terminations'), 'page' => lang('terminations')], ['link' => '#', 'page' => lang('terminations')]];
            $meta = ['page_title' => lang('terminations'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_termination', $meta, $this->data);
	    }
     } 
     public function view_termination($termination_id)
     {
        $termination = $this->corehr_model->read_termination($termination_id);
        $this->data['termination'] = $termination;
        $this->data['employee'] = $this->db->select('user_id, first_name, last_name')->get_where('employees', array('user_id'=>$termination->employee_id))->row();
        $this->data['termination_type'] = $this->db->select('*')->get_where('termination_type', array('termination_type_id'=>$termination->termination_type_id))->row();
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_termination', $this->data);
     }

     public function employee_exit()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/employee_exit'), 'page' => lang('employee_exit')], ['link' => '#', 'page' => lang('employee_exit')]];
        $meta = ['page_title' => lang('employee_exit'), 'bc' => $bc];
		$this->page_construct('core_hr/employee_exit_list', $meta, $this->data);
     }
     public function getEmployeeExit()
     {
        $this->load->library('datatables');
        $this->datatables
			->select('employee_exit.exit_id as id, CONCAT_WS(" ", first_name, last_name), employee_exit_type.type, employee_exit.exit_date, employee_exit.exit_interview')
			->from('employee_exit')
			->join('employees', 'employees.user_id = employee_exit.employee_id')
            ->join('employee_exit_type', 'employee_exit_type.exit_type_id = employee_exit.exit_type_id')
			->group_by('employee_exit.exit_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_employee_exit/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_employee_exit') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_employee_exit/$1') . "' class='tip' title='" . lang('edit_employee_exit') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_employee_exit') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_employee_exit/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }
     public function delete_employee_exit($id='')
     {
        if ($this->corehr_model->delete_employee_exit($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('employee_exit_deleted')]);
        }
     }
     public function add_employee_exit()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
     	$this->form_validation->set_rules('exit_type', lang('exit_type'), 'required');
     	$this->form_validation->set_rules('exit_date', lang('exit_date'), 'required');

        $file_name = '';
     	if ($this->form_validation->run() == true) {
            $exit_date = $this->input->post('exit_date');
            $exit_date = date('Y-m-d', strtotime($exit_date));

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'exit_type_id' => $this->input->post('exit_type'),
                'exit_date' => $exit_date,
                'exit_interview' => $this->input->post('exit_interview'),
                'is_inactivate_account' => $this->input->post('is_inactivate_account'),
                'reason' => $this->input->post('reason'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('add_employee_exit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/add_employee_exit');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->add_employee_exit($data)) {
            $this->session->set_flashdata('message', lang('employee_exit_added'));
            admin_redirect('core_hr/employee_exit');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['employee_exit_type'] = $this->db->get('employee_exit_type')->result();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/employee_exit'), 'page' => lang('employee_exit')], ['link' => '#', 'page' => lang('employee_exit')]];
            $meta = ['page_title' => lang('employee_exit'), 'bc' => $bc];
            $this->page_construct('core_hr/add_employee_exit', $meta, $this->data);
	    }
     } 
     public function edit_employee_exit($employee_exit_id='')
     {
        $employee_exit = $this->corehr_model->read_employee_exit($employee_exit_id);
        $this->data['employee_exit'] = $employee_exit;
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['employee_exit_type'] = $this->db->get('employee_exit_type')->result();
		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/employee_exit'), 'page' => lang('employee_exit')], ['link' => '#', 'page' => lang('employee_exit')]];
        $meta = ['page_title' => lang('employee_exit'), 'bc' => $bc];
        $this->page_construct('core_hr/edit_employee_exit', $meta, $this->data);

     }
     public function editEmployeeExit()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');
     	$this->form_validation->set_rules('exit_type', lang('exit_type'), 'required');
     	$this->form_validation->set_rules('exit_date', lang('exit_date'), 'required');

        $file_name = '';
     	if ($this->form_validation->run() == true) {
            $exit_id = $this->input->post('exit_id');
            $exit_date = $this->input->post('exit_date');
            $exit_date = date('Y-m-d', strtotime($exit_date));

            $data = array(
                'employee_id' => $this->input->post('employee_id'),
                'exit_type_id' => $this->input->post('exit_type'),
                'exit_date' => $exit_date,
                'exit_interview' => $this->input->post('exit_interview'),
                'is_inactivate_account' => $this->input->post('is_inactivate_account'),
                'reason' => $this->input->post('reason'),
                'added_by' => $this->session->userdata('user_id'),
		        'created_at' => date('d-m-Y'),
            );
            // print_r($data); die;
     		
     	} elseif ($this->input->post('edit_employee_exit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('core_hr/edit_employee_exit');
        }

        if($this->form_validation->run() == true &&           $this->corehr_model->edit_employee_exit($data, $exit_id)) {
            $this->session->set_flashdata('message', lang('employee_exit_added'));
            admin_redirect('core_hr/employee_exit');
        }
        else {
            $this->data['all_employees'] = $this->employees_model->all_employees();
            $this->data['employee_exit_type'] = $this->db->get('employee_exit_type')->result();
            $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('core_hr/employee_exit'), 'page' => lang('employee_exit')], ['link' => '#', 'page' => lang('employee_exit')]];
            $meta = ['page_title' => lang('employee_exit'), 'bc' => $bc];
            $this->page_construct('core_hr/edit_employee_exit', $meta, $this->data);
	    }
     } 

     public function calendar_hr()
    {
        $this->data['cal_lang'] = $this->get_cal_lang();
        $this->data['all_holidays'] = $this->db->where('is_publish', 1)->get('holidays');
        $this->data['leave_applications'] = $this->db->get('leave_applications');
        $this->data['all_events'] = $this->db->get('events')->result();
        $this->data['all_meetings'] = $this->db->get('meetings')->result();
        $this->data['all_upcoming_birthday'] = $this->corehr_model->employees_upcoming_birthday();
        $this->data['all_travel_request'] = $this->db->get('employee_travels');
        $this->data['all_training'] = $this->db->get('training');
        
        // print_r($this->data['all_upcoming_birthday']); die;
        $bc                     = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('calendar')]];
        $meta                   = ['page_title' => lang('calendar'), 'bc' => $bc];
        $this->page_construct('core_hr/calendar_hr', $meta, $this->data);
    }
    

    // add record of event/meeting/tasks/projects....
	public function add_cal_record()
	{
		//$data['title'] = $this->Xin_model->site_title();
		$record = $this->input->get('record');
        // print_r($record); die;
		$this->data = array(
		//'all_companies' => $this->Xin_model->get_companies(),
		//'get_all_companies' => $this->Xin_model->get_companies(),
		'all_employees' => $this->employees_model->all_employees(),
		// 'all_tracking_types' => $this->Goal_tracking_model->all_tracking_types(),
		'all_leave_types' => $this->employees_model->leave_categories(),
        'trainers' => $this->db->get('trainers')->result(),
		// 'travel_arrangement_types' => $this->Travel_model->travel_arrangement_types(),
		// 'all_trainers' => $this->Trainers_model->all_trainers(),
		// 'all_training_types' => $this->Training_model->all_training_types(),
		);
		//$session = $this->session->userdata('user_id');
		if(1){ 
			if($record == 0){
				$this->load->view($this->theme .'calendar_hr/options/dialog_add_holiday');
			} else if($record == 1){
                $this->load->view($this->theme .'calendar_hr/options/dialog_add_leave', $this->data);
				//$this->load->view('admin/calendar/options/dialog_add_leave', $data);
			} else if($record == 2){
                $this->load->view($this->theme .'calendar_hr/options/dialog_add_travel', $this->data);
			} else if($record == 3){
                $this->load->view($this->theme .'calendar_hr/options/dialog_add_training', $this->data);
			}  else if($record == 6){
				$this->load->view($this->theme .'calendar_hr/options/dialog_add_events', $this->data);
			} else if($record == 7){
                $this->load->view($this->theme .'calendar_hr/options/dialog_add_meetings', $this->data);
			}  
		} 
        // else {
		// 	redirect('admin/');
		// }
	}
    public function holidays()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('holidays')]];
        $meta = ['page_title' => lang('holidays'), 'bc' => $bc];
		$this->page_construct('core_hr/holidays_list', $meta, $this->data);
     }
     public function getHolidays()
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('holidays.holiday_id as id, holidays.event_name, holidays.start_date, holidays.end_date, holidays.is_publish')
			->from('holidays')
			->group_by('holidays.holiday_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/edit_holiday/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_holiday') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_holiday') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_holiday/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }  
     public function edit_holiday($id='')
     {
        $this->data['holiday'] = $this->corehr_model->read_holiday_record($id);

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/edit_holiday', $this->data);
     }
     public function delete_holiday($id='')
     {
        if ($this->corehr_model->delete_holiday($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('holiday_deleted')]);
        }
     }
     public function add_holiday_form()
     {
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/add_holiday_form', $this->data);
     }
    public function add_holiday()
    {
        if($this->input->post()) {
            $start_date = $this->input->post('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $this->input->post('end_date');
            $end_date = date('Y-m-d', strtotime($end_date));

            $data = array(
				'event_name' => $this->input->post('event_name'),
				'start_date' => $start_date,
				'end_date' => $end_date,
				'is_publish' => $this->input->post('is_publish'),
				'description' => $this->input->post('description'),
				'created_at' => date('d-m-Y')
			);
			$insert = $this->corehr_model->add_holiday($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
        }
    }
    public function read_holiday_record()
    {
        // print_r($this->input->get());
        $holiday_id = $this->input->get('holiday_id');
        $data = $this->corehr_model->read_holiday_record($holiday_id);
         $this->data['data'] = $data;
        if($data) {
            $this->load->view($this->theme.'calendar_hr/dialog_holiday', $this->data);
        }
    }

    public function events()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('events')]];
        $meta = ['page_title' => lang('events'), 'bc' => $bc];
		$this->page_construct('core_hr/events_list', $meta, $this->data);
     }
     
     public function getEvents()
     {
        $this->load->library('datatables');
        $this->datatables
			->select('events.event_id as id, CONCAT_WS(" ", first_name, last_name), events.event_title, events.event_date, events.event_time')
			->from('events')
			->join('employees', 'employees.user_id = events.employee_id')
			->group_by('events.event_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_event/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_event') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_event_from/$1') . "' data-toggle='modal' data-target='#myModal' class='tip'  title='" . lang('edit_event') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_event') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_event/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }
     public function delete_event($id='')
     {
        if ($this->corehr_model->delete_event($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('event_deleted')]);
        }
     }
     
     public function add_event_form()
     {
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/add_event', $this->data);
     }
     public function edit_event_from($id)
     {
        $this->data['event'] = $this->corehr_model->read_event($id);
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/edit_event_from', $this->data);
     }
     public function view_event($id)
     {
        $this->data['data'] = $this->corehr_model->read_event($id);
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_event', $this->data);
     } 
    public function add_events()
    {
        if($this->input->post()) {
            $event_date = $this->input->post('event_date');
            $event_date = date('Y-m-d', strtotime($event_date));
            
            $user_ids = $this->input->post('user_ids');
            if(!empty($user_ids)) {
                $employee_ids = implode(',', $user_ids);
            }

            $data = array(
				'employee_id' => $employee_ids,
				'event_title' => $this->input->post('event_title'),
				'event_date' => $event_date,
				'event_time' => $this->input->post('event_time'),
				'event_note' => $this->input->post('event_note'),
				'created_at' => date('d-m-Y')
			);
			$insert = $this->corehr_model->add_events($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
        }
    }
    public function edit_events()
    {
        if($this->input->post()) {
            $event_id = $this->input->post('event_id');
            $event_date = $this->input->post('event_date');
            $event_date = date('Y-m-d', strtotime($event_date));
            
            $user_ids = $this->input->post('user_ids');
            if(!empty($user_ids)) {
                $employee_ids = implode(',', $user_ids);
            }

            $data = array(
				'employee_id' => $employee_ids,
				'event_title' => $this->input->post('event_title'),
				'event_date' => $event_date,
				'event_time' => $this->input->post('event_time'),
				'event_note' => $this->input->post('event_note'),
				'created_at' => date('d-m-Y')
			);
			$insert = $this->corehr_model->edit_events($data, $event_id);
			if($insert) {
				return true;
                // $this->sma->send_json(['error' => 0, 'msg' => lang('event_edited')]);

			} else {
				return false;
			}
        }
    }

    public function read_leave_record()
    {
        // print_r($this->input->get());
        $leave_id = $this->input->get('leave_id');
        $data = $this->corehr_model->read_leave_record($leave_id);
         $this->data['data'] = $data;
        if($data) {
            $this->load->view($this->theme.'calendar_hr/dialog_leave', $this->data);
        }
    }
    public function read_travel_record()
    {
        // print_r($this->input->get());
        $travel_id = $this->input->get('travel_id');
        $data = $this->corehr_model->read_travel($travel_id);
         $this->data['travel'] = $data;
        if($data) {
            $this->load->view($this->theme.'calendar_hr/dialog_travel', $this->data);
        }
    }
    public function read_event_record()
    {
        // print_r($this->input->get());
        $id = $this->input->get('event_id');
        $data = $this->corehr_model->read_event_record($id);
         $this->data['data'] = $data;
        if($data) {
            $this->load->view($this->theme.'calendar_hr/dialog_event', $this->data);
        }
    }

    public function read_meeting_record()
    {
        // print_r($this->input->get());
        $id = $this->input->get('meeting_id');
        $data = $this->corehr_model->read_meeting_record($id);
         $this->data['data'] = $data;
        if($data) {
            $this->load->view($this->theme.'calendar_hr/dialog_meeting', $this->data);
        }
    }
    public function view_meeting($id)
     {
        $this->data['data'] = $this->corehr_model->read_meeting_record($id);
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'core_hr/view_meeting', $this->data);
     } 
    public function meetings()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('meetings')]];
        $meta = ['page_title' => lang('meetings'), 'bc' => $bc];
		$this->page_construct('core_hr/meetings_list', $meta, $this->data);
     }
     public function getMeetings()
     {
        $this->load->library('datatables');
        $this->datatables
			->select('meetings.meeting_id as id, CONCAT_WS(" ", first_name, last_name), meetings.meeting_title, meetings.meeting_date, meetings.meeting_time , meetings.meeting_room')
			->from('meetings')
			->join('employees', 'employees.user_id = meetings.employee_id')
			->group_by('meetings.meeting_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('core_hr/view_meeting/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_meeting') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('core_hr/edit_meeting_form/$1') . "' data-toggle='modal' data-target='#myModal' class='tip'  title='" . lang('edit_meeting') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_meeting') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('core_hr/delete_meeting/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     } 
     public function delete_meeting($id='')
     {
        if ($this->corehr_model->delete_meeting($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('meeting_deleted')]);
        }
     }
     public function add_meeting_form()
     {
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/add_meetings', $this->data);
     } 
    public function add_meetings()
    {
        if($this->input->post()) {
            $meeting_date = $this->input->post('meeting_date');
            $meeting_date = date('Y-m-d', strtotime($meeting_date));
            
            $user_ids = $this->input->post('user_ids');
            if(!empty($user_ids)) {
                $employee_ids = implode(',', $user_ids);
            }

            $data = array(
				'employee_id' => $employee_ids,
				'meeting_title' => $this->input->post('meeting_title'),
				'meeting_date' => $meeting_date,
				'meeting_time' => $this->input->post('meeting_time'),
				'meeting_room' => $this->input->post('meeting_room'),
				'meeting_note' => $this->input->post('meeting_note'),
				'meeting_color' => '#605ca8',
				'created_at' => date('d-m-Y')
			);
			$insert = $this->corehr_model->add_meetings($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
        }
    }
    public function edit_meeting_form($id)
     {
        $this->data['meeting'] = $this->corehr_model->read_meeting_record($id);
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'core_hr/edit_meeting_form', $this->data);
     }
    public function edit_meetings()
    {
        if($this->input->post()) {
            $meeting_id = $this->input->post('meeting_id');
            $meeting_date = $this->input->post('meeting_date');
            $meeting_date = date('Y-m-d', strtotime($meeting_date));
            
            $user_ids = $this->input->post('user_ids');
            if(!empty($user_ids)) {
                $employee_ids = implode(',', $user_ids);
            }

            $data = array(
				'employee_id' => $employee_ids,
				'meeting_title' => $this->input->post('meeting_title'),
				'meeting_date' => $meeting_date,
				'meeting_time' => $this->input->post('meeting_time'),
				'meeting_room' => $this->input->post('meeting_room'),
				'meeting_note' => $this->input->post('meeting_note'),
				'meeting_color' => '#605ca8',
				'created_at' => date('d-m-Y')
			);
			$insert = $this->corehr_model->edit_meetings($data, $meeting_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
        }
    }

    public function upc_birthdays()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
         $this->data['all_employees'] = $this->employees_model->all_employees();

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('birthdays')]];
        $meta = ['page_title' => lang('birthdays'), 'bc' => $bc];
		$this->page_construct('core_hr/birthdays_list', $meta, $this->data);
     }
     public function getBirthdays()
     {
        //$empid = $this->input->post('empid'); 
        //$empid = $this->input->post('empid');

        $records = $this->corehr_model->employees_upcoming_birthday();


        foreach($records as $value) {
            $nestedData = array();

            $department = $this->db->get_where('departments', array('department_id' => $value->department_id))->row();
            $designation = $this->db->get_where('designations', array('designation_id' => $value->designation_id))->row();

            $nestedData[] = $value->user_id;
            $nestedData[] = $value->first_name.' '.$value->last_name;
            $nestedData[] = $department->department_name;
            $nestedData[] = $designation->designation_name;
            $date_of_birth = date('d M, Y', strtotime($value->date_of_birth));
            $nestedData[] = $date_of_birth;
            $nestedData[] = $value->age;

			$data[] = $nestedData;
		}
        $totalData = sizeof($records);
        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
     }  


    public function get_cal_lang()
    {
        switch ($this->Settings->user_language) {
            case 'arabic':
            $cal_lang = 'ar-ma';
            break;
            case 'french':
            $cal_lang = 'fr';
            break;
            case 'german':
            $cal_lang = 'de';
            break;
            case 'italian':
            $cal_lang = 'it';
            break;
            case 'portuguese-brazilian':
            $cal_lang = 'pt-br';
            break;
            case 'simplified-chinese':
            $cal_lang = 'zh-tw';
            break;
            case 'spanish':
            $cal_lang = 'es';
            break;
            case 'thai':
            $cal_lang = 'th';
            break;
            case 'traditional-chinese':
            $cal_lang = 'zh-cn';
            break;
            case 'turkish':
            $cal_lang = 'tr';
            break;
            case 'vietnamese':
            $cal_lang = 'vi';
            break;
            default:
            $cal_lang = 'en';
            break;
        }
        return $cal_lang;
    }
}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->lang->admin_load('cron');
        $this->load->admin_model('cron_model');
        $this->load->admin_model('employees_model');
        $this->load->admin_model('roster_model');
        $this->load->admin_model('attendance_model');
        $this->Settings = $this->cron_model->getSettings();
    }

    public function index()
    {
        show_404();
    }

    public function run()
    {
        if ($m = $this->cron_model->run_cron()) {
            if ($this->input->is_cli_request()) {
                foreach ($m as $msg) {
                    echo $msg . "\n";
                }
            } else {
                echo '<!doctype html><html><head><title>Cron Job</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style></head><body>';
                echo '<p>' . lang('cron_finished') . '</p>';
                foreach ($m as $msg) {
                    echo '<p>' . $msg . '</p>';
                }
                echo '</body></html>';
            }
        }
    }
    public function run_hr_functions()
    {
        $this->cron_model->add_roster_all_employees(); 
        $this->cron_model->roster_process_all_employees(); 
    }
    public function hr_auto_roster_process()
    {
        $this->cron_model->roster_process_all_employees(); 
        $this->session->set_flashdata('message', lang('Roster is processed for all employees!'));
        admin_redirect('hrm_roster/rosters_list');
    }
    public function hr_add_roster_default()
    {
        $this->cron_model->add_roster_all_employees(); 
        $this->session->set_flashdata('message', lang('Roster is added for all employees!'));
        admin_redirect('hrm_roster/rosters_list');
    }
    public function run_sync_purchase_raw()
    {
        $this->cron_model->synchronize_raw_purchase_from_local(); 
    }
    
    public function run_db_backup()
    {
        $this->cron_model->db_backup(); 
    }
    public function delete_old_data_1year()
    {
        // $this->cron_model->delete_sales_data(); 
        // $this->cron_model->delete_purchase_data(); 
        // $this->cron_model->delete_purchase_raw_data(); 
        // $this->cron_model->delete_attendance_log(); 
    }
    public function delete_old_data_2months()
    {
        // $this->cron_model->delete_api_log(); 
        // $this->cron_model->delete_sma_logs(); 
    }
    public function run_synchronize_salesdata()
    {
        // $this->cron_model->synchronize_salesdata_from_local(); //offline
        /*
        $this->cron_model->synchronize_salesdata_from_online(); // online

        $this->cron_model->synchronize_purchases_from_online();
        $this->cron_model->synchronize_purchasesdata_from_local();

        $this->cron_model->synchronize_expenses_from_online();
        $this->cron_model->synchronize_expenses_from_local();   
        
        $this->cron_model->staff_reminders();
        $this->cron_model->tasks_reminders();
        $this->cron_model->recurring_tasks(); 
        */
        /*
        Recurring task condition:
        If todays date is greater than or equal last_recurring_data in task table
        if (date('Y-m-d') >= $re_create_at)
        */
    }
    public function run_attandance()
    {
        $this->load->library('ZKLibrary');
        $zk = new ZKLibrary('192.168.1.201', 4370, 'TCP');
        $zk->connect();
        $zk->disableDevice();
        $attendace = $zk->getAttendance();
        $no = 1;
        $totalnumber = count($attendace);
        $entryStatus = false;
        if(count($attendace)>0){
            $newPostArray=array();
            $settings = $this->site->get_setting(); 
            $newPostArray['api-key']=$settings->api_key;
             $url = $settings->online_link . "api/v1/hrm_rosters/add_attendance_log";
            foreach($attendace as $key=>$at)
            {
                $newPostArray['log_day_time']=$at[3];
                $newPostArray['device']="1";
                $newPostArray['card_no']=$at[1];
                $options = array(
                  'http' => array(
                    'method'  => 'POST',
                    'content' => json_encode( $newPostArray ),
                    'header'=>  "Content-Type: application/json\r\n" .
                                "Accept: application/json\r\n"
                    )
                );
                $context  = stream_context_create( $options );
                $result = file_get_contents( $url, false, $context );
                $response = json_decode( $result );
                $no++;
                if($totalnumber==$no){
                    $entryStatus = true;
                }
            }
        }
        if($entryStatus){
        //$zk->clearAttendance();
        }
        $zk->enableDevice();
        $zk->disconnect();
        
    }
}

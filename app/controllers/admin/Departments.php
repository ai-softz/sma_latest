<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Departments extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('department_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('employees_model');
	}

	public function getAllDepartments()
	{
     	$this->load->library('datatables');

        $this->datatables
			->select('department_id as id, department_name, warehouses.name')
			->from('departments')
			->join('warehouses', 'warehouses.id = departments.location_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('departments/edit_department/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_department') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_department') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('departments/delete_department/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
	}

	public function delete_department($id='')
	{
		if($this->department_model->delete_department($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('department_deleted')]);
     	}
	}

	public function add_department()
	{
		// print_r($this->input->post());die;
		if($this->input->post()) {
			
			$data = [
				'department_name' => $this->input->post('department_name'),
				'secondary_name' => $this->input->post('secondary_name'),
				'location_id' => $this->input->post('warehouse_id'),
				'employee_id' => $this->input->post('employee_id'),
				'added_by' => $this->session->userdata('user_id'),
				'created_at' => date('Y-m-d'),
				'status' => 1
			];

			$insert = $this->department_model->add_department($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function edit_department($id='')
	{
		$this->data['department'] = $this->db->get_where('departments', array('department_id' => $id))->row();
		$this->data['all_warehouses'] = $this->settings_model->getAllWarehouses();
        $this->data['all_employees'] = $this->employees_model->all_employees();

		$this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'organizations/edit_department', $this->data);
	}

	public function editDepartment()
     {
     	
     	if($this->input->post()) {
     		$department_id = $this->input->post('department_id');
			
			$data = [
				'department_name' => $this->input->post('department_name'),
				'secondary_name' => $this->input->post('secondary_name'),
				'location_id' => $this->input->post('warehouse_id'),
				'employee_id' => $this->input->post('employee_id'),
				'status' => 1
				
			];

			$update = $this->department_model->editDepartment($data, $department_id);
			if($update) {
				return true;
			} else {
				return false;
			}
		}
     }

}
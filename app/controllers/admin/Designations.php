<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Designations extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('department_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('designation_model');
	}

	public function getAllDesignations()
	{
     	$this->load->library('datatables');

        $this->datatables
			->select('designation_id as id, designation_name, departments.department_name')
			->from('designations')
			->join('departments', 'departments.department_id = designations.department_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('designations/edit_designation/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_designation') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_designation') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('designations/delete_designation/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
	}

	public function delete_designation($id='')
	{
		if($this->designation_model->delete_designation($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('designation_deleted')]);
     	}
	}

	public function add_designation()
	{
		// print_r($this->input->post());die;
		if($this->input->post()) {
			
			$data = [
				'department_id' => $this->input->post('department_id'),
				'designation_name' => $this->input->post('designation_name'),
				'secondary_name' => $this->input->post('secondary_name'),
				'description' => $this->input->post('description'),
				'added_by' => $this->session->userdata('user_id'),
				'created_at' => date('Y-m-d'),
				'status' => 1
			];

			$insert = $this->designation_model->add_designation($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function edit_designation($id='')
	{
		$this->data['designation'] = $this->db->get_where('designations', array('designation_id' => $id))->row();
		$this->data['all_departments'] = $this->department_model->all_departments();

		$this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'organizations/edit_designation', $this->data);
	}

	public function editDesignation()
     {
     	
     	if($this->input->post()) {
     		$designation_id = $this->input->post('designation_id');
			
			$data = [
				'department_id' => $this->input->post('department_id'),
				'designation_name' => $this->input->post('designation_name'),
				'secondary_name' => $this->input->post('secondary_name'),
				'description' => $this->input->post('description'),
				'status' => 1
			];

			$update = $this->designation_model->editDesignation($data, $designation_id);
			if($update) {
				return true;
			} else {
				return false;
			}
		}
     }

}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Employees extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('employees_model');
        $this->load->admin_model('department_model');
        $this->load->admin_model('designation_model');
        // $this->load->admin_model('timesheet_model');

        $this->load->admin_model('settings_model');
        $this->load->admin_model('auth_model');
        $this->load->library('form_validation');

    }
//<?php echo $this->site->getCaption($row->document_type, $row->secondary_document_type);
    public function index()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('employees'), 'page' => lang('employees')], ['link' => '#', 'page' => lang('employees')]];
        $meta = ['page_title' => lang('employees'), 'bc' => $bc];
		$this->page_construct('employees/employees_list', $meta, $this->data);
     }

	public function employee_pdf()
	{
		$mpdf = new \Mpdf\Mpdf();
		$data['employees'] = $this->db
			->select('employees.user_id as user_id, employees.user_id as emp_id, CONCAT_WS(" ", first_name, last_name) as whole_name, employees.employee_id, warehouses.name as warehouse_name, employees.contact_no, employees.date_of_joining, departments.department_name, designations.designation_name')
            ->from('employees')
            ->join('warehouses', 'warehouses.id = employees.warehouse_id')
            ->join('departments', 'departments.department_id = employees.department_id')
            ->join('designations', 'designations.designation_id = employees.designation_id')
			->where('employees.user_role_id !=', 1)
            ->group_by('employees.user_id')
			->get()
			->result();
        $html = $this->load->view($this->theme.'employees/employee_list_pdf', $data, true);
		$pdfFilePath = 'employees_info'.".pdf";
        $mpdf->WriteHTML($html);
		$mpdf->Output();
        //$mpdf->Output($pdfFilePath, "D"); // opens in browser
        //$mpdf->Output($pdfFilePath,'D') will work as normal download
	} 
	public function employee_excel(){ 
        // // file name 
        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");
       // get data 
	   $usersData = $this->db
			->select('employees.user_id as user_id, employees.user_id as emp_id, CONCAT_WS(" ", first_name, last_name) as whole_name, employees.employee_id, warehouses.name as warehouse_name, employees.contact_no, employees.date_of_joining, departments.department_name, designations.designation_name')
			->from('employees')
			->join('warehouses', 'warehouses.id = employees.warehouse_id')
			->join('departments', 'departments.department_id = employees.department_id')
			->join('designations', 'designations.designation_id = employees.designation_id')
			->where('employees.user_role_id !=', 1)
			->group_by('employees.user_id')
			->get()
			->result_array();
        //$usersData = $this->db->query($sql)->result_array();
        // file creation 
        $file = fopen('php://output','w');
        $header = array("SL","Employee ID","Name","Department", "Designation", "Warehouse", "Contact"); 
        fputcsv($file, $header);
        $lines=array();
        $serial = 1;
        foreach ($usersData as $key=>$line){ 
            $lines['serial']=$serial;
            $lines['employee_id']=$line['employee_id'];
            $lines['whole_name']=$line['whole_name'];
            $lines['department_name']=$line['department_name'];
            $lines['designation_name']=$line['designation_name'];
            $lines['warehouse_name']=$line['warehouse_name'];
            $lines['contact_no']=$line['contact_no'];
// print_r($lines);exit();
			$serial++;
            fputcsv($file,$lines); 
        }
        fclose($file); 
        exit; 
    }
	public function employee_print()
	{
		$mpdf = new \Mpdf\Mpdf();
		$this->data['employees'] = $this->db
			->select('employees.user_id as user_id, employees.user_id as emp_id, CONCAT_WS(" ", first_name, last_name) as whole_name, employees.employee_id, warehouses.name as warehouse_name, employees.contact_no, employees.date_of_joining, departments.department_name, designations.designation_name')
            ->from('employees')
            ->join('warehouses', 'warehouses.id = employees.warehouse_id')
            ->join('departments', 'departments.department_id = employees.department_id')
            ->join('designations', 'designations.designation_id = employees.designation_id')
			->where('employees.user_role_id !=', 1)
            ->group_by('employees.user_id')
			->get()
			->result();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('employees_print')]];
        $meta = ['page_title' => lang('employees'), 'bc' => $bc];
		$this->page_construct('employees/employee_list_print', $meta, $this->data);
	} 
	public function employee_salary_pdf()
	{
		$mpdf = new \Mpdf\Mpdf();
		$employees = $this->db
			->get('employees')
			->result();
		$emp_salary = array();
		foreach($employees as $key=>$row) {
			$emp_salary[$i]['basic_salary'] = $row->basic_salary;

			
		}	
        $html = $this->load->view($this->theme.'employees/employee_salary_pdf', $data, true);
		$pdfFilePath = 'employee_salary'.".pdf";
        $mpdf->WriteHTML($html);
		$mpdf->Output();
        //$mpdf->Output($pdfFilePath, "D"); // opens in browser
        //$mpdf->Output($pdfFilePath,'D') will work as normal download
	} 

    public function employees_list()
    {

		$this->load->library('datatables');
// department, designation
        $this->datatables
            ->select('employees.user_id as user_id, employees.user_id as emp_id, CONCAT_WS(" ", first_name, last_name) as whole_name, employees.employee_id, warehouses.name, employees.contact_no, employees.date_of_joining')
            ->from('employees')
            ->join('warehouses', 'warehouses.id = employees.warehouse_id')
			->where('employees.user_role_id !=', 1)
            ->group_by('employees.user_id')
            ->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/detail/$1') . "' class='tip' title='" . lang('view_employee') . "'><i class=\"fa fa-file-text\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_employee') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_employee/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'user_id')
            
            ;

            // print_r($this->db->error());

        echo $this->datatables->generate();
		
     }

    

	 public function detail($emp_id='') 
	 {
		$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

	 	$user_id = $this->session->userdata('user_id');

		$this->data['emp_info'] = $this->employees_model->read_employee_information($emp_id);
		// echo "<pre>";print_r($result); die;
		$this->data['all_departments'] = $this->department_model->all_departments();
    	$this->data['all_warehouses'] = $this->settings_model->getAllWarehouses();
    	$this->data['all_designations'] = $this->designation_model->all_designations();
    	$this->data['all_groupemp'] = $this->employees_model->getGroupsEmp();
    	$this->data['all_office_shifts'] = $this->employees_model->all_office_shifts();
    	$this->data['all_employees'] = $this->employees_model->all_employees();
    	$this->data['leave_categories'] = $this->employees_model->leave_categories();
    	
		$cat_ids =[];
		if(!empty($this->data['emp_info']->leave_categories)) {
			$leave_categories = explode(',', $this->data['emp_info']->leave_categories);
			
			foreach($leave_categories as $value) {
				$cat_ids[] = $this->db->get_where('leave_type', array('leave_type_id'=>$value))->row()->leave_type_id;
			} 
		}
    	$this->data['cat_ids'] = $cat_ids;
    	$this->data['user_info'] = $this->db->get_where('users', array('emp_id'=>$emp_id))->row();
    	$this->data['all_countries'] = $this->employees_model->get_countries();
    	$this->data['education_levels'] = $this->employees_model->education_level();
    	$this->data['qualification_languages'] = $this->employees_model->qualification_languages();
    	$this->data['qualification_skill'] = $this->employees_model->qualification_skill();
    	$this->data['document_types'] = $this->employees_model->get_document_types();
    	$this->data['contract_types'] = $this->employees_model->get_contract_types();

		// ____Leave Statistics
		$emp_leave_cat = $this->db->select('leave_categories')->get_where('employees', array('user_id' => $emp_id))->row();
		$emp_leaves = explode(',', $emp_leave_cat->leave_categories);
		
		// $leave_statistics = array();
		// foreach($emp_leaves as $leave_type_id) {
		// 	$total_leave_taken = $this->timesheet_model->count_total_leaves($leave_type_id,$emp_id);
		// 	$leave_type = $this->db->get_where('leave_type', array('leave_type_id' => $leave_type_id))->row();
		// 	$remaining_leave_total = $leave_type->days_per_year - $total_leave_taken;

		// 	$leave_statistics[] = [
		// 		'leave_name' => $leave_type->type_name,
		// 		'total_leave_days' => $leave_type->days_per_year,
		// 		'total_leave_taken' => $total_leave_taken,

		// 	];
		// }
		// $this->data['leave_statistics'] = $leave_statistics;

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('employees'), 'page' => lang('employees')], ['link' => '#', 'page' => lang('employees')]];
        $meta = ['page_title' => lang('employees'), 'bc' => $bc];
		$this->page_construct('employees/employee_detail', $meta, $this->data);

	 } 

	 public function getDesignations()
	 {
	 	// print_r($this->input->post()); die;
	 	$dept_id = $this->input->post('dept');
	 	$designations = $this->db->get_where('designations', array('department_id' => $dept_id))->result();
	 	// print_r($designations); die;
	 	$output = '';
	 	$output .= '<select name="designation_id" id="designation" class="form-control select" required="required">
              			<option value="">Select</option>';
              			 foreach($designations as $row) { 
              				$output .= '<option value="'.$row->designation_id.'">'.$row->designation_name.'</option>';
              			 } 
         $output .= '</select>';
         echo $output;
	 }

	 public function add_profile_picture()
	 {

	 	$emp_id = $this->input->post('emp_id');
	 	if(isset($_FILES["profile_pic"]["name"])) {

	 		$emp = $this->db->select('profile_picture')->get_where('employees', array('user_id'=>$emp_id))->row();
	 		if(!empty($emp->profile_picture)) {
	 			unlink('assets/uploads/profile/'.$emp->profile_picture);
	 		}

                $config['upload_path']          = 'assets/uploads/profile/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|';
                $config['max_size']             = 2000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('profile_pic')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
            }
       $update = $this->db->where('user_id', $emp_id)->set('profile_picture', $file_name)->update('employees');
       $emp = $this->db->select('profile_picture')->get_where('employees', array('user_id'=>$emp_id))->row();
       $output = '<img id="profile_picture" src="'. base_url('assets/uploads/profile/'.$emp->profile_picture).'" style="width: 80px">';
       if($update) {
       		echo $output;
       }
	 }

     public function delete_employee($user_id = null)
     {
        if ($this->employees_model->delete_employee($user_id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('employee_deleted')]);
        }
     }

     public function getWorkingExperience($emp_id='')
     {
     	// print_r($emp_id);die;
     	$this->load->library('datatables');
// department, designation
        $this->datatables
			->select('work_experience_id as id, company_name, from_date, to_date, post')
			->from('employee_work_experience')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_work_experience/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_work_experience') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_work_experience') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_work_experience/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function delete_work_experience($work_experience_id='')
     {
     	if($this->employees_model->delete_work_experience($work_experience_id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_deleted')]);
     	}
     }


     	// Validate and add info in database
	public function add_employee() {

		$this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('department_id', lang('department_id'), 'required');
		$this->form_validation->set_rules('designation_id', lang('designation_id'), 'required');

		if ($this->form_validation->run() == true) {

			$first_name = $this->settings_model->clean_post($this->input->post('first_name'));
			$last_name = $this->settings_model->clean_post($this->input->post('last_name'));
			$employee_id = $this->settings_model->clean_post($this->input->post('employee_id'));
			$date_of_joining = ($this->input->post('date_of_joining'));
			$date_of_joining = date('Y-m-d', strtotime($date_of_joining));
			$username = $this->settings_model->clean_post($this->input->post('username'));
			$date_of_birth = ($this->input->post('date_of_birth'));
			$date_of_birth = date('Y-m-d', strtotime($date_of_birth));
			$contact_no = $this->settings_model->clean_post($this->input->post('contact_no'));
			$address = $this->settings_model->clean_post($this->input->post('address'));
			
			$options = array('cost' => 12);
			$password_hash = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
			
			$leave_categories = array($this->input->post('leave_categories'));
			if(!empty($this->input->post('leave_categories'))) {
				$cat_ids = implode(',',$this->input->post('leave_categories'));
			}
			// print_r($cat_ids);die;

            $data = array(
			'employee_id' => $employee_id,
			// 'office_shift_id' => $this->input->post('office_shift_id'),
			'reports_to' => $this->input->post('reports_to'),
			'first_name' => $first_name,
			'last_name' => $last_name,
			'username' => $username,
			'company_id' => $this->input->post('company_id'),
			'warehouse_id' => $this->input->post('warehouse_id'),
			'email' => $this->input->post('email'),
			'password' => $password_hash,
			'pincode' => $this->input->post('pin_code'),
			'date_of_birth' => $date_of_birth,
			'gender' => $this->input->post('gender'),
			'user_role_id' => $this->input->post('role'),
			'department_id' => $this->input->post('department_id'),
			'sub_department_id' => $this->input->post('subdepartment_id'),
			'designation_id' => $this->input->post('designation_id'),
			'date_of_joining' => $date_of_joining,
			'contact_no' => $contact_no,
			'address' => $address,
			'is_active' => 1,
			// 'leave_categories' => $cat_ids,
			'created_at' => date('Y-m-d h:i:s')
		);

        // echo "<pre>";    print_r($data);die();
            
        } elseif ($this->input->post('add_employee')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('employees/add_employee');
        }

		if ($this->form_validation->run() == true && $this->employees_model->addEmployee($data)) {

			$insert_id = $this->db->insert_id();
			$additional_data = [
				'emp_id'	=> $insert_id,
				'first_name' => $data['first_name'],
				'last_name'	=> $data['last_name'],
				'phone'		=> $data['contact_no'],
				'gender'	=> $data['gender'],
				'warehouse_id' => $data['warehouse_id'],
				'group_id'	=> $data['user_role_id']

			];
			$active = 1;

			$this->load->library('ion_auth');
			$password = $this->input->post('password');
			if($this->ion_auth->register($data['username'], $password, $data['email'], $additional_data, $active)) {
				$this->session->set_flashdata('message', lang('employee_added'));
            	admin_redirect('employees');
			} else {
	            $this->session->set_flashdata('message', lang('employee_added'));
	            admin_redirect('employees/add_employee');
        	}
        }
        else {
        	$this->data['all_departments'] = $this->department_model->all_departments();
        	$this->data['all_warehouses'] = $this->settings_model->getAllWarehouses();
        	$this->data['all_designations'] = $this->designation_model->all_designations();
        	$this->data['all_groupemp'] = $this->employees_model->getGroupsEmp();
        	$this->data['all_office_shifts'] = $this->employees_model->all_office_shifts();
        	$this->data['all_employees'] = $this->employees_model->all_employees();
        	$this->data['leave_categories'] = $this->employees_model->leave_categories();

        	$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('employees'), 'page' => lang('employees')], ['link' => '#', 'page' => lang('employees')]];
        	$meta = ['page_title' => lang('employees'), 'bc' => $bc];
	    	$this->page_construct('employees/add_employee', $meta, $this->data);
	    }

	}

	public function add_basic_info()
	{

		// echo "<pre>"; print_r($this->input->post());die;
		$this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('department_id', lang('department_id'), 'required');
		$this->form_validation->set_rules('designation_id', lang('designation_id'), 'required');
		//$leave_cats_id = 
		//$data = [];
		if ($this->form_validation->run() == true) {
			$emp_id = $this->input->post('emp_id');

			$first_name = $this->settings_model->clean_post($this->input->post('first_name'));
			$last_name = $this->settings_model->clean_post($this->input->post('last_name'));
			$employee_id = $this->settings_model->clean_post($this->input->post('employee_id'));
			$date_of_joining = ($this->input->post('date_of_joining'));
			$date_of_joining = date('Y-m-d', strtotime($date_of_joining));
			$username = $this->settings_model->clean_post($this->input->post('username'));
			$date_of_birth = ($this->input->post('date_of_birth'));
			$date_of_birth = date('Y-m-d', strtotime($date_of_birth));

			$date_of_leaving = $this->input->post('date_of_leaving');
			$date_of_leaving = date('Y-m-d', strtotime($date_of_leaving));
			$contact_no = $this->settings_model->clean_post($this->input->post('contact_no'));
			$address = $this->settings_model->clean_post($this->input->post('address'));
			
			$options = array('cost' => 12);
			$password_hash = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
			
			$leave_categories = array($this->input->post('leave_categories'));
			$cat_ids = implode(',',$this->input->post('leave_categories'));
			// print_r($cat_ids);die;

            $data = array(
			'employee_id' => $employee_id,
			'office_shift_id' => $this->input->post('office_shift_id'),
			'reports_to' => $this->input->post('reports_to'),
			'first_name' => $first_name,
			'last_name' => $last_name,
			'username' => $username,
			'company_id' => $this->input->post('company_id'),
			'warehouse_id' => $this->input->post('warehouse_id'),
			'email' => $this->input->post('email'),
			'password' => $password_hash,
			'pincode' => $this->input->post('pin_code'),
			'date_of_birth' => $date_of_birth,
			'gender' => $this->input->post('gender'),
			'user_role_id' => $this->input->post('role'),
			'department_id' => $this->input->post('department_id'),
			'sub_department_id' => $this->input->post('subdepartment_id'),
			'designation_id' => $this->input->post('designation_id'),
			'date_of_joining' => $date_of_joining,
			'contact_no' => $contact_no,
			'address' => $address,
			'is_active' => 1,
			'leave_categories' => $cat_ids,

			'address' => $this->input->post('address'),
			'state' => $this->input->post('state'),
			'city' => $this->input->post('city'),
			'zipcode' => $this->input->post('zipcode'),
			'date_of_leaving' => $date_of_leaving,
			'marital_status' => $this->input->post('marital_status'),
			'blood_group' => $this->input->post('blood_group'),
			'citizenship_id' => $this->input->post('citizenship_id'),
			'nationality_id' => $this->input->post('nationality_id'),
			'is_active' => $this->input->post('status'),

			'created_at' => date('Y-m-d h:i:s')
		);


        } elseif ($this->input->post('add_employee')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('employees');
        }

		if ($this->form_validation->run() == true && $this->employees_model->updateEmployee($data, $emp_id)) {

			//$insert_id = $this->db->insert_id();
			$additional_data = [
				//'emp_id'	=> $insert_id,
				'username'	=> $data['username'],
				'email'	=> $data['email'],
				'first_name' => $data['first_name'],
				'last_name'	=> $data['last_name'],
				'phone'		=> $data['contact_no'],
				'gender'	=> $data['gender'],
				'warehouse_id' => $data['warehouse_id'],
				'group_id'	=> $data['user_role_id']

			];

			if($this->employees_model->update_user($additional_data, $emp_id)) {
				return true;
			} else {
				return false;
        	}
        }
        else {
        	//$this->data['emp_info'] = $this->employees_model->read_employee_information($emp_id);

        	$this->data['all_departments'] = $this->department_model->all_departments();
        	$this->data['all_warehouses'] = $this->settings_model->getAllWarehouses();
        	$this->data['all_designations'] = $this->designation_model->all_designations();
        	$this->data['all_groupemp'] = $this->employees_model->getGroupsEmp();
        	$this->data['all_office_shifts'] = $this->employees_model->all_office_shifts();
        	$this->data['all_employees'] = $this->employees_model->all_employees();
        	$this->data['leave_categories'] = $this->employees_model->leave_categories();

        	$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('employees'), 'page' => lang('employees')], ['link' => '#', 'page' => lang('employees')]];
        	$meta = ['page_title' => lang('employees'), 'bc' => $bc];
	    	
		}
	}

	public function add_working_experience()
	{
		if($this->input->post()) {
			
			$data = [
				'employee_id' => $this->input->post('emp_id'),
				'company_name' => $this->input->post('company_name'),
				'from_date' => $this->input->post('from_date'),
				'to_date' => $this->input->post('to_date'),				
				'post' => $this->input->post('post'),
				'description' => $this->input->post('description'),
				'created_at' => date('Y-m-d')
			];

			$insert = $this->employees_model->add_working_experience($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function editWorkExperience()
	{
		if($this->input->post()) {

			$work_experience_id = $this->input->post('work_experience_id');
			
			$data = [
				'company_name' => $this->input->post('company_name'),
				'from_date' => $this->input->post('from_date'),
				'to_date' => $this->input->post('to_date'),
				'post' => $this->input->post('post'),
				'description' => $this->input->post('description'),
			];
			$update = $this->employees_model->update_working_experience($data, $work_experience_id);
			if($update) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function edit_work_experience($work_experience_id='')
	{
		$this->data['work_experience'] = $this->db->get_where('employee_work_experience', array('work_experience_id' => $work_experience_id))->row();

		$this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'employees/edit_work_experience', $this->data);

	}

	public function edit_qualification($qualification_id='')
	{
		$this->data['qualification'] = $this->db->get_where('employee_qualification', array('qualification_id' => $qualification_id))->row();
		$this->data['education_levels'] = $this->employees_model->education_level();
    	$this->data['qualification_languages'] = $this->employees_model->qualification_languages();
    	$this->data['qualification_skill'] = $this->employees_model->qualification_skill();

		$this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'employees/edit_qualification', $this->data);

	}

	public function getEmployeeQualification($emp_id='')
	{
		$this->load->library('datatables');
// department, designation
        $this->datatables
			->select('employee_qualification.qualification_id as id, employee_qualification.name, CONCAT_WS(" to ", from_year, to_year), education_level.name as level_name')
			->from('employee_qualification')
			->join('education_level', 'education_level.education_level_id = employee_qualification.education_level_id')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_qualification/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_qualification') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_qualification') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_qualification/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
	}

	public function delete_qualification($qualification_id='')
     {
     	if($this->employees_model->delete_qualification($qualification_id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_qualification_deleted')]);
     	}
     }

	public function add_qualification()
	{
		if($this->input->post()) {
			
			$data = [
				'employee_id' => $this->input->post('emp_id'),
				'name' => $this->input->post('school'),
				'education_level_id' => $this->input->post('education_level_id'),
				'from_year' => $this->input->post('from_year'),
				'to_year' => $this->input->post('to_year'),
				'language_id' => $this->input->post('language_id'),
				'language_id' => $this->input->post('language_id'),
				'skill_id' => $this->input->post('skill_id'),
				'description' => $this->input->post('description'),
				'created_at' => date('Y-m-d')
			];
// print_r($data); print_r($emp_id); die;
			$insert = $this->employees_model->add_qualification($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function getEmergencyContact($emp_id='')
     {
     	// print_r($emp_id);die;
     	$this->load->library('datatables');
// department, designation
        $this->datatables
			->select('contact_id as id, contact_name, relation, work_email, mobile_phone')
			->from('employee_contacts')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_emergency_contact/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_emergency_contact') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_emergency_contact') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_emergency_contact/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

    public function add_emergency_contact()
	{
		if($this->input->post()) {
			$is_primary = $this->input->post('is_primary');
			$is_dependent = $this->input->post('is_dependent');
			$is_primary = isset($is_primary) ? 1 : 0;
			$is_dependent = isset($is_dependent) ? 1 : 0;
			
			$data = [
				'employee_id' => $this->input->post('emp_id'),
				'relation' => $this->input->post('relation'),
				'is_primary' =>  $is_primary,
				'is_dependent' =>  $is_dependent,
				'contact_name' => $this->input->post('contact_name'),
				'work_phone' => $this->input->post('work_phone'),
				'work_phone_extension' => $this->input->post('work_phone_extension'),
				'home_phone' => $this->input->post('home_phone'),
				'work_email' => $this->input->post('work_email'),
				'mobile_phone' => $this->input->post('mobile_phone'),
				'personal_email' => $this->input->post('personal_email'),
				'address_1' => $this->input->post('address_1'),
				'address_2' => $this->input->post('address_2'),
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'zipcode' => $this->input->post('zipcode'),
				'country' => $this->input->post('country'),
				'created_at' => date('Y-m-d')
			];
// print_r($data); print_r($emp_id); die;
			$insert = $this->employees_model->add_emergency_contact($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function edit_emergency_contact($contact_id='')
	{

		$this->data['contact_info'] = $this->employees_model->get_emergency_contact($contact_id);
		$this->data['all_countries'] = $this->employees_model->get_countries();

		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_emergency_contact', $this->data);
	}

	public function editEmergencyContact()
	{
		if($this->input->post()) {
			$contact_id = $this->input->post('contact_id');
			$is_primary = $this->input->post('is_primary');
			$is_dependent = $this->input->post('is_dependent');
			$is_primary = isset($is_primary) ? 1 : 0;
			$is_dependent = isset($is_dependent) ? 1 : 0;
			
			$data = [
				//'employee_id' => $this->input->post('emp_id'),
				'relation' => $this->input->post('relation'),
				'is_primary' =>  $is_primary,
				'is_dependent' =>  $is_dependent,
				'contact_name' => $this->input->post('contact_name'),
				'work_phone' => $this->input->post('work_phone'),
				'work_phone_extension' => $this->input->post('work_phone_extension'),
				'home_phone' => $this->input->post('home_phone'),
				'work_email' => $this->input->post('work_email'),
				'mobile_phone' => $this->input->post('mobile_phone'),
				'personal_email' => $this->input->post('personal_email'),
				'address_1' => $this->input->post('address_1'),
				'address_2' => $this->input->post('address_2'),
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'zipcode' => $this->input->post('zipcode'),
				'country' => $this->input->post('country'),
				//'created_at' => date('Y-m-d')
			];
// print_r($data); print_r($emp_id); die;
			$insert = $this->employees_model->update_emergency_contact($data, $contact_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function getEmpBankAccount($emp_id='')
     {
     	// print_r($emp_id);die;
     	$this->load->library('datatables');
// department, designation
        $this->datatables
			->select('bankaccount_id as id, account_title, account_number, bank_name, bank_code, bank_branch')
			->from('employee_bankaccount')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_emp_bank_account/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_emp_bank_account') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_emp_bank_account') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_emp_bank_account/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function add_emp_bank_account()
     {
     	if($this->input->post()) {
			
			$data = [
				'employee_id' => $this->input->post('emp_id'),
				'account_title' => $this->input->post('account_title'),
				'account_number' => $this->input->post('account_number'),
				'bank_name' => $this->input->post('bank_name'),
				'bank_code' => $this->input->post('bank_code'),				
				'bank_branch' => $this->input->post('bank_branch'),
				'created_at' => date('m-d-Y')
			];

			$insert = $this->employees_model->add_emp_bank_account($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
     }

     public function edit_emp_bank_account($id='')
     {
     	$this->data['bank_account_info'] = $this->employees_model->get_emp_bank_account($id);

		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_bank_account', $this->data);
     }

     public function EditEmpBankaccount()
     {
     	if($this->input->post()) {
     		$bankaccount_id = $this->input->post('bankaccount_id');
			
			$data = [
				'account_title' => $this->input->post('account_title'),
				'account_number' => $this->input->post('account_number'),
				'bank_name' => $this->input->post('bank_name'),
				'bank_code' => $this->input->post('bank_code'),				
				'bank_branch' => $this->input->post('bank_branch'),
			];

			$update = $this->employees_model->edit_emp_bank_account($data, $bankaccount_id);
			if($update) {
				return true;
			} else {
				return false;
			}
		}
     }

     public function delete_emp_bank_account($id='')
     {
     	if($this->employees_model->delete_emp_bank_account($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_bank_account_deleted')]);
     	}
     }

     public function getEmployeeDocument($emp_id='')
     {
     	$this->load->library('datatables');

        $this->datatables
			->select('employee_documents.document_id as id, document_type.document_type, employee_documents.title, employee_documents.date_of_expiry')
			->from('employee_documents')
			->join('document_type', 'document_type.document_type_id = employee_documents.document_type_id', 'left')
			->where('employee_id', $emp_id)
			->group_by('employee_documents.document_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_employee_document/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_employee_document') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_employee_document') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_employee_document/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')

			;
     	echo $this->datatables->generate();
     }

	 public function getDocumentType($emp_id='')
     {
     	$this->load->library('datatables');

        $this->datatables
			->select('document_type.document_type_id as id, document_type.document_type, document_type.secondary_document_type')
			->from('document_type')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_document_type/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_document_type') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_document_type') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_document_type/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')

			;
     	echo $this->datatables->generate();
     }
	 public function delete_document_type($id)
     {
     	if($this->employees_model->delete_document_type($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('document_type_deleted')]);
     	}
     }

	 public function edit_document_type($id='')
     {
     	$this->data['document_type'] = $this->employees_model->get_document_type($id);
     	$this->data['document_types'] = $this->employees_model->get_document_types();

		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_document_type', $this->data);
     }

     public function edit_employee_document($id='')
     {
     	$this->data['employee_document'] = $this->employees_model->get_employee_document($id);
     	$this->data['document_types'] = $this->employees_model->get_document_types();

		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_employee_document', $this->data);
     }

     public function delete_employee_document($id)
     {
     	if($this->employees_model->delete_employee_document($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_document_deleted')]);
     	}
     }

     public function add_employee_document()
	{

		if($this->input->post()) {

			if(isset($_FILES["document_file"]["name"])) {
                $config['upload_path']          = 'assets/uploads/documents/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|txt|doc|docx|xls|xlsx';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('document_file')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
            }
			
			$data = [
				'employee_id' => $this->input->post('emp_id'),
				'document_type_id' => $this->input->post('document_type_id'),
				'title' => $this->input->post('title'),
				'date_of_expiry' => $this->input->post('date_of_expiry'),				
				'document_file' => $file_name,			
				
				'created_at' => date('d-m-Y')
			];

			$insert = $this->employees_model->add_employee_document($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}
	public function add_document_type()
	{
		if($this->input->post()) {
			$data = [
				'document_type' => $this->input->post('document_type'),
				'secondary_document_type' => $this->input->post('secondary_document_type'),				
				'created_at' => date('d-m-Y')
			];

			$insert = $this->employees_model->add_document_type($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}
	public function editDocumentType()
	{
		$document_type_id = $this->input->post('document_type_id');

		if($this->input->post()) {
			$data = [
				'document_type' => $this->input->post('document_type'),
				'secondary_document_type' => $this->input->post('secondary_document_type'),				
			];

			$insert = $this->employees_model->edit_document_type($data, $document_type_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public function getEmployeeImmigrantion($emp_id='')
     {
     	$this->load->library('datatables');

        $this->datatables
			->select('employee_immigration.immigration_id as id, document_type.document_type, employee_immigration.issue_date, employee_immigration.expiry_date, countries.country_name, employee_immigration.eligible_review_date')
			->from('employee_immigration')
			->join('document_type', 'document_type.document_type_id = employee_immigration.document_type_id', 'left')
			->join('countries', 'countries.country_id = employee_immigration.country_id')
			->where('employee_id', $emp_id)
			->group_by('employee_immigration.immigration_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_employee_immigration/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_employee_immigration') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_employee_immigrationt') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_employee_immigration/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')

			;
     	echo $this->datatables->generate();
     }

     public function delete_employee_immigration($id='')
     {
     	if($this->employees_model->delete_employee_immigration($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_immigration_deleted')]);
     	}
     }

     public function editEmployeeDocument()
     {
     	// print_r($this->input->post()); print_r($_FILES); die;
		if($this->input->post()) {
			$document_id = $this->input->post('document_id');

			if(isset($_FILES["document_file"]["name"])) {
	 			$document = $this->db->select('document_file')->get_where('employee_documents', array('document_id'=>$document_id))->row();
		 		if(!empty($document->document_file)) {
		 			unlink('assets/uploads/documents/'.$document->document_file);
		 		}

                $config['upload_path']          = 'assets/uploads/documents/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|txt|doc|docx|xls|xlsx';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('document_file')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
            }
			
			$data = [
				//'employee_id' => $this->input->post('emp_id'),
				'document_type_id' => $this->input->post('document_type_id'),
				'title' => $this->input->post('title'),
				'date_of_expiry' => $this->input->post('date_of_expiry'),				
				'document_file' => $file_name,			
				
				//'created_at' => date('d-m-Y')
			];

			$insert = $this->employees_model->edit_employee_document($data, $document_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
     }


     public function add_employee_immigration()
     {
     	if($this->input->post()) {
     		$immigration_id = $this->input->post('immigration_id');

			if(isset($_FILES["document_file"]["name"])) {
				$document = $this->db->select('document_file')->get_where('employee_immigration', array('immigration_id'=>$immigration_id))->row();
		 		if(!empty($document->document_file)) {
		 			unlink('assets/uploads/documents/'.$document->document_file);
		 		}
                $config['upload_path']          = 'assets/uploads/documents/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|txt|doc|docx|xls|xlsx';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('document_file')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name']; 
                }
            }
			
			$data = [
				'employee_id' => $this->input->post('emp_id'),
				'document_type_id' => $this->input->post('document_type_id'),
				'document_number' => $this->input->post('document_number'),
				'issue_date' => $this->input->post('issue_date'),				
				'expiry_date' => $this->input->post('expiry_date'),				
				'eligible_review_date' => $this->input->post('eligible_review_date'),				
				'country_id' => $this->input->post('country'),				
				'document_file' => $file_name,			
				
				'created_at' => date('d-m-Y')
			];

			$insert = $this->employees_model->add_employee_immigration($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
     }

     public function edit_employee_immigration($id='')
     {
     	$this->data['employee_immigration'] = $this->employees_model->get_employee_immigration($id);
     	$this->data['document_types'] = $this->employees_model->get_document_types();
     	$this->data['all_countries'] = $this->employees_model->get_countries();

		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_employee_immigration', $this->data);
     }

     public function editEmployeeImmigration()
     {
     	if($this->input->post()) {
     		$immigration_id = $this->input->post('immigration_id');

			if(isset($_FILES["document_file"]["name"])) {
				$document = $this->db->select('document_file')->get_where('employee_immigration', array('immigration_id'=>$immigration_id))->row();
		 		if(!empty($document->document_file)) {
		 			unlink('assets/uploads/documents/'.$document->document_file);
		 		}
                $config['upload_path']          = 'assets/uploads/documents/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|txt|doc|docx|xls|xlsx';
                $config['max_size']             = 5000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('document_file')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name']; 
                }
            }
			
			$data = [
				'document_type_id' => $this->input->post('document_type_id'),
				'document_number' => $this->input->post('document_number'),
				'issue_date' => $this->input->post('issue_date'),				
				'expiry_date' => $this->input->post('expiry_date'),				
				'eligible_review_date' => $this->input->post('eligible_review_date'),				
				'country_id' => $this->input->post('country'),				
				'document_file' => $file_name,			

			];

			$insert = $this->employees_model->edit_employee_immigration($data, $immigration_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
     }

     public function add_social_network() 
     {

     	if($this->input->post()) {

     		$emp_id = $this->input->post('emp_id');
	     	$data = array(
			'facebook_link' => $this->input->post('facebook_profile'),
			'twitter_link' => $this->input->post('twitter_profile'),
			'blogger_link' => $this->input->post('blogger_profile'),
			'linkdedin_link' => $this->input->post('linkedin_profile'),
			'google_plus_link' => $this->input->post('google_plus_profile'),
			'instagram_link' => $this->input->post('instagram_profile'),
			'pinterest_link' => $this->input->post('pinterest_profile'),
			'youtube_link' => $this->input->post('youtube_profile')
			);

			$update = $this->employees_model->update_social_info($data, $emp_id);
			if($update) {
				return true;
			} else {
				return false;
			}
     	} 

 	}

 	public function update_employee_password()
 	{
 		// a559f47fe29bed655c572fdee5e140510cd1e9a4
 		if($this->input->post()) {

 			$emp_id = $this->input->post('emp_id');
 			$old_password = $this->input->post('old_password');
 			$new_password = $this->input->post('new_password');
 			$new_password_confirm = $this->input->post('new_password_confirm');

 			if($new_password !== $new_password_confirm) {
 				$this->sma->send_json(['error' => 1, 'msg' => lang('password_not_match')]);
 			}

 			//$user_id = $this->session->userdata('user_id');

 			$emp_identity = $this->db->select('username, email')->get_where('users', array('emp_id' => $emp_id))->row();
 			if(!empty($emp_identity->email)) {
 				$identity = $emp_identity->email;
 			} 
 			else {
 				$identity = $emp_identity->username;
 			}
 			//$this->session->userdata('');
 			$update_password = $this->auth_model->change_password($identity, $old_password, $new_password);
 			if($update_password) {
 				$this->sma->send_json(['error' => 0, 'msg' => lang('password_updated')]);
 			} else {
 				$this->sma->send_json(['error' => 1, 'msg' => lang('password_not_updated')]);
 			}

 		}	

 	}

 	public function getEmployeeContract($emp_id='')
     {
     	$this->load->library('datatables');

        $this->datatables
			->select('employee_contract.contract_id as id, CONCAT_WS(" to ", from_date, to_date), designations.designation_name, contract_type.name, employee_contract.title')
			->from('employee_contract')
			->join('contract_type', 'contract_type.contract_type_id = employee_contract.contract_type_id')
			->join('designations', 'designations.designation_id = employee_contract.designation_id')
			->where('employee_id', $emp_id)
			->group_by('employee_contract.contract_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_emp_contract/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_emp_contract') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_emp_contract') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_emp_contract/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')

			;
     	echo $this->datatables->generate();
     }

     public function delete_emp_contract($id='')
     {
     	if($this->employees_model->delete_emp_contract($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_contract_deleted')]);
     	}
     }

     public function add_emp_contract()
     {
     	if($this->input->post()) {

     		
	     	$data = array(
				'employee_id' => $this->input->post('emp_id'),
				'contract_type_id' => $this->input->post('contract_type_id'),
				'title' => $this->input->post('title'),
				'from_date' => $this->input->post('from_date'),
				'to_date' => $this->input->post('to_date'),
				'designation_id' => $this->input->post('designation_id'),
				'description' => $this->input->post('description'),
				'created_at' => date('d-m-Y')
			);

			$insert = $this->employees_model->add_emp_contract($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function edit_emp_contract($contract_id)
     {
     	$this->data['emp_contract'] = $this->employees_model->get_emp_contract($contract_id);
     	$this->data['all_designations'] = $this->designation_model->all_designations();
     	$this->data['contract_types'] = $this->employees_model->get_contract_types();

		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_emp_contract', $this->data);
     }

     public function editEmpContract()
     {
     	if($this->input->post()) {

     		$contract_id = $this->input->post('contract_id');
	     	$data = array(
				'contract_type_id' => $this->input->post('contract_type_id'),
				'title' => $this->input->post('title'),
				'from_date' => $this->input->post('from_date'),
				'to_date' => $this->input->post('to_date'),
				'designation_id' => $this->input->post('designation_id'),
				'description' => $this->input->post('description'),
			);

			$update = $this->employees_model->update_emp_contract($data, $contract_id);
			if($update) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function update_employee_salary()
     {
     	// print_r($this->input->post()); die;
     	if($this->input->post()) {
     		$emp_id = $this->input->post('emp_id');
     		
	     	$data = array(
				'wages_type' => $this->input->post('wages_type'),
				'basic_salary' => $this->input->post('basic_salary'),
			);

			$insert = $this->employees_model->update_employee_salary($data, $emp_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function getAllowances($emp_id='')
     {
     	// print_r($emp_id);die;
     	$this->load->library('datatables');
// department, designation
        $this->datatables
			->select('allowance_id as id, allowance_title, allowance_amount, is_allowance_taxable, amount_option')
			->from('salary_allowances')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_emp_allowance/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_emp_allowance') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_emp_allowance') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_emp_allowance/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function delete_emp_allowance($id='')
     {
     	if($this->employees_model->delete_emp_allowance($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_allowance_deleted')]);
     	}
     }

     public function add_employee_allowances()
     {
     	// print_r($this->input->post()); die;
     	if($this->input->post()) {
     		
	     	$data = array(
				'employee_id' => $this->input->post('emp_id'),
				'is_allowance_taxable' => $this->input->post('is_allowance_taxable'),
				'amount_option' => $this->input->post('amount_option'),
				'allowance_title' => $this->input->post('allowance_title'),
				'allowance_amount' => $this->input->post('allowance_amount'),
				'allowance_type' => $this->input->post('allowance_type'),
				'created_at' => date('d-m-Y')
			);

			$insert = $this->employees_model->add_employee_allowances($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function edit_emp_allowance($id='')
     {
     	$this->data['emp_allowance'] = $this->employees_model->get_emp_allowance($id);
     	
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_emp_allowance', $this->data);
     }

     public function editEmpAllowance($id='')
     {
     	if($this->input->post()) {
     		$allowance_id = $this->input->post('allowance_id');
     		
	     	$data = array(
				
				'is_allowance_taxable' => $this->input->post('is_allowance_taxable'),
				'amount_option' => $this->input->post('amount_option'),
				'allowance_title' => $this->input->post('allowance_title'),
				'allowance_amount' => $this->input->post('allowance_amount'),
				'created_at' => date('d-m-Y')
			);

			$insert = $this->employees_model->editEmpAllowance($data, $allowance_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function getCommissions($emp_id='')
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('salary_commissions_id as id, commission_title, commission_amount, is_commission_taxable, amount_option')
			->from('salary_commissions')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_emp_commisions/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_emp_commisions') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_emp_commisions') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_emp_commisions/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function delete_emp_commisions($id='')
     {
     	if($this->employees_model->delete_emp_commisions($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_commissions_deleted')]);
     	}
     }

     public function add_employee_commisions()
     {
     	// print_r($this->input->post()); die;
     	if($this->input->post()) {
     		
	     	$data = array(
				'employee_id' => $this->input->post('emp_id'),
				'is_commission_taxable' => $this->input->post('is_commission_taxable'),
				'amount_option' => $this->input->post('amount_option'),
				'commission_title' => $this->input->post('commission_title'),
				'commission_amount' => $this->input->post('commission_amount'),
				'created_at' => date('d-m-Y')
			);

			$insert = $this->employees_model->add_employee_commisions($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function edit_emp_commisions($id='')
     {
     	$this->data['emp_commissions'] = $this->employees_model->get_emp_commisions($id);
     	
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_emp_commisions', $this->data);
     }
     
     public function editEmpCommission($id='')
     {
     	if($this->input->post()) {
     		$salary_commissions_id = $this->input->post('salary_commissions_id');
     		
	     	$data = array(
				
				'is_commission_taxable' => $this->input->post('is_commission_taxable'),
				'amount_option' => $this->input->post('amount_option'),
				'commission_title' => $this->input->post('commission_title'),
				'commission_amount' => $this->input->post('commission_amount'),
				'created_at' => date('d-m-Y')
			);

			$insert = $this->employees_model->editEmpCommission($data, $salary_commissions_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function getStatutoryDeduction($emp_id='')
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('statutory_deductions_id as id, deduction_title, deduction_amount, statutory_options')
			->from('salary_statutory_deductions')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_statutory_deductions/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_statutory_deductions') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_statutory_deductions') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_statutory_deductions/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function delete_statutory_deductions($id='')
     {
     	if($this->employees_model->delete_statutory_deductions($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_statutory_deductions_deleted')]);
     	}
     }

     public function add_statutory_deductions()
     {
     	// print_r($this->input->post()); die;
     	if($this->input->post()) {
     		
	     	$data = array(
				'employee_id' => $this->input->post('emp_id'),
				
				'statutory_options' => $this->input->post('statutory_options'),
				'deduction_title' => $this->input->post('deduction_title'),
				'deduction_amount' => $this->input->post('deduction_amount'),
				'created_at' => date('d-m-Y')
			);

			$insert = $this->employees_model->add_employee_statutory_deductions($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }
     
     public function edit_statutory_deductions($id='')
     {
     	$this->data['statutory_deductions'] = $this->employees_model->get_statutory_deductions($id);
     	
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_statutory_deductions', $this->data);
     }

     
     public function editStatutoryDeductions($id='')
     {
     	if($this->input->post()) {
     		$statutory_deductions_id = $this->input->post('statutory_deductions_id');
     		
	     	$data = array(
				'statutory_options' => $this->input->post('statutory_options'),
				'deduction_title' => $this->input->post('deduction_title'),
				'deduction_amount' => $this->input->post('deduction_amount'),
			);

			$insert = $this->employees_model->editStatutoryDeductions($data, $statutory_deductions_id);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }
     
     public function getOtherPayment($emp_id='')
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('other_payments_id as id, payments_title, payments_amount, is_otherpayment_taxable, amount_option')
			->from('salary_other_payments')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_other_payment/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_other_payment') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_other_payment') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_other_payment/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function delete_other_payment($id='')
     {
     	if($this->employees_model->delete_other_payment($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('other_payment_deleted')]);
     	}
     }

     public function add_other_payment()
     {
     	// print_r($this->input->post()); die;
     	if($this->input->post()) {
     		
	     	$data = array(
				'employee_id' => $this->input->post('emp_id'),
				'is_otherpayment_taxable' => $this->input->post('is_otherpayment_taxable'),
				'amount_option' => $this->input->post('amount_option'),
				'payments_title' => $this->input->post('payments_title'),
				'payments_amount' => $this->input->post('payments_amount'),
				'created_at' => date('d-m-Y')
			);

			$insert = $this->employees_model->add_other_payment($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function edit_other_payment($id='')
     {
     	$this->data['other_payment'] = $this->employees_model->get_other_payment($id);
     	
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_other_payment', $this->data);
     }

     public function editOtherPayment($id='')
     {
     	if($this->input->post()) {
     		$other_payments_id = $this->input->post('other_payments_id');
     		
	     	$data = array(
				'is_otherpayment_taxable' => $this->input->post('is_otherpayment_taxable'),
				'amount_option' => $this->input->post('amount_option'),
				'payments_title' => $this->input->post('payments_title'),
				'payments_amount' => $this->input->post('payments_amount'),
			);

			$update = $this->employees_model->editOtherPayment($data, $other_payments_id);
			if($update) {
				return true;
			} else {
				return false;
			}
     	} 
     }
     
     public function getSalaryOvertime($emp_id='')
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('salary_overtime_id as id, overtime_type, no_of_days, overtime_hours, overtime_rate')
			->from('salary_overtime')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_salary_overtime/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_salary_overtime') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_salary_overtime') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_salary_overtime/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function delete_salary_overtime($id='')
     {
     	if($this->employees_model->delete_salary_overtime($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('salary_overtime_deleted')]);
     	}
     }

     public function add_salary_overtime()
     {
     	// print_r($this->input->post()); die;
     	if($this->input->post()) {
     		
	     	$data = array(
				'employee_id' => $this->input->post('emp_id'),
				'overtime_type' => $this->input->post('overtime_type'),
				'no_of_days' => $this->input->post('no_of_days'),
				'overtime_rate' => $this->input->post('overtime_rate'),
				'overtime_hours' => $this->input->post('overtime_hours'),
			);

			$insert = $this->employees_model->add_salary_overtime($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function edit_salary_overtime($id='')
     {
     	$this->data['salary_overtime'] = $this->employees_model->get_salary_overtime($id);
     	
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_salary_overtime', $this->data);
     }
      public function editSalaryOvertime($id='')
     {
     	if($this->input->post()) {
     		$salary_overtime_id = $this->input->post('salary_overtime_id');
     		
	     	$data = array(
				'overtime_type' => $this->input->post('overtime_type'),
				'no_of_days' => $this->input->post('no_of_days'),
				'overtime_rate' => $this->input->post('overtime_rate'),
				'overtime_hours' => $this->input->post('overtime_hours'),
			);

			$update = $this->employees_model->editSalaryOvertime($data, $salary_overtime_id);
			if($update) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function getEmployeeLoan($emp_id='')
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('loan_deduction_id as id, loan_deduction_title, loan_options, CONCAT_WS(" to ", start_date, end_date), monthly_installment, loan_time')
			->from('salary_loan_deductions')
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('employees/edit_employee_loan/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_employee_loan') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_employee_loan') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('employees/delete_employee_loan/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }
     public function delete_employee_loan($id='')
     {
     	if($this->employees_model->delete_employee_loan($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('employee_loan_deleted')]);
     	}
     }

     public function add_employee_loan()
     {
     	// print_r($this->input->post()); die;
     	if($this->input->post()) {

     		$tm = $this->employees_model->get_month_diff($this->input->post('start_date'),$this->input->post('end_date'));
			if($tm < 1) {
				$m_ins = $this->input->post('monthly_installment');
			} else {
				$m_ins = $this->input->post('monthly_installment')/$tm;
			}
     		
	     	$data = array(
				'employee_id' => $this->input->post('emp_id'),
				'loan_options' => $this->input->post('loan_options'),
				'loan_deduction_title' => $this->input->post('loan_deduction_title'),
				'monthly_installment' => $this->input->post('monthly_installment'),
				'loan_deduction_amount' => $m_ins,
				'loan_time' => $tm,
				'start_date' => $this->input->post('start_date'),
				'end_date' => $this->input->post('end_date'),
				'reason' => $this->input->post('reason'),
			);

			$insert = $this->employees_model->add_employee_loan($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
     	} 
     }

     public function edit_employee_loan($id='')
     {
     	$this->data['employee_loan'] = $this->employees_model->get_employee_loan($id);
     	
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme.'employees/edit_employee_loan', $this->data);
     }
      public function editEmployeeLoan($id='')
     {
     	if($this->input->post()) {
     		$loan_deduction_id = $this->input->post('loan_deduction_id');
     		
     		$tm = $this->employees_model->get_month_diff($this->input->post('start_date'),$this->input->post('end_date'));
			if($tm < 1) {
				$m_ins = $this->input->post('monthly_installment');
			} else {
				$m_ins = $this->input->post('monthly_installment')/$tm;
			}
	     	$data = array(
				'loan_options' => $this->input->post('loan_options'),
				'loan_deduction_title' => $this->input->post('loan_deduction_title'),
				'monthly_installment' => $this->input->post('monthly_installment'),
				'loan_deduction_amount' => $m_ins,
				'loan_time' => $tm,
				'start_date' => $this->input->post('start_date'),
				'end_date' => $this->input->post('end_date'),
				'reason' => $this->input->post('reason'),
			);

			$update = $this->employees_model->editEmployeeLoan($data, $loan_deduction_id);
			if($update) {
				return true;
			} else {
				return false;
			}
     	} 
     }

}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hr_reports extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('employees_model');
        $this->load->admin_model('payroll_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('timesheet_model');
		$this->load->library('form_validation');
    }

	public function employee_reports()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('employees'), 'page' => lang('employees')], ['link' => '#', 'page' => lang('employees')]];
        $meta = ['page_title' => lang('employees'), 'bc' => $bc];
		$this->page_construct('hr_reports/employees_reports', $meta, $this->data);
     }
	 public function employees_list()
	 {
 
		 $this->load->library('datatables');
 // department, designation
		 $this->datatables
			 ->select('employees.user_id as emp_id, CONCAT_WS(" ", first_name, last_name) as whole_name, employees.employee_id, departments.department_name, designations.designation_name,  warehouses.name, employees.contact_no, employees.email, employees.date_of_joining')
			 ->from('employees')
			 ->join('warehouses', 'warehouses.id = employees.warehouse_id')
			 ->join('departments', 'departments.department_id = employees.department_id')
			 ->join('designations', 'designations.designation_id = employees.designation_id')
			 ->where('employees.user_role_id !=', 1)
			 ->group_by('employees.user_id')			 
			 ;
 
			 // print_r($this->db->error());
 
		 echo $this->datatables->generate();
		 
	  } 
	  public function employee_pdf()
	  {
		  $mpdf = new \Mpdf\Mpdf();
		  $data['employees'] = $this->db
			  ->select('employees.user_id as emp_id, CONCAT_WS(" ", first_name, last_name) as whole_name, employees.employee_id, warehouses.name as warehouse_name, employees.contact_no,  employees.contact_no, , employees.email, employees.date_of_joining, departments.department_name, designations.designation_name')
			  ->from('employees')
			  ->join('warehouses', 'warehouses.id = employees.warehouse_id')
			  ->join('departments', 'departments.department_id = employees.department_id')
			  ->join('designations', 'designations.designation_id = employees.designation_id')
			  ->where('employees.user_role_id !=', 1)
			  ->group_by('employees.user_id')
			  ->get()
			  ->result();
		  $html = $this->load->view($this->theme.'hr_reports/employee_list_pdf', $data, true);
		  $pdfFilePath = 'employees_info'.".pdf";
		  $mpdf->WriteHTML($html);
		  $mpdf->Output();
		  //$mpdf->Output($pdfFilePath, "D"); // opens in browser
		  //$mpdf->Output($pdfFilePath,'D') will work as normal download
	}
	public function employee_excel(){ 
        // // file name 
        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");
       // get data 
	   $usersData = $this->db
			->select('employees.user_id as user_id, employees.user_id as emp_id, CONCAT_WS(" ", first_name, last_name) as whole_name, employees.employee_id, warehouses.name as warehouse_name, employees.contact_no, employees.date_of_joining, departments.department_name, designations.designation_name')
			->from('employees')
			->join('warehouses', 'warehouses.id = employees.warehouse_id')
			->join('departments', 'departments.department_id = employees.department_id')
			->join('designations', 'designations.designation_id = employees.designation_id')
			->where('employees.user_role_id !=', 1)
			->group_by('employees.user_id')
			->get()
			->result_array();
        //$usersData = $this->db->query($sql)->result_array();
        // file creation 
        $file = fopen('php://output','w');
        $header = array("SL","Employee ID","Name","Department", "Designation", "Warehouse", "Contact"); 
        fputcsv($file, $header);
        $lines=array();
        $serial = 1;
        foreach ($usersData as $key=>$line){ 
            $lines['serial']=$serial;
            $lines['employee_id']=$line['employee_id'];
            $lines['whole_name']=$line['whole_name'];
            $lines['department_name']=$line['department_name'];
            $lines['designation_name']=$line['designation_name'];
            $lines['warehouse_name']=$line['warehouse_name'];
            $lines['contact_no']=$line['contact_no'];
// print_r($lines);exit();
			$serial++;
            fputcsv($file,$lines); 
        }
        fclose($file); 
        exit; 
    }

	public function attendance_monthly_report()
	{
		$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error'); 

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('attendance_monthly_report')]];
        $meta = ['page_title' => lang('attendance_monthly_report'), 'bc' => $bc];
		$this->page_construct('hr_reports/attendance_monthly_report', $meta, $this->data);
	}
	
	public function attendance_monthly_list()
	{
		$iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

		$dateSearch=$this->input->post('dateSearch');
		$dateSearch =date('Y-m-d', strtotime($dateSearch));
		
		$all_employees = $this->timesheet_model->all_employees_attendances($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);

		$j=0;
		if(!empty($dateSearch) && $dateSearch != '1970-01-01') {
            $date = strtotime($this->input->post('dateSearch'));
        } else {
			$date = strtotime(date("Y-m-d"));
		}
		$attend_date = $date;
		//$date = strtotime('2020-05-01');
		$day = date('d', $date);
		$month = date('m', $date);
		$year = date('Y', $date);
		$month_year = date('Y-m', $date);
		// total days in month
		$daysInMonth =  date('t', $date);
		$data = [];

		foreach($all_employees as $r) { 

			$present_count = 0;
			$absent_count = 0;
			$leave_count = 0;
			$holiday_count = 0;
			$total_hours_monthly = 0;
			$total_late_in = 0;
			$total_early_leave = 0;
			for($i = 1; $i <= $daysInMonth; $i++):
				$i = str_pad($i, 2, 0, STR_PAD_LEFT);
				// get date <
				$attendance_date = $year.'-'.$month.'-'.$i;
				$tdate = $year.'-'.$month.'-'.$i;
				$get_day = strtotime($attendance_date);
				$day = date('l', $get_day);
				$user_id = $r->user_id;
				// office shift
				$office_shift = $this->timesheet_model->read_office_shift_information($r->office_shift_id);
				if(!is_null($office_shift)){
				// get holiday
				$h_date_chck = $this->timesheet_model->holiday_date_check($attendance_date);
				$holiday_arr = array();
				if($h_date_chck->num_rows() == 1){
					$h_date = $this->timesheet_model->holiday_date($attendance_date);
					$begin = new DateTime( $h_date[0]->start_date );
					$end = new DateTime( $h_date[0]->end_date);
					$end = $end->modify( '+1 day' ); 
					
					$interval = new DateInterval('P1D');
					$daterange = new DatePeriod($begin, $interval ,$end);
					
					foreach($daterange as $date){
						$holiday_arr[] =  $date->format("Y-m-d");
					}
				} else {
					$holiday_arr[] = '99-99-99';
				}
				//echo '<pre>'; print_r($holiday_arr);
				// get leave/employee
				$leave_date_chck = $this->timesheet_model->leave_date_check($r->user_id,$attendance_date);
				$leave_arr = array();
				if($leave_date_chck->num_rows() == 1){
					$leave_date = $this->timesheet_model->leave_date($r->user_id,$attendance_date);
					$begin1 = new DateTime( $leave_date[0]->from_date );
					$end1 = new DateTime( $leave_date[0]->to_date);
					$end1 = $end1->modify( '+1 day' ); 
					
					$interval1 = new DateInterval('P1D');
					$daterange1 = new DatePeriod($begin1, $interval1 ,$end1);
					
					foreach($daterange1 as $date1){
						$leave_arr[] =  $date1->format("Y-m-d");
					}	
				} else {
					$leave_arr[] = '99-99-99';
				}
				$attendance_status = '';
				$check = $this->timesheet_model->attendance_first_in_check($r->user_id,$attendance_date);
				if($office_shift[0]->monday_in_time == '' && $day == 'Monday') {
					$status = 'H';	
					$holiday_count++;
				} else if($office_shift[0]->tuesday_in_time == '' && $day == 'Tuesday') {
					$status = 'H';
					$holiday_count++;
				} else if($office_shift[0]->wednesday_in_time == '' && $day == 'Wednesday') {
					$status = 'H';
					$holiday_count++;
				} else if($office_shift[0]->thursday_in_time == '' && $day == 'Thursday') {
					$status = 'H';
					$holiday_count++;
				} else if($office_shift[0]->friday_in_time == '' && $day == 'Friday') {
					$status = 'H';
					$holiday_count++;
				} else if($office_shift[0]->saturday_in_time == '' && $day == 'Saturday') {
					$status = 'H';
					$holiday_count++;
				} else if($office_shift[0]->sunday_in_time == '' && $day == 'Sunday') {
					$status = 'H';
					$holiday_count++;
				} else if(in_array($attendance_date,$holiday_arr)) { // holiday
					$status = 'H';
					$holiday_count++;
				} else if(in_array($attendance_date,$leave_arr)) { // on leave
					$status = 'L';
					$leave_count++;
				} else if($check->num_rows() > 0){
					$attendance = $this->timesheet_model->attendance_first_in($r->user_id,$attendance_date);
					$status = 'P';	
					$present_count++;				
				} else {
					$status = 'A';
					$absent_count++;
					//$pcount += 0;
				}

				// total hours work/ed
				$total_hrs = $this->timesheet_model->total_hours_worked_attendance($r->user_id,$attendance_date);
				$hrs_old_int1 = '';
				$Total = '';
				$Trest = '';
				$total_time_rs = '';
				$hrs_old_int_res1 = '';
				$hours_total = 0;
				
				foreach ($total_hrs->result() as $hour_work){		
					// total work			
					$timee = $hour_work->total_work.':00';
					$str_time =$timee;
		
					$str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
					
					sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
					
					$hrs_old_seconds = $hours * 3600 + $minutes * 60 + $seconds;
					
					$hrs_old_int1 = $hrs_old_seconds;
					
					$Total = gmdate("H:i", $hrs_old_int1);	
					$hours_total += $hours; 
				}
				if($Total=='') {
					$total_work = '00:00';
				} else {
					$total_work = $Total;
				}
				$total_hours_monthly += $hours_total; 

				// get clock in/clock out of each employee
				if($day == 'Monday') {
					if($office_shift[0]->monday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->monday_in_time;
						$out_time = $office_shift[0]->monday_out_time;
					}
				} else if($day == 'Tuesday') {
					if($office_shift[0]->tuesday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->tuesday_in_time;
						$out_time = $office_shift[0]->tuesday_out_time;
					}
				} else if($day == 'Wednesday') {
					if($office_shift[0]->wednesday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->wednesday_in_time;
						$out_time = $office_shift[0]->wednesday_out_time;
					}
				} else if($day == 'Thursday') {
					if($office_shift[0]->thursday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->thursday_in_time;
						$out_time = $office_shift[0]->thursday_out_time;
					}
				} else if($day == 'Friday') {
					if($office_shift[0]->friday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->friday_in_time;
						$out_time = $office_shift[0]->friday_out_time;
					}
				} else if($day == 'Saturday') {
					if($office_shift[0]->saturday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->saturday_in_time;
						$out_time = $office_shift[0]->saturday_out_time;
					}
				} else if($day == 'Sunday') {
					if($office_shift[0]->sunday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->sunday_in_time;
						$out_time = $office_shift[0]->sunday_out_time;
					}
				}
				// Late Entrance
				$check = $this->timesheet_model->attendance_first_in_check($r->user_id,$attendance_date);
				
				if($check->num_rows() > 0){
					// check clock in time
					$attendance = $this->timesheet_model->attendance_first_in($r->user_id,$attendance_date);
					
					// clock in
					$clock_in = new DateTime($attendance[0]->clock_in);
					$clock_in2 = $clock_in->format('h:i a');
					
					$clkInIp = $clock_in2;
					
					$office_time =  new DateTime($in_time.' '.$attendance_date);
					//time diff > total time late
					$office_time_new = strtotime($in_time.' '.$attendance_date);
					$clock_in_time_new = strtotime($attendance[0]->clock_in);
					if($clock_in_time_new <= $office_time_new) {
						$total_time_l = '00:00';
					} else {
						$total_late_in++;
					}

					/* early time */
					$early_time =  new DateTime($out_time.' '.$attendance_date);
					// check clock in time
					$first_out = $this->timesheet_model->attendance_first_out($r->user_id,$attendance_date);
					// clock out
					$clock_out = new DateTime($first_out[0]->clock_out);
					
					if ($first_out[0]->clock_out!='') {
						$clock_out2 = $clock_out->format('h:i a');
						
						$clkOutIp = $clock_out2;

						// early leaving
						$early_new_time = strtotime($out_time.' '.$attendance_date);
						$clock_out_time_new = strtotime($first_out[0]->clock_out);
					
						if($early_new_time <= $clock_out_time_new) {
							$total_time_e = '00:00';
						} else {			
							$total_early_leave++;
						}
						
					}
				}
				// print_r($total_hours_monthly); die;
				//$pcount += $check->num_rows();
				// set to present date
				$iattendance_date = strtotime($attendance_date);
				$icurrent_date = strtotime(date('Y-m-d'));
				$status = $status;
				if($iattendance_date <= $icurrent_date){
					$status = $status;
				} else {
					$status = '--';
				}
				$idate_of_joining = strtotime($r->date_of_joining);
				if($idate_of_joining < $iattendance_date){
					$status = $status;
				} else {
					$status = '--';
				}
				} else {
					$status = '<a href="javascript:void(0)" class="badge badge-danger">'.$this->lang->line('xin_office_shift_not_assigned').'</a>';
					$attendance_date = '';
					$attendance_date = '';
				}
				$someArray[] = array(
					'title' => $status,
					'resourceId' => $r->user_id,
					'start'   => $attendance_date,
					'end'   => $attendance_date,
				);
			  endfor;	

			  $department_name = $this->db->get_where('departments', array('department_id' => $r->department_id))->row()->department_name;
			  $working_days = $daysInMonth - ($holiday_count + $leave_count); 
			//   print_r($date);die;
			  $date_cloumn = date('Y-m-d', $attend_date);
			  $nestedData = [];
			  $nestedData[] = $r->user_id;
			  $nestedDataPdf['user_id'] = $r->user_id;
			  $nestedData[] = $r->first_name.' '.$r->last_name;
			  $nestedDataPdf['emp_name'] = $r->first_name.' '.$r->last_name;
			  $nestedData[] = $r->employee_id;
			  $nestedDataPdf['employee_id'] = $r->employee_id;
			  $nestedData[] = $department_name;
			  $nestedDataPdf['department_name'] = $department_name;
			  $nestedData[] = $date_cloumn;
			  $nestedData[] = $daysInMonth;
			  $nestedDataPdf['daysInMonth'] = $daysInMonth;
			  $nestedData[] = $present_count;
			  $nestedDataPdf['present_count'] = $present_count;
			  $nestedData[] = $absent_count;
			  $nestedDataPdf['absent_count'] = $absent_count;
			  $nestedData[] = $holiday_count;
			  $nestedDataPdf['holiday_count'] = $holiday_count;
			  $nestedData[] = $leave_count;
			  $nestedDataPdf['leave_count'] = $leave_count;
			  $nestedData[] = $working_days;
			  $nestedDataPdf['working_days'] = $working_days;
			  $nestedData[] = $total_late_in;
			  $nestedDataPdf['total_late_in'] = $total_late_in;
			  $nestedData[] = $total_early_leave;
			  $nestedDataPdf['total_early_leave'] = $total_early_leave;
			  $nestedData[] = $total_hours_monthly;
			  $nestedDataPdf['total_hours_monthly'] = $total_hours_monthly;

			  $data[] = $nestedData;
			  $dataPdf[] = $nestedDataPdf;
		}
		//$all_users = $this->payroll_model->totalEmployees($filter_emp, $filter_salaryMonth, $sSearch);

		$all_users = $this->timesheet_model->totalEmployees($sSearch);
		$totalData = sizeof($all_users); 

		//$displayRecords = sizeof($records);

		$sOutput = [
			//'sEcho'                => intval($this->ci->input->post('sEcho')),
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
			//'sColumns'             => implode(',', $sColumns),
		];
		// for pdf
		$print_pdf = $this->input->get('pdf');
		if($print_pdf == 1) {
			$data['data'] = $dataPdf;
			$mpdf = new \Mpdf\Mpdf();
			$html = $this->load->view($this->theme.'hr_reports/attendance_monthly_pdf', $data, true);
			$pdfFilePath = 'employees_info'.".pdf";
			$mpdf->WriteHTML($html);
			$mpdf->Output();
		} 
		// show datatable
		else {
			echo json_encode($sOutput);
		}	

		//echo json_encode($sOutput);
	}

	public function salary_reports()
    {
    	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

		$this->data['all_employees'] = $this->employees_model->all_employees();

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('salary_reports')]];
        $meta = ['page_title' => lang('salary_reports'), 'bc' => $bc];
		$this->page_construct('hr_reports/salary_reports', $meta, $this->data);
    }

    // payslip > employees
	public function payslip_list() 
	{
		
		$iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
		$filter_emp = $this->input->post('empid');
		$filter_salaryMonth = $this->input->post('salaryMonth');
		$filter_salaryMonth = date('Y-m', strtotime($filter_salaryMonth));

		$records = $this->payroll_model->get_employees_payslip($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $filter_emp, $filter_salaryMonth);

		

		// $p_date = $this->input->get("month_year") ? $this->input->get("month_year") : date('Y-m');
		$p_date = $filter_salaryMonth;
// echo "<pre>"; print_r($p_date); die;
		$i = 0;
		$data = array();
		if (isset($records) && count($records) > 0) {
			foreach ($records as $key => $value) {

				// ___get total hours > worked > employee___	
				$pay_day_input = $this->input->get("month_year");		
				$pay_date = $this->input->get("month_year") ? date('Y-m',strtotime($pay_day_input)) : date('Y-m');
				

				$total_hours_worked = $this->payroll_model->total_hours_worked($value->user_id,$pay_date);
				// echo "<pre>"; print_r($total_hours_worked->result()); die;
				$hrs_old_int1 = 0;
				$pcount = 0;
				$Trest = 0;
				$total_time_rs = 0;
				$hrs_old_int_res1 = 0;
				foreach ($total_hours_worked->result() as $hour_work){
					// total work			
					$clock_in =  new DateTime($hour_work->clock_in);
					$clock_out =  new DateTime($hour_work->clock_out);
					$interval_late = $clock_in->diff($clock_out);
					$hours_r  = $interval_late->format('%h');
					$minutes_r = $interval_late->format('%i');			
					$total_time = $hours_r .":".$minutes_r.":".'00';
					
					$str_time = $total_time;
					$str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
					sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
					$hrs_old_seconds = $hours * 3600 + $minutes * 60 + $seconds;
					$hrs_old_int1 += $hrs_old_seconds;
					
					$pcount = gmdate("H", $hrs_old_int1);			
				}
				$pcount = $pcount;
				// end ___get total hours > worked > employee___

				// 1: salary type
				if($value->wages_type==1){
					$wages_type = $this->lang->line('payroll_basic_salary');
					$basic_salary = $value->basic_salary;
					$p_class = 'emo_monthly_pay';
					$view_p_class = 'payroll_template_modal';
				} else if($value->wages_type==2){
					$wages_type = lang('employee_daily_wages');
					if($pcount > 0){
						$basic_salary = $pcount * $value->basic_salary;
					} else {
						$basic_salary = $pcount;
					}
					$p_class = 'emo_hourly_pay';
					$view_p_class = 'hourlywages_template_modal';
				} else {
					$wages_type = lang('payroll_basic_salary');
					$basic_salary = $value->basic_salary;
					$p_class = 'emo_monthly_pay';
					$view_p_class = 'payroll_template_modal';
					
				}	
				//echo ";"; print_r($value->user_id); echo "-"; print_r($pcount); echo "-";print_r($basic_salary); if($value->user_id == 24) die;
				// 2: all allowances
				$salary_allowances = $this->payroll_model->read_salary_allowances($value->user_id);
				$count_allowances = $this->payroll_model->count_employee_allowances($value->user_id);
				$allowance_amount = 0;
				if($count_allowances > 0) {
					foreach($salary_allowances as $sl_allowances){
						if($this->Settings->is_half_monthly==1){
					  	 if($this->Settings->half_deduct_month==2){
							 $eallowance_amount = $sl_allowances->allowance_amount/2;
						 } else {
							 $eallowance_amount = $sl_allowances->allowance_amount;
						 }
						  $allowance_amount += $eallowance_amount;
                      } else {
						  //$eallowance_amount = $sl_allowances->allowance_amount;
						  if($sl_allowances->is_allowance_taxable == 1) {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount;
							  } else {
								  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
							  }
							 $allowance_amount -= $iallowance_amount; 
						  } else if($sl_allowances->is_allowance_taxable == 2) {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount / 2;
							  } else {
								  $iallowance_amount = ($basic_salary / 100) / 2 * $sl_allowances->allowance_amount;
							  }
							 $allowance_amount -= $iallowance_amount; 
						  } else {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount;
							  } else {
								  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
							  }
							  $allowance_amount += $iallowance_amount;
						  }
                      }
					 
					}
				} else {
					$allowance_amount = 0;
				}
				// get allowance column
				$house_rent = 0;
				$medical_allowance = 0;
				$travel_allowance = 0;
				$other_allowance = 0;
				if($count_allowances > 0) {
					foreach($salary_allowances as $sl_allowances){
						
						if($sl_allowances->allowance_type == 'House Rent') {
							$house_rent = $sl_allowances->allowance_amount;
						}
						if($sl_allowances->allowance_type == 'Meidcal') {
							$medical_allowance = $sl_allowances->allowance_amount;
						}
						if($sl_allowances->allowance_type == 'Travel') {
							$travel_allowance = $sl_allowances->allowance_amount;
						}
						if($sl_allowances->allowance_type == 'Other') {
							$other_allowance = $sl_allowances->allowance_amount;
						}
					}
				} else {
					$house_rent = 0;
					$medical_allowance = 0;
					$travel_allowance = 0;
					$other_allowance = 0;
				}

				// 3: all loan/deductions
				$salary_loan_deduction = $this->payroll_model->read_salary_loan_deductions($value->user_id);
				$count_loan_deduction = $this->payroll_model->count_employee_deductions($value->user_id);
				$loan_de_amount = 0;
				if($count_loan_deduction > 0) {
					foreach($salary_loan_deduction as $sl_salary_loan_deduction){
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount/2;
						  } else {
							  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
						  }
                      } else {
						  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
                      }
					  $loan_de_amount += $er_loan;
					}
				} else {
					$loan_de_amount = 0;
				}

				// commissions
				$count_commissions = $this->payroll_model->count_employee_commissions($value->user_id);
				$commissions = $this->payroll_model->set_employee_commissions($value->user_id);
				$commissions_amount = 0;
				if($count_commissions > 0) {
					foreach($commissions->result() as $sl_salary_commissions){
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $ecommissions_amount = $sl_salary_commissions->commission_amount/2;
						  } else {
							  $ecommissions_amount = $sl_salary_commissions->commission_amount;
						  }
						  $commissions_amount += $ecommissions_amount;
                      } else {
						 // $ecommissions_amount = $sl_salary_commissions->commission_amount;
						 if($sl_salary_commissions->is_commission_taxable == 1) {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount;
							  } else {
								  $ecommissions_amount = $basic_salary / 100 * $sl_salary_commissions->commission_amount;
							  }
							 $commissions_amount -= $ecommissions_amount; 
						  } else if($sl_salary_commissions->is_commission_taxable == 2) {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount / 2;
							  } else {
								  $ecommissions_amount = ($basic_salary / 100) / 2 * $sl_salary_commissions->commission_amount;
							  }
							 $commissions_amount -= $ecommissions_amount; 
						  } else {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount;
							  } else {
								  $ecommissions_amount = $basic_salary / 100 * $sl_salary_commissions->commission_amount;
							  }
							  $commissions_amount += $ecommissions_amount;
						  }
                      }
					  
					}
				} else {
					$commissions_amount = 0;
				}

				// otherpayments
				$count_other_payments = $this->payroll_model->count_employee_other_payments($value->user_id);
				$other_payments = $this->payroll_model->set_employee_other_payments($value->user_id);
				$other_payments_amount = 0;
				if($count_other_payments > 0) {
					foreach($other_payments->result() as $sl_other_payments) {
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $epayments_amount = $sl_other_payments->payments_amount/2;
						  } else {
							  $epayments_amount = $sl_other_payments->payments_amount;
						  }
						  $other_payments_amount += $epayments_amount;
                      } else {
						 // $epayments_amount = $sl_other_payments->payments_amount;
						  if($sl_other_payments->is_otherpayment_taxable == 1) {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount;
							  } else {
								  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
							  }
							 $other_payments_amount -= $epayments_amount; 
						  } else if($sl_other_payments->is_otherpayment_taxable == 2) {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount / 2;
							  } else {
								  $epayments_amount = ($basic_salary / 100) / 2 * $sl_other_payments->payments_amount;
							  }
							 $other_payments_amount -= $epayments_amount; 
						  } else {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount;
							  } else {
								  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
							  }
							  $other_payments_amount += $epayments_amount;
						  }
                      }
					  
					}
				} else {
					$other_payments_amount = 0;
				}

				// statutory_deductions
				$count_statutory_deductions = $this->payroll_model->count_employee_statutory_deductions($value->user_id);
				$statutory_deductions = $this->payroll_model->set_employee_statutory_deductions($value->user_id);
				$statutory_deductions_amount = 0;
				if($count_statutory_deductions > 0) {
					foreach($statutory_deductions->result() as $sl_salary_statutory_deductions){
						if($this->Settings->is_half_monthly==1){
							  if($this->Settings->half_deduct_month==2){
								  $single_sd = $sl_salary_statutory_deductions->deduction_amount/2;
							  } else {
								   $single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  }
							  $statutory_deductions_amount += $single_sd;
						  } else {
							  //$single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  if($sl_salary_statutory_deductions->statutory_options == 0) {
								  $single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  } else {
								  $single_sd = $basic_salary / 100 * $sl_salary_statutory_deductions->deduction_amount;
							  }
							  $statutory_deductions_amount += $single_sd;
						  }
						  
					}
				} else {
					$statutory_deductions_amount = 0;
				}	

				// 5: overtime
				$salary_overtime = $this->payroll_model->read_salary_overtime($value->user_id);
				$count_overtime = $this->payroll_model->count_employee_overtime($value->user_id);
				$overtime_amount = 0;
				if($count_overtime > 0) {
					foreach($salary_overtime as $sl_overtime){
						if($this->Settings->is_half_monthly==1){
							if($this->Settings->half_deduct_month==2){
								$eovertime_hours = $sl_overtime->overtime_hours/2;
								$eovertime_rate = $sl_overtime->overtime_rate/2;
							} else {
								$eovertime_hours = $sl_overtime->overtime_hours;
								$eovertime_rate = $sl_overtime->overtime_rate;
							}
						} else {
							$eovertime_hours = $sl_overtime->overtime_hours;
							$eovertime_rate = $sl_overtime->overtime_rate;
						}
						$overtime_total = $eovertime_hours * $eovertime_rate;
						//$overtime_total = $sl_overtime->overtime_hours * $sl_overtime->overtime_rate;
						$overtime_amount += $overtime_total;
					}
				} else {
					$overtime_amount = 0;
				}

				// add amount				
				$total_earning = $basic_salary + $allowance_amount + $overtime_amount + $commissions_amount + $other_payments_amount;
				$total_deduction = $loan_de_amount + $statutory_deductions_amount;
				$total_net_salary = $total_earning - $total_deduction;
				$net_salary = $total_net_salary;
				$advance_amount = 0;

				// get advance salary
				$advance_salary = $this->payroll_model->advance_salary_by_employee_id($value->user_id);
				$emp_value = $this->payroll_model->get_paid_salary_by_employee_id($value->user_id);
				
				if(!is_null($advance_salary)){
					$monthly_installment = $advance_salary[0]->monthly_installment;
					$advance_amount = $advance_salary[0]->advance_amount;
					$total_paid = $advance_salary[0]->total_paid;
					//check ifpaid
					$em_advance_amount = $advance_salary[0]->advance_amount;
					$em_total_paid = $advance_salary[0]->total_paid;
					
					if($em_advance_amount > $em_total_paid){
						if($monthly_installment=='' || $monthly_installment==0) {
							
							$ntotal_paid = $emp_value[0]->total_paid;
							$nadvance = $emp_value[0]->advance_amount;
							$i_net_salary = $nadvance - $ntotal_paid;
							//$pay_amount = $net_salary - $i_net_salary;
							$advance_amount = $i_net_salary;
						} else {
							//
							$re_amount = $em_advance_amount - $em_total_paid;
							if($monthly_installment > $re_amount){
								$advance_amount = $re_amount;
								//$total_net_salary = $net_salary - $re_amount;
								$pay_amount = $net_salary - $re_amount;
							} else {
								$advance_amount = $monthly_installment;
								//$total_net_salary = $net_salary - $monthly_installment;
								$pay_amount = $net_salary - $monthly_installment;
							}
						}
						
					} else {
						$i_net_salary = $net_salary - 0;
						$pay_amount = $net_salary - 0;
						$advance_amount = 0;
					}
				} else {
					$pay_amount = $net_salary - 0;
					$i_net_salary = $net_salary - 0;	
					$advance_amount = 0;
				}
				$total_net_salary = $total_net_salary - $advance_amount;
// echo "<pre>"; print_r($total_net_salary); die;

				$payroll_data = array(
					'total_net_salary' => $total_net_salary
				);

				// Action column, view payslip
				$payment_check = $this->payroll_model->read_make_payment_payslip_check($value->user_id,$p_date);
				// print_r($payment_check->result());die;
					if($payment_check->num_rows() > 0){
						$make_payment = $this->payroll_model->read_make_payment_payslip($value->user_id,$p_date);
						$view_url = 'payroll/payslip/id/'.$make_payment[0]->payslip_key;
						
						$status = '<span class="label label-success">'.$this->lang->line('xin_payroll_paid').'</span>';
						
						$view_payslip = "<div class=\"text-center\"><a href='" . admin_url('payroll/dialog_payroll_detail/?user_id='.$value->user_id.'&p_date='.$p_date) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_payroll_detail') . "'><i class=\"fa fa-eye\"></i></a> ";
						$mpay = "<a href='" . admin_url($view_url) . "' class='tip' title='" . lang('view_payment') . "'><i class=\"fa fa-file-text\"></i></a> ";
						
						$delete = " <a href='#' class='tip po' title='<b>" . lang('delete_payment') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('payroll/delete_payslip/'.$make_payment[0]->payslip_id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
						
						$mpay = $view_payslip . $mpay . $delete; 
						$total_net_salary = $make_payment[0]->net_salary;
					} else {
						$status = '<span class="label label-danger">'.$this->lang->line('xin_payroll_unpaid').'</span>';
						$mpay = "<div class=\"text-center\"><a href='" . admin_url('payroll/dialog_payroll_detail/?user_id='.$value->user_id.'&p_date='.$p_date) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_payroll_detail') . "'><i class=\"fa fa-eye\"></i></a> <a href='" . admin_url('payroll/dialog_make_payment?employee_id='.$value->user_id . "&pay_date=" . $p_date) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('make_payment') . "'><i class=\"fa fa-money\"></i></a></div>";
						$delete = '';
						$total_net_salary = $total_net_salary;
					}
					//detail link
				$detail = '<span data-toggle="tooltip" data-state="primary" data-placement="top" title="'.$this->lang->line('xin_view').'"><button type="button" class="btn icon-btn btn-sm btn-outline-secondary waves-effect waves-light" data-toggle="modal" data-target="#'.$view_p_class.'" data-employee_id="'. $value->user_id . '"><span class="fa fa-eye"></span></button></span>';

				// Datatable set columns
				if($value->wages_type == 1) {
					$wages_type = 'Monthly Payslip';
				} elseif($value->wages_type == 1) {
					$wages_type = 'Hourly Payslip';
				}

				$i++;
				$nestedData = array();
				// $nestedData["DT_RowId"] = $value->user_id;
				$department_name = $this->db->get_where('departments', array('department_id' => $value->department_id))->row()->department_name;

				$nestedData[] = $value->user_id;
				
				$nestedData[] = $value->first_name. ' '. $value->last_name;
				$nestedDataPdf['name'] = $value->first_name. ' '. $value->last_name;
				$nestedData[] = $value->employee_id;
				$nestedDataPdf['employee_id'] = $value->employee_id;
				// $nestedData[] = $department_name;
				$nestedDataPdf['department_name'] = $department_name;
				$nestedData[] = $wages_type;
				$nestedDataPdf['wage_type'] = $wages_type;
				$nestedData[] = $value->basic_salary;
				$nestedDataPdf['basic_salary'] = $value->basic_salary;
				$nestedData[] = $house_rent;
				$nestedDataPdf['house_rent'] = $house_rent;
				$nestedData[] = $medical_allowance;
				$nestedDataPdf['medical_allowance'] = $medical_allowance;
				$nestedData[] = $travel_allowance;
				$nestedDataPdf['travel_allowance'] = $travel_allowance;
				$nestedData[] = $other_allowance;
				$nestedDataPdf['other_allowance'] = $other_allowance;
				$nestedData[] = $allowance_amount;
				$nestedDataPdf['allowance_amount'] = $allowance_amount;
				$nestedData[] = $overtime_amount;
				$nestedDataPdf['overtime_amount'] = $overtime_amount;
				$nestedData[] = $commissions_amount;
				$nestedDataPdf['commissions_amount'] = $commissions_amount;
				$nestedData[] = $other_payments_amount;
				$nestedDataPdf['other_payments_amount'] = $other_payments_amount;
				$nestedData[] = $loan_de_amount;
				$nestedDataPdf['loan_de_amount'] = $loan_de_amount;
				$nestedData[] = $statutory_deductions_amount;
				$nestedDataPdf['statutory_deductions_amount'] = $statutory_deductions_amount;
				$nestedData[] = $total_net_salary;
				$nestedDataPdf['total_net_salary'] = $total_net_salary;
				$nestedData[] = $value->e_status;
				$nestedDataPdf['e_status'] = $value->e_status;
				$edit = '';
				$edit = anchor("edit_supplier/".$value->user_id,
                    '<i class="fa fa-eye"></i> ',
                    array('class'=>'text-center', 'title'=>'View'));
				$edit .= ' ' . anchor("supplier/supplier/delete_supplier/".$value->user_id,
                    ' <i class="fa fa-money" aria-hidden="true"></i>',
                    array('class'=>'text-center', 'title'=>'Make Payment', 'onclick' => "return confirm('Do you want delete this record')"));
				//$nestedData[] = $mpay;

				$data[] = $nestedData;
				$dataPdf[] = $nestedDataPdf;
			}
		}
		$all_users = $this->payroll_model->totalEmployees($filter_emp, $filter_salaryMonth, $sSearch);
		$totalData = sizeof($all_users); 

		$sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		$print_pdf = $this->input->get('pdf');
		if($print_pdf == 1) {
			$data['data'] = $dataPdf;
			$mpdf = new \Mpdf\Mpdf();
			$html = $this->load->view($this->theme.'hr_reports/salary_reports_pdf', $data, true);
			$pdfFilePath = 'employees_info'.".pdf";
			$mpdf->WriteHTML($html);
			$mpdf->Output();
		} else {
			echo json_encode($sOutput);
		}
		//return ['aaData' => $aaData, 'sColumns' => $sColumns];
	}

}
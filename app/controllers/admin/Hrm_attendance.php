<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_attendance extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('roster_model');
        $this->load->admin_model('shift_model');
        $this->load->admin_model('employees_model');
        $this->load->admin_model('attendance_model');
        $this->load->admin_model('department_model');
        $this->load->admin_model('hrm_attendance_model');
        $this->load->library('form_validation');
    }

    public function attendance_log_list()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('attendance_log')]];
        $meta = ['page_title' => lang('attendance_log'), 'bc' => $bc];
		$this->page_construct('hrm_attendance/attendance_log_list', $meta, $this->data);
    } 

    public function getAttendanceLog()
    {
        // echo "<pre>"; print_r($this->input->post()); die;
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
		$empid=$this->input->post('empid');
		$department_id=$this->input->post('departmentId');
		$start_date= date('Y-m-d', strtotime($this->input->post('startDate')));
		$end_date= date('Y-m-d', strtotime($this->input->post('endDate')));

        $records = $this->hrm_attendance_model->get_attendance_log($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $empid, $department_id, $start_date, $end_date);    
        
        $data = [];
        foreach($records as $value) {

            if(!empty($value->employee_id)) {
                $emp_name = $this->employees_model->get_emp_name($value->employee_id);
                $emp_full_name = $emp_name->first_name.' '.$emp_name->last_name . '-';
            } else {
                $emp_full_name ='';
            }
            $shift = $this->shift_model->read_shift_master($value->shift_id);

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $emp_full_name;
            $nestedData[] = date('d M, Y', strtotime($value->roster_date));
            $nestedData[] = $shift->name;
            $nestedData[] = $value->entry_roster_time;
            $nestedData[] = $value->exit_roster_time;
            $nestedData[] = $value->entry_time;
            $nestedData[] = $value->exit_time;
            $nestedData[] = $value->present_status;
            $nestedData[] = $value->leave_status;
            
			$data[] = $nestedData;
		}
        $all_users = $this->hrm_attendance_model->total_attendance_log($sSearch, $empid, $department_id, $start_date, $end_date);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function punch_history()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('punch_history')]];
        $meta = ['page_title' => lang('punch_history'), 'bc' => $bc];
		$this->page_construct('hrm_attendance/punch_history_list', $meta, $this->data);
    } 
    public function getPunchHistory()
    {
        
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        $empid=$this->input->post('empid');
		$start_date= date('Y-m-d', strtotime($this->input->post('startDate')));
		$end_date= date('Y-m-d', strtotime($this->input->post('endDate')));

        $records = $this->hrm_attendance_model->get_punch_history($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $empid, $start_date, $end_date);    
        
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->device;
            $nestedData[] = $value->card_no;
            $nestedData[] = date('d M, Y h:i a', strtotime($value->log_day_time));
            
			$data[] = $nestedData;
		}
        $all_users = $this->hrm_attendance_model->total_punch_history($sSearch, $empid, $start_date, $end_date);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_payroll extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('employees_model');
        $this->load->admin_model('payroll_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('hrm_payroll_model');
        $this->load->admin_model('designation_model');
        $this->load->admin_model('department_model');
        $this->load->admin_model('shift_model');
        $this->load->library('form_validation');
    }

    public function add_deduct_list()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('roster')]];
        $meta = ['page_title' => lang('roster'), 'bc' => $bc];
        $this->page_construct('hrm_payroll/add_deduct_list', $meta, $this->data);
    }

    public function getAddDeduct()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $records = $this->hrm_payroll_model->getAddDeduct($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $value->allowance_type;
            $nestedData[] = $value->percentage_of_basic;
            $nestedData[] = $value->max_limit;
            $nestedData[] = $value->addition_deduction;

                $edit = '';
                $edit .= " <a href='" . admin_url('hrm_payroll/edit_add_deduct_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_add_deduct') . "'><i class=\"fa fa-edit\"></i></a>";
                if($value->is_default != 1) {
                    $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_add_deduct') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payroll/delete_add_deduct/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                }
                $nestedData[] = $edit;
                $data[] = $nestedData;
        }
        $all_users = $this->hrm_payroll_model->total_AddDeduct($sSearch);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
    }
    public function delete_add_deduct($id='')
    {
        if($this->hrm_payroll_model->delete_add_deduct($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('add_deduct_deleted')]);
        }
    }
    public function edit_add_deduct_form($id='')
    {
        $this->data['add_deduct'] = $this->hrm_payroll_model->get_add_deduct_byId($id);

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/edit_add_deduct_form', $this->data);
    }
    public function add_add_deduct_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/add_add_deduct_form', $this->data);
    }
    
    public function add_addition_deduct()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'addition_deduction' => $this->input->post('addition_deduction'),
                'allowance_type' => $this->input->post('allowance_type'),
                'percentage_of_basic' => $this->input->post('percentage_of_basic'),
                'max_limit' => $this->input->post('max_limit'),
                'pay_nature'=> 'Common',
                'active_status' => 'Active',
                'is_default' => 0,
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->hrm_payroll_model->addAdditionDeduct($data);
       }

    }
    public function add_addition_default()
    {
            $data = array(
                array(
                    'caption' => 'الأساسي',
                    'caption_alt' => 'Basic',
                    'addition_deduction' => 'Addition',
                    'allowance_type' => 'Percent',
                    'percentage_of_basic' => 50.0,
                    'max_limit' => 0.0,
                    'pay_nature' => 'Basic',
                    'active_status' => 'Active',
                    'is_default' => 1
                ),
                array(
                    'caption' => 'إضافة أخرى',
                    'caption_alt' => 'Other Addition',
                    'addition_deduction' => 'Addition',
                    'allowance_type' => 'Fixed',
                    'percentage_of_basic' => 0.0,
                    'max_limit' => 0.0,
                    'pay_nature' => 'OA',
                    'active_status' => 'Active',
                    'is_default' => 1
                ),
                array(
                    'caption' => 'متأخر، بعد فوات الوقت',
                    'caption_alt' => 'Overtime',
                    'addition_deduction' => 'Addition',
                    'allowance_type' => 'Fixed',
                    'percentage_of_basic' => 0.0,
                    'max_limit' => 0.0,
                    'pay_nature' => 'Overtime',
                    'active_status' => 'Active',
                    'is_default' => 1
                ),
                
                array(
                    'caption' => 'صندوق التوفير او الادخار',
                    'caption_alt' => 'Provident Fund',
                    'addition_deduction' => 'Deduction',
                    'allowance_type' => 'Percent',
                    'percentage_of_basic' => 0.0,
                    'max_limit' => 0.0,
                    'pay_nature' => 'Refundable',
                    'active_status' => 'Active',
                    'is_default' => 1
                ),
                array(
                    'caption' => 'خصم الحضور',
                    'caption_alt' => 'Attendance Deduction',
                    'addition_deduction' => 'Deduction',
                    'allowance_type' => 'Fixed',
                    'percentage_of_basic' => 0.0,
                    'max_limit' => 0.0,
                    'pay_nature' => 'AtnDeduct',
                    'active_status' => 'Active',
                    'is_default' => 1
                ),
                array(
                    'caption' => 'ضريبة الدخل',
                    'caption_alt' => 'Income Tax',
                    'addition_deduction' => 'Deduction',
                    'allowance_type' => 'Fixed',
                    'percentage_of_basic' => 0.0,
                    'max_limit' => 0.0,
                    'pay_nature' => 'Tax',
                    'active_status' => 'Active',
                    'is_default' => 1
                ),
                array(
                    'caption' => 'خصم أخرى',
                    'caption_alt' => 'Other Deduction',
                    'addition_deduction' => 'Deduction',
                    'allowance_type' => 'Fixed',
                    'percentage_of_basic' => 0.0,
                    'max_limit' => 0.0,
                    'pay_nature' => 'OD',
                    'active_status' => 'Active',
                    'is_default' => 1
                ),
            );

            foreach($data as $row) {
                
                $addition_deduction = $row['addition_deduction'];
                $pay_nature = $row['pay_nature'];
                $is_default = $row['is_default'];

                $findAddition = $this->hrm_payroll_model->findAddition($addition_deduction, $pay_nature, $is_default);

                if(empty($findAddition)) {
                    $this->hrm_payroll_model->addAdditionDeduct($row);
                }
            }

    } 
    
    public function edit_addition_deduct()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

            $id = $this->input->post('add_deduct_id');
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'addition_deduction' => $this->input->post('addition_deduction'),
                'allowance_type' => $this->input->post('allowance_type'),
                'percentage_of_basic' => $this->input->post('percentage_of_basic'),
                'max_limit' => $this->input->post('max_limit'),
                'pay_nature'=> 'Common',
                'active_status' => 'Active',
                'is_default' => 0,
                'remark' => $this->input->post('remark'),
            );
            $this->hrm_payroll_model->edit_addition_deduct($id, $data);
       }

    }
    public function income_tax()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('income_tax_list')]];
        $meta = ['page_title' => lang('roster'), 'bc' => $bc];
        $this->page_construct('hrm_payroll/income_tax_list', $meta, $this->data);
    }
    public function getIncomeTax()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $records = $this->hrm_payroll_model->getIncomeTax($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;

            $edit = '';
            $edit .= " <a href='" . admin_url('hrm_payroll/edit_income_tax_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_income_tax') . "'><i class=\"fa fa-edit\"></i></a>";
            
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_income_tax') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payroll/delete_income_tax/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
            $nestedData[] = $edit;
            $data[] = $nestedData;
        }
        $all_users = $this->hrm_payroll_model->total_IncomeTax($sSearch);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
    }
    public function delete_income_tax($id='')
    {
        if($this->hrm_payroll_model->delete_income_tax($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('income_tax_deleted')]);
        }
    }
    public function add_income_tax_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/add_income_tax_form', $this->data);
    }
    public function add_income_tax()
    {
        // print_r($this->input->post());die;
       if($this->input->post()) {
        
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $income_tax_id = $this->hrm_payroll_model->add_income_tax($data);

            for($i = 0; $i < count($_POST['row_no']); $i++) {

                $is_close_i =  $_POST['is_close'];
                if(isset($is_close_i) && $i == count($_POST['row_no'])-1) {
                    $is_close = 0;
                } else {
                    $is_close = 1;
                }
                $row_data = array(
                    'tax_setup_id' => $income_tax_id,
                    'start_amount' => $_POST['start_amount'][$i],
                    'end_amount' => $_POST['end_amount'][$i],
                    'tax_percent' => $_POST['tax_percent'][$i],
                    'is_open' => 1,
                    'is_close' => $is_close,
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d')
                );
                $this->hrm_payroll_model->add_income_tax_dtl($row_data);
            }
       }

    }

    public function edit_income_tax_form($id='')
    {
        $this->data['income_tax'] = $this->hrm_payroll_model->get_income_tax_byId($id);
        $this->data['income_tax_dtls'] = $this->hrm_payroll_model->get_income_tax_dtls($id);

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/edit_income_tax_form', $this->data);
    }

    public function edit_income_tax()
    {
       if($this->input->post()) {

            $income_tax_id = $this->input->post('income_tax_id');
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $update = $this->hrm_payroll_model->edit_income_tax($data, $income_tax_id);

            if($update) {

                $this->db->where('tax_setup_id', $income_tax_id)->delete('hrm_income_tax_setup_dtl'); 

                for($i = 0; $i < count($_POST['row_no']); $i++) {

                    $is_close_i =  $_POST['is_close'];
                    if(isset($is_close_i) && $i == count($_POST['row_no'])-1) {
                        $is_close = 0;
                    } else {
                        $is_close = 1;
                    }
                    $row_data = array(
                        'tax_setup_id' => $income_tax_id,
                        'start_amount' => $_POST['start_amount'][$i],
                        'end_amount' => $_POST['end_amount'][$i],
                        'tax_percent' => $_POST['tax_percent'][$i],
                        'is_open' => 1,
                        'is_close' => $is_close,
                        'created_by' => $this->session->userdata('user_id'),
                        'created_at' => date('Y-m-d')
                    );
                    $this->hrm_payroll_model->add_income_tax_dtl($row_data);

                }
            }
       }

    }

    public function payroll_policy()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_policy_list')]];
        $meta = ['page_title' => lang('roster'), 'bc' => $bc];
        $this->page_construct('hrm_payroll/payroll_policy_list', $meta, $this->data);
    }
    public function getPayrollPolicy()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $records = $this->hrm_payroll_model->getPayrollPolicy($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $value->quantity;

            $edit = '';
            $edit .= " <a href='" . admin_url('hrm_payroll/edit_payroll_policy_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_payroll_policy') . "'><i class=\"fa fa-edit\"></i></a>";
            
            if($value->is_default == 0) {
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_payroll_policy') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payroll/delete_payroll_policy/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
            }
            $nestedData[] = $edit;
            $data[] = $nestedData;
        }
        $all_users = $this->hrm_payroll_model->total_IncomeTax($sSearch);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
    }
    public function delete_payroll_policy($id='')
    {
        if($this->hrm_payroll_model->delete_payroll_policy($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('payroll_policy_deleted')]);
        }
    }

    public function add_payroll_policy_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/add_payroll_policy_form', $this->data);
    }

    public function add_default_payroll_policy()
    {
            $data = array(
                array(
                    'caption' => 'إجازة غير مصرح بها',
                    'caption_alt' => 'Unauthorized Leave',
                    'salary_status' => 'Unauthorized_Leave',
                    'quantity' => 1,
                    'active_status' => 'Active',
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d'),
                    'is_default' => 1
                ),
                array(
                    'caption' => 'الراحل الحاضر',
                    'caption_alt' => 'Late Present',
                    'salary_status' => 'LatePresent',
                    'quantity' => 3,
                    'active_status' => 'Active',
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d'),
                    'is_default' => 1
                ),
                array(
                    'caption' => 'إجازة لنصف يوم',
                    'caption_alt' => 'Half Day Leave',
                    'salary_status' => 'Half_Day_Leave',
                    'quantity' => 2,
                    'active_status' => 'Active',
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d'),
                    'is_default' => 1
                ),
                array(
                    'caption' => 'اترك المبكر',
                    'caption_alt' => 'Early Leave',
                    'salary_status' => 'EarlyLeave',
                    'quantity' => 0,
                    'active_status' => 'Active',
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d'),
                    'is_default' => 1
                ),
                array(
                    'caption' => 'أيام الراتب',
                    'caption_alt' => 'Salary Days',
                    'salary_status' => 'Salary_Days',
                    'quantity' => 30,
                    'active_status' => 'Active',
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d'),
                    'is_default' => 1
                ),
            );

            foreach($data as $row) {
                
                $salary_status = $row['salary_status'];
                
                $find_payroll_policy = $this->hrm_payroll_model->find_payroll_policy($salary_status);

                if(empty($find_payroll_policy)) {
                    $this->hrm_payroll_model->add_payroll_policy($row);
                }
            }
    }
    
    public function add_payroll_policy()
    {
        $data = array(
            'caption' => $this->input->post('caption'),
            'caption_alt' => $this->input->post('caption_alt'),
            'salary_status' => $this->input->post('salary_status'),
            'quantity' => $this->input->post('quantity'),
            'active_status' => 'Active',
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        $this->hrm_payroll_model->add_payroll_policy($data);
    }
    public function edit_payroll_policy_form($id='')
    {
        $this->data['payroll_policy'] = $this->hrm_payroll_model->get_payroll_policy_byId($id);
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/edit_payroll_policy_form', $this->data);
    }
    public function edit_payroll_policy()
    {
        $payroll_policy_id = $this->input->post('payroll_policy_id');
        $data = array(
            'caption' => $this->input->post('caption'),
            'caption_alt' => $this->input->post('caption_alt'),
            'salary_status' => $this->input->post('salary_status'),
            'quantity' => $this->input->post('quantity'),
            'active_status' => 'Active',
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        $this->hrm_payroll_model->edit_payroll_policy($data, $payroll_policy_id);
    }

    public function pay_grade()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('pay_grade_list')]];
        $meta = ['page_title' => lang('pay_grade'), 'bc' => $bc];
        $this->page_construct('hrm_payroll/pay_grade_list', $meta, $this->data);
    }

    public function getPayGrade()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $records = $this->hrm_payroll_model->getPayGrade($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $value->payment_frequency_type;
            $nestedData[] = $value->gross_salary;
            $nestedData[] = $value->basic_percent;
            $nestedData[] = $value->basic_salary;
            $nestedData[] = $value->hourly_rate;

            $edit = '';
            if($value->payment_frequency_type == 'Monthly') {
                $edit .= " <a href='" . admin_url('hrm_payroll/edit_pay_grade_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_pay_grade') . "'><i class=\"fa fa-edit\"></i></a>";
            } else {
                $edit .= " <a href='" . admin_url('hrm_payroll/edit_pay_grade_hourly_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_pay_grade') . "'><i class=\"fa fa-edit\"></i></a>";
            }
            
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_pay_grade') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payroll/delete_pay_grade/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";

            $nestedData[] = $edit;
            $data[] = $nestedData;
        }
        $all_users = $this->hrm_payroll_model->total_PayGrade($sSearch);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
    }
    public function delete_pay_grade($id='')
    {
        if($this->hrm_payroll_model->delete_pay_grade($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('pay_grade_deleted')]);
        }
    }
    
    public function add_pay_grade_form($id='')
    {
        $this->data['error'] = '';
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/add_pay_grade_form', $this->data);
    }
    public function add_pay_grade_hourly_form($id='')
    {
        $this->data['error'] = '';
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/add_pay_grade_hourly_form', $this->data);
    }
    public function edit_pay_grade_form($id)
    {
        $this->data['pay_grade'] = $this->hrm_payroll_model->get_pay_grade_byId($id);
        
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/edit_pay_grade_form', $this->data);
    }
    public function edit_pay_grade_hourly_form($id)
    {
        $this->data['pay_grade'] = $this->hrm_payroll_model->get_pay_grade_byId($id);
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/edit_pay_grade_hourly_form', $this->data);
    }
    public function add_pay_grade()
    {
        $payment_frequency_type = $this->input->post('payment_frequency_type');
        if($payment_frequency_type == 'hourly') {
            $payment_frequency_type = 'Hourly';
        } else {
            $payment_frequency_type = 'Monthly';
        }
        $data = array(
            'caption' => $this->input->post('caption'),
            'caption_alt' => $this->input->post('caption_alt'),
            'payment_frequency_type' => $payment_frequency_type,
            'hourly_rate' => $this->input->post('hourly_rate'),
            'gross_salary' => $this->input->post('gross_salary'),
            'basic_percent' => $this->input->post('basic_percent'),
            'basic_salary' => $this->input->post('basic_salary'),
            'overtime_rate' => $this->input->post('overtime_rate'),
            'remark' => $this->input->post('remark'),
            'active_status' => 'Active',
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        $this->hrm_payroll_model->add_pay_grade($data);
    }
    
    public function edit_pay_grade()
    {
        $pay_grade_id = $this->input->post('pay_grade_id');
        $payment_frequency_type = $this->input->post('payment_frequency_type');
        if($payment_frequency_type == 'hourly') {
            $payment_frequency_type = 'Hourly';
        } else {
            $payment_frequency_type = 'Monthly';
        }
        $data = array(
            'caption' => $this->input->post('caption'),
            'caption_alt' => $this->input->post('caption_alt'),
            'payment_frequency_type' => $payment_frequency_type,
            'hourly_rate' => $this->input->post('hourly_rate'),
            'gross_salary' => $this->input->post('gross_salary'),
            'basic_percent' => $this->input->post('basic_percent'),
            'basic_salary' => $this->input->post('basic_salary'),
            'overtime_rate' => $this->input->post('overtime_rate'),
            'remark' => $this->input->post('remark'),
            'active_status' => 'Active',
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        $this->hrm_payroll_model->edit_pay_grade($data, $pay_grade_id);
    }

    public function payroll_setup()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_setup_list')]];
        $meta = ['page_title' => lang('payroll_setup'), 'bc' => $bc];
        $this->page_construct('hrm_payroll/payroll_setup_list', $meta, $this->data);
    }
    public function get_payroll_setup()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $records = $this->hrm_payroll_model->get_payroll_setup($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
            $dept = $this->shift_model->read_department_information($value->department_id);
            $pay_grade = $this->db->get_where('hrm_pay_grade', ['id' => $value->pay_grade_id])->row();

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $dept->department_name;
            $nestedData[] = $emp_fullname;
            $nestedData[] = $value->payment_frequency_type;
            $nestedData[] = $pay_grade ? $pay_grade->caption : '';
            $nestedData[] = $value->hourly_rate;
            $nestedData[] = $value->gross_salary;
            $nestedData[] = $value->basic_salary;
            $nestedData[] = $value->tax_effect == 1 ? 'Yes' : 'No' ;
            $nestedData[] = $value->tax_amount;


            $edit = '';
            $edit .= " <a href='" . admin_url('hrm_payroll/edit_payroll_setup_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_payroll_setup') . "'><i class=\"fa fa-edit\"></i></a>";
            
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_payroll_setup') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payroll/delete_payroll_setup/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";

            $nestedData[] = $edit;
            $data[] = $nestedData;
        }
        $all_users = $this->hrm_payroll_model->total_payroll_setup($sSearch);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
    }
    public function delete_payroll_setup($id='')
    {
        if($this->hrm_payroll_model->delete_payroll_setup($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('payroll_setup_deleted')]);
        }
    }
    
    public function add_payroll_setup_form()
    {
        $this->data['all_pay_grade'] = $this->hrm_payroll_model->get_all_pay_grade();
        $this->data['all_bank_accounts'] = $this->hrm_payroll_model->all_bank_accounts();
        $this->data['payroll_bonuses'] = $this->hrm_payroll_model->payroll_bonuses();
        $this->data['all_income_tax'] = $this->hrm_payroll_model->all_income_tax();
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/add_payroll_setup_form', $this->data);
    }
    
    public function add_payroll_setup()
    {
        $payment_frequency_type = $this->input->post('payment_frequency_type');
        $emp_id = explode('-', $this->input->post('employee'));
        $emp_id = $emp_id[0];
        $department_id = explode('-', $this->input->post('department'));
        $department_id = $department_id[0];
        $tax_amount = $this->input->post('tax_amount');
        $tax_effect = $this->input->post('tax_effect');
        if(isset($tax_effect)) {
            $tax_effect = 1;
        }

        if($payment_frequency_type == 'Hourly') {
            $payment_frequency_type = 'Hourly';
            $gross_salary = 0.0;
            $basic_salary = 0.0;
            $basic_percent = 0.0;
            $tax_effect = 0;
            $pf_effect = 0;
            $tax_amount = 0.0;
        } else {
            $payment_frequency_type = 'Monthly';
            $gross_salary = $this->input->post('gross_salary');
            $basic_salary = $this->input->post('basic_salary');
            $basic_percent = $this->input->post('basic_percent');
            $net_salary = $this->input->post('net_salary');
            
        }
        $bonus_eligibile = $this->input->post('bonus_eligibile');
        if(!empty($bonus_eligibile)) {
            $bonus_eligibile_ids = implode(',', $bonus_eligibile);
        }

        $data = array(
            'caption' => 'Dummy',
            'caption_alt' => 'Dummy',
            'employee_id' => $emp_id,
            'department_id' => $department_id,
            'pay_grade_id' => $this->input->post('pay_grade_id'),
            'payment_frequency_type' => $payment_frequency_type,
            'payment_type' =>  $this->input->post('payment_type'),
            'hourly_rate' => $this->input->post('hourly_rate'),
            'gross_salary' => $gross_salary,
            'basic_percent' => $basic_percent,
            'basic_salary' => $basic_salary,
            'net_salary' => $this->input->post('net_salary'),
            'overtime_rate' => $this->input->post('overtime_rate'),

            'tax_amount' => $tax_amount,
            'tax_effect' => $tax_effect,
            'income_tax_id' => $this->input->post('income_tax_id'),
            'account_id' => $this->input->post('account_id'),
            'iban_number' => $this->input->post('iban_number'),
            'account_number' => $this->input->post('account_number'),
            'bonus_eligibile' => $bonus_eligibile_ids,
            'remark' => $this->input->post('remark'),
            'active_status' => 'Active',
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        // echo '<pre>'; print_r($this->input->post()); die;
        $insert_id = $this->hrm_payroll_model->add_payroll_setup($data);
        // die;
        if($insert_id) {

            for($i=0; $i < count($_POST['row_no']); $i++) {
                
                if($_POST['pay_nature'][$i] == 'Refundable') {
                    $pf_amount = $_POST['actual_amount'][$i];
                }
                $data_dtl = array(
                    'hr_prl_emp_mst_id' => $insert_id,
                    'payroll_head_id' => $_POST['payroll_head_id'][$i],
                    'addition_deduction' => $_POST['addition_deduction'][$i],
                    'allowance_type' => $_POST['allowance_type'][$i],
                    'pay_nature' => $_POST['pay_nature'][$i],
                    'actual_amt' => $_POST['actual_amount'][$i],
                );
                $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtl);
            }

                $tax_head = $this->hrm_payroll_model->findByNature('Tax');
                $data_dtlTax = array(
                    'hr_prl_emp_mst_id' => $insert_id,
                    'payroll_head_id' => $tax_head->id,
                    'addition_deduction' => 'Deduction',
                    'allowance_type' => 'Fixed',
                    'pay_nature' => 'Tax',
                    'actual_amt' => $this->input->post('tax_amount'),
                );
                $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtlTax);

                if($payment_frequency_type == 'Monthly') {
                    $basic_head = $this->hrm_payroll_model->findByNature('Basic');
                    $data_dtl_basic = array(
                        'hr_prl_emp_mst_id' => $insert_id,
                        'payroll_head_id' => $basic_head->id,
                        'addition_deduction' => $basic_head->addition_deduction,
                        'allowance_type' => $basic_head->allowance_type,
                        'pay_nature' => $basic_head->pay_nature,
                        'actual_amt' => $this->input->post('basic_salary'),
                        'max_limit' => $basic_head->max_limit,
                    );
                    $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtl_basic);
                    $atnDeduct_head = $this->hrm_payroll_model->findByNature('AtnDeduct');
                    $data_dtl_atnDeduct = array(
                        'hr_prl_emp_mst_id' => $insert_id,
                        'payroll_head_id' => $atnDeduct_head->id,
                        'addition_deduction' => $atnDeduct_head->addition_deduction,
                        'allowance_type' => $atnDeduct_head->allowance_type,
                        'pay_nature' => $atnDeduct_head->pay_nature,
                        'actual_amt' => $atnDeduct_head->percentage_of_basic,
                        'max_limit' => $atnDeduct_head->max_limit,
                    );
                    $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtl_atnDeduct);
                }
        }
        if(!empty($pf_amount)) {
            $data_pf_amount_update = array('pf_amount' => $pf_amount);
            $this->hrm_payroll_model->edit_payroll_setup($data_pf_amount_update, $insert_id);
        }
    }

    public function edit_payroll_setup_form($id='')
    {
        $this->data['payroll_setup'] = $this->hrm_payroll_model->get_payroll_setup_byId($id);
        $this->data['payroll_dtls'] = $this->hrm_payroll_model->get_payroll_dtl($id);
        $this->data['all_pay_grade'] = $this->hrm_payroll_model->get_all_pay_grade();
        $this->data['all_bank_accounts'] = $this->hrm_payroll_model->all_bank_accounts();
        $this->data['all_income_tax'] = $this->hrm_payroll_model->all_income_tax();
        $this->data['payroll_bonuses'] = $this->hrm_payroll_model->payroll_bonuses();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/edit_payroll_setup_form', $this->data);
    }
    public function edit_payroll_setup()
    {
        $payroll_setup_id = $this->input->post('payroll_setup_id');
        $pay_grade_id_prev = $this->hrm_payroll_model->get_payroll_setup_byId($payroll_setup_id)->pay_grade_id;

        $payment_frequency_type = $this->input->post('payment_frequency_type');
        $emp_id = explode('-', $this->input->post('employee'));
        $emp_id = $emp_id[0];
        $department_id = explode('-', $this->input->post('department'));
        $department_id = $department_id[0];
        $tax_amount = $this->input->post('tax_amount');
        $tax_effect = $this->input->post('tax_effect');
        if(isset($tax_effect)) {
            $tax_effect = 1;
        }

        if($payment_frequency_type == 'Hourly') {
            $payment_frequency_type = 'Hourly';
            $gross_salary = 0.0;
            $basic_salary = 0.0;
            $basic_percent = 0.0;
            $tax_effect = 0;
            $pf_effect = 0;
            $tax_amount = 0.0;
        } else {
            $payment_frequency_type = 'Monthly';
            $gross_salary = $this->input->post('gross_salary');
            $basic_salary = $this->input->post('basic_salary');
            $basic_percent = $this->input->post('basic_percent');
            $net_salary = $this->input->post('net_salary');
            
        }
        $pay_grade_id = $this->input->post('pay_grade_id');
        $bonus_eligibile = $this->input->post('bonus_eligibile');
        if(!empty($bonus_eligibile)) {
            $bonus_eligibile_ids = implode(',', $bonus_eligibile);
        }

        $data = array(
            'caption' => 'Dummy',
            'caption_alt' => 'Dummy',
            'employee_id' => $emp_id,
            'department_id' => $department_id,
            'pay_grade_id' => $pay_grade_id,
            'payment_frequency_type' => $payment_frequency_type,
            'payment_type' =>  $this->input->post('payment_type'),
            'hourly_rate' => $this->input->post('hourly_rate'),
            'gross_salary' => $gross_salary,
            'basic_percent' => $basic_percent,
            'basic_salary' => $basic_salary,
            'net_salary' => $this->input->post('net_salary'),
            'overtime_rate' => $this->input->post('overtime_rate'),
            
            'tax_amount' => $tax_amount,
            'tax_effect' => $tax_effect,
            'income_tax_id' => $this->input->post('income_tax_id'),
            'account_id' => $this->input->post('account_id'),
            'iban_number' => $this->input->post('iban_number'),
            'account_number' => $this->input->post('account_number'),
            'bonus_eligibile' => $bonus_eligibile_ids,
            'remark' => $this->input->post('remark'),
            'active_status' => 'Active',
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        // echo '<pre>'; print_r($this->input->post()); die;
        $update = $this->hrm_payroll_model->edit_payroll_setup($data, $payroll_setup_id);
        // die;
        if($update) {

            if($pay_grade_id != $pay_grade_id_prev) {
                $this->hrm_payroll_model->delete_payroll_dtls($payroll_setup_id);
            }

            for($i=0; $i < count($_POST['row_no']); $i++) {
                
                $payroll_setup_dtl_id = $_POST['payroll_setup_dtl_id'][$i];
                if($_POST['pay_nature'][$i] == 'Refundable') {
                    $pf_amount = $_POST['actual_amount'][$i];
                }
                $data_dtl = array(
                    'hr_prl_emp_mst_id' => $payroll_setup_id,
                    'payroll_head_id' => $_POST['payroll_head_id'][$i],
                    'addition_deduction' => $_POST['addition_deduction'][$i],
                    'allowance_type' => $_POST['allowance_type'][$i],
                    'pay_nature' => $_POST['pay_nature'][$i],
                    'actual_amt' => $_POST['actual_amount'][$i],
                );
                if($pay_grade_id != $pay_grade_id_prev) {
                    $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtl);
                } else {
                    $this->hrm_payroll_model->edit_payroll_setup_dtl($data_dtl, $payroll_setup_dtl_id);
                }
            }

            if($pay_grade_id != $pay_grade_id_prev) {
                $tax_head = $this->hrm_payroll_model->findByNature('Tax');
                $data_dtlTax = array(
                    'hr_prl_emp_mst_id' => $payroll_setup_id,
                    'payroll_head_id' => $tax_head->id,
                    'addition_deduction' => 'Deduction',
                    'allowance_type' => 'Fixed',
                    'pay_nature' => 'Tax',
                    'actual_amt' => $this->input->post('tax_amount'),
                );
                $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtlTax);

                if($payment_frequency_type == 'Monthly') {
                    $basic_head = $this->hrm_payroll_model->findByNature('Basic');
                    $data_dtl_basic = array(
                        'hr_prl_emp_mst_id' => $payroll_setup_id,
                        'payroll_head_id' => $basic_head->id,
                        'addition_deduction' => $basic_head->addition_deduction,
                        'allowance_type' => $basic_head->allowance_type,
                        'pay_nature' => $basic_head->pay_nature,
                        'actual_amt' => $this->input->post('basic_salary'),
                        'max_limit' => $basic_head->max_limit,
                    );
                    $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtl_basic);
                    $atnDeduct_head = $this->hrm_payroll_model->findByNature('AtnDeduct');
                    $data_dtl_atnDeduct = array(
                        'hr_prl_emp_mst_id' => $payroll_setup_id,
                        'payroll_head_id' => $atnDeduct_head->id,
                        'addition_deduction' => $atnDeduct_head->addition_deduction,
                        'allowance_type' => $atnDeduct_head->allowance_type,
                        'pay_nature' => $atnDeduct_head->pay_nature,
                        'actual_amt' => $atnDeduct_head->percentage_of_basic,
                        'max_limit' => $atnDeduct_head->max_limit,
                    );
                    $this->hrm_payroll_model->add_payroll_setup_dtl($data_dtl_atnDeduct);
                }
            }
        }
        if(!empty($pf_amount)) {
            $data_pf_amount_update = array('pf_amount' => $pf_amount);
            $this->hrm_payroll_model->edit_payroll_setup($data_pf_amount_update, $payroll_setup_id);
        }
    }

    public function changePaymentFrequencyType()
    {

        $paymentFrequencyType = $this->input->post('paymentFrequencyType');

        $results = $this->hrm_payroll_model->paymentFrequencyType($paymentFrequencyType); 
        $dataReturn = array();
        foreach($results as $value) {

            $data = array(
                'id' => $value->id,
                'value' => $value->caption
            );
            $dataReturn[] = $data;
        }

        $this->sma->send_json(array('dataReturn' => $dataReturn));
    }
    public function getPayGradeDetail()
    {

        $pay_grade_id = $this->input->post('payGradeId');
        $pay_grade = $this->hrm_payroll_model->get_pay_grade_byId($pay_grade_id);
        $results = $this->hrm_payroll_model->get_hrm_add_deduct();

        $data_return = array();
        foreach($results as $row) {

            if($row->allowance_type == 'Percent') {
                $actual_amount = $row->percentage_of_basic ? (($pay_grade->basic_salary * $row->percentage_of_basic) / 100) : 0.0;
            } else {
                $actual_amount = 0.0;
            }
            $data = array();
            if ($row->pay_nature != 'Tax') {

                $data['payroll_head_id'] = $row->id;
                $data['payroll_head_text'] = $row->caption;
                $data['addition_deduction'] = $row->addition_deduction;
                $data['pay_nature'] = $row->pay_nature;
                $data['percentage_of_basic'] = $row->percentage_of_basic;
                $data['actual_amount'] = $actual_amount;
                $data['allowance_type'] = $row->allowance_type;
                $data['max_limit'] = $row->max_limit;

                $data_return[] = $data;
            }
        }
        // echo '<pre>';print_r($data_return); die;
        $this->sma->send_json(array('payGradeDetail' => $data_return, 'payGrade' => $pay_grade));
    }

    public function payroll_salary_mst()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_salary_mst')]];
        $meta = ['page_title' => lang('payroll_salary_mst'), 'bc' => $bc];
        $this->page_construct('hrm_payroll/payroll_salary_mst', $meta, $this->data);
    }

    public function get_payroll_salary_mst()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $records = $this->hrm_payroll_model->get_payroll_salary_mst($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
            $dept = $this->shift_model->read_department_information($value->department_id);
            $month_arr = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            $pay_grade = $this->db->get_where('hrm_pay_grade', ['id' => $value->pay_grade_id])->row();
            $account = $this->db->get_where('hrm_bank_account', ['id' => $value->account_id])->row();
            $user = $this->db->get_where('users', ['id' => $value->approved_by])->row();

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $month_arr[$value->salary_month];
            $nestedData[] = $value->salary_year;
            $nestedData[] = $emp_fullname;
            $nestedData[] = $pay_grade ? $pay_grade->caption : '';
            $nestedData[] = $value->basic_salary;
            
            $nestedData[] = $value->gross_salary;
            $nestedData[] = $value->net_salary;
            $nestedData[] = $value->tax_amount;
            $nestedData[] = $value->payment_type;
            $nestedData[] = $account ? $account->caption : '';
            $nestedData[] = $value->account_number;
            $nestedData[] = $user ? $user->first_name . ' ' . $user->last_name : '';
            $nestedData[] = $value->voucher_id;
            
            $edit = '';
            //$edit .= " <a href='" . admin_url('hrm_payroll/edit_payroll_salary_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_payroll_salary') . "'><i class=\"fa fa-edit\"></i></a>";
            
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_payroll_salary') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payroll/delete_payroll_salary/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";

            $nestedData[] = $edit;
            $data[] = $nestedData;
        }
        $all_users = $this->hrm_payroll_model->total_payroll_salary_mst($sSearch);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
    }
    public function delete_payroll_salary($id='')
    {
        if($this->hrm_payroll_model->delete_payroll_salary($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('payroll_salary_deleted')]);
        }
    }
    public function add_payroll_salary_form()
    {
        $this->data['all_pay_grade'] = $this->hrm_payroll_model->get_all_pay_grade();
        $this->data['all_bank_accounts'] = $this->hrm_payroll_model->all_bank_accounts();
        $this->data['payroll_bonuses'] = $this->hrm_payroll_model->payroll_bonuses();
        $this->data['all_income_tax'] = $this->hrm_payroll_model->all_income_tax();
        $this->data['staffs'] = $this->employees_model->get_all_users();
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/add_payroll_salary_form', $this->data);
    }

    public function edit_payroll_salary_form($id='')
    {
        $this->data['payroll_salary'] = $this->hrm_payroll_model->get_payroll_salary_byId($id);
        $this->data['all_pay_grade'] = $this->hrm_payroll_model->get_all_pay_grade();
        $this->data['all_bank_accounts'] = $this->hrm_payroll_model->all_bank_accounts();
        $this->data['payroll_bonuses'] = $this->hrm_payroll_model->payroll_bonuses();
        $this->data['all_income_tax'] = $this->hrm_payroll_model->all_income_tax();
        $this->data['staffs'] = $this->employees_model->get_all_users();
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payroll/edit_payroll_salary_form', $this->data);
    }

    public function add_payroll_emp_salary()
    {
        $emp_id = explode('-', $this->input->post('employee'));
        $emp_id = $emp_id[0];
        $department_id = explode('-', $this->input->post('department'));
        $department_id = $department_id[0];
        $tax_amount = $this->input->post('tax_amount');

        $salary_year_in = $this->input->post('salary_year');
        $salary_year = date('Y', strtotime($salary_year_in));
        $salary_month = date('n', strtotime($salary_year_in));

        $first_day_month = date('Y-m-01', strtotime($salary_year_in));
        $last_day_month = date('Y-m-t', strtotime($salary_year_in));

        $payroll_setup = $this->hrm_payroll_model->get_payroll_setup_row($emp_id);
// echo '<pre>'; print_r($payroll_setup); die;

            $emp_name = $this->employees_model->get_emp_name($emp_id);
            $full_name = $emp_name->first_name.' '.$emp_name->last_name;
            $caption = $full_name.' - '.$salary_month.' - '.$salary_year;
            $data = array(
                'caption' => $caption,
                'employee_id' => $emp_id,
                'department_id' => $department_id,
                'salary_month' => $salary_month,
                'salary_year' => $salary_year,
                'salary_month_int' => $salary_month,
                // 'salary_group_id' => $this->input->post('salary_group_id'),
                'pay_grade_id' => $payroll_setup->pay_grade_id,
                'payment_frequency_type' => $payroll_setup->payment_frequency_type,
                'basic_salary' => $payroll_setup->basic_salary,
                'basic_percent' => $payroll_setup->basic_percent,
                'gross_salary' => $payroll_setup->gross_salary,
                'net_salary' => $payroll_setup->net_salary,
                'tax_amount' => $payroll_setup->tax_amount,
                'tax_effect' => $payroll_setup->tax_effect,
                'overtime_rate' => $payroll_setup->overtime_rate,
                'pf_amount' => $payroll_setup->pf_amount,
                'hourly_rate' => $payroll_setup->hourly_rate,
                'income_tax_id' => $payroll_setup->income_tax_id,
                'payment_type' => $payroll_setup->payment_type,
                'account_id' => $payroll_setup->account_id,
                'account_number' => $payroll_setup->account_number,
                'active_status' => 'Active',
                'remark' => $this->input->post('remark'),
                'checked_by' => $this->input->post('checked_by'),
                'approved_by' => $this->input->post('approved_by'),
                'created_at' => date('Y-m-d'),
                'created_by'   => $this->session->userdata('user_id'),
            );

            $insert_id = $this->hrm_payroll_model->add_payroll_salary($data);

            $payroll_setup_dtls = $this->hrm_payroll_model->get_payroll_setup_dtls($payroll_setup->id);
            // echo '<pre>';print_r($payroll_setup_dtls); die;

            $sqlWhere = "";
            if(!empty($department_id)) {
                $sqlWhere .= " AND roster.department_id=".$department_id;
            }
            if(!empty($emp_id)) {
                $sqlWhere .= " AND roster.employee_id=".$emp_id;
            }
            $sqlQuery = "
                    select roster.employee_id                                                           AS employee_id,
                           sum(CASE WHEN roster.salary_status = 'Unauthorized_Leave' THEN 1 ELSE 0 END) AS unauthorized_leave,
                           sum(CASE WHEN roster.salary_status = 'LatePresent' THEN 1 ELSE 0 END)        AS late_present,
                           sum(CASE WHEN roster.salary_status = 'EarlyLeave' THEN 1 ELSE 0 END)         AS early_leave,
                           sum(CASE WHEN roster.salary_status = 'InLeave' THEN 1 ELSE 0 END)            AS in_leave,
                           sum(CASE WHEN roster.salary_status = 'InTour' THEN 1 ELSE 0 END)             AS in_tour,
                           sum(CASE WHEN roster.salary_status = 'Regular' THEN 1 ELSE 0 END)            AS present,
                           sum(CASE WHEN roster.salary_status = 'Half_Day_Leave' THEN 1 ELSE 0 END)     AS half_day_leave,
                           sum(CASE WHEN roster.salary_status = 'Weekend' THEN 1 ELSE 0 END)            AS weekend,
                           sum(overtime_minute)                                                         AS overtime_minute,
                           sum(work_minute)                                                         AS work_minute
                    FROM sma_hrm_roster_details roster WHERE roster.roster_date >= '". $first_day_month . "' AND roster.roster_date <= '" . $last_day_month . "'" .
                        $sqlWhere .
                    "GROUP BY roster.employee_id
                    order by roster.employee_id"
                    ;
    
            $salary_status = $this->db->query($sqlQuery)->result();
            // print_r($salary_status); die;
            if($payroll_setup->payment_frequency_type == 'Hourly') {

                $gross_salary = 0;
                $net_salary = 0;
                $additive_amount = 0;
                $deductive_amount = 0;

                $total_hours = $salary_status[0]->work_minute / 60;

                $gross_salary = $payroll_setup->hourly_rate * $total_hours;
                $net_salary = $gross_salary;

                $actualAmt = $payroll_setup->income_tax_id ? $this->getIncomeTax2($payroll_setup->income_tax_id, $net_salary) : 0.0;
                // print_r($payroll_setup->hourly_rate); print_r($total_hours); print_r($net_salary); die;
            }
            else {
            $roster_map = array();
            $overtime_map = array();
            foreach($salary_status as $ss) {
                $roster_map['Unauthorized_Leave'] = $ss->unauthorized_leave;
                $roster_map['LatePresent'] = $ss->late_present;
                $roster_map['EarlyLeave'] = $ss->early_leave;
                $roster_map['InLeave'] = $ss->in_leave;
                $roster_map['Half_Day_Leave'] = $ss->half_day_leave;
                $roster_map['Present'] = $ss->present;

                $overtime = $ss->overtime_minute;
            }
    // print_r($roster_map);die;

            $salary_days = 30;
            $payroll_polices = $this->hrm_payroll_model->get_payroll_polices();
            $policy_array = array();
            foreach($payroll_polices as $payroll_policy) {

                if($payroll_policy->salary_status == "Salary_Days") {
                    $salary_days = $payroll_policy->quantity;
                } else {
                    $policy_array[$payroll_policy->salary_status] = $payroll_policy->quantity;
                }
            }
            // echo '<pre>';print_r($roster_map); 
            $gross_salary = 0;
            $net_salary = 0;
            $additive_amount = 0;
            $deductive_amount = 0;
            foreach($payroll_setup_dtls as $row) {

            if ($row->pay_nature != 'Tax') {    
                $data_dtl = array(
                    'salary_mst_id' => $insert_id,
                    'payroll_head_id' => $row->payroll_head_id,
                    'addition_deduction' => $row->addition_deduction,
                    'allowance_type' => $row->allowance_type,
                    'pay_nature' => $row->pay_nature,
                    // 'actual_amt' => $row->actual_amt,
                    'created_at' => date('Y-m-d'),
                    'created_by'   => $this->session->userdata('user_id'),
                );

                if($row->pay_nature == 'AtnDeduct') {
                    $AtnDeduct = $this->getAtnDeduct($roster_map, $policy_array, $salary_days,  $payroll_setup->basic_salary);
                    $data_dtl['actual_amt'] = $AtnDeduct;
                    $deductive_amount = $deductive_amount + $AtnDeduct;
                } else if($row->pay_nature == 'Overtime') {
                    $Overtime = $this->getOvertime($overtime,  $payroll_setup->overtime_rate);
                    $data_dtl['actual_amt'] = $Overtime;
                    $additive_amount = $additive_amount + $Overtime;
                } else {
                    $data_dtl['actual_amt'] = $row->actual_amt;

                    if($row->addition_deduction == 'Addition') {
                        $additive_amount = $additive_amount + $row->actual_amt;
                    } else {
                        $deductive_amount = $deductive_amount + $row->actual_amt;
                    }  
                }
                
                // add salary Detail
                $this->hrm_payroll_model->add_payroll_salaryDtls($data_dtl);
            }
            }
            
            $tax_head = $this->hrm_payroll_model->findByNature('Tax');
            $data_dtlTax = array(
                'salary_mst_id' => $insert_id,
                'payroll_head_id' => $tax_head->id,
                'addition_deduction' => $tax_head->addition_deduction,
                'allowance_type' => $tax_head->allowance_type,
                'pay_nature' => $tax_head->pay_nature,
               
            );

            $gross_salary = $additive_amount;
            $net_salary = $additive_amount - $deductive_amount;
        
            
            $actualAmt = $payroll_setup->income_tax_id ? $this->getIncomeTax2($payroll_setup->income_tax_id, $net_salary) : 0.0;
            $deductive_amount = $deductive_amount + $actualAmt;
            $this->hrm_payroll_model->add_payroll_salaryDtls($data_dtlTax);

        }
            
            
            $salary_mst_data_update = array(
                'tax_amount' => $actualAmt,
                'additive_amount' => $additive_amount,
                'deductive_amount' => $deductive_amount,
                'gross_salary' => $gross_salary,
                'net_salary' => $net_salary,
            );
            $this->hrm_payroll_model->update_payroll_salary($salary_mst_data_update, $insert_id);
        // }
        // echo '<pre>';print_r($payroll_setup_list); die;
    }

    public function edit_payroll_emp_salary()
    {
        $emp_id = explode('-', $this->input->post('employee'));
        $emp_id = $emp_id[0];
        $department_id = explode('-', $this->input->post('department'));
        $department_id = $department_id[0];
        $tax_amount = $this->input->post('tax_amount');

        $salary_year_in = $this->input->post('salary_year');
        $salary_year = date('Y', strtotime($salary_year_in));
        $salary_month = date('n', strtotime($salary_year_in));

        $first_day_month = date('Y-m-01', strtotime($salary_year_in));
        $last_day_month = date('Y-m-t', strtotime($salary_year_in));

        $payroll_setup = $this->hrm_payroll_model->get_payroll_setup_row($emp_id);
// echo '<pre>';print_r($payroll_setup); die;

            $emp_name = $this->employees_model->get_emp_name($emp_id);
            $full_name = $emp_name->first_name.' '.$emp_name->last_name;
            $caption = $full_name.' - '.$salary_month.' - '.$salary_year;

            $payroll_salary_id = $this->input->post('payroll_salary_id');
            $data = array(
                'caption' => $caption,
                'employee_id' => $emp_id,
                'department_id' => $department_id,
                'salary_month' => $salary_month,
                'salary_year' => $salary_year,
                'salary_month_int' => $salary_month,
                // 'salary_group_id' => $this->input->post('salary_group_id'),
                'pay_grade_id' => $this->input->post('pay_grade_id'),
                'payment_frequency_type' => $payroll_setup->payment_frequency_type,
                'basic_salary' => $this->input->post('basic_salary'),
                'basic_percent' => $payroll_setup->basic_percent,
                'gross_salary' => $this->input->post('gross_salary'),
                'net_salary' => $this->input->post('net_salary'),
                'tax_amount' => $this->input->post('tax_amount'),
                'overtime_rate' => $this->input->post('overtime_rate'),
                'pf_amount' => $this->input->post('pf_amount'),
                'hourly_rate' => $this->input->post('hourly_rate'),
                'income_tax_id' => $payroll_setup->income_tax_id,
                'payment_type' => $payroll_setup->payment_type,
                'account_id' => $payroll_setup->account_id,
                'account_number' => $payroll_setup->account_number,
                'remark' => $this->input->post('remark'),
                'checked_by' => $this->input->post('checked_by'),
                'approved_by' => $this->input->post('approved_by'),
                'created_at' => date('Y-m-d'),
                'created_by'   => $this->session->userdata('user_id'),
            );

            $update = $this->hrm_payroll_model->update_payroll_salary($data, $payroll_salary_id);

            $this->hrm_payroll_model->delete_payroll_salary_dtl($payroll_salary_id);

            $payroll_setup_dtls = $this->hrm_payroll_model->get_payroll_setup_dtls($payroll_setup->id);
            // echo '<pre>';print_r($payroll_setup_dtls); die;

            $sqlWhere = "";
            if(!empty($department_id)) {
                $sqlWhere .= " AND roster.department_id=".$department_id;
            }
            if(!empty($emp_id)) {
                $sqlWhere .= " AND roster.employee_id=".$emp_id;
            }
            $sqlQuery = "
                    select roster.employee_id                                                           AS employee_id,
                           sum(CASE WHEN roster.salary_status = 'Unauthorized_Leave' THEN 1 ELSE 0 END) AS unauthorized_leave,
                           sum(CASE WHEN roster.salary_status = 'LatePresent' THEN 1 ELSE 0 END)        AS late_present,
                           sum(CASE WHEN roster.salary_status = 'EarlyLeave' THEN 1 ELSE 0 END)         AS early_leave,
                           sum(CASE WHEN roster.salary_status = 'InLeave' THEN 1 ELSE 0 END)            AS in_leave,
                           sum(CASE WHEN roster.salary_status = 'InTour' THEN 1 ELSE 0 END)             AS in_tour,
                           sum(CASE WHEN roster.salary_status = 'Regular' THEN 1 ELSE 0 END)            AS present,
                           sum(CASE WHEN roster.salary_status = 'Half_Day_Leave' THEN 1 ELSE 0 END)     AS half_day_leave,
                           sum(CASE WHEN roster.salary_status = 'Weekend' THEN 1 ELSE 0 END)            AS weekend,
                           sum(overtime_minute)                                                         AS overtime_minute
                    FROM sma_hrm_roster_details roster WHERE roster.roster_date >= '". $first_day_month . "' AND roster.roster_date <= '" . $last_day_month . "'" .
                        $sqlWhere .
                    "GROUP BY roster.employee_id
                    order by roster.employee_id"
                    ;
    
            $salary_status = $this->db->query($sqlQuery)->result();
    // print_r($salary_status);die;
            $roster_map = array();
            $overtime_map = array();
            if(!empty($salary_status)) {
                foreach($salary_status as $ss) {
                    $roster_map['Unauthorized_Leave'] = $ss->unauthorized_leave;
                    $roster_map['LatePresent'] = $ss->late_present;
                    $roster_map['EarlyLeave'] = $ss->early_leave;
                    $roster_map['InLeave'] = $ss->in_leave;
                    $roster_map['Present'] = $ss->present;
                    $roster_map['Half_Day_Leave'] = $ss->half_day_leave;
                    $roster_map['Present'] = $ss->present;

                    $overtime = $ss->overtime_minute;
                }
            } else {
                    $roster_map['Unauthorized_Leave'] = 0;
                    $roster_map['LatePresent'] = 0;
                    $roster_map['EarlyLeave'] = 0;
                    $roster_map['InLeave'] = 0;
                    $roster_map['Present'] = 0;
                    $roster_map['Half_Day_Leave'] = 0;
                    $roster_map['Present'] = 0;

                    $overtime = 0;
            }

            $salary_days = 30;
            $payroll_polices = $this->hrm_payroll_model->get_payroll_polices();
            $policy_array = array();
            foreach($payroll_polices as $payroll_policy) {

                if($payroll_policy->salary_status == "Salary_Days") {
                    $salary_days = $payroll_policy->quantity;
                } else {
                    $policy_array[$payroll_policy->salary_status] = $payroll_policy->quantity;
                }
            }
            // echo '<pre>';print_r($roster_map); 
            $gross_salary = 0;
            $net_salary = 0;
            $additive_amount = 0;
            $deductive_amount = 0;
            foreach($payroll_setup_dtls as $row) {

            if ($row->pay_nature != 'Tax') {    
                $data_dtl = array(
                    'salary_mst_id' => $payroll_salary_id,
                    'payroll_head_id' => $row->payroll_head_id,
                    'addition_deduction' => $row->addition_deduction,
                    'allowance_type' => $row->allowance_type,
                    'pay_nature' => $row->pay_nature,
                    // 'actual_amt' => $row->actual_amt,
                    'created_at' => date('Y-m-d'),
                    'created_by'   => $this->session->userdata('user_id'),
                );

                if($row->pay_nature == 'AtnDeduct') {
                    $data_dtl['actual_amt'] = $this->getAtnDeduct($roster_map, $policy_array, $salary_days,  $payroll_setup->basic_salary);
                    $deductive_amount = $deductive_amount + $row->actual_amt;
                } else if($row->pay_nature == 'Overtime') {
                    $data_dtl['actual_amt'] = $this->getOvertime($overtime,  $payroll_setup->overtime_rate);
                    $additive_amount = $additive_amount + $row->actual_amt;
                } else {
                    $data_dtl['actual_amt'] = $row->actual_amt;

                    if($row->addition_deduction == 'Addition') {
                        $additive_amount = $additive_amount + $row->actual_amt;
                    } else {
                        $deductive_amount = $deductive_amount + $row->actual_amt;
                    }

                    $gross_salary = $additive_amount;
                    $net_salary = $additive_amount - $deductive_amount;
                    
                }
                // add salary Detail
                $this->hrm_payroll_model->add_payroll_salaryDtls($data_dtl);
            }
            }
            
            $tax_head = $this->hrm_payroll_model->findByNature('Tax');
            $data_dtlTax = array(
                'salary_mst_id' => $payroll_salary_id,
                'payroll_head_id' => $tax_head->id,
                'addition_deduction' => $tax_head->addition_deduction,
                'allowance_type' => $tax_head->allowance_type,
                'pay_nature' => $tax_head->pay_nature,
               
            );
            
            $actualAmt = $payroll_setup->income_tax_id ? $this->getIncomeTax2($payroll_setup->income_tax_id, $net_salary) : 0.0;
            $deductive_amount = $deductive_amount + $actualAmt;
            $this->hrm_payroll_model->add_payroll_salaryDtls($data_dtlTax);
            
            $salary_mst_data_update = array(
                'tax_amount' => $actualAmt,
                'additive_amount' => $additive_amount,
                'deductive_amount' => $deductive_amount,
                'gross_salary' => $gross_salary,
                'net_salary' => $net_salary,
            );
            $this->hrm_payroll_model->update_payroll_salary($salary_mst_data_update, $payroll_salary_id);
        // }
        // echo '<pre>';print_r($payroll_setup_list); die;
    }

    public function add_payroll_emp_salary_batch()
    {
        $emp_id = explode('-', $this->input->post('employee'));
        $emp_id = $emp_id[0];
        $department_id = explode('-', $this->input->post('department'));
        $department_id = $department_id[0];
        $tax_amount = $this->input->post('tax_amount');

        $salary_year_in = $this->input->post('salary_year');
        $salary_year = date('Y', strtotime($salary_year_in));
        $salary_month = date('n', strtotime($salary_year_in));

        $first_day_month = date('Y-m-01', strtotime($salary_year_in));
        $last_day_month = date('Y-m-t', strtotime($salary_year_in));

        $payroll_setup_list = $this->hrm_payroll_model->get_payroll_setup_list($emp_id, $department_id);
// echo '<pre>';print_r($payroll_setup_list); die;
        foreach($payroll_setup_list as $value) {

            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $full_name = $emp_name->first_name.' '.$emp_name->last_name;
            $caption = $full_name.' - '.$salary_month.' - '.$salary_year;
            $data = array(
                'caption' => $caption,
                'employee_id' => $emp_id,
                'department_id' => $department_id,
                'salary_month' => $salary_month,
                'salary_year' => $salary_year,
                'salary_month_int' => $salary_month,
                'salary_group_id' => $this->input->post('salary_group_id'),
                'pay_grade_id' => $value->pay_grade_id,
                'payment_frequency_type' => $this->input->post('payment_frequency_type'),
                'basic_salary' => $value->basic_salary,
                'basic_percent' => $value->pay_grade_id,
                'gross_salary' => $value->gross_salary,
                'net_salary' => $value->net_salary,
                'tax_amount' => $value->tax_amount,
                'overtime_rate' => $value->overtime_rate,
                'pf_amount' => $value->pf_amount,
                'hourly_rate' => $value->hourly_rate,
                'income_tax_id' => $value->income_tax_id,
                'payment_type' => $value->payment_type,
                'account_id' => $value->account_id,
                'account_number' => $value->account_number,
                'remark' => $this->input->post('remark'),
                'created_at' => date('Y-m-d'),
                'created_by'   => $this->session->userdata('user_id'),
            );

            // $insert_id = $this->hrm_payroll_model->add_payroll_salary($data);

            $payroll_setup_dtls = $this->hrm_payroll_model->get_payroll_setup_dtls($value->id);

            $sqlWhere = "";
            if(!empty($department_id)) {
                $sqlWhere .= " AND roster.department_id=".$department_id;
            }
            if(!empty($emp_id)) {
                $sqlWhere .= " AND roster.employee_id=".$emp_id;
            }
            $sqlQuery = "
                    select roster.employee_id                                                           AS employee_id,
                           sum(CASE WHEN roster.salary_status = 'Unauthorized_Leave' THEN 1 ELSE 0 END) AS unauthorized_leave,
                           sum(CASE WHEN roster.salary_status = 'LatePresent' THEN 1 ELSE 0 END)        AS late_present,
                           sum(CASE WHEN roster.salary_status = 'EarlyLeave' THEN 1 ELSE 0 END)         AS early_leave,
                           sum(CASE WHEN roster.salary_status = 'InLeave' THEN 1 ELSE 0 END)            AS in_leave,
                           sum(CASE WHEN roster.salary_status = 'InTour' THEN 1 ELSE 0 END)             AS in_tour,
                           sum(CASE WHEN roster.salary_status = 'Regular' THEN 1 ELSE 0 END)            AS present,
                           sum(CASE WHEN roster.salary_status = 'Half_Day_Leave' THEN 1 ELSE 0 END)     AS half_day_leave,
                           sum(CASE WHEN roster.salary_status = 'Weekend' THEN 1 ELSE 0 END)            AS weekend,
                           sum(overtime_minute)                                                         AS overtime_minute
                    FROM sma_hrm_roster_details roster WHERE roster.roster_date >= '". $first_day_month . "' AND roster.roster_date <= '" . $last_day_month . "'" .
                        $sqlWhere .
                    "GROUP BY roster.employee_id
                    order by roster.employee_id"
                    ;
    
            $salary_status = $this->db->query($sqlQuery)->result();
    
            $roster_map = array();
            $overtime_map = array();
            foreach($salary_status as $ss) {
                $roster_map['Unauthorized_Leave'] = $ss->unauthorized_leave;
                $roster_map['LatePresent'] = $ss->late_present;
                $roster_map['EarlyLeave'] = $ss->early_leave;
                $roster_map['InLeave'] = $ss->in_leave;
                $roster_map['Present'] = $ss->present;
                $roster_map['Half_Day_Leave'] = $ss->half_day_leave;

                $overtime = $ss->overtime_minute;
            }
            // echo '<pre>';print_r($roster_map); die;

            $salary_days = 30;
            $payroll_polices = $this->hrm_payroll_model->get_payroll_polices();
            $policy_array = array();
            foreach($payroll_polices as $payroll_policy) {

                if($payroll_policy->salary_status == "Salary_Days") {
                    $salary_days = $payroll_policy->quantity;
                } else {
                    $policy_array[$payroll_policy->salary_status] = $payroll_policy->quantity;
                }
            }
            // echo '<pre>';print_r($roster_map); 
            // echo '<pre>';echo 'policy-';print_r($policy_array); 
            // die;
            foreach($payroll_setup_dtls as $row) {

            if ($row->pay_nature != 'Tax') {    
                $data_dtl = array(
                    'salary_mst_id' => $insert_id,
                    'payroll_head_id' => $row->payroll_head_id,
                    'addition_deduction' => $row->addition_deduction,
                    'allowance_type' => $row->allowance_type,
                    'pay_nature' => $row->pay_nature,
                    // 'actual_amt' => $row->actual_amt,
                );

                if($row->pay_nature == 'AtnDeduct') {
                    $data_dtl['actual_amt'] = $this->getAtnDeduct($roster_map, $policy_array, $salary_days,  $value->basic_salary);
                    $deductive_amount = $deductive_amount + $row->actual_amt;
                } else if($row->pay_nature == 'Overtime') {
                    $data_dtl['actual_amt'] = $this->getOvertime($overtime,  $value->overtime_rate);
                    $additive_amount = $additive_amount + $row->actual_amt;
                } else {
                    $data_dtl['actual_amt'] = $row->actual_amt;

                    if($row->addition_deduction == 'Addition') {
                        $additive_amount = $additive_amount + $row->actual_amt;
                    } else {
                        $deductive_amount = $deductive_amount + $row->actual_amt;
                    }

                    $gross_salary = $additive_amount;
                    $net_salary = $additive_amount - $deductive_amount;
                    
                }
                // add salary Detail
                $this->hrm_payroll_model->add_payroll_salaryDtls($data_dtl);
            }
            }
            
            $tax_head = $this->hrm_payroll_model->findByNature('Tax');
            $data_dtlTax = array(
                'salary_mst_id' => $insert_id,
                'payroll_head_id' => $row->payroll_head_id,
                'addition_deduction' => $row->addition_deduction,
                'allowance_type' => $row->allowance_type,
                'pay_nature' => $row->pay_nature,
               
            );
            
            $actualAmt = $value->income_tax_id ? $this->getIncomeTax2($value->income_tax_id, $net_salary) : 0.0;
            $deductive_amount = $value->deductive_amount + $actualAmt;
            $this->hrm_payroll_model->add_payroll_salaryDtls($data_dtl);

            $salary_mst_data_update = array(
                'tax_amount' => $actualAmt,
                'deductive_amount' => $deductive_amount
            );
        }
        // echo '<pre>';print_r($payroll_setup_list); die;
    }

    public function getIncomeTax2($income_tax_id, $net_salary)
    {
        $left = $net_salary;
        $taxAmount = 0.0;
        $slabList = array();
        $rateList = array();

        $income_tax_dtls = $this->hrm_payroll_model->get_income_tax_dtls($income_tax_id);
        foreach($income_tax_dtls as $value) {
            if($value->end_amount) {
                $slabList[] = $value->end_amount;
            } else {
                $slabList[] = $value->start_amount + $net_salary;
            }
            $rateList[] = $value->tax_percent;
        }

        for($i=1; $i < sizeof($slabList) && $left > 0; $i++) {

            $applyAmount = min($slabList[$i] - $slabList[$i-1], $left);
            $taxAmount += ($rateList[$i] * $applyAmount) > 0.0 ? ($rateList[$i] * $applyAmount) / 100 : 0.0;
            $left -= $applyAmount;
            
        }
        return $taxAmount;
    }

    public function getAtnDeduct($roster_map, $policy_array, $salary_days,  $basic_salary)
    {
        $totalCutDays = 0;
        $actualAmt = 0.0;
        foreach($policy_array as $key => $policy) {

            if($policy > 0 && $roster_map[$key] > 0) {

                $totalCutDays += $roster_map[$key] / $policy;
            }
        }
        if($totalCutDays > 0 && $salary_days > 0 && $basic_salary > 0.0) {
            $actualAmt = ($basic_salary / $salary_days) * $totalCutDays;
            $actualAmt = round($actualAmt * 100) / 100;
        }
        // print_r($totalCutDays); die;
        return $actualAmt;
        
    }
    public function getOvertime($overtime, $overtime_rate)
    {

        $actualAmt = 0.0;
        if(empty($overtime) || empty($overtime_rate)) {
            return $actualAmt;
        }
        if($overtime > 0 && $overtime_rate > 0.0) {
            $actualAmt = ($overtime / 60) * $overtime_rate;
            $actualAmt = round($actualAmt * 100) / 100;
         }

        return $actualAmt;
    }
}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_payslip extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('employees_model');
        $this->load->admin_model('payroll_model');
        $this->load->admin_model('hrm_payslip_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('hrm_payroll_model');
        $this->load->admin_model('designation_model');
        $this->load->admin_model('department_model');
        $this->load->admin_model('shift_model');
		$this->load->library('form_validation');
    }

    public function index()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('pauslip')]];
        $meta = ['page_title' => lang('payslip'), 'bc' => $bc];
		$this->page_construct('hrm_payslip/payslip_list', $meta, $this->data);
    }

    public function getPayslip()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        $pdf=$this->input->post('pdf');
        $empid=$this->input->post('empid');
		$department_id=$this->input->post('departmentId');
        $start_date = date('Y-m-d', strtotime($this->input->post('startDate')));
		$year= date('Y', strtotime($this->input->post('startDate')));
		$month= date('n', strtotime($this->input->post('startDate')));
        
        if(!empty($start_date) && $start_date != '1970-01-01') {
            $records = $this->hrm_payslip_model->getPayslip($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $empid, $department_id, $year, $month);
        } else {
            $records = [];
        }
        // print_r($records);print_r($month);die;
        $data = [];
        foreach($records as $value) {

            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
            $pay_grade = $this->hrm_payroll_model->get_pay_grade_byId($value->pay_grade_id);

            $user = $this->hrm_payslip_model->get_user_byId($value->approved_by);
            $month_arr = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $emp_fullname;
            $nestedData[] = $pay_grade->caption;
            $nestedData[] = $value->salary_year;
            $nestedData[] = $month_arr[$value->salary_month];
            $nestedData[] = $value->basic_salary;
            $nestedData[] = $value->gross_salary;
            $nestedData[] = $value->net_salary;
            $nestedData[] = $value->draft_status;
            $nestedData[] = $user ? $user->first_name : '';

				$edit = '';
                
				$edit .= " <a href='" . admin_url('hrm_payslip/payslip_pdf/'.$value->id) . "' class='tip' title='" . lang('edit_add_deduct') . "'><i class=\"fa fa-edit\"></i></a>";
				
				// $nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->hrm_payslip_model->total_Payslip($sSearch, $empid, $department_id, $year, $month);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
        
		    echo json_encode($sOutput);
    }


    public function payslip_pdf2($empid='', $department_id='', $year='', $month='') 
    {
        
        $mpdf = new \Mpdf\Mpdf();
        $empid = $this->input->get('empid');
        $department_id = $this->input->get('departmentId');
        $year= date('Y', strtotime($this->input->get('startDate')));
		$month= date('n', strtotime($this->input->get('startDate')));

        $where = "";
        if(!empty($empid)) {
            $where .= " master.employee_id = {$empid} AND ";
        }
        if(!empty($department_id)) {
            $where .= " master.department_id = {$department_id} AND ";
        }
        $sql = "
        select
        head.caption as head_name, 
        head.addition_deduction as addition_deduction,
        head.percentage_of_basic as percentage_of_basic,
        salary.actual_amt,
        CASE WHEN head.addition_deduction = 'Addition' THEN salary.actual_amt*1 ELSE salary.actual_amt*(-1)  END AS sum_amt,
        master.employee_id, employee.first_name, employee.last_name, department.department_name

        FROM {$this->db->dbprefix('hrm_prl_salary_dtl salary')} 
            LEFT JOIN {$this->db->dbprefix('hrm_add_deduct head')} on salary.payroll_head_id = head.id
            LEFT JOIN {$this->db->dbprefix('hrm_prl_salary_mst master')} on salary.salary_mst_id = master.id
            LEFT JOIN {$this->db->dbprefix('employees employee')} on master.employee_id = employee.user_id
            LEFT JOIN {$this->db->dbprefix('departments department')} on employee.department_id = department.department_id
    
        where {$where} master.salary_year = {$year} and master.salary_month_int = {$month} order by head.addition_deduction"
        ;

        $data = $this->db->query($sql)->result();
        echo '<pre>';print_r($data); die;
        $newData = array();
        foreach($data as $value) {
            $newData[$value->employee_id][] = $value;
        }

        $this->data['data'] = $newData;

        $this->data['month'] = $month;
        $this->data['year'] = $year;
        $this->data['site_name'] = $this->Settings->site_name;

        $html = $this->load->view($this->theme . 'hrm_payslip/payslip_pdf', $this->data, true);
        $name = "1.pdf";
        $pdfFilePath = 'payslip_empid_'.$empid.".pdf";
        $mpdf->WriteHTML($html);
        // $mpdf->Output();
        $mpdf->Output($pdfFilePath,'D');

    }

    public function payslip_pdf($empid='', $department_id='', $year='', $month='') 
    {
        
        $mpdf = new \Mpdf\Mpdf();
        $empid = $this->input->get('empid');
        $department_id = $this->input->get('departmentId');
        $year= date('Y', strtotime($this->input->get('startDate')));
		$month= date('n', strtotime($this->input->get('startDate')));
       
        $where = "";
        $newData = array();
        if(!empty($empid)) {

            $payroll_setup = $this->hrm_payroll_model->get_payroll_setup_row($empid);

            if($payroll_setup->payment_frequency_type == 'Hourly') {
                
                $sql2 = "SELECT * FROM {$this->db->dbprefix('hrm_prl_salary_mst')} where employee_id = {$empid} AND salary_year = {$year} AND salary_month_int = {$month}";

                $data2 = $this->db->query($sql2)->row();
                $newData[$empid][] = $data2;
            }
            else {
                $sql = "
                    select
                    head.caption as head_name, 
                    head.addition_deduction as addition_deduction,
                    head.percentage_of_basic as percentage_of_basic,
                    salary.actual_amt,
                    CASE WHEN head.addition_deduction = 'Addition' THEN salary.actual_amt*1 ELSE salary.actual_amt*(-1)  END AS sum_amt,
                    master.employee_id, employee.first_name, employee.last_name, department.department_name

                    FROM {$this->db->dbprefix('hrm_prl_salary_dtl salary')} 
                        LEFT JOIN {$this->db->dbprefix('hrm_add_deduct head')} on salary.payroll_head_id = head.id
                        LEFT JOIN {$this->db->dbprefix('hrm_prl_salary_mst master')} on salary.salary_mst_id = master.id
                        LEFT JOIN {$this->db->dbprefix('employees employee')} on master.employee_id = employee.user_id
                        LEFT JOIN {$this->db->dbprefix('departments department')} on employee.department_id = department.department_id
        
                    where master.employee_id = {$empid} and master.salary_year = {$year} and master.salary_month_int = {$month} order by head.addition_deduction"
                    ;
                    $data = $this->db->query($sql)->result();
                    $newData[$empid] = $data;
                
            }    
        }
        
        else {
            $employees = $this->hrm_payroll_model->get_employees_byDept($department_id);
            
            $payment_data = array();
            foreach($employees as $employee) {

                $payroll_setup = $this->hrm_payroll_model->get_payroll_setup_row($employee->user_id);
                
            if(!empty($payroll_setup)) {

                if($payroll_setup->payment_frequency_type == 'Hourly') {
                    
                    $sql2 = "SELECT * FROM {$this->db->dbprefix('hrm_prl_salary_mst')} where employee_id = {$employee->user_id} AND salary_year = {$year} AND salary_month_int = {$month}";

                    $data2 = $this->db->query($sql2)->row();
                    $newData[$employee->user_id][] = $data2;
                }
                else {
                    $sql = "
                        select
                        head.caption as head_name, 
                        head.addition_deduction as addition_deduction,
                        head.percentage_of_basic as percentage_of_basic,
                        salary.actual_amt,
                        CASE WHEN head.addition_deduction = 'Addition' THEN salary.actual_amt*1 ELSE salary.actual_amt*(-1)  END AS sum_amt,
                        master.employee_id, employee.first_name, employee.last_name, department.department_name

                        FROM {$this->db->dbprefix('hrm_prl_salary_dtl salary')} 
                            LEFT JOIN {$this->db->dbprefix('hrm_add_deduct head')} on salary.payroll_head_id = head.id
                            LEFT JOIN {$this->db->dbprefix('hrm_prl_salary_mst master')} on salary.salary_mst_id = master.id
                            LEFT JOIN {$this->db->dbprefix('employees employee')} on master.employee_id = employee.user_id
                            LEFT JOIN {$this->db->dbprefix('departments department')} on employee.department_id = department.department_id
            
                        where master.employee_id = {$employee->user_id} and master.salary_year = {$year} and master.salary_month_int = {$month} order by head.addition_deduction"
                        ;
                        $data = $this->db->query($sql)->result();
                        $newData[$employee->user_id] = $data;
                }
            }
            
            } // .End fereach loop

        }
        // echo '<pre>'; print_r($newData); die;
        $this->data['data'] = $newData;

        $this->data['month'] = $month;
        $this->data['year'] = $year;
        // $this->data['year'] = $year;
        $this->data['site_name'] = $this->Settings->site_name;

        $html = $this->load->view($this->theme . 'hrm_payslip/payslip_pdf', $this->data, true);
        $name = "1.pdf";
        $pdfFilePath = 'payslip_empid_'.$empid.'_'.$year.'_'.$month.".pdf";
        $mpdf->WriteHTML($html);
        // $mpdf->Output();
        $mpdf->Output($pdfFilePath,'D');
        
    }

    public function payroll_bonus_setup()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_bonus_setup')]];
        $meta = ['page_title' => lang('payroll_bonus_setup'), 'bc' => $bc];
		$this->page_construct('hrm_payslip/payroll_bonus_setup', $meta, $this->data);
    }
    public function getBonusSetup()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        
        $records = $this->hrm_payslip_model->getBonusSetup($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $value->allowance_type;
            $nestedData[] = $value->percentage_of_bonus;
            $nestedData[] = $value->max_limit;

				$edit = '';
                
				$edit .= " <a href='" . admin_url('hrm_payslip/edit_bonus_setup_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_add_deduct') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_bonus_setup') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payslip/delete_bonus_setup/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->hrm_payslip_model->total_getBonusSetup($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
        
		    echo json_encode($sOutput);
    }
    public function delete_bonus_setup($id='')
    {
        if($this->hrm_payslip_model->delete_bonus_setup($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('bonus_setup_deleted')]);
        }
    }
    public function add_bonus_setup_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/add_bonus_setup', $this->data);
    }

    public function add_bonus_setup()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'allowance_type' => $this->input->post('allowance_type'),
                'percentage_of_bonus' => $this->input->post('percentage_of_bonus'),
                'max_limit' => $this->input->post('max_limit'),
                'active_status' => 'Active',
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->hrm_payslip_model->add_bonus_setup($data);
       }

    }
    public function edit_bonus_setup_form($id='')
    {
        $this->data['bonus_setup'] = $this->hrm_payslip_model->get_bonus_setup_byId($id);

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/edit_bonus_setup_form', $this->data);
    }
    public function edit_bonus_setup()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

            $id = $this->input->post('bonus_setup_id');
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'allowance_type' => $this->input->post('allowance_type'),
                'percentage_of_bonus' => $this->input->post('percentage_of_bonus'),
                'max_limit' => $this->input->post('max_limit'),
                'active_status' => 'Active',
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->hrm_payslip_model->edit_bonus_setup($id, $data);
       }

    }

    public function payroll_salary_group()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_salary_group')]];
        $meta = ['page_title' => lang('payroll_salary_group'), 'bc' => $bc];
		$this->page_construct('hrm_payslip/payroll_salary_group', $meta, $this->data);
    }
    public function get_salary_group()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        
        $records = $this->hrm_payslip_model->get_salary_group($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {
            $dept = $this->department_model->read_department_information($value->department_id);

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $dept[0]->department_name;
            
				$edit = '';
                
				$edit .= " <a href='" . admin_url('hrm_payslip/edit_salary_group_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_salary_group') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_salary_group') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payslip/delete_salary_group/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->hrm_payslip_model->total_getBonusSetup($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
        
		    echo json_encode($sOutput);
    }
    public function delete_salary_group($id='')
    {
        if($this->hrm_payslip_model->delete_salary_group($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('salary_group_deleted')]);
        }
    }
    public function add_salary_group_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/add_salary_group', $this->data);
    }

    public function add_salary_group()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $department_id = explode('|', $this->input->post('department'));
        // print_r($department_id);die;
        $department_id = $department_id[1];
        
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'department_id' => $department_id,
                
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->hrm_payslip_model->add_salary_group($data);
       }

    }
    public function edit_salary_group_form($id='')
    {
        $this->data['salary_group'] = $this->hrm_payslip_model->get_salary_group_byId($id);
        
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/edit_salary_group', $this->data);
    }
    public function edit_salary_group()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $id = $this->input->post('salary_group_id');
        $department_id = explode('|', $this->input->post('department'));
        // print_r($department_id);die;
        $department_id = $department_id[1];
        
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'department_id' => $department_id,
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->hrm_payslip_model->edit_salary_group($id, $data);
       }

    }
    public function payroll_loan_proof()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_loan_proof')]];
        $meta = ['page_title' => lang('payroll_loan_proof'), 'bc' => $bc];
		$this->page_construct('hrm_payslip/payroll_loan_proof', $meta, $this->data);
    }
    public function get_payroll_loan_proof()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        
        $records = $this->hrm_payslip_model->get_payroll_loan_proof($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {
            
            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $value->document_type;
            $nestedData[] = $value->is_mandatory == 1 ? 'Yes' : 'No';
            
				$edit = '';
                
				$edit .= " <a href='" . admin_url('hrm_payslip/edit_loan_proof_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_loan_proof') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_loan_proof') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payslip/delete_loan_proof/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->hrm_payslip_model->total_payroll_loan_proof($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
        
		    echo json_encode($sOutput);
    }
    public function delete_loan_proof($id='')
    {
        if($this->hrm_payslip_model->delete_loan_proof($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('loan_proof_deleted')]);
        }
    }
    
    public function add_loan_proof_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/add_loan_proof_form', $this->data);
    }
    public function add_loan_proof()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $is_mandatory =  $this->input->post('is_mandatory');
        if(isset($is_mandatory)) {
            $is_mandatory = 1;
        } else {
            $is_mandatory = 0;
        }

            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'document_type' => $this->input->post('document_type'),
                'is_mandatory' => $is_mandatory,
                'active_status' => 'Active',
                
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->hrm_payslip_model->add_loan_proof($data);
       }

    }
    public function edit_loan_proof_form($id='')
    {
        $this->data['loan_proof'] = $this->hrm_payslip_model->get_loan_proof_byId($id);
        
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/edit_loan_proof_form', $this->data);
    }
    public function edit_loan_proof()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $id = $this->input->post('loan_proof_id');
        $is_mandatory =  $this->input->post('is_mandatory');
        if(isset($is_mandatory)) {
            $is_mandatory = 1;
        } else {
            $is_mandatory = 0;
        }
        
        $data = array(
            'caption' => $this->input->post('caption'),
            'caption_alt' => $this->input->post('caption_alt'),
            'document_type' => $this->input->post('document_type'),
            'is_mandatory' => $is_mandatory,
            'active_status' => 'Active',
            'remark' => $this->input->post('remark'),
        );
            $this->hrm_payslip_model->edit_loan_proof($id, $data);
       }

    }
    public function payroll_loan_type()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_loan_type')]];
        $meta = ['page_title' => lang('payroll_loan_type'), 'bc' => $bc];
		$this->page_construct('hrm_payslip/payroll_loan_type', $meta, $this->data);
    }
    public function get_payroll_loan_type()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        
        $records = $this->hrm_payslip_model->get_payroll_loan_type($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {
            
            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->salary_rules;
            $nestedData[] = $value->loan_percentage;
            $nestedData[] = $value->is_interest_payable == 1 ? 'Yes' : 'No';
            $nestedData[] = $value->interest_rate;
            $nestedData[] = $value->instalments;
            $nestedData[] = $value->disburse_method;
            $nestedData[] = $value->repayment_method;

            
				$edit = '';
                
				$edit .= " <a href='" . admin_url('hrm_payslip/edit_loan_type_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_loan_type') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_loan_type') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payslip/delete_loan_type/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->hrm_payslip_model->total_payroll_loan_type($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
        
		    echo json_encode($sOutput);
    }
    public function delete_loan_type($id='')
    {
        if($this->hrm_payslip_model->delete_loan_type($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('loan_type_deleted')]);
        }
    }
    public function add_loan_type_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/add_loan_type_form', $this->data);
    }
    public function add_loan_type()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $is_interest_payable =  $this->input->post('is_interest_payable');
        if(isset($is_interest_payable)) {
            $is_interest_payable = 1;
        } else {
            $is_interest_payable = 0;
        }

            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'salary_rules' => $this->input->post('salary_rules'),
                'loan_percentage' => $this->input->post('loan_percentage'),
                'interest_rate' => $this->input->post('interest_rate'),
                'instalments' => $this->input->post('instalments'),
                'disburse_method' => $this->input->post('disburse_method'),
                'repayment_method' => $this->input->post('repayment_method'),
                'is_interest_payable' => $is_interest_payable,
                'active_status' => 'Active',
                
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->hrm_payslip_model->add_loan_type($data);
       }

    }
    public function edit_loan_type_form($id='')
    {
        $this->data['loan_type'] = $this->hrm_payslip_model->get_loan_type_byId($id);
        
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/edit_loan_type_form', $this->data);
    }
    public function edit_loan_type()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $loan_type_id = $this->input->post('loan_type_id');
        $is_interest_payable =  $this->input->post('is_interest_payable');
        if(isset($is_interest_payable)) {
            $is_interest_payable = 1;
        } else {
            $is_interest_payable = 0;
        }

            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'salary_rules' => $this->input->post('salary_rules'),
                'loan_percentage' => $this->input->post('loan_percentage'),
                'interest_rate' => $this->input->post('interest_rate'),
                'instalments' => $this->input->post('instalments'),
                'disburse_method' => $this->input->post('disburse_method'),
                'repayment_method' => $this->input->post('repayment_method'),
                'is_interest_payable' => $is_interest_payable,
                'active_status' => 'Active',
                
                'remark' => $this->input->post('remark'),
                
            );
            $this->hrm_payslip_model->edit_loan_type($loan_type_id, $data);
       }

    }
    public function payroll_loan_request()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_loan_request')]];
        $meta = ['page_title' => lang('payroll_loan_request'), 'bc' => $bc];
		$this->page_construct('hrm_payslip/payroll_loan_request', $meta, $this->data);
    }
    public function get_payroll_loan_request()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        
        $records = $this->hrm_payslip_model->get_payroll_loan_request($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
            
            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $emp_fullname;
            $nestedData[] = $value->guarantor1_id;
            $nestedData[] = $value->guarantor2_id;
            $nestedData[] = $value->transaction_date;
            $nestedData[] = $value->first_settlement_date;
            $nestedData[] = $value->salary_rules;
            $nestedData[] = $value->loan_type;
            $nestedData[] = $value->repayment_method;
            $nestedData[] = $value->disburse_method;
            $nestedData[] = $value->instalments;

            
				$edit = '';
                
				$edit .= " <a href='" . admin_url('hrm_payslip/edit_loan_request_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_loan_request') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_loan_request') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payslip/delete_loan_request/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->hrm_payslip_model->total_payroll_loan_request($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
        
		    echo json_encode($sOutput);
    }
    public function delete_loan_request($id='')
    {
        if($this->hrm_payslip_model->delete_loan_request($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('loan_request_deleted')]);
        }
    }
    public function add_loan_request_form()
    {
        $this->data[] = '';

        $this->data['loan_proof'] = $this->hrm_payslip_model->get_all_loan_proof();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/add_loan_request_form', $this->data);
    }
    public function add_loan_request()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $employee_id = $this->input->post('employee_id');
        $employee = explode('|', $employee_id);
        $guarantor1_id = $this->input->post('guarantor1_id');
        $guarantor1 = explode('|', $guarantor1_id);
        $guarantor2_id = $this->input->post('guarantor2_id');
        $guarantor2 = explode('|', $guarantor2_id);

        $transaction_date_in = $this->input->post('transaction_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));
        $first_settlement_date = $this->input->post('first_settlement_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));

            $data = array(
                'caption' => 'Dummy',
                'employee_id' => $employee[0],
                'loan_type' => $this->input->post('loan_type'),
                'transaction_date' => $this->input->post('transaction_date'),
                'first_settlement_date' => $this->input->post('first_settlement_date'),
                'salary_rules' => $this->input->post('salary_rules'),
                'disburse_method' => $this->input->post('disburse_method'),
                'repayment_method' => $this->input->post('repayment_method'),
                // 'maximum_amount' => $this->input->post('maximum_amount'),
                'interest_rate' => $this->input->post('interest_rate'),
                'principle_amount' => $this->input->post('principle_amount'),
                'interest_amount' => $this->input->post('interest_amount'),
                'instalments' => $this->input->post('instalments'),
                'net_amount' => $this->input->post('net_amount'),
                'guarantor1_id' => $guarantor1 ? $guarantor1[0] : '',
                'guarantor2_id' => $guarantor2 ? $guarantor2[0] : '',
                
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            // echo '<pre>'; print_r($this->input->post()); print_r($_FILES); die;
            $insert_id = $this->hrm_payslip_model->add_loan_request($data);

            if($insert_id) {

                for($i = 0; $i < sizeof($_POST['loan_proof_id']); $i++) {
                    
                    if(isset($_FILES["document_path_".($i+1)])) {
                            $this->load->library('upload'); 
                           $config['upload_path']          = 'assets/uploads/loan_proof/';
                           $config['allowed_types']        = 'gif|jpg|jpeg|png|';
                           $config['max_size']             = 2000;
                           $this->upload->initialize($config); 
                           if ($this->upload->do_upload('document_path_'.($i+1))) {
                                   $data = array('upload_data' => $this->upload->data());
                                   $file_name = $data['upload_data']['file_name'];
                                  
                           }
                       }

                    $data_loan_proof = array(
                        'loan_advance_id' => $insert_id,
                        'loan_proof_id' => $_POST['loan_proof_id'][$i],
                        'document_path' => $file_name,
                        'remark' => $_POST['remark_doc'][$i],
                        'created_by' => $this->session->userdata('user_id'),
                        'created_at' => date('Y-m-d')
                    );
                    $this->hrm_payslip_model->add_loan_proof_doc($data_loan_proof);
                }
            }
       }

    }
    public function edit_loan_request_form($id)
    {
        $this->data['loan_request'] = $this->hrm_payslip_model->get_loan_request_byId($id);
        $this->data['loan_proof_docs'] = $this->hrm_payslip_model->get_loan_proof_docs($id);

        $this->data['loan_proof'] = $this->hrm_payslip_model->get_all_loan_proof();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/edit_loan_request_form', $this->data);
    }
    public function edit_loan_request()
    {
    //    print_r($this->input->post()); print_r($_FILES); die;
       if($this->input->post()) {

        $loan_request_id = $this->input->post('loan_request_id');
        
        $employee_id = $this->input->post('employee_id');
        $employee = explode('|', $employee_id);
        $guarantor1_id = $this->input->post('guarantor1_id');
        $guarantor1 = explode('|', $guarantor1_id);
        $guarantor2_id = $this->input->post('guarantor2_id');
        $guarantor2 = explode('|', $guarantor2_id);

        $transaction_date_in = $this->input->post('transaction_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));
        $first_settlement_date = $this->input->post('first_settlement_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));

            $data = array(
                'caption' => 'Dummy',
                'employee_id' => $employee[0],
                'loan_type' => $this->input->post('loan_type'),
                'transaction_date' => $this->input->post('transaction_date'),
                'first_settlement_date' => $this->input->post('first_settlement_date'),
                'salary_rules' => $this->input->post('salary_rules'),
                'disburse_method' => $this->input->post('disburse_method'),
                'repayment_method' => $this->input->post('repayment_method'),
                // 'maximum_amount' => $this->input->post('maximum_amount'),
                'interest_rate' => $this->input->post('interest_rate'),
                'principle_amount' => $this->input->post('principle_amount'),
                'interest_amount' => $this->input->post('interest_amount'),
                'instalments' => $this->input->post('instalments'),
                'net_amount' => $this->input->post('net_amount'),
                'guarantor1_id' => $guarantor1 ? $guarantor1[0] : '',
                'guarantor2_id' => $guarantor2 ? $guarantor2[0] : '',
                
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            // echo '<pre>'; print_r($this->input->post()); print_r($_FILES); die;
            $update = $this->hrm_payslip_model->edit_loan_request($loan_request_id, $data);

            if($update) {

                if(isset($_FILES)) {

                // $loan_proof_docs = $this->db->get_where('hrm_loan_proof_doc', ['loan_advance_id' => $loan_request_id])->result();
                // foreach($loan_proof_docs as $value) {
                //     unlink('assets/uploads/loan_proof/'.$value->document_path);
                // }
                // $this->db->where('loan_advance_id', $loan_request_id)->delete('hrm_loan_proof_doc');

                for($i = 0; $i < sizeof($_POST['loan_proof_id']); $i++) {

                    if(isset($_FILES["document_path_".($i+1)])) {
                        
                           $this->load->library('upload'); 
                           $config['upload_path']          = 'assets/uploads/loan_proof/';
                           $config['allowed_types']        = 'gif|jpg|jpeg|png|';
                           $config['max_size']             = 2000;
                           $this->upload->initialize($config); 
                           if ($this->upload->do_upload('document_path_'.($i+1))) {
                                   $data = array('upload_data' => $this->upload->data());
                                   $file_name = $data['upload_data']['file_name'];
                                  
                           }
                       }

                       $data_loan_proof = array(
                        'loan_advance_id' => $loan_request_id,
                        'loan_proof_id' => $_POST['loan_proof_id'][$i],
                        'document_path' => $file_name,
                        'remark' => $_POST['remark_doc'][$i],
                        'created_by' => $this->session->userdata('user_id'),
                        'created_at' => date('Y-m-d')
                    );

                    
                    $this->hrm_payslip_model->add_loan_proof_doc($data_loan_proof);
                }
                }
            }
       }

    }
    public function delete_proof_doc()
    {
        if($this->input->post()) {
            $loan_proof_id = $this->input->post('loanProofId');
            $loanRequestId = $this->input->post('loanRequestId');

            $delete = $this->hrm_payslip_model->delete_proof_doc($loan_proof_id);
            if($delete) {
                $loan_proof_docs = $this->hrm_payslip_model->get_loan_proof_docs($loanRequestId);
                $this->sma->send_json(['loanProofDocs' => $loan_proof_docs]);
            }
        }
    } 

    public function payroll_loan_advance()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('payroll_loan_advance')]];
        $meta = ['page_title' => lang('payroll_loan_advance'), 'bc' => $bc];
		$this->page_construct('hrm_payslip/payslip_loan_advance', $meta, $this->data);
    }
    public function get_payroll_loan_advance()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
        
        $records = $this->hrm_payslip_model->get_payroll_loan_advance($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
            
            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $emp_fullname;
            $nestedData[] = $value->loan_type;
            $nestedData[] = $value->principle_amount;
            $nestedData[] = $value->instalments;
            $nestedData[] = $value->interest_rate;
            $nestedData[] = $value->interest_amount;
            $nestedData[] = $value->net_amount;
            $nestedData[] = $value->transaction_date;
            $nestedData[] = $value->first_settlement_date;
            
				$edit = '';
                $edit .= " <a href='" . admin_url('hrm_payslip/view_loan_advance/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_loan_advance') . "'><i class=\"fa fa-file-text\"></i></a> ";
				$edit .= " <a href='" . admin_url('hrm_payslip/edit_loan_advance_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_loan_advance') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_loan_advance') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_payslip/delete_loan_advance/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->hrm_payslip_model->total_payroll_loan_advance($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
        
		    echo json_encode($sOutput);
    }
    public function delete_loan_advance($id='')
    {
        if($this->hrm_payslip_model->delete_loan_advance($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('loan_advance_deleted')]);
        }
    }

    public function add_loan_advance_form()
    {
        $this->data[] = '';

        $this->data['loan_proof'] = $this->hrm_payslip_model->get_all_loan_proof();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/add_loan_advance_form', $this->data);
    }

    public function add_loan_advance()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $employee_id = $this->input->post('employee_id');
        $employee = explode('|', $employee_id);
        $guarantor1_id = $this->input->post('guarantor1_id');
        $guarantor1 = explode('|', $guarantor1_id);
        $guarantor2_id = $this->input->post('guarantor2_id');
        $guarantor2 = explode('|', $guarantor2_id);

        $transaction_date_in = $this->input->post('transaction_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));
        $first_settlement_date = $this->input->post('first_settlement_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));

            $data = array(
                'caption' => 'Dummy',
                'employee_id' => $employee[0],
                'loan_type' => $this->input->post('loan_type'),
                'transaction_date' => $this->input->post('transaction_date'),
                'first_settlement_date' => $this->input->post('first_settlement_date'),
                'salary_rules' => $this->input->post('salary_rules'),
                'disburse_method' => $this->input->post('disburse_method'),
                'repayment_method' => $this->input->post('repayment_method'),
                // 'maximum_amount' => $this->input->post('maximum_amount'),
                'interest_rate' => $this->input->post('interest_rate'),
                'principle_amount' => $this->input->post('principle_amount'),
                'interest_amount' => $this->input->post('interest_amount'),
                'instalments' => $this->input->post('instalments'),
                'net_amount' => $this->input->post('net_amount'),
                'guarantor1_id' => $guarantor1 ? $guarantor1[0] : '',
                'guarantor2_id' => $guarantor2 ? $guarantor2[0] : '',
                
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            // echo '<pre>'; print_r($this->input->post()); print_r($_FILES); die;
            $insert_id = $this->hrm_payslip_model->add_loan_advance($data);

            if($insert_id) {
// print_r($_POST); die;
                for($i = 0; $i < sizeof($_POST['loanAdvanceDtlsId']); $i++) {
                    
                    $loan_advance_dtls = array(
                        'loan_advance_id' => $insert_id,
                        'transaction_date' => $_POST['transactionDate'][$i],
                        'settlement_date' => $_POST['settlementDate'][$i],
                        'principle_amount' => $_POST['principleAmount'][$i],
                        'instalment_amount' => $_POST['instalmentAmount'][$i],
                        'interest_amount' => $_POST['interestAmount'][$i],
                        'paid_amount' => $_POST['paidAmount'][$i],
                        'created_by' => $this->session->userdata('user_id'),
                        'created_at' => date('Y-m-d')
                    );
                    $this->hrm_payslip_model->add_loan_advance_dtls($loan_advance_dtls);
                }
            }
       }

    }

    public function edit_loan_advance_form($id)
    {
        $this->data['loan_advance'] = $this->hrm_payslip_model->get_loan_advance_byId($id);

        $this->data['loan_proof'] = $this->hrm_payslip_model->get_all_loan_proof();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/edit_loan_advance_form', $this->data);
    }
    public function edit_loan_advance()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $loan_advance_id = $this->input->post('loan_advance_id');
        $employee_id = $this->input->post('employee_id');
        $employee = explode('|', $employee_id);
        $guarantor1_id = $this->input->post('guarantor1_id');
        $guarantor1 = explode('|', $guarantor1_id);
        $guarantor2_id = $this->input->post('guarantor2_id');
        $guarantor2 = explode('|', $guarantor2_id);

        $transaction_date_in = $this->input->post('transaction_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));
        $first_settlement_date = $this->input->post('first_settlement_date');
        $transaction_date = date('Y-m-d', strtotime($transaction_date_in));

            $data = array(
                'caption' => 'Dummy',
                'employee_id' => $employee[0],
                'loan_type' => $this->input->post('loan_type'),
                'transaction_date' => $this->input->post('transaction_date'),
                'first_settlement_date' => $this->input->post('first_settlement_date'),
                'salary_rules' => $this->input->post('salary_rules'),
                'disburse_method' => $this->input->post('disburse_method'),
                'repayment_method' => $this->input->post('repayment_method'),
                // 'maximum_amount' => $this->input->post('maximum_amount'),
                'interest_rate' => $this->input->post('interest_rate'),
                'principle_amount' => $this->input->post('principle_amount'),
                'interest_amount' => $this->input->post('interest_amount'),
                'instalments' => $this->input->post('instalments'),
                'net_amount' => $this->input->post('net_amount'),
                'guarantor1_id' => $guarantor1 ? $guarantor1[0] : '',
                'guarantor2_id' => $guarantor2 ? $guarantor2[0] : '',
                
                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            // echo '<pre>'; print_r($this->input->post()); print_r($_FILES); die;
            $insert_id = $this->hrm_payslip_model->edit_loan_advance($loan_advance_id, $data);

          
       }

    }
    public function view_loan_advance($id)
    {
        $this->data['loan_advance'] = $this->hrm_payslip_model->get_loan_advance_byId($id);
        $this->data['loan_advance_dtls'] = $this->hrm_payslip_model->get_loan_advance_dtls($id);

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_payslip/view_loan_advance', $this->data);
    }
    
}
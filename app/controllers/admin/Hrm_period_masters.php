<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_period_masters extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('employees_model');
        $this->load->admin_model('attendance_model');
        $this->load->library('form_validation');
    }

    public function period_master()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('period_master')]];
        $meta = ['page_title' => lang('period_master'), 'bc' => $bc];
		$this->page_construct('hrn_period_masters/period_master', $meta, $this->data);
    }
    public function period_master_form()
    {
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrn_period_masters/period_master_form', $this->data);
    }
    public function getPeriodMaster()
     {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->attendance_model->get_period_master($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);    
        $data = [];
        foreach($records as $value) {
            $nestedData = array();
				$nestedData[] = $value->id;
				$nestedData[] = $value->name;
				$nestedData[] = $value->name_alt;
				$nestedData[] = $value->start_date;
				$nestedData[] = $value->end_date;
                $nestedData[] = $value->period_type;
                $nestedData[] = $value->is_open;
                $nestedData[] = $value->is_close;

				$edit = '';
				//$edit = "<div class=\"text-center\"><a href='" . admin_url('attendances/view_period_master/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_period_master') . "'><i class=\"fa fa-file-text\"></i></a>";
				//$edit .= " <a href='" . admin_url('attendances/edit_period_master/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_period_master') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_period_master') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_period_masters/delete_period_master/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->attendance_model->total_period_master($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);

     }
     public function delete_period_master($period_master_id)
     {
        if ($this->attendance_model->delete_period_master($period_master_id)) {

            $this->attendance_model->delete_period_details($period_master_id);
            $this->sma->send_json(['error' => 0, 'msg' => lang('period_master_deleted')]);
        }
     }
     public function add_period_master()
     {
        // print_r($this->input->post());die;
        if($this->input->post()) {
    
            $period_type = $this->input->post('period_type');
            $year = $this->input->post('year');

            if($period_type == 'customize') {

                $start_date = $this->input->post('start_date');
                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = $this->input->post('end_date');
                $end_date = date('Y-m-d', strtotime($end_date));

                if($start_date == '1970-01-01' || $end_date == '1970-01-01') {
                    $this->sma->send_json(['error' => 1, 'msg' => lang('please_enter_start_date_and_end_date')]);
                }
                $caption = date('d-m-Y', strtotime($start_date)). ' - '. date('d-m-Y', strtotime($end_date));
                $data_period_master = array(
                    'name' => $this->input->post('name') ? $this->input->post('name') : $caption,
                    'name_alt' => $this->input->post('name_alt') ? $this->input->post('name_alt') : $caption,
                    'period_type' => $period_type,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'year' => date('Y', strtotime($start_date)),
                    'is_open' => 0,
                    'is_close' => 0,
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d')
                );
                $prd_master_id = $this->attendance_model->add_period_master($data_period_master);
                $dateDay = $start_date;
                while ($dateDay <= $end_date) {

                    $caption_periodDtl = date('d', strtotime($dateDay)).'-'.date('m', strtotime($dateDay)).'-'.date('Y', strtotime($dateDay)).'-'.$caption;
                    $data_periodDtl = array(
                        'prd_master_id' => $prd_master_id,
                        'name' => $caption_periodDtl,
                        'name_alt' => $this->input->post('name_alt'),
                        'prd_date' =>  $dateDay,
                        'is_open' => 0,
                        'is_close' => 0,
                        'cal_day' => date('d', strtotime($dateDay)),
                        'cal_month' => date('n', strtotime($dateDay)),
                        'cal_week' => date('W', strtotime($dateDay)),
                        'cal_year' => date('Y', strtotime($dateDay)),
                        'day' => date('l', strtotime($dateDay)),
                        'created_by' => $this->session->userdata('user_id'),
                        'created_at' => date('Y-m-d')
                    );
                    
                    $this->attendance_model->add_period_detail($data_periodDtl);
                    $dateDayNext = strtotime('+1 day', strtotime($dateDay));
                    $dateDay = date('Y-m-d', $dateDayNext);
                }

            } else {
                if($period_type == 'monthly') {

                    for ($i = 1; $i <= 12; $i++) {
                        $start_date = mktime(0, 0, 0, $i, 1, $year);
                        $last_day = date('t', $start_date);
                        $end_date = mktime(0, 0, 0, $i, $last_day, $year);
                        $month = date('F', $start_date);
                        $caption = $period_type. ' Period of- '. $month . ' - ' . $year;
                        
                        $startDate = date('Y-m-d', $start_date);
                        $endDate = date('Y-m-d', $end_date);
                        $dateDay = $startDate;

                        $data_period_master = array(
                            'name' => $this->input->post('name') ? $this->input->post('name') : $caption,
                            'name_alt' => $this->input->post('name_alt'),
                            'period_type' => $period_type,
                            'start_date' => $startDate,
                            'end_date' => $endDate,
                            'year' => $year,
                            'is_open' => 0,
                            'is_close' => 0,
                            'created_by' => $this->session->userdata('user_id'),
                            'created_at' => date('Y-m-d')
                        );
                        // print_r($startDate); echo '</br>'; 
                        // print_r($endDate); echo '</br>'; die;
                        
                        $prd_master_id = $this->attendance_model->add_period_master($data_period_master);

                        while ($dateDay <= $endDate) {

                            $caption_periodDtl = date('d', strtotime($dateDay)).'-'.date('m', strtotime($dateDay)).'-'.date('Y', strtotime($dateDay)).'-'.$caption;
                            $data_periodDtl = array(
                                'prd_master_id' => $prd_master_id,
                                'name' => $caption_periodDtl,
                                'name_alt' => $this->input->post('name_alt'),
                                'prd_date' =>  $dateDay,
                                'is_open' => 0,
                                'is_close' => 0,
                                'cal_day' => date('d', strtotime($dateDay)),
                                'cal_month' => date('n', strtotime($dateDay)),
                                'cal_week' => date('W', strtotime($dateDay)),
                                'cal_year' => date('Y', strtotime($dateDay)),
                                'day' => date('l', strtotime($dateDay)),
                                'created_by' => $this->session->userdata('user_id'),
                                'created_at' => date('Y-m-d')
                            );
                            
                            $this->attendance_model->add_period_detail($data_periodDtl);
                            // $dateDay = $dateDay + 1;
                            $dateDayNext = strtotime('+1 day', strtotime($dateDay));
                            $dateDay = date('Y-m-d', $dateDayNext);
                            // print_r($data_periodDtl); print_r($dateDay);
                            // die;
                        }
                        
                    } 

                } else if($period_type == 'weekly') {


                    $total_weeks = max(date("W", strtotime($year ."-12-27")), date("W", strtotime($year ."-12-29")), date("W", strtotime($year ."-12-31")));

                    $temp_date = mktime(0, 0, 0, 1, 1, $year);
                    for ($i = 1; $i <= $total_weeks; $i++) {
                        $start_date = $temp_date;
                        $end_date = strtotime('+6 days', $start_date);
                        $temp_date = strtotime('+1 day', $end_date); // $end_date + 1

                        $caption = $period_type. ' Period of week- '. $i . ' - ' . $year;
                        
                        $startDate = date('Y-m-d', $start_date);
                        $endDate = date('Y-m-d', $end_date);
                        $dateDay = $startDate;

                        $data_period_master = array(
                            'name' => $this->input->post('name') ? $this->input->post('name') : $caption,
                            'name_alt' => $this->input->post('name_alt'),
                            'period_type' => $period_type,
                            'start_date' => $startDate,
                            'end_date' => $endDate,
                            'year' => $year,
                            'is_open' => 0,
                            'is_close' => 0,
                            'created_by' => $this->session->userdata('user_id'),
                            'created_at' => date('Y-m-d')
                        );
                        
                        $prd_master_id = $this->attendance_model->add_period_master($data_period_master);

                        while ($dateDay <= $endDate) {

                            $caption_periodDtl = date('d', strtotime($dateDay)).'-'.date('m', strtotime($dateDay)).'-'.date('Y', strtotime($dateDay)).'-'.$caption;
                            $data_periodDtl = array(
                                'prd_master_id' => $prd_master_id,
                                'name' => $caption_periodDtl,
                                'name_alt' => $this->input->post('name_alt'),
                                'prd_date' =>  $dateDay,
                                'is_open' => 0,
                                'is_close' => 0,
                                'cal_day' => date('d', strtotime($dateDay)),
                                'cal_month' => date('n', strtotime($dateDay)),
                                'cal_week' => date('W', strtotime($dateDay)),
                                'cal_year' => date('Y', strtotime($dateDay)),
                                'day' => date('l', strtotime($dateDay)),
                                'created_by' => $this->session->userdata('user_id'),
                                'created_at' => date('Y-m-d')
                            );
                            
                            $this->attendance_model->add_period_detail($data_periodDtl);
                            $dateDayNext = strtotime('+1 day', strtotime($dateDay));
                            $dateDay = date('Y-m-d', $dateDayNext);
                            
                        }
                        
                    } 
                }
            }

            // $data = array(
			// 	'event_name' => $this->input->post('event_name'),
			// 	'start_date' => $start_date,
			// 	'end_date' => $end_date,
			// 	'is_publish' => $this->input->post('is_publish'),
			// 	'description' => $this->input->post('description'),
			// 	'created_at' => date('d-m-Y')
			// );
			// $insert = $this->corehr_model->add_period_master($data);
			// if($insert) {
			// 	return true;
			// } else {
			// 	return false;
			// }
        }
     }

     public function edit_period_master($id)
     {
        $period_master = $this->attendance_model->read_period_master($id);
        $this->data['period_master'] = $period_master;
        
		$this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'hrn_period_masters/edit_period_master', $this->data);
     }
}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_roster extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('roster_model');
        $this->load->admin_model('shift_model');
        $this->load->admin_model('employees_model');
        $this->load->admin_model('attendance_model');
        $this->load->admin_model('department_model');
        $this->load->library('form_validation');
    }
    public function rosters_list()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('roster')]];
        $meta = ['page_title' => lang('roster'), 'bc' => $bc];
		$this->page_construct('hrm_roster/rosters_list', $meta, $this->data);
    } 
    public function getRosters()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->roster_model->get_rosters($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);    
        $data = [];
        foreach($records as $value) {

            $period_master = $this->attendance_model->read_period_master($value->period_mst_id);
            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $name = $emp_name->first_name.' '.$emp_name->last_name . '-';
            $dept = $this->shift_model->read_department_information($value->department_id);

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $period_master ? $period_master->name : '';
            $nestedData[] = $emp_name->first_name.' '.$emp_name->last_name;
            $nestedData[] = $dept->department_name;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->active_status == 1 ? 'ACTIVE' : 'DEACTIVE';

				$edit = '';
				//$edit .= " <a href='" . admin_url('hrm_shift_master/edit_shift_master/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_shift_master') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_roster') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_roster/delete_roster/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->roster_model->total_rosters($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function delete_roster($id='')
    {
        if($this->roster_model->delete_roster($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('roster_deleted')]);
        }
    }
    public function add_roster_form()
    {
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();
        $this->data['period_masters'] = $this->roster_model->all_period_masters();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_roster/add_roster_form', $this->data);
    }
    public function add_roster()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

        $department_id = $this->input->post('department');    
        $employee_id = $this->input->post('employee');
        $period_mst_id = $this->input->post('period_mst_id');
        
        if(!empty($department_id)) {

            if(empty($employee_id)) {
                $employees = $this->roster_model->all_employees_by_dept($department_id);
            } else {
                $employees = $this->roster_model->get_employee_by_id($employee_id);
            }

            foreach($employees as $employee) {

                $emp_mst_shift = $this->roster_model->get_emp_shifts($employee->user_id);
                // echo '<pre>';print_r($emp_mst_shift);die;
                if(!empty($emp_mst_shift)) {
                //foreach($emp_mst_shifts as $emp_mst_shift) {

                    $shift_master = $this->roster_model->get_shift_master($emp_mst_shift->shift_mst_id);
                    $check_roster = $this->roster_model->check_emp_roster($employee->user_id, $period_mst_id);

                    $period_master = $this->attendance_model->read_period_master($period_mst_id);
                    $start_date = $period_master->start_date;
                    $end_date = $period_master->end_date;
                    $current_year = date('Y', strtotime($start_date));
                    $current_month = date('n', strtotime($start_date));
// echo '<pre>';print_r($check_roster);die;
                    $emp_name = $this->employees_model->get_emp_name($employee->user_id);
                    $caption = $emp_name->first_name. ' '.$emp_name->last_name . ' - ' . $period_master->name;

                    if(empty($check_roster)) {
                        $data_roster = array(
                            'caption' => $caption,
                            'period_mst_id' => $period_mst_id,
                            'department_id' => $this->input->post('department'),
                            'employee_id' => $employee->user_id,
                            'current_year' => $current_year,
                            'current_month' => $current_month,
                            'remark' => $this->input->post('remark'),
                            'created_by' => $this->session->userdata('user_id'),
                            'created_at' => date('Y-m-d')
                        );
                    
                        $roster_id = $this->roster_model->add_roster($data_roster);
                        // $roster_id = 1;
                        $dateDay = $start_date;
                        while ($dateDay <= $end_date) {

                            $week_days = date('l', strtotime($dateDay)); 
                            $week_day = strtoupper($week_days);

                            if($week_day == 'FRIDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->fri_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');

                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->fri_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->fri_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->fri_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'SATURDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->sat_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                // print_r($entry_roster);die;
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->sat_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->sat_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->sat_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'SUNDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->sun_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->sun_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->sun_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->sun_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'MONDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->mon_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->mon_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->mon_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->mon_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'TUESDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->tues_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->tues_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->tues_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->tues_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'WEDNESDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->wed_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->wed_end_time);
                                } else {
                                    $exit_roster = 
                                    new DateTime($dateDay .' '. $emp_mst_shift->wed_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->wed_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'THURSDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->thurs_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->thurs_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->thurs_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->thurs_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            $roster_diff = $exit_roster->diff($entry_roster);
                            $hours = $roster_diff->format('%h'); 
                            $minutes = $roster_diff->format('%i');
                            $roster_minute = $hours * 60 + $minutes;
                            
                            $tolerance_date = clone($entry_roster);
                            $entry_roster_tol = $tolerance_date->add(new DateInterval('PT'.$emp_mst_shift->late_tolerance.'M'));
                            $entry_roster_time_tol = $entry_roster_tol->format('Y-m-d H:i:s');
                            // print_r($entry_roster); die;
                            $overtime_enable = $emp_mst_shift->overtime_enable;
                            $auto_rostering = $emp_mst_shift->auto_rostering;
                            $log_required = $emp_mst_shift->log_required;
                            $logout_required = $emp_mst_shift->logout_required;
                            $half_day_absent = $emp_mst_shift->half_day_absent;
                            $overtime_minute = 0;

                            if(!empty($emp_mst_shift->overtime_after)) {
                                $overtime_after_date = clone($exit_roster);
                                $overtime_after_add = $overtime_after_date->add(new DateInterval('PT'.$emp_mst_shift->overtime_after.'M'));
                                $overtime_after = $overtime_after_add->format('Y-m-d H:i:s');
                            } else {
                                $overtime_after = null;
                            }
                            if(!empty($emp_mst_shift->half_day_leave)) {
                                $half_day_date = clone($entry_roster);
                                $half_day_time_add = $half_day_date->add(new DateInterval('PT'.$emp_mst_shift->half_day_leave.'M'));
                                $half_day_time = $half_day_time_add->format('Y-m-d H:i:s');
                            } else {
                                $half_day_time = null;
                            }
                            if(isset($emp_mst_shift->start_punch)) {
                                $start_punch_time_date = clone($entry_roster);
                                // print_r($entry_roster); die;
                                $start_punch_time_add = $start_punch_time_date->add(new DateInterval('PT'.$emp_mst_shift->start_punch.'M'));
                                $start_punch_time = $start_punch_time_add->format('Y-m-d H:i:s');
                            } else {
                                $start_punch_time = null;
                            }
                            if(isset($emp_mst_shift->end_punch)) {
                                $end_punch_time_date = clone($exit_roster);
                                $end_punch_time_add = $end_punch_time_date->add(new DateInterval('PT'.$emp_mst_shift->end_punch.'M'));
                                $end_punch_time = $end_punch_time_add->format('Y-m-d H:i:s');
                            } else {
                                $end_punch_time = null;
                            }
                            
                            $caption = date('d',strtotime($dateDay)).'-'. date('m',strtotime($dateDay)).'-'.date('Y',strtotime($dateDay));
                            $emp_name = $this->employees_model->get_emp_name($employee->user_id);
                            $emp_full_name = $emp_name->first_name.' '.$emp_name->last_name;

                            $data_roster_detail = array(
                                'roster_id' => $roster_id,
                                'roster_date' => $dateDay,
                                'shift_id' => $emp_mst_shift->shift_mst_id,
                                'emp_shift_id' => $emp_mst_shift->id,
                                'day_duration' => $shift_master->day_duration,
                                'period_id' => $period_mst_id,
                                'department_id' => $department_id,
                                'employee_id' => $employee->user_id,
                                'caption' => $caption,
                                'employee_caption' => $emp_full_name,
                                'employee_caption_alt' => $emp_full_name,
                                'card_no' => $emp_mst_shift->card_number,
                                'entry_roster_time' => $entry_roster_time,
                                'exit_roster_time' => $exit_roster_time,
                                'roster_minute' => $roster_minute,
                                'entry_roster_time_tol' => $entry_roster_time_tol,
                                'overtime_enable' => $overtime_enable,
                                'auto_rostering' => $auto_rostering,
                                'log_required' => $log_required,
                                'logout_required' => $logout_required,
                                'half_day_absent' => $half_day_absent,
                                'overtime_minute' => $overtime_minute,
                                'overtime_after' => $overtime_after,
                                'half_day_time' => $half_day_time,
                                'start_punch_time' => $start_punch_time,
                                'end_punch_time' => $end_punch_time,
                                'is_weekend' => $is_weekend,
                                'created_by' => $this->session->userdata('user_id'),
                                'created_at' => date('Y-m-d')
                            );
                            // echo '<pre>'; print_r($data_roster_detail); die;
                            $this->roster_model->add_roster_detail($data_roster_detail);
                            $dateDayNext = strtotime('+1 day', strtotime($dateDay));
                            $dateDay = date('Y-m-d', $dateDayNext);
                        }
                        
                    } else {
                       
                    }
                //}
                } else {
                    $this->sma->send_json(array('error' => 1, 'msg' => lang('shift_is_not_assigned_for_this_employee!')));
                }

            }

            $this->sma->send_json(array('error' => 0, 'msg' => lang('success!')));

        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang('please_enter_department!')));
        }

       }
   
    }

    public function get_dept_employees()
    {
        if($this->input->post())
        {
            $dept_id = $this->input->post('dept_id');
            $dept_employees = $this->db->where('department_id', $dept_id)->get('employees')->result();
// print_r($dept_id); print_r($dept_employees); die;
            $output = '';
            $output .= '<select name="employee" id="employee_id" class="form-control select">
                <option value="">Select</option>';
                foreach($dept_employees as $row) { 
                    $output .= '<option value="'.$row->user_id.'">'.$row->first_name. ' '.$row->last_name.'</option>';
                } 
            $output .= '</select>';
            echo $output;
        }
    }

    public function roster_process_form()
    {
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();
        $this->data['period_masters'] = $this->roster_model->all_period_masters();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_roster/roster_process_form', $this->data);
    }

    public function roster_process()
    {
        if($this->input->post()) {

            $roster_date = date('Y-m-d', strtotime($this->input->post('roster_date')));

            $employee = explode('-',$this->input->post('employee'));
            $employee_id = $employee[0];
            $department = explode('-',$this->input->post('department'));
            $department_id = $department[0];

            $emp_ids = [];
            $emp_ids_string ='';
            if(!empty($employee_id)) {
                $emp_ids[] = $employee_id;
            } else {
                $employees = $this->roster_model->get_employees_by_dept($department_id);
                foreach($employees as $row) {
                    $emp_ids[] = $row->user_id; 
                }
            }
            $emp_ids_string = implode(',', $emp_ids);

            $sql = "
                select roster.id, roster.employee_id,
                min(log.log_day_time) as entry_time,
                max(log.log_day_time) as exit_time,
                TIMESTAMPDIFF(MINUTE, min(log.log_day_time), max(log.log_day_time)) as work_minute,
                TIMESTAMPDIFF(MINUTE, roster.entry_roster_time, roster.exit_roster_time) as roster_minute,
                CASE
                    WHEN (roster.overtime_enable = 1) THEN DATE_FORMAT(roster.overtime_after, '%d-%m-%Y %h:%i %p')
                    ELSE '-'
                    END as overtime_after,
                CASE
                    WHEN min(log.log_day_time) is null THEN 'Absent'
                    WHEN roster.exit_roster_time > max(log.log_day_time) THEN 'EarlyLeave'
                    WHEN roster.entry_roster_time_tol < min(log.log_day_time) THEN 'LatePresent'
                    ELSE 'Regular'
                    END as present_status,
                CASE
                    WHEN max(log.log_day_time) is null THEN 'Absent'
                    WHEN roster.half_day_time < min(log.log_day_time) THEN 'Half_Day_Leave'
                    ELSE 'Regular'
                    END as leave_status,
                CASE
                    WHEN (roster.overtime_enable = 1) and 
                    TIMESTAMPDIFF(MINUTE, max(log.log_day_time), roster.overtime_after) > roster.overtime_minute
                       THEN
                            TIMESTAMPDIFF(MINUTE, max(log.log_day_time), roster.overtime_after)
                    ELSE 0
                    END as overtime_minute,
                GROUP_CONCAT(DATE_FORMAT(log.log_day_time, '%h:%i %p')) as punch_log        
               
                from {$this->db->dbprefix('hrm_roster_details roster')} 
                left join {$this->db->dbprefix('hrm_attendance_log log')} 
                        on (log.card_no = roster.card_no and log.log_day_time >= roster.start_punch_time and log.log_day_time <= roster.end_punch_time)
                where roster.roster_date = '".$roster_date."' and roster.employee_id IN (".$emp_ids_string.")
                
            ";
            // on (log.card_no = roster.card_no and log.log_day_time >= roster.start_punch_time and log.log_day_time <= roster.end_punch_time)

            $results = $this->db->query($sql)->result();
            $sql_holiday = "";
// print_r($emp_ids_string); die;
            foreach($results as $row) {

                $roster_detail = $this->roster_model->read_roster_detail($row->id);

                $holiday = $this->roster_model->holiday_date($roster_date);    
                $travel = $this->roster_model->travel_date($row->employee_id, $roster_date);
                $leave_application = $this->roster_model->leave_application_date($row->employee_id, $roster_date);
// print_r($roster_date); print_r($holiday); die;
                // Calculate salary_status
                if ($row->present_status == 'Absent') {
                    if ($roster_detail->leave_application_id) {
                        $salaryStatus = 'InLeave';
                    } else if ($roster_detail->travel_id) {
                        $salaryStatus = 'InTour';
                    } else if ($roster_detail->holiday_id) {
                        $salaryStatus = 'Holidays';
                    } else if ($roster_detail->is_weekend) {
                        $salaryStatus = 'Weekend';
                    } else {
                        $salaryStatus = 'Unauthorized_Leave';
                    }
                } else if ($row->leave_status == 'Half_Day_Leave') {
                    $salaryStatus = 'Half_Day_Leave';
                } else if ($row->present_status == 'LatePresent') {
                    $salaryStatus = 'LatePresent';
                } else if ($row->present_status == 'EarlyLeave') {
                    $salaryStatus = 'EarlyLeave';
                } else {
                    $salaryStatus = 'Regular';
                }

                $data_roster_detail = array(
                    'work_minute' => $row->work_minute,
                    'roster_minute' => $row->roster_minute,
                    'entry_time' => $row->entry_time,
                    'exit_time' => $row->exit_time,
                    'present_status' => $row->present_status,
                    'leave_status' => $row->leave_status,
                    'petrol_log' => $row->punch_log,
                    'overtime_minute' => $row->overtime_minute,
                    'leave_application_id' => $leave_application ? $leave_application->leave_id : null,
                    'travel_id' => $travel ? $travel->travel_id : null,
                    'holiday_id' => $holiday ? $holiday->holiday_id : null,
                    'salary_status' => $salaryStatus
                );
                $this->roster_model->update_roster_detail($data_roster_detail, $row->id);
            }
            
        }
    }
 
}
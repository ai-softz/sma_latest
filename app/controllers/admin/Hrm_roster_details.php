<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_roster_details extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('roster_model');
        $this->load->admin_model('shift_model');
        $this->load->admin_model('employees_model');
        $this->load->admin_model('attendance_model');
        $this->load->admin_model('department_model');
        $this->load->library('form_validation');
    }
    public function roster_details_list()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['period_masters'] = $this->roster_model->all_period_masters();
        $this->data['rosters'] = $this->roster_model->all_rosters();


        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('roster')]];
        $meta = ['page_title' => lang('roster'), 'bc' => $bc];
        $this->page_construct('hrm_roster/roster_details_list', $meta, $this->data);
    } 
    public function getRosterDetails()
    {
        // echo "<pre>"; print_r($this->input->post()); die;

        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $filter_emp = $this->input->post('empid');
        $filter_rosterId = $this->input->post('rosterId');
        $filter_period_id = $this->input->post('period_id');
        $filter_salaryDate = $this->input->post('salaryDate');
        $filter_salaryDate = date('Y-m-d', strtotime($filter_salaryDate));

        $records = $this->roster_model->get_roster_details($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $filter_emp, $filter_rosterId, $filter_period_id, $filter_salaryDate);    
        $data = [];
        foreach($records as $value) {

            $period_master = $this->roster_model->read_period_master($value->period_id);
            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $name = !empty($emp_name) ? $emp_name->first_name.' '.$emp_name->last_name : '';
            $dept = $this->shift_model->read_department_information($value->department_id);

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->card_no;
            $nestedData[] = $value->roster_date;
            $nestedData[] = $period_master ? $period_master->name : '';
            $nestedData[] = $name;
            $nestedData[] = $value->entry_roster_time;
            $nestedData[] = $value->exit_roster_time;

                $edit = '';
                $edit .= " <a href='" . admin_url('hrm_roster_details/edit_roster_detail_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_roster_detail') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_roster_detail') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_roster_details/delete_roster_detail/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                $nestedData[] = $edit;
                $data[] = $nestedData;
        }
        $all_users = $this->roster_model->total_roster_details($sSearch, $filter_emp, $filter_rosterId, $filter_period_id, $filter_salaryDate);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
    }
    public function delete_roster_detail($id='')
    {
        if($this->roster_model->delete_roster_detail($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('roster_detail_deleted')]);
        }
    }

    public function edit_roster_detail_form($id = '') 
    {
        $this->data['roster_detail'] = $this->roster_model->read_roster_detail($id);
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();
        $this->data['period_masters'] = $this->roster_model->all_period_masters();
        $this->data['rosters'] = $this->roster_model->all_rosters();
        $this->data['shift_masters'] = $this->roster_model->all_shift_masters();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_roster/edit_roster_detail_form', $this->data);
  
    }

    public function add_roster_details_form()
    {
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();
        $this->data['period_masters'] = $this->roster_model->all_period_masters();
        $this->data['rosters'] = $this->roster_model->all_rosters();
        $this->data['shift_masters'] = $this->roster_model->all_shift_masters();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_roster/add_roster_details_form', $this->data);
    }
    public function add_roster_details()
    {
            //    print_r($this->input->post());die;

            
       if($this->input->post()) {

        $roster_date_input = $this->input->post('roster_date');
        $roster_date = date('Y-m-d', strtotime($roster_date_input));    
        $caption = date('d',strtotime($roster_date_input)).'-'. date('m',strtotime($roster_date_input)).'-'.date('Y',strtotime($roster_date_input));   
        $employee_id = explode('-', $this->input->post('employee'));
        $employee_id = $employee_id[0];
        $emp_name = $this->employees_model->get_emp_name($employee_id);
        $emp_full_name = $emp_name->first_name.' '.$emp_name->last_name;

        $entry_roster = $this->input->post('entry_roster_time');
        $exit_roster = $this->input->post('exit_roster_time');

        $entry_rosterDatetime = new DateTime($entry_roster);
        $exit_rosterDatetime = new DateTime($exit_roster);
        $roster_diff = $exit_rosterDatetime->diff($entry_rosterDatetime);
        $hours = $roster_diff->format('%h'); 
        $minutes = $roster_diff->format('%i');
        $roster_minute = $hours * 60 + $minutes;

        $overtime_enable = $this->input->post('overtime_enable');
        $overtime_enable = isset($overtime_enable) ? 1 : 0;
        $is_weekend = $this->input->post('is_weekend');
        $is_weekend = isset($is_weekend) ? 1 : 0;
        $is_consider = $this->input->post('is_consider');
        $is_consider = isset($is_consider) ? 1 : 0;
        $is_edited = $this->input->post('is_edited');
        $is_edited = isset($is_edited) ? 1 : 0;
        $is_approved = $this->input->post('is_approved');
        $is_approved = isset($is_approved) ? 1 : 0;

        $roster_id = $this->input->post('roster_id');
        $roster_id = $roster_id[0];
        $period_id = $this->input->post('period_id');
        $period_id = $period_id[0];

        $department_id = $this->input->post('department');
        $department_id = $department_id[0];

        $emp_mst_shift = $this->roster_model->get_emp_shifts($employee_id);

        $entry_roster_time_tol = date('Y-m-d H:i:s', strtotime($this->input->post('entry_roster_time_tol')));
        $overtime_after = date('Y-m-d H:i:s', strtotime($this->input->post('overtime_after')));
        $half_day_time = date('Y-m-d H:i:s', strtotime($this->input->post('half_day_time')));
        $entry_time = date('Y-m-d H:i:s', strtotime($this->input->post('entry_time')));
        $exit_time = date('Y-m-d H:i:s', strtotime($this->input->post('exit_time')));
                
        $data_roster_detail = array(
            'roster_id' => $roster_id,
            'roster_date' => $roster_date,
            'shift_id' => $this->input->post('shift_id'),
            'emp_shift_id' => $emp_mst_shift->id,
            'day_duration' => $this->input->post('day_duration'),
            'period_id' => $period_id,
            'department_id' => $department_id,
            'employee_id' => $employee_id,
            'caption' => $caption,
            'employee_caption' => $emp_full_name,
            'card_no' => $this->input->post('card_no'),
            'entry_roster_time' => date('Y-m-d H:i:s', strtotime($entry_roster)),
            'exit_roster_time' => date('Y-m-d H:i:s', strtotime($exit_roster)),
            'roster_minute' => $roster_minute,
            'entry_roster_time_tol' => $entry_roster_time_tol,
            'overtime_enable' => $overtime_enable,
            'overtime_minute' => 0,
            'overtime_after' => $overtime_after,
            'half_day_time' => $half_day_time,
            'entry_time' => $entry_time,
            'exit_time' => $exit_time,
            'start_punch_time' => $entry_time,
            'end_punch_time' => $exit_time,
            'present_status' => $this->input->post('present_status'),
            'leave_status' => $this->input->post('leave_status'),
            'duty_type' => $this->input->post('duty_type'),
            'pay_type' => $this->input->post('pay_type'),
            'salary_status' => $this->input->post('salary_status'),
            'confirm_status' => $this->input->post('confirm_status'),
            'is_weekend' => $is_weekend,
            'is_consider' => $is_consider,
            'is_edited' => $is_edited,
            'is_approved' => $is_approved,
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        // print_r($data_roster_detail); die;
        $this->roster_model->add_roster_detail($data_roster_detail);
       }
    }
    public function edit_roster_details()
    {
            //    print_r($this->input->post());die;
       if($this->input->post()) {

        $roster_detail_id = $this->input->post('roster_detail_id');
        $roster_date_input = $this->input->post('roster_date');
        $roster_date = date('Y-m-d', strtotime($roster_date_input));    
        $caption = date('d',strtotime($roster_date_input)).'-'. date('m',strtotime($roster_date_input)).'-'.date('Y',strtotime($roster_date_input));   
        $employee_id = $this->input->post('employee');
        $emp_name = $this->employees_model->get_emp_name($employee_id);
        $emp_full_name = $emp_name->first_name.' '.$emp_name->last_name;

        $entry_roster = $this->input->post('entry_roster_time');
        $exit_roster = $this->input->post('exit_roster_time');

        $entry_rosterDatetime = new DateTime($entry_roster);
        $exit_rosterDatetime = new DateTime($exit_roster);
        $roster_diff = $exit_rosterDatetime->diff($entry_rosterDatetime);
        $hours = $roster_diff->format('%h'); 
        $minutes = $roster_diff->format('%i');
        $roster_minute = $hours * 60 + $minutes;

        $overtime_enable = $this->input->post('overtime_enable');
        $overtime_enable = isset($overtime_enable) ? 1 : 0;
        $is_weekend = $this->input->post('is_weekend');
        $is_weekend = isset($is_weekend) ? 1 : 0;
        $is_consider = $this->input->post('is_consider');
        $is_consider = isset($is_consider) ? 1 : 0;
        $is_edited = $this->input->post('is_edited');
        $is_edited = isset($is_edited) ? 1 : 0;
        $is_approved = $this->input->post('is_approved');
        $is_approved = isset($is_approved) ? 1 : 0;

        $entry_roster_time_tol = date('Y-m-d H:i:s', strtotime($this->input->post('entry_roster_time_tol')));
        $overtime_after = date('Y-m-d H:i:s', strtotime($this->input->post('overtime_after')));
        $half_day_time = date('Y-m-d H:i:s', strtotime($this->input->post('half_day_time')));
        $entry_time = date('Y-m-d H:i:s', strtotime($this->input->post('entry_time')));
        $exit_time = date('Y-m-d H:i:s', strtotime($this->input->post('exit_time')));

        $data_roster_detail = array(
            'roster_id' => $roster_id,
            'roster_date' => $roster_date,
            'shift_id' => $this->input->post('shift_id'),
            'emp_shift_id' => $emp_mst_shift->id,
            'day_duration' => $this->input->post('day_duration'),
            'period_id' => $period_id,
            'department_id' => $department_id,
            'employee_id' => $employee_id,
            'caption' => $caption,
            'employee_caption' => $emp_full_name,
            'card_no' => $this->input->post('card_no'),
            'entry_roster_time' => date('Y-m-d H:i:s', strtotime($entry_roster)),
            'exit_roster_time' => date('Y-m-d H:i:s', strtotime($exit_roster)),
            'roster_minute' => $roster_minute,
            'entry_roster_time_tol' => $entry_roster_time_tol,
            'overtime_enable' => $overtime_enable,
            'overtime_minute' => 0,
            'overtime_after' => $overtime_after,
            'half_day_time' => $half_day_time,
            'entry_time' => $entry_time,
            'exit_time' => $exit_time,
            'start_punch_time' => $entry_time,
            'end_punch_time' => $exit_time,
            'present_status' => $this->input->post('present_status'),
            'leave_status' => $this->input->post('leave_status'),
            'duty_type' => $this->input->post('duty_type'),
            'pay_type' => $this->input->post('pay_type'),
            'salary_status' => $this->input->post('salary_status'),
            'confirm_status' => $this->input->post('confirm_status'),
            'is_weekend' => $is_weekend,
            'is_consider' => $is_consider,
            'is_edited' => $is_edited,
            'is_approved' => $is_approved,
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        // print_r($data_roster_detail); die;
        $this->roster_model->edit_roster_details($data_roster_detail, $roster_detail_id);
       }
    }

    public function get_roster_select($value='')
    {
        $response = array();
        if ($_POST['roster_name']) {
            $roster_name = $_POST['roster_name'];
            $results = $this->db->like('caption', $roster_name)->get('hrm_rosters')->result();
            foreach ($results as $key => $value) {
                $response[] = array(
                    "value"=> $value->id,
                    "label"=> $value->caption
                );
            }
            echo json_encode($response);
        }
    }   
    public function get_roster_select2($value='')
    {
        $response = array();
        if ($_POST['roster_name']) {
            $roster_name = $_POST['roster_name'];
            $results = $this->db->like('caption', $roster_name)->get('hrm_rosters')->result();
            foreach ($results as $key => $value) {
                $response[] = array(
                    "value"=> $value->id,
                    "label"=> $value->caption
                );
            }
            echo json_encode($response);
        }
    }   
    public function get_employee_select($value='')
    {
        $response = array();
        if ($_POST['name']) {
            $name = $_POST['name'];
            $results = $this->db->where('user_role_id !=', 1)->like('first_name', $name)->or_like('last_name', $name)->or_like('employee_id', $name)->or_like('contact_no', $name)->get('employees')->result();
            foreach ($results as $key => $value) {
                $response[] = array(
                    "value"=> $value->user_id,
                    "label"=> $value->first_name. ' '.$value->last_name
                );
            }
            echo json_encode($response);
        }
    }
    public function get_period_select($value='')
    {
        $response = array();
        if ($_POST['name']) {
            $name = $_POST['name'];
            $results = $this->db->like('name', $name)->get('period_master')->result();
            foreach ($results as $key => $value) {
                $response[] = array(
                    "value"=> $value->id,
                    "label"=> $value->name
                );
            }
            echo json_encode($response);
        }
    }
    public function get_department_select($value='')
    {
        $response = array();
        if ($_POST['name']) {
            $name = $_POST['name'];
            $results = $this->db->like('department_name', $name)->get('departments')->result();
            foreach ($results as $key => $value) {
                $response[] = array(
                    "value"=> $value->department_id,
                    "label"=> $value->department_name
                );
            }
            echo json_encode($response);
        }
    }

    public function xget_roster_select()
    {
        // java code starts
       /*
            int iDisplayStart = params.page ? Integer.parseInt(params.page) : 1
            int iDisplayLength = params.iDisplayLength ? params.getInt('iDisplayLength') : 30
            String sSortDir = params.sSortDir_0 ? params.sSortDir_0 : CommonUtils.SORT_ORDER_ASC
            int iSortingCol = params.iSortCol_0 ? params.getInt('iSortCol_0') : CommonUtils.DEFAULT_PAGINATION_SORT_IDX
            String sSearch = params.q ? CommonUtils.PERCENTAGE_SIGN + params.q + CommonUtils.PERCENTAGE_SIGN : null
            int offset = params.page ? Integer.parseInt(params.page) : 0
            String sortColumn = CommonUtils.getSortColumn(sortColumns, iSortingCol)
            def session = RequestContextHolder.currentRequestAttributes().getSession()
            Long organization = session.defaultDept.organizationId
            List dataReturns = new ArrayList()
            def c = FofGuest.createCriteria()
            def results = c.list(max: iDisplayLength, offset: (iDisplayStart - 1)*30) {
                params.stakHldrType ? eq("stakHldrType", params.stakHldrType) : null
                eq('organization', organization)
                if (sSearch) {
                    or {
                        ilike("code", sSearch)
                        ilike("caption", sSearch)
                        ilike("street", sSearch)
                        ilike("city", sSearch)
                        ilike("state", sSearch)
                        ilike("zipCode", sSearch)
                        ilike("email", sSearch)
                        ilike("phone", sSearch)
    
                    }
                }
                order(sortColumn, sSortDir)
            }
            int totalCount = results.totalCount
            int serial = (iDisplayStart-1)*30;
            String srcLinkThumb = ""
            if (totalCount > 0) {
                if (sSortDir.equals(CommonUtils.SORT_ORDER_DESC)) {
                    serial = iDisplayStart*30+1
                }
                results.each { FofGuest dataRow ->
                    serial++
                    if (dataRow.imagePathThumb) {
                        srcLinkThumb = grailsLinkGenerator.link(controller: "globalAccess", action: "index", params: [imagePath: dataRow.imagePathThumb, showImage: 'true'])
                    }
                    dataReturns.add([id: dataRow.id, serial: serial, imgsrc: '<img src="' + srcLinkThumb + '" alt="Blank" width="40px" height="40px"/>', code: dataRow.code, caption: commonUtilityService.getDefaultCaption(dataRow.caption, dataRow.captionAlt), phone: dataRow.phone ? dataRow.phone : '', email: dataRow.email ? dataRow.email : '', street: dataRow.street ? dataRow.street : '', city: dataRow.city ? dataRow.city : '', state: dataRow.state ? dataRow.state : '', zipCode: dataRow.zipCode ? dataRow.zipCode : ''])
                    srcLinkThumb = ''
                }
            }
            return [total_count: totalCount, incomplete_results: false, items: dataReturns]
       */
    //   print_r($this->input->post()); die; 
        //java code end
      
    }

}
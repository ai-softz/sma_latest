<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_roster_grid extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('roster_model');
        $this->load->admin_model('shift_model');
        $this->load->admin_model('employees_model');
        $this->load->admin_model('attendance_model');
        $this->load->admin_model('department_model');
        $this->load->library('form_validation');
    }

    public function roster_grid_list()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['period_masters'] = $this->roster_model->all_period_masters();
        $this->data['rosters'] = $this->roster_model->all_rosters();
        $this->data['shift_masters'] = $this->shift_model->all_shift_masters();


        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('roster')]];
        $meta = ['page_title' => lang('roster'), 'bc' => $bc];
		$this->page_construct('hrm_roster/roster_grid_list', $meta, $this->data);
    }

    public function add_roster_grid_form()
    {
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();
        $this->data['period_masters'] = $this->roster_model->all_period_masters();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_roster/add_roster_grid_form', $this->data);
    }

    public function add_roster_grid()
    {
        $employee_id = explode('-', $this->input->post('employee'));
        $employee_id = $employee_id[0];
        $roster_date = date('Y-m-d',strtotime($this->input->post('roster_date')));
        $entry_roster_time = $this->input->post('entry_roster_time');
        $entry_roster_time_input = date('Y-m-d H:i:s', strtotime($entry_roster_time));
        $exit_roster_time = $this->input->post('exit_roster_time');
        $exit_roster_time_input = date('Y-m-d H:i:s', strtotime($exit_roster_time));
        $day_duration = $this->input->post('day_duration');
        $entry_roster_time_tol = $this->input->post('entry_roster_time_tol');

        $emp_mst_shift = $this->roster_model->get_emp_shifts($employee_id);
        
        $shift_master = $this->roster_model->get_shift_master($emp_mst_shift->shift_mst_id);
        $emp_name = $this->roster_model->get_employee_by_id($employee_id);
        $emp_name = $emp_name[0];
        $caption = $emp_name->first_name. ' ' .$emp_name->last_name;
        $emp_full_name = $emp_name->first_name. ' ' .$emp_name->last_name;
        

        $entry_rosterInt = strtotime($entry_roster_time);
        $exit_rosterInt = strtotime($exit_roster_time);
        
        $roster_minute = ($exit_rosterInt - $entry_rosterInt) / 60;

        $overtime_enable = $this->input->post('overtime_enable');
        $overtime_enable = isset($overtime_enable) ? 1 : 0;
        $is_weekend = $this->input->post('is_weekend');
        $is_weekend = isset($is_weekend) ? 1 : 0;

        $auto_rostering = $emp_mst_shift->auto_rostering;
        $log_required = $emp_mst_shift->log_required;
        $logout_required = $emp_mst_shift->logout_required;
        $half_day_absent = $emp_mst_shift->half_day_absent;
        $overtime_minute = 0;

        $entry_roster = new DateTime($entry_roster_time);
        $exit_roster = new DateTime($exit_roster_time);

        if(!empty($emp_mst_shift->start_punch)) {
            $start_punch_time_add = $entry_roster->add(new DateInterval('PT'.$emp_mst_shift->start_punch.'M'));
            $start_punch_time = $start_punch_time_add->format('Y-m-d H:i:s');
        } else {
            $start_punch_time = null;
        }
        if(!empty($emp_mst_shift->end_punch)) {
            $end_punch_time_add = $exit_roster->add(new DateInterval('PT'.$emp_mst_shift->end_punch.'M'));
            $end_punch_time = $end_punch_time_add->format('Y-m-d H:i:s');
        } else {
            $end_punch_time = null;
        }
        // print_r($emp_name->department_id); echo ' / '; print_r($emp_name->department_id);die;
        $department_id = $emp_name->department_id;
        $data_roster_detail = array(
            // 'roster_id' => $roster_id,
            'roster_date' => $roster_date,
            'shift_id' => $emp_mst_shift->shift_mst_id,
            'emp_shift_id' => $emp_mst_shift->id,
            'day_duration' => $day_duration,
            // 'period_id' => $period_mst_id,
            'department_id' => $department_id,
            'employee_id' => $employee_id,
            'caption' => $caption,
            'employee_caption' => $emp_full_name,
            'employee_caption_alt' => $emp_full_name,
            'card_no' => $emp_mst_shift->card_number,
            'entry_roster_time' => $entry_roster_time_input,
            'exit_roster_time' => $exit_roster_time_input,
            'roster_minute' => $roster_minute,
            'entry_roster_time_tol' => $entry_roster_time_tol,
            'overtime_enable' => $overtime_enable,
            'auto_rostering' => $auto_rostering,
            'log_required' => $log_required,
            'logout_required' => $logout_required,
            'half_day_absent' => $half_day_absent,
            'overtime_minute' => $overtime_minute,
            // 'overtime_after' => $overtime_after,
            // 'half_day_time' => $half_day_time,
            'start_punch_time' => $start_punch_time,
            'end_punch_time' => $end_punch_time,
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        // print_r($data_roster_detail); die;
        $this->roster_model->add_roster_detail($data_roster_detail);
    }

    public function get_roster_grid()
    {
        if($this->input->post()) {
            $start_date = date('Y-m-d', strtotime($this->input->post('startDate')));
            $end_date = date('Y-m-d', strtotime($this->input->post('endDate')));
            $emp_id = explode('-', $this->input->post('empid'));
            $emp_id = $emp_id[0];
            $department_id = explode('-', $this->input->post('department'));
            $department_id = $department_id[0];

            $results = $this->roster_model->roster_grids($start_date, $end_date, $emp_id, $department_id);
            $rosters_employees = array();
            
            foreach($results as $row) {
                $roster = array(
                    'id' => $row->id,
                    'roster_date' => $row->roster_date,
                    'entry_roster_time' => $row->entry_roster_time,
                    'exit_roster_time' => $row->exit_roster_time,
                    'confirm_status' => $row->confirm_status
                ) ; 
                $rosters_employees[$row->employee_id][] = $roster; 
            }
            $html = '';
            $html .= '<div class="table-responsive">';
            $html .= '<table class="table table-bordered table-hover table-striped">';

            $html .= '<tr>';
            $html .= '<th>'. 'Employee' .'</th>';
            $startDate = $start_date;
            while($startDate <= $end_date) {
                $html .= '<th>'. date('D d M, Y', strtotime($startDate)).'</th>';
                $startDate = date('Y-m-d', strtotime('+1 day', strtotime($startDate))); 
            }
            $html .= '</tr>';

            foreach($rosters_employees as $emp_id => $rosters_employee) {
                
                $emp_name = $this->roster_model->get_employee_by_id($emp_id);
                $emp_name = $emp_name[0];
                $html .= '<tr>';
                $html .= '<td>'. $emp_name->first_name.' '.$emp_name->last_name.'</td>';
                $startDate2 = $start_date;

                while($startDate2 <= $end_date) {
                    if(!empty($rosters_employee)) {
                        foreach($rosters_employee as $key=>$row) {
                            
                            $row_roster_date = date('Y-m-d', strtotime($row['entry_roster_time']));
                            if($startDate2 === $row_roster_date) {
                                $entry_roster_time = $row['entry_roster_time'];
                                $exit_roster_time = $row['exit_roster_time'];
                                unset($rosters_employee[$key]);
                                break;
                            } else {
                                $entry_roster_time = '';
                            }
                        }
                    } else {
                        $entry_roster_time = '';
                    }
                    if($entry_roster_time == '') {
                        $html .= '<td>'.'-'.'</td>';
                    }else {
                        $html .= '<td>'. date('h:i a',strtotime($entry_roster_time)).' - '.date('h:i a',strtotime($exit_roster_time)).'</td>';
                    }
                    $startDate2 = date('Y-m-d', strtotime('+1 day', strtotime($startDate2)));
                }
                $html .= '</tr>';
            }
            $html .= '</table>';
            $html .= '</div>';
            echo $html;

        } 
    }

    public function get_roster_grid_default()
    {
            $start_date = date('Y-m-d', strtotime('-6 days', time()));
            $end_date = date('Y-m-d', time());
            $results = $this->roster_model->roster_grids($start_date, $end_date);

            $rosters_employees = array();
            
            foreach($results as $row) {
                $roster = array(
                    'id' => $row->id,
                    'roster_date' => $row->roster_date,
                    'entry_roster_time' => $row->entry_roster_time,
                    'exit_roster_time' => $row->exit_roster_time,
                    'confirm_status' => $row->confirm_status
                ) ; 
                $rosters_employees[$row->employee_id][] = $roster;
            }
            $html = '';
            $html .= '<div class="table-responsive">';
            $html .= '<table class="table table-bordered table-hover table-striped">';

            $html .= '<tr>';
            $html .= '<th>'. 'Employee' .'</th>';
            $startDate = $start_date;
            while($startDate <= $end_date) {
                $html .= '<th>'. date('D d M, Y', strtotime($startDate)).'</th>';
                $startDate = date('Y-m-d', strtotime('+1 day', strtotime($startDate))); 
            }
            $html .= '</tr>';
            foreach($rosters_employees as $emp_id => $rosters_employee2) {
                
                $emp_name = $this->roster_model->get_employee_by_id($emp_id);
                $emp_name = $emp_name[0];
                $html .= '<tr>';
                $html .= '<td>'. $emp_name->first_name.' '.$emp_name->last_name.'</td>';
                $startDate2 = $start_date;

                while($startDate2 <= $end_date) {
                    if(!empty($rosters_employee2)) {
                        foreach($rosters_employee2 as $key=>$row) {

                            $row_roster_date = date('Y-m-d', strtotime($row['entry_roster_time']));
                            if($startDate2 === $row_roster_date) {
                                $entry_roster_time = $row['entry_roster_time'];
                                $exit_roster_time = $row['exit_roster_time'];
                                unset($rosters_employee2[$key]);
                                break;
                            } else {
                                $entry_roster_time = '';
                            }
                        }
                    } else {
                        $entry_roster_time = '';
                    }
                    if($entry_roster_time == '') {
                        $html .= '<td>'.'-'.'</td>';
                    }else {
                        $html .= '<td>'. date('h:i a',strtotime($entry_roster_time)).' - '.date('h:i a',strtotime($exit_roster_time)).'</td>';
                    }
                    $startDate2 = date('Y-m-d', strtotime('+1 day', strtotime($startDate2)));
                }
                $html .= '</tr>';
            }
            $html .= '</table>';
            $html .= '</div>';
            echo $html;
        }

}
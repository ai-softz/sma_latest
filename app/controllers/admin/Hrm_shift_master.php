<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_shift_master extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('shift_model');
        $this->load->admin_model('employees_model');
        $this->load->admin_model('attendance_model');
        $this->load->admin_model('department_model');
        $this->load->library('form_validation');
    }

    public function shift_master()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('period_master')]];
        $meta = ['page_title' => lang('shift_master'), 'bc' => $bc];
		$this->page_construct('hrm_shift_master/shift_master', $meta, $this->data);
    }

    public function getShiftMaster()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->shift_model->get_shift_master($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);    
        $data = [];
        foreach($records as $value) {
            $nestedData = array();
				$nestedData[] = $value->id;
				$nestedData[] = $value->name;
				$nestedData[] = $value->name_alt;
				$nestedData[] = $value->start_time . ' to ' . $value->end_time;
				$nestedData[] = $value->weekly_holiday;
                $nestedData[] = $value->day_duration;
                $nestedData[] = $value->late_tolerance;
                $nestedData[] = $value->start_punch;
                $nestedData[] = $value->end_punch;

				$edit = '';
				$edit .= " <a href='" . admin_url('hrm_shift_master/edit_shift_master/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_shift_master') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_shift_master') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_shift_master/delete_shift_master/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->shift_model->total_shift_master($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }

    public function delete_shift_master($shift_master_id)
     {
        if ($this->shift_model->delete_shift_master($shift_master_id)) {

            $this->shift_model->delete_shift_details($shift_master_id);
            $this->sma->send_json(['error' => 0, 'msg' => lang('shift_master_deleted')]);
        }
     }

    public function shift_master_form()
    {
        $this->data['all_employees'] = $this->employees_model->all_employees();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_shift_master/shift_master_form', $this->data);
    }
    public function add_shift_master()
    {
       // print_r($this->input->post());die;
       if($this->input->post()) {

        $name = $this->input->post('name');
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');

        $holidays = $this->input->post('weekly_holiday');
        $weekly_holidays = implode(',', $holidays);

        $data_shift_master = array(
            'name' => $name,
            'name_alt' => $this->input->post('name_alt'),
            'start_time' => $start_time,
            'end_time' => $end_time,
            'weekly_holiday' => $weekly_holidays,
            'late_tolerance' => $this->input->post('late_tolerance'),
            'half_day_leave' => $this->input->post('half_day_leave'),
            'start_punch' => $this->input->post('start_punch'),
            'end_punch' => $this->input->post('end_punch'),
            'day_duration' => $this->input->post('day_duration'),
            'remark' => $this->input->post('remark'),
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        $shift_master_id = $this->shift_model->add_shift_master($data_shift_master);
        // $shift_master_id = 1;
// print_r($data_shift_master);
        $week_days = array('FRIDAY', 'SATURDAY', 'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY');
        foreach($week_days as $day) {

            $caption_shiftDtl = $name .' '. $day;
            $is_holiday = in_array($day, $holidays) ? 1 : 0;
            $data_shiftDtl = array(
                'shift_master_id' => $shift_master_id,
                'name' => $caption_shiftDtl,
                'name_alt' => $this->input->post('name_alt'),
                'date_day' =>  $day,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'day_duration' => $this->input->post('day_duration'),
                'is_holiday' => $is_holiday,
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
// print_r($data_shiftDtl); echo '</br>';           
            $this->shift_model->add_shift_detail($data_shiftDtl);
            
        }
// die;
       }
   
    }

    public function edit_shift_master($id)
    {
        $shift_master = $this->shift_model->read_shift_master($id);
        $this->data['shift_master'] = $shift_master;
        $this->data['shift_details'] = $this->shift_model->read_shift_details($id);
        
		$this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme.'hrm_shift_master/edit_shift_master', $this->data);
    }
    public function edit_shift_detail()
    {
        $shift_detail_id = $this->input->post('id');
        $data = array(
            'start_time' =>$this->input->post('startTime'),
            'end_time' => $this->input->post('endTime'),
            'day_duration' => $this->input->post('dayDuration'),
            'is_holiday' => $this->input->post('isHoliday')
        );
        $update = $this->shift_model->edit_shift_detail($data, $shift_detail_id);
        if($update) {
            $this->sma->send_json(array('isError' => false, 'msg' => 'Record updated successfully!'));
        } else {
            $this->sma->send_json(array('isError' => true, 'msg' => 'Something went wrong!'));
        }
    }

    public function editShiftMaster()
    {
        if($this->input->post()) {
// print_r($this->input->post()); die;
            $shift_master_id = $this->input->post('shift_master_id');
            $name = $this->input->post('name');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
    
            $holidays = $this->input->post('weekly_holiday');
            $weekly_holidays = implode(',', $holidays);
    
            $data_shift_master = array(
                'name' => $name,
                'name_alt' => $this->input->post('name_alt'),
                'start_time' => $start_time,
                'end_time' => $end_time,
                'weekly_holiday' => $weekly_holidays,
                'late_tolerance' => $this->input->post('late_tolerance'),
                'half_day_leave' => $this->input->post('half_day_leave'),
                'start_punch' => $this->input->post('start_punch'),
                'end_punch' => $this->input->post('end_punch'),
                'day_duration' => $this->input->post('day_duration'),
                'remark' => $this->input->post('remark'),
            );
            $update = $this->shift_model->editShiftMaster($data_shift_master, $shift_master_id);
            if($update) {
                $this->sma->send_json(array('isError' => false, 'msg' => 'Record updated successfully!'));
            } else {
                $this->sma->send_json(array('isError' => true, 'msg' => 'Something went wrong!'));
            }
        }
    }

    public function employee_shift_mapping()
    {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')]];
        $meta = ['page_title' => lang('employee_shift_mapping'), 'bc' => $bc];
		$this->page_construct('hrm_shift_master/employee_shift_mapping', $meta, $this->data);
    }
    public function shift_mapping_form()
    {
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();
        $this->data['shift_masters'] = $this->shift_model->all_shift_masters();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_shift_master/shift_mapping_form', $this->data);
    }
    
    public function get_shift_details()
    {
        if($this->input->post()) 
        {
            $shift_mst_id = $this->input->post('shift_mst_id');
            $shift_details = $this->shift_model->read_shift_details($shift_mst_id);
            $shift_master = $this->shift_model->read_shift_master($shift_mst_id);
            
            $shift_data = []; 

            if(!empty($shift_details)) {

            $shift_data['work_start_time'] = $shift_master->start_time;
            $shift_data['work_end_time'] = $shift_master->end_time;
            $shift_data['late_tolerance'] = $shift_master->late_tolerance;
            $shift_data['half_day_leave'] = $shift_master->half_day_leave;
            $shift_data['start_punch'] = $shift_master->start_punch;
            $shift_data['end_punch'] = $shift_master->end_punch;
            $shift_data['half_day_absent'] = $shift_master->half_day_absent;       

            foreach($shift_details as $row) {

                if($row->date_day == 'FRIDAY') {
                    $shift_data['fri_start_time'] = $row->start_time;
                    $shift_data['fri_end_time'] = $row->end_time;
                    $shift_data['fri_off'] = $row->is_holiday;
                }
                else if($row->date_day == 'SATURDAY') {
                    $shift_data['sat_start_time'] = $row->start_time;
                    $shift_data['sat_end_time'] = $row->end_time;
                    $shift_data['sat_off'] = $row->is_holiday;
                }
                else if($row->date_day == 'SUNDAY') {
                    $shift_data['sun_start_time'] = $row->start_time;
                    $shift_data['sun_end_time'] = $row->end_time;
                    $shift_data['sun_off'] = $row->is_holiday;
                }
                else if($row->date_day == 'MONDAY') {
                    $shift_data['mon_start_time'] = $row->start_time;
                    $shift_data['mon_end_time'] = $row->end_time;
                    $shift_data['mon_off'] = $row->is_holiday;
                }
                else if($row->date_day == 'TUESDAY') {
                    $shift_data['tues_start_time'] = $row->start_time;
                    $shift_data['tues_end_time'] = $row->end_time;
                    $shift_data['tues_off'] = $row->is_holiday;
                }
                else if($row->date_day == 'WEDNESDAY') {
                    $shift_data['wed_start_time'] = $row->start_time;
                    $shift_data['wed_end_time'] = $row->end_time;
                    $shift_data['wed_off'] = $row->is_holiday;
                }
                else if($row->date_day == 'THURSDAY') {
                    $shift_data['thurs_start_time'] = $row->start_time;
                    $shift_data['thurs_end_time'] = $row->end_time;
                    $shift_data['thurs_off'] = $row->is_holiday;
                }
            }
                echo json_encode(array('shift_data' => $shift_data, 'isError' => 0));
            }
            else {
                echo json_encode(array('shift_data' => $shift_data, 'isError' => 1));
            }
            
        }
    }

    public function add_emp_shift_map()
    {
       // print_r($this->input->post());die;
       if($this->input->post()) {

        $emp_id = $this->input->post('employee');
        $emp_shift_exist = $this->shift_model->get_emp_shift($emp_id);

        if(!$emp_shift_exist) {

        $auto_rostering = $this->input->post('auto_rostering');
        $auto_rostering = isset($auto_rostering) ? 1 : 0;
        $log_required = $this->input->post('log_required');
        $log_required = isset($log_required) ? 1 : 0;
        $logout_required = $this->input->post('logout_required');
        $logout_required = isset($logout_required) ? 1 : 0;
        $half_day_absent = $this->input->post('half_day_absent');
        $half_day_absent = isset($half_day_absent) ? 1 : 0;
        $overtime_enable = $this->input->post('overtime_enable');
        $overtime_enable = isset($overtime_enable) ? 1 : 0;

        $fri_off = $this->input->post('fri_off');
        $fri_off = isset($fri_off) ? 1 : 0;
        $sat_off = $this->input->post('sat_off');
        $sat_off = isset($sat_off) ? 1 : 0;
        $sun_off = $this->input->post('sun_off');
        $sun_off = isset($sun_off) ? 1 : 0;
        $mon_off = $this->input->post('mon_off');
        $mon_off = isset($mon_off) ? 1 : 0;
        $tues_off = $this->input->post('tues_off');
        $tues_off = isset($tues_off) ? 1 : 0;
        $wed_off = $this->input->post('wed_off');
        $wed_off = isset($wed_off) ? 1 : 0;
        $thurs_off = $this->input->post('thurs_off');
        $thurs_off = isset($thurs_off) ? 1 : 0;

        $shift_master = $this->shift_model->read_shift_master($this->input->post('shift_mst_id'));
        $emp_name = $this->employees_model->get_emp_name($this->input->post('employee'));
        $name = $emp_name->first_name.' '.$emp_name->last_name . '-' . $shift_master->name;

        $data_emp_shift_map = array(
            'shift_mst_id' => $this->input->post('shift_mst_id'),
            'department_id' => $this->input->post('department'),
            'employee_id' => $this->input->post('employee'),
            'card_number' => $this->input->post('card_number'),
            'name' => $name,
            'name_alt' => $name,
            'work_start_time' => $this->input->post('work_start_time'),
            'work_end_time' => $this->input->post('work_end_time'),
            'late_tolerance' => $this->input->post('late_tolerance'),
            'start_punch' => $this->input->post('start_punch'),
            'end_punch' => $this->input->post('end_punch'),
            'half_day_leave' => $this->input->post('half_day_leave'),
            'auto_rostering' => $auto_rostering,
            'log_required' => $log_required,
            'logout_required' => $logout_required,
            'overtime_enable' => $overtime_enable,
            'half_day_absent' => $half_day_absent,
            'overtime_after' => $this->input->post('overtime_after'),
            'fri_start_time' => $this->input->post('fri_start_time'),
            'fri_end_time' => $this->input->post('fri_end_time'),
            'sat_start_time' => $this->input->post('sat_start_time'),
            'sat_end_time' => $this->input->post('sat_end_time'),
            'sun_start_time' => $this->input->post('sun_start_time'),
            'sun_end_time' => $this->input->post('sun_end_time'),
            'mon_start_time' => $this->input->post('mon_start_time'),
            'mon_end_time' => $this->input->post('mon_end_time'),
            'tues_start_time' => $this->input->post('tues_start_time'),
            'tues_end_time' => $this->input->post('tues_end_time'),
            'wed_start_time' => $this->input->post('wed_start_time'),
            'wed_end_time' => $this->input->post('wed_end_time'),
            'thurs_start_time' => $this->input->post('thurs_start_time'),
            'thurs_end_time' => $this->input->post('thurs_end_time'),
            'fri_off' => $fri_off,
            'sat_off' => $sat_off,
            'sun_off' => $sun_off,
            'mon_off' => $mon_off,
            'tues_off' => $tues_off,
            'wed_off' => $wed_off,
            'thurs_off' => $thurs_off,
            //'remark' => $this->input->post('remark'),
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        //print_r($data_emp_shift_map); die; 
        $shift_master_id = $this->shift_model->add_emp_shift_map($data_emp_shift_map);
        }
        else {
            return $this->sma->send_json(array('error' => 1, 'message' => lang('emp_shift_already_exit')));
        }
       }
   
    }

    public function edit_emp_shift_map()
    {
        // print_r($this->input->post());die;
       if($this->input->post()) {

        $emp_shift_id = $this->input->post('emp_shift_id');

        $auto_rostering = $this->input->post('auto_rostering');
        $auto_rostering = isset($auto_rostering) ? 1 : 0;
        $log_required = $this->input->post('log_required');
        $log_required = isset($log_required) ? 1 : 0;
        $logout_required = $this->input->post('logout_required');
        $logout_required = isset($logout_required) ? 1 : 0;
        $half_day_absent = $this->input->post('half_day_absent');
        $half_day_absent = isset($half_day_absent) ? 1 : 0;
        $overtime_enable = $this->input->post('overtime_enable');
        $overtime_enable = isset($overtime_enable) ? 1 : 0;

        $fri_off = $this->input->post('fri_off');
        $fri_off = isset($fri_off) ? 1 : 0;
        $sat_off = $this->input->post('sat_off');
        $sat_off = isset($sat_off) ? 1 : 0;
        $sun_off = $this->input->post('sun_off');
        $sun_off = isset($sun_off) ? 1 : 0;
        $mon_off = $this->input->post('mon_off');
        $mon_off = isset($mon_off) ? 1 : 0;
        $tues_off = $this->input->post('tues_off');
        $tues_off = isset($tues_off) ? 1 : 0;
        $wed_off = $this->input->post('wed_off');
        $wed_off = isset($wed_off) ? 1 : 0;
        $thurs_off = $this->input->post('thurs_off');
        $thurs_off = isset($thurs_off) ? 1 : 0;

        $shift_master = $this->shift_model->read_shift_master($this->input->post('shift_mst_id'));
        $emp_name = $this->employees_model->get_emp_name($this->input->post('employee'));
        $name = $emp_name->first_name.' '.$emp_name->last_name . '-' . $shift_master->name;

        $data_emp_shift_map = array(
            'shift_mst_id' => $this->input->post('shift_mst_id'),
            'department_id' => $this->input->post('department'),
            'employee_id' => $this->input->post('employee'),
            'card_number' => $this->input->post('card_number'),
            'name' => $name,
            'name_alt' => $name,
            'work_start_time' => $this->input->post('work_start_time'),
            'work_end_time' => $this->input->post('work_end_time'),
            'late_tolerance' => $this->input->post('late_tolerance'),
            'start_punch' => $this->input->post('start_punch'),
            'end_punch' => $this->input->post('end_punch'),
            'half_day_leave' => $this->input->post('half_day_leave'),
            'auto_rostering' => $auto_rostering,
            'log_required' => $log_required,
            'logout_required' => $logout_required,
            'overtime_enable' => $overtime_enable,
            'half_day_absent' => $half_day_absent,
            'overtime_after' => $this->input->post('overtime_after'),
            'fri_start_time' => $this->input->post('fri_start_time'),
            'fri_end_time' => $this->input->post('fri_end_time'),
            'sat_start_time' => $this->input->post('sat_start_time'),
            'sat_end_time' => $this->input->post('sat_end_time'),
            'sun_start_time' => $this->input->post('sun_start_time'),
            'sun_end_time' => $this->input->post('sun_end_time'),
            'mon_start_time' => $this->input->post('mon_start_time'),
            'mon_end_time' => $this->input->post('mon_end_time'),
            'tues_start_time' => $this->input->post('tues_start_time'),
            'tues_end_time' => $this->input->post('tues_end_time'),
            'wed_start_time' => $this->input->post('wed_start_time'),
            'wed_end_time' => $this->input->post('wed_end_time'),
            'thurs_start_time' => $this->input->post('thurs_start_time'),
            'thurs_end_time' => $this->input->post('thurs_end_time'),
            'fri_off' => $fri_off,
            'sat_off' => $sat_off,
            'sun_off' => $sun_off,
            'mon_off' => $mon_off,
            'tues_off' => $tues_off,
            'wed_off' => $wed_off,
            'thurs_off' => $thurs_off,
            //'remark' => $this->input->post('remark'),
            'created_by' => $this->session->userdata('user_id'),
            'created_at' => date('Y-m-d')
        );
        // print_r($data_shift_master); die; 
        $update = $this->shift_model->edit_emp_shift_map($data_emp_shift_map, $emp_shift_id);
        if($update) {
            return true;
        } else {
            return false;
        }
        
       }
    }

    public function get_dept_employees()
    {
        if($this->input->post())
        {
            $dept_id = $this->input->post('dept_id');
            $dept_employees = $this->db->where('department_id', $dept_id)->get('employees')->result();
// print_r($dept_id); print_r($dept_employees); die;
            $output = '';
            $output .= '<select name="employee" id="employee_id" class="form-control select" required="required">
                <option value="">Select</option>';
                foreach($dept_employees as $row) { 
                    $output .= '<option value="'.$row->user_id.'">'.$row->first_name. ' '.$row->last_name.'</option>';
                } 
            $output .= '</select>';
            echo $output;
        }
    }
    public function get_employees_card()
    {
        
        if($this->input->post())
        {
            $employee_id = $this->input->post('employee_id');
            $employee_id = $this->db->where('user_id', $employee_id)->get('employees')->row();
            echo $employee_id->employee_id;
        }
    }
    public function getEmployeeShiftMap()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->shift_model->get_employee_shift($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);    
        $data = [];
        foreach($records as $value) {
            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $dept = $this->shift_model->read_department_information($value->department_id);
            $shift_master = $this->shift_model->read_shift_master($value->shift_mst_id);
            $nestedData = array();
				$nestedData[] = $value->id;
				$nestedData[] = $emp_name->first_name.' '.$emp_name->last_name;
				$nestedData[] = $dept->department_name;
				$nestedData[] = $shift_master->name;
				$nestedData[] = $value->name;
				$nestedData[] = $value->log_required ? 'YES' : 'NO';
				$nestedData[] = $value->start_punch;
                $nestedData[] = $value->end_punch;

				$edit = '';
				$edit .= " <a href='" . admin_url('hrm_shift_master/edit_employee_shift_map/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_employee_shift_map') . "'><i class=\"fa fa-edit\"></i></a>";
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_employee_shift_map') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('hrm_shift_master/delete_employee_shift_map/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->shift_model->total_employee_shift($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function edit_employee_shift_map($id='')
    {
        $this->data['employee_shift'] = $this->shift_model->read_employee_shift_map($id);
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();
        $this->data['shift_masters'] = $this->shift_model->all_shift_masters();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'hrm_shift_master/edit_employee_shift_map', $this->data);
    }
    public function delete_employee_shift_map($id)
     {
        if ($this->shift_model->delete_employee_shift_map($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('employee_shift_map_deleted')]);
        }
     }
}
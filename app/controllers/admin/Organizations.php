<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Organizations extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('department_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('employees_model');
	}

	public function departments_list()
	{
        $this->data['all_warehouses'] = $this->settings_model->getAllWarehouses();
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('organizations'), 'page' => lang('organizations')], ['link' => '#', 'page' => lang('organizations')]];
        $meta = ['page_title' => lang('organizations'), 'bc' => $bc];
		$this->page_construct('organizations/departments_list', $meta, $this->data);
	}
    public function designations_list()
	{
        $this->data['all_warehouses'] = $this->settings_model->getAllWarehouses();
        $this->data['all_employees'] = $this->employees_model->all_employees();
        $this->data['all_departments'] = $this->department_model->all_departments();

		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('organizations'), 'page' => lang('organizations')], ['link' => '#', 'page' => lang('organizations')]];
        $meta = ['page_title' => lang('organizations'), 'bc' => $bc];
		$this->page_construct('organizations/designations_list', $meta, $this->data);
	}

}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('employees_model');
        $this->load->admin_model('payroll_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('designation_model');
        $this->load->admin_model('department_model');
		$this->load->library('form_validation');
    }

	public function getDeptSelect()
	{
		// print_r($this->input->post('warehoue_id'));die;
		if($this->input->post()) {
			$warehouse_id = $this->input->post('warehoue_id');
			$departments = $this->db->where('location_id', $warehouse_id)->get('departments')->result();

			$html = '';
			$html .= '<select name="department_id" class="form-control select" id="departmentId" style="width:100%"> 
				<option value="0">All</option>';
			foreach($departments as $row) {
				$html .= '<option value="'.$row->department_id.'">'.$row->department_name.'</option>';
			}
			$html .= '</select>';

			echo $html;
		}
	}
	// public function payslip_delete_all($id) {
		
	// 	$this->payroll_model->delete_payslip($id);
		
	// }
	public function add_pay_to_all()
	{
		if($this->input->post()) {

		if($this->input->post('warehouse_id') == 0 && $this->input->post('department_id') == 0) {	
			$result = $this->payroll_model->get_all_employees();
		} else if($this->input->post('warehouse_id') != 0 && $this->input->post('department_id') == 0 ) {
			$result = $this->payroll_model->get_warehouse_employees($this->input->post('warehouse_id'));
		} else if($this->input->post('warehouse_id') != 0 && $this->input->post('department_id') != 0 ) {
			$result = $this->payroll_model->get_warehouse_dept_employees($this->input->post('department_id'), $this->input->post('warehouse_id'));
		}
		foreach($result as $empid) {
			$user_id = $empid->user_id;
			$user = $this->employees_model->read_user_info($user_id);			
			/* Server side PHP input validation */
			if($empid->wages_type==1){
				$basic_salary = $empid->basic_salary;
			} else {
				$basic_salary = $empid->daily_wages;
			}
			$pay_count = $this->payroll_model->read_make_payment_payslip_check($user_id,$this->input->post('month_year'));
			if($pay_count->num_rows() > 0){
				$pay_val = $this->payroll_model->read_make_payment_payslip($user_id,$this->input->post('month_year'));
				$this->delete_payslip_all($pay_val[0]->payslip_id);
			}
			if($basic_salary > 0) {
				
				// get designation
				$designation = $this->designation_model->read_designation_information($user[0]->designation_id);
				if(!is_null($designation)){
					$designation_id = $designation[0]->designation_id;
				} else {
					$designation_id = 1;	
				}
				// department
				$department = $this->department_model->read_department_information($user[0]->department_id);
				if(!is_null($department)){
					$department_id = $department[0]->department_id;
				} else {
					$department_id = 1;	
				}
				// warehouse
				$warehouse_id = $this->db->where('id', $user[0]->warehouse_id)->get('warehouses')->row()->id;
				
				$salary_allowances = $this->payroll_model->read_salary_allowances($user_id);
				$count_allowances = $this->payroll_model->count_employee_allowances($user_id);
				$allowance_amount = 0;
				if($count_allowances > 0) {
					foreach($salary_allowances as $sl_allowances){
						$allowance_amount += $sl_allowances->allowance_amount;
					}
				} else {
					$allowance_amount = 0;
				}
				// 3: all loan/deductions
				$salary_loan_deduction = $this->payroll_model->read_salary_loan_deductions($user_id);
				$count_loan_deduction = $this->payroll_model->count_employee_deductions($user_id);
				$loan_de_amount = 0;
				if($count_loan_deduction > 0) {
					foreach($salary_loan_deduction as $sl_salary_loan_deduction){
						$loan_de_amount += $sl_salary_loan_deduction->loan_deduction_amount;
					}
				} else {
					$loan_de_amount = 0;
				}
				
				
				// 5: overtime
				$salary_overtime = $this->payroll_model->read_salary_overtime($user_id);
				$count_overtime = $this->payroll_model->count_employee_overtime($user_id);
				$overtime_amount = 0;
				if($count_overtime > 0) {
					foreach($salary_overtime as $sl_overtime){
						$overtime_total = $sl_overtime->overtime_hours * $sl_overtime->overtime_rate;
						$overtime_amount += $overtime_total;
					}
				} else {
					$overtime_amount = 0;
				}
				
				
				
				// 6: statutory deductions
				// 4: other payment
				$other_payments = $this->payroll_model->set_employee_other_payments($user_id);
				$other_payments_amount = 0;
				if(!is_null($other_payments)):
					foreach($other_payments->result() as $sl_other_payments) {
						$other_payments_amount += $sl_other_payments->payments_amount;
					}
				endif;
				// all other payment
				$all_other_payment = $other_payments_amount;
				// 5: commissions
				$commissions = $this->payroll_model->set_employee_commissions($user_id);
				if(!is_null($commissions)):
					$commissions_amount = 0;
					foreach($commissions->result() as $sl_commissions) {
						$commissions_amount += $sl_commissions->commission_amount;
					}
				endif;
				// 6: statutory deductions
				$statutory_deductions = $this->payroll_model->set_employee_statutory_deductions($user_id);
				if(!is_null($statutory_deductions)):
					$statutory_deductions_amount = 0;
					foreach($statutory_deductions->result() as $sl_statutory_deductions) {
						if($sl_statutory_deductions->statutory_options == 1):
							$sta_salary = $basic_salary;
							$st_amount = $sta_salary / 100 * $sl_statutory_deductions->deduction_amount;
							$statutory_deductions_amount += $st_amount;
						else:
							$statutory_deductions_amount += $sl_statutory_deductions->deduction_amount;
						endif;
					}
				endif;
				
				// add amount
				$add_salary = $allowance_amount + $basic_salary + $overtime_amount + $other_payments_amount + $commissions_amount;
				// add amount
				$net_salary_default = $add_salary - $loan_de_amount - $statutory_deductions_amount;
				$net_salary = $net_salary_default;
				$net_salary = number_format((float)$net_salary, 2, '.', '');
				$jurl = random_string('alnum', 40);		
				$data = array(
				'employee_id' => $user_id,
				'department_id' => $department_id,
				//'company_id' => $user[0]->company_id,
				'location_id' => $warehouse_id,
				'designation_id' => $designation_id,
				'salary_month' => $this->input->post('month_year'),
				'basic_salary' => $basic_salary,
				'net_salary' => $net_salary,
				'wages_type' => $empid->wages_type,
				'total_allowances' => $allowance_amount,
				'total_loan' => $loan_de_amount,
				'total_overtime' => $overtime_amount,
				'total_commissions' => $commissions_amount,
				'total_statutory_deductions' => $statutory_deductions_amount,
				'total_other_payments' => $other_payments_amount,
				'is_payment' => '1',
				'payslip_type' => 'full_monthly',
				'payslip_key' => $jurl,
				'year_to_date' => date('d-m-Y'),
				'created_at' => date('d-m-Y h:i:s')
				);
				$result = $this->payroll_model->add_salary_payslip($data);	
				
				if ($result) {
					$date = date('Y-m-d H:i:s');
					$category_id = 1; // For Salary category_id = 1
					$amount = $net_salary;
					$data_expense = [
					'date'         => $date,
					'reference'    => $jurl,
					'amount'       => $amount,
					'created_by'   => $this->session->userdata('user_id'),
					'note'         => 'Salary Expense',
					'category_id'  => $category_id,
					'warehouse_id' => $warehouse_id,
					];
					$this->purchases_model->addExpense($data_expense);
					
				} else {
					$this->sma->send_json(['success' => 0, 'msg' => lang('something_went_wrong')]);
				}
				
			
			} // if basic salary
			
		}
		
		$this->sma->send_json(['success' => 1, 'msg' => lang('bulk_payment_successfully_completed')]);
	}

	}
	public function delete_payslip_all($payslip_id)
	{
		 $payslip_key = $this->db->select('payslip_key')->get_where('salary_payslips', array('payslip_id'=>$payslip_id))->row();
		if($this->payroll_model->delete_payslip($payslip_id)) {
			$this->payroll_model->delete_salary_expense($payslip_key->payslip_key);
     	}
	}

    public function generate_payslip()
    {
    	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

		$this->data['all_employees'] = $this->employees_model->all_employees();
		$this->data['all_warehouses'] = $this->db->get('warehouses')->result();

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => '#', 'page' => lang('payslip_list')]];
        $meta = ['page_title' => lang('payslip_list'), 'bc' => $bc];
		$this->page_construct('payroll/generate_payslip', $meta, $this->data);
    }

	public function getAllPayslipsOfEmp($emp_id='')
     {
     	$this->load->library('datatables');
        $this->datatables
			->select('payslip_key,  net_salary, salary_month, created_at, status')
			->from('salary_payslips')
			->where('status', 2)
			->where('employee_id', $emp_id)
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('payroll/payslip/id/$1') . "' class='tip' target='_blank' title='" . lang('view_payment') . "'><i class=\"fa fa-file-text\"></i></a></div>", 'payslip_key')
			;

     	echo $this->datatables->generate();
     }

   	// payslip > employees
	public function payslip_list() 
	{
		
		$iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
		$filter_emp = $this->input->post('empid');
		$filter_salaryMonth = $this->input->post('salaryMonth');
		$filter_salaryMonth = date('Y-m', strtotime($filter_salaryMonth));

		$records = $this->payroll_model->get_employees_payslip($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $filter_emp, $filter_salaryMonth);

		// $p_date = $this->input->get("month_year") ? $this->input->get("month_year") : date('Y-m');
// echo "<pre>"; print_r($p_date); die;

		if(!empty($filter_salaryMonth) && $filter_salaryMonth != '1970-01') {
			$p_date = $filter_salaryMonth;
		} else {
			$p_date = date('Y-m');
		}
		$i = 0;
		$data = array();
		if (isset($records) && count($records) > 0) {
			foreach ($records as $key => $value) {

				// ___get total hours > worked > employee___	

				$total_hours_worked = $this->payroll_model->total_hours_worked($value->user_id,$p_date);
				// echo "<pre>"; print_r($total_hours_worked->result()); die;
				$hrs_old_int1 = 0;
				$pcount = 0;
				$Trest = 0;
				$total_time_rs = 0;
				$hrs_old_int_res1 = 0;
				foreach ($total_hours_worked->result() as $hour_work){
					// total work			
					$clock_in =  new DateTime($hour_work->clock_in);
					$clock_out =  new DateTime($hour_work->clock_out);
					$interval_late = $clock_in->diff($clock_out);
					$hours_r  = $interval_late->format('%h');
					$minutes_r = $interval_late->format('%i');			
					$total_time = $hours_r .":".$minutes_r.":".'00';
					
					$str_time = $total_time;
					$str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
					sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
					$hrs_old_seconds = $hours * 3600 + $minutes * 60 + $seconds;
					$hrs_old_int1 += $hrs_old_seconds;
					
					$pcount = gmdate("H", $hrs_old_int1);			
				}
				$pcount = $pcount;
				// end ___get total hours > worked > employee___

				// 1: salary type
				if($value->wages_type==1){
					$wages_type = $this->lang->line('payroll_basic_salary');
					$basic_salary = $value->basic_salary;
					$p_class = 'emo_monthly_pay';
					$view_p_class = 'payroll_template_modal';
				} else if($value->wages_type==2){
					$wages_type = lang('employee_daily_wages');
					if($pcount > 0){
						$basic_salary = $pcount * $value->basic_salary;
					} else {
						$basic_salary = $pcount;
					}
					$p_class = 'emo_hourly_pay';
					$view_p_class = 'hourlywages_template_modal';
				} else {
					$wages_type = lang('payroll_basic_salary');
					$basic_salary = $value->basic_salary;
					$p_class = 'emo_monthly_pay';
					$view_p_class = 'payroll_template_modal';
					
				}	
				//echo ";"; print_r($value->user_id); echo "-"; print_r($pcount); echo "-";print_r($basic_salary); if($value->user_id == 24) die;
				// 2: all allowances
				$salary_allowances = $this->payroll_model->read_salary_allowances($value->user_id);
				$count_allowances = $this->payroll_model->count_employee_allowances($value->user_id);
				$allowance_amount = 0;
				if($count_allowances > 0) {
					foreach($salary_allowances as $sl_allowances){
						if($this->Settings->is_half_monthly==1){
					  	 if($this->Settings->half_deduct_month==2){
							 $eallowance_amount = $sl_allowances->allowance_amount/2;
						 } else {
							 $eallowance_amount = $sl_allowances->allowance_amount;
						 }
						  $allowance_amount += $eallowance_amount;
                      } else {
						  //$eallowance_amount = $sl_allowances->allowance_amount;
						  if($sl_allowances->is_allowance_taxable == 1) {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount;
							  } else {
								  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
							  }
							 $allowance_amount -= $iallowance_amount; 
						  } else if($sl_allowances->is_allowance_taxable == 2) {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount / 2;
							  } else {
								  $iallowance_amount = ($basic_salary / 100) / 2 * $sl_allowances->allowance_amount;
							  }
							 $allowance_amount -= $iallowance_amount; 
						  } else {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount;
							  } else {
								  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
							  }
							  $allowance_amount += $iallowance_amount;
						  }
                      }
					 
					}
				} else {
					$allowance_amount = 0;
				}

				// 3: all loan/deductions
				$salary_loan_deduction = $this->payroll_model->read_salary_loan_deductions($value->user_id);
				$count_loan_deduction = $this->payroll_model->count_employee_deductions($value->user_id);
				$loan_de_amount = 0;
				if($count_loan_deduction > 0) {
					foreach($salary_loan_deduction as $sl_salary_loan_deduction){
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount/2;
						  } else {
							  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
						  }
                      } else {
						  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
                      }
					  $loan_de_amount += $er_loan;
					}
				} else {
					$loan_de_amount = 0;
				}

				// commissions
				$count_commissions = $this->payroll_model->count_employee_commissions($value->user_id);
				$commissions = $this->payroll_model->set_employee_commissions($value->user_id);
				$commissions_amount = 0;
				if($count_commissions > 0) {
					foreach($commissions->result() as $sl_salary_commissions){
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $ecommissions_amount = $sl_salary_commissions->commission_amount/2;
						  } else {
							  $ecommissions_amount = $sl_salary_commissions->commission_amount;
						  }
						  $commissions_amount += $ecommissions_amount;
                      } else {
						 // $ecommissions_amount = $sl_salary_commissions->commission_amount;
						 if($sl_salary_commissions->is_commission_taxable == 1) {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount;
							  } else {
								  $ecommissions_amount = $basic_salary / 100 * $sl_salary_commissions->commission_amount;
							  }
							 $commissions_amount -= $ecommissions_amount; 
						  } else if($sl_salary_commissions->is_commission_taxable == 2) {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount / 2;
							  } else {
								  $ecommissions_amount = ($basic_salary / 100) / 2 * $sl_salary_commissions->commission_amount;
							  }
							 $commissions_amount -= $ecommissions_amount; 
						  } else {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount;
							  } else {
								  $ecommissions_amount = $basic_salary / 100 * $sl_salary_commissions->commission_amount;
							  }
							  $commissions_amount += $ecommissions_amount;
						  }
                      }
					  
					}
				} else {
					$commissions_amount = 0;
				}

				// otherpayments
				$count_other_payments = $this->payroll_model->count_employee_other_payments($value->user_id);
				$other_payments = $this->payroll_model->set_employee_other_payments($value->user_id);
				$other_payments_amount = 0;
				if($count_other_payments > 0) {
					foreach($other_payments->result() as $sl_other_payments) {
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $epayments_amount = $sl_other_payments->payments_amount/2;
						  } else {
							  $epayments_amount = $sl_other_payments->payments_amount;
						  }
						  $other_payments_amount += $epayments_amount;
                      } else {
						 // $epayments_amount = $sl_other_payments->payments_amount;
						  if($sl_other_payments->is_otherpayment_taxable == 1) {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount;
							  } else {
								  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
							  }
							 $other_payments_amount -= $epayments_amount; 
						  } else if($sl_other_payments->is_otherpayment_taxable == 2) {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount / 2;
							  } else {
								  $epayments_amount = ($basic_salary / 100) / 2 * $sl_other_payments->payments_amount;
							  }
							 $other_payments_amount -= $epayments_amount; 
						  } else {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount;
							  } else {
								  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
							  }
							  $other_payments_amount += $epayments_amount;
						  }
                      }
					  
					}
				} else {
					$other_payments_amount = 0;
				}

				// statutory_deductions
				$count_statutory_deductions = $this->payroll_model->count_employee_statutory_deductions($value->user_id);
				$statutory_deductions = $this->payroll_model->set_employee_statutory_deductions($value->user_id);
				$statutory_deductions_amount = 0;
				if($count_statutory_deductions > 0) {
					foreach($statutory_deductions->result() as $sl_salary_statutory_deductions){
						if($this->Settings->is_half_monthly==1){
							  if($this->Settings->half_deduct_month==2){
								  $single_sd = $sl_salary_statutory_deductions->deduction_amount/2;
							  } else {
								   $single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  }
							  $statutory_deductions_amount += $single_sd;
						  } else {
							  //$single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  if($sl_salary_statutory_deductions->statutory_options == 0) {
								  $single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  } else {
								  $single_sd = $basic_salary / 100 * $sl_salary_statutory_deductions->deduction_amount;
							  }
							  $statutory_deductions_amount += $single_sd;
						  }
						  
					}
				} else {
					$statutory_deductions_amount = 0;
				}	

				// 5: overtime
				$salary_overtime = $this->payroll_model->read_salary_overtime($value->user_id);
				$count_overtime = $this->payroll_model->count_employee_overtime($value->user_id);
				$overtime_amount = 0;
				if($count_overtime > 0) {
					foreach($salary_overtime as $sl_overtime){
						if($this->Settings->is_half_monthly==1){
							if($this->Settings->half_deduct_month==2){
								$eovertime_hours = $sl_overtime->overtime_hours/2;
								$eovertime_rate = $sl_overtime->overtime_rate/2;
							} else {
								$eovertime_hours = $sl_overtime->overtime_hours;
								$eovertime_rate = $sl_overtime->overtime_rate;
							}
						} else {
							$eovertime_hours = $sl_overtime->overtime_hours;
							$eovertime_rate = $sl_overtime->overtime_rate;
						}
						$overtime_total = $eovertime_hours * $eovertime_rate;
						//$overtime_total = $sl_overtime->overtime_hours * $sl_overtime->overtime_rate;
						$overtime_amount += $overtime_total;
					}
				} else {
					$overtime_amount = 0;
				}

				// add amount				
				$total_earning = $basic_salary + $allowance_amount + $overtime_amount + $commissions_amount + $other_payments_amount;
				$total_deduction = $loan_de_amount + $statutory_deductions_amount;
				$total_net_salary = $total_earning - $total_deduction;
				$net_salary = $total_net_salary;
				$advance_amount = 0;

				// get advance salary
				$advance_salary = $this->payroll_model->advance_salary_by_employee_id($value->user_id);
				$emp_value = $this->payroll_model->get_paid_salary_by_employee_id($value->user_id);
				
				if(!is_null($advance_salary)){
					$monthly_installment = $advance_salary[0]->monthly_installment;
					$advance_amount = $advance_salary[0]->advance_amount;
					$total_paid = $advance_salary[0]->total_paid;
					//check ifpaid
					$em_advance_amount = $advance_salary[0]->advance_amount;
					$em_total_paid = $advance_salary[0]->total_paid;
					
					if($em_advance_amount > $em_total_paid){
						if($monthly_installment=='' || $monthly_installment==0) {
							
							$ntotal_paid = $emp_value[0]->total_paid;
							$nadvance = $emp_value[0]->advance_amount;
							$i_net_salary = $nadvance - $ntotal_paid;
							//$pay_amount = $net_salary - $i_net_salary;
							$advance_amount = $i_net_salary;
						} else {
							//
							$re_amount = $em_advance_amount - $em_total_paid;
							if($monthly_installment > $re_amount){
								$advance_amount = $re_amount;
								//$total_net_salary = $net_salary - $re_amount;
								$pay_amount = $net_salary - $re_amount;
							} else {
								$advance_amount = $monthly_installment;
								//$total_net_salary = $net_salary - $monthly_installment;
								$pay_amount = $net_salary - $monthly_installment;
							}
						}
						
					} else {
						$i_net_salary = $net_salary - 0;
						$pay_amount = $net_salary - 0;
						$advance_amount = 0;
					}
				} else {
					$pay_amount = $net_salary - 0;
					$i_net_salary = $net_salary - 0;	
					$advance_amount = 0;
				}
				$total_net_salary = $total_net_salary - $advance_amount;
// echo "<pre>"; print_r($total_net_salary); die;

				$payroll_data = array(
					'total_net_salary' => $total_net_salary
				);

				// Action column, view payslip
				$payment_check = $this->payroll_model->read_make_payment_payslip_check($value->user_id,$p_date);
				// print_r($payment_check->result());die;
					if($payment_check->num_rows() > 0){
						$make_payment = $this->payroll_model->read_make_payment_payslip($value->user_id,$p_date);
						$view_url = 'payroll/payslip/id/'.$make_payment[0]->payslip_key;
						
						$status = '<span class="label label-success">'.lang('payroll_paid').'</span>';
						
						$view_payslip = "<div class=\"text-center\"><a href='" . admin_url('payroll/dialog_payroll_detail/?user_id='.$value->user_id.'&p_date='.$p_date) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_payroll_detail') . "'><i class=\"fa fa-eye\"></i></a> ";
						$mpay = "<a href='" . admin_url($view_url) . "' class='tip' title='" . lang('view_payment') . "'><i class=\"fa fa-file-text\"></i></a> ";
						
						$delete = " <a href='#' class='tip po' title='<b>" . lang('delete_payment') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('payroll/delete_payslip/'.$make_payment[0]->payslip_id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
						
						$mpay = $view_payslip . $mpay . $delete; 
						$total_net_salary = $make_payment[0]->net_salary;
					} else {
						$status = '<span class="label label-danger">'.lang('payroll_unpaid').'</span>';
						$mpay = "<div class=\"text-center\"><a href='" . admin_url('payroll/dialog_payroll_detail/?user_id='.$value->user_id.'&p_date='.$p_date) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_payroll_detail') . "'><i class=\"fa fa-eye\"></i></a> <a href='" . admin_url('payroll/dialog_make_payment?employee_id='.$value->user_id . "&pay_date=" . $p_date) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('make_payment') . "'><i class=\"fa fa-money\"></i></a></div>";
						$delete = '';
						$total_net_salary = $total_net_salary;
					}
					//detail link
				$detail = '<span data-toggle="tooltip" data-state="primary" data-placement="top" title="'.lang('view').'"><button type="button" class="btn icon-btn btn-sm btn-outline-secondary waves-effect waves-light" data-toggle="modal" data-target="#'.$view_p_class.'" data-employee_id="'. $value->user_id . '"><span class="fa fa-eye"></span></button></span>';

				// Datatable set columns
				if($value->wages_type == 1) {
					$wages_type = 'Monthly Payslip';
				} elseif($value->wages_type == 1) {
					$wages_type = 'Hourly Payslip';
				}

				$i++;
				$nestedData = array();
				// $nestedData["DT_RowId"] = $value->user_id;
				$nestedData[] = $value->user_id;
				
				$nestedData[] = $value->first_name. ' '. $value->last_name;
				$nestedData[] = $value->first_name;
				$nestedData[] = $wages_type;
				$nestedData[] = $value->basic_salary;
				$nestedData[] = $total_net_salary;
				$nestedData[] = $value->e_status;
				$edit = '';
				$edit = anchor("edit_supplier/".$value->user_id,
                    '<i class="fa fa-eye"></i> ',
                    array('class'=>'text-center', 'title'=>'View'));
				$edit .= ' ' . anchor("supplier/supplier/delete_supplier/".$value->user_id,
                    ' <i class="fa fa-money" aria-hidden="true"></i>',
                    array('class'=>'text-center', 'title'=>'Make Payment', 'onclick' => "return confirm('Do you want delete this record')"));
				$nestedData[] = $mpay;

				$data[] = $nestedData;
			}
		}
		//$all_users = $this->db->where('user_role_id != ', 1)->get('employees')->result();
		$all_users = $this->payroll_model->totalEmployees($filter_emp, $filter_salaryMonth, $sSearch);
		$totalData = sizeof($all_users); 

		//$displayRecords = sizeof($records);

		$sOutput = [
			//'sEcho'                => intval($this->ci->input->post('sEcho')),
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
			//'sColumns'             => implode(',', $sColumns),
		];
		echo json_encode($sOutput);
		//return ['aaData' => $aaData, 'sColumns' => $sColumns];

	}
	

	public function delete_payslip($payslip_id)
	{
		 $payslip_key = $this->db->select('payslip_key')->get_where('salary_payslips', array('payslip_id'=>$payslip_id))->row();
		if($this->payroll_model->delete_payslip($payslip_id)) {
			$this->payroll_model->delete_salary_expense($payslip_key->payslip_key);
     		$this->sma->send_json(['error' => 0, 'msg' => lang('payslip_deleted')]);
     	}
	}

	public function dialog_make_payment()
	{

		$emp_id = $this->input->get('employee_id');
		$pay_date = $this->input->get('pay_date');
		$emp_info = $this->employees_model->read_employee_information($emp_id);
		// $emp_info = $this->payroll_model->read_user_info($emp_id);
		
		$this->data['wages_type'] = $emp_info->wages_type;
		$this->data['basic_salary'] = $emp_info->basic_salary;
		$this->data['department_id'] = $emp_info->department_id;
		$this->data['designation_id'] = $emp_info->designation_id;
		$this->data['location_id'] = $emp_info->warehouse_id;
		$this->data['pay_date'] = $pay_date;
		$this->data['user_id'] = $emp_id;

		$this->data['modal_js']   = $this->site->modal_js(); 
        $this->load->view($this->theme . 'payroll/dialog_make_payment', $this->data); 
	}

	public function dialog_payroll_detail()
	{

		$user_id = $this->input->get('user_id');
		$p_date = $this->input->get('p_date');
		$this->data['user_id'] = $user_id;
		$this->data['pay_date'] = $p_date; 

		$this->data['modal_js']   = $this->site->modal_js(); 
        $this->load->view($this->theme . 'payroll/dialog_payroll_detail', $this->data); 
	}

	public function xdialog_payroll_detail($payslip_id)
	{
		//$key = $this->uri->segment(5);
		
		$result = $this->payroll_model->read_salary_payslip_info_key($payslip_id);

		
		if(is_null($result)){
			redirect('admin/payroll/generate_payslip');
		}

		$user = $this->payroll_model->read_user_info($result[0]->employee_id);
		// echo"<pre>";print_r($payslip_id);print_r($user);die;
		// user full name
		if(!is_null($user)){
			$first_name = $user[0]->first_name;
			$last_name = $user[0]->last_name;
		} else {
			$first_name = '--';
			$last_name = '--';
		}
		// get designation
		$designation = $this->payroll_model->read_designation_information($user[0]->designation_id);
		if(!is_null($designation)){
			$designation_name = $designation[0]->designation_name;
		} else {
			$designation_name = '--';	
		}
		// department
		$department = $this->payroll_model->read_department_information($user[0]->department_id);
		if(!is_null($department)){
			$department_name = $department[0]->department_name;
		} else {
			$department_name = '--';	
		}
		$p_method = '';

		$data = array(
				'title' => lang('payroll_employee_payslip'),
				'first_name' => $first_name,
				'last_name' => $last_name,
				'employee_id' => $user[0]->employee_id,
				'euser_id' => $user[0]->user_id,
				'contact_no' => $user[0]->contact_no,
				'date_of_joining' => $user[0]->date_of_joining,
				'department_name' => $department_name,
				'designation_name' => $designation_name,
				'date_of_joining' => $user[0]->date_of_joining,
				'profile_picture' => $user[0]->profile_picture,
				'gender' => $user[0]->gender,
				'make_payment_id' => $result[0]->payslip_id,
				'wages_type' => $result[0]->wages_type,
				'payment_date' => $result[0]->salary_month,
				'year_to_date' => $result[0]->year_to_date,
				'basic_salary' => $result[0]->basic_salary,
				'daily_wages' => $result[0]->daily_wages,
				'payment_method' => $p_method,				
				'total_allowances' => $result[0]->total_allowances,
				'total_loan' => $result[0]->total_loan,
				'total_overtime' => $result[0]->total_overtime,
				'total_commissions' => $result[0]->total_commissions,
				'total_statutory_deductions' => $result[0]->total_statutory_deductions,
				'total_other_payments' => $result[0]->total_other_payments,
				'net_salary' => $result[0]->net_salary,
				'other_payment' => $result[0]->other_payment,
				'payslip_key' => $result[0]->payslip_key,
				'payslip_type' => $result[0]->payslip_type,
				'hours_worked' => $result[0]->hours_worked,
				'pay_comments' => $result[0]->pay_comments,
				'saudi_gosi_percent' => $result[0]->saudi_gosi_percent,
				'saudi_gosi_amount' => $result[0]->saudi_gosi_amount,
				'is_advance_salary_deduct' => $result[0]->is_advance_salary_deduct,
				'advance_salary_amount' => $result[0]->advance_salary_amount,
				'is_payment' => $result[0]->is_payment,
				'approval_status' => $result[0]->status,
				);
// echo "<pre>"; print_r($user) ; 
		$this->data['payslip_info'] = $data;

		$this->data['modal_js']   = $this->site->modal_js(); 
        $this->load->view($this->theme . 'payroll/dialog_payroll_detail', $this->data); 
	}

	public function add_pay_monthly() {
	// echo '<pre>'; print_r($this->input->post()); die; 
		if($this->input->post()) {		
			$basic_salary = $this->input->post('basic_salary');

			// get advance salary
		$advance_salary = $this->payroll_model->advance_salary_by_employee_id($this->input->post('emp_id'));
		
		if(!is_null($advance_salary)){
			$emp_value = $this->payroll_model->get_paid_salary_by_employee_id($this->input->post('emp_id'));
			$monthly_installment = $advance_salary[0]->monthly_installment;
			$total_paid = $advance_salary[0]->total_paid;
			$advance_amount = $advance_salary[0]->advance_amount;
			//check ifpaid
			$em_advance_amount = $emp_value[0]->advance_amount;
			$em_total_paid = $emp_value[0]->total_paid;
			if($em_advance_amount > $em_total_paid){
				if($monthly_installment=='' || $monthly_installment==0) {
					$add_amount = $em_total_paid + $this->input->post('advance_amount');
					//pay_date //emp_id
					$adv_data = array('total_paid' => $add_amount);
					$payslip_deduct = $this->input->post('advance_amount');
					//
					$this->payroll_model->updated_advance_salary_paid_amount($adv_data,$this->input->post('emp_id'));
					$deduct_salary = $payslip_deduct;
					$is_advance_deducted = 1;
				} else {
					$add_amount = $em_total_paid + $this->input->post('advance_amount');
					$payslip_deduct = $this->input->post('advance_amount');
					//pay_date //emp_id
					$adv_data = array('total_paid' => $add_amount);
					//
					$this->Payroll_model->updated_advance_salary_paid_amount($adv_data,$this->input->post('emp_id'));
					$deduct_salary = $payslip_deduct;
					$is_advance_deducted = 1;
				}
				
			}
		} else {
			$deduct_salary = 0;
			$is_advance_deducted = 0;
		}
		$jurl = random_string('alnum', 40);	
		$data = array(
			'employee_id' => $this->input->post('emp_id'),
			'department_id' => $this->input->post('department_id'),
			// 'company_id' => $this->input->post('company_id'),
			'location_id' => $this->input->post('location_id'),
			'designation_id' => $this->input->post('designation_id'),
			'salary_month' => $this->input->post('pay_date'),
			'basic_salary' => $basic_salary,
			'net_salary' => $this->input->post('net_salary'),
			'wages_type' => $this->input->post('wages_type'),
			// 'is_half_monthly_payroll' => $is_half_monthly_payroll,
			'total_commissions' => $this->input->post('total_commissions'),
			'total_statutory_deductions' => $this->input->post('total_statutory_deductions'),
			'total_other_payments' => $this->input->post('total_other_payments'),
			'total_allowances' => $this->input->post('total_allowances'),
			'total_loan' => $this->input->post('total_loan'),
			'total_overtime' => $this->input->post('total_overtime'),
			'saudi_gosi_amount' => $this->input->post('saudi_gosi_amount'),
			'saudi_gosi_percent' => $this->input->post('saudi_gosi_percent'),
			'is_advance_salary_deduct' => $is_advance_deducted,
			'advance_salary_amount' => $deduct_salary,
			'is_payment' => '1',
			'status' => '0',
			'payslip_type' => 'full_monthly',
			'payslip_key' => $jurl,
			'year_to_date' => date('d-m-Y'),
			'created_at' => date('d-m-Y h:i:s')
			);
			// echo "<pre>";print_r($data); die; 

			$amount = $this->input->post('net_salary');
			$warehouse_id = $this->input->post('location_id');

			$result = $this->payroll_model->add_salary_payslip($data);
			if($result) {
				$date = date('Y-m-d H:i:s');
				$category_id = 1; // For Salary category_id = 1

				$data_expense = [
                'date'         => $date,
                'reference'    => $jurl,
                'amount'       => $amount,
                'created_by'   => $this->session->userdata('user_id'),
                'note'         => 'Salary Expense',
                'category_id'  => $category_id,
                'warehouse_id' => $warehouse_id,
            	];
            	$this->purchases_model->addExpense($data_expense);
            	return true;
			} else {
				return false;
			}
		}

	}

	public function add_pay_hourly() {

		if($this->input->post()) {		
			// print_r($this->input->post()); die; 
		$basic_salary = $this->input->post('basic_salary');
		$jurl = random_string('alnum', 40);	
		$data = array(
		'employee_id' => $this->input->post('emp_id'),
		'department_id' => $this->input->post('department_id'),
		'company_id' => $this->input->post('company_id'),
		'warehouse_id' => $this->input->post('location_id'),
		'designation_id' => $this->input->post('designation_id'),
		'salary_month' => $this->input->post('pay_date'),
		'basic_salary' => $basic_salary,
		'net_salary' => $this->input->post('net_salary'),
		'wages_type' => $this->input->post('wages_type'),
		'is_half_monthly_payroll' => 0,
		'total_commissions' => $this->input->post('total_commissions'),
		'total_statutory_deductions' => $this->input->post('total_statutory_deductions'),
		'total_other_payments' => $this->input->post('total_other_payments'),
		'total_allowances' => $this->input->post('total_allowances'),
		'total_loan' => $this->input->post('total_loan'),
		'total_overtime' => $this->input->post('total_overtime'),
		'hours_worked' => $this->input->post('hours_worked'),
		'saudi_gosi_amount' => $this->input->post('saudi_gosi_amount'),
		'saudi_gosi_percent' => $this->input->post('saudi_gosi_percent'),
		'is_payment' => '1',
		'status' => '0',
		'payslip_type' => 'hourly',
		'payslip_key' => $jurl,
		'year_to_date' => date('d-m-Y'),
		'created_at' => date('d-m-Y h:i:s')
		);
		$result = $this->payroll_model->add_salary_payslip($data);	

		$amount = $this->input->post('net_salary');
		$warehouse_id = $this->input->post('location_id');
		if($result) {
			$date = date('Y-m-d H:i:s');
			$category_id = 1; // For Salary category_id = 1

				$data_expense = [
                'date'         => $date,
                'reference'    => $jurl,
                'amount'       => $amount,
                'created_by'   => $this->session->userdata('user_id'),
                'note'         => 'Salary Expense',
                'category_id'  => $category_id,
                'warehouse_id' => $warehouse_id,
            	];
            	$this->purchases_model->addExpense($data_expense);
			return true;
		} else {
			return false;
		}
		}

	}

	public function payslip()
	{
		$key = $this->uri->segment(5);
		// print_r($key);die;
		$result = $this->payroll_model->read_salary_payslip_info_key($key);
		
		if(is_null($result)){
			redirect('admin/payroll/generate_payslip');
		}

		$user = $this->payroll_model->read_user_info($result[0]->employee_id);
		// user full name
		if(!is_null($user)){
			$first_name = $user[0]->first_name;
			$last_name = $user[0]->last_name;
		} else {
			$first_name = '--';
			$last_name = '--';
		}
		// get designation
		$designation = $this->payroll_model->read_designation_information($user[0]->designation_id);
		if(!is_null($designation)){
			$designation_name = $designation[0]->designation_name;
		} else {
			$designation_name = '--';	
		}
		// department
		$department = $this->payroll_model->read_department_information($user[0]->department_id);
		if(!is_null($department)){
			$department_name = $department[0]->department_name;
		} else {
			$department_name = '--';	
		}
		$p_method = '';

		$data = array(
				'title' => lang('payroll_employee_payslip'),
				'first_name' => $first_name,
				'last_name' => $last_name,
				'employee_id' => $user[0]->employee_id,
				'euser_id' => $user[0]->user_id,
				'contact_no' => $user[0]->contact_no,
				'date_of_joining' => $user[0]->date_of_joining,
				'department_name' => $department_name,
				'designation_name' => $designation_name,
				'date_of_joining' => $user[0]->date_of_joining,
				'profile_picture' => $user[0]->profile_picture,
				'gender' => $user[0]->gender,
				'make_payment_id' => $result[0]->payslip_id,
				'wages_type' => $result[0]->wages_type,
				'payment_date' => $result[0]->salary_month,
				'year_to_date' => $result[0]->year_to_date,
				'basic_salary' => $result[0]->basic_salary,
				'daily_wages' => $result[0]->daily_wages,
				'payment_method' => $p_method,				
				'total_allowances' => $result[0]->total_allowances,
				'total_loan' => $result[0]->total_loan,
				'total_overtime' => $result[0]->total_overtime,
				'total_commissions' => $result[0]->total_commissions,
				'total_statutory_deductions' => $result[0]->total_statutory_deductions,
				'total_other_payments' => $result[0]->total_other_payments,
				'net_salary' => $result[0]->net_salary,
				'other_payment' => $result[0]->other_payment,
				'payslip_key' => $result[0]->payslip_key,
				'payslip_type' => $result[0]->payslip_type,
				'hours_worked' => $result[0]->hours_worked,
				'pay_comments' => $result[0]->pay_comments,
				'saudi_gosi_percent' => $result[0]->saudi_gosi_percent,
				'saudi_gosi_amount' => $result[0]->saudi_gosi_amount,
				'is_advance_salary_deduct' => $result[0]->is_advance_salary_deduct,
				'advance_salary_amount' => $result[0]->advance_salary_amount,
				'is_payment' => $result[0]->is_payment,
				'approval_status' => $result[0]->status,
				);
// echo "<pre>"; print_r($user) ; 
		$this->data['payslip_info'] = $data;
		$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('employees'), 'page' => lang('employees')], ['link' => '#', 'page' => lang('employees')]];
        $meta = ['page_title' => lang('employees'), 'bc' => $bc];
		$this->page_construct('payroll/payslip', $meta, $this->data);
	}

	public function change_payroll_status()
     {
     	
     	$payslip_id = $this->input->post('payslip_id');
     	$data = [
     		'status' => $this->input->post('status'),
     	];
     	$update = $this->payroll_model->change_payroll_status($data, $payslip_id);
     	if($update) {
				return true;
			} else {
				return false;
			}
     }

}

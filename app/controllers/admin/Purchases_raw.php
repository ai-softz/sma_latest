<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchases_raw extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->lang->admin_load('purchases', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('purchase_raw_model');
        $this->load->admin_model('reports_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path         = 'assets/uploads/';
        $this->thumbs_path         = 'assets/uploads/thumbs/';
        $this->image_types         = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types  = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size   = '1024';
        $this->data['logo']        = true;
    }

    public function index()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('raw_material_purchases')]];
        $meta = ['page_title' => lang('raw_material_purchases'), 'bc' => $bc];
		$this->page_construct('purchase_raw/purchase_raw_list', $meta, $this->data);
    }

    public function getPurchaseRawTable()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->purchase_raw_model->getPurchaseRawTable($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            if($value->status == 'received') {
                $status = '<span class="row_status label label-success">'. $value->status .'</span>';
            } else if($value->status == 'pending') {
                $status = '<span class="row_status label label-warning">'. $value->status .'</span>';
            } else if($value->status == 'ordered') {
                $status = '<span class="row_status label label-primary">'. $value->status .'</span>';
            }
            if($value->payment_status == 'paid') {
                $payment_status = '<span class="row_status label label-success">'. $value->payment_status .'</span>';
            } else if($value->payment_status == 'pending') {
                $payment_status = '<span class="row_status label label-warning">'. $value->payment_status .'</span>';
            } else if($value->payment_status == 'due') {
                $payment_status = '<span class="row_status label label-danger">'. $value->payment_status .'</span>';
            }
            else if($value->payment_status == 'partial') {
                $payment_status = '<span class="row_status label label-primary">'. $value->payment_status .'</span>';
            }
            
            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->date;
            $nestedData[] = $value->reference_no;
            $nestedData[] = $status;
            $nestedData[] = $value->grand_total;
            $nestedData[] = $value->paid;
            $nestedData[] = $payment_status;
            
            $edit = '';
            $edit .= " <a href='" . admin_url('purchases_raw/edit_purchase_raw/'.$value->id) . "' class='tip' title='" . lang('edit_purchase_raw') . "'><i class=\"fa fa-edit\"></i></a>";
            $edit .= " <a href='" . admin_url('purchases_raw/add_payment_raw/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('add_payment_raw') . "'><i class=\"fa fa-money\"></i></a>";
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_purchase_raw') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('purchases_raw/delete_purchase_raw/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->purchase_raw_model->total_getPurchaseRawTable($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function delete_purchase_raw($id='')
    {
        if($this->purchase_raw_model->delete_purchase_raw($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('purchase_raw_deleted')]);
        }
    }

    public function modal_view_raw($purchase_id = null)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv                 = $this->purchase_raw_model->getPurchaseRawByID($purchase_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows']            = $this->purchase_raw_model->getAllPurchaseItemsRaw($purchase_id);
        $this->data['supplier']        = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['warehouse']       = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv']             = $inv;
        $this->data['payments']        = $this->purchase_raw_model->getPaymentsForPurchaseRaw($purchase_id);
        $this->data['created_by']      = $this->site->getUser($inv->created_by);
        $this->data['updated_by']      = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        // $this->data['return_purchase'] = $inv->return_id ? $this->purchases_model->getPurchaseByID($inv->return_id) : null;
        // $this->data['return_rows']     = $inv->return_id ? $this->purchases_model->getAllPurchaseItems($inv->return_id) : null;

        $this->load->view($this->theme . 'purchase_raw/modal_view_raw', $this->data);
    }

    public function add_purchase_raw($quote_id = null)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line('no_zero_required'));
        $this->form_validation->set_rules('warehouse', $this->lang->line('warehouse'), 'required|is_natural_no_zero');

        $this->session->unset_userdata('csrf_token');
        if ($this->form_validation->run() == true) {
// print_r($this->input->post()); die;
            if($this->input->post('reference_no')) {
                $reference = $this->input->post('reference_no');
            } else {
                $purchase_last = $this->db->limit(1)->order_by('id DESC')->get('purchases_raw')->row();
                if(empty($purchase_last)) {
                    $reference = 'POR00001';
                } else {
                $ref_substr = substr($purchase_last->reference_no, 3) + 1;
                $reference = 'POR'.sprintf('%05s', $ref_substr);
                }
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id     = $this->input->post('warehouse');
            $status           = $this->input->post('status');
            $note             = $this->sma->clear_tags($this->input->post('note'));
            $payment_term     = $this->input->post('payment_term');
            $due_date         = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;

            $total            = 0;
            $i                = sizeof($_POST['product']);
            $gst_data         = [];
            
            for ($r = 0; $r < $i; $r++) {
                $item_code          = $_POST['product'][$r];
                $item_net_cost      = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                
                $item_expiry        = (isset($_POST['expiry'][$r]) && !empty($_POST['expiry'][$r])) ? $this->sma->fsd($_POST['expiry'][$r]) : null;
                $item_unit          = $_POST['product_unit'][$r];
                $item_quantity      = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($item_quantity)) {
                    $product_details = $this->purchase_raw_model->getProductByCodeRaw($item_code); 
                    if ($item_expiry) {
                        $today = date('Y-m-d');
                        if ($item_expiry <= $today) {
                            $this->session->set_flashdata('error', lang('product_expiry_date_issue') . ' (' . $product_details->name . ')');
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                    }
                    
                    $subtotal = ($item_net_cost * $item_unit_quantity);
                    $unit     = $this->site->getUnitByID($item_unit);

                    $product = [
                        'product_id'        => $product_details->id,
                        'product_code'      => $item_code,
                        'product_name'      => $product_details->caption,
                        
                        'net_unit_cost'     => $item_net_cost,
                        'quantity'          => $item_quantity,
                        'product_unit_id'   => $item_unit,
                        'product_unit_code' => $unit->code,
                        'unit_quantity'     => $item_unit_quantity,
                        'quantity_balance'  => $status == 'received' ? $item_quantity : 0,
                        'quantity_received' => $status == 'received' ? $item_quantity : 0,
                        'warehouse_id'      => $warehouse_id,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'expiry'            => $item_expiry,
                        // 'real_unit_cost'    => $real_unit_cost,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                        // 'supplier_part_no'  => $supplier_part_no,
                    ];

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang('order_items'), 'required');
            } else {
                krsort($products);
            }

            $supplier = $this->input->post('supplier');

            $supplier_name = $this->db->get_where('companies', ['id'=>$supplier, 'group_name'=>'supplier'])->row()->name;

            $grand_total    = $this->sma->formatDecimal($this->sma->formatDecimal($total));
            $data           = ['reference_no' => $reference,
                'date'                        => $date,
                'warehouse_id'                => $warehouse_id,
                'supplier_id'                 => $supplier,  
                'supplier'                    => $supplier_name,  
                'note'                        => $note,
                'total'                       => $total,
                // 'shipping'                    => $this->sma->formatDecimal($shipping),
                'grand_total'                 => $grand_total,
                'status'                      => $status,
                'created_by'                  => $this->session->userdata('user_id'),
                'payment_term'                => $payment_term,
                'due_date'                    => $due_date,
            ];
           
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path']   = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size']      = $this->allowed_file_size;
                $config['overwrite']     = false;
                $config['encrypt_name']  = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $photo              = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);
        }

        if ($this->form_validation->run() == true && $this->purchase_raw_model->addPurchaseRaw($data, $products)) {
            $this->session->set_userdata('remove_pols', 1);
            $this->session->set_flashdata('message', $this->lang->line('purchase_added'));
            // admin_redirect('purchases');
            admin_redirect('purchases_raw/');
        } else {
            if ($quote_id) {
                $this->data['quote'] = $this->purchases_model->getQuoteByID($quote_id);
                $supplier_id         = $this->data['quote']->supplier_id;
                $items               = $this->purchases_model->getAllQuoteItems($quote_id);
                krsort($items);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if ($row->type == 'combo') {
                        $combo_items = $this->site->getProductComboItems($row->id, $item->warehouse_id);
                        foreach ($combo_items as $citem) {
                            $crow = $this->site->getProductByID($citem->id);
                            if (!$crow) {
                                $crow      = json_decode('{}');
                                $crow->qty = $item->quantity;
                            } else {
                                unset($crow->details, $crow->product_details, $crow->price);
                                $crow->qty = $citem->qty * $item->quantity;
                            }
                            $crow->base_quantity  = $item->quantity;
                            $crow->base_unit      = $crow->unit ? $crow->unit : $item->product_unit_id;
                            $crow->base_unit_cost = $crow->cost ? $crow->cost : $item->unit_cost;
                            $crow->unit           = $item->product_unit_id;
                            $crow->discount       = $item->discount ? $item->discount : '0';
                            $supplier_cost        = $supplier_id ? $this->getSupplierCost($supplier_id, $crow) : $crow->cost;
                            $crow->cost           = $supplier_cost ? $supplier_cost : 0;
                            $crow->tax_rate       = $item->tax_rate_id;
                            $crow->real_unit_cost = $crow->cost ? $crow->cost : 0;
                            $crow->expiry         = '';
                            $options              = $this->purchases_model->getProductOptions($crow->id);
                            $units                = $this->site->getUnitsByBUID($row->base_unit);
                            $tax_rate             = $this->site->getTaxRateByID($crow->tax_rate);
                            $ri                   = $this->Settings->item_addition ? $crow->id : $c;

                            $pr[$ri] = ['id' => $c, 'item_id' => $crow->id, 'label' => $crow->name . ' (' . $crow->code . ')', 'row' => $crow, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options];
                            $c++;
                        }
                    } elseif ($row->type == 'standard') {
                        if (!$row) {
                            $row           = json_decode('{}');
                            $row->quantity = 0;
                        } else {
                            unset($row->details, $row->product_details);
                        }

                        $row->id             = $item->product_id;
                        $row->code           = $item->product_code;
                        $row->name           = $item->product_name;
                        $row->base_quantity  = $item->quantity;
                        $row->base_unit      = $row->unit ? $row->unit : $item->product_unit_id;
                        $row->base_unit_cost = $row->cost ? $row->cost : $item->unit_cost;
                        $row->unit           = $item->product_unit_id;
                        $row->qty            = $item->unit_quantity;
                        $row->option         = $item->option_id;
                        $row->discount       = $item->discount ? $item->discount : '0';
                        $supplier_cost       = $supplier_id ? $this->getSupplierCost($supplier_id, $row) : $row->cost;
                        $row->cost           = $supplier_cost ? $supplier_cost : 0;
                        $row->tax_rate       = $item->tax_rate_id;
                        $row->expiry         = '';
                        $row->real_unit_cost = $row->cost ? $row->cost : 0;
                        $options             = $this->purchases_model->getProductOptions($row->id);

                        $units    = $this->site->getUnitsByBUID($row->base_unit);
                        $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        $ri       = $this->Settings->item_addition ? $row->id : $c;

                        $pr[$ri] = ['id' => $c, 'item_id' => $row->id, 'label' => $row->name . ' (' . $row->code . ')',
                            'row'        => $row, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, ];
                        $c++;
                    }
                }
                $this->data['quote_items'] = json_encode($pr);
            }

            $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['quote_id']   = $quote_id;
            $this->data['suppliers']  = $this->site->getAllCompanies('supplier');
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates']  = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['ponumber']   = ''; //$this->site->getReference('po');
            $this->load->helper('string');
            $value = random_string('alnum', 20);
            $this->session->set_userdata('user_csrf', $value);
            $this->data['csrf'] = $this->session->userdata('user_csrf');
            $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('purchases'), 'page' => lang('purchases')], ['link' => '#', 'page' => lang('add_purchase')]];
            $meta               = ['page_title' => lang('add_purchase'), 'bc' => $bc];
            $this->page_construct('purchase_raw/add_purchase_raw', $meta, $this->data);
        }
    }

    public function edit_purchase_raw($id='')
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line('no_zero_required'));
        $this->form_validation->set_rules('warehouse', $this->lang->line('warehouse'), 'required|is_natural_no_zero');

        $this->session->unset_userdata('csrf_token');
        if ($this->form_validation->run() == true) {

            if($this->input->post('reference_no')) {
                $reference = $this->input->post('reference_no');
            } else {
                $purchase_last = $this->db->limit(1)->order_by('id DESC')->get('purchases_raw')->row();
                if(empty($purchase_last)) {
                    $reference = 'POR00001';
                } else {
                $ref_substr = substr($purchase_last->reference_no, 3) + 1;
                $reference = 'POR'.sprintf('%05s', $ref_substr);
                }
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $purchase_raw_id     = $this->input->post('purchase_raw_id');
            $warehouse_id     = $this->input->post('warehouse');
            $status           = $this->input->post('status');
            $note             = $this->sma->clear_tags($this->input->post('note'));
            $payment_term     = $this->input->post('payment_term');
            $due_date         = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;

            $total            = 0;
            $i                = sizeof($_POST['product']);
            $gst_data         = [];
            
            for ($r = 0; $r < $i; $r++) {
                $item_code          = $_POST['product'][$r];
                $item_net_cost      = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                
                $item_expiry        = (isset($_POST['expiry'][$r]) && !empty($_POST['expiry'][$r])) ? $this->sma->fsd($_POST['expiry'][$r]) : null;
                $item_unit          = $_POST['product_unit'][$r];
                $item_quantity      = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($item_quantity)) {
                    $product_details = $this->purchase_raw_model->getProductByCodeRaw($item_code);
                    if ($item_expiry) {
                        $today = date('Y-m-d');
                        if ($item_expiry <= $today) {
                            $this->session->set_flashdata('error', lang('product_expiry_date_issue') . ' (' . $product_details->name . ')');
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                    }
                    
                    $subtotal = ($item_net_cost * $item_unit_quantity);
                    $unit     = $this->site->getUnitByID($item_unit);

                    $product = [
                        'product_id'        => $product_details->id,
                        'product_code'      => $item_code,
                        'product_name'      => $product_details->caption,
                        
                        'net_unit_cost'     => $item_net_cost,
                        'quantity'          => $item_quantity,
                        'product_unit_id'   => $item_unit,
                        'product_unit_code' => $unit->code,
                        'unit_quantity'     => $item_unit_quantity,
                        'quantity_balance'  => $status == 'received' ? $item_quantity : 0,
                        'quantity_received' => $status == 'received' ? $item_quantity : 0,
                        'warehouse_id'      => $warehouse_id,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'expiry'            => $item_expiry,
                        // 'real_unit_cost'    => $real_unit_cost,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                        // 'supplier_part_no'  => $supplier_part_no,
                    ];

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang('order_items'), 'required');
            } else {
                krsort($products);
            }

            $grand_total    = $this->sma->formatDecimal($this->sma->formatDecimal($total));
            $data           = ['reference_no' => $reference,
                'date'                        => $date,
                'warehouse_id'                => $warehouse_id,
                'note'                        => $note,
                'total'                       => $total,
                
                'shipping'                    => $this->sma->formatDecimal($shipping),
                'grand_total'                 => $grand_total,
                'status'                      => $status,
                'created_by'                  => $this->session->userdata('user_id'),
                'payment_term'                => $payment_term,
                'due_date'                    => $due_date,
            ];
           
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path']   = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size']      = $this->allowed_file_size;
                $config['overwrite']     = false;
                $config['encrypt_name']  = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $photo              = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);
        }

        if ($this->form_validation->run() == true && $this->purchase_raw_model->updatePurchaseRaw($purchase_raw_id, $data, $products)) {
            $this->session->set_userdata('remove_pols', 1);
            $this->session->set_flashdata('message', $this->lang->line('purchase_edited'));
            // admin_redirect('purchases');
            admin_redirect('purchases_raw/');
        } else {

            $inv_items = $this->purchase_raw_model->getAllPurchaseItemsRaw($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->purchase_raw_model->getProductByIDRaw($item->product_id);
                if (!$row) {
                    $this->session->set_flashdata('error', lang('product_deleted_x_edit'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $row->expiry           = (($item->expiry && $item->expiry != '0000-00-00') ? $this->sma->hrsd($item->expiry) : '');
                $row->base_quantity    = $item->quantity;
                $row->base_unit        = $row->unit ? $row->unit : $item->product_unit_id;
                $row->unit             = $item->product_unit_id;
                $row->oqty             = $item->quantity;
                $row->received         = $item->quantity_received ? $item->quantity_received : $item->quantity;
                $row->quantity_balance = $item->quantity_balance + ($item->quantity - $row->received);
                $row->net_unit_cost   = $item->net_unit_cost;
                $row->real_unit_cost   = $item->real_unit_cost;
                $units    = $this->site->getUnitsByBUID($row->base_unit);
                $ri       = $this->Settings->item_addition ? $row->id : $c;

                $pr[$ri] = ['id' => $c, 'item_id' => $row->id, 'label' => $row->caption . ' (' . $row->code . ')',
                    'row'        => $row, 'units' => $units, ];
                $c++;
            }

            $this->data['inv_items']  = json_encode($pr);

            $inv = $this->purchase_raw_model->getPurchaseRawByID($id);

            $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['inv']   = $inv;
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['ponumber']   = $inv->reference_no; //$this->site->getReference('po');
            $this->load->helper('string');
            $value = random_string('alnum', 20);
            $this->session->set_userdata('user_csrf', $value);
            $this->data['csrf'] = $this->session->userdata('user_csrf');
            $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('purchases'), 'page' => lang('purchases_raw')], ['link' => '#', 'page' => lang('edit_purchase_raw')]];
            $meta               = ['page_title' => lang('edit_purchase_raw'), 'bc' => $bc];
            $this->page_construct('purchase_raw/edit_purchase_raw', $meta, $this->data);
        }
    }

    public function add_payment_raw($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $purchase = $this->purchase_raw_model->getPurchaseRawByID($id);
        if ($purchase->payment_status == 'paid' && $purchase->grand_total == $purchase->paid) {
            $this->session->set_flashdata('error', lang('purchase_already_paid'));
            $this->sma->md();
        }

        //$this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang('amount'), 'required');
        $this->form_validation->set_rules('paid_by', lang('paid_by'), 'required');
        $this->form_validation->set_rules('userfile', lang('attachment'), 'xss_clean');
        if ($this->form_validation->run() == true) {
            // print_r($this->input->post()); die;
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $payment = [
                'date'         => $date,
                'purchase_raw_id'  => $this->input->post('purchase_id'),
                'reference_no' => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('ppay'),
                'amount'       => $this->input->post('amount-paid'),
                'paid_by'      => $this->input->post('paid_by'),
                'cheque_no'    => $this->input->post('cheque_no'),
                'cc_no'        => $this->input->post('pcc_no'),
                'cc_holder'    => $this->input->post('pcc_holder'),
                'cc_month'     => $this->input->post('pcc_month'),
                'cc_year'      => $this->input->post('pcc_year'),
                'cc_type'      => $this->input->post('pcc_type'),
                'note'         => $this->sma->clear_tags($this->input->post('note')),
                'created_by'   => $this->session->userdata('user_id'),
                'type'         => 'sent',
            ];

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path']   = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size']      = $this->allowed_file_size;
                $config['overwrite']     = false;
                $config['encrypt_name']  = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $photo                 = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);
        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']);
        }

        if ($this->form_validation->run() == true && $this->purchase_raw_model->addPaymentRaw($payment)) {
            $this->session->set_flashdata('message', lang('payment_added'));
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->data['error']       = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['inv']         = $purchase;
            $this->data['payment_ref'] = ''; //$this->site->getReference('ppay');
            $this->data['modal_js']    = $this->site->modal_js();

            $this->load->view($this->theme . 'purchase_raw/add_payment_raw', $this->data);
        }
    }

    public function make_products()
    {
        
        if ($this->input->post('make_products')) {
            $this->session->set_userdata('remove_pols', 1);
            // echo '<pre>'; print_r($this->input->post()); die;

            $date = $this->input->post('date');
            $date = date('Y-m-d', strtotime($date));
            
            for($i = 0; $i < sizeof($_POST['product_id']); $i++) {

                $quantity = $_POST['quantity'][$i];

                $product_id = $_POST['product_id'][$i];
                $product_qty = $this->purchase_raw_model->get_product_quantity($product_id);
                
                $new_qty = $product_qty->quantity + $_POST['quantity'][$i];
                // print_r($new_qty); die;
                // $new_qty = $product_qty->make_product_qty + $_POST['quantity'][$i];
                $data_product = array(
                    'quantity' => $new_qty
                );
                $update = $this->purchase_raw_model->update_product_quantity($product_id, $data_product);

                $data_make_products = array(
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'date' => $date,
                    'warehouse_id' => $this->input->post('warehouse'),
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d')
                );
                $this->purchase_raw_model->add_make_products($data_make_products);
            }
            
            $this->session->set_flashdata('message', $this->lang->line('make_product_successfully_added'));
            admin_redirect('purchases_raw/make_products');
        } else {
            $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            // $this->session->set_userdata('user_csrf', $value);
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['csrf'] = $this->session->userdata('user_csrf');

            $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('purchases'), 'page' => lang('purchases')], ['link' => '#', 'page' => lang('make_products')]];
            $meta               = ['page_title' => lang('add_purchase'), 'bc' => $bc];
            $this->page_construct('purchase_raw/make_products', $meta, $this->data);
        }
    }

    public function suggestions_raw()
    {
        $term        = $this->input->get('term', true);
        $supplier_id = $this->input->get('supplier_id', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed  = $this->sma->analyze_term($term);
        $sr        = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $sr        = addslashes($sr);
        $qty       = $analyzed['quantity'] ?? null;
        $bprice    = $analyzed['price']    ?? null;

        $rows = $this->purchase_raw_model->getProductNamesRaw($sr);
        if ($rows) {
            $r = 0;
            foreach ($rows as $row) {
                
                $c                    = uniqid(mt_rand(), true);
                $option               = false;
                // $row->item_tax_method = $row->tax_method;
                $options              = $this->purchases_model->getProductOptions($row->id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->purchases_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt       = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = false;
                }
                $row->option           = $option_id;
                
                $base_quantity = 1.0;
                $row->base_quantity    = $base_quantity;
                $row->base_unit        = $row->unit;
                $row->net_unit_cost        = $row->price;
                
                $row->new_entry        = 1;
                $row->expiry           = '';
                $row->qty              = 1.0;
                $row->quantity_balance = '';
                $row->discount         = '0';

                $units    = $this->site->getUnitsByBUID($row->base_unit);

                $pr[] = ['id' => sha1($c . $r), 'item_id' => $row->id, 'label' => $row->caption . ' (' . $row->code . ')',
                    'row'     => $row, 'units' => $units, 'options' => $options, ];
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json([['id' => 0, 'label' => lang('no_match_found'), 'value' => $term]]);
        }
    }

    public function category_raw_material()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('category')]];
        $meta = ['page_title' => lang('category'), 'bc' => $bc];
		$this->page_construct('purchase_raw/category_raw_material', $meta, $this->data);
    }
    public function getRawMaterialCategories()
    {
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->purchase_raw_model->getRawMaterialCategories($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            
				$edit = '';
				$edit .= " <a href='" . admin_url('purchases_raw/edit_category_raw_material/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_add_deduct') . "'><i class=\"fa fa-edit\"></i></a>";
                
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_category_raw_material') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('purchases_raw/delete_category_raw_material/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->purchase_raw_model->total_RawMaterialCategories($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function delete_category_raw_material($id='')
    {
        if($this->purchase_raw_model->delete_category_raw_material($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('category_raw_material_deleted')]);
        }
    }

    public function add_categories_raw_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'purchase_raw/add_categories_raw_form', $this->data);
    }
    public function edit_category_raw_material($id='')
    {
        $this->data['data'] = $this->purchase_raw_model->get_category_byId($id);

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'purchase_raw/edit_category_raw_material', $this->data);
    }

    public function add_categories_raw()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),

                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->purchase_raw_model->add_categories_raw($data);
       }

    }
    public function edit_raw_categories()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {
            $raw_category_id = $this->input->post('raw_category_id');
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),

                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->purchase_raw_model->edit_raw_categories($data, $raw_category_id);
       }

    }

    public function products_raw_material()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['categories_raw'] = $this->purchase_raw_model->get_categories_raw(); 

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('raw_material_products')]];
        $meta = ['page_title' => lang('raw_material_products'), 'bc' => $bc];
		$this->page_construct('purchase_raw/products_raw_material', $meta, $this->data);
    }
    public function getProductsRawTable()
    {
        $categoryId=$this->input->post('categoryId');
        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $records = $this->purchase_raw_model->getProductsRawTable($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $categoryId);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $category_raw = $this->db->where('id', $value->category_id)->get('categories_raw')->row();
            $unit = $this->db->where('id', $value->unit)->get('units')->row();

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $value->cat_caption ? $value->cat_caption : '';
            $nestedData[] = $value->price;
            $nestedData[] = $unit ? $unit->name : '';
            
            $edit = '';
            $edit .= " <a href='" . admin_url('purchases_raw/edit_products_raw_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_products_raw') . "'><i class=\"fa fa-edit\"></i></a>";
            
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_products_raw') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('purchases_raw/delete_products_raw/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->purchase_raw_model->total_getProductsRawTable($sSearch, $categoryId);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function delete_products_raw($id='')
    {
        if($this->purchase_raw_model->delete_products_raw($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('products_raw_deleted')]);
        }
    }
    public function add_products_raw_form()
    {
        $this->data[] = '';
        $this->data['categories_raw'] = $this->purchase_raw_model->getCategoriesRaw();
        $this->data['base_units'] = $this->site->getAllBaseUnits();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'purchase_raw/add_products_raw_form', $this->data);
    }
    public function edit_products_raw_form($id='')
    {
        $this->data[] = '';
        $this->data['products_raw'] = $this->purchase_raw_model->getProductsRawByid($id);
        $this->data['categories_raw'] = $this->purchase_raw_model->getCategoriesRaw();
        $this->data['base_units'] = $this->site->getAllBaseUnits();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'purchase_raw/edit_products_raw_form', $this->data);
    }

    public function add_products_raw()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {

            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'category_id' => $this->input->post('category_id'),
                'price' => $this->input->post('price'),
                'unit' => $this->input->post('unit'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),

                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->purchase_raw_model->add_products_raw($data);
       }

    }
    public function edit_products_raw()
    {
    //    print_r($this->input->post());die;
       if($this->input->post()) {
            
            $products_raw_id = $this->input->post('products_raw_id');
            $data = array(
                'caption' => $this->input->post('caption'),
                'caption_alt' => $this->input->post('caption_alt'),
                'category_id' => $this->input->post('category_id'),
                'price' => $this->input->post('price'),
                'unit' => $this->input->post('unit'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),

                'remark' => $this->input->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->purchase_raw_model->edit_products_raw($data, $products_raw_id);
       }

    }

    public function daily_purchases_raw($warehouse_id = null, $year = null, $month = null, $pdf = null, $user_id = null)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $config              = [
            'show_next_prev' => true,
            'next_prev_url'  => admin_url('purchases_raw/daily_purchases_raw/' . ($warehouse_id ? $warehouse_id : 0)),
            'month_type'     => 'long',
            'day_type'       => 'long',
        ];

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $purchases = $this->purchase_raw_model->getDailyPurchasesRaw($year, $month, $warehouse_id);

        if (!empty($purchases)) {
            foreach ($purchases as $purchase) {
                $daily_purchase[$purchase->date] = "<table class='table table-bordered table-hover table-striped table-condensed data' style='margin:0;'>". '<tr><td>' . lang('total') . '</td><td>' . $this->sma->formatMoney($purchase->total) . '</td></tr></table>';
            }
        } else {
            $daily_purchase = [];
        }

        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_purchase);
        $this->data['year']     = $year;
        $this->data['month']    = $month;
        if ($pdf) {
            $html = $this->load->view($this->theme . 'purchase_raw/daily', $this->data, true);
            $name = lang('daily_purchases') . '_' . $year . '_' . $month . '.pdf';
            $html = str_replace('<p class="introtext">' . lang('reports_calendar_text') . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses']    = $this->site->getAllWarehouses();
        $this->data['warehouse_id']  = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        $bc                          = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('reports'), 'page' => lang('reports')], ['link' => '#', 'page' => lang('daily_purchases_report')]];
        $meta                        = ['page_title' => lang('daily_purchases_report'), 'bc' => $bc];
        $this->page_construct('purchase_raw/daily_purchases_raw', $meta, $this->data);
    }

    public function monthly_purchases_raw($warehouse_id = null, $year = null, $pdf = null, $user_id = null)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->load->language('calendar');
        $this->data['error']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['year']      = $year;
        $this->data['purchases'] = $this->purchase_raw_model->getMonthlyPurchasesRaw($year, $warehouse_id);
        if ($pdf) {
            $html = $this->load->view($this->theme . 'reports/monthly', $this->data, true);
            $name = lang('monthly_purchases') . '_' . $year . '.pdf';
            $html = str_replace('<p class="introtext">' . lang('reports_calendar_text') . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses']    = $this->site->getAllWarehouses();
        $this->data['warehouse_id']  = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        $bc                          = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('reports'), 'page' => lang('reports')], ['link' => '#', 'page' => lang('monthly_purchases_report')]];
        $meta                        = ['page_title' => lang('monthly_purchases_report'), 'bc' => $bc];
        $this->page_construct('purchase_raw/monthly_purchases_raw', $meta, $this->data);
    }

    public function purchase_raw_product()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        // print_r($_POST);die;
        $this->data['product'] = $this->input->post('product');
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('raw_products_purchase_report')]];
        $meta = ['page_title' => lang('raw_products_purchase_report'), 'bc' => $bc];
		$this->page_construct('purchase_raw/product_raw_purchase', $meta, $this->data);
    }
    
    public function getPrRawPurchaseReport()
    {

        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $product     = $this->input->get('product') ? $this->input->get('product') : null;
        $start_date  = $this->input->get('start_date') ? $this->input->get('start_date') : null;
        $end_date    = $this->input->get('end_date') ? $this->input->get('end_date') : null;
        // echo '<pre>'; print_r($start_date); print_r($product);die;
        $sql = $this->purchase_raw_model->getPrRawPurchaseReport($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product);
        $records = $this->db->query($sql)->result();
        
        
        $data = [];
        foreach($records as $value) {

            $purchased = explode('__', $value->purchased);
            $nestedData = array();
            $nestedData[] = $value->code;
            $nestedData[] = $value->caption;
            $nestedData[] = '('. number_format($purchased[0], 0) .') ' . number_format($purchased[1], 2);
            
				$data[] = $nestedData;
		}
        $all_sql = $this->purchase_raw_model->total_getProductsRawReport($sSearch, $start_date, $end_date, $product);
        $all_records = $this->db->query($sql)->result();
		$totalData = sizeof($all_records); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }

    public function purchase_make_raw_product()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('make_product')]];
        $meta = ['page_title' => lang('make_product'), 'bc' => $bc];
		$this->page_construct('purchase_raw/purchase_make_raw_product', $meta, $this->data);
    }
    
    public function getPurchaseMakePrReport()
    {

        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $product     = $this->input->get('product') ? $this->input->get('product') : null;
        $start_date  = $this->input->get('start_date') ? $this->input->get('start_date') : null;
        $end_date    = $this->input->get('end_date') ? $this->input->get('end_date') : null;
        // echo '<pre>'; print_r($start_date); print_r($product);die;
        $sql = $this->purchase_raw_model->getPurchaseMakePrReport2($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product);
        $records = $this->db->query($sql)->result();
        
        // print_r($records); die;
        $data = [];
        foreach($records as $value) {

            // $product = $this->db->select('code, name')->get_where('products', ['id' => $value->product_id])->row();

            // $make_products_qty = $this->db->get_where('products', ['id' => $value->id])->row();

            // $purchased = explode('__', $value->purchased);
            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->name . ' (' . $value->code . ')';
            $nestedData[] = $value->make_qty;
            // $nestedData[] = $value->date;

            // $nestedData[] = '('. number_format($purchased[0], 0) .') ' . number_format($purchased[1], 2);
            // $nestedData[] = $make_products_qty->make_product_qty;
            
			$data[] = $nestedData;
		}
        $all_sql = $this->purchase_raw_model->total_getPurchaseMakePrReport2($sSearch, $start_date, $end_date, $product);
        $all_records = $this->db->query($sql)->result();
		$totalData = sizeof($all_records); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }

    public function get_products_raw()
    {
        $term = $this->input->get('term', true);
        if (strlen($term) < 1) {
            die();
        }
        $term = addslashes($term);
        $rows = $this->purchase_raw_model->getProductNamesRaw($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = ['id' => $row->id, 'label' => $row->caption . ' (' . $row->code . ')' ];
            }
            $this->sma->send_json($pr);
        } else {
            echo false;
        }
    }

    public function get_products()
    {
        $term = $this->input->get('term', true);
        if (strlen($term) < 1) {
            die();
        }
        $term = addslashes($term);
        $rows = $this->purchase_raw_model->getProductNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = ['id' => $row->id, 'label' => $row->name . ' (' . $row->code . ')' ];
            }
            $this->sma->send_json($pr);
        } else {
            echo false;
        }
    }

    public function make_products_report()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('purchase_raw_product')]];
        $meta = ['page_title' => lang('purchase_raw_product'), 'bc' => $bc];
		$this->page_construct('purchase_raw/product_raw_purchase', $meta, $this->data);
    }
    
    public function get_make_products_report()
    {

        $iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');

        $product     = $this->input->get('product') ? $this->input->get('product') : null;
        $start_date  = $this->input->get('start_date') ? $this->input->get('start_date') : null;
        $end_date    = $this->input->get('end_date') ? $this->input->get('end_date') : null;
        // echo '<pre>'; print_r($start_date); print_r($product);die;
        $sql = $this->purchase_raw_model->get_make_products_report($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product);
        $records = $this->db->query($sql)->result();
        
        $data = [];
        foreach($records as $value) {

            $purchased = explode('__', $value->purchased);
            $nestedData = array();
            $nestedData[] = $value->code;
            $nestedData[] = $value->caption;
            $nestedData[] = '('. number_format($purchased[0], 0) .') ' . number_format($purchased[1], 2);
            
				$data[] = $nestedData;
		}
        $all_sql = $this->purchase_raw_model->total_getProductsRawReport($sSearch, $start_date, $end_date, $product);
        $all_records = $this->db->query($sql)->result();
		$totalData = sizeof($all_records); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }

    public function xget_products_raw($value='')
    {
        $response = array();
        if($_POST['name']) {
            $name = $_POST['name'];
            
            $results = $this->db->like('caption', $name)->or_like('caption_alt', $name)->or_like('code', $name)->or_like('slug', $name)->get('products_raw')->result();
            // print_r($results); die;
            foreach ($results as $key => $value) {
                $response[] = array(
                    "value"=> $value->id,
                    "label"=> $value->caption
                );
            }
            echo json_encode($response);
        }
    }
}
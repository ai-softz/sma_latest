<?php

defined('BASEPATH') or die('No direct script access allowed');

class Synchronize_data extends MY_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->admin_model('Sales_model');
		$this->load->admin_model('Api_model');
		$this->load->api_model('sales_api');
		$this->load->model('site');

		$this->load->helper("file");
        $this->load->helper("directory");

        $this->path = APPPATH . DIRECTORY_SEPARATOR;
		$this->upload_path        = 'assets/uploads/';
        $this->thumbs_path        = 'assets/uploads/thumbs/';
        $this->image_types        = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size  = '1024';
        //$this->file = $this->path . "controllers/.txt";
        //print_r($this->path);exit();
	}

	public function index()
	{
		$sales = $this->db->limit(5)->get_where('sales', array('is_syncronize'=>0))->result_array();

		if(!empty($sales)) {
			foreach($sales as $sale) {
					$this->makeSalesJsonData($sale);
			}
		} else {
			echo "No data to synchronized!";
		}
		$this->data = array();
		//$this->load->view($this->theme . 'sales/synchronize_data', $this->data);
	}

	function makeSalesJsonData($sale)
	{
		$newPostArray=array();
		$settings = $this->site->get_setting(); 

		$newPostArray=$sale;
		// $newPostArray['api-key']="ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw";
		$newPostArray['api-key']=$settings->api_key;
		$newPostArray['user_id']=$sale["created_by"];
		$newPostArray['customer']=$sale["customer_id"];
		$newPostArray['biller']=$sale["biller_id"];
		$newPostArray['warehouse']=$sale["warehouse_id"];
		$newPostArray['pos_note']=$sale["note"];
		$newPostArray['staff_note']=$sale["staff_note"];
		$newPostArray['order_tax']=$sale["order_tax_id"];
		$newPostArray['discount']=$sale["order_discount_id"];//
		$newPostArray['shipping']=$sale["shipping"];
		$newPostArray['paidby']=$sale["payment_method"];//
		$newPostArray['local_id']=$sale["id"];//
		$newPostArray['order_platform']="Desktop";//
		$newPostArray['is_syncronize']="1";//
		$newPostArray['details'] = array();

		$items_array = $this->db->get_where('sale_items', array('sale_id'=>$sale['id']))->result_array();
		$itemsDetailArray = array();

		for ($i=0; $i < sizeof($items_array) ; $i++) { 
			$itemsDetailArray[$i]=$items_array[$i];
			$itemsDetailArray[$i]["product_base_quantity"]=$items_array[$i]["quantity"];
			$itemsDetailArray[$i]["product_unit"]=$items_array[$i]["product_unit_id"];
			$itemsDetailArray[$i]["product_option"]=$items_array[$i]["option_id"];
			$itemsDetailArray[$i]["product_discount"]=$items_array[$i]["item_discount"];
			$itemsDetailArray[$i]["product_tax"]=$items_array[$i]["tax_rate_id"];
			$itemsDetailArray[$i]["product_comment"]=$items_array[$i]["comment"];
			$itemsDetailArray[$i]["serial"]=$items_array[$i]["serial_no"];
		}
		$newPostArray['details'] = $itemsDetailArray;

		$payment_array = $this->db->get_where('payments', array('sale_id'=>$sale['id']))->result_array();
		$payment_details=array();

		for ($i=0; $i < sizeof($payment_array) ; $i++) { 
			$payment_details[$i] = $payment_array[$i];
			if($payment_array[$i]['paid_by'] == "gift_card"){
				$payment_details[$i]['paying_gift_card_no'] = $payment_array[$i]['cc_no'];
			}
			$payment_details[$i]['cc_cvv2'] = ""; 
			$payment_details[$i]['payment_note'] = $payment_array[$i]['note'];
			$payment_details[$i]['user_id'] = $sale["created_by"];
			$payment_details[$i]['amount'] = $payment_array[$i]['pos_paid'];
			$payment_details[$i]['balance_amount'] = $payment_array[$i]['pos_balance'];
		}
		$newPostArray['payment_details']=$payment_details;
// print_r($newPostArray); die();

		$options = array(
		  'http' => array(
		    'method'  => 'POST',
		    'content' => json_encode( $newPostArray ),
		    'header'=>  "Content-Type: application/json\r\n" .
		                "Accept: application/json\r\n"
		    )
		);
 		
		// $url="http://localhost/retaurant_online/api/v1/sales/postsales";
		
		$url = $settings->online_link."api/v1/sales/postsales";
		$context  = stream_context_create( $options );
		$result = file_get_contents( $url, false, $context );
		$response = json_decode( $result );

		if(!empty($response->sale)) {
			$sale_id = $response->sale->sale_id;
			$data = [
				'online_id' => $sale_id,
				'is_syncronize' => 1,
				'order_platform' => 'Desktop'
			];
			$update = $this->db->where('id', $sale["id"])->update('sales', $data);

		}

		if($update) {
			echo "Data is synchronized successfully! Local ID: " . $sale["id"] . " -> Online ID: ". $sale_id . "<br>";
		}
		else {
			echo "Something went wrong!";
		}

	}


	/*function synchronizeSales(){
			select all where is_syncronize=0
			loop sales{
				create json data
				after submit update online id and is_synchronize=1
			}
	}*/

	public function syncronize_user_from_online()
	{
		$settings = $this->site->get_setting(); 
		$url = $settings->online_link;
		$api_key = $settings->api_key;

		$url = $url."api/v1/synchronize_data/allusers?api-key=".$api_key;
		$json = file_get_contents($url);
		$records = json_decode($json);

		if(!empty($records->records_online)) {
			$records_online = $records->records_online;
		}
		$records_local = $this->db->get('users')->result();

		if(!empty($records->records_online)) {
			foreach($records_online  as $record_onl) {

				$check_local = $this->db->get_where('users', array('id' => $record_onl->id))->row();
				if($record_onl->id == $check_local->id) {
						$data = array(
							'last_ip_address' => $record_onl->last_ip_address,
							'ip_address' => $record_onl->ip_address,
							'username' => $record_onl->username,
							'password' => $record_onl->password,
							'salt' => $record_onl->salt,
							'email' => $record_onl->email,
							'activation_code' => $record_onl->activation_code,
							'forgotten_password_code' => $record_onl->forgotten_password_code,
							'forgotten_password_time' => $record_onl->forgotten_password_time,
							'remember_code' => $record_onl->remember_code,
							'created_on' => $record_onl->created_on,
							'last_login' => $record_onl->last_login,
							'active' => $record_onl->active,
							'first_name' => $record_onl->first_name,
							'last_name' => $record_onl->last_name,
							'company' => $record_onl->company,
							'phone' => $record_onl->phone,
							'avatar' => $record_onl->avatar,
							'gender' => $record_onl->gender,
							'group_id' => $record_onl->group_id,
							'warehouse_id' => $record_onl->warehouse_id,
							'biller_id' => $record_onl->biller_id,
							'company_id' => $record_onl->company_id,
							'show_cost' => $record_onl->show_cost,
							'show_price' => $record_onl->show_price,
							'award_points' => $record_onl->award_points,
							'view_right' => $record_onl->view_right,
							'edit_right' => $record_onl->edit_right,
							'allow_discount' => $record_onl->allow_discount,
							//'is_synchronize' => 1,
						);
						$update = $this->db->where('id', $record_onl->id)->update('users', $data);

					} else {
						$data = array(
							'id' => $record_onl->id,
							'last_ip_address' => $record_onl->last_ip_address,
							'ip_address' => $record_onl->ip_address,
							'username' => $record_onl->username,
							'password' => $record_onl->password,
							'salt' => $record_onl->salt,
							'email' => $record_onl->email,
							'activation_code' => $record_onl->activation_code,
							'forgotten_password_code' => $record_onl->forgotten_password_code,
							'forgotten_password_time' => $record_onl->forgotten_password_time,
							'remember_code' => $record_onl->remember_code,
							'created_on' => $record_onl->created_on,
							'last_login' => $record_onl->last_login,
							'active' => $record_onl->active,
							'first_name' => $record_onl->first_name,
							'last_name' => $record_onl->last_name,
							'company' => $record_onl->company,
							'phone' => $record_onl->phone,
							'avatar' => $record_onl->avatar,
							'gender' => $record_onl->gender,
							'group_id' => $record_onl->group_id,
							'warehouse_id' => $record_onl->warehouse_id,
							'biller_id' => $record_onl->biller_id,
							'company_id' => $record_onl->company_id,
							'show_cost' => $record_onl->show_cost,
							'show_price' => $record_onl->show_price,
							'award_points' => $record_onl->award_points,
							'view_right' => $record_onl->view_right,
							'edit_right' => $record_onl->edit_right,
							'allow_discount' => $record_onl->allow_discount,
							//'is_synchronize' => 1,
						);
						$insert = $this->db->insert('users', $data);

					}

			}
			if($update || $insert) {
				$this->session->set_flashdata('message', lang('users_synchronized_succesfully'));
            	admin_redirect('users');
			} else {
				$this->session->set_flashdata('error', lang('something_wrong_users_not_synchronized'));
        		admin_redirect('users');
			}
		} else {
			$this->session->set_flashdata('error', lang('something_wrong_users_not_synchronized'));
            admin_redirect('users');
		}
	}

	public function syncronize_billers_from_online()
	{
		$settings = $this->site->get_setting(); 
		$url = $settings->online_link;
		$api_key = $settings->api_key;

		$url = $url."api/v1/synchronize_data/allbillers?api-key=".$api_key;
		$json = file_get_contents($url);
		$records = json_decode($json);

		if(!empty($records->records_online)) {
			$records_online = $records->records_online;
		}
		
		if(!empty($records->records_online)) {
			foreach($records_online  as $record_onl) {

				$check_local = $this->db->get_where('companies', array('id' => $record_onl->id))->row();	
				
				if($record_onl->id == $check_local->id) {
						$data = array(
							'group_id' => $record_onl->group_id,
							'group_name' => $record_onl->group_name,
							'customer_group_id' => $record_onl->customer_group_id,
							'customer_group_name' => $record_onl->customer_group_name,
							'name' => $record_onl->name,
							'company' => $record_onl->company,
							'vat_no' => $record_onl->vat_no,
							'address' => $record_onl->address,
							'city' => $record_onl->city,
							'state' => $record_onl->state,
							'postal_code' => $record_onl->postal_code,
							'country' => $record_onl->country,
							'phone' => $record_onl->phone,
							'email' => $record_onl->email,
							'cf1' => $record_onl->cf1,
							'cf2' => $record_onl->cf2,
							'cf3' => $record_onl->cf3,
							'cf4' => $record_onl->cf4,
							'cf5' => $record_onl->cf5,
							'cf6' => $record_onl->cf6,
							'invoice_footer' => $record_onl->invoice_footer,
							'payment_term' => $record_onl->payment_term,
							'logo' => $record_onl->logo,
							'award_points' => $record_onl->award_points,
							'deposit_amount' => $record_onl->deposit_amount,
							'price_group_id' => $record_onl->price_group_id,
							'price_group_name' => $record_onl->price_group_name,
							'gst_no' => $record_onl->gst_no,
						);
						$update = $this->db->where('id', $record_onl->id)->update('companies', $data);
						
					} else {
						$data = array(
							'id' => $record_onl->id,
							'group_id' => $record_onl->group_id,
							'group_name' => $record_onl->group_name,
							'customer_group_id' => $record_onl->customer_group_id,
							'customer_group_name' => $record_onl->customer_group_name,
							'name' => $record_onl->name,
							'company' => $record_onl->company,
							'vat_no' => $record_onl->vat_no,
							'address' => $record_onl->address,
							'city' => $record_onl->city,
							'state' => $record_onl->state,
							'postal_code' => $record_onl->postal_code,
							'country' => $record_onl->country,
							'phone' => $record_onl->phone,
							'email' => $record_onl->email,
							'cf1' => $record_onl->cf1,
							'cf2' => $record_onl->cf2,
							'cf3' => $record_onl->cf3,
							'cf4' => $record_onl->cf4,
							'cf5' => $record_onl->cf5,
							'cf6' => $record_onl->cf6,
							'invoice_footer' => $record_onl->invoice_footer,
							'payment_term' => $record_onl->payment_term,
							'logo' => $record_onl->logo,
							'award_points' => $record_onl->award_points,
							'deposit_amount' => $record_onl->deposit_amount,
							'price_group_id' => $record_onl->price_group_id,
							'price_group_name' => $record_onl->price_group_name,
							'gst_no' => $record_onl->gst_no,
						);
						$insert = $this->db->insert('companies', $data);

					}

			}
			if($update || $insert) {
				$this->session->set_flashdata('message', lang('billers_synchronized_succesfully'));
            	admin_redirect('billers');
			} else {
				$this->session->set_flashdata('error', lang('something_wrong_billers_not_synchronized'));
        		admin_redirect('billers');
			}
		} else {
			$this->session->set_flashdata('error', lang('something_wrong_billers_not_synchronized'));
            admin_redirect('billers');
		}
	}

	public function syncronize_suppliers_from_online()
	{
		$settings = $this->site->get_setting(); 
		$url = $settings->online_link;
		$api_key = $settings->api_key;

		$url = $url."api/v1/synchronize_data/allsuppliers?api-key=".$api_key;
		$json = file_get_contents($url);
		$records = json_decode($json);

		if(!empty($records->records_online)) {
			$records_online = $records->records_online;
		}
		
		if(!empty($records->records_online)) {
			foreach($records_online as $record_onl) {
			
				$check_local = $this->db->get_where('companies', array('id' => $record_onl->id))->row();

				if($record_onl->id == $check_local->id) {
						$data = array(
							'group_id' => $record_onl->group_id,
							'group_name' => $record_onl->group_name,
							'customer_group_id' => $record_onl->customer_group_id,
							'customer_group_name' => $record_onl->customer_group_name,
							'name' => $record_onl->name,
							'company' => $record_onl->company,
							'vat_no' => $record_onl->vat_no,
							'address' => $record_onl->address,
							'city' => $record_onl->city,
							'state' => $record_onl->state,
							'postal_code' => $record_onl->postal_code,
							'country' => $record_onl->country,
							'phone' => $record_onl->phone,
							'email' => $record_onl->email,
							'cf1' => $record_onl->cf1,
							'cf2' => $record_onl->cf2,
							'cf3' => $record_onl->cf3,
							'cf4' => $record_onl->cf4,
							'cf5' => $record_onl->cf5,
							'cf6' => $record_onl->cf6,
							'invoice_footer' => $record_onl->invoice_footer,
							'payment_term' => $record_onl->payment_term,
							'logo' => $record_onl->logo,
							'award_points' => $record_onl->award_points,
							'deposit_amount' => $record_onl->deposit_amount,
							'price_group_id' => $record_onl->price_group_id,
							'price_group_name' => $record_onl->price_group_name,
							'gst_no' => $record_onl->gst_no,
						);
						$update = $this->db->where('id', $record_onl->id)->update('companies', $data);
						
				} else {
						$data = array(
							'id' => $record_onl->id,
							'group_id' => $record_onl->group_id,
							'group_name' => $record_onl->group_name,
							'customer_group_id' => $record_onl->customer_group_id,
							'customer_group_name' => $record_onl->customer_group_name,
							'name' => $record_onl->name,
							'company' => $record_onl->company,
							'vat_no' => $record_onl->vat_no,
							'address' => $record_onl->address,
							'city' => $record_onl->city,
							'state' => $record_onl->state,
							'postal_code' => $record_onl->postal_code,
							'country' => $record_onl->country,
							'phone' => $record_onl->phone,
							'email' => $record_onl->email,
							'cf1' => $record_onl->cf1,
							'cf2' => $record_onl->cf2,
							'cf3' => $record_onl->cf3,
							'cf4' => $record_onl->cf4,
							'cf5' => $record_onl->cf5,
							'cf6' => $record_onl->cf6,
							'invoice_footer' => $record_onl->invoice_footer,
							'payment_term' => $record_onl->payment_term,
							'logo' => $record_onl->logo,
							'award_points' => $record_onl->award_points,
							'deposit_amount' => $record_onl->deposit_amount,
							'price_group_id' => $record_onl->price_group_id,
							'price_group_name' => $record_onl->price_group_name,
							'gst_no' => $record_onl->gst_no,
						);
						$insert = $this->db->insert('companies', $data);

					}

			}
			if($update || $insert) {
				$this->session->set_flashdata('message', lang('suplliers_synchronized_succesfully'));
            	admin_redirect('suppliers');
			} else {
				$this->session->set_flashdata('error', lang('something_wrong_suplliers_not_synchronized'));
        		admin_redirect('suppliers');
			}
		} else {
			$this->session->set_flashdata('error', lang('something_wrong_suplliers_not_synchronized'));
            admin_redirect('suppliers');
		}
	}

	public function syncronize_customers_from_online()
	{
		$settings = $this->site->get_setting(); 
		$url = $settings->online_link;
		$api_key = $settings->api_key;

		$url = $url."api/v1/synchronize_data/allcustomers?api-key=".$api_key;
		$json = file_get_contents($url);
		$records = json_decode($json);

		if(!empty($records->records_online)) {
			$records_online = $records->records_online;
		}

		if(!empty($records->records_online)) {
			foreach($records_online  as $record_onl) {

				$check_local = $this->db->get_where('companies', array('id' => $record_onl->id))->row();
				if($record_onl->id == $check_local->id) {
						$data = array(
							'group_id' => $record_onl->group_id,
							'group_name' => $record_onl->group_name,
							'customer_group_id' => $record_onl->customer_group_id,
							'customer_group_name' => $record_onl->customer_group_name,
							'name' => $record_onl->name,
							'company' => $record_onl->company,
							'vat_no' => $record_onl->vat_no,
							'address' => $record_onl->address,
							'city' => $record_onl->city,
							'state' => $record_onl->state,
							'postal_code' => $record_onl->postal_code,
							'country' => $record_onl->country,
							'phone' => $record_onl->phone,
							'email' => $record_onl->email,
							'cf1' => $record_onl->cf1,
							'cf2' => $record_onl->cf2,
							'cf3' => $record_onl->cf3,
							'cf4' => $record_onl->cf4,
							'cf5' => $record_onl->cf5,
							'cf6' => $record_onl->cf6,
							'invoice_footer' => $record_onl->invoice_footer,
							'payment_term' => $record_onl->payment_term,
							'logo' => $record_onl->logo,
							'award_points' => $record_onl->award_points,
							'deposit_amount' => $record_onl->deposit_amount,
							'price_group_id' => $record_onl->price_group_id,
							'price_group_name' => $record_onl->price_group_name,
							'gst_no' => $record_onl->gst_no,
						);
					$update = $this->db->where('id', $record_onl->id)->update('companies', $data);

					} else {
						$data = array(
							'id' => $record_onl->id,
							'group_id' => $record_onl->group_id,
							'group_name' => $record_onl->group_name,
							'customer_group_id' => $record_onl->customer_group_id,
							'customer_group_name' => $record_onl->customer_group_name,
							'name' => $record_onl->name,
							'company' => $record_onl->company,
							'vat_no' => $record_onl->vat_no,
							'address' => $record_onl->address,
							'city' => $record_onl->city,
							'state' => $record_onl->state,
							'postal_code' => $record_onl->postal_code,
							'country' => $record_onl->country,
							'phone' => $record_onl->phone,
							'email' => $record_onl->email,
							'cf1' => $record_onl->cf1,
							'cf2' => $record_onl->cf2,
							'cf3' => $record_onl->cf3,
							'cf4' => $record_onl->cf4,
							'cf5' => $record_onl->cf5,
							'cf6' => $record_onl->cf6,
							'invoice_footer' => $record_onl->invoice_footer,
							'payment_term' => $record_onl->payment_term,
							'logo' => $record_onl->logo,
							'award_points' => $record_onl->award_points,
							'deposit_amount' => $record_onl->deposit_amount,
							'price_group_id' => $record_onl->price_group_id,
							'price_group_name' => $record_onl->price_group_name,
							'gst_no' => $record_onl->gst_no,
						);
						$insert = $this->db->insert('companies', $data);

					}

			}
			if($update || $insert) {
				$this->session->set_flashdata('message', lang('customers_synchronized_succesfully'));
            	admin_redirect('customers');
			} else {
				$this->session->set_flashdata('error', lang('something_wrong_customers_not_synchronized'));
        		admin_redirect('customers');
			}
		} else {
			$this->session->set_flashdata('error', lang('something_wrong_customers_not_synchronized'));
            admin_redirect('customers');
		}
	}

	public function syncronize_categories_from_online()
	{
		$settings = $this->site->get_setting(); 
		$url = $settings->online_link;
		$api_key = $settings->api_key;

		$url = $url."api/v1/synchronize_data/allcategories?api-key=".$api_key;
		$json = file_get_contents($url);
		$records = json_decode($json);

		if(!empty($records->records_online)) {
			$records_online = $records->records_online;
		}

		if(!empty($records->records_online)) {
			foreach($records_online  as $record_onl) {

				$check_local = $this->db->get_where('categories', array('id' => $record_onl->id))->row();
				

					if($record_onl->id == $check_local->id) {
						$data = array(
							'code' => $record_onl->code,
							'name' => $record_onl->name,
							'name_alt' => $record_onl->name_alt,
							'image' => $record_onl->image,
							'parent_id' => $record_onl->parent_id,
							'slug' => $record_onl->slug,
							'description' => $record_onl->description,
							'hide_status' => $record_onl->hide_status,
							'display_serial' => $record_onl->display_serial,
						);
						
						$update = $this->db->where('id', $record_onl->id)->update('categories', $data);
						
					}else{
						
						$data = array(
							'id' => $record_onl->id,
							'code' => $record_onl->code,
							'name' => $record_onl->name,
							'name_alt' => $record_onl->name_alt,
							'image' => $record_onl->image,
							'parent_id' => $record_onl->parent_id,
							'slug' => $record_onl->slug,
							'description' => $record_onl->description,
							'hide_status' => $record_onl->hide_status,
							'display_serial' => $record_onl->display_serial,
						);
						
						$insert = $this->db->insert('categories', $data);

					}
					if (!file_exists($this->upload_path.$record_onl->image)) {
						copy($settings->online_link.'assets/uploads/'.$record_onl->image, $this->upload_path.$record_onl->image);
					}
					if (!file_exists($this->thumbs_path.$record_onl->image)) {
						copy($settings->online_link.'assets/uploads/thumbs/'.$record_onl->image, $this->thumbs_path.$record_onl->image);
					} 
			}
			if($update || $insert) {
				$this->site->generateCategoryjsonfiles();
				$this->session->set_flashdata('message', lang('categories_synchronized_succesfully'));
            	admin_redirect('system_settings/categories');
			} else {
				$this->session->set_flashdata('error', lang('something_wrong_categories_not_synchronized'));
        		admin_redirect('system_settings/categories');
			}
		} else {
			$this->session->set_flashdata('error', lang('something_wrong_categories_not_synchronized'));
            admin_redirect('system_settings/categories');
		}
	}

	public function syncronize_products_from_online()
	{
		$settings = $this->site->get_setting(); 
		$url = $settings->online_link;
		$api_key = $settings->api_key;

		$url = $url."/api/v1/synchronize_data/allproducts?api-key=".$api_key;
		$json = file_get_contents($url);
		$records = json_decode($json);

		if(!empty($records->records_online)) {
			$records_online = $records->records_online;
		}
// print_r($records_online); die;
		if(!empty($records->records_online)) {
			foreach($records_online  as $record_onl) {
					$check_local = $this->db->get_where('products', array('id' => $record_onl->id))->row();
					if($record_onl->id == $check_local->id) {

						$fields_table   = $this->db->list_fields($this->db->dbprefix('products'));
						foreach ($fields_table as $field) {
				                $data[$field] = $record_onl->$field;
				        }
						unset($data['id']);
						$update = $this->db->where('id', $record_onl->id)->update('products', $data);

						// Sync product variants
						if($update) {
							$this->db->where('product_id', $record_onl->id)->delete('product_variants');
							if(!empty($record_onl->product_variants)) {
							foreach($record_onl->product_variants as $variant_online) {
									$data_variant = [
										'id' => $variant_online->id,
										'product_id' => $variant_online->product_id,
										'name' => $variant_online->name,
										'cost' => $variant_online->cost,
										'price' => $variant_online->price,
										'quantity' => $variant_online->quantity,
									];
									$this->db->insert('product_variants', $data_variant);
							}
							}
						}
						
					} else {
						$fields_table   = $this->db->list_fields($this->db->dbprefix('products'));
						foreach ($fields_table as $field) {
				                $data[$field] = $record_onl->$field;
				        }
						$insert = $this->db->insert('products', $data);

						// Sync product variants
						if($insert) {
						if(!empty($record_onl->product_variants)) {
							foreach($record_onl->product_variants as $variant_online) {
								$data_variant = [
									'id' => $variant_online->id,
									'product_id' => $variant_online->product_id,
									'name' => $variant_online->name,
									'cost' => $variant_online->cost,
									'price' => $variant_online->price,
									'quantity' => $variant_online->quantity,
								];
								$this->db->insert('product_variants', $data_variant);
							}
						}
						}
					}

					if (!file_exists($this->upload_path.$record_onl->image)) {
						copy($settings->online_link.'assets/uploads/'.$record_onl->image, $this->upload_path.$record_onl->image);
					}
					if (!file_exists($this->thumbs_path.$record_onl->image)) {
						copy($settings->online_link.'assets/uploads/thumbs/'.$record_onl->image, $this->thumbs_path.$record_onl->image);
					} 

			}
			if($update || $insert) {
				$this->site->generateProductjsonfiles();
				$this->session->set_flashdata('message', lang('products_synchronized_succesfully'));
            	admin_redirect('products');
			} else {
				$this->session->set_flashdata('error', lang('something_wrong_products_retrieved_but_not_synchronized'));
        		admin_redirect('products');
			}
		} else {
			$this->session->set_flashdata('error', lang('something_wrong_products_not_synchronized'));
            admin_redirect('products');
		}
	}

	public function synchronize_raw_category_from_local()
    {
        $records = $this->db->limit(5)->get_where('categories_raw', array('is_synchronize'=>0))->result_array();

        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makeRawCategoryJsonFromLocal($record);
            }
			$this->session->set_flashdata('message', lang('data_synchronized_succesfully'));
			admin_redirect('purchases_raw/category_raw_material');
        } else {
			$this->session->set_flashdata('error', lang('something_wrong_or_data_already_synchronized'));
			admin_redirect('purchases_raw/category_raw_material');
        }
        $this->data = array();
    }

    function makeRawCategoryJsonFromLocal($record)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        $newPostArray=$record;
        $newPostArray['api-key']=$settings->api_key;
		$api_key=$settings->api_key;
        $newPostArray['is_synchronize']= "1";//
		
		$options = array(
			'http' => array(
			  'method'  => 'POST',
			  'content' => json_encode( $newPostArray ),
			  'header'=>  "Content-Type: application/json\r\n" .
						  "Accept: application/json\r\n"
			  )
		  );
		   		  
		// $url = "https://eshtri.net/resturant_test/api/v1/purchases_raw/addcategories";
		$url = $settings->online_link."api/v1/purchases_raw/add_raw_categories";
		$context  = stream_context_create( $options );
		$result = file_get_contents( $url, false, $context );
		$response = json_decode( $result );
		$update = false;
        if(!empty($response)) {
            // $category_id = $response->category_id;
            $data = [
                'is_synchronize' => 1,
            ];
            $update = $this->db->where('id', $record["id"])->update('categories_raw', $data);
        }
        if($update) {
            // echo "Data is synchronized successfully! Local ID: " . $record["id"] . " -> Online ID: ". $category_id . "<br>";
            echo "Data is synchronized successfully! ID: " . $record["id"] . "<br>";
			// return true;
        }
        else {
            // echo "Something went wrong!";
        }
    }
	public function synchronize_raw_products_from_local()
    {
        $records = $this->db->limit(5)->get_where('products_raw', array('is_synchronize'=>0))->result_array();
        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makeRawProductsJsonFromLocal($record);
            }
			$this->session->set_flashdata('message', lang('data_synchronized_succesfully'));
			admin_redirect('purchases_raw/products_raw_material');
        } else {
			$this->session->set_flashdata('error', lang('something_wrong_or_data_already_synchronized'));
			admin_redirect('purchases_raw/products_raw_material');
        }
        $this->data = array();
    }

    function makeRawProductsJsonFromLocal($record)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        $newPostArray=$record;
        $newPostArray['api-key']=$settings->api_key;
		$api_key=$settings->api_key;
        $newPostArray['is_synchronize']= "1";//
		
		$options = array(
			'http' => array(
			  'method'  => 'POST',
			  'content' => json_encode( $newPostArray ),
			  'header'=>  "Content-Type: application/json\r\n" .
						  "Accept: application/json\r\n"
			  )
		  );
		   		  
		// $url = "https://eshtri.net/resturant_test/api/v1/purchases_raw/addcategories";
		$url = $settings->online_link."api/v1/purchases_raw/add_raw_products";
		$context  = stream_context_create( $options );
		$result = file_get_contents( $url, false, $context );
		$response = json_decode( $result );

		$update = false;
        if(!empty($response)) {
            $insert_id = $response->insert_id;
            $data = [
                'is_synchronize' => 1,
            ];
            $update = $this->db->where('id', $record["id"])->update('products_raw', $data);
        }
        if($update) {
            echo "Data is synchronized successfully! Local ID: " . $record["id"] . "<br>";
        }
        else {
            echo "Something went wrong!";
        }
    }
	public function synchronize_raw_purchase_from_local()
    {
        $records = $this->db->limit(5)->get_where('purchases_raw', array('is_synchronize'=>0))->result_array();
        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makeRawPurchaseJsonFromLocal($record);
            }
			$this->session->set_flashdata('message', lang('data_synchronized_succesfully'));
			admin_redirect('purchases_raw');
        } else {
            $this->session->set_flashdata('error', lang('something_wrong_data_not_synchronized'));
			admin_redirect('purchases_raw');
        }
        $this->data = array();
    }

    function makeRawPurchaseJsonFromLocal($record)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        $newPostArray=$record;
		$newPostArray['api-key']=$settings->api_key;
		$newPostArray['is_syncronize']="1";//
		
		$items_array = $this->db->get_where('purchase_items_raw', array('purchase_id '=>$record['id']))->result_array();
		$itemsDetailArray = array();

		for ($i=0; $i < sizeof($items_array) ; $i++) { 
			$itemsDetailArray[$i]=$items_array[$i];
		}
		$newPostArray['details'] = $itemsDetailArray;

		$payment_array = $this->db->get_where('payments', array('purchase_raw_id'=>$record['id']))->result_array();
		$payment_details=array();

		for ($i=0; $i < sizeof($payment_array) ; $i++) { 
			$payment_details[$i] = $payment_array[$i];
		}
		$newPostArray['payment_details']=$payment_details;
		
		$options = array(
			'http' => array(
			  'method'  => 'POST',
			  'content' => json_encode( $newPostArray ),
			  'header'=>  "Content-Type: application/json\r\n" .
						  "Accept: application/json\r\n"
			  )
		  );
		   		  
		// $url = "https://eshtri.net/resturant_test/api/v1/purchases_raw/addcategories";
		$url = $settings->online_link."api/v1/purchases_raw/add_raw_purchases";
		$context  = stream_context_create( $options );
		$result = file_get_contents( $url, false, $context );
		$response = json_decode( $result );
// echo "<pre>"; print_r($response); die;
		$update = false;
        if(!empty($response)) {
            $insert_id = $response->insert_id;
            $data = [
                'is_synchronize' => 1,
            ];
            $update = $this->db->where('id', $record["id"])->update('purchases_raw', $data);
        }
        if($update) {
            echo "Data is synchronized successfully! Local ID: " . $record["id"] . " -> Online ID: ". $insert_id . "<br>";
        }
        else {
            echo "Something went wrong!";
        }
    }

}
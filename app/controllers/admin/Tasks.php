<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tasks extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('tasks_model');
        $this->load->library('form_validation');

    }

    public function list_tasks()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $this->data['task_statuses']  = array(
        	array('id' => '1', 'name' => 'Not Started'),
        	array('id' => '2', 'name' => 'Awaiting Feedback'),
        	array('id' => '3', 'name' => 'Testing'),
        	array('id' => '4', 'name' => 'In Progress'),
        	array('id' => '5', 'name' => 'Complete'),
        );

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('tasks'), 'page' => lang('tasks')], ['link' => '#', 'page' => lang('tasks')]];
        $meta = ['page_title' => lang('tasks'), 'bc' => $bc];
        $this->page_construct('tasks/tasks', $meta, $this->data);
    }

    public function getTasks()
    {

        $iDisplayLength=$this->input->post('iDisplayLength');
        $iDisplayStart=$this->input->post('iDisplayStart');
        $sSortDir_0=$this->input->post('sSortDir_0');
        $iSortCol_0=$this->input->post('iSortCol_0');
        $sSearch=$this->input->post('sSearch');

        $records = $this->tasks_model->getTaskTable($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);

        $data = [];
        foreach($records as $value) {

            $task_timer=$this->db->get_where('sma_taskstimers', array('task_id'=>$value->task_id, 'status'=>'incomplete'))->row();
            if($task_timer->status == 'incomplete') {
                $task_status = '<i class="fa task-info-icon fa-fw fa-lg fa-clock-o text-danger"></i>';
            } else {
                $task_status = '';
            }
            
            $nestedData = array();
            $nestedData[] = $value->id ;
            $nestedData[] = $value->task_id;
            $nestedData[] = $value->name . ' ' . $task_status;
            $nestedData[] = $value->startdate;
            $nestedData[] = $value->duedate;
            $nestedData[] = $value->users_name;
            $nestedData[] = $value->tag_name;
            $nestedData[] = $value->task_status;
            $nestedData[] = $value->task_priority;
            
            $edit = '';
            $edit = "<div class=\"text-center\"><a href='" . admin_url('tasks/view_task/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_task') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('tasks/edit_task/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_task') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_task') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('tasks/delete_task/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                
                $nestedData[] = $edit;
                $data[] = $nestedData;
        }
        $all_users = $this->tasks_model->total_getTaskTable($sSearch);
        $totalData = sizeof($all_users); 

        $sOutput = [
            'iTotalRecords'        => $totalData,
            'iTotalDisplayRecords' => $totalData,
            'aaData'               => $data,
        ];
        echo json_encode($sOutput);
        
    }

    public function xgetTasks()
    {
              
        $this->load->library('datatables');
// tasks.status as task_status,,  tasks.priority as task_priority
        $this->datatables
            ->select('tasks.id as id, tasks.id as task_id, tasks.name,  tasks.startdate, tasks.duedate, users.first_name as first_name, tags.name as tag_name, tasks.status as task_status, tasks.priority as task_priority')
            ->from('tasks')
            ->join('taggables', 'taggables.rel_id = tasks.id', 'left')
            ->join('tags', 'tags.id = taggables.tag_id', 'left')
            ->join('task_assigned', 'task_assigned.taskid = tasks.id', 'left')
            ->join('users', 'users.id = task_assigned.staffid', 'left')

            ->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('tasks/view_task/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('view_task') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('tasks/edit_task/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_task') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_task') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('tasks/delete_task/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
            ->group_by('tasks.id')
           
            ;

        echo $this->datatables->generate();
    }

    public function bulk_delete()
    {
        if($this->input->post()) {

            $ids = $this->input->post('ids');
            foreach($ids as $id) {
                $this->tasks_model->delete_task($id);
            }
        }
        return true;
    }

    public function tasks_status_bulk_action()
    {
        if($this->input->post()) {
            if(!empty($this->input->post('status'))) {
                $ids = $this->input->post('ids');
                $status_id = $this->input->post('status');
                $user_id = $this->session->userdata('user_id');
                foreach($ids as $id) {
                    $this->tasks_model->updateTaskStatus($id, $status_id, $user_id);
                }
            }
        }
        return true;
    }

        public function task_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->form_validation->set_rules('form_action', lang('form_action'), 'required');

        if ($this->form_validation->run() == true) {
            print_r($_POST['val']); die;
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteSale($id);
                    }
                    $this->session->set_flashdata('message', lang('sales_deleted'));
                    redirect($_SERVER['HTTP_REFERER']);
                } 
            } else {
                $this->session->set_flashdata('error', lang('no_task_selected'));
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function bulk_actions()
    {
        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'tasks/bulk_actions', $this->data);
    }

    public function delete_task($task_id)
    {
        $delete = $this->tasks_model->delete_task($task_id);
        if($delete) {
             $this->sma->send_json(['error' => 0, 'msg' => lang('task_deleted')]);
        }
    }

    public function view_task($task_id=null) 
    {
    	$task = $this->tasks_model->getTask($task_id);
    	$checklists = $this->tasks_model->get_checklist_items($task_id);
    	$task->checklist_items = $checklists;

    	$this->data['task'] = $task;
    	$this->data['checklists'] = $this->tasks_model->get_checklist_items($task_id);

         // Get all task assignees
         $task_assignees = $this->db->get_where('task_assigned', array('taskid'=>$task->id))->result();
         $assignees_ids = array(); 
         foreach ($task_assignees as $row) {
            $assignees_ids[] = $row->staffid;
         }
         $this->data['assignees_ids'] =  $assignees_ids;

         $task_assignees2 = $this->db
                    ->select('task_assigned.*, users.first_name, users.last_name')
                    ->from('task_assigned')
                    ->join('users', 'users.id = task_assigned.staffid')
                    ->where(array('taskid'=>$task->id))
                    ->get()
                    ->result_array();
         $this->data['task_assignees'] =  $task_assignees2;           
         // Get all staffs
         $this->data['staff'] = $this->db->select('id, email, first_name, last_name')->from('users')->get()->result_array();

         $taskid = $this->input->get('taskid');
         $result = $this->tasks_model->get_checklist_items($task_id);
         $this->data['checklist_items'] = $result;

         $this->data['task_comments'] = 
                 $this->db
                 ->select('task_comments.*, users.first_name, users.last_name')
                 ->from('task_comments')
                 ->join('users', 'users.id = task_comments.staffid', 'left')
                 ->where('task_comments.taskid', $task_id)
                 ->group_by('task_comments.id')
                 ->order_by('task_comments.id desc')
                 ->get()
                 ->result_array();

         $this->data['reminders'] = $this->db
                     ->select('task_reminders.*, users.first_name, users.last_name')
                     ->from('task_reminders')
                     ->join('users', 'users.id = task_reminders.staff', 'left')
                     ->where(array('rel_id' => $task_id, 'rel_type' => 'task'))
                     ->get()
                     ->result_array();

         $tags_result = $this->db
                     ->select('taggables.*, tags.name')
                     ->from('taggables')
                     ->join('tags', 'tags.id = taggables.tag_id', 'left')
                     ->where(array('rel_id' => $task_id, 'rel_type' => 'task'))
                     ->get()
                     ->result();
         $tags_arr = array();
         foreach($tags_result as $tag) {
            $tags_arr[] = $tag->name;
         }

         $this->data['tags'] = implode(',', $tags_arr);

    	
    	$this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'tasks/view_task', $this->data);

    }

    public function edit_task($task_id=null) 
    {
        $task = $this->tasks_model->getTask($task_id);
        

        $this->data['task'] = $task;
        
         $tags_result = $this->db
                     ->select('taggables.*, tags.name')
                     ->from('taggables')
                     ->join('tags', 'tags.id = taggables.tag_id', 'left')
                     ->where(array('rel_id' => $task_id, 'rel_type' => 'task'))
                     ->get()
                     ->result();
         $tags_arr = array();
         foreach($tags_result as $tag) {
            $tags_arr[] = $tag->name;
         }

         $this->data['tags'] = implode(',', $tags_arr);

         $this->form_validation->set_rules('name', lang('name'), 'trim|required|alpha_numeric_spaces');

        if ($this->form_validation->run() == true) {
            $data = [
                'task_id'            => $this->input->post('task_id'),
                'name'            => $this->input->post('name'),
                'startdate'            => $this->input->post('startdate'),
                'duedate'            => $this->input->post('duedate'),
                'priority'            => $this->input->post('priority'),
                'repeat_every'            => $this->input->post('repeat_every'),
                'cycles'            => $this->input->post('cycles') ? $this->input->post('cycles') : 0,
                'tags'            => $this->input->post('tags'),
                'description'            => $this->input->post('description'),
            ];
        } elseif ($this->input->post('edit_task')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('tasks/edit_task');
        }

        if ($this->form_validation->run() == true && $this->tasks_model->editTask($data)) {
            // print_r($this->input->post()); exit();
            $this->session->set_flashdata('message', lang('task_edited'));
            admin_redirect('tasks/list_tasks');
        }
        else {
            $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'tasks/edit_task', $this->data);
        }

        
        

    }

    public function add_task_assignees() 
    {
        // echo "<pre>";print_r($this->input->post());die;
        if($this->input->post()) {
            $assignee_id = $this->input->post('assignee_id');
            $taskid = $this->input->post('taskid');
            $user_id = $this->session->userdata('user_id');

            $insert = $this->tasks_model->addTaskAssignee($assignee_id, $taskid, $user_id);

            if($insert) {
                // return true;
                echo json_encode(array('taskid' => $taskid));
            } else {
                return false;
            }
        }
    }

    public function delete_task_assignees() 
    {
        // echo "<pre>";print_r($this->input->post());die;
        if($this->input->post()) {
            $assignee_id = $this->input->post('assignee_id');
            $taskid = $this->input->post('taskid');

            $delete = $this->tasks_model->deleteTaskAssignee($assignee_id, $taskid);

            if($delete) {
                // return true;
                echo json_encode(array('taskid' => $taskid));
            } else {
                return false;
            }
        }
    }

    public function get_task_assignees()
    {
        if($this->input->post()) {
            $taskid = $this->input->post('taskid');
        }

        $task_assignees = $this->db
                    ->select('task_assigned.*, users.first_name, users.last_name')
                    ->from('task_assigned')
                    ->join('users', 'users.id = task_assigned.staffid')
                    ->where(array('taskid'=>$taskid))
                    ->get()
                    ->result_array();

           $_assignees = '';
           foreach ($task_assignees as $assignee) {
            $_remove_assigne = '';
            
              $_remove_assigne = ' <a href="#" class="remove-task-user text-danger" onclick="remove_assignee(' . $assignee['id'] . ',' . $taskid . '); return false;"><i class="fa fa-remove"></i></a>';
           
           $_assignees .= '
           <div class="task-user"  data-toggle="tooltip" data-title="'.html_escape($assignee['first_name']).'">
           '. $assignee['first_name'] . ' ' . $assignee['last_name'] . $_remove_assigne . '</span>
           </div>';
           }
           if ($_assignees == '') {
           $_assignees = '<div class="text-danger display-block">'.lang('task_no_assignees').'</div>';
           }
           echo $_assignees;
               
    }

    public function task_mark_as()
    {
        // print_r($this->input->post()); exit();
        if($this->input->post()) {
            $taskid = $this->input->post('taskid');
            $status = $this->input->post('status');
            $user_id = $this->session->userdata('user_id');

            $update = $this->tasks_model->task_mark_as($taskid, $status, $user_id);

            if($update) {
                return true;
            }
        } 
    }

    public function task_mark_as2()
    {
        // print_r($this->input->post()); exit();
        if($this->input->post()) {
            $taskid = $this->input->post('taskid');
            $status = $this->input->post('status');
            $user_id = $this->session->userdata('user_id');

            $update = $this->tasks_model->task_mark_as($taskid, $status, $user_id);

            if($update) {
                $task = $this->tasks_model->getTask($taskid);
                $output = '';
                $output .= '<i class="fa task-info-icon fa-fw fa-lg pull-left fa-star-o"></i>';
                $task_status= 
                        array(
                          array('id' => '1', 'name' => 'Not Started'),
                          array('id' => '2', 'name' => 'Awaiting Feedback'),
                          array('id' => '3', 'name' => 'Testing'),
                          array('id' => '4', 'name' => 'In Progress'),
                          array('id' => '5', 'name' => 'Complete'),
                        );    
                        
                $output .= ''.lang('task_status').': ';
                  
                foreach ($task_status as $status) {
                      if ($status['id'] == $task->status) {
                $output .= '<a class="dropdown-toggle" data-toggle="dropdown" href="#" >'. $status['name']. '</a>';
                      }
                  }
                $output .= '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
                        // show status dropdown
                        foreach($task_status as $status){ 
                         if($task->status != $status['id']){ 
                            $output .= '<li  class="border-bottom">
                               <a href="#" onclick="task_mark_as('.$status['id'].',' . $task->id.'); return false;">
                               '. $status['name'].'
                               </a>
                            </li>';
                       
                         } 
                        } 
                $output .= '</ul>';
            echo $output;
        } 
        }
    }

    public function task_change_priority()
    {
        // print_r($this->input->post()); exit();
        if($this->input->post()) {
            $taskid = $this->input->post('taskid');
            $priority = $this->input->post('priority');
            $user_id = $this->session->userdata('user_id');

            $update = $this->tasks_model->task_change_priority($taskid, $priority, $user_id);

            if($update) {
                $task = $this->tasks_model->getTask($taskid);
                $output = '';
                $output .= '<i class="fa task-info-icon fa-fw fa-lg pull-left fa-bolt"></i>';
                $task_priorities  = array(
                            array('id' => '1', 'name' => lang('task_priority_low')),
                            array('id' => '2', 'name' => lang('task_priority_medium')),
                            array('id' => '3', 'name' => lang('task_priority_high')),
                            array('id' => '4', 'name' => lang('task_priority_urgent')),
                          );    
                        
                $output .= ''.lang('task_single_priority').': ';
                  
                foreach ($task_priorities as $priority) {
                      if ($priority['id'] == $task->priority) {
                $output .= '<a class="dropdown-toggle" data-toggle="dropdown" href="#" >'. $priority['name']. '</a>';
                      }
                  }
                $output .= '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
                        // show priority dropdown
                        foreach($task_priorities as $priority){ 
                         if($task->priority != $priority['id']){ 
                            $output .= '<li  class="border-bottom">
                               <a href="#" onclick="task_change_priority('.$priority['id'].',' . $task->id.'); return false;">
                               '. $priority['name'].'
                               </a>
                            </li>';
                       
                         } 
                        } 
                $output .= '</ul>';
            echo $output;
        } 
        }
    } 

    public function start_stop_timer()
    {
        $taskid = $this->input->post('taskid');
        $staff_id = $this->session->userdata('user_id');
        $now = $this->get_current_utc_time();

        $current_clock_record = $this->current_clock_in_record($taskid);
        // print_r($current_clock_record); die;

        if ($current_clock_record && $current_clock_record->id) {
            $data = array(
                "out_time" => $now,
                "status" => "complete",
                "note" => $note
            );
            $id = $current_clock_record->id;
            $this->db->where('id', $id)->update('taskstimers', $data);
            $time_diff = strtotime($now) - strtotime($current_clock_record->in_time);

            $hours = floor($time_diff / 3600);
            $minutes = floor(($time_diff / 60) % 60);
            $seconds = $time_diff % 60;

            $hours = ($hours < 10) ? ('0'.$hours) : $hours;
            $minutes = ($minutes < 10) ? ('0'.$minutes) : $minutes;
            $seconds = ($seconds < 10) ? ('0'.$seconds) : $seconds;
            $show_time = $hours . ' : ' . $minutes . ' : ' . $seconds;
            echo $show_time;

        } else {
            $data = array(
                "in_time" => $now,
                "status" => "incomplete",
                "staff_id" => $staff_id,
                "task_id" => $taskid
            );
            $this->db->insert('taskstimers', $data);
            echo "Clock Started: ".date('d-m-Y H:i:s');
        }
    }
    function current_clock_in_record($taskid) {
        $taskstimers_table = $this->db->dbprefix('taskstimers');
        $sql = "SELECT $taskstimers_table.*
        FROM $taskstimers_table
        WHERE $taskstimers_table.deleted=0 AND $taskstimers_table.task_id=$taskid AND $taskstimers_table.status='incomplete'";

        $result = $this->db->query($sql);
        if ($result->num_rows()) {
            return $result->row();
        } else {
            return false;
        }
    }
    public function get_current_utc_time($format = "Y-m-d H:i:s") {
        $d = DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d H:i:s"));
        $d->setTimeZone(new DateTimeZone("UTC"));
        return $d->format($format);
    }

    public function count_time()
    {
        if(isset($_POST['time'])) {

            if($_POST['time'] == '0') {
                $total_logged_time = $this->session->userdata('logged_time');
                $taskid = $this->input->post('taskid');

                $task_timer=$this->db->get_where('sma_taskstimers', array('task_id'=>$taskid))->row();
                $total_duration = $total_logged_time + $task_timer->duration;
                
                $data = ['duration' => $total_duration, 'timer_status' => 0];
                $this->db->where('task_id', $taskid)->update('sma_taskstimers', $data);  
                
                $this->session->unset_userdata('count_time');
            }
        } else {
            $session_count_time = $this->session->userdata('count_time');
            if(!isset($session_count_time)) {
                $this->session->set_userdata('count_time', time());
                $taskid = $this->input->post('taskid');
                $staffid = $this->session->userdata('user_id');

                $task_timer=$this->db->get_where('sma_taskstimers', array('task_id'=>$taskid))->row();
                if(empty($task_timer)) {
                    $data = ['task_id' => $taskid, 'timer_status' => 1, 'staff_id' => $staffid];
                    $this->db->insert('sma_taskstimers', $data);
                } else {
                    $data = ['timer_status' => 1];
                    $this->db->where('task_id', $taskid)->update('sma_taskstimers', $data);
                }
                
                 
            } else {

                $logged_time = time() - $this->session->userdata('count_time');
                $this->session->set_userdata('logged_time', $logged_time);

                $hours = floor($logged_time / 3600);
                $minutes = floor(($logged_time / 60) % 60);
                $seconds = $logged_time % 60;

                $hours = ($hours < 10) ? ('0'.$hours) : $hours;
                $minutes = ($minutes < 10) ? ('0'.$minutes) : $minutes;
                $seconds = ($seconds < 10) ? ('0'.$seconds) : $seconds;
                $show_time = $hours . ' : ' . $minutes . ' : ' . $seconds;

                $taskid = $this->input->post('taskid');
                $task_timer=$this->db->get_where('sma_taskstimers', array('task_id'=>$taskid))->row();
// print_r($task_timer->timer_status);
                if($task_timer->timer_status == 1) {
                echo $show_time;
                }
            }

        }
    }

    public function select_checklist_item()
    {
        $taskid = $this->input->get('taskid');
        $result = $this->tasks_model->get_checklist_items($taskid);
        // print_r($taskid);die();
        $output = '';
        $output .= '  
        <div class="table">  
           <table class="table" width="100%">  
           
               ';  
        foreach($result as $row)
          {  
               $output .= '  
                    <tr>  
                         <input type="hidden" data-taskid="'.$taskid.'" name="taskid" class="taskid">
                         <td width="10%">
                         <input data-checklist_id="'.$row["id"].'" class="checklist_checkbox" type="checkbox" '
                         . ($row["finished"] == 1 ? 'checked' : '') .'>
                         </td>  
                         <td width="80%" class="description" data-checklist_id="'.$row["id"].'" contenteditable>'.$row["description"].'</td>  
                         
                         <td width="10%"><a href="#" onclick="remove_task_checklist(' . $row['id'] . '); return false;"><i class="fa fa-times text-danger"></i></span></a>  </td>
                    </tr>  
               ';  
          }
        // Delete button  
        //   <td><button type="button" name="delete_btn" data-checklist_id="'.$row["id"].'" class="btn btn-xs fa fa-remove btn_delete"></button></td>
        $output .= '  
           <tr>  
                <td width="10%"></td>  
                <td width="80%" id="description" contenteditable></td>  
                <td width="10%"><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>  
                
           </tr>  
      ';  

        $output .= '</table>  
              </div>';  
              
         echo $output;    
    }

    public function add_checklist_item()
    {
        
        // if ($this->input->is_ajax_request()) {
            // print_r($this->input->post());die;
            if ($this->input->post()) {
                $taskid = $_POST['taskid'];
                $checklist_des = $_POST['description'];

                if(trim($checklist_des) != '') {
                    $insert = $this->tasks_model->add_checklist_item($taskid, $checklist_des);
                }
                if($insert) {
                    //echo 'Data Inserted';
                    return true; 
                } 
            }
        // }
    }

    public function edit_checklist_item()
    {
        
        if ($this->input->is_ajax_request()) {
            
            if ($this->input->post()) {
                $checklist_id = $_POST['checklist_id'];
                $checklist_des = $_POST['description'];

                if(trim($checklist_des) != '') {
                    $insert = $this->tasks_model->edit_checklist_item($checklist_id, $checklist_des);
                }
                if($insert) {
                    echo 'Data Updated'; 
                } 
            }
        }
    }

    public function delete_checklist_item()
    {

        if($this->input->post()) {
            $checklist_id = $_POST['checklist_id'];
            $delete = $this->tasks_model->delete_checklist_item($checklist_id);
        }
        if($delete) {
            // echo 'Data deleted';
            return true;
        }

    }

    public function finish_checklist_item()
    {
        if($this->input->post()) {
            $checklist_id = $_POST['checklist_id'];
            $finished = $_POST['finished'];
            $q = $this->tasks_model->finish_checklist_item($checklist_id, $finished);
        }
    }

    public function add_task() 
    {
    	$this->form_validation->set_rules('name', lang('name'), 'required');
        $name = htmlspecialchars($this->input->post('name'));
    	if ($this->form_validation->run() == true) {
            $data = [
                'name'            => $this->input->post('name'),
                'startdate'            => $this->input->post('startdate'),
                'duedate'            => $this->input->post('duedate'),
                'priority'            => $this->input->post('priority'),
                'repeat_every'            => $this->input->post('repeat_every'),
                'cycles'            => $this->input->post('cycles') ? $this->input->post('cycles') : 0,
                'tags'            => $this->input->post('tags'),
                'description'            => $this->input->post('description'),
            ];
            // print_r($data);die;
        } elseif ($this->input->post('add_task')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('tasks/add_task');
        }

    	if ($this->form_validation->run() == true && $this->tasks_model->addTask($data)) {
    		// print_r($this->input->post()); exit();
            $this->session->set_flashdata('message', lang('task_added'));
            admin_redirect('tasks/list_tasks');
        }
        else {
	    	$this->data['modal_js']   = $this->site->modal_js();
	        $this->load->view($this->theme . 'tasks/add_task', $this->data);
	    }
    }

    public function changeTaskStatus()
    {
    	$id = $this->input->get('id');
    	$status_id = $this->input->get('status_id');
    	$user_id = $this->session->userdata('user_id');

    	$update = $this->tasks_model->updateTaskStatus($id, $status_id, $user_id);
    	if($update) {
    		$this->session->set_flashdata('message', lang('task_status_updated'));
            admin_redirect('tasks/list_tasks');
        }

    }

    public function changeTaskPriority()
    {
    	$id = $this->input->get('id');
    	$priority_id = $this->input->get('priority_id');
    	$user_id = $this->session->userdata('user_id');

    	$update = $this->tasks_model->updateTaskPriority($id, $priority_id, $user_id);
    	if($update) {
    		$this->session->set_flashdata('message', lang('task_priority_updated'));
            admin_redirect('tasks/list_tasks');
        }

    }

    public function update_startdate()
    {
        
        if($_POST) {
            $startdate2 = str_replace('/', '-', $_POST['startdate']);
            $startdate = date('Y-m-d',strtotime($startdate2));
            $taskid = $_POST['taskid'];
            $update = $this->tasks_model->update_startdate($startdate, $taskid);
        }
        if($update) {
            return true;
        }
    }

    public function update_duedate()
    {
        if($_POST) {
            $duedate = date('Y-m-d',strtotime($_POST['duedate']));
            $taskid = $_POST['taskid'];
            $update = $this->tasks_model->update_duedate($duedate, $taskid);
        }
        if($update) {
            return true;
        }
    }

    public function update_description()
    {
        if($this->input->post()) {
            $taskid = $this->input->post('taskid');
            $description = $this->input->post('description');

            $update = $this->tasks_model->update_description($taskid, $description);

            if($update) {
                echo $description;
            }
        } 
    }

    public function add_task_comment()
    {
        // print_r($this->input->post());print_r($_FILES); 
        if($this->input->post()) {
            $taskid = $this->input->post('taskid');
            $comment = $this->input->post('comment');
            $user_id = $this->session->userdata('user_id');

            $file_name = '';
            if(isset($_FILES["userfile"]["name"])) {
                $config['upload_path']          = 'assets/uploads/task_files/';
                $config['allowed_types']        = 'gif|jpg|png|docx|php|mp4|avi|mov|mpeg|OGG|WMV|WEBM';
                // $config['max_size']             = 2000;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('userfile')) {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                       
                }
            }

            $insert = $this->tasks_model->add_task_comment($taskid, $comment, $user_id, $file_name);

            if($insert) {
                return true;
            }
        } 
    }

    public function fetch_comments()
    {
        $taskid = $this->input->get('taskid');
        $task_comments = 
                 $this->db
                 ->select('task_comments.*, users.first_name, users.last_name')
                 ->from('task_comments')
                 ->join('users', 'users.id = task_comments.staffid', 'left')
                 // ->get_where('task_comments', array('taskid' => $task_id))
                 ->where('task_comments.taskid', $taskid)
                 ->group_by('task_comments.id')
                 ->order_by('task_comments.id desc')
                 ->get()
                 ->result_array();
            $comments = '';
                  $len = count($task_comments);
                  $i = 0;
                  foreach ($task_comments as $comment) {
                    $comments .= '<div id="comment_'.$comment['id'].'" data-commentid="' . $comment['id'] . '" data-task-attachment-id="'.$comment['file_id'].'" class="tc-content task-comment'.(strtotime($comment['dateadded']) >= strtotime('-16 hours') ? ' highlight-bg' : '').'">';
                    
                    if($comment['staffid'] != 0){
                     $comments .= '<a href="' . $comment['first_name'].' '.$comment['last_name']. '" target="_blank">'
                      
                   . '</a>';
                  } 

                  if ($comment['staffid'] == $this->session->userdata('user_id') ) {
                     $comment_added = strtotime($comment['dateadded']);
                     $minus_1_hour = strtotime('-1 hours');
                    //  if(is_admin()){
                       $comments .= '<span class="pull-right"><a href="#" onclick="remove_task_comment(' . $comment['id'] . '); return false;"><i class="fa fa-times text-danger"></i></span></a>';
                       $comments .= '<span class="pull-right mright5"><a href="#" onclick="edit_task_comment(' . $comment['id'] . '); return false;"><i class="fa fa-pencil-square-o"></i></span></a>';
                    // }
                  }

                  $comments .= '<div class="media-body comment-wrapper">';
                  $comments .= '<div class="mleft40">';

                  if($comment['staffid'] != 0){
                   $comments .= $comment['first_name'].' '.$comment['last_name'] . '<br />';
                  } 
                  $comments .= '<small class="border-bottom">' . date('d M, Y h:i:sa',strtotime($comment['dateadded'])) . '</small>';

                  $comments .= '<div data-edit-comment="'.$comment['id'].'" class="edit-task-comment" style="display:none"><textarea rows="5" id="task_comment_'.$comment['id'].'" class="ays-ignore form-control">'.str_replace('[task_attachment]', '', $comment['content']).'</textarea>
                  <div class="clearfix mtop20"></div>
                  <button type="button" class="btn btn-info pull-right" onclick="save_edited_comment('.$comment['id'].','.$taskid.')">'.lang('submit').'</button>
                  <button type="button" class="btn btn-default pull-right mright5" onclick="cancel_edit_comment('.$comment['id'].')">'.lang('cancel').'</button>
                  </div>';
                  if($comment['file_id'] != 0){
                  $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$attachments_data[$comment['file_id']],$comment['content']);
                  // Replace lightbox to prevent loading the image twice
                  $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-attachment-comment-'.$comment['id'].'"',$comment['content']);
                  } 
                  /*else if(count($comment['attachments']) > 0 && isset($comments_attachments[$comment['id']])) {
                   $comment_attachments_html = '';
                   foreach($comments_attachments[$comment['id']] as $comment_attachment) {
                       $comment_attachments_html .= trim($comment_attachment);
                   }
                   $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$comment_attachments_html,$comment['content']);
                   // Replace lightbox to prevent loading the image twice
                   $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-comment-files-'.$comment['id'].'"',$comment['content']);
                   $comment['content'] .='<div class="clearfix"></div>';
                   $comment['content'] .='<div class="text-center download-all">
                   <hr class="hr-10" />
                   <a href="'.admin_url('tasks/download_files/'.$task->id.'/'.$comment['id']).'" class="bold">'.lang('download_all').' (.zip)
                   </a>
                   </div>';
                  }*/
                  $comments .= '<div id="comment_view' . $comment['id'] .'" class="comment-content mtop10">'.($comment['content']) . '</div>';
                  $comments .= '<div id="" class=""> <a target="_blank" href="'.base_url().'assets/uploads/task_files/'.($comment['file_name']) . '">' .($comment['file_name']) . '</a></div>';
                  $comments .= '</div>';
                  if ($i >= 0 && $i != $len - 1) {
                     $comments .= '<hr class="task-info-separator" />';
                  }
                  $comments .= '</div>';
                  $comments .= '</div>';
                  $i++;
                  }
                  echo $comments;
    }

    public function remove_task_comment()
    {
        // print_r($this->input->post());exit();
        if($this->input->post()) {

            $comment_id = $this->input->post('comment_id');

            $comment = $this->db->get_where('task_comments', array('id' => $comment_id))->row();

            if(!empty($comment->file_name)) {
                unlink('assets/uploads/task_files/'.$comment->file_name);
            }

            $delete = $this->tasks_model->remove_task_comment($comment_id);

            if($delete) {
                return true;
            }
        }
    }

    public function update_task_comment()
    {
        if($this->input->post()) {
            // print_r($this->input->post());exit();
            $comment_id = $this->input->post('comment_id');
            $taskid = $this->input->post('taskid');
            $comment_content = $this->input->post('comment_content');

            $update = $this->tasks_model->update_task_comment($comment_id, $taskid, $comment_content);
            if($update) {
                return true;
            } 
        }
    }

    public function update_tags()
    {
        if($this->input->post()) {
            $tags = $this->input->post('tags');
            $taskid = $this->input->post('taskid');

            $update = $this->tasks_model->update_tags($tags, $taskid);
            if($update) {
                return true;
            } 
        }
    }

    public function add_task_reminder()
    {

        if($this->input->post()) {
            // print_r($this->input->post());exit();
            $reminder_date = $this->input->post('reminder_date');
            $reminder_date = date('Y-m-d H:i:s', strtotime($reminder_date));

            $taskid = $this->input->post('taskid');
            $reminder_staff = $this->input->post('reminder_staff');
            $reminder_description = $this->input->post('reminder_description');

            $creator = $this->session->userdata('user_id');

            $insert = $this->tasks_model->add_task_reminder($taskid, $reminder_staff, $reminder_date, $reminder_description, $creator);
            if($insert) {
                return true;
            } 
        }
    }

    public function fetch_reminders()
    {
        $taskid = $this->input->post('taskid');
        $reminders = $this->db
                     ->select('task_reminders.*, users.first_name, users.last_name')
                     ->from('task_reminders')
                     ->join('users', 'users.id = task_reminders.staff', 'left')
                     ->where(array('rel_id' => $taskid, 'rel_type' => 'task'))
                     ->get()
                     ->result_array();

        $output = '';
        $output .= '<ul class="mtop10">';
        foreach($reminders as $rKey => $reminder) {
              
            $output .= '<li class="" data-id="'.$reminder['id'].'">
               <div class="mbot15">
                  <div>
                     <p class="bold">';
                        $output .= lang('reminder_for'). ' ' . $reminder['first_name'] . ' ' . $reminder['last_name'] . ' ' . $reminder['date'];
                        if ($reminder['creator'] == $this->session->userdata('user_id')) {
                        $output .=' <a href="#" onclick="remove_task_reminder(' . $reminder['id'] . '); return false;" class="text-danger delete-reminder"> <i class="fa fa-remove"></i></a>';
                        }
                     $output .='</p>';
                        if(!empty($reminder['description'])) {
                           $output .= $reminder['description'];
                        } else {
                           '<p class="text-muted no-mbot">'.('no_description_provided').'</p>';
                        }
                  $output .='</div>';
                  if(count($reminders) -1 != $rKey) { 
                  $output .='<hr class="hr-10" />';
                  } 
               $output .= '</div>
                </li>';
             } 
             $output .= 
                '</ul>';
             echo $output;
    }

    public function remove_task_reminder()
    {
        // print_r($this->input->post());exit();
        if($this->input->post()) {

            $comment_id = $this->input->post('reminder_id');

            $delete = $this->tasks_model->remove_task_reminder($comment_id);

            if($delete) {
                return true;
            }
        }
    }

    public function tags_list()
    {
        $tags = $this->db->get('tags')->result();

        // $output = '';
        // $output .= '<select class="select2">';
        // foreach($tags as $tag) {
        //     $output .= '<option>' . $tag->name . '</option>';
        // }
        // $output .= '</select>';

        // echo $output;

        $tags_list = array();
        foreach($tags as $tag) {
            $tags_list[] = $tag->name;
        }

        echo json_encode($tags_list);
    }

}
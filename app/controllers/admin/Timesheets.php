<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Timesheets extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('timesheet_model');
        $this->load->admin_model('employees_model');
        $this->load->library('form_validation');

    }

     public function officeShifts()
	{
     	$this->load->library('datatables');

        $this->datatables
			->select('office_shift_id as id, shift_name, CONCAT_WS(" to ",monday_in_time,monday_out_time), CONCAT_WS(" to ",tuesday_in_time,tuesday_out_time), CONCAT_WS(" to ",wednesday_in_time,wednesday_out_time), CONCAT_WS(" to ",thursday_in_time,thursday_out_time), CONCAT_WS(" to ",friday_in_time,friday_out_time), CONCAT_WS(" to ",saturday_in_time,saturday_out_time), CONCAT_WS(" to ",sunday_in_time,sunday_out_time),')
			->from('office_shift')
			//->join('warehouses', 'warehouses.id = departments.location_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('timesheets/edit_office_shift/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_office_shift') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_office_shift') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('timesheets/delete_office_shift/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
	}

	public function delete_office_shift($id='')
	{
		if($this->timesheet_model->delete_office_shift($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('office_shift_deleted')]);
     	}
	}


	public function add_office_shift()
	{
		// print_r($this->input->post());die;
		if($this->input->post()) {
			
			$data = [
				'shift_name' => $this->input->post('shift_name'),
				'secondary_name' => $this->input->post('secondary_name'),
				'monday_in_time' => $this->input->post('monday_in_time'),
				'monday_out_time' => $this->input->post('monday_out_time'),
				'tuesday_in_time' => $this->input->post('tuesday_in_time'),
				'tuesday_out_time' => $this->input->post('tuesday_out_time'),
				'wednesday_in_time' => $this->input->post('wednesday_in_time'),
				'wednesday_out_time' => $this->input->post('wednesday_out_time'),
				'thursday_in_time' => $this->input->post('thursday_in_time'),
				'thursday_out_time' => $this->input->post('thursday_out_time'),
				'friday_in_time' => $this->input->post('friday_in_time'),
				'friday_out_time' => $this->input->post('friday_out_time'),
				'saturday_in_time' => $this->input->post('saturday_in_time'),
				'saturday_out_time' => $this->input->post('saturday_out_time'),
				'sunday_in_time' => $this->input->post('sunday_in_time'),
				'sunday_out_time' => $this->input->post('sunday_out_time'),
				'created_at' => date('Y-m-d'),
				
			];

			$insert = $this->timesheet_model->add_office_shift($data);
			if($insert) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function edit_office_shift($id='')
	{
		$this->data['office_shift'] = $this->db->get_where('office_shift', array('office_shift_id' => $id))->row();
		$this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'organizations/edit_office_shift', $this->data);
	}

	public function editOfficeShift()
     {
     	
     	if($this->input->post()) {
     		$office_shift_id = $this->input->post('office_shift_id');
			
			$data = [
				'shift_name' => $this->input->post('shift_name'),
				'secondary_name' => $this->input->post('secondary_name'),
				'monday_in_time' => $this->input->post('monday_in_time'),
				'monday_out_time' => $this->input->post('monday_out_time'),
				'tuesday_in_time' => $this->input->post('tuesday_in_time'),
				'tuesday_out_time' => $this->input->post('tuesday_out_time'),
				'wednesday_in_time' => $this->input->post('wednesday_in_time'),
				'wednesday_out_time' => $this->input->post('wednesday_out_time'),
				'thursday_in_time' => $this->input->post('thursday_in_time'),
				'thursday_out_time' => $this->input->post('thursday_out_time'),
				'friday_in_time' => $this->input->post('friday_in_time'),
				'friday_out_time' => $this->input->post('friday_out_time'),
				'saturday_in_time' => $this->input->post('saturday_in_time'),
				'saturday_out_time' => $this->input->post('saturday_out_time'),
				'sunday_in_time' => $this->input->post('sunday_in_time'),
				'sunday_out_time' => $this->input->post('sunday_out_time'),
				
			];

			$update = $this->timesheet_model->editOfficeShift($data, $office_shift_id);
			if($update) {
				return true;
			} else {
				return false;
			}
		}
     }

     public function leaves()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('timesheets/leaves'), 'page' => lang('leaves')], ['link' => '#', 'page' => lang('leaves')]];
        $meta = ['page_title' => lang('leaves'), 'bc' => $bc];
		$this->page_construct('timesheets/leaves_list', $meta, $this->data);
     }
     public function getAllLeaves()
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('leave_id as id, leave_type.type_name, departments.department_name, CONCAT_WS(" ", first_name, last_name), CONCAT_WS(" to ", from_date, to_date), applied_on')
			->from('leave_applications')
			->join('leave_type', 'leave_type.leave_type_id = leave_applications.leave_type_id')
			->join('employees', 'employees.user_id = leave_applications.employee_id')
			->join('departments', 'departments.department_id = leave_applications.department_id')
			->group_by('leave_applications.leave_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('timesheets/detail/$1') . "' class='tip' title='" . lang('view_task') . "'><i class=\"fa fa-file-text\"></i></a> <a href='" . admin_url('timesheets/edit_leave_application/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_leave_application') . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang('delete_leave_application') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('timesheets/delete_leave_application/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

	 public function getAllLeavesOfEmp($employee_id)
     {

     	$this->load->library('datatables');
        $this->datatables
			->select('leave_id as id, leave_type.type_name, departments.department_name,  CONCAT_WS(" to ", from_date, to_date), applied_on')
			->from('leave_applications')
			->join('leave_type', 'leave_type.leave_type_id = leave_applications.leave_type_id')
			->join('employees', 'employees.user_id = leave_applications.employee_id')
			->join('departments', 'departments.department_id = leave_applications.department_id')
			->where('leave_applications.employee_id', $employee_id)
			->group_by('leave_applications.leave_id')
			->add_column('Actions', "<div class=\"text-center\"><a href='" . admin_url('timesheets/detail/$1') . "' class='tip' target='_blank' title='" . lang('view_task') . "'><i class=\"fa fa-file-text\"></i></a></div>", 'id')
			;

     	echo $this->datatables->generate();
     }

     public function delete_leave_application($id='')
     {
     	if($this->timesheet_model->delete_leave_application($id)) {
     		$this->sma->send_json(['error' => 0, 'msg' => lang('leave_application_deleted')]);
     	}
     }

     public function edit_leave_application($id='')
     {
     	$this->data['leave_application'] = $this->db->get_where('leave_applications', array('leave_id' => $id))->row();
		$this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'timesheets/edit_leave_application', $this->data);
     }

     public function editLeaveApplication()
     {
     	if($this->input->post()) {
     		$leave_id = $this->input->post('leave_id');
			
			$data = [
				'reason' => $this->input->post('reason'),
				'remarks' => $this->input->post('remarks'),
			
			];

			$update = $this->timesheet_model->editLeaveApplication($data, $leave_id);
			if($update) {
				return true;
			} else {
				return false;
			}
		}
     }

     public function add_leave_application()
     {
     	$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');

     	if ($this->form_validation->run() == true) {

     		$emp_id = $this->input->post('employee_id');
     		$leave_type_id = $this->input->post('leave_type_id');
			
			 //Calculate total remaining leaves for an employee 
     		$total_leave_taken = $this->timesheet_model->count_total_leaves($leave_type_id,$emp_id);
     		$leave_type = $this->db->get_where('leave_type', array('leave_type_id' => $leave_type_id))->row();
     		$remaining_leave_total = $leave_type->days_per_year - $total_leave_taken;
     		$from_date = strtotime($this->input->post('from_date'));
	        $to_date = strtotime($this->input->post('to_date'));
	        $datediff = $to_date - $from_date;
	        $leave_days = round($datediff / (60 * 60 * 24)) + 1; // +1 
	        $is_half_day = $this->input->post('is_half_day');
	        if(isset($is_half_day)) {
	        	$leave_days = ($leave_days / 2);
	        }
	        // print_r($remaining_leave_total); print_r($leave_days); die;
     		
     		if($remaining_leave_total >= $leave_days) {
	     		$from_date = strtotime($this->input->post('from_date'));
		        $to_date = strtotime($this->input->post('to_date'));
		        $datediff = $to_date - $from_date;
		        $leave_days = round($datediff / (60 * 60 * 24)) + 1; // +1 
		        
		        $is_half_day = $this->input->post('is_half_day');
		        if(isset($is_half_day)) {
		        	$leave_days = $leave_days / 2;
		        }
		        if(isset($is_half_day)) {
		        	$half_day = 1;
		        }
		        $emp_dept = $this->db->get_where('employees', array('user_id'=>$emp_id))->row();

		        if(isset($_FILES["leave_attachment"]["name"])) {
	                $config['upload_path']          = 'assets/uploads/leave_documents/';
	                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|txt|doc|docx|xls|xlsx';
	                $config['max_size']             = 5000;

	                $this->load->library('upload', $config);
	                if ($this->upload->do_upload('leave_attachment')) {
	                        $data = array('upload_data' => $this->upload->data());
	                        $file_name = $data['upload_data']['file_name'];
	                       
	                }
            	}
		        
	     		$data = array(
	     			'employee_id' => $this->input->post('employee_id'),
	     			'leave_type_id' => $this->input->post('leave_type_id'),
	     			'department_id' => $emp_dept->department_id,
	     			'from_date' => $this->input->post('from_date'),
	     			'to_date' => $this->input->post('to_date'),
	     			'leave_days' => $leave_days,
	     			'is_half_day' => $half_day,
	     			'reason' => $this->input->post('reason'),
	     			'remarks' => $this->input->post('remarks'),
	     			'is_half_day' => $this->input->post('is_half_day'),
	     			'leave_attachment' => $file_name,
	     			'applied_on' => date('Y-m-d H:i:s'),
	     		);
	     		if($this->timesheet_model->addLeaveApplication($data)) {
	     			$this->session->set_flashdata('message', lang('leave_application_added'));
	         		admin_redirect('timesheets/leaves');
	     		}
     		} else {
     			$this->session->set_flashdata('error', lang('days_are_more_than_remaining_leave'));
            	admin_redirect('timesheets/add_leave_application');
     		}
     	} elseif ($this->input->post('add_leave_application')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('timesheets/add_leave_application');
        }

        // if ($this->form_validation->run() == true && $this->timesheet_model->addLeaveApplication($data)) {
        // 	 $this->session->set_flashdata('message', lang('leave_application_added'));
	       //   admin_redirect('timesheets/leaves');
        // }

        else {
	     	$this->data['all_employees'] = $this->employees_model->all_employees();
	     	$this->data['leave_categories'] = $this->employees_model->leave_categories();

	     	$bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('employees'), 'page' => lang('employees')], ['link' => '#', 'page' => lang('employees')]];
	        	$meta = ['page_title' => lang('employees'), 'bc' => $bc];
		    	$this->page_construct('timesheets/add_leave_applications', $meta, $this->data);
	    }
     }

     public function getEmpLeaves()
     {
     	$emp_id = $this->input->post('empid');
     	$emp_leave_cat = $this->db->select('leave_categories')->get_where('employees', array('user_id' => $emp_id))->row();
     	
     	$emp_leaves = explode(',', $emp_leave_cat->leave_categories);
     	$output = '';
     	$output .= '<select name="leave_type_id" class="form-control">';
     	$output .= '<option value="">Select</option>';
     	foreach($emp_leaves as $leave_type_id) {

			//Calculate total remaining leaves for an employee where leave status = 2 (Approved) 
     		$total_leave_taken = $this->timesheet_model->count_total_leaves($leave_type_id,$emp_id);
     		$leave_type = $this->db->get_where('leave_type', array('leave_type_id' => $leave_type_id))->row();
     		$remaining_leave_total = $leave_type->days_per_year - $total_leave_taken;

     		if($remaining_leave_total == 0.5) {
     			$remaining_leave_total = 'Half';
     		} else {
     			$remaining_leave_total = $remaining_leave_total;
     		}

     		$output .= '<option value="'.$leave_type->leave_type_id.'">'. $leave_type->type_name . ' (' . $remaining_leave_total. ' Days Remaining)' .'</option> ';

     	}
     	$output .= '</select>';
     	echo $output;
     }

     public function detail($id='')
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

     	$leave_application = $this->timesheet_model->get_leave_application($id);

     	$emp_id = $leave_application->employee_id;

     	$emp_info = $this->db->select('first_name, last_name')->get_where('employees', array('user_id'=>$leave_application->employee_id))->row();
     	$dept_info = $this->db->select('department_name')->get_where('departments', array('department_id'=>$leave_application->department_id))->row();
     	$leave_type = $this->db->select('type_name')->get_where('leave_type', array('leave_type_id'=>$leave_application->leave_type_id))->row();



		$this->data['emp_name'] = $emp_info->first_name.' '.$emp_info->last_name;
		$this->data['department_name'] = $dept_info->department_name;
		$this->data['leave_type_name'] = $leave_type->type_name;
     	$this->data['leave_application'] = $leave_application;
     	
     	$sql = "SELECT * FROM sma_leave_applications ORDER BY leave_id DESC LIMIT 1";

     	$last_leave = $this->db->query($sql)->row();
     	$last_leave_type = $this->db->select('type_name')->get_where('leave_type', array('leave_type_id'=>$last_leave->leave_type_id))->row();
     	$this->data['last_leave'] = $last_leave;
     	$this->data['last_leave_type_name'] = $last_leave_type->type_name;

     	// ____Leave Statistics
     	$emp_leave_cat = $this->db->select('leave_categories')->get_where('employees', array('user_id' => $leave_application->employee_id))->row();
     	$emp_leaves = explode(',', $emp_leave_cat->leave_categories);
     	
     	$leave_statistics = array();
     	foreach($emp_leaves as $leave_type_id) {
     		$total_leave_taken = $this->timesheet_model->count_total_leaves($leave_type_id,$emp_id);
     		$leave_type = $this->db->get_where('leave_type', array('leave_type_id' => $leave_type_id))->row();
     		$remaining_leave_total = $leave_type->days_per_year - $total_leave_taken;

     		$leave_statistics[] = [
     			'leave_name' => $leave_type->type_name,
     			'total_leave_days' => $leave_type->days_per_year,
     			'total_leave_taken' => $total_leave_taken,

     		];
     	}

     	$this->data['leave_statistics'] = $leave_statistics;

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('timesheets'), 'page' => lang('timesheets')], ['link' => '#', 'page' => lang('leave_applications')]];
        $meta = ['page_title' => lang('leave_applications'), 'bc' => $bc];
		$this->page_construct('timesheets/detail', $meta, $this->data);
     }

     public function change_leave_status()
     {
     	
     	$leave_id = $this->input->post('leave_id');
     	$data = [
     		'status' => $this->input->post('status'),
     		'remarks' => $this->input->post('remarks')
     	];
     	$insert = $this->timesheet_model->change_leave_status($data, $leave_id);
     	if($insert) {
				return true;
			} else {
				return false;
			}
     }

	 public function attendance()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('timesheets/attendance'), 'page' => lang('attendance')], ['link' => '#', 'page' => lang('attendance')]];
        $meta = ['page_title' => lang('attendance'), 'bc' => $bc];
		$this->page_construct('timesheets/attendance_list', $meta, $this->data);
     }

	 public function attendance_list()
	 {

		$iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
		$dateSearch=$this->input->post('dateSearch');
		$dateSearch=date('Y-m-d', strtotime($dateSearch));
// print_r($dateSearch); die;
		$user_id = $this->session->userdata('user_id');
		$user_info = $this->timesheet_model->read_user_info($user_id);

		$attendance_date = $this->input->get("attendance_date");
		$employee = $this->timesheet_model->all_employees($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $dateSearch);
		$data = array();
		// print_r(sizeof($employee));die;
		foreach($employee as $r) {

			// user full name
			$full_name = $r->first_name.' '.$r->last_name;	
			// get office shift for employee
			$get_day = strtotime($attendance_date);
			$day = date('l', $get_day);

			$office_shift = $this->timesheet_model->read_office_shift_information($r->office_shift_id);
			// 
			if(!is_null($office_shift)){
				// get clock in/clock out of each employee
				if($day == 'Monday') {
					if($office_shift[0]->monday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->monday_in_time;
						$out_time = $office_shift[0]->monday_out_time;
					}
				} else if($day == 'Tuesday') {
					if($office_shift[0]->tuesday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->tuesday_in_time;
						$out_time = $office_shift[0]->tuesday_out_time;
					}
				} else if($day == 'Wednesday') {
					if($office_shift[0]->wednesday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->wednesday_in_time;
						$out_time = $office_shift[0]->wednesday_out_time;
					}
				} else if($day == 'Thursday') {
					if($office_shift[0]->thursday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->thursday_in_time;
						$out_time = $office_shift[0]->thursday_out_time;
					}
				} else if($day == 'Friday') {
					if($office_shift[0]->friday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->friday_in_time;
						$out_time = $office_shift[0]->friday_out_time;
					}
				} else if($day == 'Saturday') {
					if($office_shift[0]->saturday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->saturday_in_time;
						$out_time = $office_shift[0]->saturday_out_time;
					}
				} else if($day == 'Sunday') {
					if($office_shift[0]->sunday_in_time==''){
						$in_time = '00:00:00';
						$out_time = '00:00:00';
					} else {
						$in_time = $office_shift[0]->sunday_in_time;
						$out_time = $office_shift[0]->sunday_out_time;
					}
				}

				// check if clock-in for date
				$attendance_status = '';
				$check = $this->timesheet_model->attendance_first_in_check($r->user_id,$attendance_date);
				
				if($check->num_rows() > 0){
					// check clock in time
					$attendance = $this->timesheet_model->attendance_first_in($r->user_id,$attendance_date);
					
					// clock in
					$clock_in = new DateTime($attendance[0]->clock_in);
					$clock_in2 = $clock_in->format('h:i a');
					
					$clkInIp = $clock_in2;
					
					$office_time =  new DateTime($in_time.' '.$attendance_date);
					//time diff > total time late
					$office_time_new = strtotime($in_time.' '.$attendance_date);
					$clock_in_time_new = strtotime($attendance[0]->clock_in);
					if($clock_in_time_new <= $office_time_new) {
						$total_time_l = '00:00';
					} else {
						$interval_late = $clock_in->diff($office_time);
						$hours_l   = $interval_late->format('%h');
						$minutes_l = $interval_late->format('%i');			
						$total_time_l = $hours_l ."h ".$minutes_l."m";
					}
					
					// total hours work/ed
					$total_hrs = $this->timesheet_model->total_hours_worked_attendance($r->user_id,$attendance_date);
					$hrs_old_int1 = '';
					$Total = '';
					$Trest = '';
					$total_time_rs = '';
					$hrs_old_int_res1 = '';
					foreach ($total_hrs->result() as $hour_work){		
						// total work			
						$timee = $hour_work->total_work.':00';
						$str_time =$timee;
			
						$str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
						
						sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
						
						$hrs_old_seconds = $hours * 3600 + $minutes * 60 + $seconds;
						
						$hrs_old_int1 = $hrs_old_seconds;
						
						$Total = gmdate("H:i", $hrs_old_int1);	
					}
					if($Total=='') {
						$total_work = '00:00';
					} else {
						$total_work = $Total;
					}
					
					// total rest > 
					$total_rest = $this->timesheet_model->total_rest_attendance($r->user_id,$attendance_date);
					foreach ($total_rest->result() as $rest){			
						// total rest
						$str_time_rs = $rest->total_rest.':00';
						//$str_time_rs =$timee_rs;
			
						$str_time_rs = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time_rs);
						
						sscanf($str_time_rs, "%d:%d:%d", $hours_rs, $minutes_rs, $seconds_rs);
						
						$hrs_old_seconds_rs = $hours_rs * 3600 + $minutes_rs * 60 + $seconds_rs;
						
						$hrs_old_int_res1 = $hrs_old_seconds_rs;
						
						$total_time_rs = gmdate("H:i", $hrs_old_int_res1);
					}
					
					// check attendance status
					$status = $attendance[0]->attendance_status;
					if($total_time_rs=='') {
						$Trest = '00:00';
					} else {
						$Trest = $total_time_rs;
					}
					
				} else {
					$clock_in2 = '-';
						$total_time_l = '00:00';
						$total_work = '00:00';
						$Trest = '00:00';
						$clkInIp = $clock_in2;
						// get holiday/leave or absent
						/* attendance status */
						// get holiday
						$h_date_chck = $this->timesheet_model->holiday_date_check($attendance_date);
						
						$holiday_arr = array();
						if($h_date_chck->num_rows() == 1){
							$h_date = $this->timesheet_model->holiday_date($attendance_date);
							//print_r($h_date->result());die;
							$begin = new DateTime( $h_date[0]->start_date );
							$end = new DateTime( $h_date[0]->end_date);
							$end = $end->modify( '+1 day' ); 
							
							$interval = new DateInterval('P1D');
							$daterange = new DatePeriod($begin, $interval ,$end);
							
							foreach($daterange as $date){
								$holiday_arr[] =  $date->format("Y-m-d");
							}
						} else {
							$holiday_arr[] = '99-99-99';
						}

						// get leave/employee
						$leave_date_chck = $this->timesheet_model->leave_date_check($r->user_id,$attendance_date);
						$leave_arr = array();
						if($leave_date_chck->num_rows() == 1){
							$leave_date = $this->timesheet_model->leave_date($r->user_id,$attendance_date);
							$begin1 = new DateTime( $leave_date[0]->from_date );
							$end1 = new DateTime( $leave_date[0]->to_date);
							$end1 = $end1->modify( '+1 day' ); 
							
							$interval1 = new DateInterval('P1D');
							$daterange1 = new DatePeriod($begin1, $interval1 ,$end1);
							
							foreach($daterange1 as $date1){
								$leave_arr[] =  $date1->format("Y-m-d");
							}	
						} else {
							$leave_arr[] = '99-99-99';
						}
						// print_r($total_work); die;
						if($office_shift[0]->monday_in_time == '' && $day == 'Monday') {
							$status = lang('absent');	
						} else if($office_shift[0]->tuesday_in_time == '' && $day == 'Tuesday') {
							$status = lang('absent');
						} else if($office_shift[0]->wednesday_in_time == '' && $day == 'Wednesday') {
							$status = lang('absent');
						} else if($office_shift[0]->thursday_in_time == '' && $day == 'Thursday') {
							$status = lang('absent');
						} else if($office_shift[0]->friday_in_time == '' && $day == 'Friday') {
							$status = lang('absent');
						} else if($office_shift[0]->saturday_in_time == '' && $day == 'Saturday') {
							$status = lang('absent');
						} else if($office_shift[0]->sunday_in_time == '' && $day == 'Sunday') {
							$status = lang('absent');
						} else if(in_array($attendance_date,$holiday_arr)) { // holiday
							$status = lang('absent');
						} else if(in_array($attendance_date,$leave_arr)) { // on leave
							$status =lang('absent');
						} 
						else {
							$status = lang('absent');
						}
				}

				// check if clock-out for date
				$check_out = $this->timesheet_model->attendance_first_out_check($r->user_id,$attendance_date);		
				if($check_out->num_rows() == 1){
					/* early time */
					$early_time =  new DateTime($out_time.' '.$attendance_date);
					// check clock in time
					$first_out = $this->timesheet_model->attendance_first_out($r->user_id,$attendance_date);
					// clock out
					$clock_out = new DateTime($first_out[0]->clock_out);
					
					if ($first_out[0]->clock_out!='') {
						$clock_out2 = $clock_out->format('h:i a');
						
						$clkOutIp = $clock_out2;

						// early leaving
						$early_new_time = strtotime($out_time.' '.$attendance_date);
						$clock_out_time_new = strtotime($first_out[0]->clock_out);
					
						if($early_new_time <= $clock_out_time_new) {
							$total_time_e = '00:00';
						} else {			
							$interval_lateo = $clock_out->diff($early_time);
							$hours_e   = $interval_lateo->format('%h');
							$minutes_e = $interval_lateo->format('%i');			
							$total_time_e = $hours_e ."h ".$minutes_e."m";
						}
						
						/* over time */
						$over_time =  new DateTime($out_time.' '.$attendance_date);
						$overtime2 = $over_time->format('h:i a');
						// over time
						$over_time_new = strtotime($out_time.' '.$attendance_date);
						$clock_out_time_new1 = strtotime($first_out[0]->clock_out);
						
						if($clock_out_time_new1 <= $over_time_new) {
							$overtime2 = '00:00';
						} else {			
							$interval_lateov = $clock_out->diff($over_time);
							$hours_ov   = $interval_lateov->format('%h');
							$minutes_ov = $interval_lateov->format('%i');			
							$overtime2 = $hours_ov ."h ".$minutes_ov."m";
						}				
						
					} else {
						$clock_out2 =  '-';
						$total_time_e = '00:00';
						$overtime2 = '00:00';
						$clkOutIp = $clock_out2;
					}
							
				} else {
					$clock_out2 =  '-';
					$total_time_e = '00:00';
					$overtime2 = '00:00';
					$clkOutIp = $clock_out2;
				}	
				//
				if($user_info[0]->user_role_id==1){
					$fclckIn = $clkInIp;
					$fclckOut = $clkOutIp;
				} else {
					$fclckIn = $clock_in2;
					$fclckOut = $clock_out2;
				}
			} else {
				// attendance date
				//$d_date = $this->Xin_model->set_date_format($attendance_date);
				$d_date = date('d-m-Y', strtotime($attendance_date));
				$status = '<a href="javascript:void(0)" class="badge badge-danger">'.lang('office_shift_not_assigned').'</a>';
				$fclckIn = '--';
				$fclckOut = '--';
				$total_time_l = '--';
				$total_time_e = '--';
				$overtime2 = '--';
				$total_work = '--';
				$Trest = '--';
			}
			$d_date = date('d-m-Y', strtotime($attendance_date));
			$data[] = array(
				$r->user_id,
				$full_name,
				$r->employee_id,
				$d_date,
				$status,
				$fclckIn,
				$fclckOut,
				$total_time_l,
				$total_time_e,
				$overtime2,
				$total_work,
				$Trest
			);
		}

		$allEmployees = $this->timesheet_model->total_employees($dateSearch, $sSearch);
		$totalData = sizeof($allEmployees);
		// echo "<pre>"; print_r($allEmployees);die;
		$sOutput = [
			//'sEcho'                => intval($this->ci->input->post('sEcho')),
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
			//'sColumns'             => implode(',', $sColumns),
		];
		echo json_encode($sOutput);
	 }

	 public function update_attendance()
     {
     	$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
		$this->data['all_employees'] = $this->employees_model->all_employees();

        $bc   = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('timesheets/attendance'), 'page' => lang('attendance')], ['link' => '#', 'page' => lang('update_attendance')]];
        $meta = ['page_title' => lang('update_attendance'), 'bc' => $bc];
		$this->page_construct('timesheets/update_attendance', $meta, $this->data);
     } 
	 public function update_attendance_list()
	 {
		$iDisplayLength=$this->input->post('iDisplayLength');
		$iDisplayStart=$this->input->post('iDisplayStart');
		$sSortDir_0=$this->input->post('sSortDir_0');
		$iSortCol_0=$this->input->post('iSortCol_0');
		$sSearch=$this->input->post('sSearch');
		$dateSearch=$this->input->post('dateSearch');
		$dateSearch=date('Y-m-d', strtotime($dateSearch));
		$filter_emp=$this->input->post('empId');
// print_r($dateSearch); die;
		$user_id = $this->session->userdata('user_id');
		$user_info = $this->timesheet_model->read_user_info($user_id);

		$attendance_date = $this->input->get("attendance_date");
		$employee_attendance = $this->timesheet_model->employees_update_attendance($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $filter_emp, $dateSearch);

// print_r($employee_attendance); die;
		$data = [];
		foreach($employee_attendance as $r) 
		{
			$edit = "";
			
			$edit .= "<a href='" . admin_url('timesheets/update_attendance_form/'.$r->time_attendance_id.'?emp_id='.$r->employee_id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_attendance') . "'><i class=\"fa fa-edit\"></i></a> ";
			$edit .= "<a href='#' class='tip po' title='<b>" . lang('delete_attendance') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('timesheets/delete_attendance/'.$r->time_attendance_id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>";
			$data[] = array(
				$r->time_attendance_id,
				$r->employee_id,
				$r->clock_in,
				$r->clock_out,
				$r->total_work,
				$edit
			);
		}
		//$allEmployees = $this->timesheet_model->total_employees($dateSearch, $sSearch);
		$totalData = sizeof($employee_attendance);
		$sOutput = [
			//'sEcho'                => intval($this->ci->input->post('sEcho')),
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
			//'sColumns'             => implode(',', $sColumns),
		];
		echo json_encode($sOutput);
	 }

	public function delete_attendance($id)
	{
		$this->timesheet_model->delete_attendance($id);
		$this->sma->send_json(['error' => 0, 'msg' => lang('attendance_deleted')]);
	}

	public function update_attendance_form($id)
     {
		$this->data['attendance'] = $this->timesheet_model->read_attendance($id);

		$emp_id = $this->input->get('emp_id'); 
		$emp_name = $this->db->where('user_id', $emp_id)->get('employees')->row();
        $this->data['all_employees'] = $this->employees_model->all_employees();
		$this->data['emp_id'] = $emp_id;
		$this->data['emp_name'] = $emp_name->first_name.' '.$emp_name->last_name;

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'timesheets/update_attendance_form', $this->data);
     }

	 public function updateAttendance()
    {
        if($this->input->post()) {
            $time_attendance_id = $this->input->post('time_attendance_id');
            $attendance_date = $this->input->post('attendance_date');
            $attendance_date = date('Y-m-d', strtotime($attendance_date));

			$clock_in = $this->input->post('clock_in');
			$clock_out = $this->input->post('clock_out');
			
			$clock_in2 = $attendance_date.' '.$clock_in.':00';
			$clock_out2 = $attendance_date.' '.$clock_out.':00';
			
			//total work
			$total_work_cin =  new DateTime($clock_in2);
			$total_work_cout =  new DateTime($clock_out2);
			
			$interval_cin = $total_work_cout->diff($total_work_cin);
			$hours_in   = $interval_cin->format('%h');
			$minutes_in = $interval_cin->format('%i');
			$total_work = $hours_in .":".$minutes_in;

            $data = array(
				'employee_id' => $this->input->post('emp_id'),
				'attendance_date' => $attendance_date,
				'clock_in' => $clock_in2,
				'clock_out' => $clock_out2,
				//'time_late' => $clock_in2,
				'total_work' => $total_work,
				//'early_leaving' => $clock_out2,
				//'overtime' => $clock_out2,
				'attendance_status' => 'Present',
				'clock_in_out' => '0'
			);
			$insert = $this->timesheet_model->update_attendance($data, $time_attendance_id);
			if($insert) {
				return true;
                // $this->sma->send_json(['error' => 0, 'msg' => lang('event_edited')]);

			} else {
				return false;
			}
        }
    }
	public function add_attendance_form()
     {
		$emp_id = $this->input->get('emp_id'); 
		$emp_name = $this->db->where('user_id', $emp_id)->get('employees')->row();
        $this->data['all_employees'] = $this->employees_model->all_employees();
		$this->data['emp_id'] = $emp_id;
		$this->data['emp_name'] = $emp_name->first_name.' '.$emp_name->last_name;

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'timesheets/add_attendance_from', $this->data);
     }

	 public function addAttendance()
	 {
		$emp_id = $this->input->post('emp_id');
		$attendance_date = $this->input->post('attendance_date');
        $attendance_date = date('Y-m-d', strtotime($attendance_date));

		$clock_in = $this->input->post('clock_in');
		$clock_out = $this->input->post('clock_out');
		
		$clock_in2 = $attendance_date.' '.$clock_in.':00';
		$clock_out2 = $attendance_date.' '.$clock_out.':00';
		
		//total work
		$total_work_cin =  new DateTime($clock_in2);
		$total_work_cout =  new DateTime($clock_out2);
		
		$interval_cin = $total_work_cout->diff($total_work_cin);
		$hours_in   = $interval_cin->format('%h');
		$minutes_in = $interval_cin->format('%i');
		$total_work = $hours_in .":".$minutes_in;
	
		$data = array(
		'employee_id' => $this->input->post('emp_id'),
		'attendance_date' => $attendance_date,
		'clock_in' => $clock_in2,
		'clock_out' => $clock_out2,
		'time_late' => $clock_in2,
		'total_work' => $total_work,
		'early_leaving' => $clock_out2,
		'overtime' => $clock_out2,
		'attendance_status' => 'Present',
		'clock_in_out' => '0'
		);
		$check_emp_attendance = $this->db->get_where('attendance_time', array('employee_id' => $emp_id, 'attendance_date' => $attendance_date))->row();
		if(empty($check_emp_attendance)) {
			$insert = $this->timesheet_model->add_employee_attendance($data);
		}
		
		if($insert) {
			return true;
		} else {
			return false;
		}
	 }

	 public function add_attendance()
	 {
		$employees = $this->employees_model->all_employees();

		foreach($employees as $value) {

			$date = date('Y-m-d');
			$clock_in = date('Y-m-d H:i:s');
			$clock_out = date('Y-m-d H:i:s',strtotime('+6 hours'));
			$clock_in_obj = new DateTime($clock_in);
			$clock_out_obj = new DateTime($clock_out);
			$total_work_obj = $clock_in_obj->diff($clock_out_obj);
			$hours_total_work   = $total_work_obj->format('%h');
			$minutes_total_work = $total_work_obj->format('%i');			
			$total_work = $hours_total_work .":".$minutes_total_work;
			$data = array(
				'employee_id' => $value->user_id,
				'attendance_date' => $date,
				'clock_in' => $clock_in,
				'clock_out' => $clock_out,
				'total_work' => $total_work
			);
			// print_r($data); die;
			$check_emp_attendance = $this->db->get_where('attendance_time', array('employee_id' => $value->user_id, 'attendance_date' => $date))->row();
			if(empty($check_emp_attendance)) {
				$this->db->insert('attendance_time', $data);
			}
		}

		$this->session->set_flashdata('message', 'Attendances updated!');
		admin_redirect('timesheets/attendance');
		
	 }

	public function add_leave_cal()
	{
		if($this->input->post()) {
		$this->form_validation->set_rules('employee_id', lang('employee_id'), 'required');

		$emp_id = $this->input->post('employee_id');
		$leave_type_id = $this->input->post('leave_type_id');
		$total_leave_taken = $this->timesheet_model->count_total_leaves($leave_type_id,$emp_id);
		$leave_type = $this->db->get_where('leave_type', array('leave_type_id' => $leave_type_id))->row();
		$remaining_leave_total = $leave_type->days_per_year - $total_leave_taken;
		$from_date = strtotime($this->input->post('from_date'));
		$to_date = strtotime($this->input->post('to_date'));
		$datediff = $to_date - $from_date;
		$leave_days = round($datediff / (60 * 60 * 24)) +1;
		$is_half_day = $this->input->post('is_half_day');
		if(isset($is_half_day)) {
			$leave_days = ($leave_days / 2);
		}
	   // print_r($remaining_leave_total); print_r($leave_days); die;
		
		if($remaining_leave_total >= $leave_days) {
			$from_date = strtotime($this->input->post('from_date'));
		   $to_date = strtotime($this->input->post('to_date'));
		   $datediff = $to_date - $from_date;
		   $leave_days = round($datediff / (60 * 60 * 24)) +1;
		   
		   $is_half_day = $this->input->post('is_half_day');
		   if(isset($is_half_day)) {
			   $leave_days = $leave_days / 2;
		   }
		   $half_day = null;
		   if(isset($is_half_day)) {
			   $half_day = 1;
		   }
		   $emp_dept = $this->db->get_where('employees', array('user_id'=>$emp_id))->row();
		   $file_name='';
		   if(isset($_FILES["leave_attachment"]["name"])) {
			   $config['upload_path']          = 'assets/uploads/leave_documents/';
			   $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|txt|doc|docx|xls|xlsx';
			   $config['max_size']             = 5000;

			   $this->load->library('upload', $config);
			   if ($this->upload->do_upload('leave_attachment')) {
					   $data = array('upload_data' => $this->upload->data());
					   $file_name = $data['upload_data']['file_name'];
					  
			   }
		   }
		   
			$data = array(
				'employee_id' => $this->input->post('employee_id'),
				'leave_type_id' => $this->input->post('leave_type_id'),
				'department_id' => $emp_dept->department_id,
				'from_date' => $this->input->post('from_date'),
				'to_date' => $this->input->post('to_date'),
				'leave_days' => $leave_days,
				'is_half_day' => $half_day,
				'reason' => $this->input->post('reason'),
				'remarks' => $this->input->post('remarks'),
				'is_half_day' => $this->input->post('is_half_day'),
				'leave_attachment' => $file_name,
				'applied_on' => date('Y-m-d H:i:s'),
			);
			if($this->timesheet_model->addLeaveApplication($data)) {
				//return true;
				$this->sma->send_json(['success' => 1, 'msg' => lang('leave_added')]);
			}
		} else {
			$this->sma->send_json(['error' => 0, 'msg' => lang('leave_quota_not_enough')]);
			//return false;
		}
		}
	}

	public function delete_holiday($id)
	{
		$this->timesheet_model->delete_holiday($id);
		return true;
	}
	
}
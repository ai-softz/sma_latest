<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Companies extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('companies_api');
    }

    protected function setCompany($company)
    {
        $company->company = !empty($company->company) && $company->company != '-' ? $company->company : null;
        $company->person  = $company->name;
        if ($company->group_name == 'customer') {
            unset($company->id, $company->group_id, $company->group_name, $company->invoice_footer, $company->logo, $company->name);
        } elseif ($company->group_name == 'supplier') {
            unset($company->id, $company->group_id, $company->group_name, $company->invoice_footer, $company->logo, $company->customer_group_id, $company->customer_group_name, $company->deposit_amount, $company->payment_term, $company->price_group_id, $company->price_group_name, $company->award_points, $company->name);
        } elseif ($company->group_name == 'biller') {
            $company->logo = base_url('assets/uploads/logos/' . $company->logo);
            unset($company->id, $company->group_id, $company->group_name, $company->customer_group_id, $company->customer_group_name, $company->deposit_amount, $company->payment_term, $company->price_group_id, $company->price_group_name, $company->award_points, $company->name);
        }
        $company = (array) $company;
        ksort($company);
        return $company;
    }

    public function index_get()
    {
        $name = $this->get('name');

        $filters = [
            'name'     => $name,
            'include'  => $this->get('include') ? explode(',', $this->get('include')) : null,
            'group'    => $this->get('group') ? $this->get('group') : 'customer',
            'start'    => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
            'limit'    => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
            'order_by' => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['company', 'acs'],
        ];

        if ($name === null) {
            if ($companies = $this->companies_api->getCompanies($filters)) {
                $pr_data = [];
                foreach ($companies as $company) {
                    if (!empty($filters['include'])) {
                        foreach ($filters['include'] as $include) {
                            if ($include == 'user') {
                                $company->users = $this->companies_api->getCompanyUser($company->id);
                            }
                        }
                    }

                    $pr_data[] = $this->setCompany($company);
                }

                $data = [
                    'data'  => $pr_data,
                    'limit' => $filters['limit'],
                    'start' => $filters['start'],
                    'total' => $this->companies_api->countCompanies($filters),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No company were found.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            if ($company = $this->companies_api->getCompany($filters)) {
                if (!empty($filters['include'])) {
                    foreach ($filters['include'] as $include) {
                        if ($include == 'user') {
                            $company->users = $this->companies_api->getCompanyUser($company->id);
                        }
                    }
                }

                $company = $this->setCompany($company);
                $this->set_response($company, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'message' => 'Company could not be found for name ' . $name . '.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }


    public function singlecustomer_get()
    {
        // http://localhost/resturant_faysalia/api/v1/companies/singlecustomer?api-key=ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw&mobileno=0506142165&group=customer
        $mobile = $this->get('mobileno');
        $group_name = $this->get('group');
        $customer = $this->companies_api->getSingleCompanies($mobile, $group_name);
            if ($customer) {
            $customer->status=true;
            $this->set_response($customer, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'message' => 'Customer could not be found for mobile ' . $mobile . '.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
        
    }

    public function addcustomer_post()
    {
        // http://localhost/resturant_faysalia/api/v1/companies/singlecustomer?api-key=ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw&mobileno=0506142165&group=customer
        $mobile = $this->post('mobileno');
        $group_name = $this->post('group');
        $customer = $this->companies_api->getSingleCompanies($mobile, $group_name);
        $data = [
                'name'                => $this->post('name'),
                'email'               => $this->post('email'),
                'group_id'            => '3',
                'group_name'          => 'customer',
                'customer_group_id'   => "1",
                'customer_group_name' => "General",
                'price_group_id'      => null,
                'price_group_name'    => null,
                'company'             => $this->post('company'),
                'address'             => $this->post('address'),
                'vat_no'              => $this->post('vat_no'),
                'city'                => $this->post('city'),
                'state'               => $this->post('state'),
                'postal_code'         => $this->post('postal_code'),
                'country'             => $this->post('country'),
                'phone'               => $this->post('phone'),
                'cf1'                 => $this->post('cf1'),
                'cf2'                 => $this->post('cf2'),
                'cf3'                 => $this->post('cf3'),
                'cf4'                 => $this->post('cf4'),
                'cf5'                 => $this->post('cf5'),
                'cf6'                 => $this->post('cf6'),
                'gst_no'              => $this->post('gst_no'),
            ];
            if (!$customer) {
                $cid = $this->companies_api->addCompany($data);
                $newCustomer = $this->companies_api->getCompanyById($cid);
                $this->set_response([
                        'data' => $newCustomer,
                        'status'  => true,
                    ], REST_Controller::HTTP_OK);
            } else {
                $id = $customer->id;
                $this->companies_api->updateCompany($id, $data);
                $updateCustomer = $this->companies_api->getCompanyById($id);
                $this->set_response([
                    'data' => $updateCustomer,
                    'message' => 'Customer be found for mobile ' . $mobile . '.',
                    'status'  => true,
                ], REST_Controller::HTTP_OK);
            }
        
    }
}

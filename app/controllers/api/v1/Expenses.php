<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Expenses extends REST_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('purchases_model');
    }
	public function addexpense_post()
    {
        $amount=$this->post('amount');
        $date=$this->post('date');
        $user_id=$this->post('user_id');
        
        if ($amount && $user_id) {
            if ($date) {
                $date = $this->sma->fld(trim($this->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $data = [
                'date'         => $date,
                'reference'    => $this->post('reference') ? $this->post('reference') : $this->site->getReference('ex'),
                'amount'       => $this->post('amount'),
                'created_by'   => $user_id,
                'note'         => $this->post('note'),
                'category_id'  => $this->post('category'),
                'warehouse_id' => $this->post('warehouse'),
            ];


           if ($this->purchases_model->addExpense($data)) {
            	$this->set_response([
                    'message' => lang('expense_added'),
                    'status'  => true
             	], REST_Controller::HTTP_OK);
	        } else {
	           $this->set_response([
                    'message' => lang('fill_all_information'),
                    'status'  => false
             	], REST_Controller::HTTP_OK);
	        }
        }else{
  			$this->set_response([
                    'message' => lang('fill_all_information'),
                    'status'  => false
             ], REST_Controller::HTTP_OK);
        }
    }

    

 }
<?php

// API link http://localhost/retaurant/api/v1/tasks/changecheckliststatus

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Hrm_rosters extends REST_Controller
{
    public function __construct()
    {
    	parent::__construct();
        $this->load->api_model('hrm_roster_api');
    }
  
    public function add_attendance_log_post()
    {
        $log_day_time = $this->post('log_day_time');
        $log_day_time = date('Y-m-d H:i:s', strtotime($log_day_time));
		$device = $this->post('device');
		$card_no = $this->post('card_no');
        $hasID = $this->hrm_roster_api->getUserCardid($card_no);
        if($hasID){
            $card_no = $hasID;
        }
        $data = array(
            'log_day_time' => $log_day_time,
            'device' => $device,
            'card_no' => $card_no,
        );
        $hasData = $this->hrm_roster_api->getUserAttendanceLog($data);
        if($hasData){
            $returnStatus['message']='Attendance log Exist!';
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }else{
            $insert = $this->hrm_roster_api->add_attendance_log($data);
            if($insert) {
            $returnStatus['message']='Attendance log added!';
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
           } else{
               $returnStatus['message']='Something went wrong, try again!';
               $returnStatus['status']=false;
               $this->set_response($returnStatus, REST_Controller::HTTP_OK);
           }
        }
        
        
    }

    public function add_attendance_post(){

        $emp_id = $this->post('employee_id');
		$attendance_date = $this->post('attendance_date');
        $attendance_date = date('Y-m-d', strtotime($attendance_date));

		$clock_in = $this->post('clock_in');
		$clock_out = $this->post('clock_out');

        $clock_in2 = $attendance_date.' '.$clock_in.':00';
		$clock_out2 = $attendance_date.' '.$clock_out.':00';
        //total work
		$total_work_cin =  new DateTime($clock_in2);
		$total_work_cout =  new DateTime($clock_out2);
		
		$interval_cin = $total_work_cout->diff($total_work_cin);
		$hours_in   = $interval_cin->format('%h');
		$minutes_in = $interval_cin->format('%i');
		$total_work = $hours_in .":".$minutes_in;

        $data = array(
            'employee_id' => $emp_id,
            'attendance_date' => $attendance_date,
            'clock_in' => $clock_in2,
            'clock_out' => $clock_out2,
            'time_late' => $clock_in2,
            'total_work' => $total_work,
            'early_leaving' => $clock_out2,
            'overtime' => $clock_out2,
            'attendance_status' => 'Present',
            'clock_in_out' => '0'
            );

        $insert = $this->hrm_roster_api->add_attendance($data);
        if($insert) {
        	 $returnStatus['message']='Attendance added!';
            //  $taskdata = $this->tasks_api->getTaskByID($task_id);
            //  $returnStatus['data']=$taskdata;
             $returnStatus['status']=true;
             $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else{
            $returnStatus['message']='Something went wrong, try again!';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

}
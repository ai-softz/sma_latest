    
<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Login extends REST_Controller
{

	public function __construct()
    {
        parent::__construct();

        $this->load->api_model('login_api');
        $this->load->admin_model('auth_model');
        
		$this->hash_method    = $this->config->item('hash_method', 'ion_auth');
		$this->store_salt      = $this->config->item('store_salt', 'ion_auth');
		$this->salt_length     = $this->config->item('salt_length', 'ion_auth');
       
    }

    public function checkLogin_post() {

    	$identity = $this->post('identity');

    	$password = $this->post('password');

    	if (empty($identity) || empty($password)) {
           $this->set_response([
                    'message' => lang('fill_all_information'),
                    'status'  => false,

                ], REST_Controller::HTTP_OK);
        }

    	$user = $this->login_api->login($identity, $password);

    	if($user) {
			$returnStatus['message1']=lang('login_successful');
            $returnStatus['status']=true;
			$returnStatus['data']=$user;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
    	} else {
    		$this->set_response([
                    'message' => lang('login_unsuccessful'),
                    'status'  => false,
                    //'data'  => array('identity' => $identity, 'password' => $password),
                ], REST_Controller::HTTP_OK);
    	}

    }


    


}
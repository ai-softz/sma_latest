<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Notifications extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('settings_api');
       
    }

    public function index_get()
    {
        $user_id = $this->get('user_id');
        //select warehouse id from sma_users where id = user_id
        //show all global and show all where warehouse=warhouseid from above query and show all for this user_id
        // order by readable above Notifications table
        // sales order status by pending desc

        $start= $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1;
        $limit= $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10;

        $warehouse_id = $this->db->select('warehouse_id')->from('users')->where('users.id', $user_id)->get()->row()->warehouse_id;           
        $notifications = $this->db
                            ->select('*')
                            ->from('notifications')
                            ->where('target_type', 'global')
                            ->or_where('warehouse_id', $warehouse_id)
                            ->or_where('id IN (SELECT notify_id FROM sma_notify_staffs WHERE user_id ='. $user_id . ')')
                            ->order_by('notifications.id desc')
                            ->limit($limit, ($start - 1))
                            ->get()
                            ->result()
                            ;
        $all_notifications = [];
        foreach($notifications as $notification) {
            $notify_data = $this->db->get_where('notifications_alert', array('notify_id'=>$notification->id, 'user_id'=>$user_id))->row();
            $notification->notify_alert = $notify_data;
            $all_notifications[]=$notification;
        }
        // arsort($all_notifications);
        if($all_notifications) {
            $data = [
                'data' => $all_notifications,
                'limit' => (int) $limit,
                'start' => (int) $start,
                'total' => count($all_notifications)
            ];
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'message' => 'No data found.',
                'status'  => false,
            ], REST_Controller::HTTP_OK);
        }
    }

    public function notifications_alert_get()
    {
        
        $user_id = $this->get('user_id');
        // select one row where notify_alert=null where user_id=this user id,

      $sql = "SELECT notify_id, user_id FROM sma_notify_staffs WHERE user_id=".$user_id." AND NOT EXISTS (SELECT notify_id FROM sma_notifications_alert WHERE sma_notifications_alert.notify_id= sma_notify_staffs.notify_id)";        
      $notify = $this->db->query($sql)->row();
      $notify_alert = '';
      if(!empty($notify)) {
            $notify_alert = $this->db->get_where('notifications', array('id' => $notify->notify_id))->row(); 
        }

        if($notify_alert) {
            $data = $notify_alert;
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'message' => 'No data found.',
                'status'  => false,
            ], REST_Controller::HTTP_OK);
        }
    } 
     
    public function single_notification_get()
    {
        $notify_id = $this->get('id');

        if(!empty($notify_id)) {
            $notifications_users = $this->db
                        ->select('*')
                        ->from('notifications')
                        ->where('notifications.id', $notify_id)
                        ->get()
                        ->row();
        } 
        if($notifications_users) {
            $data = $notifications_users;
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'message' => 'No data found.',
                'status'  => false,
            ], REST_Controller::HTTP_OK);
        }
    } 

    public function is_notified_post()
    {
        $user_id=$this->post('user_id');
        $notify_id=$this->post('notify_id');
        $is_notify=$this->post('is_notified');

        $data = [
            'notify_id' => $notify_id,
            'user_id' => $user_id,
            'is_notified' => $is_notify
        ];
        $notifications_alert = $this->db->get_where('notifications_alert', array('notify_id'=>$notify_id, 'user_id'=>$user_id))->row();
        // print_r($notifications_alert);exit();
        $insert = false;
        if(empty($notifications_alert)) {
            $insert = $this->db->insert('notifications_alert', $data);
            
        }
        if($insert) {
            
            $returnStatus['message'] = 'Notification alert is done.';
            $returnStatus['status'] = true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }else {
            $returnStatus['message'] = 'Already notified!';
            $returnStatus['status'] = false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function is_touch_post()
    {
        $user_id=$this->post('user_id');
        $notify_id=$this->post('notify_id');
        $is_touch=$this->post('is_touch');

        $data = [
            'is_read' => $is_touch
        ];

        $update = $this->db->where(array('notify_id'=>$notify_id, 'user_id'=>$user_id))->update('notifications_alert', $data);
        if($update) {
            
            $returnStatus['message'] = 'Notification is read.';
            $returnStatus['status'] = true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }else {
            $returnStatus['message'] = 'Something went wrong!';
            $returnStatus['status'] = false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    /*public function index_get()
    {

        $notifications = $this->db
                    ->select('notifications.*, notifications_alert.notify_id, notifications_alert.user_id')
                    ->from('notifications')
                    ->join('notifications_alert', 'notifications_alert.notify_id = notifications.id')
                    ->where('notifications_alert.is_notified', 0)
                    ->group_by('notifications.id')
                    ->order_by('notifications.id', 'asc')
                    ->get()
                    ->row()
                    ;
        if($notifications) {
            $data = $notifications;
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'message' => 'No data found.',
                'status'  => false,
            ], REST_Controller::HTTP_OK);
        }

    }*/

}
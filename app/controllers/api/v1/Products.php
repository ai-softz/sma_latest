<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Products extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('products_api');
        $this->load->api_model('settings_api');
        $this->load->admin_model('pos_model');
        $this->load->helper('file');
    }

    protected function setProduct($product)
    {
        $product->tax_rate       = $this->products_api->getTaxRateByID($product->tax_rate);
        $product->unit           = $this->products_api->getProductUnit($product->unit);
        $ctax                    = $this->site->calculateTax($product, $product->tax_rate);
        $product->price          = $this->sma->formatDecimal($product->price);
        $product->net_price      = $this->sma->formatDecimal($product->tax_method ? $product->price : $product->price - $ctax['amount']);
        $product->unit_price     = $this->sma->formatDecimal($product->tax_method ? $product->price + $ctax['amount'] : $product->price);
        $product->tax_method     = $product->tax_method ? 'exclusive' : 'inclusive';
        $product->tax_rate->type = $product->tax_rate->type ? 'percentage' : 'fixed';
        $options                 = $this->pos_model->getProductOptions($product->id, 1);
        $option = null;
        $optiontext = null;
        $optionprice = 0;
            if ($options) {
                $opt = current($options);
                if (!$option) {
                    $option = $opt->id;
                    $optiontext = $opt->name;
                    $optionprice = $this->sma->formatDecimal($opt->price);
                }
            } else {
                $opt        = json_decode('{}');
                $opt->price = 0;
            }
        
        $product->option   = $option;
        $product->optiontext   = $optiontext;
        $product->optionprice   = $optionprice;
        $product->options = $options;
        // $product->display_serial   = $product->display_serial;

        $product                 = (array) $product;
        ksort($product);
        return $product;
    }

    public function index_get()
    {
        $code = $this->get('code');

        $filters = [
            'code'     => $code,
            'include'  => $this->get('include') ? explode(',', $this->get('include')) : null,
            'start'    => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
            'limit'    => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
            'order_by' => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['display_serial', 'asc'],
            'brand'    => $this->get('brand_code') ? $this->get('brand_code') : null,
            'category' => $this->get('category_code') ? $this->get('category_code') : null,
            'hide_pos' => $this->get('hide_pos') ? $this->get('hide_pos') : 0,
            
        ];

        if ($code === null) {
            $this->db->cache_on();
            if ($products = $this->products_api->getProducts($filters)) {
                $pr_data = [];
                foreach ($products as $product) {
                    if (!empty($filters['include'])) {
                        foreach ($filters['include'] as $include) {
                            if ($include == 'brand') {
                                $product->brand = $this->products_api->getBrandByID($product->brand);
                            } elseif ($include == 'category') {
                                $product->category = $this->products_api->getCategoryByID($product->category);
                            } elseif ($include == 'photos') {
                                $product->photos = $this->products_api->getProductPhotos($product->id);
                            } elseif ($include == 'sub_units') {
                                $product->sub_units = $this->products_api->getSubUnits($product->unit);
                            }
                        }
                    }

                    $pr_data[] = $this->setProduct($product);
                }

                $data = [
                    'data'  => $pr_data,
                    'limit' => $filters['limit'],
                    'start' => $filters['start'],
                    // 'total' => $this->products_api->countProducts($filters),
                    'total' => count($pr_data)
                ];
                $this->db->cache_off(); 
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No product were found.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            if ($product = $this->products_api->getProduct($filters)) {
                if (!empty($filters['include'])) {
                    foreach ($filters['include'] as $include) {
                        if ($include == 'brand') {
                            $product->brand = $this->products_api->getBrandByID($product->brand);
                        } elseif ($include == 'category') {
                            $product->category = $this->products_api->getCategoryByID($product->category);
                        } elseif ($include == 'photos') {
                            $product->photos = $this->products_api->getProductPhotos($product->id);
                        } elseif ($include == 'sub_units') {
                            $product->sub_units = $this->products_api->getSubUnits($product->unit);
                        }
                    }
                }

                $product = $this->setProduct($product);
                $this->set_response($product, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'message' => 'Product could not be found for code ' . $code . '.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
    public function generateProductjsonfiles_get()
    {
        if (!is_dir('assets/jsondata/product')) {
            mkdir('assets/jsondata/product', 0777, true);
        }
       $categories = $this->settings_api->getAllCategories();
       foreach ($categories as $category) {
        if (!is_dir('assets/jsondata/products/'.$category->id)) {
            mkdir('assets/jsondata/products/'.$category->id, 0777, true);
        }
        $this->db->where('category_id', $category->id);
        $query = $this->db->get('products');
        $qlenght=$query->num_rows();
            for( $i= 1 ; $i <= $qlenght ; $i+=12 )
            {
               //$i=1;
                $filters = [
                    'code'     => $code,
                    'include'  => $this->get('include') ? explode(',', $this->get('include')) : null,
                    'start'    => $i,
                    'limit'    => 12,
                    // 'start'    => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
                    // 'limit'    => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
                    'order_by' => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['display_serial', 'asc'],
                    'brand'    => $this->get('brand_code') ? $this->get('brand_code') : null,
                    'category' => $category->id,
                ];
                if ($products = $this->products_api->getProducts($filters)) {
                $pr_data = [];
                foreach ($products as $product) {
                    if (!empty($filters['include'])) {
                        foreach ($filters['include'] as $include) {
                            if ($include == 'brand') {
                                $product->brand = $this->products_api->getBrandByID($product->brand);
                            } elseif ($include == 'category') {
                                $product->category = $this->products_api->getCategoryByID($product->category);
                            } elseif ($include == 'photos') {
                                $product->photos = $this->products_api->getProductPhotos($product->id);
                            } elseif ($include == 'sub_units') {
                                $product->sub_units = $this->products_api->getSubUnits($product->unit);
                            }
                        }
                    }
                    $prdtc=$this->setProduct($product);
                    $json = json_encode($prdtc,JSON_PRETTY_PRINT);
                    if(write_file('assets/jsondata/product/'.$product->id.'.json', $json)){}
                    $pr_data[] = $prdtc;

                }
                 $data = [
                    'data'  => $pr_data,
                    'limit' => 12,
                    'start' => $i,
                    //  'limit' => $filters['limit'],
                    // 'start' => $filters['start'],
                    'total' => count($pr_data),
                ];
                // $this->db->cache_off(); 
                //$this->response($data, REST_Controller::HTTP_OK);
               // break;
                
                 $json = json_encode($data,JSON_PRETTY_PRINT);
                if(write_file('assets/jsondata/products/'.$category->id.'/'.$i.'.json', $json)){

                }

            }else{
                 break;
             }
           }
       }
     }

     public function generateCategoryjsonfiles_get()
    {
        //$categories = $this->settings_api->getAllCategories();
        delete_files('assets/jsondata/allcategories');
        if (!is_dir('assets/jsondata/allcategories')) {
            mkdir('assets/jsondata/allcategories', 0777, true);
        }
        $this->db->where('hide_status', 0);
        $query = $this->db->get('categories');
        $qlenght=$query->num_rows();
         for( $i= 1 ; $i <= $qlenght ; $i+=12)
            {
                $filters = [
                    'start'    => $i,
                    'limit'    => 12,
                ];
                if ($categories = $this->settings_api->getCategoriesByLimit($filters)) {
                 $data = [
                    'data'  => $categories,
                    'limit' => 12,
                    'start' => $i,
                    'total' => count($categories),
                ];
               
                $json = json_encode($data,JSON_PRETTY_PRINT);
                if(write_file('assets/jsondata/allcategories/'.$i.'.json', $json)){

                }

            }else{
                 break;
            }
         }
     }
    public function add_product_post()
    {
        
        $warehouses = $this->site->getAllWarehouses();
        $productByCode = $this->products_model->getProductByCode($this->post('code'));
// print_r($productByCode);
        if (empty($productByCode)) {
            $insert_product = $this->addProduct();

            if ($insert_product) {
                $returnStatus['message']='Product added successfully!';
                $returnStatus['status']=true;
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            } else {
                $returnStatus['message']='Something went wrong, try again!';
                $returnStatus['status']=false;
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            }
            
        } else {
            $update_product = $this->editProduct($productByCode->id);

            if ($update_product) {
                $returnStatus['message']='Product updated successfully!';
                $returnStatus['status']=true;
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            } else {
                $returnStatus['message']='Something went wrong, try again!';
                $returnStatus['status']=false;
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            }
        }

        
    }

    public function addProduct()
    {
        // print_r($this->post());
        $tax_rate = $this->post('tax_rate') ? $this->site->getTaxRateByID($this->post('tax_rate')) : null;
            $data     = [
                'display_serial'    => $this->post('display_serial'),
                'code'              => $this->post('code'),
                'barcode_symbology' => $this->post('barcode_symbology'),
                'name'              => htmlspecialchars($this->post('name')),
                'type'              => $this->post('type'),
                'brand'             => $this->post('brand'),
                'category_id'       => $this->post('category'),
                'subcategory_id'    => $this->post('subcategory') ? $this->post('subcategory') : null,
                'cost'              => $this->sma->formatDecimal($this->post('cost')),
                'price'             => $this->sma->formatDecimal($this->post('price')),
                'unit'              => $this->post('unit'),
                'sale_unit'         => $this->post('default_sale_unit'),
                'purchase_unit'     => $this->post('default_purchase_unit'),
                'tax_rate'          => $this->post('tax_rate'),
                'tax_method'        => $this->post('tax_method'),
                'alert_quantity'    => $this->post('alert_quantity'),
                'track_quantity'    => $this->post('track_quantity') ? $this->post('track_quantity') : '0',
                'details'           => $this->post('details'),
                'product_details'   => $this->post('product_details'),
                'supplier1'         => $this->post('supplier'),
                'supplier1price'    => $this->sma->formatDecimal($this->post('supplier_price')),
                'supplier2'         => $this->post('supplier_2'),
                'supplier2price'    => $this->sma->formatDecimal($this->post('supplier_2_price')),
                'supplier3'         => $this->post('supplier_3'),
                'supplier3price'    => $this->sma->formatDecimal($this->post('supplier_3_price')),
                'supplier4'         => $this->post('supplier_4'),
                'supplier4price'    => $this->sma->formatDecimal($this->post('supplier_4_price')),
                'supplier5'         => $this->post('supplier_5'),
                'supplier5price'    => $this->sma->formatDecimal($this->post('supplier_5_price')),
                'cf1'               => $this->post('cf1'),
                'cf2'               => $this->post('cf2'),
                'cf3'               => $this->post('cf3'),
                'cf4'               => $this->post('cf4'),
                'cf5'               => $this->post('cf5'),
                'cf6'               => $this->post('cf6'),
                'promotion'         => $this->post('promotion'),
                'promo_price'       => $this->sma->formatDecimal($this->post('promo_price')),
                'start_date'        => $this->post('start_date') ? $this->sma->fsd($this->post('start_date')) : null,
                'end_date'          => $this->post('end_date') ? $this->sma->fsd($this->post('end_date')) : null,
                'supplier1_part_no' => $this->post('supplier_part_no'),
                'supplier2_part_no' => $this->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->post('supplier_5_part_no'),
                'file'              => $this->post('file_link'),
                'slug'              => $this->post('slug'),
                'weight'            => $this->post('weight'),
                'featured'          => $this->post('featured'),
                'hsn_code'          => $this->post('hsn_code'),
                'hide'              => $this->post('hide') ? $this->post('hide') : 0,
                'second_name'       => htmlspecialchars($this->post('second_name')),
                'is_packed'         => $this->post('is_packed') ? $this->post('is_packed') : 0,
                'pack_piece'        => $this->post('pack_piece'),
                'packed_product'    => $this->post('packed_product'),
                'printer_selection'    => $this->post('printer_selection'),
            ];
            // print_r($data); die;
            $warehouse_qty      = null;
            $product_attributes = null;
            $this->load->library('upload');
            if ($this->post('type') == 'standard') {
                $wh_total_quantity = 0;
                $pv_total_quantity = 0;
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s]           = $this->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    if ($this->post('wh_qty_' . $warehouse->id)) {
                        $warehouse_qty[] = [
                            'warehouse_id' => $this->post('wh_' . $warehouse->id),
                            'quantity'     => $this->post('wh_qty_' . $warehouse->id),
                            'rack'         => $this->post('rack_' . $warehouse->id) ? $this->post('rack_' . $warehouse->id) : null,
                        ];
                        $wh_total_quantity += $this->post('wh_qty_' . $warehouse->id);
                    }
                }

                if ($this->post('attributes')) {
                    $a = sizeof($this->post('attr_name'));
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($this->post('attr_name')[$r])) {
                            $product_attributes[] = [
                                'name'         => $this->post('attr_name')[$r],
                                'warehouse_id' => $this->post('attr_warehouse')[$r],
                                'quantity'     => $this->post('attr_quantity')[$r],
                                'price'        => $this->post('attr_price')[$r],
                            ];
                            $pv_total_quantity += $this->post('attr_quantity')[$r];
                        }
                    }
                } else {
                    $product_attributes = null;
                }

                // if ($wh_total_quantity != $pv_total_quantity && $pv_total_quantity != 0) {
                //     $this->form_validation->set_rules('wh_pr_qty_issue', 'wh_pr_qty_issue', 'required');
                //     $this->form_validation->set_message('required', lang('wh_pr_qty_issue'));
                // }
            }

            if ($this->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->post('type') == 'combo') {
                $total_price = 0;
                $c           = sizeof($this->post('combo_item_code')) - 1;
                for ($r = 0; $r <= $c; $r++) {
                    if (isset($this->post('combo_item_code')[$r]) && isset($this->post('combo_item_quantity')[$r]) && isset($this->post('combo_item_price')[$r])) {
                        $items[] = [
                            'item_code'  => $this->post('combo_item_code')[$r],
                            'quantity'   => $this->post('combo_item_quantity')[$r],
                            'unit_price' => $this->post('combo_item_price')[$r],
                        ];
                    }
                    $total_price += $this->post('combo_item_price')[$r] * $this->post('combo_item_quantity')[$r];
                }
                // if ($this->sma->formatDecimal($total_price) != $this->sma->formatDecimal($this->post('price'))) {
                //     $this->form_validation->set_rules('combo_price', 'combo_price', 'required');
                //     $this->form_validation->set_message('required', lang('pprice_not_match_ciprice'));
                // }
                $data['track_quantity'] = 0;
            } elseif ($this->post('type') == 'digital') {
                if ($_FILES['digital_file']['size'] > 0) {
                    $config['upload_path']   = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size']      = $this->allowed_file_size;
                    $config['overwrite']     = false;
                    $config['encrypt_name']  = true;
                    $config['max_filename']  = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('digital_file')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect('products/add');
                    }
                    $file         = $this->upload->file_name;
                    $data['file'] = $file;
                } else {
                    // if (!$this->post('file_link')) {
                    //     $this->form_validation->set_rules('digital_file', lang('digital_file'), 'required');
                    // }
                }
                $config                 = null;
                $data['track_quantity'] = 0;
            }
            if (!isset($items)) {
                $items = null;
            }

            $photos = null;
            $data['quantity'] = $wh_total_quantity ?? 0;
            // $this->sma->print_arrays($data, $warehouse_qty, $product_attributes);
            $insert_product = $this->products_model->addProduct($data, $items, $warehouse_qty, $product_attributes, $photos, "desktop_bakala");
            // print_r($insert_product); die;
            
            return $insert_product;
    }
    public function editProduct($id)
    {
        // print_r($this->post());
        $warehouses          = $this->site->getAllWarehouses();
        $warehouses_products = $this->products_model->getAllWarehousesWithPQ($id);
        $product             = $this->site->getProductByID($id);
        
            $data = ['code'         => $this->post('code'),
                'barcode_symbology' => $this->post('barcode_symbology'),
                'name'              => htmlspecialchars($this->post('name')),
                'display_serial'    => $this->post('display_serial'),
                'type'              => $this->post('type'),
                'brand'             => $this->post('brand'),
                'category_id'       => $this->post('category'),
                'subcategory_id'    => $this->post('subcategory') ? $this->post('subcategory') : null,
                'cost'              => $this->sma->formatDecimal($this->post('cost')),
                'price'             => $this->sma->formatDecimal($this->post('price')),
                'unit'              => $this->post('unit'),
                'sale_unit'         => $this->post('default_sale_unit'),
                'purchase_unit'     => $this->post('default_purchase_unit'),
                'tax_rate'          => $this->post('tax_rate'),
                'tax_method'        => $this->post('tax_method'),
                'alert_quantity'    => $this->post('alert_quantity'),
                'track_quantity'    => $this->post('track_quantity') ? $this->post('track_quantity') : '0',
                'details'           => $this->post('details'),
                'product_details'   => $this->post('product_details'),
                'supplier1'         => $this->post('supplier'),
                'supplier1price'    => $this->sma->formatDecimal($this->post('supplier_price')),
                'supplier2'         => $this->post('supplier_2'),
                'supplier2price'    => $this->sma->formatDecimal($this->post('supplier_2_price')),
                'supplier3'         => $this->post('supplier_3'),
                'supplier3price'    => $this->sma->formatDecimal($this->post('supplier_3_price')),
                'supplier4'         => $this->post('supplier_4'),
                'supplier4price'    => $this->sma->formatDecimal($this->post('supplier_4_price')),
                'supplier5'         => $this->post('supplier_5'),
                'supplier5price'    => $this->sma->formatDecimal($this->post('supplier_5_price')),
                'cf1'               => $this->post('cf1'),
                'cf2'               => $this->post('cf2'),
                'cf3'               => $this->post('cf3'),
                'cf4'               => $this->post('cf4'),
                'cf5'               => $this->post('cf5'),
                'cf6'               => $this->post('cf6'),
                'promotion'         => $this->post('promotion'),
                'promo_price'       => $this->sma->formatDecimal($this->post('promo_price')),
                'start_date'        => $this->post('start_date') ? $this->sma->fsd($this->post('start_date')) : null,
                'end_date'          => $this->post('end_date') ? $this->sma->fsd($this->post('end_date')) : null,
                'supplier1_part_no' => $this->post('supplier_part_no'),
                'supplier2_part_no' => $this->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->post('supplier_5_part_no'),
                'slug'              => $this->post('slug'),
                'weight'            => $this->post('weight'),
                'featured'          => $this->post('featured'),
                'hsn_code'          => $this->post('hsn_code'),
                'hide'              => $this->post('hide') ? $this->post('hide') : 0,
                'hide_pos'          => $this->post('hide_pos') ? $this->post('hide_pos') : 0,
                'second_name'       => htmlspecialchars($this->post('second_name')),
                'is_packed'         => $this->post('is_packed') ? $this->post('is_packed') : 0,
                'pack_piece'        => $this->post('pack_piece'),
                'packed_product'    => $this->post('packed_product'),
                'printer_selection'    => $this->post('printer_selection'),

            ];
            // print_r($data); die;
            $warehouse_qty      = null;
            $product_attributes = null;
            $update_variants    = [];
            $this->load->library('upload');
            if ($this->post('type') == 'standard') {
                if ($product_variants = $this->products_model->getProductOptions($id)) {
                    foreach ($product_variants as $pv) {
                        $update_variants[] = [
                            'id'    => $this->post('variant_id_' . $pv->id),
                            'name'  => $this->post('variant_name_' . $pv->id),
                            'cost'  => $this->post('variant_cost_' . $pv->id),
                            'price' => $this->post('variant_price_' . $pv->id),
                        ];
                    }
                }
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s]           = $this->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    $warehouse_qty[] = [
                        'warehouse_id' => $this->post('wh_' . $warehouse->id),
                        'rack'         => $this->post('rack_' . $warehouse->id) ? $this->post('rack_' . $warehouse->id) : null,
                    ];
                }

                if ($this->post('attributes')) {
                    $a = sizeof($this->post('attr_name'));
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($this->post('attr_name')[$r])) {
                            if ($product_variatnt = $this->products_model->getPrductVariantByPIDandName($id, trim($this->post('attr_name')[$r]))) {
                                $this->form_validation->set_message('required', lang('product_already_has_variant') . ' (' . $this->post('attr_name')[$r] . ')');
                                $this->form_validation->set_rules('new_product_variant', lang('new_product_variant'), 'required');
                            } else {
                                $product_attributes[] = [
                                    'name'         => $this->post('attr_name')[$r],
                                    'warehouse_id' => $this->post('attr_warehouse')[$r],
                                    'quantity'     => $this->post('attr_quantity')[$r],
                                    'price'        => $this->post('attr_price')[$r],
                                ];
                            }
                        }
                    }
                } else {
                    $product_attributes = null;
                }
            }

            if ($this->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->post('type') == 'combo') {
                $total_price = 0;
                $c           = sizeof($this->post('combo_item_code')) - 1;
                for ($r = 0; $r <= $c; $r++) {
                    if (isset($this->post('combo_item_code')[$r]) && isset($this->post('combo_item_quantity')[$r]) && isset($this->post('combo_item_price')[$r])) {
                        $items[] = [
                            'item_code'  => $this->post('combo_item_code')[$r],
                            'quantity'   => $this->post('combo_item_quantity')[$r],
                            'unit_price' => $this->post('combo_item_price')[$r],
                        ];
                    }
                    $total_price += $this->post('combo_item_price')[$r] * $this->post('combo_item_quantity')[$r];
                }
                // if ($this->sma->formatDecimal($total_price) != $this->sma->formatDecimal($this->post('price'))) {
                //     $this->form_validation->set_rules('combo_price', 'combo_price', 'required');
                //     $this->form_validation->set_message('required', lang('pprice_not_match_ciprice'));
                // }
                $data['track_quantity'] = 0;
            } 
            
            if (!isset($items)) {
                $items = null;
            }

            $photos = null;
            $data['quantity'] = $wh_total_quantity ?? 0;
        // }
        $update = $this->products_model->updateProduct($id, $data, $items, $warehouse_qty, $product_attributes, $photos, $update_variants);
        $this->site->generateSingleProductjsonfiles($id, $this->post('platform'));

        return true;
    }

    public function insertProductByCode(){
        $code = $this->get('code');
        $str = file_get_contents('assets/jsondata/product/'.$code.'.json');
        $json = json_decode($str, true);
         $checkproducts = $this->products_model->getProductByCode($json['code']);
         if($checkproducts){
         }else{
        $pathinfo = pathinfo($json['image_url']);
        $img=$pathinfo['filename'].'.'.$pathinfo['extension'];
        $data     = [
           'id'                => $json['id'],
           'code'              => $json['code'],
           'barcode_symbology' => "code128",
           'name'              => $json['name'],
           'type'              => "standard",
           'brand'             => "",
           'category_id'       => $json['category'],
           'subcategory_id'    => null,
           'price'             => $json['price'],
           'cost'              => $json['price'],
           'unit'              => 1,
           'sale_unit'         => 1,
           'purchase_unit'     => 1,
           'tax_rate'          => 2,
           'tax_method'        => 0,
           'alert_quantity'    => 5,
           'track_quantity'    => 1,
           'details'           => "",
           'product_details'   => "",
           'supplier1'         => null,
           'supplier1price'    => 0,
           'supplier2'         => null,
           'supplier2price'    => 0,
           'supplier3'         => null,
           'supplier3price'    => 0,
           'supplier4'         => null,
           'supplier4price'    => 0,
           'supplier5'         => null,
           'supplier5price'    => 0,
           'cf1'               => null,
           'cf2'               => null,
           'cf3'               => null,
           'cf4'               => null,
           'cf5'               => null,
           'cf6'               => null,
           'promotion'         => 0,
           'promo_price'       => 0,
           'start_date'        => null,
           'end_date'          => null,
           'supplier1_part_no' => null,
           'supplier2_part_no' => null,
           'supplier3_part_no' => null,
           'supplier4_part_no' => null,
           'supplier5_part_no' => null,
           'file'              => null,
           'slug'              => $json['slug'],
           'weight'            => null,
           'featured'          => 0,
           'hsn_code'          => 0,
           'hide'              => 0,
           'second_name'       => $json['second_name'],
           'is_packed'         => 0,
           'pack_piece'        => 0,
           'packed_product'    => null,
           'image'             => $img,
          ];
          $this->db->insert('products', $data);
          }
        
    }
}

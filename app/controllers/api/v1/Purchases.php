<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Purchases extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('purchases_api');
        $this->load->admin_model('purchases_model');
        $this->load->model('site');
    }

    protected function setPurchase($purchase)
    {
        unset($purchase->attachment, $purchase->updated_at, $purchase->purchase_id, $purchase->return_id, $purchase->return_purchase_ref, $purchase->return_purchase_total);
        if (isset($purchase->items) && !empty($purchase->items)) {
            foreach ($purchase->items as &$item) {
                if (isset($item->option_id) && !empty($item->option_id)) {
                    if ($variant = $this->purchases_api->getProductVariantByID($item->option_id)) {
                        $item->product_variant_id   = $variant->id;
                        $item->product_variant_name = $variant->name;
                    }
                }
                $item->product_unit_quantity = $item->unit_quantity;
                unset($item->id, $item->date, $item->transfer_id, $item->quantity_balance, $item->quantity_received, $item->purchase_id, $item->warehouse_id, $item->real_unit_cost, $item->supplier_part_no, $item->purchase_item_id, $item->option_id, $item->unit_quantity);
                $item = (array) $item;
                ksort($item);
            }
        }
        $purchase = (array) $purchase;
        ksort($purchase);
        return $purchase;
    }

    public function index_get()
    {
        $reference = $this->get('reference');

        $filters = [
            'reference'   => $reference,
            'include'     => $this->get('include') ? explode(',', $this->get('include')) : null,
            'start'       => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
            'limit'       => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
            'start_date'  => $this->get('start_date') && is_numeric($this->get('start_date')) ? $this->get('start_date') : null,
            'end_date'    => $this->get('end_date') && is_numeric($this->get('end_date')) ? $this->get('end_date') : null,
            'order_by'    => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['id', 'decs'],
            'supplier_id' => $this->get('supplier_id') ? $this->get('supplier_id') : null,
            'customer'    => $this->get('customer') ? $this->get('customer') : null,
        ];

        if ($reference === null) {
            if ($purchases = $this->purchases_api->getPurchases($filters)) {
                $sl_data = [];
                foreach ($purchases as $purchase) {
                    if (!empty($filters['include'])) {
                        foreach ($filters['include'] as $include) {
                            if ($include == 'items') {
                                $purchase->items = $this->purchases_api->getPurchaseItems($purchase->id);
                            }
                            if ($include == 'warehouse') {
                                $purchase->warehouse = $this->purchases_api->getWarehouseByID($purchase->warehouse_id);
                            }
                        }
                    }

                    $purchase->created_by = $this->purchases_api->getUser($purchase->created_by);
                    $sl_data[]            = $this->setPurchase($purchase);
                }

                $data = [
                    'data'  => $sl_data,
                    'limit' => (int) $filters['limit'],
                    'start' => (int) $filters['start'],
                    'total' => $this->purchases_api->countPurchases($filters),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No purchase record found.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            if ($purchase = $this->purchases_api->getPurchase($filters)) {
                if (!empty($filters['include'])) {
                    foreach ($filters['include'] as $include) {
                        if ($include == 'items') {
                            $purchase->items = $this->purchases_api->getPurchaseItems($purchase->id);
                        }
                        if ($include == 'warehouse') {
                            $purchase->warehouse = $this->purchases_api->getWarehouseByID($purchase->warehouse_id);
                        }
                    }
                }

                $purchase->created_by = $this->purchases_api->getUser($purchase->created_by);
                $purchase             = $this->setPurchase($purchase);
                $this->set_response($purchase, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'message' => 'Purchase could not be found for reference ' . $reference . '.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function postexpenses_post()
    {
        $data = [
                'date'         => $this->post('date'),
                'reference'    => $this->post('reference'),
                'amount'       => $this->post('amount'),
                'created_by'   => $this->post('created_by'),
                'note'         => $this->post('note', true),
                'attachment'   => $this->post('attachment'),
                'category_id'  => $this->post('category_id', true),
                'warehouse_id' => $this->post('warehouse_id', true),
                'local_id'          => $this->post('local_id') ? $this->post('local_id') : null,
                'online_id'         => $this->post('online_id') ? $this->post('online_id') : null,
                'platform'    => $this->post('platform') ? $this->post('platform') : null,// Desktop, Online, Waiter, QR Code
                'is_synchronize'     => $this->post('is_syncronize') ? $this->post('is_syncronize') : 0,
            ];
            if($expense_id = $this->purchases_api->addExpense($data)) {
                $returnStatus['expense_id']=$expense_id;
                $returnStatus['status']=true;
                $returnStatus['message']=lang('expense_added');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            } else {
                $returnStatus['status']=false;
                $returnStatus['message']=lang('something_wrong');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            }
    }

    public function postpurchases_post()
    {
        // $this->sma->checkPermissions();

        // $this->form_validation->set_message('is_natural_no_zero', $this->lang->line('no_zero_required'));
        // $this->form_validation->set_rules('warehouse', $this->lang->line('warehouse'), 'required|is_natural_no_zero');
        // $this->form_validation->set_rules('supplier', $this->lang->line('supplier'), 'required');

        // $this->session->unset_userdata('csrf_token');
        if (true) {
            $reference = $this->post('reference_no') ? $this->post('reference_no') : $this->site->getReference('po');
            
            $date = $this->post('date');
            $warehouse_id     = $this->post('warehouse');
            $supplier_id      = $this->post('supplier_id');
            $status           = $this->post('status');
            $shipping         = $this->post('shipping') ? $this->post('shipping') : 0;
            $supplier_details = $this->site->getCompanyByID($supplier_id);
            $supplier         = $this->post('supplier');
            $note             = $this->sma->clear_tags($this->post('note'));
            $payment_term     = $this->post('payment_term');
            $due_date         = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;

            $total            = 0;
            $product_tax      = 0;
            $product_discount = 0;
            $i                = sizeof($this->post('product'));
            $gst_data         = [];
            $total_cgst       = $total_sgst       = $total_igst = 0;
            $product_details = $this->post('product');
            // echo "<pre>";print_r($product_details);die;
            for ($r = 0; $r < $i; $r++) {
                $item_code          = $product_details[$r]['product_code'];
                $item_net_cost      = $this->sma->formatDecimal($product_details[$r]['net_cost']);
                $unit_cost          = $this->sma->formatDecimal($product_details[$r]['unit_cost']);
                $real_unit_cost     = $this->sma->formatDecimal($product_details[$r]['real_unit_cost']);
                $item_unit_quantity = $product_details[$r]['quantity'];
                $item_option        =  null;
                $item_tax_rate      = $product_details[$r]['item_tax'] ?? null;
                $tax_rate_id      = $product_details[$r]['tax_rate_id'] ?? null;
                $item_discount      = $product_details[$r]['product_discount'] ?? null;
                $item_expiry        = (isset($product_details[$r]['expiry']) && !empty($product_details[$r]['expiry'])) ? $this->sma->fsd($product_details[$r]['expiry']) : null;
                $supplier_part_no   = (isset($product_details[$r]['part_no']) && !empty($product_details[$r]['part_no'])) ? $product_details[$r]['part_no'] : null;
                $item_unit          = $product_details[$r]['product_unit'];
                $item_quantity      = $product_details[$r]['product_base_quantity'];

                if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {
                    $product_details = $this->purchases_model->getProductByCode($item_code);
                    if ($item_expiry) { 
                        $today = date('Y-m-d');
                        if ($item_expiry <= $today) {
                            $returnStatus['status']=false;
                            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
                        }
                    }
                    // $unit_cost = $real_unit_cost;
                    $pr_discount      = $this->site->calculateDiscount($item_discount, $unit_cost);
                    $unit_cost        = $this->sma->formatDecimal($unit_cost - $pr_discount);
                    $item_net_cost    = $unit_cost;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax         = '';

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax        = $this->site->calculateTax($product_details, $tax_details, $unit_cost);
                        $item_tax    = $this->sma->formatDecimal($ctax['amount']);
                        $tax         = $ctax['tax'];
                        if ($product_details->tax_method != 1) {
                            $item_net_cost = $unit_cost - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_unit_quantity, 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculateIndianGST($pr_item_tax, ($this->Settings->state == $supplier_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_cost * $item_unit_quantity) + $pr_item_tax);
                    $unit     = $this->site->getUnitByID($item_unit);

                    $product = [
                        'product_id'        => $product_details->id,
                        'product_code'      => $item_code,
                        'product_name'      => $product_details->name,
                        'option_id'         => $item_option,
                        'net_unit_cost'     => $item_net_cost,
                        'unit_cost'         => $this->sma->formatDecimal($item_net_cost + $item_tax),
                        'quantity'          => $item_quantity,
                        'product_unit_id'   => $item_unit,
                        'product_unit_code' => $unit->code,
                        'unit_quantity'     => $item_unit_quantity,
                        'quantity_balance'  => $status == 'received' ? $item_quantity : 0,
                        'quantity_received' => $status == 'received' ? $item_quantity : 0,
                        'warehouse_id'      => $warehouse_id,
                        'item_tax'          => $pr_item_tax,
                        'tax_rate_id'       => $tax_rate_id,
                        'tax'               => $tax,
                        'discount'          => $item_discount,
                        'item_discount'     => $pr_item_discount,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'expiry'            => $item_expiry,
                        'real_unit_cost'    => $real_unit_cost,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                        'supplier_part_no'  => $supplier_part_no,
                    ];

                    if ($unit->id != $product_details->unit) {
                        $product['base_unit_cost'] = $this->site->convertToBase($unit, $real_unit_cost);
                    } else {
                        $product['base_unit_cost'] = $real_unit_cost;
                    }

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                //$this->form_validation->set_rules('product', lang('order_items'), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->post('discount'), ($total + $product_tax), true);
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax      = $this->site->calculateOrderTax($this->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax      = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total    = $this->sma->formatDecimal(($this->sma->formatDecimal($total) + $this->sma->formatDecimal($total_tax) + $this->sma->formatDecimal($shipping) - $this->sma->formatDecimal($order_discount)), 4);
            $data           = ['reference_no' => $reference,
                'date'                        => $date,
                'supplier_id'                 => $supplier_id,
                'supplier'                    => $supplier,
                'warehouse_id'                => $warehouse_id,
                'note'                        => $note,
                'total'                       => $total,
                'product_discount'            => $product_discount,
                'order_discount_id'           => $this->post('order_discount_id'),
                'order_discount'              => $order_discount,
                'total_discount'              => $total_discount,
                'product_tax'                 => $product_tax,
                'order_tax_id'                => $this->post('order_tax_id'),
                'order_tax'                   => $order_tax,
                'total_tax'                   => $total_tax,
                'shipping'                    => $this->sma->formatDecimal($shipping),
                'grand_total'                 => $grand_total,
                'status'                      => $status,
                'created_by'                  => $this->post('created_by'),
                'payment_term'                => $payment_term,
                'due_date'                    => $due_date,
                'local_id'          => $this->post('local_id') ? $this->post('local_id') : null,
                'online_id'         => $this->post('online_id') ? $this->post('online_id') : null,
                'platform'    => $this->post('platform') ? $this->post('platform') : null,// Desktop, Online, Waiter, QR Code
                'is_synchronize'     => $this->post('is_syncronize') ? $this->post('is_syncronize') : 0,

            ];
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }
            $payment_details = $this->post('payment_details');
            $payment = [
                'date'         => $payment_details['date'],
                //'purchase_id'  => $this->post('purchase_id'),
                'reference_no' => $payment_details['reference_no'] ,
                'amount'       => $payment_details['amount-paid'] ,
                'paid_by'      => $payment_details['paid_by'] ,
                'cheque_no'    => $payment_details['cheque_no'] ,
                'cc_no'        => $payment_details['pcc_no'] ,
                'cc_holder'    => $payment_details['pcc_holder'] ,
                'cc_month'     => $payment_details['pcc_month'] ,
                'cc_year'      => $payment_details['pcc_year'] ,
                'cc_type'      => $payment_details['pcc_type'] ,
                'note'         => $payment_details['note'] ,
                'created_by'   => $payment_details['created_by'] ,
                'type'         => 'sent',
            ];

            // if ($_FILES['document']['size'] > 0) {
            //     $this->load->library('upload');
            //     $config['upload_path']   = $this->digital_upload_path;
            //     $config['allowed_types'] = $this->digital_file_types;
            //     $config['max_size']      = $this->allowed_file_size;
            //     $config['overwrite']     = false;
            //     $config['encrypt_name']  = true;
            //     $this->upload->initialize($config);
            //     if (!$this->upload->do_upload('document')) {
            //         $error = $this->upload->display_errors();
            //         $this->session->set_flashdata('error', $error);
            //         redirect($_SERVER['HTTP_REFERER']);
            //     }
            //     $photo              = $this->upload->file_name;
            //     $data['attachment'] = $photo;
            // }

            // $this->sma->print_arrays($data, $products);
        }
// echo "<pre>";print_r($data);print_r($products); die;
        if ($purchase_id = $this->purchases_api->addPurchase($data, $products, $payment)) {
            $returnStatus['purchase_id']=$purchase_id; 
            $returnStatus['status']=true; 
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }
}

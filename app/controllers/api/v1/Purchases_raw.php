<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Purchases_raw extends REST_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('purchases_api');
        $this->load->api_model('purchase_raw_api');
        $this->load->model('site');
        // $this->lang->admin_load('purchases', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('purchase_raw_model');
        $this->load->admin_model('reports_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path         = 'assets/uploads/';
        $this->thumbs_path         = 'assets/uploads/thumbs/';
        $this->image_types         = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types  = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size   = '1024';
        $this->data['logo']        = true;
    }
    

    public function index_get()
    {
        
    }

    
    public function delete_purchase_raw($id='')
    {
        if($this->purchase_raw_model->delete_purchase_raw($id)) {
            $returnStatus['status']= true;   
            $returnStatus['message']=lang('purchase_raw_deleted');
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function add_purchase_raw_post()
    {
            if($this->post('reference_no')) {
                $reference = $this->post('reference_no');
            } else {
                $purchase_last = $this->db->limit(1)->order_by('id DESC')->get('purchases_raw')->row();
                if(empty($purchase_last)) {
                    $reference = 'POR00001';
                } else {
                $ref_substr = substr($purchase_last->reference_no, 3) + 1;
                $reference = 'POR'.sprintf('%05s', $ref_substr);
                }
            }
            
    
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id     = $this->post('warehouse');
            $status           = $this->post('status');
            $note             = $this->sma->clear_tags($this->post('note'));
            $payment_term     = $this->post('payment_term');
            $due_date         = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;

            $total            = 0;
            $i                = sizeof($this->post('product'));
            $gst_data         = [];
            for ($r = 0; $r < $i; $r++) {
                $item_code          = $this->post('product')[$r];
                $item_net_cost      = $this->sma->formatDecimal($this->post('net_cost')[$r]);
                $item_unit_quantity = $this->post('quantity')[$r];
                
                $item_expiry        = null;
                $item_unit          = $this->post('product_unit')[$r];
                $item_quantity      = $this->post('product_base_quantity')[$r];

                if (isset($item_code) && isset($item_quantity)) {
                    $product_details = $this->purchase_raw_model->getProductByCodeRaw($item_code); 
                    
                    $subtotal = ($item_net_cost * $item_unit_quantity);
                    $unit     = $this->site->getUnitByID($item_unit);

                    $product = [
                        'product_id'        => $product_details->id,
                        'product_code'      => $item_code,
                        'product_name'      => $product_details->caption,
                        
                        'net_unit_cost'     => $item_net_cost,
                        'quantity'          => $item_quantity,
                        'product_unit_id'   => $item_unit,
                        'product_unit_code' => $unit->code,
                        'unit_quantity'     => $item_unit_quantity,
                        'quantity_balance'  => $status == 'received' ? $item_quantity : 0,
                        'quantity_received' => $status == 'received' ? $item_quantity : 0,
                        'warehouse_id'      => $warehouse_id,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'expiry'            => $item_expiry,
                        // 'real_unit_cost'    => $real_unit_cost,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                        // 'supplier_part_no'  => $supplier_part_no,
                    ];

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $returnStatus['status']= false;   
                $returnStatus['message']=lang('purchase_items_required');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            } else {
                krsort($products);
            }
            $supplier = $this->post('supplier');

            $supplier_name = $this->db->get_where('companies', ['id'=>$supplier, 'group_name'=>'supplier'])->row()->name;

            $grand_total    = $this->sma->formatDecimal($this->sma->formatDecimal($total));
            $data           = [
                'reference_no'                => $reference,
                'date'                        => $date,
                'warehouse_id'                => $warehouse_id,
                'supplier_id'                 => $supplier,  
                'supplier'                    => $supplier_name,  
                'note'                        => $note,
                'total'                       => $total,
                'grand_total'                 => $grand_total,
                'status'                      => $status,
                'created_by'                  => $this->post("user_id"),
                'payment_term'                => $payment_term,
                'due_date'                    => $due_date,
            ];
            
        $this->purchase_raw_api->addPurchaseRaw($data, $products);
        $returnStatus['status']=true;
        $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        $returnStatus['message']=lang('purchase_added');
        $this->response($returnStatus, REST_Controller::HTTP_OK);
    }

    public function edit_purchase_raw($id='')
    {
        $this->sma->checkPermissions();

        $this->session->unset_userdata('csrf_token');
        if ($this->post('warehouse')) {

            if($this->post('reference_no')) {
                $reference = $this->post('reference_no');
            } else {
                $purchase_last = $this->db->limit(1)->order_by('id DESC')->get('purchases_raw')->row();
                if(empty($purchase_last)) {
                    $reference = 'POR00001';
                } else {
                $ref_substr = substr($purchase_last->reference_no, 3) + 1;
                $reference = 'POR'.sprintf('%05s', $ref_substr);
                }
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $purchase_raw_id     = $this->post('purchase_raw_id');
            $warehouse_id     = $this->post('warehouse');
            $status           = $this->post('status');
            $note             = $this->sma->clear_tags($this->post('note'));
            $payment_term     = $this->post('payment_term');
            $due_date         = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;

            $total            = 0;
            $i                = sizeof($this->post('product'));
            $gst_data         = [];
            
            for ($r = 0; $r < $i; $r++) {
                $item_code          = $this->post('product')[$r];
                $item_net_cost      = $this->sma->formatDecimal($this->post('net_cost')[$r]);
                $item_unit_quantity = $this->post('quantity')[$r];
                
                $item_expiry        = (isset($this->post('expiry')[$r]) && !empty($this->post('expiry')[$r])) ? $this->sma->fsd($this->post('expiry')[$r]) : null;
                $item_unit          = $this->post('product_unit')[$r];
                $item_quantity      = $this->post('product_base_quantity')[$r];

                if (isset($item_code) && isset($item_quantity)) {
                    $product_details = $this->purchase_raw_model->getProductByCodeRaw($item_code);
                    if ($item_expiry) {
                        $today = date('Y-m-d');
                        if ($item_expiry <= $today) {
                            $returnStatus['status']=false;
                            $returnStatus['message']=lang('product_expiry_date_issue'). ' (' . $product_details->name . ')';
                            $this->response($returnStatus, REST_Controller::HTTP_OK);
                        }
                    }
                    
                    $subtotal = ($item_net_cost * $item_unit_quantity);
                    $unit     = $this->site->getUnitByID($item_unit);

                    $product = [
                        'product_id'        => $product_details->id,
                        'product_code'      => $item_code,
                        'product_name'      => $product_details->caption,
                        
                        'net_unit_cost'     => $item_net_cost,
                        'quantity'          => $item_quantity,
                        'product_unit_id'   => $item_unit,
                        'product_unit_code' => $unit->code,
                        'unit_quantity'     => $item_unit_quantity,
                        'quantity_balance'  => $status == 'received' ? $item_quantity : 0,
                        'quantity_received' => $status == 'received' ? $item_quantity : 0,
                        'warehouse_id'      => $warehouse_id,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'expiry'            => $item_expiry,
                        // 'real_unit_cost'    => $real_unit_cost,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                        'supplier_part_no'  => $supplier_part_no,
                    ];

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $returnStatus['status']=false;
                $returnStatus['message']=lang('order_items_required');
                $this->response($returnStatus, REST_Controller::HTTP_OK); 
                
            } else {
                krsort($products);
            }

            $grand_total    = $this->sma->formatDecimal($this->sma->formatDecimal($total));
            $data           = ['reference_no' => $reference,
                'date'                        => $date,
                'warehouse_id'                => $warehouse_id,
                'note'                        => $note,
                'total'                       => $total,
                
                'shipping'                    => $this->sma->formatDecimal($shipping),
                'grand_total'                 => $grand_total,
                'status'                      => $status,
                'created_by'                  => $this->session->userdata('user_id'),
                'payment_term'                => $payment_term,
                'due_date'                    => $due_date,
            ];
           
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path']   = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size']      = $this->allowed_file_size;
                $config['overwrite']     = false;
                $config['encrypt_name']  = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $returnStatus['status']=false;
                    $returnStatus['message']=lang('error');
                    $this->response($returnStatus, REST_Controller::HTTP_OK); 
                }
                $photo              = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);
        }

        $this->purchase_raw_model->updatePurchaseRaw($purchase_raw_id, $data, $products);

        $this->session->set_userdata('remove_pols', 1);
        
        $returnStatus['status']=true;
        $returnStatus['message']=lang('purchase_edited');
        $this->response($returnStatus, REST_Controller::HTTP_OK); 
        
    }

    public function add_payment_raw($data, $id)
    {
        
        $purchase = $this->purchase_raw_model->getPurchaseRawByID($id);
        if ($purchase->payment_status == 'paid' && $purchase->grand_total == $purchase->paid) {
            $returnStatus['status']=false;
            $returnStatus['message']=lang('purchase_already_paid');
            $this->response($returnStatus, REST_Controller::HTTP_OK); 
            $this->sma->md();
        }

        if ($this->post('date')) {
            // print_r($this->post()); die;
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $payment = [
                'date'         => $date,
                'purchase_raw_id'  => $this->post('purchase_id'),
                'reference_no' => $this->post('reference_no') ? $this->post('reference_no') : $this->site->getReference('ppay'),
                'amount'       => $this->post('amount-paid'),
                'paid_by'      => $this->post('paid_by'),
                'cheque_no'    => $this->post('cheque_no'),
                'cc_no'        => $this->post('pcc_no'),
                'cc_holder'    => $this->post('pcc_holder'),
                'cc_month'     => $this->post('pcc_month'),
                'cc_year'      => $this->post('pcc_year'),
                'cc_type'      => $this->post('pcc_type'),
                'note'         => $this->sma->clear_tags($this->post('note')),
                'created_by'   => $this->session->userdata('user_id'),
                'type'         => 'sent',
            ];

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path']   = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size']      = $this->allowed_file_size;
                $config['overwrite']     = false;
                $config['encrypt_name']  = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    
                }
                $photo                 = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);
        } 
        $this->purchase_raw_model->addPaymentRaw($payment); 

        $returnStatus['status']=true;
        $returnStatus['message']=lang('payment_added');
        $this->response($returnStatus, REST_Controller::HTTP_OK);
    }

    public function xxadd_raw_purchase_post()
    {
        
        $date = date('Y-m-d', strtotime($this->post('date')));
        
        if ($amount && $user_id) {
            if ($date) {
                $date = $this->sma->fld(trim($this->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $data = [
                'date'         => $date,
                'reference'    => $this->post('reference'),
                'supplier_id'    => $this->post('supplier_id'),
                'grand_total'       => $this->post('grand_total'),
                'warehouse_id' => $this->post('warehouse_id'),
            ];



            $item_code = $this->post('item_code');
            $item_quantity = $this->post('item_quantity');
            $item_net_cost = $this->post('net_cost');

            $product_details = $this->purchase_raw_model->getProductByCodeRaw($item_code); 
            $item_unit = $product_details->id;
            $subtotal = ($item_net_cost * $item_quantity);
            $unit     = $this->site->getUnitByID($item_unit);

            $product = [
                'product_id'        => $product_details->id,
                'product_code'      => $item_code,
                'product_name'      => $product_details->caption,
                'net_unit_cost'     => $item_net_cost,
                'quantity'          => $item_quantity,
                'product_unit_id'   => $item_unit,
                'product_unit_code' => $unit->code,
                'unit_quantity'     => $item_quantity,
                'quantity_balance'  => $status == 'received' ? $item_quantity : 0,
                'quantity_received' => $status == 'received' ? $item_quantity : 0,
                'warehouse_id'      => $warehouse_id,
                'subtotal'          => $this->sma->formatDecimal($subtotal),
                'expiry'            => $item_expiry,
                'date'              => date('Y-m-d', strtotime($date)),
                'status'            => $status,
            ];

            $total = $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);


           if ($this->purchases_model->addExpense($data)) {
            	$this->set_response([
                    'message' => lang('expense_added'),
                    'status'  => true
             	], REST_Controller::HTTP_OK);
	        } else {
	           $this->set_response([
                    'message' => lang('fill_all_information'),
                    'status'  => false
             	], REST_Controller::HTTP_OK);
	        }
        }else{
  			$this->set_response([
                    'message' => lang('fill_all_information'),
                    'status'  => false
             ], REST_Controller::HTTP_OK);
        }
    }

    public function make_products()
    {
        
        if ($this->post('make_products')) {
            $this->session->set_userdata('remove_pols', 1);
            // echo '<pre>'; print_r($this->post()); die;

            $date = $this->post('date');
            $date = date('Y-m-d', strtotime($date));
            
            for($i = 0; $i < sizeof($this->post('product_id')); $i++) {

                $quantity = $this->post('quantity')[$i];

                $product_id = $this->post('product_id')[$i];
                $product_qty = $this->purchase_raw_model->get_product_quantity($product_id);
                
                $new_qty = $product_qty->quantity + $this->post('quantity')[$i];
                // print_r($new_qty); die;
                // $new_qty = $product_qty->make_product_qty + $this->post('quantity')[$i];
                $data_product = array(
                    'quantity' => $new_qty
                );
                $update = $this->purchase_raw_model->update_product_quantity($product_id, $data_product);

                $data_make_products = array(
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'date' => $date,
                    'warehouse_id' => $this->post('warehouse'),
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d')
                );
                $this->purchase_raw_model->add_make_products($data_make_products);
            }
            
            $this->session->set_flashdata('message', $this->lang->line('make_product_successfully_added'));
            admin_redirect('purchases_raw/make_products');
        } else {
            $this->data['error']      = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            // $this->session->set_userdata('user_csrf', $value);
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['csrf'] = $this->session->userdata('user_csrf');

            $bc                 = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('purchases'), 'page' => lang('purchases')], ['link' => '#', 'page' => lang('make_products')]];
            $meta               = ['page_title' => lang('add_purchase'), 'bc' => $bc];
            $this->page_construct('purchase_raw/make_products', $meta, $this->data);
        }
    }

    public function suggestions_raw()
    {
        $term        = $this->get('term', true);
        $supplier_id = $this->get('supplier_id', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed  = $this->sma->analyze_term($term);
        $sr        = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $sr        = addslashes($sr);
        $qty       = $analyzed['quantity'] ?? null;
        $bprice    = $analyzed['price']    ?? null;

        $rows = $this->purchases_model->getProductNamesRaw($sr);
        if ($rows) {
            $r = 0;
            foreach ($rows as $row) {
                
                $c                    = uniqid(mt_rand(), true);
                $option               = false;
                // $row->item_tax_method = $row->tax_method;
                $options              = $this->purchases_model->getProductOptions($row->id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->purchases_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt       = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = false;
                }
                $row->option           = $option_id;
                
                $base_quantity = 1.0;
                $row->base_quantity    = $base_quantity;
                $row->base_unit        = $row->unit;
                $row->net_unit_cost        = $row->price;
                
                $row->new_entry        = 1;
                $row->expiry           = '';
                $row->qty              = 1.0;
                $row->quantity_balance = '';
                $row->discount         = '0';

                $units    = $this->site->getUnitsByBUID($row->base_unit);

                $pr[] = ['id' => sha1($c . $r), 'item_id' => $row->id, 'label' => $row->caption . ' (' . $row->code . ')',
                    'row'     => $row, 'units' => $units, 'options' => $options, ];
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json([['id' => 0, 'label' => lang('no_match_found'), 'value' => $term]]);
        }
    }

    public function category_raw_material()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('category')]];
        $meta = ['page_title' => lang('category'), 'bc' => $bc];
		$this->page_construct('purchase_raw/category_raw_material', $meta, $this->data);
    }
    public function getRawMaterialCategories()
    {
        $iDisplayLength=$this->post('iDisplayLength');
		$iDisplayStart=$this->post('iDisplayStart');
		$sSortDir_0=$this->post('sSortDir_0');
		$iSortCol_0=$this->post('iSortCol_0');
		$sSearch=$this->post('sSearch');

        $records = $this->purchase_raw_model->getRawMaterialCategories($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            
				$edit = '';
				//$edit .= " <a href='" . admin_url('hrm_payroll/edit_add_deduct_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_add_deduct') . "'><i class=\"fa fa-edit\"></i></a>";
                
                $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_category_raw_material') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('purchases_raw/delete_category_raw_material/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->purchase_raw_model->total_RawMaterialCategories($sSearch);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function delete_category_raw_material($id='')
    {
        if($this->purchase_raw_model->delete_category_raw_material($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('category_raw_material_deleted')]);
        }
    }

    public function add_categories_raw_form()
    {
        $this->data[] = '';

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'purchase_raw/add_categories_raw_form', $this->data);
    }

    public function add_categories_raw()
    {
    //    print_r($this->post());die;
       if($this->post()) {

            $data = array(
                'caption' => $this->post('caption'),
                'caption_alt' => $this->post('caption_alt'),

                'remark' => $this->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->purchase_raw_model->add_categories_raw($data);
       }

    }

    public function products_raw_material()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['categories_raw'] = $this->purchase_raw_model->get_categories_raw(); 

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('raw_material_products')]];
        $meta = ['page_title' => lang('raw_material_products'), 'bc' => $bc];
		$this->page_construct('purchase_raw/products_raw_material', $meta, $this->data);
    }
    public function getProductsRawTable()
    {
        $categoryId=$this->post('categoryId');
        $iDisplayLength=$this->post('iDisplayLength');
		$iDisplayStart=$this->post('iDisplayStart');
		$sSortDir_0=$this->post('sSortDir_0');
		$iSortCol_0=$this->post('iSortCol_0');
		$sSearch=$this->post('sSearch');

        $records = $this->purchase_raw_model->getProductsRawTable($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $categoryId);
        // print_r($records);die;
        $data = [];
        foreach($records as $value) {

            $category_raw = $this->db->where('id', $value->category_id)->get('categories_raw')->row();
            $unit = $this->db->where('id', $value->unit)->get('units')->row();

            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->caption;
            $nestedData[] = $value->caption_alt;
            $nestedData[] = $value->cat_caption ? $value->cat_caption : '';
            $nestedData[] = $value->price;
            $nestedData[] = $unit->name;
            
            $edit = '';
            $edit .= " <a href='" . admin_url('purchases_raw/edit_products_raw_form/'.$value->id) . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang('edit_products_raw') . "'><i class=\"fa fa-edit\"></i></a>";
            
            $edit .= " <a href='#' class='tip po' title='<b>" . lang('delete_products_raw') . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('purchases_raw/delete_products_raw/'.$value->id) . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>";
                
				$nestedData[] = $edit;
				$data[] = $nestedData;
		}
        $all_users = $this->purchase_raw_model->total_getProductsRawTable($sSearch, $categoryId);
		$totalData = sizeof($all_users); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }
    public function delete_products_raw($id='')
    {
        if($this->purchase_raw_model->delete_products_raw($id)) {
            $this->sma->send_json(['error' => 0, 'msg' => lang('products_raw_deleted')]);
        }
    }
    public function add_products_raw_form()
    {
        $this->data[] = '';
        $this->data['categories_raw'] = $this->purchase_raw_model->getCategoriesRaw();
        $this->data['base_units'] = $this->site->getAllBaseUnits();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'purchase_raw/add_products_raw_form', $this->data);
    }
    public function edit_products_raw_form($id='')
    {
        $this->data[] = '';
        $this->data['products_raw'] = $this->purchase_raw_model->getProductsRawByid($id);
        $this->data['categories_raw'] = $this->purchase_raw_model->getCategoriesRaw();
        $this->data['base_units'] = $this->site->getAllBaseUnits();

        $this->data['modal_js']   = $this->site->modal_js();
        $this->load->view($this->theme . 'purchase_raw/edit_products_raw_form', $this->data);
    }

    public function add_products_raw()
    {
    //    print_r($this->post());die;
       if($this->post()) {

            $data = array(
                'caption' => $this->post('caption'),
                'caption_alt' => $this->post('caption_alt'),
                'category_id' => $this->post('category_id'),
                'price' => $this->post('price'),
                'unit' => $this->post('unit'),
                'code' => $this->post('code'),
                'slug' => $this->post('slug'),

                'remark' => $this->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->purchase_raw_model->add_products_raw($data);
       }

    }
    public function edit_products_raw()
    {
    //    print_r($this->post());die;
       if($this->post()) {
            
            $products_raw_id = $this->post('products_raw_id');
            $data = array(
                'caption' => $this->post('caption'),
                'caption_alt' => $this->post('caption_alt'),
                'category_id' => $this->post('category_id'),
                'price' => $this->post('price'),
                'unit' => $this->post('unit'),
                'code' => $this->post('code'),
                'slug' => $this->post('slug'),

                'remark' => $this->post('remark'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );
            $this->purchase_raw_model->edit_products_raw($data, $products_raw_id);
       }

    }

    public function daily_purchases_raw($warehouse_id = null, $year = null, $month = null, $pdf = null, $user_id = null)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $config              = [
            'show_next_prev' => true,
            'next_prev_url'  => admin_url('purchases_raw/daily_purchases_raw/' . ($warehouse_id ? $warehouse_id : 0)),
            'month_type'     => 'long',
            'day_type'       => 'long',
        ];

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $purchases = $this->purchase_raw_model->getDailyPurchasesRaw($year, $month, $warehouse_id);

        if (!empty($purchases)) {
            foreach ($purchases as $purchase) {
                $daily_purchase[$purchase->date] = "<table class='table table-bordered table-hover table-striped table-condensed data' style='margin:0;'>". '<tr><td>' . lang('total') . '</td><td>' . $this->sma->formatMoney($purchase->total) . '</td></tr></table>';
            }
        } else {
            $daily_purchase = [];
        }

        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_purchase);
        $this->data['year']     = $year;
        $this->data['month']    = $month;
        if ($pdf) {
            $html = $this->load->view($this->theme . 'purchase_raw/daily', $this->data, true);
            $name = lang('daily_purchases') . '_' . $year . '_' . $month . '.pdf';
            $html = str_replace('<p class="introtext">' . lang('reports_calendar_text') . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses']    = $this->site->getAllWarehouses();
        $this->data['warehouse_id']  = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        $bc                          = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('reports'), 'page' => lang('reports')], ['link' => '#', 'page' => lang('daily_purchases_report')]];
        $meta                        = ['page_title' => lang('daily_purchases_report'), 'bc' => $bc];
        $this->page_construct('purchase_raw/daily_purchases_raw', $meta, $this->data);
    }

    public function monthly_purchases_raw($warehouse_id = null, $year = null, $pdf = null, $user_id = null)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->load->language('calendar');
        $this->data['error']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['year']      = $year;
        $this->data['purchases'] = $this->purchase_raw_model->getMonthlyPurchasesRaw($year, $warehouse_id);
        if ($pdf) {
            $html = $this->load->view($this->theme . 'reports/monthly', $this->data, true);
            $name = lang('monthly_purchases') . '_' . $year . '.pdf';
            $html = str_replace('<p class="introtext">' . lang('reports_calendar_text') . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses']    = $this->site->getAllWarehouses();
        $this->data['warehouse_id']  = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        $bc                          = [['link' => base_url(), 'page' => lang('home')], ['link' => admin_url('reports'), 'page' => lang('reports')], ['link' => '#', 'page' => lang('monthly_purchases_report')]];
        $meta                        = ['page_title' => lang('monthly_purchases_report'), 'bc' => $bc];
        $this->page_construct('purchase_raw/monthly_purchases_raw', $meta, $this->data);
    }

    public function purchase_raw_product()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('raw_products_purchase_report')]];
        $meta = ['page_title' => lang('raw_products_purchase_report'), 'bc' => $bc];
		$this->page_construct('purchase_raw/product_raw_purchase', $meta, $this->data);
    }
    
    public function getPrRawPurchaseReport()
    {

        $iDisplayLength=$this->post('iDisplayLength');
		$iDisplayStart=$this->post('iDisplayStart');
		$sSortDir_0=$this->post('sSortDir_0');
		$iSortCol_0=$this->post('iSortCol_0');
		$sSearch=$this->post('sSearch');

        $product     = $this->get('product') ? $this->get('product') : null;
        $start_date  = $this->get('start_date') ? $this->get('start_date') : null;
        $end_date    = $this->get('end_date') ? $this->get('end_date') : null;
        // echo '<pre>'; print_r($start_date); print_r($product);die;
        $sql = $this->purchase_raw_model->getPrRawPurchaseReport($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product);
        $records = $this->db->query($sql)->result();
        
        
        $data = [];
        foreach($records as $value) {

            $purchased = explode('__', $value->purchased);
            $nestedData = array();
            $nestedData[] = $value->code;
            $nestedData[] = $value->caption;
            $nestedData[] = '('. number_format($purchased[0], 0) .') ' . number_format($purchased[1], 2);
            
				$data[] = $nestedData;
		}
        $all_sql = $this->purchase_raw_model->total_getProductsRawReport($sSearch, $start_date, $end_date, $product);
        $all_records = $this->db->query($sql)->result();
		$totalData = sizeof($all_records); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }

    public function purchase_make_raw_product()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('make_product')]];
        $meta = ['page_title' => lang('make_product'), 'bc' => $bc];
		$this->page_construct('purchase_raw/purchase_make_raw_product', $meta, $this->data);
    }
    
    public function getPurchaseMakePrReport()
    {

        $iDisplayLength=$this->post('iDisplayLength');
		$iDisplayStart=$this->post('iDisplayStart');
		$sSortDir_0=$this->post('sSortDir_0');
		$iSortCol_0=$this->post('iSortCol_0');
		$sSearch=$this->post('sSearch');

        $product     = $this->get('product') ? $this->get('product') : null;
        $start_date  = $this->get('start_date') ? $this->get('start_date') : null;
        $end_date    = $this->get('end_date') ? $this->get('end_date') : null;
        // echo '<pre>'; print_r($start_date); print_r($product);die;
        $sql = $this->purchase_raw_model->getPurchaseMakePrReport2($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product);
        $records = $this->db->query($sql)->result();
        
        // print_r($records); die;
        $data = [];
        foreach($records as $value) {

            // $product = $this->db->select('code, name')->get_where('products', ['id' => $value->product_id])->row();

            // $make_products_qty = $this->db->get_where('products', ['id' => $value->id])->row();

            // $purchased = explode('__', $value->purchased);
            $nestedData = array();
            $nestedData[] = $value->id;
            $nestedData[] = $value->name . ' (' . $value->code . ')';
            $nestedData[] = $value->make_qty;
            // $nestedData[] = $value->date;

            // $nestedData[] = '('. number_format($purchased[0], 0) .') ' . number_format($purchased[1], 2);
            // $nestedData[] = $make_products_qty->make_product_qty;
            
			$data[] = $nestedData;
		}
        $all_sql = $this->purchase_raw_model->total_getPurchaseMakePrReport2($sSearch, $start_date, $end_date, $product);
        $all_records = $this->db->query($sql)->result();
		$totalData = sizeof($all_records); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }

    public function get_products_raw()
    {
        $term = $this->get('term', true);
        if (strlen($term) < 1) {
            die();
        }
        $term = addslashes($term);
        $rows = $this->purchase_raw_model->getProductNamesRaw($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = ['id' => $row->id, 'label' => $row->caption . ' (' . $row->code . ')' ];
            }
            $this->sma->send_json($pr);
        } else {
            echo false;
        }
    }

    public function get_products()
    {
        $term = $this->get('term', true);
        if (strlen($term) < 1) {
            die();
        }
        $term = addslashes($term);
        $rows = $this->purchase_raw_model->getProductNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = ['id' => $row->id, 'label' => $row->name . ' (' . $row->code . ')' ];
            }
            $this->sma->send_json($pr);
        } else {
            echo false;
        }
    }

    public function make_products_report()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc   = [['link' => base_url(), 'page' => lang('home')],  ['link' => '#', 'page' => lang('purchase_raw_product')]];
        $meta = ['page_title' => lang('purchase_raw_product'), 'bc' => $bc];
		$this->page_construct('purchase_raw/product_raw_purchase', $meta, $this->data);
    }
    
    public function get_make_products_report()
    {

        $iDisplayLength=$this->post('iDisplayLength');
		$iDisplayStart=$this->post('iDisplayStart');
		$sSortDir_0=$this->post('sSortDir_0');
		$iSortCol_0=$this->post('iSortCol_0');
		$sSearch=$this->post('sSearch');

        $product     = $this->get('product') ? $this->get('product') : null;
        $start_date  = $this->get('start_date') ? $this->get('start_date') : null;
        $end_date    = $this->get('end_date') ? $this->get('end_date') : null;
        // echo '<pre>'; print_r($start_date); print_r($product);die;
        $sql = $this->purchase_raw_model->get_make_products_report($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product);
        $records = $this->db->query($sql)->result();
        
        $data = [];
        foreach($records as $value) {

            $purchased = explode('__', $value->purchased);
            $nestedData = array();
            $nestedData[] = $value->code;
            $nestedData[] = $value->caption;
            $nestedData[] = '('. number_format($purchased[0], 0) .') ' . number_format($purchased[1], 2);
            
				$data[] = $nestedData;
		}
        $all_sql = $this->purchase_raw_model->total_getProductsRawReport($sSearch, $start_date, $end_date, $product);
        $all_records = $this->db->query($sql)->result();
		$totalData = sizeof($all_records); 

        $sOutput = [
			'iTotalRecords'        => $totalData,
			'iTotalDisplayRecords' => $totalData,
			'aaData'               => $data,
		];
		echo json_encode($sOutput);
    }

    public function xget_products_raw($value='')
    {
        $response = array();
        if($this->post('name')) {
            $name = $this->post('name');
            
            $results = $this->db->like('caption', $name)->or_like('caption_alt', $name)->or_like('code', $name)->or_like('slug', $name)->get('products_raw')->result();
            // print_r($results); die;
            foreach ($results as $key => $value) {
                $response[] = array(
                    "value"=> $value->id,
                    "label"=> $value->caption
                );
            }
            echo json_encode($response);
        }
    }

    public function add_raw_categories_post()
    {
        $id = $this->post('id');
        
            $row_exist = $this->db->get_where('categories_raw', ['id' => $id])->row();
            // print_r($row_exist); die;
            if($row_exist) {
                $data = [
                    'code'         => $this->post('code'),
                    'caption'    => $this->post('caption'),
                    'caption_alt'       => $this->post('caption_alt'),
                    'slug'   => $this->post('slug'),
                    'remark'         => $this->post('remark', true),
                    'created_by'   => $this->post('created_by'),
                    'created_at'  => $this->post('created_at', true),
                    'is_synchronize'     => $this->post('is_synchronize') ? $this->post('is_synchronize') : 0,
                ];
                $update = $this->db->where('id', $id)->update('categories_raw', $data);
            } else {
                $data = [
                    'id'         => $id,
                    'code'         => $this->post('code'),
                    'caption'    => $this->post('caption'),
                    'caption_alt'       => $this->post('caption_alt'),
                    'slug'   => $this->post('slug'),
                    'remark'         => $this->post('remark', true),
                    'created_by'   => $this->post('created_by'),
                    'created_at'  => $this->post('created_at', true),
                    'is_synchronize'     => $this->post('is_synchronize') ? $this->post('is_synchronize') : 0,
                ];
                $insert = $this->db->insert('categories_raw', $data);
            }
            // if($category_id = $this->purchase_raw_api->addRawCategory($data)) {
            if($insert || $update) {
                // $returnStatus['category_id']=$category_id;
                $returnStatus['status'] = true;
                $returnStatus['message']=lang('category_added');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            } else {
                $returnStatus['status']=false;
                $returnStatus['message']=lang('something_wrong');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            }
    }
    public function add_raw_products_post()
    {
        $id = $this->post('id');
        
            $row_exist = $this->db->get_where('products_raw', ['id' => $id])->row();
            if($row_exist) {
                $data = [
                    'code'         => $this->post('code'),
                    'caption'    => $this->post('caption'),
                    'caption_alt'       => $this->post('caption_alt'),
                    'slug'   => $this->post('slug'),
                    'remark'         => $this->post('remark', true),
                    'created_by'   => $this->post('created_by'),
                    'created_at'  => $this->post('created_at', true),
                    'is_synchronize'     => $this->post('is_synchronize') ? $this->post('is_synchronize') : 0,
                    'category_id'   => $this->post('category_id'),
                    'price'   => $this->post('price'),
                    'unit'   => $this->post('unit'),
                    'type'   => $this->post('type'),
                ];
                $update = $this->db->where('id', $id)->update('products_raw', $data);
            } else {
                $data = [
                    'id'         => $id,
                    'code'         => $this->post('code'),
                    'caption'    => $this->post('caption'),
                    'caption_alt'       => $this->post('caption_alt'),
                    'slug'   => $this->post('slug'),
                    'remark'         => $this->post('remark', true),
                    'created_by'   => $this->post('created_by'),
                    'created_at'  => $this->post('created_at', true),
                    'is_synchronize'     => $this->post('is_synchronize') ? $this->post('is_synchronize') : 0,
                    'category_id'   => $this->post('category_id'),
                    'price'   => $this->post('price'),
                    'unit'   => $this->post('unit'),
                    'type'   => $this->post('type'),
                ];
                $insert = $this->db->insert('products_raw', $data);
            }
            if($insert || $update) {
                $returnStatus['insert_id']=$insert_id;
                $returnStatus['status']=true;
                $returnStatus['message']=lang('data_added');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            } else {
                $returnStatus['status']=false;
                $returnStatus['message']=lang('something_wrong');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            }
    }
    public function add_raw_purchases_post()
    {
        $id = $this->post('id');
        $data = array();
        $fields_table   = $this->db->list_fields(db_prefix() . 'purchases_raw');
        foreach ($fields_table as $field) {
                $data[$field] = $this->post($field);
        }
        
        $row_exist = $this->db->get_where('purchases_raw', ['id' => $id])->row();
        // if data already exist update
        if($row_exist) {
            unset($data['id']);
            $items = array();
            for($r = 0; $r < sizeof($product_details); $r++) {

                $fields_table   = $this->db->list_fields(db_prefix() . 'purchase_items_raw');
                foreach ($fields_table as $field) {
                    $item[$field] = $product_details[$r][$field];
                }
                unset($item['id']);
                $items[] = $item;
            }
            $payments = array();
            $payment_detail = $this->post('payment_details');
            for($r = 0; $r < sizeof($payment_detail); $r++) {
                $fields_table   = $this->db->list_fields(db_prefix() . 'payments');
                foreach ($fields_table as $field) {
                    $payment[$field] = $payment_detail[$r][$field];
                }
                unset($payment['id']);
                $payments[] = $payment;
            }
            $update = $this->purchase_raw_api->updateRawPurchase($data, $items, $payments, $id);
        } 
        // if data not exist update
        else {
            $product_details = $this->post('details');
            $items = array();
            for($r = 0; $r < sizeof($product_details); $r++) {

                $fields_table   = $this->db->list_fields(db_prefix() . 'purchase_items_raw');
                foreach ($fields_table as $field) {
                    $item[$field] = $product_details[$r][$field];
                }
                $items[] = $item;
            }
            $payments = array();
            $payment_detail = $this->post('payment_details');
            for($r = 0; $r < sizeof($payment_detail); $r++) {
                $fields_table   = $this->db->list_fields(db_prefix() . 'payments');
                foreach ($fields_table as $field) {
                    $payment[$field] = $payment_detail[$r][$field];
                }
                $payments[] = $payment;
            }
            $insert_id = $this->purchase_raw_api->addRawPurchase($data, $items, $payments);
        }
    
        if($insert_id || $update) {
            $returnStatus['insert_id']=$insert_id;
            $returnStatus['status']=true;
            $returnStatus['message']=lang('data_added');
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
            $returnStatus['status']=false;
            $returnStatus['message']=lang('something_wrong');
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }
}
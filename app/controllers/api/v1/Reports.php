<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Reports extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->methods['index_get']['limit'] = 500;
        $this->load->admin_model('reports_model');
        $this->load->api_model('settings_api');
        $this->load->library('form_validation');
    }

	public function todayprofit_get()
    {
    	$date = null; $warehouse_id = null; $re = null;
        
        if (!$date) {
            $date = date('Y-m-d');
        }
        $warehouse_id_get = $this->get('warehouse_id');
        $pc_no = $this->get('pc_no') ? $this->get('pc_no') : 1;
        if ($warehouse_id_get) {
            $warehouse_id = $warehouse_id_get;
        }
        $costing    = $this->reports_model->getCosting($date, $warehouse_id);
        $discount   = $this->reports_model->getOrderDiscount($date, $warehouse_id);
        $expenses   = $this->reports_model->getExpenses($date, $warehouse_id);
        $returns    = $this->reports_model->getReturns($date, $warehouse_id);
        $warehouses = $this->site->getAllWarehouses();
        $swh        = $warehouse_id;
        $date       = $date;
        $discount = $discount ? $discount->order_discount : 0;
        $expense = $expenses ? $expenses->total : 0;
        $costingcal = $costing ? $costing->cost : 0;
        $products_cost = $costing ? $costing->sales : 0;
        
        $data= array(
                'costing' => $this->sma->formatMoney($costingcal), 
                'products_cost' => $this->sma->formatMoney($products_cost), 
                'net_cost' => $this->sma->formatMoney($costing->net_cost), 
                'net_sales' => $this->sma->formatMoney($costing->net_sales), 
                'discount' => $this->sma->formatMoney($discount), 
                'expenses' => $this->sma->formatMoney($expense),
                'profits' => $this->sma->formatMoney($costing->sales - $costing->cost - $discount - $expense),
                'returns' => $this->sma->formatMoney($returns->total),
                'warehouses' => $warehouses,
                'swh' => $swh,
                'date' => $date,
                'register_close_time' => date('Y-m-d H:i:s'),
                'printers' => $this->settings_api->getAllPrinters($pc_no),
            );
        $this->response([
            'data' => $data,
            'message' => 'No found.',
            'status'  => false,
        ], REST_Controller::HTTP_OK);
        
    }

}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Sales extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('sales_api');
        $this->load->api_model('settings_api');
        $this->load->api_model('companies_api');
        $this->load->api_model('order_status_tracking_api');

        $this->load->admin_model('pos_model');
        $this->load->admin_model('sales_model');
        $this->load->model('site');
        $this->load->helper('text');
        $this->pos_settings           = $this->pos_model->getSetting();
        $this->pos_settings->pin_code = $this->pos_settings->pin_code ? md5($this->pos_settings->pin_code) : null;
        $this->data['pos_settings']   = $this->pos_settings;
        $this->Settings = $this->site->get_setting();
        $this->session->set_userdata('last_activity', now());
        // $this->lang->admin_load('pos', $this->Settings->user_language);
        $this->load->helper('file');
        $this->load->library('form_validation');
    }

    protected function setSale($sale)
    {
        unset($sale->address_id, $sale->api, $sale->attachment, $sale->hash, $sale->pos, $sale->reserve_id, $sale->return_id, $sale->return_sale_ref, $sale->return_sale_total, $sale->sale_id, $sale->shop, $sale->staff_note, $sale->surcharge, $sale->updated_at, $sale->suspend_note);
        if (isset($sale->items) && !empty($sale->items)) {
            foreach ($sale->items as &$item) {
                if (isset($item->option_id) && !empty($item->option_id)) {
                    if ($variant = $this->sales_api->getProductVariantByID($item->option_id)) {
                        $item->product_variant_id   = $variant->id;
                        $item->product_variant_name = $variant->name;
                    }
                }
                $item->product_unit_quantity = $item->unit_quantity;
                unset($item->id, $item->sale_id, $item->warehouse_id, $item->real_unit_price, $item->sale_item_id, $item->option_id, $item->unit_quantity);
                $item = (array) $item;
                ksort($item);
            }
        }
        $sale = (array) $sale;
        ksort($sale);
        return $sale;
    }

    public function index_get()
    {

        $is_printed = $this->get('is_printed');
        $reference = $this->get('reference');
        $sales_id = $this->get('sales_id');
        $customermob = $this->get('customermob');
        $customerid = $this->get('customer_id') ? $this->get('customer_id') : null;
        if($customermob){
            $customerinfo = $this->companies_api->getSingleCompaniesByMobile($customermob);
            if($customerinfo){
                $customerid = $customerinfo->id;
            }
        }
        
        
        $pc_no = $this->get('pc_no') ? $this->get('pc_no') : 1;
        if($this->get('device_name')=="tab1"){
            $this->site->generateNewOrderCreatedjsonfiles(false, "tab1");
        }else if($this->get('device_name')=="tab2"){
            $this->site->generateNewOrderCreatedjsonfiles(false, "tab2");
        }

        $filters = [
            'reference'   => $reference,
            'sales_id'   => $sales_id,
            'order_no'   => $this->get('order_no') ? $this->get('order_no') : null,           
            'include'     => $this->get('include') ? explode(',', $this->get('include')) : null,
            'start'       => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
            'limit'       => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
            'start_date'  => $this->get('start_date') && is_numeric($this->get('start_date')) ? $this->get('start_date') : null,
            'end_date'    => $this->get('end_date') && is_numeric($this->get('end_date')) ? $this->get('end_date') : null,
            'order_by'    => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['id', 'desc'],
            'customer_id' => $this->get('customer_id') ? $this->get('customer_id') : null,
            'customer'    => $this->get('customer') ? $this->get('customer') : null,
            'created_by'    => $this->get('created_by') ? $this->get('created_by') : null,
            'customer_type'    => $this->get('customer_type'),
            'order_status'    => $this->get('order_status') ? $this->get('order_status') : null,
            'order_platform'    => $this->get('order_platform') ? $this->get('order_platform') : null,
            'is_printed'    => $this->get('is_printed') ? $this->get('is_printed') : 0,
        ];

        if ($sales_id === null) {
            if ($sales = $this->sales_api->getSales($filters)) {
                $sl_data = [];
                foreach ($sales as $sale) {
                    if (!empty($filters['include'])) {
                        foreach ($filters['include'] as $include) {
                            if ($include == 'items') {
                                $sale->items = $this->sales_api->getSaleItems($sale->id);
                            }
                            if ($include == 'warehouse') {
                                $sale->warehouse = $this->sales_api->getWarehouseByID($sale->warehouse_id);
                            }
                            if ($include == 'biller') {
                                $sale->billerdetails = $this->sales_api->getBillerByID($sale->biller_id);
                            }

                            if ($include == 'payment') {
                                $sale->payments = $this->pos_model->getInvoicePayments($sale->id);
                            }
                            if ($include == 'customer') {
                            $sale->customerdetails = $this->sales_api->getBillerByID($sale->customer_id);
                            }
                            if ($include == 'printers') {
                            $sale->printers = $this->settings_api->getAllPrinters($pc_no);
                            }
                            if($include == 'restaurant_table' && $sale->table_no) {
                                 $sale->restaurant_table = $this->sales_api->getRestaurantTable($sale->table_no);
                            }
                        }
                    }

                    $sale->created_by = $this->sales_api->getUser($sale->created_by);
                    $sl_data[]        = $this->setSale($sale);
                }

                $data = [
                    'data'  => $sl_data,
                    'limit' => (int) $filters['limit'],
                    'start' => (int) $filters['start'],
                    'total' => $this->sales_api->countSales($filters),
                    'status' => true,
                    'message' => 'sale record found.',
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No sale record found.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            if ($sale = $this->sales_api->getSale($filters)) {
                if (!empty($filters['include'])) {
                    foreach ($filters['include'] as $include) {
                        if ($include == 'items') {
                            $sale->items = $this->sales_api->getSaleItems($sale->id);
                        }
                        if ($include == 'warehouse') {
                            $sale->warehouse = $this->sales_api->getWarehouseByID($sale->warehouse_id);
                        }
                        if ($include == 'biller') {
                            $sale->billerdetails = $this->sales_api->getBillerByID($sale->biller_id);
                        }
                        if ($include == 'printers') {
                            $sale->printers = $this->settings_api->getAllPrinters($pc_no);
                        }
                        if ($include == 'payment') {
                            $sale->payments = $this->pos_model->getInvoicePayments($sale->id);
                        }
                        if ($include == 'customer') {
                            $sale->customerdetails = $this->sales_api->getBillerByID($sale->customer_id);
                        }
                        if($include == 'restaurant_table' && $sale->table_no) {
                                 $sale->restaurant_table = $this->sales_api->getRestaurantTable($sale->table_no);
                            }
                    }
                     //$sale->payments = $this->pos_model->getInvoicePayments($sale->id);
                     $payments = $this->pos_model->getInvoicePayments($sale->id);
                     if($payments){
                        foreach ($payments as $payment) {
                            if($payment->paid_by){
                                $sale->payment_method = lang($payment->paid_by);
                                break;
                            }
                        }
                     }
                }

                $sale->created_by = $this->sales_api->getUser($sale->created_by);
                $sale             = $this->setSale($sale);
                $this->set_response($sale, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'message' => 'Sale could not be found for reference ' . $reference . '.',
                    'status'  => false,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function postsales_post()
    {

        $sid = null;
        //  $postdata=$this->post("api-key");
        //  $emailid=$this->post("emailid");
        //  $mobile= $this->post( 'mobile' );
         //$myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
        // $txt = "John Doe\n";
        //$txt = json_encode($this->post());
        // $txt .= "".$postdata."\n";
        // $txt .= "".$emailid."\n";
        // $txt .= "".$mobile."\n";
        //  fwrite($myfile, $txt);
        //  fclose($myfile);
        // $data = [
        //             'data'  => "aaaa",
        //             'limit' => "kkkka"
        //         ];
        // $this->response($data, REST_Controller::HTTP_OK);

        // post function starts

        $this->site->generateNewOrderCreatedjsonfiles(true, "all");
        if (!$this->pos_settings->default_biller || !$this->pos_settings->default_customer || !$this->pos_settings->default_category) {
             $this->set_response([
                    'message' => lang('please_update_settings'),
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
        }
        if ($register = $this->pos_model->registerData($this->post("user_id"))) {
            $register_data = ['register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date];
            $this->session->set_userdata($register_data);
        } else {
            $this->set_response([
                    'message' => lang('register_not_open'),
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
        }

        $this->data['sid'] = $this->post('suspend_id') ? $this->post('suspend_id') : $sid;
        $did               = $this->post('delete_id') ? $this->post('delete_id') : null;
        $suspend           = $this->post('suspend') ? true : false;
        $count             = $this->post('count') ? $this->post('count') : null;

        $duplicate_sale = $this->post('duplicate') ? $this->post('duplicate') : null;

        //validate form input
        // $this->form_validation->set_rules('customer', $this->lang->line('customer'), 'trim|required');
        // $this->form_validation->set_rules('warehouse', $this->lang->line('warehouse'), 'required');
        // $this->form_validation->set_rules('biller', $this->lang->line('biller'), 'required');
            $is_validation=false;
            $returnStatus=[];
            $returnStatus['status']=false;
            $pc_no="1";
            if($this->post('pc_no')){
                $pc_no=$this->post('pc_no');
            }
            if(empty($this->post('warehouse')) || empty($this->post('customer')) || empty($this->post('biller'))){
                $returnStatus['message']=lang('customer');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            }else{
//                    $returnStatus['message2']=lang('akhane asece 2');
//                 $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            $is_validation=true;
            $date             = $this->post('date') ? $this->post('date') : date('Y-m-d H:i:s');
            // $date             = $this->post('date');
            $warehouse_id     = $this->post('warehouse');
            $customer_id      = $this->post('customer');
            $biller_id        = $this->post('biller');
            $total_items      = $this->post('total_items');
            $sale_status      = 'completed';
            $payment_term     = 0;
            $due_date         = date('Y-m-d', strtotime('+' . $payment_term . ' days'));
            $shipping         = $this->post('shipping') ? $this->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer         = $customer_details->company && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
            $biller_details   = $this->site->getCompanyByID($biller_id);
            $biller           = $biller_details->company && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note             = $this->sma->clear_tags($this->post('pos_note'));
            $staff_note       = $this->sma->clear_tags($this->post('staff_note'));
            

            $total            = 0;
            $product_tax      = 0;
            $product_discount = 0;
            $digital          = false;
            $gst_data         = [];
            $total_cgst       = $total_sgst       = $total_igst       = 0;
            $product_details = $this->post( 'details' );
            $i                = count($product_details);
            for ($r = 0; $r < $i; $r++) {
                $item_type          = $product_details[$r]['product_type'];
                $item_code          = $product_details[$r]['product_code'];
                 $get_product_details = $item_type != 'manual' ? $this->pos_model->getProductByCode($item_code) : null;
                  $item_id            = $product_details[$r]['product_id'];
                 
                  
                  $item_name          = $get_product_details->name;
                  $item_name_alt      = $get_product_details->second_name;
                  $printer_selection  = $get_product_details->printer_selection;
                  $item_comment       = $product_details[$r]['product_comment'];
                 $item_option        = isset($product_details[$r]['product_option']) && $product_details[$r]['product_option'] != 'false' ? $product_details[$r]['product_option'] : null;
                 if($item_option !=null){
                    $optioninfo=$this->pos_model->getProductOptionByID($item_option);
                    $item_name          .=" (".$optioninfo->name.")";
                    $item_name_alt          .=" (".$optioninfo->name.")";
                 }
                 
                 if($product_details[$r]['is_product_name_custom']=="1" && $product_details[$r]['product_custom_name']){
                    $item_name          =$product_details[$r]['product_custom_name'];
                    $item_name_alt      =$product_details[$r]['product_custom_name'];
                 }
                  $real_unit_price    = $this->sma->formatDecimal($product_details[$r]['real_unit_price']);
                  $unit_price         = $this->sma->formatDecimal($product_details[$r]['unit_price']);
                  $item_unit_quantity = $product_details[$r]['quantity'];
                  $item_serial        = $product_details[$r]['serial']           ?? '';
                  $item_tax_rate      = $product_details[$r]['product_tax']      ?? null;
                  $item_discount      = $product_details[$r]['product_discount'] ?? null;
                  $item_unit          = $product_details[$r]['product_unit'];
                  $item_quantity      = $product_details[$r]['product_base_quantity'];

                 if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                //     $returnStatus[$item_code.'=100message'.$r]="message  jsdf=".$item_type;
                    //$this->pos_model->getProductByCode($item_code);
                   
                    if ($item_type == 'digital') {
                        $digital = true;
                    }
                    $pr_discount      = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price       = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price   = $unit_price;
                     $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                     $product_discount += $pr_item_discount;
                     $pr_item_tax = $item_tax = 0;
                     $tax         = '';

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax        = $this->site->calculateTax($get_product_details, $tax_details, $unit_price);
                        $item_tax    = $this->sma->formatDecimal($ctax['amount']);
                        $tax         = $ctax['tax'];
                        if (!$get_product_details || (!empty($get_product_details) && $get_product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculateIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                    $unit     = $this->site->getUnitByID($item_unit);

                    $product = [
                        'product_id'        => $item_id,
                        'product_code'      => $item_code,
                        'product_name'      => $item_name,
                        'name_alt'      => $item_name_alt,
                        'product_type'      => $item_type,
                        'option_id'         => $item_option,
                        'net_unit_price'    => $item_net_price,
                        'unit_price'        => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity'          => $item_quantity,
                        'product_unit_id'   => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity'     => $item_unit_quantity,
                        'warehouse_id'      => $warehouse_id,
                        'item_tax'          => $pr_item_tax,
                        'tax_rate_id'       => $item_tax_rate,
                        'tax'               => $tax,
                        'discount'          => $item_discount,
                        'item_discount'     => $pr_item_discount,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'serial_no'         => $item_serial,
                        'real_unit_price'   => $real_unit_price,
                        'comment'           => $item_comment,
                        'printer_selection' => $printer_selection,
                    ];

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
//            $returnStatus['message402']="thiii is message402";
//                 $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            if (empty($products)) {
                $returnStatus['message']=lang('order_items');
                $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            } elseif ($this->pos_settings->item_order == 0) {
//                 $returnStatus['message400']="thiii is message400";
//                 $this->set_response($returnStatus, REST_Controller::HTTP_OK);
                krsort($products);
            }
           // $returnStatus['message401']="thiii is message401";
             //    $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            $order_discount = $this->site->calculateDiscount($this->post('discount'), ($total + $product_tax), true);
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax      = $this->site->calculateOrderTax($this->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax      = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total    = $this->sma->formatDecimal(($this->sma->formatDecimal($total) + $this->sma->formatDecimal($total_tax) + $this->sma->formatDecimal($shipping) - $this->sma->formatDecimal($order_discount)), 4);
            $rounding       = 0;
            if ($this->pos_settings->rounding) {
                $round_total = $this->sma->roundNumber($grand_total, $this->pos_settings->rounding);
                $rounding    = $this->sma->formatMoney($round_total - $grand_total);
            }
            $data = ['date'         => $date,
                'customer_id'       => $customer_id,
                'customer'          => $customer,
                'biller_id'         => $biller_id,
                'biller'            => $biller,
                'warehouse_id'      => $warehouse_id,
                // 'note'              => $note."===".$this->post("user_id"),
                'note'              => $note,
                'staff_note'        => $staff_note,
                'total'             => $total,
                'product_discount'  => $product_discount,
                'order_discount_id' => $this->post('discount'),
                'order_discount'    => $order_discount,
                'total_discount'    => $total_discount,
                'product_tax'       => $product_tax,
                'order_tax_id'      => $this->post('order_tax'),
                'order_tax'         => $order_tax,
                'total_tax'         => $total_tax,
                'shipping'          => $this->sma->formatDecimal($shipping),
                'grand_total'       => $grand_total,
                'total_items'       => $total_items,
                'sale_status'       => $sale_status,
                'payment_status'    => $grand_total > 0 ? 'due' : 'paid',
                'payment_method'    => $this->post('paidby'),
                'payment_term'      => $payment_term,
                'rounding'          => $rounding,
                'suspend_note'      => $this->post('suspend_note'),
                'pos'               => 1,
                'paid'              => $this->post('amount-paid') ? $this->post('amount-paid') : 0,
                'created_by'        => $this->post("user_id"),
                'hash'              => hash('sha256', microtime() . mt_rand()),
                'customer_type'     => $this->post('is_dine_in') ? $this->post('is_dine_in') : 1, // 2 Table and 1= parcel
                'local_id'          => $this->post('local_id') ? $this->post('local_id') : null,
                'online_id'         => $this->post('online_id') ? $this->post('online_id') : null,
                'order_platform'    => $this->post('order_platform') ? $this->post('order_platform') : null,// Desktop, Online, Waiter, QR Code
                'table_no'          => $this->post('table_no') ? $this->post('table_no') : null,
                'is_syncronize'     => $this->post('is_syncronize') ? $this->post('is_syncronize') : 0,
                'is_printed'     => $this->post('is_printed') ? $this->post('is_printed') : 0,
            ];
            //$returnStatus['message403']="thiii is message403";
              //   $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if (!$suspend) {
                //$returnStatus['message404']="thiii is message404";
                 //$this->set_response($returnStatus, REST_Controller::HTTP_OK);
                $payment_details = $this->post( 'payment_details' );
                $p    = count($payment_details);
                $paid = 0;
                for ($r = 0; $r < $p; $r++) {
                    if (isset($payment_details[$r]['amount']) && !empty($payment_details[$r]['amount']) && isset($payment_details[$r]['paid_by']) && !empty($payment_details[$r]['paid_by'])) {
                        $amount = $this->sma->formatDecimal($payment_details[$r]['balance_amount'] > 0 ? $payment_details[$r]['amount'] - $payment_details[$r]['balance_amount'] : $payment_details[$r]['amount']);
                        if ($payment_details[$r]['paid_by'] == 'deposit') {
                            if (!$this->site->check_customer_deposit($customer_id, $amount)) {
                                $returnStatus['message']=lang('amount_greater_than_deposit');
                                 $this->set_response($returnStatus, REST_Controller::HTTP_OK);
                            }
                        }
                        if ($payment_details[$r]['paid_by'] == 'gift_card') {
                            $gc            = $this->site->getGiftCardByNO($payment_details[$r]['paying_gift_card_no']);
                            $amount_paying = $payment_details[$r]['amount'] >= $gc->balance ? $gc->balance : $payment_details[$r]['amount'];
                            $gc_balance    = $gc->balance - $amount_paying;
                            $payment[]     = [
                                'date' => $date,
                                // 'reference_no' => $this->site->getReference('pay'),
                                'amount'      => $amount,
                                'paid_by'     => $payment_details[$r]['paid_by'],
                                'cheque_no'   => $payment_details[$r]['cheque_no'],
                                'cc_no'       => $payment_details[$r]['paying_gift_card_no'],
                                'cc_holder'   => $payment_details[$r]['cc_holder'],
                                'cc_month'    => $payment_details[$r]['cc_month'],
                                'cc_year'     => $payment_details[$r]['cc_year'],
                                'cc_type'     => $payment_details[$r]['cc_type'],
                                // 'cc_cvv2'     => $payment_details[$r]['cc_cvv2'],
                                'created_by'  => $this->post("user_id"),
                                'type'        => 'received',
                                'note'        => $payment_details[$r]['payment_note'],
                                'pos_paid'    => $payment_details[$r]['amount'],
                                'pos_balance' => $payment_details[$r]['balance_amount'],
                                'gc_balance'  => $gc_balance,
                            ];
                        } else {
                            $payment[] = [
                                'date' => $date,
                                // 'reference_no' => $this->site->getReference('pay'),
                                'amount'      => $amount,
                                'paid_by'     => $payment_details[$r]['paid_by'],
                                'cheque_no'   => $payment_details[$r]['cheque_no'],
                                'cc_no'       => $payment_details[$r]['cc_no'],
                                'cc_holder'   => $payment_details[$r]['cc_holder'],
                                'cc_month'    => $payment_details[$r]['cc_month'],
                                'cc_year'     => $payment_details[$r]['cc_year'],
                                'cc_type'     => $payment_details[$r]['cc_type'],
                                //'cc_cvv2'     => $payment_details[$r]['cc_cvv2'],
                                'created_by'  => $this->post("user_id"),
                                'type'        => 'received',
                                'note'        => $payment_details[$r]['payment_note'],
                                'pos_paid'    => $payment_details[$r]['amount'],
                                'pos_balance' => $payment_details[$r]['balance_amount'],
                            ];
                        }
                    }
                }
            }
            if (!isset($payment) || empty($payment)) {
                $payment = [];
            }

//$returnStatus['message406']="thiii is message406";
                // $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
//$returnStatus['message407']="thiii is message407";
                // $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        if ($is_validation== true && !empty($products) && !empty($data)) {
            //$returnStatus['message408']="thiii is message408";
              //   $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            if ($suspend) {
              //  $returnStatus['message409']="thiii is message409";
                 $this->set_response($returnStatus, REST_Controller::HTTP_OK);
                if ($this->pos_model->suspendSale($data, $products, $did)) {
                    $this->session->set_userdata('remove_posls', 1);
                    // admin_redirect('pos');
                    //$returnStatus['message40010']=lang('sale_suspended');
                   // $returnStatus['message40010']="thiii is message40010";
                    $this->response($returnStatus, REST_Controller::HTTP_OK);
                }
            } else {
               // $returnStatus['message40019']="thiii is message40019";
                //$returnStatus['datadata']=$data;
                //$returnStatus['productsproducts']=$products;
                //$returnStatus['paymentpayment']=$payment;
                //$returnStatus['diddid']=$did;
                //$returnStatus['sale']=$this->pos_model->addSale($data, $products, $payment, $did);
                 //$this->set_response($returnStatus, REST_Controller::HTTP_OK);
                //  print_r($data); die;
                if ($sale = $this->pos_model->addSale($data, $products, $payment, $did, $pc_no)) {
                    //$returnStatus['message40020']="thiii is message40020";
                    $returnStatus['sale']=$sale;
//                    $returnStatus['payment']=$payment;
                    $returnStatus['status']=true;
                 $this->set_response($returnStatus, REST_Controller::HTTP_OK);
                    $this->session->set_userdata('remove_posls', 1);
                    $returnStatus['message']=lang('sale_added');
//                        $returnStatus['payment_details']=$payment;
                    $this->response($returnStatus, REST_Controller::HTTP_OK);
                } else {
                    $returnStatus['message']=lang('an_error_ocurred');
                    $returnStatus['status']=false;
                    $returnStatus['get_product_details']=$get_product_details;
                    $this->set_response($returnStatus, REST_Controller::HTTP_OK);
                } 
            }
         }else{
            $returnStatus['message']=lang('an_error_ocurred');
            $returnStatus['status']=false;
            //$returnStatus['products']=$products;
            $returnStatus['data']=$data;
            // $returnStatus['postdata']=$this->post();
            // $returnStatus['payment']=$payment;
           // $returnStatus['payment_details']=$this->post( 'payment_details' );
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } 
        // post function finished
    
    }

    public function add_payment_post()
    {
        
        
        if ($this->post('id')) {
            $id = $this->get('id');
        }
        $sale = $this->sales_model->getInvoiceByID($id);
        if ($sale->payment_status == 'paid' && $sale->grand_total == $sale->paid) {
            $returnStatus['message']=lang('sale_already_paid');
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }

        
        if ($this->post('amount-paid')) {
            $sale = $this->sales_model->getInvoiceByID($this->post('sale_id'));
            $customer_id = null;
            $date = date('Y-m-d H:i:s');
            $payment = [
                'date'         => $date,
                'sale_id'      => $this->post('sale_id'),
                'reference_no' => $this->post('reference_no') ? $this->post('reference_no') : $this->site->getReference('pay'),
                'amount'       => $this->post('amount-paid'),
                'paid_by'      => $this->post('paid_by'),
                'cheque_no'    => $this->post('cheque_no'),
                'cc_no'        => $this->post('pcc_no'),
                'cc_holder'    => $this->post('pcc_holder'),
                'cc_month'     => $this->post('pcc_month'),
                'cc_year'      => $this->post('pcc_year'),
                'cc_type'      => $this->post('pcc_type'),
                'note'         => $this->post('note'),
                'created_by'   => $this->post('user_id'),
                'type'         => $sale->sale_status == 'returned' ? 'returned' : 'received',
            ];

        if ($this->sales_model->addPayment($payment, $customer_id)) {
            $returnStatus['message']=lang('payment_added');
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
            $returnStatus['message']=lang('error');
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }

        } else {
            $returnStatus['message']=lang('amount_required');
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
        
    }
     public function changeorderstatus_post(){

        //error_reporting(0);

        $order_id=$this->post( 'id');
        $status_id=$this->post( 'status_id');
        $user_id=$this->post( 'user_id');
        $platform=$this->post( 'platform');
        //$this->custom_response( $this->post() ) ;
        $conds = array(
            "id" => $order_id,
            // "trans_status_id" => $trans_status_id
        );
        // print_r($order_id);exit();
        $orderdata = $this->sales_api->getSaleByID($conds);


        if(!$order_id || !$status_id || $orderdata->order_status==5){
            $returnStatus['message']='Order already completed';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
            // $this->error_response( get_msg( 'order_already_completed' ) );
            // exit;
        }
        
        //$this->db->trans_start();
        if($status_id && $order_id){
            
            if($platform =="kitchen" && $status_id==2){
                $this->orderStatusLoop($status_id, $order_id, $user_id, $orderdata);
            }else if($platform =="kitchen"){
                $status_id=5;
               $this->orderStatusLoop($status_id, $order_id, $user_id, $orderdata);
            }else{
                if ($orderdata->order_status && $status_id != 5) {
                $status_id = $orderdata->order_status + 1;
                if($status_id>4){
                    $status_id=5;
                }
                }
                $this->orderStatusLoop($status_id, $order_id, $user_id, $orderdata);
            }
        }

    }

    function orderStatusLoop($status_id, $order_id, $user_id, $orderdata){

        if(!$this->order_status_tracking_api->getStatus($status_id, $order_id)){
            $assign_to_data = array('order_status' => $status_id);
            $this->sales_api->update_sales($assign_to_data, $order_id);
            $status_data = array('order_id' => $order_id, 'status_id' => $status_id, 'created_by' => $user_id);
            // save data
            $this->order_status_tracking_api->save($status_data);

            $returnStatus['message']='Status updated!';
            $conds=array("id"=>$order_id);
             //$orderdata = $this->sales_api->getSaleByID($conds);
            $orderdata->order_status=$status_id;
             $returnStatus['data']=$orderdata;
             $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }else{

            $returnStatus['message']='Order already updated';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function neworderno_get(){
        $returnStatus['data']=$this->site->getReference('pos');
        $returnStatus['status']=true;
        $this->set_response($returnStatus, REST_Controller::HTTP_OK);
    }


    public function delete_sale_post()
    {

        if ($this->post( 'id')) {
            $id = $this->post( 'id');
        }

        $inv = $this->sales_model->getInvoiceByID($id);
        if ($inv->sale_status == 'returned') {
        $this->set_response(['error' => 1,'status' => true, 'msg' => lang('sale_x_action')], REST_Controller::HTTP_OK);
        }

        if ($this->sales_model->deleteSale($id)) {
            $this->set_response(['error' => 0,'status' => true, 'msg' => lang('sale_deleted')], REST_Controller::HTTP_OK);
        }
    }



    public function todayssales_get()
    {
        
        $user_id=$this->get('user_id');
        $pc_no=$this->get('pc_no') ? $this->get('pc_no') : 1;
        if(!$user_id){
            $this->response([
                'message' => 'No found.',
                'status'  => false,
            ], REST_Controller::HTTP_OK);
        }else{
             $registarDetails=$this->sales_api->getPosRegisterBalance($user_id);
             if($registarDetails){
             $register_open_time=$registarDetails->date;
            $ccsales        = $this->pos_model->getRegisterCCSales($register_open_time, $user_id);
            $cashsales      = $this->pos_model->getRegisterCashSales($register_open_time, $user_id);
            $chsales        = $this->pos_model->getRegisterChSales($register_open_time, $user_id);
            $gcsales        = $this->pos_model->getRegisterGCSales($register_open_time, $user_id);
            $pppsales       = $this->pos_model->getRegisterPPPSales($register_open_time, $user_id);
            $stripesales    = $this->pos_model->getRegisterStripeSales($register_open_time, $user_id);
            $othersales     = $this->pos_model->getRegisterOtherSales($register_open_time, $user_id);
            $authorizesales = $this->pos_model->getRegisterAuthorizeSales($register_open_time, $user_id);
            $totalsales     = $this->pos_model->getRegisterSales($register_open_time, $user_id);
            $refunds        = $this->pos_model->getRegisterRefunds($register_open_time, $user_id);
            $returns        = $this->pos_model->getRegisterReturns($register_open_time, $user_id);
            $expenses       = $this->pos_model->getRegisterExpenses($register_open_time, $user_id);

            $cashsalesAmount = $this->sma->formatMoney($cashsales->paid ? $cashsales->paid : '0.00') . ' (' . $this->sma->formatMoney($cashsales->total ? $cashsales->total : '0.00') . ')';
            $checkSales = $this->sma->formatMoney($chsales->paid ? $chsales->paid : '0.00') . ' (' . $this->sma->formatMoney($chsales->total ? $chsales->total : '0.00') . ')';
            $cc_sales = $this->sma->formatMoney($ccsales->paid ? $ccsales->paid : '0.00') . ' (' . $this->sma->formatMoney($ccsales->total ? $ccsales->total : '0.00') . ')';
            $gc_sales=$this->sma->formatMoney($gcsales->paid ? $gcsales->paid : '0.00') . ' (' . $this->sma->formatMoney($gcsales->total ? $gcsales->total : '0.00') . ')';
            $other_sales=$this->sma->formatMoney($othersales->paid ? $othersales->paid : '0.00') . ' (' . $this->sma->formatMoney($othersales->total ? $othersales->total : '0.00') . ')';
            //$paypal_sales=$this->sma->formatMoney($pppsales->paid ? $pppsales->paid : '0.00') . ' (' . $this->sma->formatMoney($pppsales->total ? $pppsales->total : '0.00') . ')';
           // $stripe_sale = $this->sma->formatMoney($stripesales->paid ? $stripesales->paid : '0.00') . ' (' . $this->sma->formatMoney($stripesales->total ? $stripesales->total : '0.00') . ')';
            $total_saless = $this->sma->formatMoney($totalsales->paid ? $totalsales->paid : '0.00') . ' (' . $this->sma->formatMoney($totalsales->total ? $totalsales->total : '0.00') . ')';
            $refund_amount = $this->sma->formatMoney($refunds->returned ? $refunds->returned : '0.00') . ' (' . $this->sma->formatMoney($refunds->total ? $refunds->total : '0.00') . ')';
            $return = $this->sma->formatMoney($returns->total ? '-' . $returns->total : '0.00');
            $expense = $expenses ? $expenses->total : 0; 
            $expense_amont=$this->sma->formatMoney($expense) . ' (' . $this->sma->formatMoney($expense) . ')';
            $total_cash_amount = $cashsales->paid ? (($cashsales->paid + ($registarDetails->cash_in_hand)) + ($refunds->returned ? $refunds->returned : 0) - ($returns->total ? $returns->total : 0) - $expense) : ($registarDetails->cash_in_hand - $expense - ($returns->total ? $returns->total : 0));
            $data= array(
                'cash_in_hand' => $registarDetails->cash_in_hand, 
                'register_id' => $registarDetails->id, 
                'ccsales' => $cc_sales,
                'total_cc_slips' => $ccsales->total_cc_slips,
                'cashsales' => $cashsalesAmount,
                'chsales' => $checkSales,
                'gcsales' => $gc_sales,
                // 'pppsales' => $paypal_sales,
                // 'stripesales' => $stripe_sale,
                'othersales' => $other_sales,
                // 'authorizesales' => $authorizesales,
                'totalsales' => $total_saless,
                'refunds' => $refund_amount,
                'cashrefunds' => $return,
                'expenses' => $expense_amont,
                'total_cash_amount' => "".$total_cash_amount."",
                'register_open_time' => $register_open_time,
                'register_close_time' => date('Y-m-d H:i:s'),
                'printers' => $this->settings_api->getAllPrinters($pc_no),
            );
        $this->response([
            'data' => $data,
            'message' => 'No found.',
            'status'  => false,
        ], REST_Controller::HTTP_OK);
    }else{
        $this->response([
            'message' => 'No found.',
            'status'  => false,
        ], REST_Controller::HTTP_OK);
    }
        }
    }

    public function view_get()
    {
        $sale_id=$this->get('sale_id');
        $api_key=$this->get('api-key');
        $platform=$this->get('platform');
        $this->load->helper('pos');

        $this->load->library('inv_qrcode');
        
        $this->data['Settings'] = $this->site->get_setting();
        $this->data['message'] = '';
        if($platform == "offline" || $platform == "desktop"){
            $inv = $this->pos_model->getInvoiceByOfflineID($sale_id);
        }else{
            $inv = $this->pos_model->getInvoiceByID($sale_id);
        }
        
        $this->data['rows']            = $this->pos_model->getAllInvoiceItems($inv->id);
        $biller_id                     = $inv->biller_id;
        $customer_id                   = $inv->customer_id;
        $this->data['biller']          = $this->pos_model->getCompanyByID($biller_id);
        $this->data['customer']        = $this->pos_model->getCompanyByID($customer_id);
        $this->data['payments']        = $this->pos_model->getInvoicePayments($inv->id);
        $this->data['pos']             = $this->pos_model->getSetting();
        // $this->data['barcode']         = $this->barcode($inv->id, $platform, $api_key, 'code128', 30);
        $this->data['return_sale']     = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : null;
        $this->data['return_rows']     = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : null;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : null;
        $this->data['inv']             = $inv;
        $this->data['sid']             = $inv->id;
        $this->data['api_key']         = $api_key;
        $this->data['platform']        = $platform;
        $this->data['modal']           = null;
        $this->data['created_by']      = $this->site->getUser($inv->created_by);
        $this->data['printer']         = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['page_title']      = $this->lang->line('invoice');
        $this->data['assets'] = base_url() . 'themes/default/admin/assets/';

        // TLV QR Code
        $this->load->library('generate_qr'); 
        $this->load->library('tags/Seller'); 
        $this->load->library('tags/TaxNumber'); 
        $this->load->library('tags/InvoiceDate'); 
        $this->load->library('tags/InvoiceTotalAmount'); 
        $this->load->library('tags/InvoiceTaxAmount'); 
        $this->load->library('tag'); 

        $tax_number = $this->pos_settings->cf_value1;
        $date_format = explode(' ', $inv->date);
        $inv_date = $date_format[0] . 'T' . $date_format[1] . 'Z';
        $grand_total = number_format($inv->grand_total, 2, '.', '');
        $total_tax = number_format($inv->total_tax, 2, '.', '');

        $generatedString = $this->generate_qr->encode(
            new Seller($inv->biller), 
            new TaxNumber($tax_number), 
            new invoiceDate($inv_date),
            new InvoiceTotalAmount($grand_total), 
            new InvoiceTaxAmount($total_tax) 
        );        

        $qr_code = $this->sma->tlv_code($generatedString);
        // ./TLV QR Code
        
        // echo '<pre>'; print_r($generatedString); die;
        $this->data['qr_code']  =  $qr_code;

        $this->theme = $this->Settings->theme . '/admin/views/';
        $this->load->view($this->theme . 'pos/view_api', $this->data);

    }
    public function barcode($sale_id, $platform, $api_key, $bcs = 'code128', $height = 50)
    {
        return admin_url('products/gen_barcode/' . $text . '/' . $bcs . '/' . $height);
    }


     public function suggestions($pos = 0)
    {
        $term         = $this->get('term', true);
        $warehouse_id = $this->get('warehouse_id', true);
        $customer_id  = $this->get('customer_id', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed  = $this->sma->analyze_term($term);
        $sr        = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $sr        = addslashes($sr);
        $qty       = $analyzed['quantity'] ?? null;
        $bprice    = $analyzed['price']    ?? null;

        $warehouse      = $this->site->getWarehouseByID($warehouse_id);
        $customer       = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows           = $this->sales_model->getProductNames($sr, $warehouse_id, $pos);

        if ($rows) {

            $r = 0;
            foreach ($rows as $row) {
                $c = uniqid(mt_rand(), true);
                if($row->is_packed =='1'){
                    if($row->packed_product){
                        $row->child_product=$this->getChildProduct($row->packed_product, $warehouse_id, $customer_id, $term, $r, $row->pack_piece);
                    }else{
                        $row->is_packed=0;
                    }


                }
                unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option               = false;
                $row->quantity        = 0;
                $row->item_tax_method = $row->tax_method;
                $row->qty             = 1;
                $row->discount        = '0';
                $row->serial          = '';

                $options              = $this->sales_model->getProductOptions($row->id, $warehouse_id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt        = json_decode('{}');
                    $opt->price = 0;
                    $option_id  = false;
                }
                $row->option = $option_id;
                $pis         = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                if ($pis) {
                    $row->quantity = 0;
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                if ($options) {
                    $option_quantity = 0;
                    foreach ($options as $option) {
                        $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                        if ($pis) {
                            foreach ($pis as $pi) {
                                $option_quantity += $pi->quantity_balance;
                            }
                        }
                        if ($option->quantity > $option_quantity) {
                            $option->quantity = $option_quantity;
                        }
                    }
                }
                if ($this->sma->isPromo($row)) {
                    $row->price = $row->promo_price;
                } elseif ($customer->price_group_id) {
                    if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer->price_group_id)) {
                        $row->price = $pr_group_price->price;
                    }
                } elseif ($warehouse->price_group_id) {
                    if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $warehouse->price_group_id)) {
                        $row->price = $pr_group_price->price;
                    }
                }
                if ($customer_group->discount && $customer_group->percent < 0) {
                    $row->discount = (0 - $customer_group->percent) . '%';
                } else {
                    $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                }
                $row->real_unit_price = $row->price;
                $row->base_quantity   = 1;
                $row->base_unit       = $row->unit;
                $row->base_unit_price = $row->price;
                $row->unit            = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->comment         = '';
                $combo_items          = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                }
                $row->qty = $qty ? $qty : ($bprice ? $bprice / $row->price : 1);
                $units    = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);

                $pr[] = ['id' => sha1($c . $r), 'item_id' => $row->id, 'label' => $row->name . ' (' . $row->code . ')', 'category' => $row->category_id,
                    'row'     => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, ];
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json([['id' => 0, 'label' => lang('no_match_found'), 'value' => $term]]);
        }
    }

    public function updateisprinted_get()
    {
        $sale_id=$this->get('sale_id');
        $api_key=$this->get('api-key');
        $data = array('is_printed' => 0);
        if($sale_id){
            $this->sales_api->update_sales($data, $sale_id);
        }
    }
}

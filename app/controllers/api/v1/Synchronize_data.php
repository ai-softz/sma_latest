<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Synchronize_data extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('sales_api');
        $this->load->api_model('settings_api');
        $this->load->api_model('order_status_tracking_api');
        $this->load->api_model('purchases_api');
        $this->load->admin_model('purchases_model');

        $this->load->admin_model('pos_model');
        $this->load->admin_model('sales_model');
        $this->load->helper('text');
        $this->pos_settings           = $this->pos_model->getSetting();
        $this->pos_settings->pin_code = $this->pos_settings->pin_code ? md5($this->pos_settings->pin_code) : null;
        $this->data['pos_settings']   = $this->pos_settings;
        // $this->load->api_model('sales_api');

	}

    public function allusers_get() {
        $users = $this->db->get_where('users')->result();
        if($users) {
            $returnStatus['records_online']=$users;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
             $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

	public function xallusers_get() {

    	$users = $this->db->get_where('users', array('is_synchronize' => 0))->result();
    	foreach($users as $user) {
    		$this->db->where('id', $user->id)->set('is_synchronize',1)->update('users');
    	}
        if($users) {
            $returnStatus['records_online']=$users;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
        	 $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function allbillers_get() {
        $records = $this->db->get_where('companies', array('group_name'=>'biller'))->result();
        if($records) {
            $returnStatus['records_online']=$records;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
             $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function allsuppliers_get() {
        $records = $this->db->get_where('companies', array('group_name'=>'supplier'))->result();
        if($records) {
            $returnStatus['records_online']=$records;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
             $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function allcustomers_get() {
        $records = $this->db->get_where('companies', array('group_name'=>'customer'))->result();
        if($records) {
            $returnStatus['records_online']=$records;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
             $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function allcategories_get() {
        $records = $this->db->get_where('categories')->result();
        if($records) {
            $returnStatus['records_online']=$records;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
             $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function allproducts_get() {
        $records = $this->db->get('products')->result_array();

        $records_array = array();
        foreach($records as $key => $record) {
            
            $product_variants = $this->db->get_where('product_variants', array('product_id' => $record['id']))->result_array();
            $records_array[$key] = $record;
            $records_array[$key]['product_variants'] = $product_variants; 
        }

        if($records_array) {
            $returnStatus['records_online']=$records_array;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
             $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function allexpenses_get() {
        $records = $this->db->get_where('expenses', array('is_synchronize'=>0))->result();
        foreach($records as $record) {
            $this->db->where('id', $record->id)->set('is_synchronize',1)->update('expenses');
        }
        if($records) {
            $returnStatus['records_online']=$records;
            $returnStatus['status']=true;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else {
             $returnStatus['records_online']='';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    // public function allpurchases_get() {

    //     $records = $this->db->get_where('products', array('is_synchronize' => 0))->result();
    //     foreach($records as $record) {
    //         $this->db->where('id', $record->id)->set('is_synchronize',1)->update('products');
    //     }
    //     if($records) {
    //         $returnStatus['records_online']=$records;
    //         $returnStatus['status']=true;
    //         $this->set_response($returnStatus, REST_Controller::HTTP_OK);
    //     } else {
    //         $returnStatus['records_online']='';
    //         $returnStatus['status']=false;
    //         $this->set_response($returnStatus, REST_Controller::HTTP_OK);
    //     }
    // }

  protected function setSale($sale)
    {
        // unset($sale->address_id, $sale->api, $sale->attachment, $sale->hash, $sale->pos, $sale->reserve_id, $sale->return_id, $sale->return_sale_ref, $sale->return_sale_total, $sale->sale_id, $sale->shop, $sale->staff_note, $sale->surcharge, $sale->updated_at, $sale->suspend_note);
        if (isset($sale->items) && !empty($sale->items)) {
            foreach ($sale->items as &$item) {
                if (isset($item->option_id) && !empty($item->option_id)) {
                    if ($variant = $this->sales_api->getProductVariantByID($item->option_id)) {
                        $item->product_variant_id   = $variant->id;
                        $item->product_variant_name = $variant->name;
                    }
                }
                $item->product_unit_quantity = $item->unit_quantity;
                unset($item->id, $item->sale_id, $item->warehouse_id, $item->real_unit_price, $item->sale_item_id, $item->option_id, $item->unit_quantity);
                $item = (array) $item;
                ksort($item);
            }
        }
        $sale = (array) $sale;
        ksort($sale);
        return $sale;
    }
// http://localhost/retaurant/api/v1/synchronize_data/allsales?start=0&limit=15&api-key=ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw&include=payment,items,warehouse,biller
    public function allsales_get()
    {
        
        $reference = $this->get('reference');
        $sales_id = $this->get('sales_id');
        $pc_no = $this->get('pc_no') ? $this->get('pc_no'): 1;

        $filters = [
            'reference'   => $reference,
            'sales_id'   => $sales_id,
            'include'     => $this->get('include') ? explode(',', $this->get('include')) : null,
            'start'       => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
            'limit'       => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
            'start_date'  => $this->get('start_date') && is_numeric($this->get('start_date')) ? $this->get('start_date') : null,
            'end_date'    => $this->get('end_date') && is_numeric($this->get('end_date')) ? $this->get('end_date') : null,
            'order_by'    => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['id', 'asc'],
            'customer_id' => $this->get('customer_id') ? $this->get('customer_id') : null,
            'customer'    => $this->get('customer') ? $this->get('customer') : null,
            'created_by'    => $this->get('created_by') ? $this->get('created_by') : null,
            'customer_type'    => $this->get('customer_type'),
            'order_status'    => $this->get('order_status') ? $this->get('order_status') : null,
            'order_platform'    => $this->get('order_platform') ? $this->get('order_platform') : null,
            'is_synchronize'  => $this->get('is_synchronize') ? $this->get('is_synchronize') : 0,

        ];

        if ($sales_id === null) {
            if ($sales = $this->sales_api->getSalesSync($filters)) {
                $sl_data = [];
                foreach ($sales as $sale) {
                    // Update is_syncronize = 1
                    $this->db->where('id', $sale->id)->set('is_syncronize', 1)->update('sales');

                    if (!empty($filters['include'])) {
                        foreach ($filters['include'] as $include) {
                            if ($include == 'items') {
                                $sale->items = $this->sales_api->getSaleItems($sale->id);
                            }
                            if ($include == 'warehouse') {
                                $sale->warehouse = $this->sales_api->getWarehouseByID($sale->warehouse_id);
                            }

                            if ($include == 'biller') {
                                $sale->billerdetails = $this->sales_api->getBillerByID($sale->biller_id);
                            }

                            if ($include == 'payment') {
                                $sale->payments = $this->pos_model->getInvoicePayments($sale->id);
                            }

                            if($include == 'restaurant_table' && $sale->table_no) {
                                 $sale->restaurant_table = $this->sales_api->getRestaurantTable($sale->table_no);
                            }
                        }
                    }

                    $sale->created_by = $this->sales_api->getUser($sale->created_by);
                    $sl_data[]        = $this->setSale($sale);
                }

                $data = [
                    'data'  => $sl_data,
                    'limit' => (int) $filters['limit'],
                    'start' => (int) $filters['start'],
                    'total' => $this->sales_api->countSales($filters),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No sale record found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
        } else {
            if ($sale = $this->sales_api->getSale($filters)) {
                if (!empty($filters['include'])) {
                    foreach ($filters['include'] as $include) {
                        if ($include == 'items') {
                            $sale->items = $this->sales_api->getSaleItems($sale->id);
                        }
                        if ($include == 'warehouse') {
                            $sale->warehouse = $this->sales_api->getWarehouseByID($sale->warehouse_id);
                        }
                        if ($include == 'biller') {
                            $sale->billerdetails = $this->sales_api->getBillerByID($sale->biller_id);
                        }
                        if ($include == 'printers') {
                            $sale->printers = $this->settings_api->getAllPrinters($pc_no);
                        }
                        if ($include == 'payment') {
                            $sale->payments = $this->pos_model->getInvoicePayments($sale->id);
                        }
                        if($include == 'restaurant_table' && $sale->table_no) {
                                 $sale->restaurant_table = $this->sales_api->getRestaurantTable($sale->table_no);
                            }
                    }
                     //$sale->payments = $this->pos_model->getInvoicePayments($sale->id);
                     $payments = $this->pos_model->getInvoicePayments($sale->id);
                     if($payments){
                        foreach ($payments as $payment) {
                            if($payment->paid_by){
                                $sale->payment_method = lang($payment->paid_by);
                                break;
                            }
                        }
                     }
                }

                $sale->created_by = $this->sales_api->getUser($sale->created_by);
                $sale             = $this->setSale($sale);
                $this->set_response($sale, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'message' => 'Sale could not be found for reference ' . $reference . '.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    // http://localhost/retaurant/api/v1/synchronize_data/allpurchases?start=0&limit=15&api-key=ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw&include=items,warehouse
    public function allpurchases_get()
    {

        $reference = $this->get('reference');

        $filters = [
            'reference'   => $reference,
            'include'     => $this->get('include') ? explode(',', $this->get('include')) : null,
            'start'       => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
            'limit'       => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
            'start_date'  => $this->get('start_date') && is_numeric($this->get('start_date')) ? $this->get('start_date') : null,
            'end_date'    => $this->get('end_date') && is_numeric($this->get('end_date')) ? $this->get('end_date') : null,
            'order_by'    => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['id', 'decs'],
            'supplier_id' => $this->get('supplier_id') ? $this->get('supplier_id') : null,
            'customer'    => $this->get('customer') ? $this->get('customer') : null,
            'is_synchronize'  => $this->get('is_synchronize') ? $this->get('is_synchronize') : 0,
        ];

        if ($reference === null) {
            if ($purchases = $this->purchases_api->getPurchases($filters)) {
                // print_r($purchases);die;
                $sl_data = [];
                foreach ($purchases as $purchase) {
                    if (!empty($filters['include'])) {
                        foreach ($filters['include'] as $include) {
                            if ($include == 'items') {
                                $purchase->items = $this->purchases_api->getPurchaseItems($purchase->id);
                            }
                            if ($include == 'warehouse') {
                                $purchase->warehouse = $this->purchases_api->getWarehouseByID($purchase->warehouse_id);
                            }
                            if ($include == 'payment') {
                                $purchase->payments = $this->purchases_api->getPurchasePayments($purchase->id);
                                //print_r($payments);die;
                            }
                        }
                    }

                    $purchase->created_by = $this->purchases_api->getUser($purchase->created_by);
                    $sl_data[]            = $this->setPurchase($purchase);
                    // update is_sync
                    $update_is_sync = $this->purchases_api->update_is_sync($purchase->id);
                }

                $data = [
                    'data'  => $sl_data,
                    'limit' => (int) $filters['limit'],
                    'start' => (int) $filters['start'],
                    'total' => $this->purchases_api->countPurchases($filters),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No purchase record found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
        } else {
            if ($purchase = $this->purchases_api->getPurchase($filters)) {
                if (!empty($filters['include'])) {
                    foreach ($filters['include'] as $include) {
                        if ($include == 'items') {
                            $purchase->items = $this->purchases_api->getPurchaseItems($purchase->id);
                        }
                        if ($include == 'warehouse') {
                            $purchase->warehouse = $this->purchases_api->getWarehouseByID($purchase->warehouse_id);
                        }
                        if ($include == 'payment') {
                            $purchase->payments = $this->purchases_api->getPurchasePayments($purchase->id);
                            //print_r($payments);die;
                        }
                    }
                }

                $purchase->created_by = $this->purchases_api->getUser($purchase->created_by);
                $purchase             = $this->setPurchase($purchase);
                $this->set_response($purchase, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'message' => 'Purchase could not be found for reference ' . $reference . '.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    protected function setPurchase($purchase)
    {
        //unset($purchase->attachment, $purchase->updated_at, $purchase->purchase_id, $purchase->return_id, $purchase->return_purchase_ref, $purchase->return_purchase_total);
        if (isset($purchase->items) && !empty($purchase->items)) {
            foreach ($purchase->items as &$item) {
                if (isset($item->option_id) && !empty($item->option_id)) {
                    if ($variant = $this->purchases_api->getProductVariantByID($item->option_id)) {
                        $item->product_variant_id   = $variant->id;
                        $item->product_variant_name = $variant->name;
                    }
                }
                $item->product_unit_quantity = $item->unit_quantity;
                //unset($item->id, $item->date, $item->transfer_id, $item->quantity_balance, $item->quantity_received, $item->purchase_id, $item->warehouse_id, $item->real_unit_cost, $item->supplier_part_no, $item->purchase_item_id, $item->option_id, $item->unit_quantity);
                $item = (array) $item;
                ksort($item);
            }
        }
        $purchase = (array) $purchase;
        ksort($purchase);
        return $purchase;
    }

}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class System_settings extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->methods['index_get']['limit'] = 500;
        $this->load->api_model('settings_api');
        $this->load->api_model('sales_api');
        $this->load->admin_model('pos_model');
    }


    public function index_get()
    {
        
    }

     public function tax_rates_get()
    {
        if ($taxes = $this->settings_api->getAllTaxRates()) {
                $pr_data = $taxes;
                

                $data = [
                    'data'  => $pr_data,
                    'limit' => 10,
                    'start' => 0,
                    'total' => count($pr_data),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No Tax were found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
    }

    public function categories_get()
    {
        
         if ($categories = $this->settings_api->getAllCategories()) {
                $pr_data = $categories;
                
                $data = [
                    'data'  => $pr_data,
                    'limit' => 20,
                    'start' => 0,
                    'total' => count($pr_data),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
    }


    public function printers_get()
    {
        
         if ($printers = $this->settings_api->getAllPrinters("1")) {
                $pr_data = $printers;
                

                $data = [
                    'data'  => $pr_data,
                    'limit' => 20,
                    'start' => 0,
                    'total' => count($pr_data),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
    }


    public function restauranttables_get()
    {
        
         if ($tables = $this->settings_api->getAllTables()) {
                $pr_data = $tables;
                

                $data = [
                    'data'  => $pr_data,
                    'limit' => 20,
                    'start' => 0,
                    'total' => count($pr_data),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
    }



     public function registerdetails_get()
    {
        $user_id=$this->get('user_id');
        $pc_no=$this->get('pc_no') ? $this->get('pc_no') : 1;
        if(!$user_id){
            $this->response([
                'message' => 'No found.',
                'status'  => false,
            ], REST_Controller::HTTP_OK);
        }else{
            $registarDetails=$this->sales_api->getPosRegisterBalance($user_id);
             if($registarDetails){
             $register_open_time=$registarDetails->date;
            $ccsales        = $this->pos_model->getRegisterCCSales($register_open_time, $user_id);
            $cashsales      = $this->pos_model->getRegisterCashSales($register_open_time, $user_id);
            $chsales        = $this->pos_model->getRegisterChSales($register_open_time, $user_id);
            $gcsales        = $this->pos_model->getRegisterGCSales($register_open_time, $user_id);
            $pppsales       = $this->pos_model->getRegisterPPPSales($register_open_time, $user_id);
            $stripesales    = $this->pos_model->getRegisterStripeSales($register_open_time, $user_id);
            $othersales     = $this->pos_model->getRegisterOtherSales($register_open_time, $user_id);
            $authorizesales = $this->pos_model->getRegisterAuthorizeSales($register_open_time, $user_id);
            $totalsales     = $this->pos_model->getRegisterSales($register_open_time, $user_id);
            $refunds        = $this->pos_model->getRegisterRefunds($register_open_time, $user_id);
            $returns        = $this->pos_model->getRegisterReturns($register_open_time, $user_id);
            $expenses       = $this->pos_model->getRegisterExpenses($register_open_time, $user_id);

            $cashsalesAmount = $this->sma->formatMoney($cashsales->paid ? $cashsales->paid : '0.00') . ' (' . $this->sma->formatMoney($cashsales->total ? $cashsales->total : '0.00') . ')';
            $checkSales = $this->sma->formatMoney($chsales->paid ? $chsales->paid : '0.00') . ' (' . $this->sma->formatMoney($chsales->total ? $chsales->total : '0.00') . ')';
            $cc_sales = $this->sma->formatMoney($ccsales->paid ? $ccsales->paid : '0.00') . ' (' . $this->sma->formatMoney($ccsales->total ? $ccsales->total : '0.00') . ')';
            $gc_sales=$this->sma->formatMoney($gcsales->paid ? $gcsales->paid : '0.00') . ' (' . $this->sma->formatMoney($gcsales->total ? $gcsales->total : '0.00') . ')';
            $other_sales=$this->sma->formatMoney($othersales->paid ? $othersales->paid : '0.00') . ' (' . $this->sma->formatMoney($othersales->total ? $othersales->total : '0.00') . ')';
            //$paypal_sales=$this->sma->formatMoney($pppsales->paid ? $pppsales->paid : '0.00') . ' (' . $this->sma->formatMoney($pppsales->total ? $pppsales->total : '0.00') . ')';
           // $stripe_sale = $this->sma->formatMoney($stripesales->paid ? $stripesales->paid : '0.00') . ' (' . $this->sma->formatMoney($stripesales->total ? $stripesales->total : '0.00') . ')';
            $total_saless = $this->sma->formatMoney($totalsales->paid ? $totalsales->paid : '0.00') . ' (' . $this->sma->formatMoney($totalsales->total ? $totalsales->total : '0.00') . ')';
            $refund_amount = $this->sma->formatMoney($refunds->returned ? $refunds->returned : '0.00') . ' (' . $this->sma->formatMoney($refunds->total ? $refunds->total : '0.00') . ')';
            $return = $this->sma->formatMoney($returns->total ? '-' . $returns->total : '0.00');
            $expense = $expenses ? $expenses->total : 0; 
            $expense_amont=$this->sma->formatMoney($expense) . ' (' . $this->sma->formatMoney($expense) . ')';
            $total_cash_amount = $cashsales->paid ? (($cashsales->paid + ($registarDetails->cash_in_hand)) + ($refunds->returned ? $refunds->returned : 0) - ($returns->total ? $returns->total : 0) - $expense) : ($registarDetails->cash_in_hand - $expense - ($returns->total ? $returns->total : 0));
            $total_cc_slips=$this->sma->formatMoney($ccsales->total_cc_slips ? $ccsales->total_cc_slips : '0.00');
            $data= array(
                'cash_in_hand' => $registarDetails->cash_in_hand, 
                'register_id' => $registarDetails->id, 
                'ccsales' => $cc_sales,
                'total_cc_slips' => $total_cc_slips,
                'cashsales' => $cashsalesAmount,
                'chsales' => $checkSales,
                'gcsales' => $gc_sales,
                // 'pppsales' => $paypal_sales,
                // 'stripesales' => $stripe_sale,
                'othersales' => $other_sales,
                // 'authorizesales' => $authorizesales,
                'totalsales' => $total_saless,
                'refunds' => $refund_amount,
                'cashrefunds' => $return,
                'expenses' => $expense_amont,
                'total_cash_amount' => $total_cash_amount,
                'register_open_time' => $register_open_time,
                'register_close_time' => date('Y-m-d H:i:s'),
                'printers' => $this->settings_api->getAllPrinters($pc_no),
            );
        $this->response([
            'data' => $data,
            'message' => 'No found.',
            'status'  => false,
        ], REST_Controller::HTTP_OK);
    }else{
        $this->response([
            'message' => 'No found.',
            'status'  => false,
        ], REST_Controller::HTTP_OK);
    }
    }
    }



    public function closeregister_post()
    {
        
            $user_id = $this->post("user_id");
            
                $user_register = $user_id ? $this->pos_model->registerData($user_id) : null;
                $rid           = $user_register ? $user_register->id : $this->post("register_id");
                $user_id       = $user_register ? $user_register->user_id : $user_id;
            
            $data = [
                'closed_at'                => date('Y-m-d H:i:s'),
                'total_cash'               => $this->post('total_cash'),
                'total_cheques'            => $this->post('total_cheques'),
                'total_cc_slips'           => $this->post('total_cc_slips'),
                'total_cash_submitted'     => $this->post('total_cash_submitted'),
                'total_cheques_submitted'  => $this->post('total_cheques_submitted'),
                'total_cc_slips_submitted' => $this->post('total_cc_slips_submitted'),
                'note'                     => $this->post('note'),
                'status'                   => 'close',
                'transfer_opened_bills'    => $this->post('transfer_opened_bills'),
                'closed_by'                => $user_id,
            ];
        

        if ($this->pos_model->closeRegister($rid, $user_id, $data)) {
            $this->resetOrderRef();
           $this->response([
            'message' => 'Success',
            'status'  => true,
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
            'message' => 'Not Done.',
            'status'  => false,
            ], REST_Controller::HTTP_OK);
        }
    }

    public function resetOrderRef()
    {
        $reset_ref = ['pos' => 1];
        $this->db->update('order_ref', $reset_ref, ['ref_id' => 1]);
        return true;
    }

    public function checkOpenRegister_get()
    {
        $user_id=$this->get('user_id');
        if ($register = $this->pos_model->registerData($user_id)) {
           $this->response([
            'data' => $register,
            'message' => 'Success',
            'status'  => true,
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
            'message' => 'Not Found.',
            'status'  => false,
            ], REST_Controller::HTTP_OK);
        }
    }

    public function open_register_post()
    {
        
        $user_id = $this->post("user_id");
        if ($register = $this->pos_model->registerData($user_id)) {
           $this->response([
            'message' => 'Already Has open.',
            'status'  => false,
            ], REST_Controller::HTTP_OK);
        }else{
            $data = [
                'date'         => date('Y-m-d H:i:s'),
                'cash_in_hand' => $this->post('cash_in_hand'),
                'user_id'      => $user_id,
                'status'       => 'open',
            ];

            if ($this->pos_model->openRegister($data)) {
               $this->response([
                'message' => 'Success',
                'status'  => true,
                ], REST_Controller::HTTP_OK);
            } else {
               $this->response([
                'message' => 'Not Done.',
                'status'  => false,
                ], REST_Controller::HTTP_OK);
            }

        }
    }

    public function expenses_category_get()
    {
        
         if ($categories = $this->settings_api->getAllExpensesCategories()) {
                $pr_data = $categories;
                

                $data = [
                    'data'  => $pr_data,
                    'limit' => 20,
                    'start' => 0,
                    'total' => count($pr_data),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
    }
}

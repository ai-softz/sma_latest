<?php

// API link http://localhost/retaurant/api/v1/tasks/changecheckliststatus

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Tasks extends REST_Controller
{
    public function __construct()
    {
    	parent::__construct();
        $this->load->api_model('tasks_api');
    }

    public function index_get()
    {
        
        $filters = [
            'start'       => $this->get('start') && is_numeric($this->get('start')) ? $this->get('start') : 1,
            'limit'       => $this->get('limit') && is_numeric($this->get('limit')) ? $this->get('limit') : 10,
            'startdate'  => $this->get('startdate') ? $this->get('startdate') : null,
           'duedate'    => $this->get('duedate') ? $this->get('duedate') : null,
            'order_by'    => $this->get('order_by') ? explode(',', $this->get('order_by')) : ['id', 'desc'],
            'status' => $this->get('status') ? $this->get('status') : null,
            'priority' => $this->get('priority') ? $this->get('priority') : null,
            'staffid' => $this->get('staffid') ? $this->get('staffid') : null,
            'is_notified' => $this->get('is_notified') ? $this->get('is_notified') : null,
            'task_id' => $this->get('task_id') ? $this->get('task_id') : null,
            
        ];

            if ($tasks = $this->tasks_api->getTasks($filters)) {
                $data = [
                    'data'  => $tasks,
                    'limit' => (int) $filters['limit'],
                    'start' => (int) $filters['start'],
                    'total' => $this->tasks_api->countTasks($filters),
                ];
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'message' => 'No Task record found.',
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
            }
         
    }

    public function changetaskstatus_post(){

        $task_id=$this->post('id');
        $status_id=$this->post('status_id');
        $user_id=$this->post( 'user_id');

        $update = $this->tasks_api->updateTaskStatus($task_id, $status_id, $user_id);
        if($update) {
        	 $returnStatus['message']='Status updated!';
             $taskdata = $this->tasks_api->getTaskByID($task_id);
             $returnStatus['data']=$taskdata;
             $returnStatus['status']=true;
             $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else{
            $returnStatus['message']='Something went wrong, try again!';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function changetaskpriority_post(){

        $task_id=$this->post('id');
        $priority_id=$this->post('priority_id');
        $user_id=$this->post( 'user_id');

        $update = $this->tasks_api->updateTaskPriority($task_id, $priority_id, $user_id);
        if($update) {
        	 $returnStatus['message']='Status updated!';
             $taskdata = $this->tasks_api->getTaskByID($task_id);
             $returnStatus['data']=$taskdata;
             $returnStatus['status']=true;
             $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else{
            $returnStatus['message']='Something went wrong, try again!';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

    public function changecheckliststatus_post()
    {
        $taskid= $this->post('taskid');
        $checklist_id= $this->post('checklist_id');
        $user_id= $this->post('user_id');
        $finished= $this->post('finished');

        //print_r($this->post('taskid'));

        $update = $this->tasks_api->finish_checklist_item($taskid, $checklist_id, $finished, $user_id);

        if($update) {
             $returnStatus['message']='Checklist item updated!';
             $checklist_data = $this->tasks_api->getChecklistByID($checklist_id);
             $returnStatus['data']=$checklist_data;
             $returnStatus['status']=true;
             $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else{
            $returnStatus['message']='Checklist item not updated, try again!';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }

    }

	public function newtask_get()
	{
		  $staffid=$this->get('staffid');
		  $is_notified = 0;
		  $tasks = $this->tasks_api->getNewTask($staffid, $is_notified);

		  if($tasks) {
        	 $returnStatus['message']='New tasks found!';
             
             $returnStatus['data']=$tasks;
             $returnStatus['status']=true;
             $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else{
            $returnStatus['message']='Something went wrong, try again!';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }

		 
		  // get task information where assind to staffid and where is_notified=0
		  // return data;

		  // return false;
	}
    public function updatenotification_post(){

        $task_id=$this->post('taskid');
        $staff_id=$this->post('staffid');
        // $assigned_from=$this->post('assigned_from');

        $task_assigned_id = $this->tasks_api->updateNotification($task_id, $staff_id);
        if($task_assigned_id) {
        	 $returnStatus['message']='Task is assigned!';
             $task_assignee = $this->tasks_api->getTaskAssignee($task_assigned_id);
             $returnStatus['data']=$task_assignee;
             $returnStatus['status']=true;
             $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        } else{
            $returnStatus['message']='Something went wrong, try again!';
            $returnStatus['status']=false;
            $this->set_response($returnStatus, REST_Controller::HTTP_OK);
        }
    }

}
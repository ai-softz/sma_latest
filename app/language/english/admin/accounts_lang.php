<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * Language: English
 * Module: Reports
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */

$lang['add_customer']                   =  'Add Customer';
$lang['c_o_a']                          =  'c o a';
$lang['coa']                            =  'coa';
$lang['Balance_Sheet']                  =  'Balance Sheet';
$lang['add_new_customer']               =  'Add new customer';
$lang['customer_name']                  =  'Customer name';
$lang['customer_email']                 =  'Customer email';
$lang['customer_mobile']                =  'Customer mobile';
$lang['customer_address']               =  'Customer address';
$lang['start_date']                     =  'Start date';
$lang['end_date']                       =  'End date';
$lang['search']                         =  'Search';
$lang['particulars']                    =  'Particulars';
$lang['income']                         =  'Income';
$lang['expense']                        =  'Expense';
<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * Language: English
 * Module: Reports
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */


$lang['bank_name']                      ='Bank name';
$lang['ac_name']                        ='Ac name';
$lang['ac_no']                          ='Ac no';
$lang['branch']                         ='Branch';
$lang['signature_pic']                  ='Signature pic';
$lang['reset']                          ='Reset';
$lang['save']                           ='Save';
$lang['supplier_payment']               ='Supplier payment';
$lang['voucher_no']                     ='Voucher no';
$lang['payment_type']                   ='Payment type';
$lang['cash_payment']                   ='Cash payment';
$lang['bank_payment']                   ='Bank payment';
$lang['remark']                         ='Remark';
$lang['supplier_name']                  ='Supplier name';
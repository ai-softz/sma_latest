<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * Language: English
 * Module: Reports
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */

$lang['the_number_sign'] = '#';
$lang['tasks_dt_name'] = 'Name';
$lang['task_status'] = 'Status';
$lang['tasks_dt_datestart'] = 'Start Date';
$lang['task_duedate'] = 'Due Date';
$lang['task_assigned'] = 'Assigned To';
$lang['tags'] = 'Tags';
$lang['tasks_list_priority'] = 'Priority';
$lang['tasks'] = 'Tasks';
$lang['add_task'] = 'Add Task';
$lang['task_status_updated'] = 'Task Status Updated';

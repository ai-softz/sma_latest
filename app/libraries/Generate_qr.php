<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generate_qr
{
    public function encode($seller, $taxNumber, $invoiceDate, $InvoiceTotalAmount, $InvoiceTaxAmount)
    {
        return $this->toBase64($this->toTLV($seller, $taxNumber, $invoiceDate, $InvoiceTotalAmount, $InvoiceTaxAmount));
    }

    public function toTLV($seller, $taxNumber, $invoiceDate, $InvoiceTotalAmount, $InvoiceTaxAmount)
    {
        return $seller->__toString() . $taxNumber->__toString() . $invoiceDate->__toString() . $InvoiceTotalAmount->__toString() . $InvoiceTaxAmount->__toString();
    }

    public function toBase64($strTLV)
    {
        return base64_encode($strTLV);
    }

}
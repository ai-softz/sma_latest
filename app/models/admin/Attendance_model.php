<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Attendance_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_period_master($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('period_master');
        if(!empty($sSearch)) {
            $this->db
                ->like('period_master.code', $sSearch)
                ->or_like('period_master.name', $sSearch)
                ->or_like('period_master.name_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('period_master.id', $sSortDir_0)
					->group_by('period_master.id')
					->get()
					->result();

        return $records;
    }
    public function total_period_master($sSearch)
    {
        $this->db
            ->select('*')
            ->from('period_master');
        if(!empty($sSearch)) {
            $this->db
                ->like('period_master.code', $sSearch)
                ->or_like('period_master.name', $sSearch)
                ->or_like('period_master.name_alt', $sSearch);
        }
        $records =  $this->db->group_by('period_master.id')
					->get()
					->result();

        return $records;
    }

    public function add_period_master($data)
    {
        $insert = $this->db->insert('period_master', $data);
        $insert_id = $this->db->insert_id();
        if($insert) {
            return $insert_id;
        } else {
            return false;
        }
    }
    public function add_period_detail($data)
    {
        $insert = $this->db->insert('period_details', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function read_period_master($id)
    {
        return $this->db->get_where('period_master', array('id' => $id))->row();
    }
    public function delete_period_master($period_master_id)
    {
        $delete = $this->db->where('id', $period_master_id)->delete('period_master');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_period_details($period_master_id)
    {
        $delete = $this->db->where('prd_master_id', $period_master_id)->delete('period_details');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }

}
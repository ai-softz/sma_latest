<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cmt_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function addNotification($data)
    {
        $user_ids = [];
        $user_ids = $data['user_ids'];
        unset($data['user_ids']);
        if ($this->db->insert('notifications', $data)) {
            $notify_id = $this->db->insert_id();

            if(!empty($user_ids)) {
                foreach($user_ids as $user) {
                    $notify_data = ['notify_id' => $notify_id, 'user_id' => $user];
                    $this->db->insert('notify_staffs', $notify_data);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function deleteComment($id)
    {
        if ($this->db->delete('notifications', ['id' => $id])) {
            return true;
        }
        return false;
    }

    public function getAllComments()
    {
        $q = $this->db->get('notifications');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCommentByID($id)
    {
        $q = $this->db->get_where('notifications', ['id' => $id], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    public function getNotifications()
    {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where('from_date <=', $date);
        $this->db->where('till_date >=', $date);
        if (!$this->Owner) {
            if ($this->Supplier) {
                $this->db->where('scope', 4);
            } elseif ($this->Customer) {
                $this->db->where('scope', 1)->or_where('scope', 3);
            } elseif (!$this->Customer && !$this->Supplier) {
                $this->db->where('scope', 2)->or_where('scope', 3);
            }
        }
        $q = $this->db->get('notifications');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function updateNotification($id, $data)
    {
        $user_ids = [];
        $user_ids = $data['user_ids'];
        unset($data['user_ids']);
        $this->db->where('id', $id);
        if ($this->db->update('notifications', $data)) {

            $delete = $this->db->where('notify_id', $id)->delete('notify_staffs');
                if($delete) { 
                    foreach($user_ids as $user) {
                        $notify_data = ['notify_id' => $id, 'user_id' => $user];
                        $this->db->insert('notify_staffs', $notify_data);
                    }
                }
            return true;
        } else {
            return false;
        }
    }
}

/* End of file pts_model.php */
/* Location: ./application/models/pts_types_model.php */

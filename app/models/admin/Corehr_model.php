<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Corehr_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function award_type()
    {
       return $this->db->get('award_type')->result();
    }

    public function add_award($data)
    {
        $insert = $this->db->insert('awards', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_award($id)
    {
        $delete = $this->db->where('award_id', $id)->delete('awards');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function read_award($award_id='')
    {
        return $this->db->get_where('awards', array('award_id' => $award_id))->row();
    }

    public function edit_award($data, $award_id)
    {
        $update = $this->db->where('award_id', $award_id)->update('awards', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function add_transfer($data)
    {
        $insert = $this->db->insert('employee_transfer', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function read_transfer($id)
    {
        return $this->db->get_where('employee_transfer', array('transfer_id' => $id))->row();
    }
    public function edit_transfer($data, $transfer_id)
    {
        $update = $this->db->where('transfer_id', $transfer_id)->update('employee_transfer', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_transfer($id)
    {
        $delete = $this->db->where('transfer_id', $id)->delete('employee_transfer');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_resignation($id)
    {
        $delete = $this->db->where('resignation_id', $id)->delete('employee_resignations');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function add_resignation($data)
    {
        $insert = $this->db->insert('employee_resignations', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function read_resignation($id)
    {
        return $this->db->get_where('employee_resignations', array('resignation_id' => $id))->row();
    }
    
    public function edit_resignation($data, $resignation_id)
    {
        $update = $this->db->where('resignation_id', $resignation_id)->update('employee_resignations', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function add_promotion($data)
    {
        $insert = $this->db->insert('employee_promotions', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_promotion($id)
    {
        $delete = $this->db->where('promotion_id', $id)->delete('employee_promotions');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function read_promotion($id)
    {
        return $this->db->get_where('employee_promotions', array('promotion_id' => $id))->row();
    }
    public function edit_promotion($data, $promotion_id)
    {
        $update = $this->db->where('promotion_id', $promotion_id)->update('employee_promotions', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function add_travel($data)
    {
        $insert = $this->db->insert('employee_travels', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_travel($id)
    {
        $delete = $this->db->where('travel_id', $id)->delete('employee_travels');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function read_travel($id)
    {
        return $this->db->get_where('employee_travels', array('travel_id' => $id))->row();
    }
    public function edit_travel($data, $travel_id)
    {
        $update = $this->db->where('travel_id', $travel_id)->update('employee_travels', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function add_complaint($data)
    {
        $insert = $this->db->insert('employee_complaints', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_complaint($id)
    {
        $delete = $this->db->where('complaint_id', $id)->delete('employee_complaints');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function read_complaint($id)
    {
        return $this->db->get_where('employee_complaints', array('complaint_id' => $id))->row();
    }
    public function edit_complaint($data, $complaint_id)
    {
        $update = $this->db->where('complaint_id', $complaint_id)->update('employee_complaints', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_warning($id)
    {
        $delete = $this->db->where('warning_id', $id)->delete('employee_warnings');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function add_warning($data)
    {
        $insert = $this->db->insert('employee_warnings', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function read_warning($id)
    {
        return $this->db->get_where('employee_warnings', array('warning_id' => $id))->row();
    }
    public function edit_warning($data, $warning_id)
    {
        $update = $this->db->where('warning_id', $warning_id)->update('employee_warnings', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_termination($id)
    {
        $delete = $this->db->where('termination_id', $id)->delete('employee_terminations');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function add_termination($data)
    {
        $insert = $this->db->insert('employee_terminations', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function read_termination($id)
    {
        return $this->db->get_where('employee_terminations', array('termination_id' => $id))->row();
    }
    public function edit_termination($data, $termination_id)
    {
        $update = $this->db->where('termination_id', $termination_id)->update('employee_terminations', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_employee_exit($id)
    {
        $delete = $this->db->where('exit_id', $id)->delete('employee_exit');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function add_employee_exit($data)
    {
        $insert = $this->db->insert('employee_exit', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    } 
    public function read_employee_exit($id)
    {
        return $this->db->get_where('employee_exit', array('exit_id' => $id))->row();
    }
    public function edit_employee_exit($data, $exit_id)
    {
        $update = $this->db->where('exit_id', $exit_id)->update('employee_exit', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function add_holiday($data)
    {
        $insert = $this->db->insert('holidays', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    } 
    public function read_holiday_record($id)
    {
        return $this->db->get_where('holidays', array('holiday_id' => $id))->row();
    }
    public function delete_holiday($id)
    {
        $delete = $this->db->where('holiday_id', $id)->delete('holidays');
        if($delete) {
            return true;
        } else {
            return false;
        }
    } 

    public function add_events($data)
    {
        $insert = $this->db->insert('events', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    } 
    public function read_leave_record($id)
    {
        return $this->db->get_where('leave_applications', array('leave_id' => $id))->row();
    }
    public function read_event_record($id)
    {
        return $this->db->get_where('events', array('event_id' => $id))->row();
    }
    public function delete_event($id)
    {
        $delete = $this->db->where('event_id', $id)->delete('events');
        if($delete) {
            return true;
        } else {
            return false;
        }
    } 
    public function read_event($id)
    {
        return $this->db->get_where('events', array('event_id' => $id))->row();
    }
    public function edit_events($data, $event_id)
    {
        $update = $this->db->where('event_id', $event_id)->update('events', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function read_meeting_record($id)
    {
        return $this->db->get_where('meetings', array('meeting_id' => $id))->row();
    }
    public function add_meetings($data)
    {
        $insert = $this->db->insert('meetings', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    } 
    public function delete_meeting($id)
    {
        $delete = $this->db->where('meeting_id', $id)->delete('meetings');
        if($delete) {
            return true;
        } else {
            return false;
        }
    } 
    public function edit_meetings($data, $meeting_id)
    {
        $update = $this->db->where('meeting_id', $meeting_id)->update('meetings', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    // get employees upcoming birthday
	public function employees_upcoming_birthday() {
	
		//$query = $this->db->query("SELECT * FROM xin_employees WHERE date_of_birth BETWEEN DATE_ADD(NOW(), INTERVAL 1 DAY) AND DATE_ADD( NOW() , INTERVAL 1 MONTH)");
		$query = $this->db->query("SELECT user_id, first_name, last_name, department_id, designation_id, warehouse_id, date_of_birth,
            DATE_ADD(
                date_of_birth, 
                INTERVAL IF(DAYOFYEAR(date_of_birth) >= DAYOFYEAR(CURDATE()),
                    YEAR(CURDATE())-YEAR(date_of_birth),
                    YEAR(CURDATE())-YEAR(date_of_birth)+1
                ) YEAR
            ) AS next_birthday,
            YEAR(CURDATE())-YEAR(date_of_birth) AS age 
        FROM ".db_prefix()."employees
        WHERE 
            date_of_birth IS NOT NULL
        HAVING 
            next_birthday BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 1 MONTH)
        ORDER BY next_birthday");
                return $query->result();
    }

    public function get_trainings()
    {
        return $this->db->get('training')->result();
    }
    public function delete_training($id)
    {
        $delete = $this->db->where('training_id', $id)->delete('training');
        if($delete) {
            return true;
        } else {
            return false;
        }
    } 
    public function read_training($id)
    {
        return $this->db->get_where('training', array('training_id' => $id))->row();
    }
    public function add_training($data)
    {
        $insert = $this->db->insert('training', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    } 
    public function edit_training($data, $training_id)
    {
        $update = $this->db->where('training_id', $training_id)->update('training', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function add_trainer($data)
    {
        $insert = $this->db->insert('trainers', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    } 
    public function delete_trainer($id)
    {
        $delete = $this->db->where('trainer_id', $id)->delete('trainers');
        if($delete) {
            return true;
        } else {
            return false;
        }
    } 
    public function read_trainer($id)
    {
        return $this->db->get_where('trainers', array('trainer_id' => $id))->row();
    }
    public function edit_trainer($data, $trainer_id)
    {
        $update = $this->db->where('trainer_id', $trainer_id)->update('trainers', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
}
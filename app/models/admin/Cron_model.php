<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cron_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->lang->admin_load('cron');
    }

    public function getSettings()
    {
        $q = $this->db->get_where('settings', ['setting_id' => 1], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function run_cron()
    {
        $m = [];
        if ($this->resetOrderRef()) {
            $m[] = lang('order_ref_updated');
        }
        if ($pendingInvoices = $this->getAllPendingInvoices()) {
            $p = 0;
            foreach ($pendingInvoices as $invoice) {
                $this->updateInvoiceStatus($invoice->id);
                $p++;
            }
            $m[] = sprintf(lang('x_pending_to_due'), $p);
        }
        if ($partialInvoices = $this->getAllPPInvoices()) {
            $pp = 0;
            foreach ($partialInvoices as $invoice) {
                $this->updateInvoiceStatus($invoice->id);
                $pp++;
            }
            $m[] = sprintf(lang('x_partial_to_due'), $pp);
        }
        if ($unpaidpurchases = $this->getUnpaidPuchases()) {
            $up = 0;
            foreach ($unpaidpurchases as $purchase) {
                $this->db->update('purchases', ['payment_status' => 'due'], ['id' => $purchase->id]);
                $up++;
            }
            $m[] = sprintf(lang('x_purchases_changed'), $up);
        }
        if ($pis = $this->get_expired_products()) {
            $e  = 0;
            $ep = 0;
            foreach ($pis as $pi) {
                $this->db->update('purchase_items', ['quantity_balance' => 0], ['id' => $pi->id]);
                $e++;
                $ep += $pi->quantity_balance;
            }
            $this->site->syncQuantity(null, null, $pis);
            $m[] = sprintf(lang('x_products_expired'), $e, $ep);
        }
        if ($promos = $this->getPromoProducts()) {
            $pro = 0;
            foreach ($promos as $pr) {
                $this->db->update('products', ['promotion' => 0], ['id' => $pr->id]);
                $pro++;
            }
            $m[] = sprintf(lang('x_promotions_expired'), $pro);
        }
        $date = date('Y-m-d H:i:s', strtotime('-1 month'));
        if ($this->deleteUserLgoins($date)) {
            $m[] = sprintf(lang('user_login_deleted'), $date);
        }
        if ($this->db_backup()) {
            $m[] = lang('backup_done');
        }
        if ($this->checkUpdate()) {
            $m[] = lang('update_available');
        }
        $r = !empty($m) ? $m : false;
        $this->send_email($r);
        // $this->db->truncate('sessions');
        $this->db->delete('sessions', ['timestamp <' => time() - 60 * 60 * 48]);
        return $r;
    }

    public function send_email($details)
    {
        if ($details) {
            $table_html = '';
            $tables     = $this->cron_model->yesterday_report();
            foreach ($tables as $table) {
                $table_html .= $table . '<div style="clear:both"></div>';
            }
            foreach ($details as $detail) {
                $table_html = $table_html . $detail;
            }
            $msg_with_yesterday_report = $table_html;
            $owners                    = $this->db->get_where('users', ['group_id' => 1])->result();
            $this->load->library('email');
            $config['useragent'] = 'Stock Manager Advance';
            $config['protocol']  = $this->Settings->protocol;
            $config['mailtype']  = 'html';
            $config['crlf']      = "\r\n";
            $config['newline']   = "\r\n";
            if ($this->Settings->protocol == 'sendmail') {
                $config['mailpath'] = $this->Settings->mailpath;
            } elseif ($this->Settings->protocol == 'smtp') {
                $config['smtp_host'] = $this->Settings->smtp_host;
                $config['smtp_user'] = $this->Settings->smtp_user;
                $config['smtp_pass'] = $this->Settings->smtp_pass;
                $config['smtp_port'] = $this->Settings->smtp_port;
                if (!empty($this->Settings->smtp_crypto)) {
                    $config['smtp_crypto'] = $this->Settings->smtp_crypto;
                }
            }
            $this->email->initialize($config);

            foreach ($owners as $owner) {
                list($user, $domain) = explode('@', $owner->email);
                if ($domain != 'tecdiary.com') {
                    $this->load->library('parser');
                    $parse_data = [
                        'name'      => $owner->first_name . ' ' . $owner->last_name,
                        'email'     => $owner->email,
                        'msg'       => $msg_with_yesterday_report,
                        'site_link' => base_url(),
                        'site_name' => $this->Settings->site_name,
                        'logo'      => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>',
                    ];
                    $msg     = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/cron.html');
                    $message = $this->parser->parse_string($msg, $parse_data);
                    $subject = lang('cron_job') . ' - ' . $this->Settings->site_name;

                    $this->email->from($this->Settings->default_email, $this->Settings->site_name);
                    $this->email->to($owner->email);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    $this->email->send();
                }
            }
        }
    }

    private function checkUpdate()
    {
        $fields = ['version' => $this->Settings->version, 'code' => $this->Settings->purchase_code, 'username' => $this->Settings->envato_username, 'site' => base_url()];
        $this->load->helper('update');
        $protocol = is_https() ? 'https://' : 'http://';
        $updates  = get_remote_contents($protocol . 'tecdiary.com/api/v1/update/', $fields);
        $response = json_decode($updates);
        if (!empty($response->data->updates)) {
            $this->db->update('settings', ['update' => 1], ['setting_id' => 1]);
            return true;
        }
        return false;
    }

    public function db_backup()
    {
        $this->load->dbutil();
        $prefs = [
            'format'   => 'txt',
            'filename' => 'sma_db_backup.sql',
        ];
        $back    = $this->dbutil->backup($prefs);
        $backup  = &$back;
        $db_name = 'db-backup-on-' . date('Y-m-d-H-i-s') . '.txt';
        $save    = './files/backups/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);

        $files = glob('./files/backups/*.txt', GLOB_BRACE);
        $now   = time();
        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 60 * 60 * 24 * 30) {
                    unlink($file);
                }
            }
        }

        return true;
    }

    private function deleteUserLgoins($date)
    {
        $this->db->where('time <', $date);
        if ($this->db->delete('user_logins')) {
            return true;
        }
        return false;
    }

    private function gen_html($costing, $discount, $expenses, $returns, $purchases, $sales, $warehouse = null)
    {
        $html = '<div style="border:1px solid #DDD; padding:10px; margin:10px 0;"><h3>' . ($warehouse ? $warehouse->name . ' (' . $warehouse->code . ')' : lang('all_warehouses')) . '</h3>
        <table width="100%" class="stable">
        <tr>
            <td style="border-bottom: 1px solid #EEE;">' . lang('products_sale') . '</td>
            <td style="text-align:right; border-bottom: 1px solid #EEE;">' . $this->sma->formatMoney($costing->sales) . '</td>
        </tr>';
        if ($discount && $discount->order_discount > 0) {
            $html .= '
            <tr>
                <td style="border-bottom: 1px solid #DDD;">' . lang('order_discount') . '</td>
                <td style="text-align:right;border-bottom: 1px solid #DDD;">' . $this->sma->formatMoney($discount->order_discount) . '</td>
            </tr>';
        }
        $html .= '
        <tr>
            <td style="border-bottom: 1px solid #EEE;">' . lang('products_cost') . '</td>
            <td style="text-align:right; border-bottom: 1px solid #EEE;">' . $this->sma->formatMoney($costing->cost) . '</td>
        </tr>';
        if ($expenses && $expenses->total > 0) {
            $html .= '
            <tr>
                <td style="border-bottom: 1px solid #DDD;">' . lang('expenses') . '</td>
                <td style="text-align:right;border-bottom: 1px solid #DDD;">' . $this->sma->formatMoney($expenses->total) . '</td>
            </tr>';
        }
        $html .= '
        <tr>
            <td width="300px;" style="border-bottom: 1px solid #DDD;"><strong>' . lang('profit') . '</strong></td>
            <td style="text-align:right;border-bottom: 1px solid #DDD;">
                <strong>' . $this->sma->formatMoney($costing->sales - $costing->cost - ($discount ? $discount->order_discount : 0) - ($expenses ? $expenses->total : 0)) . '</strong>
            </td>
        </tr>';
        if (isset($returns->total)) {
            $html .= '
            <tr>
                <td width="300px;" style="border-bottom: 2px solid #DDD;"><strong>' . lang('return_sales') . '</strong></td>
                <td style="text-align:right;border-bottom: 2px solid #DDD;"><strong>' . $this->sma->formatMoney($returns->total) . '</strong></td>
            </tr>';
        }
        $html .= '</table><h4 style="margin-top:15px;">' . lang('general_ledger') . '</h4>
        <table width="100%" class="stable">';
        if ($sales) {
            $html .= '
            <tr>
                <td width="33%" style="border-bottom: 1px solid #DDD;">' . lang('total_sales') . ': <strong>' . $this->sma->formatMoney($sales->total_amount) . '(' . $sales->total . ')</strong></td>
                <td width="33%" style="border-bottom: 1px solid #DDD;">' . lang('received') . ': <strong>' . $this->sma->formatMoney($sales->paid) . '</strong></td>
                <td width="33%" style="border-bottom: 1px solid #DDD;">' . lang('taxes') . ': <strong>' . $this->sma->formatMoney($sales->tax) . '</strong></td>
            </tr>';
        }
        if ($purchases) {
            $html .= '
            <tr>
                <td width="33%">' . lang('total_purchases') . ': <strong>' . $this->sma->formatMoney($purchases->total_amount) . '(' . $purchases->total . ')</strong></td>
                <td width="33%">' . lang('paid') . ': <strong>' . $this->sma->formatMoney($purchases->paid) . '</strong></td>
                <td width="33%">' . lang('taxes') . ': <strong>' . $this->sma->formatMoney($purchases->tax) . '</strong></td>
            </tr>';
        }
        $html .= '</table></div>';
        return $html;
    }

    private function get_expired_products()
    {
        if ($this->Settings->remove_expired) {
            $date = date('Y-m-d');
            $this->db->where('expiry <=', $date)->where('expiry !=', null)->where('expiry !=', '0000-00-00')->where('quantity_balance >', 0);
            $q = $this->db->get('purchase_items');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
        }
        return false;
    }

    private function getAllPendingInvoices()
    {
        $today    = date('Y-m-d');
        $paid     = $this->lang->line('paid');
        $canceled = $this->lang->line('cancelled');
        $q        = $this->db->get_where('sales', ['due_date <=' => $today, 'due_date !=' => '1970-01-01', 'due_date !=' => null, 'payment_status' => 'pending']);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    private function getAllPPInvoices()
    {
        $today    = date('Y-m-d');
        $paid     = $this->lang->line('paid');
        $canceled = $this->lang->line('cancelled');
        $q        = $this->db->get_where('sales', ['due_date <=' => $today, 'due_date !=' => '1970-01-01', 'due_date !=' => null, 'payment_status' => 'partial']);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    private function getCosting($date, $warehouse_id = null)
    {
        $this->db->select('SUM( COALESCE( purchase_unit_cost, 0 ) * quantity ) AS cost, SUM( COALESCE( sale_unit_price, 0 ) * quantity ) AS sales, SUM( COALESCE( purchase_net_unit_cost, 0 ) * quantity ) AS net_cost, SUM( COALESCE( sale_net_unit_price, 0 ) * quantity ) AS net_sales', false);
        $this->db->where('costing.date', $date);
        if ($warehouse_id) {
            $this->db->join('sales', 'sales.id=costing.sale_id')
            ->where('sales.warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('costing');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getExpenses($sdate, $edate, $warehouse_id = null)
    {
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', false);
        $this->db->where('date >=', $sdate)->where('date <=', $edate);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getOrderDiscount($sdate, $edate, $warehouse_id = null)
    {
        $this->db->select('SUM( COALESCE( order_discount, 0 ) ) AS order_discount', false);
        $this->db->where('date >=', $sdate)->where('date <=', $edate);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getOrderRef()
    {
        $q = $this->db->get_where('order_ref', ['ref_id' => 1], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getPromoProducts()
    {
        $today = date('Y-m-d');
        $q     = $this->db->get_where('products', ['promotion' => 1, 'end_date !=' => null, 'end_date <=' => $today]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    private function getReturns($sdate, $edate, $warehouse_id = null)
    {
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total', false)
        ->where('sale_status', 'returned');
        $this->db->where('date >=', $sdate)->where('date <=', $edate);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getTotalPurchases($sdate, $edate, $warehouse_id = null)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', false)
            ->where('status !=', 'pending')
            ->where('date >=', $sdate)->where('date <=', $edate);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getTotalSales($sdate, $edate, $warehouse_id = null)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', false)
            ->where('sale_status !=', 'pending')
            ->where('date >=', $sdate)->where('date <=', $edate);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getUnpaidPuchases()
    {
        $today = date('Y-m-d');
        $q     = $this->db->get_where('purchases', ['payment_status !=' => 'paid', 'payment_status !=' => 'due', 'payment_term >' => 0, 'due_date !=' => null, 'due_date <=' => $today]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    private function resetOrderRef()
    {
        if ($this->Settings->reference_format == 1 || $this->Settings->reference_format == 2) {
            $month = date('Y-m') . '-01';
            $year  = date('Y') . '-01-01';
            if ($ref = $this->getOrderRef()) {
                $reset_ref = ['so' => 1, 'qu' => 1, 'po' => 1, 'to' => 1, 'pos' => 1, 'do' => 1, 'pay' => 1, 'ppay' => 1, 're' => 1, 'rep' => 1, 'ex' => 1, 'qa' => 1];
                if ($this->Settings->reference_format == 1 && strtotime($ref->date) < strtotime($year)) {
                    $reset_ref['date'] = $year;
                    $this->db->update('order_ref', $reset_ref, ['ref_id' => 1]);
                    return true;
                } elseif ($this->Settings->reference_format == 2 && strtotime($ref->date) < strtotime($month)) {
                    $reset_ref['date'] = $month;
                    $this->db->update('order_ref', $reset_ref, ['ref_id' => 1]);
                    return true;
                }
            }
        }
        return false;
    }

    private function updateInvoiceStatus($id)
    {
        if ($this->db->update('sales', ['payment_status' => 'due'], ['id' => $id])) {
            return true;
        }
        return false;
    }

    private function yesterday_report()
    {
        $date       = date('Y-m-d', strtotime('-1 day'));
        $sdate      = $date . ' 00:00:00';
        $edate      = $date . ' 23:59:59';
        $warehouses = $this->db->get('warehouses')->result();
        foreach ($warehouses as $warehouse) {
            $costing         = $this->getCosting($date, $warehouse->id);
            $discount        = $this->getOrderDiscount($sdate, $edate, $warehouse->id);
            $expenses        = $this->getExpenses($sdate, $edate, $warehouse->id);
            $returns         = $this->getReturns($sdate, $edate, $warehouse->id);
            $total_purchases = $this->getTotalPurchases($sdate, $edate, $warehouse->id);
            $total_sales     = $this->getTotalSales($sdate, $edate, $warehouse->id);
            $html[]          = $this->gen_html($costing, $discount, $expenses, $returns, $total_purchases, $total_sales, $warehouse);
        }

        $costing         = $this->getCosting($date);
        $discount        = $this->getOrderDiscount($sdate, $edate);
        $expenses        = $this->getExpenses($sdate, $edate);
        $returns         = $this->getReturns($sdate, $edate);
        $total_purchases = $this->getTotalPurchases($sdate, $edate);
        $total_sales     = $this->getTotalSales($sdate, $edate);
        $html[]          = $this->gen_html($costing, $discount, $expenses, $returns, $total_purchases, $total_sales);

        return $html;
    }

    public function synchronize_salesdata_from_local()
    {
        $sales = $this->db->limit(5)->get_where('sales', array('is_syncronize'=>0))->result_array();
// echo "<pre>";print_r($sales); die;
        if(!empty($sales)) {
            foreach($sales as $sale) {
                    $this->makeSalesJsonDataFromLocal($sale);
            }
        } else {
            echo "No data to synchronized!";
        }
        $this->data = array();
        //$this->load->view($this->theme . 'sales/synchronize_data', $this->data);
    }

    public function makeSalesJsonDataFromLocal($sale)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 

        //$newPostArray=array();
        // $newPostArray['api-key']="ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw";
        $newPostArray['api-key']=$settings->api_key;
        $newPostArray['user_id']=$sale["created_by"];
        $newPostArray['date']=$sale["date"];
        $newPostArray['customer']=$sale["customer_id"];
        $newPostArray['biller']=$sale["biller_id"];
        $newPostArray['warehouse']=$sale["warehouse_id"];
        $newPostArray['pos_note']=$sale["note"];
        $newPostArray['staff_note']=$sale["staff_note"];
        $newPostArray['order_tax']=$sale["order_tax_id"];
        $newPostArray['discount']=$sale["order_discount_id"];
        $newPostArray['shipping']=$sale["shipping"];
        //$newPostArray['amount_paid']=$sale["paid"];
        $newPostArray['paidby']=$sale["payment_method"];
        $newPostArray['total_items']=$sale["total_items"];
        $newPostArray['is_dine_in']=$sale["customer_type"];
        // paid problem
        // jsonobj.put("is_dine_in", is_dine_in);
        $newPostArray['local_id']=$sale["id"];//
        $newPostArray['order_platform']="offline";//
        $newPostArray['is_syncronize']="1";//
        $newPostArray['details'] = array();

        $items_array = $this->db->get_where('sale_items', array('sale_id'=>$sale['id']))->result_array();
        $itemsDetailArray = array();
        //echo "<pre>";print_r($items_array); die;
        for ($i=0; $i < sizeof($items_array) ; $i++) {
            $itemsDetailArray[$i]["product_id"] = $items_array[$i]["product_id"];
            $itemsDetailArray[$i]["quantity"] =  $items_array[$i]["quantity"];
            $itemsDetailArray[$i]["product_type"] = "standard";
            $itemsDetailArray[$i]["product_code"] = $items_array[$i]["product_code"];
            $itemsDetailArray[$i]["product_name"] =  $items_array[$i]["product_name"];
            $itemsDetailArray[$i]["second_name"] = $items_array[$i]["name_alt"];
            $itemsDetailArray[$i]["net_price"] =  $items_array[$i]["net_unit_price"];
            $itemsDetailArray[$i]["unit_price"] =  $items_array[$i]["unit_price"];
            $itemsDetailArray[$i]["real_unit_price"] =  $items_array[$i]["unit_price"];
            $itemsDetailArray[$i]["product_base_quantity"]=$items_array[$i]["quantity"];
            $itemsDetailArray[$i]["product_unit"]=$items_array[$i]["product_unit_id"];
            $itemsDetailArray[$i]["product_option"]=$items_array[$i]["option_id"]  !=null ? $items_array[$i]["option_id"] : "false";
            $itemsDetailArray[$i]["product_discount"]=$items_array[$i]["discount"];
            $itemsDetailArray[$i]["product_tax"]=$items_array[$i]["tax_rate_id"];
            $itemsDetailArray[$i]["product_comment"]=$items_array[$i]["comment"];
            $itemsDetailArray[$i]["serial"]=$items_array[$i]["serial_no"];
            $itemsDetailArray[$i]["is_product_name_custom"]="0";
        }
        $newPostArray['details'] = $itemsDetailArray;

        $payment_array = $this->db->get_where('payments', array('sale_id'=>$sale['id']))->result_array();
        $payment_details=array();

        for ($i=0; $i < 5 ; $i++) { 
            if($i == 0){
                $payment_details[$i]['amount'] = $sale["grand_total"];
                $payment_details[$i]['balance_amount'] = $sale["grand_total"] - $sale["paid"];
                $payment_details[$i]['paid_by'] = $payment_array[$i]['paid_by'];
                if($payment_array[$i]['paid_by'] == "gift_card"){
                    $payment_details[$i]['paying_gift_card_no'] = $payment_array[$i]['cc_no'];
                }else{
                    $payment_details[$i]['paying_gift_card_no'] = "";
                }
                $payment_details[$i]['cc_no'] = "";
                $payment_details[$i]['cc_holder'] = "";
                $payment_details[$i]['cheque_no'] = "";
                $payment_details[$i]['cc_month'] = "";
                $payment_details[$i]['cc_year'] = "";
                $payment_details[$i]['cc_type'] = $payment_array[$i]['cc_type'];
                $payment_details[$i]['cc_cvv2'] = "";
                $payment_details[$i]['payment_note'] = "";
            }else{
                $payment_details[$i]['amount'] = "";
                $payment_details[$i]['balance_amount'] = "";
                $payment_details[$i]['paid_by'] = "";
                $payment_details[$i]['paying_gift_card_no'] = "";
                $payment_details[$i]['cc_no'] = "";
                $payment_details[$i]['cc_holder'] = "";
                $payment_details[$i]['cheque_no'] = "";
                $payment_details[$i]['cc_month'] = "";
                $payment_details[$i]['cc_year'] = "";
                $payment_details[$i]['cc_type'] = "";
                $payment_details[$i]['cc_cvv2'] = "";
                $payment_details[$i]['payment_note'] = "";
            }
            
        }
        $newPostArray['payment_details']=$payment_details;

        // echo "<pre>";print_r($newPostArray); die();
        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode( $newPostArray ),
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n"
            )
        );
        // echo "<pre>";
        // print_r($options);
        // echo "</pre>";
        // die();
        // $url="http://localhost/retaurant_online/api/v1/sales/postsales";
        
        $url = $settings->online_link . "api/v1/sales/postsales";
        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );

        // echo "<pre>";print_r($response); die();
        $update=false;
        if(!empty($response->sale)) {
            $sale_id = $response->sale->sale_id;
            $data = [
                //'online_id' => $sale_id,
                'is_syncronize' => 1,
                'order_platform' => 'Desktop'
            ];
            $update = $this->db->where('id', $sale["id"])->update('sales', $data);

        }

        if($update) {
            echo "Sales Data is synchronized successfully! Local ID: " . $sale["id"] . " -> Online ID: ". $sale_id . "<br>";
        }
        else {
            echo "Something went wrong!";
            if(empty($response->get_product_details)) {
                echo " Sale item product is not found, product code may not match.";
            }
        }

    }

    public function synchronize_salesdata_from_online()
    {
        $settings = $this->site->get_setting(); 
        $url = $settings->online_link;
        $api_key = $settings->api_key;

        $url = $url."api/v1/synchronize_data/allsales?start=0&limit=5&api-key=".$api_key."&include=payment,items,warehouse,biller";
        $json = file_get_contents($url);
        $records = json_decode($json);
        //$this->load->view($this->theme . 'sales/synchronize_data', $this->data);

        // $sales = $json;
        // $records = json_decode($json);
        if(!empty($records->data)) {
            $sales = $records->data;
        }
        // echo "<pre>";print_r($sales);die;
        if(!empty($sales)) {
            foreach($sales as $sale) {
                    $this->makeSalesJsonDataFromOnline((array)$sale);
            }
        } else {
            echo "No data to synchronized!";
        }
    }

    public function makeSalesJsonDataFromOnline($sale)
    {
        // echo "<pre>"; print_r($sale);die;
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        unset($sale['billerdetails']);

        //unset($sale['items']);
        //unset($sale['payments']);
        unset($sale['warehouse']);
        unset($sale['user_id']);
        $created_by = $sale["created_by"]->id;
        unset($sale['created_by']);

        //$newPostArray=$sale;
        // $newPostArray['api-key']="ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw";
        $newPostArray['api-key']=$settings->api_key;
        $newPostArray['user_id']=$created_by;
        $newPostArray['customer']=$sale["customer_id"];
        $newPostArray['date']=$sale["date"];
        $newPostArray['biller']=$sale["biller_id"];
        $newPostArray['warehouse']=$sale["warehouse_id"];
        $newPostArray['pos_note']=$sale["note"];
        $newPostArray['staff_note']=$sale["staff_note"];
        $newPostArray['order_tax']=$sale["order_tax_id"];
        $newPostArray['discount']=$sale["order_discount_id"];
        $newPostArray['shipping']=$sale["shipping"];
        //$newPostArray['amount_paid']=$sale["paid"];
        $newPostArray['paidby']=$sale["payment_method"];
        $newPostArray['total_items']=$sale["total_items"];
        $newPostArray['is_dine_in']=$sale["customer_type"];
        // paid problem
        // jsonobj.put("is_dine_in", is_dine_in);
        $newPostArray['online_id']=$sale["id"];//
        $newPostArray['order_platform']="online";//
        $newPostArray['is_syncronize']="1";//

        $newPostArray['details'] = array();
        //$items_array = $this->db->get_where('sale_items', array('sale_id'=>$sale['id']))->result_array();
        $items_arr = $sale['items'];
        $itemsDetailArray = array();

        for ($i=0; $i < sizeof($items_arr) ; $i++) { 
            //$itemsDetailArray[$i]=(array)$items_array[$i];
            $items_array = (array)$items_arr[$i];
            $itemsDetailArray[$i]["product_id"] = $items_array["product_id"];
            $itemsDetailArray[$i]["quantity"] =  $items_array["quantity"];
            $itemsDetailArray[$i]["product_type"] = "standard";
            $itemsDetailArray[$i]["product_code"] = $items_array["product_code"];
            $itemsDetailArray[$i]["product_name"] =  $items_array["product_name"];
            $itemsDetailArray[$i]["second_name"] = $items_array["name_alt"];
            $itemsDetailArray[$i]["net_price"] =  $items_array["net_unit_price"];
            $itemsDetailArray[$i]["unit_price"] =  $items_array["unit_price"];
            $itemsDetailArray[$i]["real_unit_price"] =  $items_array["unit_price"];
            $itemsDetailArray[$i]["product_base_quantity"]=$items_array["quantity"];
            $itemsDetailArray[$i]["product_unit"]=$items_array["product_unit_id"];
            $itemsDetailArray[$i]["product_option"]="false";
            $itemsDetailArray[$i]["product_discount"]=$items_array["discount"];
            $itemsDetailArray[$i]["product_tax"]=$items_array["tax_rate_id"];
            $itemsDetailArray[$i]["product_comment"]=$items_array["comment"];
            $itemsDetailArray[$i]["serial"]=$items_array["serial_no"];
            $itemsDetailArray[$i]["is_product_name_custom"]="0";
        }
        $newPostArray['details'] = $itemsDetailArray;

        //$payment_array = $this->db->get_where('payments', array('sale_id'=>$sale['id']))->result_array();
        
        $payment_arr = $sale['payments'];
        $payment_details=array();

        for ($i=0; $i < 5 ; $i++) { 
            $payment_array = (array)$payment_arr[0];
            if($i == 0){
                $payment_details[$i]['amount'] = $sale["grand_total"];
                $payment_details[$i]['balance_amount'] = $sale["grand_total"] - $sale["paid"];
                $payment_details[$i]['paid_by'] = $payment_array['paid_by'];
                if($payment_array['paid_by'] == "gift_card"){
                    $payment_details[$i]['paying_gift_card_no'] = $payment_array['cc_no'];
                }else{
                    $payment_details[$i]['paying_gift_card_no'] = "";
                }
                $payment_details[$i]['cc_no'] = "";
                $payment_details[$i]['cc_holder'] = "";
                $payment_details[$i]['cheque_no'] = "";
                $payment_details[$i]['cc_month'] = "";
                $payment_details[$i]['cc_year'] = "";
                $payment_details[$i]['cc_type'] = $payment_array['cc_type'];
                $payment_details[$i]['cc_cvv2'] = "";
                $payment_details[$i]['payment_note'] = "";
            }else{
                $payment_details[$i]['amount'] = "";
                $payment_details[$i]['balance_amount'] = "";
                $payment_details[$i]['paid_by'] = "";
                $payment_details[$i]['paying_gift_card_no'] = "";
                $payment_details[$i]['cc_no'] = "";
                $payment_details[$i]['cc_holder'] = "";
                $payment_details[$i]['cheque_no'] = "";
                $payment_details[$i]['cc_month'] = "";
                $payment_details[$i]['cc_year'] = "";
                $payment_details[$i]['cc_type'] = "";
                $payment_details[$i]['cc_cvv2'] = "";
                $payment_details[$i]['payment_note'] = "";
            }
            
        }
        $newPostArray['payment_details']=$payment_details;
        // echo '<pre>';print_r($newPostArray); die;
        // for ($i=0; $i < sizeof($payment_arr) ; $i++) { 
        //     $payment_details[$i] = $payment_array[$i];
        //     if($payment_array[$i]['paid_by'] == "gift_card"){
        //         $payment_details[$i]['paying_gift_card_no'] = $payment_array[$i]['cc_no'];
        //     }
        //     $payment_details[$i]['cc_cvv2'] = ""; 
        //     $payment_details[$i]['payment_note'] = $payment_array[$i]['note'];
        //     $payment_details[$i]['user_id'] = $created_by;
        //     $payment_details[$i]['amount'] = $payment_array[$i]['pos_paid'];
        //     $payment_details[$i]['balance_amount'] = $payment_array[$i]['pos_balance'];
        // }
        // $newPostArray['payment_details']=$payment_details;


        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode( $newPostArray ),
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n"
            )
        );
        
       // $url="http://localhost/retaurant_online/api/v1/sales/postsales";
        
        $url = base_url() . "api/v1/sales/postsales";

        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );

        // echo '<pre>';print_r($response); die;
        if(!empty($response->sale)) {
            $sale_id = $response->sale->sale_id;
            $update=true;
        }
        // if(!empty($response->sale)) {
        //     $sale_id = $response->sale->sale_id;
        //     $data = [
        //         'online_id' => $sale_id,
        //         'is_syncronize' => 1,
        //         'order_platform' => 'Desktop'
        //     ];
        //     $update = $this->db->where('id', $sale["id"])->update('sales', $data);

        // }

        if($update) {
            echo "Sales Data is synchronized successfully! Local ID: " . $sale["id"] . " -> Online ID: ". $sale_id . "<br>";
        }
        else {
            echo "Something went wrong!";
        }

    }

    public function synchronize_purchases_from_online()
    {
        $settings = $this->site->get_setting(); 
        $url = $settings->online_link;
        $api_key = $settings->api_key;

        $url = $url."api/v1/synchronize_data/allpurchases?start=0&limit=5&is_synchronize=0&api-key=".$api_key."&include=items,warehouse,payment";
        $json = file_get_contents($url);
        $records = json_decode($json);

        $records = $records->data;
        // echo "<pre>";print_r($records);die;
        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makePurchasesJsonFromOnline((array)$record);
            }
        } else {
            echo "No data to synchronized!";
        }
    }

    public function makePurchasesJsonFromOnline($record)
    {
        // echo "<pre>";print_r($record);die;
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        
        // $newPostArray['api-key']="ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw";
        $newPostArray['api-key']=$settings->api_key;

        $newPostArray['warehouse']=$record["warehouse_id"];
        $newPostArray['discount']=$record["order_discount"];
        $newPostArray['reference_no']=$record["reference_no"];
        $newPostArray['warehouse']=$record["warehouse_id"];
        $newPostArray['supplier_id']=$record["supplier_id"];
        $newPostArray['supplier']=$record["supplier"];
        $newPostArray['status']=$record["status"];
        $newPostArray['shipping']=$record["shipping"];
        $newPostArray['note']=$record["note"];//
        $newPostArray['payment_term']=$record["payment_term"];//
        unset($newPostArray['id']);
        $newPostArray['online_id']=$record["id"];//
        $newPostArray['platform']="Online";//
        $newPostArray['is_syncronize']="1";//
        //$newPostArray['details'] = array();
        // echo '<pre>';print_r($record); die;
        //$items_array = $this->db->get_where('sale_items', array('sale_id'=>$sale['id']))->result_array();
        $itemsDetailArray = array();
        $items_array = $record['items'];
        for ($i=0; $i < sizeof($items_array) ; $i++) { 
            // $itemsDetailArray[$i]=(array)$items_array[$i];
            unset($itemsDetailArray->id);
            $itemsDetailArray[$i]["net_cost"]=$items_array[$i]->net_unit_cost;
            $itemsDetailArray[$i]["unit_cost"]=$items_array[$i]->unit_cost;
            $itemsDetailArray[$i]["real_unit_cost"]=$items_array[$i]->real_unit_cost;
            $itemsDetailArray[$i]["item_tax"]=$items_array[$i]->item_tax;
            $itemsDetailArray[$i]["tax_rate_id"]=$items_array[$i]->tax_rate_id;
            $itemsDetailArray[$i]["product_discount"]=$items_array[$i]->item_discount;
            $itemsDetailArray[$i]["expiry"]=$items_array[$i]->expiry;
            $itemsDetailArray[$i]["part_no"]=$items_array[$i]->supplier_part_no;
            $itemsDetailArray[$i]["product_unit"]=$items_array[$i]->product_unit_id;
            $itemsDetailArray[$i]["product_base_quantity"]=$items_array[$i]->unit_quantity;
            $itemsDetailArray[$i]["quantity"]=$items_array[$i]->unit_quantity;
            $itemsDetailArray[$i]["product_code"]=$items_array[$i]->product_code;
            
        }
        $newPostArray['product'] = $itemsDetailArray;
        
        $payment_array = $record['payments'];
        $payment_details=array();

        for ($i=0; $i < sizeof($payment_array) ; $i++) { 
            //$payment_details[$i] = $payment_array[$i];
            $payment_details = array(
                'date' => $payment_array[$i]->date,
                'reference_no' => $payment_array[$i]->reference_no,
                'amount-paid' => $payment_array[$i]->amount,
                'paid_by' => $payment_array[$i]->paid_by,
                'pcc_no' => $payment_array[$i]->cc_no,
                'pcc_holder' => $payment_array[$i]->cc_holder,
                'pcc_year' => $payment_array[$i]->cc_year,
                'pcc_type' => $payment_array[$i]->cc_type,
                'note' => $payment_array[$i]->note,
                'created_by' => $payment_array[$i]->created_by,
            
            );
        }
        
        $newPostArray['payment_details']=$payment_details;
// echo '<pre>';print_r($newPostArray); die;

        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode( $newPostArray ),
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n"
            )
        );
        
       // $url="http://localhost/retaurant_online/api/v1/sales/postsales";
        
        $url = base_url() . "api/v1/purchases/postpurchases";

        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );

        // echo '<pre>';print_r($result); die;
        $update=false;
        if(!empty($response)) {
            $purchase_id = $response->purchase_id;
            $data = [
                'online_id' => $record['id'],
                'is_synchronize' => 1,
                'platform' => 'Online'
            ];
            $update = $this->db->where('id', $purchase_id)->update('purchases', $data);

        }

        if($update) {
            echo "Purchases Data is synchronized successfully! Local ID: " . $record["id"] . " -> Online ID: ". $purchase_id . "<br>";
        }
        else {
            echo "Something went wrong!";
        }

    }
    
    public function synchronize_purchasesdata_from_local()
    {
        $records = $this->db->limit(5)->get_where('purchases', array('is_synchronize'=>0))->result_array();

        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makePurchasesJsonDataFromLocal($record);
            }
        } else {
            echo "No data to synchronized!";
        }
        $this->data = array();
        //$this->load->view($this->theme . 'sales/synchronize_data', $this->data);
    }

    public function makePurchasesJsonDataFromLocal($record)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        $newPostArray=$record;
        // $newPostArray['api-key']="ggsk4wkssoc4sccgskggssws04gc4gokc4g4gokw";
        $newPostArray['api-key']=$settings->api_key;
        $newPostArray['warehouse']=$record["warehouse_id"];
        $newPostArray['discount']=$record["order_discount"];
        unset($newPostArray['id']);

        $items_array = $this->db->get_where('purchase_items', array('purchase_id'=>$record['id']))->result_array();
        $itemsDetailArray = array();
        for ($i=0; $i < sizeof($items_array) ; $i++) { 
            $itemsDetailArray[$i]=$items_array[$i];
            unset($itemsDetailArray['id']);
            $itemsDetailArray[$i]["net_cost"]=$items_array[$i]["net_unit_cost"];
            $itemsDetailArray[$i]["product_tax"]=$items_array[$i]["item_tax"];
            $itemsDetailArray[$i]["product_discount"]=$items_array[$i]["item_discount"];
            $itemsDetailArray[$i]["part_no"]=$items_array[$i]["supplier_part_no"];
            $itemsDetailArray[$i]["product_unit"]=$items_array[$i]["product_unit_id"];
            $itemsDetailArray[$i]["product_base_quantity"]=$items_array[$i]["unit_quantity"];
            $itemsDetailArray[$i]["discount"]=$items_array[$i]["item_discount"];
        }

        $newPostArray['product'] = $itemsDetailArray;

        $payment_array = $this->db->get_where('payments', array('purchase_id'=>$record['id']))->row_array();
        // echo "<pre>"; print_r($payment_array); die;

        $payment_details = array(
            'date' => $payment_array['date'],
            'reference_no' => $payment_array['reference_no'],
            'amount-paid' => $payment_array['amount'],
            'paid_by' => $payment_array['paid_by'],
            'pcc_no' => $payment_array['cc_no'],
            'pcc_holder' => $payment_array['cc_holder'],
            'pcc_year' => $payment_array['cc_year'],
            'pcc_type' => $payment_array['cc_type'],
            'note' => $payment_array['note'],
            'created_by' => $payment_array['created_by'],
        );
        $newPostArray['payment_details']=$payment_details;
        // echo "<pre>"; print_r($newPostArray); die;

        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode( $newPostArray ),
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n"
            )
        );
        // $url="http://localhost/retaurant_online/api/v1/sales/postsales";
        
        $url = $settings->online_link . "api/v1/purchases/postpurchases";
        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );

        // echo "<pre>";print_r($result); die();
        $update = false;
        if(!empty($response)) {
            $purchase_id = $response->purchase_id;
            $data = [
                //'sync_id' => $purchase_id,
                'is_synchronize' => 1,
                //'platform' => 'Desktop'
            ];
            $update = $this->db->where('id', $record["id"])->update('purchases', $data);
        }
        if($update) {
            echo "Purchases Data is synchronized successfully! Local ID: " . $record["id"] . " -> Online ID: ". $purchase_id . "<br>";
        }
        else {
            echo "Something went wrong!";
        }

    }
    
    private function tasks_reminders()
    {
        $reminder_before = 1; // Reminder before 1 day
        $this->db->where('status !=', 5);
        $this->db->where('duedate IS NOT NULL');
        $this->db->where('deadline_notified', 0);

        $tasks = $this->db->get('tasks')->result_array();
        $now   = new DateTime(date('Y-m-d'));

        $notifiedUsers = [];

        foreach ($tasks as $task) {

            if (date('Y-m-d', strtotime($task['duedate'])) >= date('Y-m-d')) {
                $duedate = new DateTime($task['duedate']);
                $diff    = $duedate->diff($now)->format('%a');
                // Check if difference between start date and duedate is the same like the reminder before
                // In this case reminder wont be sent becuase the task it too short
                $start_date              = strtotime($task['startdate']);
                $duedate                 = strtotime($task['duedate']);
                $start_and_due_date_diff = $duedate - $start_date;
                $start_and_due_date_diff = floor($start_and_due_date_diff / (60 * 60 * 24));

                if ($diff <= $reminder_before && $start_and_due_date_diff > $reminder_before) {
                    $assignees = $this->tasks_model->get_task_assignees($task['id']);

                    foreach ($assignees as $member) {
                        $this->db->select('email');
                        $this->db->where('id', $member['staffid']);
                        $row = $this->db->get('users')->row();
                        if ($row) {

                            $user = $this->db->get_where('users', array('id'=>$member['staffid']))->row();
                            $user_name = $user->first_name.' '.$user->last_name;
                            $comment = lang('not_task_deadline_reminder'). ' (Task #'.$task['id'].', Assigned to - '.$user_name.')';
                            $date = date('Y-m-d H:i:s');
                            $from_date = $task['startdate'];
                            $till_date = $task['duedate'];
                            $scope = 2;
                            $touserid = $member['staffid'];
                            
                            $notified = add_Notification($comment, $date, $from_date, $till_date, $scope, $touserid);

                            //send_mail_template('task_deadline_reminder_to_staff', $row->email, $member['staffid'], $task['id']);

                            $this->db->where('id', $task['id']);
                            $this->db->update('tasks', [
                                'deadline_notified' => 1,
                            ]);
                        }
                    }
                }
            }
        }

    }

    private function staff_reminders()
    {
        $this->db->select('task_reminders.*, users.email, users.phone');
        $this->db->join('users', 'users.id=task_reminders.staff');
        $this->db->where('isnotified', 0);
        $reminders     = $this->db->get('task_reminders')->result_array();
        $notifiedUsers = [];

        foreach ($reminders as $reminder) {
            if (date('Y-m-d H:i:s') >= $reminder['date']) {
                $this->db->where('id', $reminder['id']);
                $this->db->update('task_reminders', [
                    'isnotified' => 1,
                ]);
                $taskid = $reminder['rel_id'];


                $user = $this->db->get_where('users', array('id'=>$reminder['staff']))->row();
                $user_name = $user->first_name.' '.$user->last_name;
               // $comment = ('Task Reminder'). ' (Task #'.$task['id'].', Assigned to - '.$user_name.')';
                $title = 'Task Reminder';
                $comment = 'Task Reminder- Task #'.$taskid. '. Assigned to- ' . $user_name . ' - ' . $reminder['description'];
                $date = date('Y-m-d H:i:s');
                $from_date = $date;
                $till_date = $reminder['date'];
                $scope = 2;

                $user_id = $reminder['staff'];
                $target_type = 'indivisual';

                $notified = add_Notification($title, $comment, $date, $from_date, $till_date, $scope, $user_id, $target_type);

                //$template = mail_template('staff_reminder', $reminder['email'], $reminder['staff'], $reminder);

                //if ($reminder['notify_by_email'] == 1) {
                    //$template->send();
                //}

                //$this->app_sms->trigger(SMS_TRIGGER_STAFF_REMINDER, $reminder['phone'], $template->get_merge_fields());
            }
        }

        //pusher_trigger_notification($notifiedUsers);
    }

    public function recurring_tasks()
    {


        $this->db->select('id,addedfrom,recurring_type,repeat_every,last_recurring_date,startdate,duedate');
        $this->db->where('recurring', 1);
        $this->db->where('(cycles != total_cycles OR cycles=0)');
        $recurring_tasks = $this->db->get('tasks')->result_array();

        

        foreach ($recurring_tasks as $task) {
            $type                = $task['recurring_type'];
            $repeat_every        = $task['repeat_every'];
            $last_recurring_date = $task['last_recurring_date'];
            $task_date           = $task['startdate'];

            // Current date
            $date = new DateTime(date('Y-m-d'));
            // Check if is first recurring
            if (!$last_recurring_date) {
                $last_recurring_date = date('Y-m-d', strtotime($task_date));
            } else {
                $last_recurring_date = date('Y-m-d', strtotime($last_recurring_date));
            }

            $re_create_at = date('Y-m-d', strtotime('+' . $repeat_every . ' ' . strtoupper($type), strtotime($last_recurring_date)));

            

            if (date('Y-m-d') >= $re_create_at) {
                $copy_task_data['copy_task_followers']       = 'true';
                $copy_task_data['copy_task_checklist_items'] = 'true';
                $copy_task_data['copy_task_assignees'] = 'true';
                $copy_task_data['copy_from']                 = $task['id'];

                $overwrite_params = [
                    'startdate'           => $re_create_at,
                    'status'              => 1,
                    'recurring_type'      => null,
                    'repeat_every'        => 0,
                    'cycles'              => 0,
                    'recurring'           => 0,
                    'custom_recurring'    => 0,
                    'last_recurring_date' => null,
                    'is_recurring_from'   => $task['id'],
                ];

                if (!empty($task['duedate'])) {
                    $dStart                      = new DateTime($task['startdate']);
                    $dEnd                        = new DateTime($task['duedate']);
                    $dDiff                       = $dStart->diff($dEnd);
                    $overwrite_params['duedate'] = date('Y-m-d', strtotime('+' . $dDiff->days . ' days', strtotime($re_create_at)));
                }
// echo "<pre>";print_r($copy_task_data); print_r($overwrite_params);
//die();
                $newTaskID = $this->tasks_model->copy_task($copy_task_data, $overwrite_params);

                if ($newTaskID) {
                    $this->db->where('id', $task['id']);
                    $this->db->update('tasks', [
                        'last_recurring_date' => $re_create_at,
                    ]);

                    $this->db->where('id', $task['id']);
                    $this->db->set('total_cycles', 'total_cycles+1', false);
                    $this->db->update('tasks');

                    // $this->db->where('taskid', $task['id']);
                    // $assigned = $this->db->get('task_assigned')->result_array();
                    // foreach ($assigned as $assignee) {
                    //     $assigneeId = $this->tasks_model->add_task_assignees([
                    //         'taskid'   => $newTaskID,
                    //         'assignee' => $assignee['staffid'],
                    //     ], true);

                    //     if ($assigneeId) {
                    //         $this->db->where('id', $assigneeId);
                    //         $this->db->update('task_assigned', ['assigned_from' => $task['addedfrom']]);
                    //     }
                    // }
                }
            }
        }
    }

    public function synchronize_expenses_from_local()
    {
        $records = $this->db->limit(5)->get_where('expenses', array('is_synchronize'=>0))->result_array();
        // echo "<pre>"; print_r($records);die;
        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makeExpensesJsonFromLocal($record);
            }
        } else {
            echo "No data to synchronized!";
        }
        $this->data = array();
        //$this->load->view($this->theme . 'sales/synchronize_data', $this->data);
    }

    function makeExpensesJsonFromLocal($record)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        $newPostArray=$record;
        $newPostArray['api-key']=$settings->api_key;
        $newPostArray['local_id']=$sale["id"];//
        $newPostArray['platform']="Local";//
        $newPostArray['is_synchronize']="1";//

        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode( $newPostArray ),
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n"
            )
        );
        // $url="http://localhost/retaurant_online/api/v1/sales/postexpenses";
        $url = $settings->online_link . "api/v1/purchases/postexpenses";
        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );

        if(!empty($response)) {
            $expense_id = $response->expense_id;
            $data = [
                //'sync_id' => $expense_id,
                'is_synchronize' => 1,
                //'platform' => 'Desktop'
            ];
            $update = $this->db->where('id', $record["id"])->update('expenses', $data);
        }
        if($update) {
            echo "Expense Data is synchronized successfully! Local ID: " . $record["id"] . " -> Online ID: ". $expense_id . "<br>";
        }
        else {
            echo "Something went wrong!";
        }
    }

    public function synchronize_expenses_from_online()
    {
        $settings = $this->site->get_setting(); 
        $url = $settings->online_link;
        $api_key = $settings->api_key;

        $url = $url."api/v1/synchronize_data/allexpenses?api-key=".$api_key;
        $json = file_get_contents($url);
        $records = json_decode($json);
        $records = $records->records_online;
        // echo "<pre>";var_dump($records);die;
        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makeExpensesFromOnline((array)$record);
            }
        } else {
            echo "No data to synchronized!";
        }
    }

    public function makeExpensesFromOnline($record)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        $newPostArray = $record;
        $newPostArray['api-key']=$settings->api_key;
        unset($newPostArray['id']);

        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode( $newPostArray ),
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n"
            )
        );
        
       // $url="http://localhost/retaurant_online/api/v1/sales/postsales";
        
        $url = base_url() . "api/v1/purchases/postexpenses";

        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );

        // echo '<pre>';print_r($response); die;

        if(!empty($response)) {
            $expense_id = $response->expense_id;
            $data = [
                'online_id' => $expense_id,
                'is_synchronize' => 1,
                'platform' => 'Online'
            ];
            $update = $this->db->where('id', $record["id"])->update('expenses', $data);

        }

        if($update) {
            echo "Expenses Data is synchronized successfully! Local ID: " . $record["id"] . " -> Online ID: ". $expense_id . "<br>";
        }
        else {
            echo "Something went wrong!";
        }

    }

    public function add_roster_all_employees()
    {

        $employees = $this->employees_model->all_employees();
        $year = date('Y');
        
        // echo '<pre>'; print_r($period_master_all); die;
        foreach($employees as $employee) {

            $period_master_all = $this->db->where('year', $year)->get('period_master')->result();

            $emp_mst_shift = $this->roster_model->get_emp_shifts($employee->user_id);
            if(!empty($emp_mst_shift)) {

            foreach($period_master_all as $period_master) {

                $period_mst_id = $period_master->id;
                $department_id = $employee->department_id;
                // echo '<pre>';print_r($emp_mst_shift);die;
                
                //foreach($emp_mst_shifts as $emp_mst_shift) {

                    $shift_master = $this->roster_model->get_shift_master($emp_mst_shift->shift_mst_id);
                    $check_roster = $this->roster_model->check_emp_roster($employee->user_id, $period_mst_id);

                    $period_master = $this->attendance_model->read_period_master($period_mst_id);
                    $start_date = $period_master->start_date;
                    $end_date = $period_master->end_date;
                    $current_year = date('Y', strtotime($period_master->start_date));
                    $current_month = date('n', strtotime($period_master->start_date));

                    $emp_name = $this->employees_model->get_emp_name($employee->user_id);
                    $caption = $emp_name->first_name. ' '.$emp_name->last_name . ' - ' . $period_master->name;

                    if(empty($check_roster)) {
                        $data_roster = array(
                            'caption' => $caption,
                            'period_mst_id' => $period_mst_id,
                            'department_id' => $department_id,
                            'employee_id' => $employee->user_id,
                            'current_year' => $current_year,
                            'current_month' => $current_month,
                            'remark' => $this->input->post('remark'),
                            'created_by' => $this->session->userdata('user_id'),
                            'created_at' => date('Y-m-d')
                        );
                        
                        $roster_id = $this->roster_model->add_roster($data_roster);
                        // echo '<pre>';print_r($roster_id);die;
                        // $roster_id = 1;
                        $dateDay = $start_date;
                        while ($dateDay <= $end_date) {

                            $week_days = date('l', strtotime($dateDay)); 
                            $week_day = strtoupper($week_days);

                            if($week_day == 'FRIDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->fri_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');

                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->fri_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->fri_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->fri_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'SATURDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->sat_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                // print_r($entry_roster);die;
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->sat_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->sat_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->sat_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'SUNDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->sun_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->sun_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->sun_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->sun_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'MONDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->mon_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->mon_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->mon_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->mon_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'TUESDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->tues_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->tues_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->tues_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->tues_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'WEDNESDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->wed_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->wed_end_time);
                                } else {
                                    $exit_roster = 
                                    new DateTime($dateDay .' '. $emp_mst_shift->wed_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->wed_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            if($week_day == 'THURSDAY') {
                                $entry_roster = new DateTime($dateDay .' '. $emp_mst_shift->thurs_start_time);
                                $entry_roster_time = $entry_roster->format('Y-m-d H:i:s');
                                
                                if($shift_master->day_duration == 'NightShift') {
                                    $dateDayNext = date('Y-m-d', strtotime('tomorrow',strtotime($dateDay)));
                                    $exit_roster = new DateTime($dateDayNext .' '. $emp_mst_shift->thurs_end_time);
                                } else {
                                    $exit_roster = new DateTime($dateDay .' '. $emp_mst_shift->thurs_end_time);
                                }
                                $exit_roster_time = $exit_roster->format('Y-m-d H:i:s');
                                if($emp_mst_shift->thurs_off == 1) {
                                    $is_weekend = 1;
                                } else {
                                    $is_weekend = 0;
                                }
                            }
                            $roster_diff = $exit_roster->diff($entry_roster);
                            $hours = $roster_diff->format('%h'); 
                            $minutes = $roster_diff->format('%i');
                            $roster_minute = $hours * 60 + $minutes;
                            
                            $tolerance_date = clone($entry_roster);
                            $entry_roster_tol = $tolerance_date->add(new DateInterval('PT'.$emp_mst_shift->late_tolerance.'M'));
                            $entry_roster_time_tol = $entry_roster_tol->format('Y-m-d H:i:s');
                            // print_r($entry_roster); die;
                            $overtime_enable = $emp_mst_shift->overtime_enable;
                            $auto_rostering = $emp_mst_shift->auto_rostering;
                            $log_required = $emp_mst_shift->log_required;
                            $logout_required = $emp_mst_shift->logout_required;
                            $half_day_absent = $emp_mst_shift->half_day_absent;
                            $overtime_minute = 0;

                            if(!empty($emp_mst_shift->overtime_after)) {
                                $overtime_after_date = clone($exit_roster);
                                $overtime_after_add = $overtime_after_date->add(new DateInterval('PT'.$emp_mst_shift->overtime_after.'M'));
                                $overtime_after = $overtime_after_add->format('Y-m-d H:i:s');
                            } else {
                                $overtime_after = null;
                            }
                            if(!empty($emp_mst_shift->half_day_leave)) {
                                $half_day_date = clone($entry_roster);
                                $half_day_time_add = $half_day_date->add(new DateInterval('PT'.$emp_mst_shift->half_day_leave.'M'));
                                $half_day_time = $half_day_time_add->format('Y-m-d H:i:s');
                            } else {
                                $half_day_time = null;
                            }
                            if(isset($emp_mst_shift->start_punch)) {
                                $start_punch_time_date = clone($entry_roster);
                                // print_r($entry_roster); die;
                                $start_punch_time_add = $start_punch_time_date->add(new DateInterval('PT'.$emp_mst_shift->start_punch.'M'));
                                $start_punch_time = $start_punch_time_add->format('Y-m-d H:i:s');
                            } else {
                                $start_punch_time = null;
                            }
                            if(isset($emp_mst_shift->end_punch)) {
                                $end_punch_time_date = clone($exit_roster);
                                $end_punch_time_add = $end_punch_time_date->add(new DateInterval('PT'.$emp_mst_shift->end_punch.'M'));
                                $end_punch_time = $end_punch_time_add->format('Y-m-d H:i:s');
                            } else {
                                $end_punch_time = null;
                            }
                            
                            $caption = date('d',strtotime($dateDay)).'-'. date('m',strtotime($dateDay)).'-'.date('Y',strtotime($dateDay));
                            $emp_name = $this->employees_model->get_emp_name($employee->user_id);
                            $emp_full_name = $emp_name->first_name.' '.$emp_name->last_name;

                            $data_roster_detail = array(
                                'roster_id' => $roster_id,
                                'roster_date' => $dateDay,
                                'shift_id' => $emp_mst_shift->shift_mst_id,
                                'emp_shift_id' => $emp_mst_shift->id,
                                'day_duration' => $shift_master->day_duration,
                                'period_id' => $period_mst_id,
                                'department_id' => $department_id,
                                'employee_id' => $employee->user_id,
                                'caption' => $caption,
                                'employee_caption' => $emp_full_name,
                                'employee_caption_alt' => $emp_full_name,
                                'card_no' => $emp_mst_shift->card_number,
                                'entry_roster_time' => $entry_roster_time,
                                'exit_roster_time' => $exit_roster_time,
                                'roster_minute' => $roster_minute,
                                'entry_roster_time_tol' => $entry_roster_time_tol,
                                'overtime_enable' => $overtime_enable,
                                'auto_rostering' => $auto_rostering,
                                'log_required' => $log_required,
                                'logout_required' => $logout_required,
                                'half_day_absent' => $half_day_absent,
                                'overtime_minute' => $overtime_minute,
                                'overtime_after' => $overtime_after,
                                'half_day_time' => $half_day_time,
                                'start_punch_time' => $start_punch_time,
                                'end_punch_time' => $end_punch_time,
                                'is_weekend' => $is_weekend,
                                'created_by' => $this->session->userdata('user_id'),
                                'created_at' => date('Y-m-d')
                            );
                            // echo '<pre>'; print_r($data_roster_detail); die;
                            $this->roster_model->add_roster_detail($data_roster_detail);
                            $dateDayNext = strtotime('+1 day', strtotime($dateDay));
                            $dateDay = date('Y-m-d', $dateDayNext);
                        }
                        
                    } else {
                       
                    }
                //}
                } // .End foreach period_master_all
                echo lang('roster_is_created_successfully'). '. Employee Id: ' . $employee->user_id . '. Name: ' . $employee->first_name . ' ' . $employee->last_name . '<br>';
            } 
            else {
                echo lang('shift_is_not_assigned_for_this_employee'). '. Employee Id: ' . $employee->user_id. '. Name: ' . $employee->first_name . ' ' . $employee->last_name . '<br>';
            }

        } // .End foreach employees

    }

    public function roster_process_all_employees()
    {

        $employees = $this->employees_model->all_employees();

        if($employees) {

            $roster_end_date = date('Y-m-d');
            $start_date_str = strtotime('-7 days', strtotime($roster_end_date));
            $roster_start_date = date('Y-m-d', $start_date_str);
            $roster_date = $roster_start_date;
            
            while($roster_date <= $roster_end_date) {

            $emp_ids = [];
            $emp_ids_string ='';
            foreach($employees as $employee) {
                $emp_ids[] = $employee->user_id; 
            }
            $emp_ids_string = implode(',', $emp_ids);
            /* 
            |-----------------------------------------------------
            | calculate work_minute, total_roster_minute, present_status,    leave_status etc
            | join roster_details table with attendance_log
            |-----------------------------------------------------
            */
            $sql = "
                select roster.id, roster.employee_id,
                min(log.log_day_time) as entry_time,
                max(log.log_day_time) as exit_time,
                TIMESTAMPDIFF(MINUTE, min(log.log_day_time), max(log.log_day_time)) as work_minute,
                TIMESTAMPDIFF(MINUTE, roster.entry_roster_time, roster.exit_roster_time) as roster_minute,
                CASE
                    WHEN (roster.overtime_enable = 1) THEN DATE_FORMAT(roster.overtime_after, '%d-%m-%Y %h:%i %p')
                    ELSE '-'
                    END as overtime_after,
                CASE
                    WHEN min(log.log_day_time) is null THEN 'Absent'
                    WHEN roster.exit_roster_time > max(log.log_day_time) THEN 'EarlyLeave'
                    WHEN roster.entry_roster_time_tol < min(log.log_day_time) THEN 'LatePresent'
                    ELSE 'Regular'
                    END as present_status,
                CASE
                    WHEN max(log.log_day_time) is null THEN 'Absent'
                    WHEN roster.half_day_time < min(log.log_day_time) THEN 'Half_Day_Leave'
                    ELSE 'Regular'
                    END as leave_status,
                CASE
                    WHEN (roster.overtime_enable = 1) and 
                    TIMESTAMPDIFF(MINUTE, max(log.log_day_time), roster.overtime_after) > roster.overtime_minute
                       THEN
                            TIMESTAMPDIFF(MINUTE, max(log.log_day_time), roster.overtime_after)
                    ELSE 0
                    END as overtime_minute,
                GROUP_CONCAT(DATE_FORMAT(log.log_day_time, '%h:%i %p')) as punch_log        
               
                from {$this->db->dbprefix('hrm_roster_details roster')} 
                left join {$this->db->dbprefix('hrm_attendance_log log')} 
                        on (log.card_no = roster.card_no and log.log_day_time >= roster.start_punch_time and log.log_day_time <= roster.end_punch_time)
                where roster.roster_date = '".$roster_date."' and roster.employee_id IN (".$emp_ids_string.")
               
            ";

            $results = $this->db->query($sql)->result();
            $sql_holiday = "";
// echo '<pre>'; print_r($results); die;
            foreach($results as $row) {

                $roster_detail = $this->roster_model->read_roster_detail($row->id);
                $holiday = $this->roster_model->holiday_date($roster_date);    
                $travel = $this->roster_model->travel_date($row->employee_id, $roster_date);
                $leave_application = $this->roster_model->leave_application_date($row->employee_id, $roster_date);
                // Calculate salary_status
                if ($row->present_status == 'Absent') {
                    if ($roster_detail->leave_application_id) {
                        $salaryStatus = 'InLeave';
                    } else if ($roster_detail->travel_id) {
                        $salaryStatus = 'InTour';
                    } else if ($roster_detail->holiday_id) {
                        $salaryStatus = 'Holidays';
                    } else if ($roster_detail->is_weekend) {
                        $salaryStatus = 'Weekend';
                    } else {
                        $salaryStatus = 'Unauthorized_Leave';
                    }
                } else if ($row->leave_status == 'Half_Day_Leave') {
                    $salaryStatus = 'Half_Day_Leave';
                } else if ($row->present_status == 'LatePresent') {
                    $salaryStatus = 'LatePresent';
                } else if ($row->present_status == 'EarlyLeave') {
                    $salaryStatus = 'EarlyLeave';
                } else {
                    $salaryStatus = 'Regular';
                }

                $data_roster_detail = array(
                    'work_minute' => $row->work_minute,
                    'roster_minute' => $row->roster_minute,
                    'entry_time' => $row->entry_time,
                    'exit_time' => $row->exit_time,
                    'present_status' => $row->present_status,
                    'leave_status' => $row->leave_status,
                    'petrol_log' => $row->punch_log,
                    'overtime_minute' => $row->overtime_minute,
                    'leave_application_id' => $leave_application ? $leave_application->leave_id : null,
                    'travel_id' => $travel ? $travel->travel_id : null,
                    'holiday_id' => $holiday ? $holiday->holiday_id : null,
                    'salary_status' => $salaryStatus
                );
                $this->roster_model->update_roster_detail($data_roster_detail, $row->id);
            }

                $roster_next_date = strtotime('+1 day', strtotime($roster_date));
                $roster_date = date('Y-m-d', $roster_next_date);
            } // .End roster_date while loop

            echo '<br>'. lang('roster_processing'). ': ' .lang('roster_is_processed_for_all_employees.');
            
        }
    }
	public function synchronize_raw_purchase_from_local()
    {
        $records = $this->db->limit(100)->get_where('purchases_raw', array('is_synchronize'=>0))->result_array();
        if(!empty($records)) {
            foreach($records as $record) {
                    $this->makeRawPurchaseJsonFromLocal($record);
            }
        } else {
            echo lang("no_data_to_synchronize");
        }
        $this->data = array();
    }
    function makeRawPurchaseJsonFromLocal($record)
    {
        $newPostArray=array();
        $settings = $this->site->get_setting(); 
        $newPostArray=$record;
		$newPostArray['api-key']=$settings->api_key;
		$newPostArray['is_syncronize']="1";//
		
		$items_array = $this->db->get_where('purchase_items_raw', array('purchase_id '=>$record['id']))->result_array();
		$itemsDetailArray = array();

		for ($i=0; $i < sizeof($items_array) ; $i++) { 
			$itemsDetailArray[$i]=$items_array[$i];
		}
		$newPostArray['details'] = $itemsDetailArray;

		$payment_array = $this->db->get_where('payments', array('purchase_raw_id'=>$record['id']))->result_array();
		$payment_details=array();

		for ($i=0; $i < sizeof($payment_array) ; $i++) { 
			$payment_details[$i] = $payment_array[$i];
		}
		$newPostArray['payment_details']=$payment_details;
		
		$options = array(
            'http' => array(
              'method'  => 'POST',
              'content' => json_encode( $newPostArray ),
              'header'=>  "Content-Type: application/json\r\n" .
                          "Accept: application/json\r\n"
              )
          );
		   		  
		// $url = "https://eshtri.net/resturant_test/api/v1/purchases_raw/addcategories";
		$url = $settings->online_link."api/v1/purchases_raw/add_raw_purchases";
        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );
// echo "<pre>"; print_r($newPostArray); die;
		$update = false;
        if(!empty($response)) {
            $insert_id = $response->insert_id;
            $data = [
                'is_synchronize' => 1,
            ];
            $update = $this->db->where('id', $record["id"])->update('purchases_raw', $data);
        }
        if($update) {
            echo "Data is synchronized successfully! Local ID: " . $record["id"] ."<br>";
        }
        else {
            echo "Something went wrong!";
        }
    }
    public function delete_sales_data()
    {
        // if time difference 1 year or 365 days 
        $records = $this->db->where('DATEDIFF(NOW(), date) > 365')->get('sales')->result();

        if(!empty($records)) {
            foreach($records as $record) {
                $this->sales_model->deleteSale($record->id);
                echo 'sales data deleted' . '<br>';
            }
        } else {
            echo 'no data to delete' . '<br>';
        }
    }
    
    public function delete_purchase_data()
    {
        // if time difference 1 year or 365 days 
        $records = $this->db->where('DATEDIFF(NOW(), date) > 365')->get('purchases')->result();

        if(!empty($records)) {
            foreach($records as $record) {
                $this->purchases_model->deletePurchase($id);
                echo 'purchase data deleted' . '<br>';
            }
        } else {
            echo 'no data to delete' . '<br>';
        }
    }
    public function delete_purchase_raw_data()
    {
        // if time difference 1 year or 365 days 
        $records = $this->db->where('DATEDIFF(NOW(), date) > 365')->get('purchases_raw')->result();

        if(!empty($records)) {
            foreach($records as $record) {
                $deleted = $this->db->where('id', $record->id)->delete('purchases_raw');
                if($deleted) {
                    $this->db->where('purchase_id ', $record->id)->delete('purchase_items_raw');
                    $this->db->where('purchase_id ', $record->id)->delete('purchase_items_raw');
                    $this->db->where('purchase_raw_id ', $record->id)->delete('payments');
                } 
                echo 'purchase data deleted' . '<br>';
            }
        } else {
            echo 'no data to delete' . '<br>';
        }
    }
    public function delete_attendance_log()
    {
        // if time difference 1 year or 365 days 
        $records = $this->db->where('DATEDIFF(NOW(), log_day_time) > 365')->get('hrm_attendance_log')->result();

        if(!empty($records)) {
            foreach($records as $record) {
                $deleted = $this->db->where('id', $record->id)->delete('hrm_attendance_log');
                if($deleted) {
                    echo 'attendance data deleted' . '<br>';
                } 
            }
        } else {
            echo 'no data to delete' . '<br>';
        }
    }
    public function delete_api_log()
    {
        // if time difference 1 year or 365 days 
        $records = $this->db->where('DATEDIFF(NOW(), time) > 365')->get('api_logs')->result();

        if(!empty($records)) {
            foreach($records as $record) {
                $deleted = $this->db->where('id', $record->id)->delete('api_logs');
                if($deleted) {
                    echo 'api log data deleted' . '<br>';
                } 
            }
        } else {
            echo 'no data to delete' . '<br>';
        }
    }
    public function delete_sma_logs()
    {
        // if time difference 1 year or 365 days 
        $records = $this->db->where('DATEDIFF(NOW(), date) > 365')->get('logs')->result();

        if(!empty($records)) {
            foreach($records as $record) {
                $deleted = $this->db->where('id', $record->id)->delete('logs');
                if($deleted) {
                    echo 'logs data deleted' . '<br>';
                } 
            }
        } else {
            echo 'no data to delete' . '<br>';
        }
    }
    public function xdelete_purchase_data()
    {

        $records = $this->db->get('purchases')->result();
        echo '<pre>'; print_r($records); die;
        $now = time();
        if(!empty($records)) {
            foreach($records as $record) {
                // if time difference 1 year or 365 days 
                $time_diff = $now - strtotime($record->date);
                if($time_diff >= (365 * 24 * 60 * 60)) {
                    $deleted = $this->db->where('id', $record->id)->delete('purchases');
                    if($deleted) {
                        $this->db->where('purchase_id ', $record->id)->delete('purchase_items');
                    } 
                    echo 'purchase data deleted' . '<br>';
                }
            }
            
        } else {
            echo 'no data to delete' . '<br>';
        }
        
    }

}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Department_model extends CI_Model {

	public function __construct() 
	{
		parent :: __construct();
	}

	public function all_departments()
	{
		return $this->db->get('departments')->result();
	}

	public function add_department($data='')
    {
    	$insert = $this->db->insert('departments', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function editDepartment($data, $department_id)
    {
    	$update = $this->db->where('department_id', $department_id)->update('departments', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function delete_department($id='')
    {
    	$delete = $this->db->where('department_id', $id)->delete('departments');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }
	
	public function read_department_information($id) {
	
		$sql = 'SELECT * FROM '. $this->db->dbprefix('departments').' WHERE department_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}
}
<?php


class Designation_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function all_designations()
	{
		return $this->db->get('designations')->result();
	}

	public function add_designation($data='')
	{
		$insert = $this->db->insert('designations', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
	}

	public function editDesignation($data, $designation_id)
    {
    	$update = $this->db->where('designation_id', $designation_id)->update('designations', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

	public function delete_designation($id='')
    {
    	$delete = $this->db->where('designation_id', $id)->delete('designations');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }
	public function read_designation_information($id) {
	
		$sql = 'SELECT * FROM '. db_prefix().'designations WHERE designation_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}
	

}
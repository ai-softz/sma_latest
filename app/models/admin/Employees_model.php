<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Employees_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // Function to add record in table
	public function addEmployee($data){
		$this->db->insert('employees', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateEmployee($data, $emp_id){
		$this->db->where('user_id', $emp_id)->update('employees', $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function update_user($additional_data, $emp_id)
	{
		$update = $this->db->where('emp_id', $emp_id)->update('users', $additional_data);
		if ($update) {
			return true;
		} else {
			return false;
		}
	}

	public function read_employee_information($emp_id)
	{
		$emp_info = $this->db->select('employees.*, users.id as usersid, users.emp_id')
			->from('employees')
			->join('users', 'users.emp_id = employees.user_id', 'left')
			->where(array('users.emp_id' => $emp_id))
            ->group_by('employees.user_id')
			->get()
			->row();
		return $emp_info;
	}

	public function getGroupsEmp()
    {
        $q = $this->db->where_not_in('id', array(1,2,3,4))->get('groups');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function all_office_shifts()
    {
    	return $this->db->get('office_shift')->result();
    }
    public function leave_categories()
    {
    	return $this->db->get('leave_type')->result_array();
    }

    public function all_employees()
    {
    	return $this->db->where('user_role_id !=', 1)->get('employees')->result();
    }


    public function delete_employee($user_id = null)
    {

        $delete = $this->db->where('user_id', $user_id)->delete('employees');
        if($delete) {
            return true;
        } else {
            return false;
        }

    }

    public function get_countries()
    {
    	return $this->db->get('countries')->result();
    }

    public function add_working_experience($data='')
    {
    	$insert = $this->db->insert('employee_work_experience', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }
    public function update_working_experience($data='', $work_experience_id='')
    {
    	$insert = $this->db->where('work_experience_id', $work_experience_id)->update('employee_work_experience', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function add_emergency_contact($data)
    {
    	$insert = $this->db->insert('employee_contacts', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function delete_work_experience($work_experience_id='')
    {
    	$delete = $this->db->where('work_experience_id', $work_experience_id)->delete('employee_work_experience');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function delete_qualification($qualification_id='')
    {
    	$delete = $this->db->where('qualification_id', $qualification_id)->delete('employee_qualification');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function qualification_languages()
    {
    	return $this->db->get('qualification_language')->result();
    } 
    public function qualification_skill()
    {
    	return $this->db->get('qualification_skill')->result();
    }
 
    public function education_level()
    {
    	return $this->db->get('education_level')->result();
    }

    public function add_qualification($data='')
    {
    	$insert = $this->db->insert('employee_qualification', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function get_emergency_contact($contact_id='')
    {
    	return $this->db->get_where('employee_contacts', array('contact_id'=>$contact_id))->row();
    }

    public function add_emp_bank_account($data='')
    {
    	$insert = $this->db->insert('employee_bankaccount', $data);
    	
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function get_emp_bank_account($id='')
    {
    	return $this->db->get_where('employee_bankaccount', array('bankaccount_id'=>$id))->row();
    }

    public function edit_emp_bank_account($data='', $id)
    {
    	$update = $this->db->where('bankaccount_id', $id)->update('employee_bankaccount', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function delete_emp_bank_account($id='') 
    {
    	$delete = $this->db->where('bankaccount_id', $id)->delete('employee_bankaccount');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function update_emergency_contact($data='', $contact_id='')
    {
    	$update = $this->db->where('contact_id', $contact_id)->update('employee_contacts', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function get_document_types()
    {
    	return $this->db->get('document_type')->result();
    }

    public function add_employee_document($data='')
    {

    	$insert = $this->db->insert('employee_documents', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }
    public function add_document_type($data='')
    {
    	$insert = $this->db->insert('document_type', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }
    public function delete_document_type($id='')
    {
    	$delete = $this->db->where('document_type_id', $id)->delete('document_type');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    public function get_employee_document($id='')
    {
    	return $this->db->get_where('employee_documents', array('document_id'=>$id))->row();
    }
    public function get_document_type($id='')
    {
    	return $this->db->get_where('document_type', array('document_type_id'=>$id))->row();
    }
    public function delete_employee_immigration($id='')
    {
    	$delete = $this->db->where('immigration_id', $id)->delete('employee_immigration');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function edit_employee_document($data='', $document_id='')
    {
    	$update = $this->db->where('document_id', $document_id)->update('employee_documents', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }
    public function edit_document_type($data='', $document_type_id='')
    {
    	$update = $this->db->where('document_type_id', $document_type_id)->update('document_type', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function add_employee_immigration($data)
    {
    	$insert = $this->db->insert('employee_immigration', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function get_employee_immigration($id)
    {
    	return $this->db->get_where('employee_immigration', array('immigration_id'=>$id))->row();
    }

    public function edit_employee_immigration($data, $immigration_id)
    {
    	$update = $this->db->where('immigration_id', $immigration_id)->update('employee_immigration', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function update_social_info($data='', $emp_id)
    {
    	$update = $this->db->where('user_id', $emp_id)->update('employees', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function check_old_passwprd($old_password, $emp_id)
    {
    	
    }

    public function get_contract_types()
    {
    	return $this->db->get('contract_type')->result();
    }

    public function add_emp_contract($data)
    {
    	$insert = $this->db->insert('employee_contract', $data);
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function delete_emp_contract($id='')
    {
    	$delete = $this->db->where('contract_id', $id)->delete('employee_contract');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function get_emp_contract($id)
    {
    	return $this->db->get_where('employee_contract', array('contract_id'=>$id))->row();
    }

    public function update_emp_contract($data, $contract_id)
    {
    	$update = $this->db->where('contract_id', $contract_id)->update('employee_contract', $data);
    	if($update) {
    		return true;
    	} else {
    		return false;
    	}
    }

    
    public function update_employee_salary($data, $emp_id)
    {
        $update = $this->db->where('user_id', $emp_id)->update('employees', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function add_employee_allowances($data)
    {
        $insert = $this->db->insert('salary_allowances', $data);
        // print_r($this->db->error());die;
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_emp_allowance($id='')
    {
        $delete = $this->db->where('allowance_id', $id)->delete('salary_allowances');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_emp_allowance($id='')
    {
        return $this->db->get_where('salary_allowances', array('allowance_id'=>$id))->row();
    }
    
    public function editEmpAllowance($data, $allowance_id)
    {
        $update = $this->db->where('allowance_id', $allowance_id)->update('salary_allowances', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function add_employee_commisions($data)
    {
        $insert = $this->db->insert('salary_commissions', $data);
        // print_r($this->db->error()); die;
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_emp_commisions($id='')
    {
        $delete = $this->db->where('salary_commissions_id', $id)->delete('salary_commissions');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function get_emp_commisions($id)
    {
        return $this->db->get_where('salary_commissions', array('salary_commissions_id'=>$id))->row();
    }
    
    public function editEmpCommission($data, $salary_commissions_id)
    {
        $update = $this->db->where('salary_commissions_id', $salary_commissions_id)->update('salary_commissions', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    
    public function add_employee_statutory_deductions($data)
    {
        $insert = $this->db->insert('salary_statutory_deductions', $data);
        // print_r($this->db->error()); die;
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_statutory_deductions($id='')
    {
        $delete = $this->db->where('statutory_deductions_id', $id)->delete('salary_statutory_deductions');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_statutory_deductions($id)
    {
        return $this->db->get_where('salary_statutory_deductions', array('statutory_deductions_id'=>$id))->row();
    }
    
    public function editStatutoryDeductions($data, $statutory_deductions_id)
    {
        $update = $this->db->where('statutory_deductions_id', $statutory_deductions_id)->update('salary_statutory_deductions', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function add_other_payment($data)
    {
        $insert = $this->db->insert('salary_other_payments', $data);
        // print_r($this->db->error()); die;
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_other_payment($id)
    {
        $delete = $this->db->where('other_payments_id', $id)->delete('salary_other_payments');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_other_payment($id)
    {
        return $this->db->get_where('salary_other_payments', array('other_payments_id'=>$id))->row();
    }
    
    public function editOtherPayment($data, $other_payments_id)
    {
        $update = $this->db->where('other_payments_id', $other_payments_id)->update('salary_other_payments', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function add_salary_overtime($data)
    {
        $insert = $this->db->insert('salary_overtime', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_salary_overtime($id)
    {
        $delete = $this->db->where('salary_overtime_id', $id)->delete('salary_overtime');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_salary_overtime($id)
    {
        return $this->db->get_where('salary_overtime', array('salary_overtime_id'=>$id))->row();
    }
    public function editSalaryOvertime($data, $salary_overtime_id)
    {
        $update = $this->db->where('salary_overtime_id', $salary_overtime_id)->update('salary_overtime', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    //Calculates how many months is past between two timestamps.
    public function get_month_diff($start, $end = FALSE) {
        $end OR $end = time();
        $start = new DateTime($start);
        $end   = new DateTime($end);
        $diff  = $start->diff($end);
        return $diff->format('%y') * 12 + $diff->format('%m');
    }
    
    public function add_employee_loan($data)
    {
        $insert = $this->db->insert('salary_loan_deductions', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_employee_loan($id)
    {
        $delete = $this->db->where('loan_deduction_id', $id)->delete('salary_loan_deductions');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_employee_loan($id)
    {
        return $this->db->get_where('salary_loan_deductions', array('loan_deduction_id'=>$id))->row();
    }
    public function editEmployeeLoan($data, $loan_deduction_id)
    {
        $update = $this->db->where('loan_deduction_id', $loan_deduction_id)->update('salary_loan_deductions', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function get_emp_name($user_id)
    {
        return $this->db->select('first_name, last_name')->get_where('employees', array('user_id'=>$user_id))->row();
    }
    public function get_emp_info($user_id)
    {
        return $this->db->select('*')->get_where('employees', array('user_id'=>$user_id, 'user_role_id !=' => 1 ))->row();
    }
    // get single employee
	public function read_user_info($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'employees WHERE user_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
		
	}
    
    public function get_all_users() {
	
        return $this->db->select('id, email, first_name, last_name')->from('users')->get()->result();
    }

}
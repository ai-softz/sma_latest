<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_attendance_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_attendance_log($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $empid, $department_id, $start_date, $end_date)
    {
        $this->db
            ->select('*')
            ->from('hrm_roster_details');
        if(!empty($sSearch)) {
            $this->db
                ->like('hrm_roster_details.name', $sSearch)
                ->or_like('hrm_roster_details.roster_date', $sSearch)
                ->or_like('hrm_roster_details.emoloyee_id', $sSearch);
        }
        if(!empty($empid)) {
            $this->db->where('employee_id', $empid);
        }
        if(!empty($department_id)) {
            $this->db->where('department_id', $department_id);
        }
        if(!empty($start_date) && $start_date != '1970-01-01') {
            $this->db->where('roster_date >=', $start_date);
        }
        if(!empty($end_date) && $end_date != '1970-01-01') {
            $this->db->where('roster_date <=', $end_date);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_roster_details.id', $sSortDir_0)
					->group_by('hrm_roster_details.id')
					->get()
					->result();

        return $records;
    }
    public function total_attendance_log($sSearch, $empid, $department_id, $start_date, $end_date)
    {
        $this->db
            ->select('*')
            ->from('hrm_roster_details');
        if(!empty($sSearch)) {
            $this->db
                ->like('hrm_roster_details.name', $sSearch)
                ->or_like('hrm_roster_details.roster_date', $sSearch)
                ->or_like('hrm_roster_details.emoloyee_id', $sSearch);
        }
        if(!empty($empid)) {
            $this->db->where('employee_id', $empid);
        }
        if(!empty($department_id)) {
            $this->db->where('department_id', $department_id);
        }
        if(!empty($start_date) && $start_date != '1970-01-01') {
            $this->db->where('roster_date >=', $start_date);
        }
        if(!empty($end_date) && $end_date != '1970-01-01') {
            $this->db->where('roster_date <=', $end_date);
        }
        $records =  $this->db->group_by('hrm_roster_details.id')
					->get()
					->result();

        return $records;
    }
    public function get_punch_history($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $empid, $start_date, $end_date)
    {
        // print_r($start_date); print_r($end_date);die;
        $this->db
            ->select('log.id, 
            log.device,
            log.card_no,
            log.log_day_time,
            roster.employee_caption')
            ->from('hrm_attendance_log log')
            ->join('hrm_roster_details roster', 'log.card_no = roster.card_no', 'left')
            ;
        if(!empty($sSearch)) {
            $this->db
                ->like('roster.employee_caption', $sSearch)
                ->or_like('log.card_no', $sSearch)
                ->or_like('log.log_day_time', $sSearch);
        }
        if(!empty($empid)) {
            $this->db->where('roster.employee_id', $empid);
        }
        if(!empty($start_date) && $start_date != '1970-01-01') {
            $this->db->where('DATE_FORMAT(log.log_day_time, "%Y-%m-%d") >=', $start_date);
        }
        if(!empty($end_date) && $end_date != '1970-01-01') {
            $this->db->where('DATE_FORMAT(log.log_day_time, "%Y-%m-%d") <=', $end_date);
        }
		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('log.log_day_time', 'desc')
					->group_by('log.id')
					->get()
					->result();

        return $records;
    }
    public function total_punch_history($sSearch, $empid, $start_date, $end_date)
    {
        $this->db
            ->select('log.device,
            log.card_no,
            log.log_day_time,
            roster.employee_caption')
            ->from('hrm_attendance_log log')
            ->join('hrm_roster_details roster', 'log.card_no = roster.card_no', 'left')
            ;
        if(!empty($sSearch)) {
            $this->db
                ->like('roster.employee_caption', $sSearch)
                ->or_like('log.card_no', $sSearch)
                ->or_like('log.log_day_time', $sSearch);
        }
        if(!empty($empid)) {
            $this->db->where('roster.employee_id', $empid);
        }
        if(!empty($start_date) && $start_date != '1970-01-01') {
            $this->db->where('DATE_FORMAT(log.log_day_time, "%Y-%m-%d") >=', $start_date);
        }
        if(!empty($end_date) && $end_date != '1970-01-01') {
            $this->db->where('DATE_FORMAT(log.log_day_time, "%Y-%m-%d") <=', $end_date);
        }
        $records =  $this->db->order_by('log.id')
                    ->group_by('log.id')
                    ->get()
                    ->result();

        return $records;
    }
}
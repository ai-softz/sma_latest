<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_payroll_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    
    public function getAddDeduct($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_add_deduct');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_add_deduct.caption', $sSearch)
                ->or_like('hrm_add_deduct.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_add_deduct.id', $sSortDir_0)
					->group_by('hrm_add_deduct.id')
					->get()
					->result();

        return $records;
    } 
    public function total_AddDeduct($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_add_deduct');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_add_deduct.caption', $sSearch)
                ->or_like('hrm_add_deduct.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_add_deduct.id')
					->get()
					->result();

        return $records;
    }
    public function getIncomeTax($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_income_tax_setup');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_income_tax_setup.caption', $sSearch)
                ->or_like('hrm_income_tax_setup.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_income_tax_setup.id', $sSortDir_0)
					->group_by('hrm_income_tax_setup.id')
					->get()
					->result();

        return $records;
    } 
    public function total_IncomeTax($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_income_tax_setup');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_income_tax_setup.caption', $sSearch)
                ->or_like('hrm_income_tax_setup.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_income_tax_setup.id')
					->get()
					->result();

        return $records;
    }

    public function findAddition($addition_deduction, $pay_nature, $is_default)
    {
        return $this->db->where(array('addition_deduction' => $addition_deduction, 'pay_nature' => $pay_nature, 'is_default' => $is_default))->get('hrm_add_deduct')->row();
    }
    public function addAdditionDeduct($data)
    {
        $insert = $this->db->insert('hrm_add_deduct', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_add_deduct($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_add_deduct');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_add_deduct_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_add_deduct')->row();
    }
    
    public function edit_addition_deduct($id, $data)
    {
        $update = $this->db->where('id', $id)->update('hrm_add_deduct', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function add_income_tax($data)
    {
        $insert = $this->db->insert('hrm_income_tax_setup', $data);
        $insert_id = $this->db->insert_id();
        if($insert) {
            return $insert_id;
        } else {
            return false;
        }
    }
    public function add_income_tax_dtl($data)
    {
        $insert = $this->db->insert('hrm_income_tax_setup_dtl', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_income_tax($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_income_tax_setup');
        if($delete) {
            $this->db->where('tax_setup_id', $id)->delete('hrm_income_tax_setup_dtl');
            return true;
        } else {
            return false;
        }
    }
    public function get_income_tax_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_income_tax_setup')->row();
    } 
    public function get_income_tax_dtls($id)
    {
        return $this->db->where('tax_setup_id', $id)->get('hrm_income_tax_setup_dtl')->result();
    }
    public function edit_income_tax_dtl($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_income_tax_setup_dtl', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function edit_income_tax($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_income_tax_setup', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function getPayrollPolicy($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_payroll_policy');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_payroll_policy.caption', $sSearch)
                ->or_like('hrm_payroll_policy.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_payroll_policy.id', $sSortDir_0)
					->group_by('hrm_payroll_policy.id')
					->get()
					->result();

        return $records;
    } 
    public function total_PayrollPolicy($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_income_tax_setup');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_income_tax_setup.caption', $sSearch)
                ->or_like('hrm_income_tax_setup.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_income_tax_setup.id')
					->get()
					->result();

        return $records;
    }
    
    public function find_payroll_policy($salary_status)
    {
        return $this->db->where('salary_status', $salary_status)->get('hrm_payroll_policy')->row();
    }
    public function add_payroll_policy($data)
    {
        $insert = $this->db->insert('hrm_payroll_policy', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_payroll_policy($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_payroll_policy');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_payroll_policy_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_payroll_policy')->row();
    } 
    
    public function edit_payroll_policy($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_payroll_policy', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function getPayGrade($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_pay_grade');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_pay_grade.caption', $sSearch)
                ->or_like('hrm_pay_grade.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_pay_grade.id', $sSortDir_0)
					->group_by('hrm_pay_grade.id')
					->get()
					->result();

        return $records;
    } 
    public function total_PayGrade($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_pay_grade');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_pay_grade.caption', $sSearch)
                ->or_like('hrm_pay_grade.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_pay_grade.id')
					->get()
					->result();

        return $records;
    }
    public function delete_pay_grade($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_pay_grade');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function add_pay_grade($data)
    {
        $insert = $this->db->insert('hrm_pay_grade', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function get_pay_grade_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_pay_grade')->row();
    } 
    public function edit_pay_grade($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_pay_grade', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function get_payroll_setup($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_setup_mst');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_setup_mst.caption', $sSearch)
                ->or_like('hrm_prl_setup_mst.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_prl_setup_mst.id', $sSortDir_0)
					->group_by('hrm_prl_setup_mst.id')
					->get()
					->result();

        return $records;
    } 
    public function total_payroll_setup($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_setup_mst');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_setup_mst.caption', $sSearch)
                ->or_like('hrm_prl_setup_mst.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_prl_setup_mst.id')
					->get()
					->result();

        return $records;
    }
    public function get_all_pay_grade()
    {
        return $this->db->get('hrm_pay_grade')->result();
    }
    public function all_bank_accounts()
    {
        return $this->db->get('hrm_bank_account')->result();
    }
    public function get_hrm_add_deduct()
    {
        return $this->db->where('pay_nature !=', 'Basic')->where('pay_nature !=', 'AtnDeduct')->get('hrm_add_deduct')->result();
    }
    
    public function findByNature($payNature)
    {
        return $this->db->where('pay_nature', $payNature)->get('hrm_add_deduct')->row();
    }

    public function paymentFrequencyType($paymentFrequencyType)
    {
        return $this->db->where('payment_frequency_type', $paymentFrequencyType)->get('hrm_pay_grade')->result();
    }
    public function all_income_tax()
    {
        return $this->db->get('hrm_income_tax_setup')->result();
    }

    public function add_payroll_setup($data)
    {
        $insert = $this->db->insert('hrm_prl_setup_mst', $data);
        $insert_id = $this->db->insert_id();
        if($insert) {
            return $insert_id;
        } else {
            return false;
        }
    }
    public function add_payroll_setup_dtl($data)
    {
        $insert = $this->db->insert('hrm_prl_setup_dtl', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function get_payroll_setup_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_prl_setup_mst')->row();
    } 
    public function get_payroll_dtl($id)
    {
        return $this->db->where('hr_prl_emp_mst_id', $id)->get('hrm_prl_setup_dtl')->result();
    }
    public function edit_payroll_setup($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_prl_setup_mst', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function edit_payroll_setup_dtl($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_prl_setup_dtl', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_payroll_dtls($id)
    {
        $delete = $this->db->where('hr_prl_emp_mst_id', $id)->delete('hrm_prl_setup_dtl');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_payroll_setup($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_prl_setup_mst');
        if($delete) {
            $this->db->where('hr_prl_emp_mst_id', $id)->delete('hrm_prl_setup_dtl');
            return true;
        } else {
            return false;
        }
    }
    
    public function payroll_bonuses()
    {
        return $this->db->get('hrm_prl_bonus_setup')->result();
    }

    public function get_payroll_salary_mst($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_salary_mst');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_salary_mst.caption', $sSearch)
                ->or_like('hrm_prl_salary_mst.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_prl_salary_mst.id', $sSortDir_0)
					->group_by('hrm_prl_salary_mst.id')
					->get()
					->result();

        return $records;
    } 
    public function total_payroll_salary_mst($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_salary_mst');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_salary_mst.caption', $sSearch)
                ->or_like('hrm_prl_salary_mst.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_prl_salary_mst.id')
					->get()
					->result();

        return $records;
    }
    public function delete_payroll_salary($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_prl_salary_mst');
        if($delete) {
            $this->db->where('salary_mst_id', $id)->delete('hrm_prl_salary_dtl');
            return true;
        } else {
            return false;
        }
    }
    
    public function get_payroll_setup_list($emp_id, $department_id)
    {
        if(!empty($emp_id)) {
            $this->db->where('employee_id', $emp_id);
        }
        if(!empty($department_id)) {
            $this->db->where('department_id', $department_id);
        }

        return $this->db->get('hrm_prl_setup_mst')->result();
    } 
    public function get_payroll_setup_row($emp_id)
    {
        $this->db->where('employee_id', $emp_id);
        
        return $this->db->get('hrm_prl_setup_mst')->row();
    }

    public function get_payroll_setup_dtls($id)
    {
        return $this->db->where('hr_prl_emp_mst_id', $id)->get('hrm_prl_setup_dtl')->result();
    }
    public function get_payroll_polices()
    {
        return $this->db->get('hrm_payroll_policy')->result();
    }
    
    public function add_payroll_salary($data)
    {
        $this->db->insert('hrm_prl_salary_mst', $data);
        $insert_id = $this->db->insert_id();
        if($insert_id) {
            return $insert_id;
        } else {
            return false;
        }
    }
    public function add_payroll_salaryDtls($data)
    {
        $insert = $this->db->insert('hrm_prl_salary_dtl', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function update_payroll_salary($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_prl_salary_mst', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function get_payroll_salary_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_prl_salary_mst')->row();
    }
    public function delete_payroll_salary_dtl($payroll_salary_id)
    {
        $delete = $this->db->where('salary_mst_id', $payroll_salary_id)->delete('hrm_prl_salary_dtl');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_employees_byDept($department_id)
    {
        return $this->db->where('department_id', $department_id)->get('employees')->result();
    }
    public function get_dept_name($department_id)
    {
        return $this->db->select('*')->get_where('departments', array('department_id'=>$department_id))->row();
    }
}
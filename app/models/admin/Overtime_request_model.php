<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Overtime_request_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // get overtime request>admin>all
	public function get_overtime_request_count($employee_id,$pay_date) {
		
		$sql = 'SELECT * FROM `sma_attendance_time_request` where employee_id = ? and is_approved = ? and request_date_request = ?';
		$binds = array($employee_id,2,$pay_date);
		$query = $this->db->query($sql, $binds);
		$result = $query->result();
		return $result;
	}

}
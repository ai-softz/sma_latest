<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function get_all_employees()
    {
    	return $this->db->where('user_role_id !=', 1)->get('employees')->result();
    }
	public function get_warehouse_employees($warehouse_id)
    {
    	return $this->db->where(array('user_role_id !=' => 1, 'warehouse_id' => $warehouse_id))->get('employees')->result();
    }
	public function get_warehouse_dept_employees($dept_id, $warehouse_id)
    {
    	return $this->db->where(array('user_role_id !=' => 1, 'warehouse_id' => $warehouse_id, 'department_id' => $dept_id))->get('employees')->result();
    }

    public function get_employees_payslip($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $filter_emp, $filter_salaryMonth)
    {

    	// echo "<pre>"; print_r($iDisplayLength);print_r($sSearch);print_r($filter_salaryMonth);print_r($filter_salaryMonth);die;
    	$this->db->select('employees.*')
					->from('employees')
					->join('salary_payslips', 'salary_payslips.employee_id = employees.user_id', 'left')
					->where('employees.user_role_id != ', 1);
					if(!empty($sSearch)) {
						$this->db
							->like('employees.user_id', $sSearch)
							->or_like('employees.first_name', $sSearch)
							->or_like('employees.last_name', $sSearch);
					}
					if(!empty($filter_emp)) {
						$this->db
							->like('employees.user_id', $filter_emp);
					}
					
		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('employees.user_id', $sSortDir_0)
					->group_by('employees.user_id')
					->get()
					->result();
					// print_r($records); die;
        return $records;
    }

	public function totalEmployees($filter_emp, $filter_salaryMonth, $sSearch)
	{
		$this->db->select('*')
					->from('employees')
					->join('salary_payslips', 'salary_payslips.employee_id = employees.user_id', 'left')
					->where('employees.user_role_id != ', 1);
					if(!empty($sSearch)) {
						$this->db
							->like('employees.user_id', $sSearch)
							->or_like('employees.first_name', $sSearch)
							->or_like('employees.last_name', $sSearch);
					}
					if(!empty($filter_emp)) {
						$this->db
							->like('employees.user_id', $filter_emp);
					}
					// if(!empty($filter_salaryMonth) && $filter_salaryMonth != '1970-01') {
					// 	$this->db
					// 		->like('salary_payslips.salary_month', $filter_salaryMonth);
					// }
		
		$records =  $this->db
		->group_by('employees.user_id')
					->get()
					->result();
					// print_r($records); die;
        return $records;
	}

    public function total_hours_worked($user_id,$attendance_date)
    {
    	$sql = 'SELECT * FROM '.db_prefix().'attendance_time WHERE employee_id = ? and attendance_date like ?';
		$binds = array($user_id, '%'.$attendance_date.'%');
		$query = $this->db->query($sql, $binds);
		return $query;
    }

    public function count_employee_allowances($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_allowances WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query->num_rows();
	}

	// get employee salary allowances
	public function read_salary_allowances($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_allowances WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	// get employee salary loan_deduction
	public function read_salary_loan_deductions($id) {
		
		$sql = 'SELECT * FROM '.db_prefix().'salary_loan_deductions WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	// get employee allowances
	public function count_employee_deductions($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_loan_deductions WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query->num_rows();
	}

	// get employee commissions
	public function count_employee_commissions($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_commissions WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query->num_rows();
	}
	// get employee commissions
	public function set_employee_commissions($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_commissions WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query;
	}

	// get employee other payments
	public function count_employee_other_payments($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_other_payments WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query->num_rows();
	}
	// get employee other payments
	public function set_employee_other_payments($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_other_payments WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query;
	}

	// get employee statutory deduction
	public function count_employee_statutory_deductions($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_statutory_deductions WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query->num_rows();
	}
	// get employee statutory deductions
	public function set_employee_statutory_deductions($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_statutory_deductions WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query;
	}

	// get employee overtime
	public function read_salary_overtime($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_overtime WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}
	// get employee overtime
	public function count_employee_overtime($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_overtime WHERE employee_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
	  	return $query->num_rows();
	}

	// get advance salary by employee id
	public function advance_salary_by_employee_id($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'advance_salaries WHERE employee_id = ? and status = ? order by advance_salary_id desc';
		$binds = array($id,1);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	// get advance salary by employee id >paid.total
	public function get_paid_salary_by_employee_id($id) {
	
		$this->db->query("SET SESSION sql_mode = ''");
		$sql = 'SELECT advance_salary_id,employee_id,month_year,one_time_deduct,monthly_installment,reason,status,total_paid,is_deducted_from_salary,created_at,SUM(`'.db_prefix().'advance_salaries`.advance_amount) AS advance_amount FROM `'.db_prefix().'advance_salaries` where status=1 and employee_id=? group by employee_id';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);

		return $query->result();
	}

	public function read_make_payment_payslip_check($employee_id,$p_date) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_payslips WHERE employee_id = ? and salary_month = ?';
		$binds = array($employee_id,$p_date);
		$query = $this->db->query($sql, $binds);
		return $query;
	}
	public function read_make_payment_payslip($employee_id,$p_date) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_payslips WHERE employee_id = ? and salary_month = ?';
		$binds = array($employee_id,$p_date);
		$query = $this->db->query($sql, $binds);
		
		return $query->result();
	}
	// Function to add record in table> salary payslip record
	public function add_salary_payslip($data){
		$this->db->insert('salary_payslips', $data);
		// print_r($this->db->error());die;
		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}
	// Function to update record in table > deduction of advance salary
	public function updated_advance_salary_paid_amount($data, $id){
		$this->db->where('employee_id', $id);
		if( $this->db->update('advance_salaries',$data)) {
			return true;
		} else {
			return false;
		}		
	}

	public function read_salary_payslip_info_key($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'salary_payslips WHERE payslip_key = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	// get single employee
	public function read_user_info($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'employees WHERE user_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
		
	}
	public function read_designation_information($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'designations WHERE designation_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	public function delete_payslip($payslip_id)
	{
		$delete = $this->db->where('payslip_id', $payslip_id)->delete('salary_payslips');
        if($delete) {
            return true;
        } else {
            return false;
        }
	}

	public function delete_salary_expense($payslip_key)
	{
		$delete = $this->db->where('reference', $payslip_key)->delete('expenses');
        if($delete) {
            return true;
        } else {
            return false;
        }
	}
	public function read_department_information($id) {
	
		$sql = 'SELECT * FROM '.db_prefix().'departments WHERE department_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	public function change_payroll_status($data, $payslip_id)
	{
		$update = $this->db->where('payslip_id', $payslip_id)->update('salary_payslips', $data);
		if($update) {
			return true;
		} else {
			return false;
		}
	}

	

}

<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Purchase_raw_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getPurchaseRawTable($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('purchases_raw');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('purchases_raw.caption', $sSearch)
                ->or_like('purchases_raw.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('purchases_raw.id', $sSortDir_0)
					->group_by('purchases_raw.id')
					->get()
					->result();

        return $records;
    } 
    public function total_getPurchaseRawTable($sSearch)
    {
        $this->db
            ->select('*')
            ->from('purchases_raw');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('purchases_raw.caption', $sSearch)
                ->or_like('purchases_raw.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('purchases_raw.id')
					->get()
					->result();

        return $records;
    }

    public function getPurchaseRawByID($id)
    {
        $q = $this->db->get_where('purchases_raw', ['id' => $id], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function getAllPurchaseItemsRaw($purchase_id)
    {
        $this->db->select('purchase_items_raw.*, products_raw.unit, products_raw.caption as name, products_raw.caption_alt as second_name')
            ->join('products_raw', 'products_raw.id=purchase_items_raw.product_id', 'left')
            ->group_by('purchase_items_raw.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('purchase_items_raw', ['purchase_id' => $purchase_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductByIDRaw($id)
    {
        $q = $this->db->get_where('products_raw', ['id' => $id], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function delete_purchase_raw($id)
    {
        $delete = $this->db->where('id', $id)->delete('purchases_raw');
        if($delete) {
            $this->db->where('purchase_id', $id)->delete('purchase_items_raw');
            return true;
        } else {
            return false;
        }
    }

    public function addPaymentRaw($data = [])
    {
        if ($this->db->insert('payments', $data)) {
            if ($this->site->getReference('ppay') == $data['reference_no']) {
                $this->site->updateReference('ppay');
            }
            $this->syncPurchasePaymentsRaw($data['purchase_raw_id']);
            return true;
        }
        return false;
    }

    public function syncPurchasePaymentsRaw($id)
    {
        $purchase = $this->getPurchaseRawByID($id);
        $paid     = 0;
        if ($payments = $this->getPurchasePaymentsRaw($id)) {
            foreach ($payments as $payment) {
                $paid += $payment->amount;
            }
        }

        $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
        if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
            $payment_status = 'partial';
        } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        }

        if ($this->db->update('purchases_raw', ['paid' => $paid, 'payment_status' => $payment_status], ['id' => $id])) {
            return true;
        }

        return false;
    }
    public function getPurchasePaymentsRaw($purchase_id)
    {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', ['purchase_raw_id' => $purchase_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    public function get_product_quantity($product_id)
    {
        return $this->db->select('id, quantity')->where('id', $product_id)->get('products')->row();
    }
    public function update_product_quantity($product_id, $data_product)
    {
        $update = $this->db->where('id', $product_id)->update('products', $data_product);
        if($update) {
            return true;
        } else {
            return false;
        }
    } 

    public function addPurchaseRaw($data, $items)
    {

        $this->db->trans_start();
        if ($this->db->insert('purchases_raw', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $item['option_id']   = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : null;
                $this->db->insert('purchase_items_raw', $item);
                if ($this->Settings->update_cost) {
                    $this->db->update('products_raw', ['cost' => $item['base_unit_cost']], ['id' => $item['product_id']]);
                    
                }
                
            }
            
            $expense_cat = $this->db->get_where('expense_categories', ['code' => 'purchase_raw'])->row();
            $data_expense = [
                'date'         => $data['date'],
                'reference'    => $data['reference_no'],
                'amount'       => $data['grand_total'],
                'created_by'   => $data['created_by'],
                'note'         => 'Purchase raw',
                'category_id'  => $expense_cat->id,
                'warehouse_id' => $data['warehouse_id'],
            	];

        
            $this->purchases_model->addExpense($data_expense);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            log_message('error', 'An errors has been occurred while adding the sale (Add:Purchases_model.php)');
        } else {
            return true;
        }
        return false;
    }

    public function updatePurchaseRaw($purchase_raw_id, $data, $items)
    {
      
        $update = $this->db->where('id', $purchase_raw_id)->update('purchases_raw', $data);
        if ($update) {
            $this->db->where('purchase_id', $purchase_raw_id)->delete('purchase_items_raw');
            $purchase_id = $purchase_raw_id;
            
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $item['option_id']   = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : null;
                $this->db->insert('purchase_items_raw', $item);
                if ($this->Settings->update_cost) {
                    $this->db->update('products_raw', ['cost' => $item['base_unit_cost']], ['id' => $item['product_id']]);
                    
                }
            }

            $expense = $this->db->where('reference', $data['reference_no'])->get('expenses')->row();
            $this->db->where('id', $expense->id)->update('expenses', array('amount' => $data['grand_total']));

            $this->syncPurchaseRawPayments($purchase_raw_id);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            log_message('error', 'An errors has been occurred while adding the sale (Add:Purchases_model.php)');
        } else {
            return true;
        }
        return false;
    }

    public function syncPurchaseRawPayments($id)
    {
        // $purchase = $this->getPurchaseByID($id);
        $purchase = $this->db->where('id', $id)->get('purchases_raw')->row();
        $paid     = 0;
        if ($payments = $this->getPurchasePayments($id)) {
            foreach ($payments as $payment) {
                $paid += $payment->amount;
            }
        }

        $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
        if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
            $payment_status = 'partial';
        } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        }

        if ($this->db->update('purchases_raw', ['paid' => $paid, 'payment_status' => $payment_status], ['id' => $id])) {
            return true;
        }

        return false;
    }

    public function getPurchasePayments($purchase_id)
    {
        $q = $this->db->get_where('payments', ['purchase_raw_id' => $purchase_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductByCodeRaw($code)
    {
        $q = $this->db->get_where('products_raw', ['code' => $code], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function getRawMaterialCategories($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('categories_raw');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('categories_raw.caption', $sSearch)
                ->or_like('categories_raw.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('categories_raw.id', $sSortDir_0)
					->group_by('categories_raw.id')
					->get()
					->result();

        return $records;
    } 
    public function total_RawMaterialCategories($sSearch)
    {
        $this->db
            ->select('*')
            ->from('categories_raw');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('categories_raw.caption', $sSearch)
                ->or_like('categories_raw.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('categories_raw.id')
					->get()
					->result();

        return $records;
    }
    public function get_category_byId($id)
    {
        return $this->db->get_where('categories_raw', ['id'=>$id])->row();
    }

    public function add_categories_raw($data)
    {
        $insert = $this->db->insert('categories_raw', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function edit_raw_categories($data, $id)
    {
        $insert = $this->db->where('id', $id)->update('categories_raw', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_category_raw_material($id)
    {
        $delete = $this->db->where('id', $id)->delete('categories_raw');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function getProductsRawTable($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $categoryId='')
    {
        $this->db
            ->select('products_raw.*, categories_raw.caption as cat_caption')
            ->from('products_raw');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('products_raw.caption', $sSearch)
                ->or_like('products_raw.caption_alt', $sSearch);
        }
        $this->db->join('categories_raw', 'categories_raw.id=products_raw.category_id');
        if($categoryId) {
            $this->db->where('products_raw.category_id', $categoryId);
        }
		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('products_raw.id', $sSortDir_0)
					->group_by('products_raw.id')
					->get()
					->result();

        return $records;
    } 
    public function total_getProductsRawTable($sSearch, $categoryId='')
    {
        $this->db
            ->select('*')
            ->from('products_raw');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('products_raw.caption', $sSearch)
                ->or_like('products_raw.caption_alt', $sSearch);
        }
        $this->db->join('categories_raw', 'categories_raw.id=products_raw.category_id');
        if($categoryId) {
            $this->db->where('products_raw.category_id', $categoryId);
        }
        $records =  $this->db->group_by('products_raw.id')
					->get()
					->result();

        return $records;
    }
    public function getCategoriesRaw()
    {
        return $this->db->get('categories_raw')->result();
    }
    public function add_products_raw($data)
    {
        $insert = $this->db->insert('products_raw', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function getProductsRawByid($id='')
    {
        return $this->db->where('id', $id)->get('products_raw')->row();
    }
    public function edit_products_raw($data, $id)
    {
        $update = $this->db->where('id', $id)->update('products_raw', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_products_raw($id)
    {
        $delete = $this->db->where('id', $id)->delete('products_raw');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function getPaymentsForPurchaseRaw($purchase_id)
    {
        $this->db->select('payments.date, payments.paid_by, payments.amount, payments.reference_no, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', ['purchase_id' => $purchase_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getDailyPurchasesRaw($year, $month, $warehouse_id = null)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( grand_total, 0 ) ) AS total
            FROM " . $this->db->dbprefix('purchases_raw') . ' WHERE ';
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getMonthlyPurchasesRaw($year, $warehouse_id = null)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( grand_total, 0 ) ) AS total
            FROM " . $this->db->dbprefix('purchases_raw') . ' WHERE ';
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getPrRawPurchaseReport($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product)
    {
        $pp = "
        SELECT pr.id, pr.code, pr.caption, CONCAT(COALESCE(PCost.purchasedQty, 0), '__', COALESCE(PCost.totalPurchase, 0)) AS purchased 
        FROM {$this->db->dbprefix('products_raw')} pr 
        LEFT JOIN (SELECT pi.product_id as pr_id, p.date as date, p.created_by as created_by, 
        SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN quantity ELSE 0 END) as purchasedQty, 
        SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN (pi.subtotal) ELSE 0 END) totalPurchase 
        from {$this->db->dbprefix('purchase_items_raw')} pi 
        LEFT JOIN {$this->db->dbprefix('purchases_raw')} p on p.id = pi.purchase_id 
        WHERE pi.status = 'received'
        ";

        if($start_date) {
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $end_date ? date('Y-m-d', strtotime($end_date)) : date('Y-m-d');
            $pp .= " AND DATE_FORMAT(p.date, '%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(p.date, '%Y-%m-%d') <= '{$end_date}' ";
        }
        
        $pp .= " GROUP BY pi.product_id) AS PCost ON pr.id = PCost.pr_id";
        if($product) {
            $pp .= " WHERE pr.id = {$product}";
        }
        if(!empty($sSearch)) {
            $pp .= " WHERE ({$sSearch} like %pr.caption% OR {$sSearch} like %pr.caption_alt%)";
        }
        $pp .= " LIMIT $iDisplayLength OFFSET $iDisplayStart";
        // $pp .= " ORDER BY pr.id '{$sSortDir_0}'";
        // print_r($pp); die;
        return $pp;
    } 
    public function total_getProductsRawReport($sSearch, $start_date, $end_date, $product)
    {
        $pp = "SELECT pr.id, pr.code, pr.caption, PCost.purchasedQty AS purchase_qty, PCost.totalPurchase AS total_purchase FROM {$this->db->dbprefix('products_raw')} pr LEFT JOIN ( SELECT pi.product_id as pr_id, p.date as date, p.created_by as created_by, SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN quantity ELSE 0 END) as purchasedQty, SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN (pi.subtotal) ELSE 0 END) totalPurchase from {$this->db->dbprefix('purchase_items_raw')} pi LEFT JOIN {$this->db->dbprefix('purchases_raw')} p on p.id = pi.purchase_id WHERE pi.status = 'received'";

        
        if($start_date) {
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $end_date ? date('Y-m-d', strtotime($end_date)) : date('Y-m-d');
            $pp .= " AND DATE_FORMAT(p.date, '%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(p.date, '%Y-%m-%d') <= '{$end_date}' ";
        }
        if($product) {
            $pp .= " AND pr.id = {$product}";
        }
        $pp .= " GROUP BY pi.product_id) AS PCost ON pr.id = PCost.pr_id";

        if(!empty($sSearch)) {
            $pp .= " WHERE ({$sSearch} like %pr.caption% OR {$sSearch} like %pr.caption_alt%)";
        }

        return $pp;
    }

    public function getPurchaseMakePrReport2($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product)
    {
        $where = "";
        if($start_date) {
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $end_date ? date('Y-m-d', strtotime($end_date)) : date('Y-m-d');
            $where .= " AND DATE_FORMAT(pm.date, '%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(pm.date, '%Y-%m-%d') <= '{$end_date}' ";
        }
        if($product) {
            $where .= " AND pm.product_id = {$product}";
        }

        $sql = "
            SELECT pr.id, pr.code, pr.name, SUM(pm.quantity) AS make_qty, pm.date FROM {$this->db->dbprefix('products')} AS pr 
            LEFT JOIN {$this->db->dbprefix('products_made')} AS pm ON pr.id = pm.product_id
            WHERE pm.quantity IS NOT NULL {$where}
            GROUP BY pm.product_id 
            LIMIT $iDisplayLength OFFSET $iDisplayStart
        ";
        return $sql;
    }

    public function total_getPurchaseMakePrReport2($sSearch, $start_date, $end_date, $product)
    {
        $sql = "
            SELECT pr.id, pr.code, pr.name, SUM(pm.quantity) AS make_qty, pm.date FROM {$this->db->dbprefix('products')} AS pr 
            LEFT JOIN {$this->db->dbprefix('products_made')} AS pm ON pr.id = pm.product_id
            WHERE pm.quantity IS NOT NULL
            GROUP BY pm.product_id 
        ";
        return $sql;
    }

    public function getPurchaseMakePrReport($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product)
    {
        $pp = "
        SELECT pr.id, pr.code, pr.caption, CONCAT(COALESCE(PCost.purchasedQty, 0), '__', COALESCE(PCost.totalPurchase, 0)) AS purchased 
        FROM {$this->db->dbprefix('products_raw')} pr 
        LEFT JOIN (SELECT pi.product_id as pr_id, p.date as date, p.created_by as created_by, 
        SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN quantity ELSE 0 END) as purchasedQty, 
        SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN (pi.subtotal) ELSE 0 END) totalPurchase 
        from {$this->db->dbprefix('purchase_items_raw')} pi 
        LEFT JOIN {$this->db->dbprefix('purchases_raw')} p on p.id = pi.purchase_id 
        WHERE pi.status = 'received'
        ";

        if($start_date) {
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $end_date ? date('Y-m-d', strtotime($end_date)) : date('Y-m-d');
            $pp .= " AND DATE_FORMAT(p.date, '%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(p.date, '%Y-%m-%d') <= '{$end_date}' ";
        }
        
        $pp .= " GROUP BY pi.product_id) AS PCost ON pr.id = PCost.pr_id";
        if($product) {
            $pp .= " WHERE pr.id = {$product}";
        }
        if(!empty($sSearch)) {
            $pp .= " WHERE ({$sSearch} like %pr.caption% OR {$sSearch} like %pr.caption_alt%)";
        }
        $pp .= " LIMIT $iDisplayLength OFFSET $iDisplayStart";
        // $pp .= " ORDER BY pr.id '{$sSortDir_0}'";
        // print_r($pp); die;
        return $pp;
    } 
    public function total_getPurchaseMakePrReport($sSearch, $start_date, $end_date, $product)
    {
        $pp = "SELECT pr.id, pr.code, pr.caption, PCost.purchasedQty AS purchase_qty, PCost.totalPurchase AS total_purchase FROM {$this->db->dbprefix('products_raw')} pr LEFT JOIN ( SELECT pi.product_id as pr_id, p.date as date, p.created_by as created_by, SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN quantity ELSE 0 END) as purchasedQty, SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN (pi.subtotal) ELSE 0 END) totalPurchase from {$this->db->dbprefix('purchase_items_raw')} pi LEFT JOIN {$this->db->dbprefix('purchases_raw')} p on p.id = pi.purchase_id WHERE pi.status = 'received'";

        
        if($start_date) {
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $end_date ? date('Y-m-d', strtotime($end_date)) : date('Y-m-d');
            $pp .= " AND DATE_FORMAT(p.date, '%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(p.date, '%Y-%m-%d') <= '{$end_date}' ";
        }
        if($product) {
            $pp .= " AND pr.id = {$product}";
        }
        $pp .= " GROUP BY pi.product_id) AS PCost ON pr.id = PCost.pr_id";

        if(!empty($sSearch)) {
            $pp .= " WHERE ({$sSearch} like %pr.caption% OR {$sSearch} like %pr.caption_alt%)";
        }

        return $pp;
    }

    public function getProductNamesRaw($term, $limit = 5)
    {
        $this->db->select('*')
            ->like('caption', $term)->or_like('caption_alt', $term)->or_like('code', $term);
        $this->db->limit($limit);
        $q = $this->db->get('products_raw');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function getProductNames($term, $limit = 5)
    {
        $this->db->select('*')
            ->like('name', $term)->or_like('name_alt', $term)->or_like('code', $term);
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function get_make_products_report($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $start_date, $end_date, $product)
    {
        $pp = "";
        if($start_date) {
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = $end_date ? date('Y-m-d', strtotime($end_date)) : date('Y-m-d');
            $pp .= " AND DATE_FORMAT(p.date, '%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(p.date, '%Y-%m-%d') <= '{$end_date}' ";
        }
        
        $pp .= " GROUP BY pi.product_id) AS PCost ON pr.id = PCost.pr_id";
        if($product) {
            $pp .= " WHERE pr.id = {$product}";
        }
        if(!empty($sSearch)) {
            $pp .= " WHERE ({$sSearch} like %pr.caption% OR {$sSearch} like %pr.caption_alt%)";
        }
        $pp .= " LIMIT $iDisplayLength OFFSET $iDisplayStart";
        // $pp .= " ORDER BY pr.id '{$sSortDir_0}'";
        // print_r($pp); die;
        return $pp;
    }

    public function get_categories_raw()
    {
        return $this->db->get('categories_raw')->result();
    }

    public function add_make_products($data)
    {
        $insert = $this->db->insert('products_made', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Roster_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_rosters($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_rosters');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_rosters.name', $sSearch)
                ->or_like('hrm_rosters.name_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_rosters.id', $sSortDir_0)
					->group_by('hrm_rosters.id')
					->get()
					->result();

        return $records;
    }
    public function total_rosters($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_rosters');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_rosters.name', $sSearch)
                ->or_like('hrm_rosters.name_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_rosters.id')
					->get()
					->result();

        return $records;
    }

    public function all_period_masters()
    {
        return $this->db->get('period_master')->result();
    }

    public function get_emp_shifts($employee_id)
    {
        return $this->db->where('employee_id', $employee_id)->get('employee_shift_master')->row();
    }
    public function check_emp_roster($employee_id, $period_mst_id)
    {
        return $this->db->where(array('employee_id' => $employee_id, 'period_mst_id' => $period_mst_id))->get('hrm_rosters')->result();
    }

    
    public function add_roster($data)
    {
        $insert = $this->db->insert('hrm_rosters', $data);
        $insert_id = $this->db->insert_id();
        if($insert) {
            return $insert_id;
        } else {
            return false;
        }
    }
    public function add_roster_detail($data)
    {
        $insert = $this->db->insert('hrm_roster_details', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function get_shift_master($shift_mst_id)
    {
        return $this->db->where('id', $shift_mst_id)->get('shift_masters')->row();
    }

    public function all_employees_by_dept($department_id)
    {
    	return $this->db->select('user_id, first_name, last_name')->where('user_role_id !=', 1)->where('department_id', $department_id)->get('employees')->result();
    }
    public function get_employee_by_id($id)
    {
    	return $this->db->select('user_id, first_name, last_name, department_id')->where('user_role_id !=', 1)->where('user_id', $id)->get('employees')->result();
    }
    public function delete_roster($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_rosters');
        if($delete) {
            $this->db->where('roster_id', $id)->delete('hrm_roster_details');
            return true;
        } else {
            return false;
        }
    }
    public function delete_roster_detail($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_roster_details');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function get_roster_details($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $filter_emp, $filter_rosterId, $filter_period_id, $filter_salaryDate)
    {
        
        $this->db
            ->select('*')
            ->from('hrm_roster_details');
        if(!empty($sSearch)) {
            $this->db
                ->like('hrm_roster_details.id', $sSearch)
                ->or_like('hrm_roster_details.caption', $sSearch);
        }
        if(!empty($filter_emp)) {
            $this->db
                ->where('hrm_roster_details.employee_id', $filter_emp);
        }
        if(!empty($filter_rosterId)) {
            $this->db
                ->where('hrm_roster_details.roster_id', $filter_rosterId);
        }
        if(!empty($filter_period_id)) {
            $this->db
                ->where('hrm_roster_details.period_id', $filter_period_id);
        }
        if(!empty($filter_salaryDate) && $filter_salaryDate != '1970-01-01') {
            $this->db
                ->where('hrm_roster_details.roster_date', $filter_salaryDate);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_roster_details.id', $sSortDir_0)
					->group_by('hrm_roster_details.id')
					->get()
					->result();
// print_r($records); 
// print_r($filter_emp); echo '-'; print_r($filter_rosterId);echo '-';print_r($filter_period_id);echo '-';print_r($filter_salaryDate); die;
        return $records;
    }
    public function total_roster_details($sSearch, $filter_emp, $filter_rosterId, $filter_period_id, $filter_salaryDate)
    {
        $this->db
            ->select('*')
            ->from('hrm_roster_details');
        if(!empty($sSearch)) {
            $this->db
                ->like('hrm_roster_details.caption', $sSearch)
                ->or_like('hrm_roster_details.caption', $sSearch);
        }
        if(!empty($filter_emp)) {
            $this->db
                ->where('hrm_roster_details.employee_id', $filter_emp);
        }
        if(!empty($filter_rosterId)) {
            $this->db
                ->where('hrm_roster_details.roster_id', $filter_rosterId);
        }
        if(!empty($filter_period_id)) {
            $this->db
                ->where('hrm_roster_details.period_id', $filter_period_id);
        }
        if(!empty($filter_salaryDate) && $filter_salaryDate != '1970-01-01') {
            $this->db
                ->like('hrm_roster_details.roster_date', $filter_salaryDate);
        }
        $records =  $this->db->group_by('hrm_roster_details.id')
					->get()
					->result();

        return $records;
    }
    public function read_period_master($id)
    {
        return $this->db->get_where('period_master', array('id' => $id))->row();
    }
    public function all_rosters()
    {
        return $this->db->get('hrm_rosters')->result();
    }
    public function all_shift_masters()
    {
        return $this->db->get('shift_masters')->result();
    }

    public function read_roster_detail($id)
    {
        return $this->db->get_where('hrm_roster_details', array('id' => $id))->row();
    }

    public function edit_roster_details($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_roster_details', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function roster_grids($start_date, $end_date, $emp_id='', $department_id='')
    {
        $this->db->select('*');
        $this->db->where('roster_date >=', $start_date);
        $this->db->where('roster_date <=', $end_date);

        if(!empty($emp_id)) {
            $this->db->where('employee_id', $emp_id);       
        }
        if(!empty($department_id)) {
            $this->db->where('department_id', $department_id);       
        }

        $results = $this->db->get('hrm_roster_details')->result();

        return $results;

    }

    public function get_employees_by_dept($department_id)
    {
        $results = $this->db->select('user_id, first_name, last_name, department_id')->where('department_id', $department_id)->get('employees')->result();
        return $results;
    }
    public function leave_application_date($employee_id, $roster_date)
    {
        $sql = "SELECT * FROM {$this->db->dbprefix('hrm_leave_applications')} WHERE employee_id={$employee_id} AND from_date='{$roster_date}' OR to_date='{$roster_date}' OR ('{$roster_date}' BETWEEN from_date AND to_date)";

        return $this->db->query($sql)->row();
    }
    public function travel_date($employee_id, $roster_date)
    {
        // return $this->db->where('employee_id', $employee_id)->get('hrm_travels')->row();
        $sql = "SELECT * FROM {$this->db->dbprefix('hrm_travels')} WHERE employee_id={$employee_id} AND start_date='{$roster_date}' OR end_date='{$roster_date}' OR ('{$roster_date}' BETWEEN start_date AND end_date)";

        return $this->db->query($sql)->row();
    }
    // check if holiday available
	public function holiday_date($attendance_date) {
	
		$sql = 'SELECT * FROM '. $this->db->dbprefix('hrm_holidays'). ' WHERE (? between start_date and end_date) or (start_date = ? or end_date = ?) limit 1';
		$binds = array($attendance_date,$attendance_date,$attendance_date);
		$query = $this->db->query($sql, $binds)->row();
		
		return $query;
	}
    public function update_roster_detail($data, $id)
    {
        $update = $this->db->where('id', $id)->update('hrm_roster_details', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function read_department_information($id) {
	
		$sql = 'SELECT * FROM '. db_prefix().'departments WHERE department_id = ?';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return null;
		}
	}
}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Shift_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_shift_master($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('shift_masters');
        if(!empty($sSearch)) {
            $this->db
                ->like('shift_masters.code', $sSearch)
                ->or_like('shift_masters.name', $sSearch)
                ->or_like('shift_masters.name_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('shift_masters.id', $sSortDir_0)
					->group_by('shift_masters.id')
					->get()
					->result();

        return $records;
    }
    public function total_shift_master($sSearch)
    {
        $this->db
            ->select('*')
            ->from('shift_masters');
        if(!empty($sSearch)) {
            $this->db
                ->like('shift_masters.code', $sSearch)
                ->or_like('shift_masters.name', $sSearch)
                ->or_like('shift_masters.name_alt', $sSearch);
        }
        $records =  $this->db->group_by('shift_masters.id')
					->get()
					->result();

        return $records;
    }
    
    public function add_shift_master($data)
    {
        $insert = $this->db->insert('shift_masters', $data);
        $insert_id = $this->db->insert_id();
        if($insert) {
            return $insert_id;
        } else {
            return false;
        }
    }
    public function add_shift_detail($data)
    {
        $insert = $this->db->insert('shift_details', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_shift_master($shift_master_id)
    {
        $delete = $this->db->where('id', $shift_master_id)->delete('shift_masters');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_shift_details($shift_master_id)
    {
        $delete = $this->db->where('shift_master_id', $shift_master_id)->delete('shift_details');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function read_shift_master($id)
    {
        return $this->db->get_where('shift_masters', array('id' => $id))->row();
    }
    public function read_shift_details($shift_master_id)
    {
        return $this->db->get_where('shift_details', array('shift_master_id' => $shift_master_id))->result();
    }
    public function edit_shift_detail($data, $id)
    {
        $update = $this->db->where('id', $id)->update('shift_details', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function editShiftMaster($data, $id)
    {
        $update = $this->db->where('id', $id)->update('shift_masters', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function all_shift_masters()
    {
        return $this->db->get('shift_masters')->result();
    }
    
    public function get_employee_shift($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('employee_shift_master');
        if(!empty($sSearch)) {
            $this->db               
                ->or_like('employee_shift_master.name', $sSearch)
                ->or_like('employee_shift_master.name_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('employee_shift_master.id', $sSortDir_0)
					->group_by('employee_shift_master.id')
					->get()
					->result();

        return $records;
    }
    public function total_employee_shift($sSearch)
    {
        $this->db
            ->select('*')
            ->from('employee_shift_master');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('employee_shift_master.name', $sSearch)
                ->or_like('employee_shift_master.name_alt', $sSearch);
        }
        $records =  $this->db->group_by('employee_shift_master.id')
					->get()
					->result();

        return $records;
    }
    public function add_emp_shift_map($data)
    {
        $insert = $this->db->insert('employee_shift_master', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function read_department_information($id) {
		$sql = "SELECT * FROM {$this->db->dbprefix('departments')}  WHERE department_id = ?";
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return null;
		}
	}
    public function delete_employee_shift_map($id)
    {
        $delete = $this->db->where('id', $id)->delete('employee_shift_master');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function read_employee_shift_map($id)
    {
        return $this->db->get_where('employee_shift_master', array('id' => $id))->row();
    }
    public function edit_emp_shift_map($data, $id)
    {
        $update = $this->db->where('id', $id)->update('employee_shift_master', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function get_emp_shift($emp_id)
    {
        return $this->db->get_where('employee_shift_master', array('employee_id' => $emp_id))->row();
    }
}
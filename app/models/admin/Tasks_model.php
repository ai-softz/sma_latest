<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tasks_model extends CI_Model
{

	const STATUS_NOT_STARTED = 1;
    const STATUS_AWAITING_FEEDBACK = 2;
    const STATUS_TESTING = 3;
    const STATUS_IN_PROGRESS = 4;
    const STATUS_COMPLETE = 5;

    public function __construct()
    {
        parent::__construct();
    }

    public function getTask($task_id='')
    {
        return $this->db->where('id', $task_id)->get('tasks')->row();
    }

        public function getTaskTable($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $where = "";
        if(!empty($sSearch)) {
            $where = " AND TASK.name ilike '%{$sSearch}%' OR to_char(TASK.startdate, 'DD-MM-YYYY') like '%{$sSearch}%' OR to_char(TASK.duedate, 'DD-MM-YYYY') like '%{$sSearch}%'";
        }
        $sql = "";
        $sql = "SELECT TASK.id as id, TASK.id as task_id, TASK.name,  TASK.startdate, TASK.duedate, users_name, tag_name, TASK.status as task_status, TASK.priority as task_priority
        FROM {$this->db->dbprefix('tasks')} AS TASK

        LEFT JOIN (SELECT TAGGB.rel_id, GROUP_CONCAT(TAG.name) AS tag_name FROM {$this->db->dbprefix('taggables')} AS TAGGB JOIN {$this->db->dbprefix('tags')} AS TAG
        ON TAG.id = TAGGB.tag_id GROUP BY TAGGB.rel_id) AS taggs ON taggs.rel_id = TASK.id
        
        LEFT JOIN (SELECT T_ASSI.taskid, GROUP_CONCAT(CONCAT(USERS.first_name, ' ', USERS.last_name)) AS users_name FROM {$this->db->dbprefix('task_assigned')} AS T_ASSI JOIN {$this->db->dbprefix('users')} AS USERS
        ON USERS.id = T_ASSI.staffid GROUP BY T_ASSI.taskid) AS TASK_USERS ON TASK_USERS.taskid = TASK.id WHERE 1 = 1 " . $where .
        " ORDER BY TASK.id {$sSortDir_0}" 
        ;

        $sql .= " LIMIT $iDisplayLength OFFSET $iDisplayStart";
        $records = $this->db->query($sql)->result();

        return $records;
    } 

    public function total_getTaskTable($sSearch)
    {
        $where = "";
        if(!empty($sSearch)) {
            $where = " AND TASK.name ilike '%{$sSearch}%' OR to_char(TASK.startdate, 'DD-MM-YYYY') like '%{$sSearch}%' OR to_char(TASK.duedate, 'DD-MM-YYYY') like '%{$sSearch}%'";
        }
        $sql = "";
        $sql = "SELECT TASK.id as id, TASK.id as task_id, TASK.name,  TASK.startdate, TASK.duedate, users_name, tag_name, TASK.status as task_status, TASK.priority as task_priority
        FROM {$this->db->dbprefix('tasks')} AS TASK

        LEFT JOIN (SELECT TAGGB.rel_id, TAG.name AS tag_name FROM {$this->db->dbprefix('taggables')} AS TAGGB JOIN {$this->db->dbprefix('tags')} AS TAG
        ON TAG.id = TAGGB.tag_id GROUP BY TAGGB.rel_id) AS taggs ON taggs.rel_id = TASK.id
        
        LEFT JOIN (SELECT T_ASSI.taskid, CONCAT(USERS.first_name, ' ', USERS.last_name) AS users_name FROM {$this->db->dbprefix('task_assigned')} AS T_ASSI JOIN {$this->db->dbprefix('users')} AS USERS
        ON USERS.id = T_ASSI.staffid GROUP BY T_ASSI.taskid) AS TASK_USERS ON TASK_USERS.taskid = TASK.id
        WHERE 1 = 1" . $where
        ;
        
        $records = $this->db->query($sql)->result();

        return $records;
    }

    public function get_checklist_items($taskid)
    {
        $this->db->where('taskid', $taskid);
        $this->db->order_by('id', 'desc');

        return $this->db->get('task_checklist_items')->result_array();
    }

    public function add_checklist_item($taskid='', $checklist_des)
    {

        $checktask = $this->db->where(array('taskid'=>$taskid, 'description' =>$checklist_des))->get('task_checklist_items')->row();
        if(!$checktask){
        $this->db->insert('task_checklist_items', [
            'taskid'      => $taskid,
            'description' => $checklist_des,
            'dateadded'   => date('Y-m-d H:i:s'),
            'addedfrom'   => $this->session->userdata('user_id'),
            'list_order'  => 0,
        ]);
        $insert_id = $this->db->insert_id();
            if ($insert_id) {
                return true;
            }
        }
        return false;
    }

    public function edit_checklist_item($checklist_id='', $checklist_des)
    {
    	$data = [
            'description' => $checklist_des,
        ];
        $update = $this->db->where('id', $checklist_id)->update('task_checklist_items', $data);
       
        if ($update) {
            return true;
        }
        return false;
    }

    public function delete_checklist_item($checklist_id='')
    {
    	return $this->db->where('id', $checklist_id)->delete('task_checklist_items');
    }

    public function finish_checklist_item($checklist_id='', $finished='')
    {
    	$data = ['finished' => $finished];
    	return $this->db->where('id', $checklist_id)->update('task_checklist_items', $data);
    }

    function addTaskAssignee($assignee_id='', $taskid='', $user_id='')
    {
    	$data = ['staffid' => $assignee_id, 'taskid' => $taskid, 'assigned_from' => $user_id];
    	return $this->db->insert('task_assigned', $data);
    }

    public function deleteTaskAssignee($assignee_id='', $taskid='')
    {
    	return $this->db->where(array('id' => $assignee_id , 'taskid' => $taskid))->delete('task_assigned');
    }

    public function addTask($data)
    {

    	if (isset($data['repeat_every']) && $data['repeat_every'] != '') {
            $data['recurring'] = 1;
                $_temp                    = explode('-', $data['repeat_every']);
                $data['recurring_type']   = $_temp[1];
                $data['repeat_every']     = $_temp[0];
                $data['custom_recurring'] = 0;
        } else {
            $data['recurring']    = 0;
            $data['repeat_every'] = null;
        }

        $data['is_public'] = 1;

        if (date('Y-m-d') >= $data['startdate']) {
            $data['status'] = 4;
        } else {
            $data['status'] = 1;
        }

        $data['startdate']             = date('Y-m-d', strtotime($data['startdate']));
        $data['duedate']               = date('Y-m-d', strtotime($data['duedate']));
        $data['dateadded']             = date('Y-m-d H:i:s');
        $data['addedfrom']             = $this->session->userdata('user_id');
       
        $tags = '';
        if (isset($data['tags'])) {
            $tags = $data['tags'];
            unset($data['tags']);
        }
       $insert = $this->db->insert('tasks', $data);
       $insert_id = $this->db->insert_id();

       if ($insert_id) {
       		$tags_arr = explode(',', $tags);
       		$tags_result = $this->db->get('tags')->result();
       		$tags_all = array();
       		foreach($tags_result as $tag) {
       			$tags_all[] = $tag->name;
       		}

       		foreach ($tags_arr as $key => $tag) {
                // if tag is not in db then insert
       			if(!in_array($tag, $tags_all)) {
       				$data_tag = array('name' => $tag);
	       			$this->db->insert('tags', $data_tag);
       			} 
       			$tag_id = $this->db->get_where('tags', array('name' => $tag))->row()->id;
       			$duplicate_taggables = $this->db->get_where('taggables', array('rel_id'=>$insert_id, 'tag_id' => $tag_id))->result();

       			if(empty($duplicate_taggables)) {
	       			$data_taggables = array('rel_id' => $insert_id, 'tag_id' => $tag_id, 'rel_type'=>'task');
		       		$this->db->insert('taggables', $data_taggables);
	       		}
       		}

       }

       if($insert_id) {
       		return true;
       } else {
       		return false;
       		// $error = $this->db->error();
       }
    }

    public function editTask($data)
    {

        if (isset($data['repeat_every']) && $data['repeat_every'] != '') {
            $data['recurring'] = 1;
                $_temp                    = explode('-', $data['repeat_every']);
                $data['recurring_type']   = $_temp[1];
                $data['repeat_every']     = $_temp[0];
                $data['custom_recurring'] = 0;
        } else {
            $data['recurring']    = 0;
            $data['repeat_every'] = null;
        }

        $data['is_public'] = 1;

        if (date('Y-m-d') >= $data['startdate']) {
            $data['status'] = 4;
        } else {
            $data['status'] = 1;
        }

        $data['startdate']             = date('Y-m-d', strtotime($data['startdate']));
        $data['duedate']               = date('Y-m-d', strtotime($data['duedate']));
        $data['dateadded']             = date('Y-m-d H:i:s');
        $data['addedfrom']             = $this->session->userdata('user_id');
       
        $tags = '';
        if (isset($data['tags'])) {
            $tags = $data['tags'];
            unset($data['tags']);
        }
        $task_id = $data['task_id'];
        unset($data['task_id']);
       $insert = $this->db->where('id', $task_id)->update('tasks', $data);
       $insert_id = $task_id;

       if ($insert_id) {
            $tags_arr = explode(',', $tags);
            $tags_result = $this->db->get('tags')->result();
            $tags_all = array();
            foreach($tags_result as $tag) {
                $tags_all[] = $tag->name;
            }

            foreach ($tags_arr as $key => $tag) {
                if(!in_array($tag, $tags_all)) {
                    $data_tag = array('name' => $tag);
                    $this->db->insert('tags', $data_tag);
                } 
                $tag_id = $this->db->get_where('tags', array('name' => $tag))->row()->id;
                $duplicate_taggables = $this->db->get_where('taggables', array('rel_id'=>$insert_id, 'tag_id' => $tag_id))->result();

                if(empty($duplicate_taggables)) {
                    $data_taggables = array('rel_id' => $insert_id, 'tag_id' => $tag_id, 'rel_type'=>'task');
                    $this->db->insert('taggables', $data_taggables);
                }
            }

       }

       if($insert_id) {
            return true;
       } else {
            return false;
            // $error = $this->db->error();
       }
    }

    public function delete_task($task_id='')
    {
       return $this->db->where('id', $task_id)->delete('tasks');
    }

    public function update_tags($tags='', $taskid='')
    {
    		$tags_arr = explode(',', $tags);
       		$tags_result = $this->db->get('tags')->result();
       		$tags_all = array();
       		foreach($tags_result as $tag) {
       			$tags_all[] = $tag->name;
       		}

       		foreach ($tags_arr as $key => $tag) {
       			if(!in_array($tag, $tags_all)) {
       				$data_tag = array('name' => $tag);
	       			$this->db->insert('tags', $data_tag);
       			} 
       			$tag_id = $this->db->get_where('tags', array('name' => $tag))->row()->id;
       			$duplicate_taggables = $this->db->get_where('taggables', array('rel_id'=>$taskid, 'tag_id' => $tag_id))->result();

       			if(empty($duplicate_taggables)) {
	       			$data_taggables = array('rel_id' => $taskid, 'tag_id' => $tag_id, 'rel_type'=>'task');
		       		$this->db->insert('taggables', $data_taggables);
	       		}
       		}

       		return true;
    }

    public function update_task_comment($comment_id, $taskid, $comment_content)
    {
    	$data = ['content' => $comment_content];

    	return $this->db->where(array('id'=>$comment_id, 'taskid'=>$taskid))->update('task_comments', $data);
    }

    public function add_task_comment($taskid='', $comment='', $user_id, $file_name='')
    {
        $dateadded = date('Y-m-d H:i:s', time());
    	$data =['taskid' => $taskid, 'content' => $comment, 'staffid' => $user_id, 'file_name' => $file_name, 'dateadded' => $dateadded];
    	$this->db->insert('task_comments', $data);
        return true;
    }

    public function remove_task_comment($comment_id)
    {
    	return $this->db->where('id', $comment_id)->delete('task_comments');
    }

    public function updateTaskStatus($id='', $status_id='', $user_id='')
    {
    	$data = array('status' => $status_id, 'completed_by' => $user_id);
    	$update = $this->db->where('id', $id)->update('tasks', $data);

    	if($update) {
    		return true;
    	}
    	return false;
    }

    public function updateTaskPriority($id='', $priority_id='', $user_id='')
    {
    	$data = array('priority' => $priority_id, 'completed_by' => $user_id);
    	$update = $this->db->where('id', $id)->update('tasks', $data);

    	if($update) {
    		return true;
    	}
    	return false;
    }

    public function task_mark_as($taskid='', $status='', $user_id='')
    {
    	$data = [
    		'status' => $status,
    		'completed_by' => $user_id
    	];
    	return $this->db->where('id', $taskid)->update('tasks', $data);
    }
    
    public function task_change_priority($taskid='', $priority='', $user_id='')
    {
    	$data = [
    		'priority' => $priority,
    		'completed_by' => $user_id
    	];
    	return $this->db->where('id', $taskid)->update('tasks', $data);
    }

    public function update_startdate($startdate='', $taskid='')
    {
    	$data = ['startdate' => $startdate];
    	return $this->db->where('id', $taskid)->update('tasks', $data);
    	
    }

    public function update_duedate($duedate='', $taskid='')
    {
    	$data = ['duedate' => $duedate];
    	return $this->db->where('id', $taskid)->update('tasks', $data);
    	
    }

    public function update_description($taskid='', $description='')
    {
    	$data = ['description' => $description];
    	return $this->db->where('id', $taskid)->update('tasks', $data);
    }

    public function add_task_reminder($taskid='', $reminder_staff='', $reminder_date='', $reminder_description, $creator='')
    {
        $data = ['description' => $reminder_description, 'date' => $reminder_date, 'rel_id' => $taskid, 'staff' => $reminder_staff, 'rel_type' => 'task', 'creator'=>$creator];

        return $this->db->insert('task_reminders', $data);
    }

    public function remove_task_reminder($reminder_id)
    {
        return $this->db->where('id', $reminder_id)->delete('task_reminders');
    }

    public function calc_task_total_time($task_id, $where = '')
    {
        //$sql = 'SELECT SUM(CASE 
            //WHEN end_time is NULL THEN ' . time() . '-start_time
            //ELSE end_time-start_time
            //END) as total_logged_time FROM sma_taskstimers WHERE task_id =' . $task_id . $where;
        $sql = 'SELECT SUM(duration) as total_logged_time FROM sma_taskstimers WHERE task_id =' . $task_id ;
        $result = $this->db->query($sql)->row();

        if ($result) {
            return $result->total_logged_time;
        }

        return 0;
    }

    public function copy_task($data, $overwrites = [])
    {
        $task           = $this->getTask($data['copy_from']);
        $fields_tasks   = $this->db->list_fields('tasks');
        $_new_task_data = [];

        foreach ($fields_tasks as $field) {
            if (isset($task->$field)) {
                $_new_task_data[$field] = $task->$field;
            }
        }

        unset($_new_task_data['id']);

        if (isset($data['copy_task_status']) && is_numeric($data['copy_task_status'])) {
            $_new_task_data['status'] = $data['copy_task_status'];
        } else {
            // fallback in case no status is provided
            $_new_task_data['status'] = 1;
        }

        $_new_task_data['dateadded']         = date('Y-m-d H:i:s');
        $_new_task_data['startdate']         = date('Y-m-d');
        $_new_task_data['deadline_notified'] = 0;
        $_new_task_data['billed']            = 0;
        $_new_task_data['invoice_id']        = 0;
        $_new_task_data['total_cycles']      = 0;
        $_new_task_data['is_recurring_from'] = null;

        if (!empty($task->duedate)) {
            $dStart                    = new DateTime($task->startdate);
            $dEnd                      = new DateTime($task->duedate);
            $dDiff                     = $dStart->diff($dEnd);
            $_new_task_data['duedate'] = date('Y-m-d', strtotime(date('Y-m-d', strtotime('+' . $dDiff->days . 'DAY'))));
        }

        // Overwrite data options
        if (count($overwrites) > 0) {
            foreach ($overwrites as $key => $val) {
                $_new_task_data[$key] = $val;
            }
        }

        unset($_new_task_data['datefinished']);
// echo "<pre>";print_r($_new_task_data);die();
        //$_new_task_data = hooks()->apply_filters('before_add_task', $_new_task_data);

        $this->db->insert('tasks', $_new_task_data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            // $tags = get_tags_in($data['copy_from'], 'task');
            // handle_tags_save($tags, $insert_id, 'task');

            if (isset($data['copy_task_checklist_items']) && $data['copy_task_checklist_items'] == 'true') {
                $this->copy_task_checklist_items($data['copy_from'], $insert_id);
            }

            if (isset($data['copy_task_assignees']) && $data['copy_task_assignees'] == 'true') {
                $this->copy_task_assignees($data['copy_from'], $insert_id);
            }

            //$this->copy_task_custom_fields($data['copy_from'], $insert_id);

            //hooks()->do_action('after_add_task', $insert_id);

            return $insert_id;
        }

        return false;
    }

    public function copy_task_checklist_items($from_task, $to_task)
    {
        $checklists = $this->get_checklist_items($from_task);
        foreach ($checklists as $list) {
            $this->db->insert('task_checklist_items', [
                'taskid'      => $to_task,
                'finished'    => 0,
                'description' => $list['description'],
                'dateadded'   => date('Y-m-d H:i:s'),
                'addedfrom'   => $list['addedfrom'],
                'list_order'  => $list['list_order'],
                'assigned'  => $list['assigned'],
            ]);
        }
    }

    public function copy_task_assignees($from_task, $to_task)
    {
        $assignees = $this->db->get_where('task_assigned', array('taskid'=>$from_task))->result_array();
        foreach ($assignees as $assignee) {
            $this->db->insert('task_assigned', [
                'taskid'        => $to_task,
                'staffid'       => $assignee['staffid'],
                'assigned_from' => $this->session->userdata('user_id'),
            ]);
        }
    }

    public function get_task_assignees($taskid='')
    {
        $assignees = $this->db->get_where('task_assigned', array('taskid'=>$taskid))->result_array();
        return $assignees;
    }

    public function get_all_users() 
    {
        return $this->db->get('users')->result_array();
    }


}
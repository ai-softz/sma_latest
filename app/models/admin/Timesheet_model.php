<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Timesheet_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add_office_shift($data)
    {

    	$insert = $this->db->insert('office_shift', $data);
    	// print_r($this->db->error());die;
    	if($insert) {
    		return true;
    	} else {
    		return false;
    	}
    }

    // get record of office shift > by id
	 public function read_office_shift_information($id) {
	
		$sql = 'SELECT * FROM '. db_prefix() .'office_shift WHERE office_shift_id = ? limit 1';
		$binds = array($id);
		$query = $this->db->query($sql, $binds);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}
    // check if check-in available
	public function attendance_first_in_check($employee_id,$attendance_date) {
	
		$sql = 'SELECT * FROM '. db_prefix() .'attendance_time WHERE employee_id = ? and attendance_date = ? limit 1';
		$binds = array($employee_id,$attendance_date);
		$query = $this->db->query($sql, $binds);

		return $query;
	}

    // check if check-in available
	public function attendance_first_in($employee_id,$attendance_date) {
	
		$sql = 'SELECT * FROM '. db_prefix() .'attendance_time WHERE employee_id = ? and attendance_date = ?';
		$binds = array($employee_id,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query->result();
	}
    // get total hours work > attendance
	public function total_hours_worked_attendance($id,$attendance_date) {
		
		$sql = 'SELECT * FROM '. db_prefix() .'attendance_time WHERE employee_id = ? and attendance_date = ? and total_work != ""';
		$binds = array($id,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query;
	}
    // check if holiday available
	public function holiday_date_check($attendance_date) {
	
		$sql = 'SELECT * FROM '. db_prefix() .'holidays WHERE (start_date between start_date and end_date) or (start_date = ? or end_date = ?) limit 1';
		$binds = array($attendance_date,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query;
	}
    // check if holiday available
	public function holiday_date($attendance_date) {
	
		$sql = 'SELECT * FROM '. db_prefix() .'holidays WHERE (start_date between start_date and end_date) or (start_date = ? or end_date = ?) limit 1';
		$binds = array($attendance_date,$attendance_date);
		$query = $this->db->query($sql, $binds)->result();
		
		return $query;
	}
    // get total rest > attendance
	public function total_rest_attendance($id,$attendance_date) {
		
		$sql = 'SELECT * FROM '. db_prefix() .'attendance_time WHERE employee_id = ? and attendance_date = ? and total_rest != ""';
		$binds = array($id,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query;
	}
    // check if leave available
	public function leave_date_check($emp_id,$attendance_date) {
	
		$sql = 'SELECT * from '. db_prefix() .'leave_applications where (from_date between from_date and to_date) and employee_id = ? or from_date = ? and to_date = ? limit 1';
		$binds = array($emp_id,$attendance_date,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query;
	}
    // check if check-out available
	public function attendance_first_out_check($employee_id,$attendance_date) {
	
		$sql = 'SELECT * FROM '. db_prefix() .'attendance_time WHERE employee_id = ? and attendance_date = ? order by time_attendance_id desc limit 1';
		$binds = array($employee_id,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query;
	}
    // check if check-out available
	public function attendance_first_out($employee_id,$attendance_date) {
	
		$sql = 'SELECT * FROM '. db_prefix() .'attendance_time WHERE employee_id = ? and attendance_date = ? order by time_attendance_id desc limit 1';
		$binds = array($employee_id,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query->result();
	}
    // check if leave available
	public function leave_date($emp_id,$attendance_date) {
	
		$sql = 'SELECT * from '. db_prefix() .'leave_applications where (from_date between from_date and to_date) and employee_id = ? or from_date = ? and to_date = ? limit 1';
		$binds = array($emp_id,$attendance_date,$attendance_date);
		$query = $this->db->query($sql, $binds);
		
		return $query->result();
	}
    public function all_employees($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $dateSearch)
    {
        $this->db
            ->select('*')
            ->from('employees')
            ->join('attendance_time', 'attendance_time.employee_id = employees.user_id', 'left')
            ->where('employees.user_role_id != ', 1);
        $this->db->limit($iDisplayLength, $iDisplayStart);
        if(!empty($sSearch) && $sSearch != '') {
            $this->db
                ->like('user_id', $sSearch)
                ->or_like('employees.first_name', $sSearch)
				->or_like('employees.last_name', $sSearch)
				->or_like('employees.employee_id', $sSearch)
                ;
        }
        if(!empty($dateSearch) && $dateSearch != '1970-01-01') {
            $this->db->like('attendance_time.attendance_date', $dateSearch);
        }
        $records = $this->db->order_by('employees.user_id', $sSortDir_0)
                ->group_by('employees.user_id')
                ->get()
                ->result();
        return $records;
    }

    public function employees_update_attendance($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $filter_emp, $dateSearch)
    {
        $this->db
            ->select('attendance_time.*')
            ->from('attendance_time')
            ->where('attendance_time.employee_id', $filter_emp)
            ->like('attendance_time.attendance_date', $dateSearch)
            ;
            
        $this->db->limit($iDisplayLength, $iDisplayStart);
        $records = $this->db->order_by('attendance_time.time_attendance_id', $sSortDir_0)
                ->group_by('attendance_time.time_attendance_id')
                ->get()
                ->result();
        return $records;
    }

    public function read_attendance($id)
    {
        return $this->db->get_where('attendance_time', array('time_attendance_id'=>$id))->row();
    }

    public function total_employees($dateSearch='', $sSearch='')
    {
        $this->db
                ->select('*')
                ->from('employees')
                ->join('attendance_time', 'attendance_time.employee_id = employees.user_id', 'left')
                ->where('user_role_id != ', 1);
                if(!empty($dateSearch) && $dateSearch != '1970-01-01') {
                    $this->db->like('attendance_time.attendance_date', $dateSearch);
                }
                if(!empty($sSearch)) {
                    $this->db
                        ->like('user_id', $sSearch)
                        ->or_like('employees.first_name', $sSearch)
                        ->or_like('employees.last_name', $sSearch)
                        ->or_like('employees.employee_id', $sSearch)
                        ;
                }
                $allEmployees = $this->db
                ->group_by('employees.user_id')
                ->get()->result();
                return $allEmployees;

    }

    public function add_employee_attendance($data)
    {
        $insert = $this->db->insert('attendance_time', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function update_attendance($data, $id='')
    {
        $update = $this->db->where('time_attendance_id', $id)->update('attendance_time', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_attendance($id='')
    {
    	$delete = $this->db->where('time_attendance_id', $id)->delete('attendance_time');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function delete_office_shift($id='')
    {
    	$delete = $this->db->where('office_shift_id', $id)->delete('office_shift');
    	if($delete) {
    		return true;
    	} else {
    		return false;
    	}
    }

    public function editOfficeShift($data, $office_shift_id)
    {
        $update = $this->db->where('office_shift_id', $office_shift_id)->update('office_shift', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

  
    public function count_total_leaves($leave_type_id,$employee_id) {

        $total_leave_taken = $this->db->select('SUM(leave_days) AS total_leave_days')->get_where('leave_applications', array('employee_id'=>$employee_id, 'leave_type_id'=>$leave_type_id, 'status'=>2))->row();
        return $total_leave_taken->total_leave_days;
    }

    public function addLeaveApplication($data)
    {
        $insert = $this->db->insert('leave_applications', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function get_leave_application($id='')
    {
        return $this->db->get_where('leave_applications', array('leave_id'=>$id))->row();
    }

    public function change_leave_status($data, $leave_id)
    {

        $update = $this->db->where('leave_id', $leave_id)->update('leave_applications', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function editLeaveApplication($data, $leave_id)
    {
        $update = $this->db->where('leave_id', $leave_id)->update('leave_applications', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_leave_application($id='')
    {
        $delete = $this->db->where('leave_id', $id)->delete('leave_applications');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function read_user_info($user_id)
    {
        return $this->db->get_where('employees', array('user_id' => $user_id))->result();
    }

    public function get_leave_type($id)
    {
        return $this->db->get_where('leave_type', array('leave_type_id' => $id))->row();

    }
    

    public function delete_holiday($id='')
    {
        $delete = $this->db->where('holiday_id', $id)->delete('holidays');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function all_employees_attendances($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db->select('employees.*')
            ->from('employees')
            ->where('user_role_id !=', 1);
        if(!empty($sSearch)) {
            $this->db
                ->like('employees.user_id', $sSearch)
                ->or_like('employees.first_name', $sSearch)
                ->or_like('employees.last_name', $sSearch);
        }
        $this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('employees.user_id', $sSortDir_0)
					->group_by('employees.user_id')
					->get()
					->result();

        return $records;
    }
    
    public function totalEmployees($sSearch)
	{
		$this->db->select('*')
					->from('employees')
					->join('salary_payslips', 'salary_payslips.employee_id = employees.user_id', 'left')
					->where('employees.user_role_id != ', 1);
					if(!empty($sSearch)) {
						$this->db
							->like('employees.user_id', $sSearch)
							->or_like('employees.first_name', $sSearch)
							->or_like('employees.last_name', $sSearch);
					}					
		$records =  $this->db
		->group_by('employees.user_id')
					->get()
					->result();
					// print_r($records); die;
        return $records;
	}
}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_payslip_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPayslip($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch, $empid, $department_id, $year, $month)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_salary_mst');
        if(!empty($empid)) {
            $this->db->where('employee_id', $empid);
        }
        if(!empty($department_id)) {
            $this->db->where('department_id', $department_id);
        }
        if(!empty($year) && !empty($month)) {
            $this->db->where('salary_year', $year);
            $this->db->where('salary_month_int', $month);
        }    
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_salary_mst.caption', $sSearch)
                ->or_like('hrm_prl_salary_mst.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_prl_salary_mst.id', $sSortDir_0)
					->group_by('hrm_prl_salary_mst.id')
					->get()
					->result();

        return $records;
    } 
    public function total_Payslip($sSearch, $empid, $department_id, $year, $month)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_salary_mst');
        if(!empty($empid)) {
            $this->db->where('employee_id', $empid);
        }
        if(!empty($department_id)) {
            $this->db->where('department_id', $department_id);
        }
        if(!empty($year) && !empty($month)) {
            $this->db->where('salary_year', $year);
            $this->db->where('salary_month_int', $month);
        }        
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_salary_mst.caption', $sSearch)
                ->or_like('hrm_prl_salary_mst.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_prl_salary_mst.id')
					->get()
					->result();

        return $records;
    }

    public function get_user_byId($id)
    {
        return $this->db->where('id', $id)->get('users')->row();
    }
    
    public function getBonusSetup($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_bonus_setup');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_bonus_setup.caption', $sSearch)
                ->or_like('hrm_prl_bonus_setup.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_prl_bonus_setup.id', $sSortDir_0)
					->group_by('hrm_prl_bonus_setup.id')
					->get()
					->result();

        return $records;
    } 
    public function total_getBonusSetup($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_bonus_setup');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_bonus_setup.caption', $sSearch)
                ->or_like('hrm_prl_bonus_setup.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_prl_bonus_setup.id')
					->get()
					->result();

        return $records;
    }
    public function delete_bonus_setup($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_prl_bonus_setup');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function add_bonus_setup($data)
    {
        $insert = $this->db->insert('hrm_prl_bonus_setup', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function get_bonus_setup_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_prl_bonus_setup')->row();
    }
    public function edit_bonus_setup($id, $data)
    {
        $update = $this->db->where('id', $id)->update('hrm_prl_bonus_setup', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_salary_group($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_sal_grp');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_sal_grp.caption', $sSearch)
                ->or_like('hrm_prl_sal_grp.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_prl_sal_grp.id', $sSortDir_0)
					->group_by('hrm_prl_sal_grp.id')
					->get()
					->result();

        return $records;
    } 
    public function total_get_salary_group($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_prl_sal_grp');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_prl_sal_grp.caption', $sSearch)
                ->or_like('hrm_prl_sal_grp.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_prl_sal_grp.id')
					->get()
					->result();

        return $records;
    }
    public function delete_salary_group($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_prl_sal_grp');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function add_salary_group($data)
    {
        $insert = $this->db->insert('hrm_prl_sal_grp', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function get_salary_group_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_prl_sal_grp')->row();
    }
    public function edit_salary_group($id, $data)
    {
        $update = $this->db->where('id', $id)->update('hrm_prl_sal_grp', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_payroll_loan_proof($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_proof');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_proof.caption', $sSearch)
                ->or_like('hrm_loan_proof.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_loan_proof.id', $sSortDir_0)
					->group_by('hrm_loan_proof.id')
					->get()
					->result();

        return $records;
    } 
    public function total_payroll_loan_proof($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_proof');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_proof.caption', $sSearch)
                ->or_like('hrm_loan_proof.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_loan_proof.id')
					->get()
					->result();

        return $records;
    }
    public function add_loan_proof($data)
    {
        $insert = $this->db->insert('hrm_loan_proof', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_loan_proof($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_loan_proof');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_loan_proof_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_loan_proof')->row();
    }
    public function edit_loan_proof($id, $data)
    {
        $update = $this->db->where('id', $id)->update('hrm_loan_proof', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_payroll_loan_type($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_type');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_type.caption', $sSearch)
                ->or_like('hrm_loan_type.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_loan_type.id', $sSortDir_0)
					->group_by('hrm_loan_type.id')
					->get()
					->result();

        return $records;
    } 
    public function total_payroll_loan_type($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_type');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_type.caption', $sSearch)
                ->or_like('hrm_loan_type.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_loan_type.id')
					->get()
					->result();

        return $records;
    }
    public function add_loan_type($data)
    {
        $insert = $this->db->insert('hrm_loan_type', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_loan_type($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_loan_type');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_loan_type_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_loan_type')->row();
    }
    public function edit_loan_type($id, $data)
    {
        $update = $this->db->where('id', $id)->update('hrm_loan_type', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_payroll_loan_request($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_advance');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_advance.caption', $sSearch)
                ->or_like('hrm_loan_advance.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_loan_advance.id', $sSortDir_0)
					->group_by('hrm_loan_advance.id')
					->get()
					->result();

        return $records;
    } 
    public function total_payroll_loan_request($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_advance');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_advance.caption', $sSearch)
                ->or_like('hrm_loan_advance.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_loan_advance.id')
					->get()
					->result();

        return $records;
    }
    public function get_all_loan_proof()
    {
        return $this->db->get('hrm_loan_proof')->result();
    }
    public function add_loan_request($data)
    {
        $insert = $this->db->insert('hrm_loan_advance', $data);
        if($insert) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {
            return false;
        }
    }
    
    public function add_loan_proof_doc($data)
    {
        $insert = $this->db->insert('hrm_loan_proof_doc', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_loan_request($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_loan_advance');
        if($delete) {
            $loan_proof_docs = $this->db->get_where('hrm_loan_proof_doc', ['loan_advance_id' => $id])->result();
            // print_r($loan_proof_docs); die;
            foreach($loan_proof_docs as $value) {
                unlink('assets/uploads/loan_proof/'.$value->document_path);
            }
            $this->db->where('loan_advance_id', $id)->delete('hrm_loan_proof_doc');
            return true;
        } else {
            return false;
        }
    }
    public function get_loan_request_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_loan_advance')->row();
    }
    public function get_loan_proof_docs($id)
    {
        return $this->db->where('loan_advance_id', $id)->get('hrm_loan_proof_doc')->result();
    }
    public function edit_loan_request($id, $data)
    {
        $update = $this->db->where('id', $id)->update('hrm_loan_advance', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_proof_doc($id)
    {
        $loan_proof_docs = $this->db->get_where('hrm_loan_proof_doc', ['id' => $id])->row();            
        unlink('assets/uploads/loan_proof/'.$loan_proof_docs->document_path);
            
        $delete = $this->db->where('id', $id)->delete('hrm_loan_proof_doc');
        if($delete) {
            return true;
        } else {
            return false;
        }
    }
    public function get_payroll_loan_advance($iDisplayLength, $iDisplayStart, $sSortDir_0, $iSortCol_0, $sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_advance');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_advance.caption', $sSearch)
                ->or_like('hrm_loan_advance.caption_alt', $sSearch);
        }

		$this->db->limit($iDisplayLength, $iDisplayStart);
		$records =  $this->db->order_by('hrm_loan_advance.id', $sSortDir_0)
					->group_by('hrm_loan_advance.id')
					->get()
					->result();

        return $records;
    } 
    public function total_payroll_loan_advance($sSearch)
    {
        $this->db
            ->select('*')
            ->from('hrm_loan_advance');
        if(!empty($sSearch)) {
            $this->db
                ->or_like('hrm_loan_advance.caption', $sSearch)
                ->or_like('hrm_loan_advance.caption_alt', $sSearch);
        }
        $records =  $this->db->group_by('hrm_loan_advance.id')
					->get()
					->result();

        return $records;
    }
    public function add_loan_advance($data)
    {
        $insert = $this->db->insert('hrm_loan_advance', $data);
        if($insert) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {
            return false;
        }
    }
    public function add_loan_advance_dtls($data)
    {
        $insert = $this->db->insert('hrm_loan_advance_dtl', $data);
        if($insert) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {
            return false;
        }
    }
    public function delete_loan_advance($id)
    {
        $delete = $this->db->where('id', $id)->delete('hrm_loan_advance');
        if($delete) {
            $this->db->where('loan_advance_id', $id)->delete('hrm_loan_advance_dtl');
            return true;
        } else {
            return false;
        }
    }
    public function get_loan_advance_byId($id)
    {
        return $this->db->where('id', $id)->get('hrm_loan_advance')->row();
    }
    public function edit_loan_advance($id, $data)
    {
        $update = $this->db->where('id', $id)->update('hrm_loan_advance', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }
    public function get_loan_advance_dtls($id)
    {
        return $this->db->where('loan_advance_id', $id)->get('hrm_loan_advance_dtl')->result();
    }
}
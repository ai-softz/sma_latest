<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hrm_roster_api extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

    }

    public function add_attendance($data)
    {
        // print_r($data); die;
        $insert = $this->db->insert('attendance_time', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }
    public function add_attendance_log($data)
    {
        // print_r($data); die;
        $insert = $this->db->insert('hrm_attendance_log', $data);
        if($insert) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserCardid($cardId){
        $this->db->like('employee_id', $cardId, 'after');
        $res = $this->db->get('employees')->row();
        if($res){
            return $res->employee_id;
        }else{
            return false;
        }
    } 

    public function getUserAttendanceLog($data){
        if($this->db->get_where('hrm_attendance_log', $data)->row()){
            return true;
        }else{
            return false;
        }
    }
}
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login_api extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->api_model('login_api');
        $this->load->admin_model('auth_model');
		$this->hash_method    = $this->config->item('hash_method', 'ion_auth');
		$this->store_salt      = $this->config->item('store_salt', 'ion_auth');
		$this->salt_length     = $this->config->item('salt_length', 'ion_auth');
    }

    public function login($identity, $password)
    {



       
    	$this->load->helper('email');
        $this->identity_column = valid_email($identity) ? 'email' : 'username';

        $query = $this->db->select($this->identity_column . ', username, email, id, password, active, last_login, last_ip_address, avatar, gender, group_id, warehouse_id, biller_id, company_id, view_right, edit_right, allow_discount, show_cost, show_price')
            ->where($this->identity_column, $this->db->escape_str($identity))
            ->limit(1)
            ->get('users');   


        if ($query->num_rows() === 1) {
            $user = $query->row();

            $password = $this->hash_password_db($user->id, $password);

            if ($password === true) {
                if ($user->active != 1) {
                    $this->set_response([
                    'message' => lang('user_not_active'),
                    'status'  => false,
                ], REST_Controller::HTTP_OK);
                }

                return $query->row();
            }
        }

         return false;
    }

    public function hash_password_db($id, $password, $use_sha1_override = false)
    {
        if (empty($id) || empty($password)) {
            return false;
        }

        $query = $this->db->select('password, salt')
            ->where('id', $id)
            ->limit(1)
            ->get('users');

        $hash_password_db = $query->row();

        if ($query->num_rows() !== 1) {
            return false;
        }
        // bcrypt
        if ($use_sha1_override === false && $this->hash_method == 'bcrypt') {
            if ($this->bcrypt->verify($password, $hash_password_db->password)) {

                return true;
            }

            return false;
        }

        // sha1
        if ($this->store_salt) {
            $db_password = sha1($password . $hash_password_db->salt);
        } else {
            $salt = substr($hash_password_db->password, 0, $this->salt_length);

            $db_password = $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
        }

        if ($db_password == $hash_password_db->password) {
            return true;
        } else {
            return false;
        }
    }


}
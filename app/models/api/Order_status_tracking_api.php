<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Order_status_tracking_api extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($data) 
    {
        $this->db->insert('order_status_tracking', $data);
    }

    public function getStatus($status_id, $order_id){

    	$conds = array("order_id"=> $order_id, "status_id" => $status_id);
    	if($this->db->get_where('order_status_tracking', $conds)->row()){
    		return true;
    	}else{
    		return false;
    	}
    }

}
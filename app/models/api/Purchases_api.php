<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Purchases_api extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function countPurchases($filters = [], $ref = null)
    {
        if ($filters['supplier_id']) {
            $this->db->where('supplier_id', $filters['supplier_id']);
        }
        if ($filters['start_date']) {
            $this->db->where('date >=', $filters['start_date']);
        }
        if ($filters['end_date']) {
            $this->db->where('date <=', $filters['end_date']);
        }
        $this->db->from('purchases');
        return $this->db->count_all_results();
    }

    public function getProductVariantByID($id)
    {
        return $this->db->get_where('product_variants', ['id' => $id], 1)->row();
    }

    public function getPurchase($filters)
    {
        if (!empty($purchases = $this->getPurchases($filters))) {
            return array_values($purchases)[0];
        }
        return false;
    }

    public function getPurchaseItems($purchase_id)
    {
        return $this->db->get_where('purchase_items', ['purchase_id' => $purchase_id])->result();
    }

    public function getPurchases($filters = [])
    {
        if ($filters['supplier_id']) {
            $this->db->where('supplier_id', $filters['supplier_id']);
        }
        if ($filters['start_date']) {
            $this->db->where('date >=', $filters['start_date']);
        }
        if ($filters['end_date']) {
            $this->db->where('date <=', $filters['end_date']);
        }
        if (isset($filters['is_synchronize'])) {
            $this->db->where('is_synchronize', $filters['is_synchronize']);
        }
        if ($filters['reference']) {
            $this->db->where('reference_no', $filters['reference']);
        } else {
            $this->db->order_by($filters['order_by'][0], $filters['order_by'][1] ? $filters['order_by'][1] : 'desc');
            $this->db->limit($filters['limit'], ($filters['start'] - 1));
        }

        return $this->db->get('purchases')->result();
    }

    public function getUser($id)
    {
        $uploads_url = base_url('assets/uploads/');
        $this->db->select("CONCAT('{$uploads_url}', avatar) as avatar_url, email, first_name, gender, id, last_name, username");
        return $this->db->get_where('users', ['id' => $id], 1)->row();
    }

    public function getWarehouseByID($id)
    {
        return $this->db->get_where('warehouses', ['id' => $id], 1)->row();
    }

    public function addExpense($data = [])
    {
        $insert = $this->db->insert('expenses', $data);
        $expense_id = $this->db->insert_id();
            // print_r($this->db->error());die;
        if ($insert) {
            
            if ($this->site->getReference('ex') == $data['reference']) {
                $this->site->updateReference('ex');
            }
            return $expense_id;
        }
        return false;
    }

    public function addPurchase($data, $items, $payment)
    {
        
        $this->db->trans_start();
        if ($this->db->insert('purchases', $data)) {
            
            $purchase_id = $this->db->insert_id();
            // print_r(purchase_id);die;
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            // if ($this->site->getReference('rep') == $data['return_purchase_ref']) {
            //     $this->site->updateReference('rep');
            // }
            
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $item['option_id']   = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : null;
                $this->db->insert('purchase_items', $item);
                // print_r($this->db->error());die;
                if ($this->Settings->update_cost) {
                    $this->db->update('products', ['cost' => $item['base_unit_cost']], ['id' => $item['product_id']]);
                    if ($item['option_id']) {
                        $this->db->update('product_variants', ['cost' => $item['base_unit_cost']], ['id' => $item['option_id'], 'product_id' => $item['product_id']]);
                    }
                }
                if ($data['status'] == 'received' || $data['status'] == 'returned') {
                    //$this->updateAVCO(['product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'quantity' => $item['quantity'], 'cost' => $item['base_unit_cost'] ?? $item['real_unit_cost']]);
                }
            }

            $payment['purchase_id'] = $purchase_id;
            if($this->db->insert('payments', $payment)) {
                $this->site->syncPurchasePayments($purchase_id);
            }

            if ($data['status'] == 'returned') {
                $this->db->update('purchases', ['return_purchase_ref' => $data['return_purchase_ref'], 'surcharge' => $data['surcharge'], 'return_purchase_total' => $data['grand_total'], 'return_id' => $purchase_id], ['id' => $data['purchase_id']]);
            }

            if ($data['status'] == 'received' || $data['status'] == 'returned') {
                $this->site->syncQuantity(null, $purchase_id);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            log_message('error', 'An errors has been occurred while adding the sale (Add:Purchases_model.php)');
        } else {
            return $purchase_id;
        }
        return $purchase_id;
    }

    public function getPurchasePayments($purchase_id)
    {
        $this->db->select('payments.*, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', ['purchase_id' => $purchase_id]);
        
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                
                $data[] = $row;
            }
            // print_r($data); die;
            return $data;
        }
        return false;
    }

    public function update_is_sync($purchase_id)
    {
       $update =  $this->db->where('id', $purchase_id)->set('is_synchronize', 1)->update('purchases');
       return true;
    }

}

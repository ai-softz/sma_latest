<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sales_api extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function countSales($filters = [], $ref = null)
    {
        if ($filters['customer']) {
            $this->db->where('customer', $filters['customer']);
        }
        if ($filters['customer_id']) {
            $this->db->where('customer_id', $filters['customer_id']);
        }
        if ($filters['start_date']) {
            $this->db->where('date >=', $filters['start_date']);
        }
        if ($filters['end_date']) {
            $this->db->where('date <=', $filters['end_date']);
        }
        $this->db->from('sales');
        return $this->db->count_all_results();
    }

    public function getProductVariantByID($id)
    {
        return $this->db->get_where('product_variants', ['id' => $id], 1)->row();
    }

    public function getSale($filters)
    {
        if (!empty($sales = $this->getSales($filters))) {
            return array_values($sales)[0];
        }
        return false;
    }

    public function getSaleItems($sale_id)
    {
        return $this->db->get_where('sale_items', ['sale_id' => $sale_id])->result();
    }

    public function update_sales($data, $id) 
    {
        $this->db->where(array('id'=>$id))->update('sales', $data);
    }

    public function getSaleByID($conds) {
        return $this->db->get_where('sales', $conds)->row();
    }

    public function getSales($filters = [])
    {
        if ($filters['order_no']) {
            $this->db->where('id', $filters['order_no']);
        }
        if ($filters['customer']) {
            $this->db->where('customer', $filters['customer']);
        }
        if ($filters['customer_id']) {
            $this->db->where('customer_id', $filters['customer_id']);
        }
        if ($filters['is_printed']) {
            $this->db->where('is_printed', $filters['is_printed']);
            //0 is printed and 1 not printed
        }
        if ($filters['order_status']) {
            $this->db->where('order_status', $filters['order_status']);
        }
        if ($filters['order_platform']) {
            $this->db->where('order_platform', $filters['order_platform']);
        }
        if ($filters['customer_type']) {
            $this->db->where('customer_type', $filters['customer_type']);
        }
        if ($filters['is_printed']) {
            $this->db->where('is_printed', $filters['is_printed']);
            //0 is not printed and 1 printed
        }
        if ($filters['start_date']) {
            $this->db->where('date >=', $filters['start_date']);
        }
        if ($filters['end_date']) {
            $this->db->where('date <=', $filters['end_date']);
        }
        if ($filters['created_by']) {
            $this->db->where('created_by', $filters['created_by']);
        }
        if ($filters['sales_id']) {
            $this->db->where('id', $filters['sales_id']);
        }else if ($filters['reference']) {
            $this->db->where('reference_no', $filters['reference']);
        } else {
            if ($filters['order_status'] == 1 && empty($filters['customer_type'])) {
                $this->db->order_by($filters['order_by'][0], 'desc');
            } else {
                $this->db->order_by($filters['order_by'][0], $filters['order_by'][1] ? $filters['order_by'][1] : 'asc');
            }
            $this->db->limit($filters['limit'], ($filters['start'] - 1));
        }

        return $this->db->get('sales')->result();
    }

    public function getSalesSync($filters = [])
    {
        if ($filters['customer']) {
            $this->db->where('customer', $filters['customer']);
        }
        if ($filters['customer_id']) {
            $this->db->where('customer_id', $filters['customer_id']);
        }
        if ($filters['order_status']) {
            $this->db->where('order_status', $filters['order_status']);
        }
        if ($filters['order_platform']) {
            $this->db->where('order_platform', $filters['order_platform']);
        }
        if ($filters['customer_type']) {
            $this->db->where('customer_type', $filters['customer_type']);
        }
        if ($filters['start_date']) {
            $this->db->where('date >=', $filters['start_date']);
        }
        if ($filters['end_date']) {
            $this->db->where('date <=', $filters['end_date']);
        }
        if ($filters['created_by']) {
            $this->db->where('created_by', $filters['created_by']);
        }
        if (isset($filters['is_synchronize'])) {
            $this->db->where('is_syncronize', $filters['is_synchronize']);
        }
        if ($filters['sales_id']) {
            $this->db->where('id', $filters['sales_id']);
        }else if ($filters['reference']) {
            $this->db->where('reference_no', $filters['reference']);
        } else {
            if ($filters['order_status'] == 1 && empty($filters['customer_type'])) {
                $this->db->order_by($filters['order_by'][0], 'desc');
            } else {
                $this->db->order_by($filters['order_by'][0], $filters['order_by'][1] ? $filters['order_by'][1] : 'asc');
            }
            $this->db->limit($filters['limit'], ($filters['start'] - 1));
        }
        //$this->db->where('is_syncronize', 1);
        return $this->db->get('sales')->result();
    }

    public function getUser($id)
    {
        $uploads_url = base_url('assets/uploads/');
        $this->db->select("CONCAT('{$uploads_url}', avatar) as avatar_url, email, first_name, gender, id, last_name, username");
        return $this->db->get_where('users', ['id' => $id], 1)->row();
    }

    public function getWarehouseByID($id)
    {
        return $this->db->get_where('warehouses', ['id' => $id], 1)->row();
    }

    public function getRestaurantTable($table_no='')
    {
        $q = $this->db->get_where('restaurant_tables', array('id' => $table_no));

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    public function getBillerByID($id)
    {
        return $this->db->get_where('companies', ['id' => $id], 1)->row();
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', ['code' => $code], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getPosRegisterBalance($user_id) {
         $q = $this->db->get_where('pos_register', ['user_id' => $user_id, 'status' => "open"], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
         return false;
    }

}

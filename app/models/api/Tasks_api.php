<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tasks_api extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getTasks($filters = [])
    {

       

            if (isset($filters['status'])) {
                $this->db->where('tasks.status', $filters['status']);
            }
            if (isset($filters['priority'])) {
                $this->db->where('tasks.priority', $filters['priority']);
            }
            if (isset($filters['is_notified'])) {
                $this->db->where('task_assigned.is_notified', $filters['is_notified']);
            }
            if ($filters['staffid']) {
                $this->db->where('task_assigned.staffid', $filters['staffid']);
            }
            if ($filters['startdate']) {
                $startdate = date('Y-m-d',strtotime($filters['startdate']));
                $this->db->where('tasks.startdate = ', $startdate);
            }
            if ($filters['duedate']) {
                $duedate = date('Y-m-d',strtotime($filters['duedate']));
                $this->db->where('tasks.duedate = ', $duedate);
            }
            // if($filters['task_id']){
            //     $this->db->where('tasks.id', $filters['task_id']);
            // }
            else {
                $this->db->order_by($filters['order_by'][0], $filters['order_by'][1] ? $filters['order_by'][1] : 'desc');
                $this->db->limit($filters['limit'], ($filters['start'] - 1));
            }
            if($filters['staffid'] || $filters['is_notified']) {
                if($filters['task_id']) {
                    $task = $this->db->select('tasks.*, task_assigned.staffid')
                            ->from('tasks')
                            ->join('task_assigned', 'task_assigned.taskid = tasks.id', 'left')
                            ->where('tasks.id', $filters['task_id'])
                            ->group_by('tasks.id')
                            ->get()
                            ->row();
                    if(!empty($task)) {       
                        $checklist_items = $this->db->get_where('task_checklist_items', array('taskid' => $task->id))->result();
                        $task->checklist_items = $checklist_items;
                        $task_data = $task; 
                    } else {
                        $task_data = $task;
                    }   
                } else {
                    $tasks = $this->db->select('tasks.*, task_assigned.staffid')
                            ->from('tasks')
                            ->join('task_assigned', 'task_assigned.taskid = tasks.id', 'left')
                            ->group_by('tasks.id')
                            ->get()
                            ->result();
                    $task_data = [];        
                    foreach($tasks as $task)  {
                        $checklist_items = $this->db->get_where('task_checklist_items', array('taskid' => $task->id))->result();
                        $task->checklist_items = $checklist_items;
                        $task_data[] = $task;
                    } 
                }
            }else {

                if($filters['task_id']) {
                    $task = $this->db->select('*')
                            ->from('tasks')
                            ->where('id', $filters['task_id'])
                            ->get()
                            ->row();

                    if(!empty($task)) {
                        $checklist_items = $this->db->get_where('task_checklist_items', array('taskid' => $task->id))->result();
                        $task->checklist_items = $checklist_items;
                        $task_data = $task; 
                    }   else {
                        $task_data = $task;
                    }
                } else { 
                    $tasks = $this->db->select('tasks.*')
                            ->from('tasks')
                            ->get()
                            ->result();
                    $task_data = [];
                    foreach($tasks as $task)  {
                        $checklist_items = $this->db->get_where('task_checklist_items', array('taskid' => $task->id))->result();
                        $task->checklist_items = $checklist_items;
                        $task_data[] = $task;
                    } 
                }
                    
            }
            return $task_data;
        
    }

    public function getNewTask($staffid='', $is_notified='')
    {
        $tasks = $this->db->select('tasks.*, task_assigned.staffid, task_assigned.is_notified')
                        ->from('tasks')
                        ->join('task_assigned', 'task_assigned.taskid = tasks.id', 'left')
                        ->where('task_assigned.staffid', $staffid)
                        ->where('task_assigned.is_notified', $is_notified)
                        ->group_by('tasks.id')
                        ->get()
                        ->result();
            return $tasks;
    }

    public function finish_checklist_item($taskid='', $checklist_id='', $finished='', $user_id='')
    {
        $data = ['finished' => $finished, 'finished_from' => $user_id];
        $update = $this->db->where(array('id' => $checklist_id, 'taskid' => $taskid))->update('task_checklist_items', $data);
        if($update) {
            return true;
        } else {
            return false;
        }
    }

    public function getChecklistByID($checklist_id='')
    {
        return $this->db->get_where('task_checklist_items', array('id'=>$checklist_id))->row();
    }

    public function countTasks($filters = [], $ref = null)
    {

        if ($filters['status']) {
            $this->db->where('status', $filters['status']);
        }
        if ($filters['startdate']) {
            $this->db->where('startdate >=', $filters['startdate']);
        }
        // if ($filters['enddate']) {
        //     $this->db->where('startdate <=', $filters['enddate']);
        // }
        $this->db->from('tasks');
        return $this->db->count_all_results();
    }

    public function updateTaskStatus($id='', $status_id='', $user_id='')
    {
    	$data = array('status' => $status_id, 'completed_by' => $user_id);
    	$update = $this->db->where('id', $id)->update('tasks', $data);

    	if($update) {
    		return true;
    	}
    	return false;
    }

    public function updateTaskPriority($id='', $priority_id='', $user_id='')
    {
    	$data = array('priority' => $priority_id, 'completed_by' => $user_id);
    	$update = $this->db->where('id', $id)->update('tasks', $data);

    	if($update) {
    		return true;
    	}
    	return false;
    }

    public function getTaskByID($task_id='')
    {
        return $this->db->where('id', $task_id)->get('tasks')->row();
    }

    public function updateNotification($taskid='', $staffid='')
    {
        //$data = array('taskid' => $taskid, 'staffid' => $staffid);
        //udpate task_assigned set is_notified=1 where staffid=staffdi and taskid=taskid

        
        if($task_assigned_id) {
            return $task_assigned_id;
        }else {
            return false;
        }
    }

    public function getTaskAssignee($task_assigned_id='')
    {
        return $this->db->where('id', $task_assigned_id)->get('task_assigned')->row();
    }

    public function getTaskAssignees($task_id='')
    {
        return $this->db->where('taskid', $task_id)->get('task_assigned')->result();
    }


    public function getTasksAssignee($filters = [])
    {

            if (isset($filters['status'])) {
                $this->db->where('tasks.status', $filters['status']);
            }
            if (isset($filters['priority'])) {
                $this->db->where('tasks.priority', $filters['priority']);
            }
            if (isset($filters['is_notified'])) {
                $this->db->where('task_assigned.is_notified', $filters['is_notified']);
            }
            // if ($filters['staffid']) {
            //     $this->db->where('task_assigned.staffid', $filters['staffid']);
            // }
            if ($filters['startdate']) {
                $startdate = date('Y-m-d',strtotime($filters['startdate']));
                $this->db->where('tasks.startdate = ', $startdate);
            }
            if ($filters['duedate']) {
                $duedate = date('Y-m-d',strtotime($filters['duedate']));
                $this->db->where('tasks.duedate = ', $duedate);
            }
            // if($filters['task_id']){
            //     $this->db->where('tasks.id', $filters['task_id']);
            // }
            else {
                $this->db->order_by($filters['order_by'][0], $filters['order_by'][1] ? $filters['order_by'][1] : 'desc');
                $this->db->limit($filters['limit'], ($filters['start'] - 1));
            }
            if($filters['staffid'] || $filters['is_notified']) {
                if($filters['task_id']) {
                    $tasks = $this->db->select('tasks.*')
                            ->from('tasks')
                            ->join('task_assigned', 'task_assigned.taskid = tasks.id', 'left')
                            ->where('task_assigned.staffid', $filters['staffid'])
                            ->where('tasks.id', $filters['task_id'])
                            ->get()
                            ->row();
                    $task_data = $tasks;        
                    
                } else {
                    $tasks = $this->db->select('tasks.*')
                            ->from('tasks')
                            ->join('task_assigned', 'task_assigned.taskid = tasks.id', 'left')
                            ->where('task_assigned.staffid', $filters['staffid'])
                            ->get()
                            ->result();
                    $task_data = $tasks;        
   
                }
            }else {
                if($filters['task_id']) {
                    $task = $this->db->select('tasks.*')
                            ->from('tasks')
                            ->where('tasks.id', $filters['task_id'])
                            ->get()
                            ->row();
                    $task_data = [];        
                    $assignees = $this->db->get_where('task_assigned', array('taskid' => $task->id))->result();
                    if(!empty($assignees)) {
                        $task->assignees = $assignees;
                    }
                    $task_data[] = $task;            
                } 
                else {
                    $tasks = $this->db->select('tasks.*')
                            ->from('tasks')
                            ->get()
                            ->result();
                    $task_data = [];        
                    foreach($tasks as $task)  {
                        $assignees = $this->db->get_where('task_assigned', array('taskid' => $task->id))->result();
                        if(!empty($assignees)) {
                            $task->assignees = $assignees;
                        }

                        $task_data[] = $task;
                        
                    }    
                }
                    
            }
            return $task_data;
                
    }

}
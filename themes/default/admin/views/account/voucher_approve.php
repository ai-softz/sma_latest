<?php $this->load->view('./admin/views/includes/head') ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
          
            <div class="panel-body">
 
                <div class="">
                    <table class="datatable table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line('sl_no') ?></th>
                                <th><?php echo $this->lang->line('voucher_no') ?></th>
                                 <th><?php echo $this->lang->line('date') ?></th>
                                <th><?php echo $this->lang->line('remark') ?></th>
                                <th><?php echo $this->lang->line('debit') ?></th>
                                <th><?php echo $this->lang->line('credit') ?></th>
                                <th><?php echo $this->lang->line('action') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($aprrove)) ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($aprrove as $approve) { ?>
                            <tr>
                                <td><?php echo $sl++; ?></td>
                                <td><?php echo html_escape($approve->VNo); ?></td>
                                <td><?php echo html_escape($approve->VDate); ?></td>
                                <td><?php echo html_escape($approve->Narration); ?></td>
                                <td><?php
                                 echo ($approve->Vtype=='CV'?0:$approve->Debit); ?></td>
                                <td><?php echo ($approve->Vtype=='DV'?0:$approve->Credit); ?></td>
                                <td>

                                <a href="<?php echo base_url("admin/account/isactive/$approve->VNo/active") ?>" onclick="return confirm('<?php echo $this->lang->line("are_you_sure") ?>')" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="right" title="Inactive"><?php echo $this->lang->line('approved')?></a>
<!--                                 if($this->permission1->method('aprove_v','update')->access()){ -->
                                <a href="<?php echo base_url("edit_voucher/$approve->VNo") ?>" class="btn btn-info btn-sm" title="Update"><i class="fa fa-edit"></i></a>
<!--                             }-->
<!--                             if($this->permission1->method('aprove_v','delete')->access()){ -->
                                <a href="<?php echo base_url("admin/account/voucher_delete/$approve->VNo") ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure?')" title="delete"><i class="fa fa-trash"></i></a>
<!--                             }-->
                                
                                </td>
                            </tr>
                            <?php } ?> 
                        </tbody>
                    </table>
                 
                </div>
            </div> 
        </div>
    </div>
</div>
    <!--start new-->
<?php $this->load->view('./admin/views/includes/js') ?>
<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_holiday'); ?></h4>
</div>

<div class="modal-body">
		<div class="row">
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
                            <?php $emp_name = $this->employees_model->get_emp_name($data->employee_id); ?>  
	                      	<th>Employee Name</th><td><?= $emp_name->first_name.' '.$emp_name->last_name ?></td>
	                      </tr>
                          <tr>
                            <?php
                             $leave_type = $this->timesheet_model->get_leave_type($data->leave_type_id); 
                             ?>  
	                      	<th>Leave Type</th><td><?= $leave_type->type_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Start Date</th><td><?= date('d M, Y', strtotime($data->from_date));  ?></td>
	                      </tr>
						  <tr>
	                      	<th>End Date</th><td><?= date('d M, Y', strtotime($data->to_date));  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Total Days</th><td><?= $data->leave_days ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Status</th><td><?php
                              if($data->status == 0) { echo 'Pending'; } 
                              else if($data->status == 1) { echo 'Approved'; } 
                              else if($data->status == 2) { echo 'Rejected'; } 
                              ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Reason</th><td><?= $data->reason ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Reason</th><td><?= $data->remarks ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		  </div>
</div>

<div class="modal-footer">
    <button class="delete_event btn btn-danger" data-id="<?= $data->leave_id ?>" type="button" >Delete</button>
</div>

<script> 
    $(document).ready(function(){

        $(document).on('click', '.delete_event', function(){
            var id = $(this).data('id');
            $.ajax({
                url: site.base_url + 'timesheets/delete_leave_application/' + id,
                method: 'GET',
                dataType: 'text',
                success : function() {
                    $('.cal_modal').modal('hide');
                    location.reload();
                }
            });
        });
    });
</script>
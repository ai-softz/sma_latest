<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_meeting'); ?></h4>
</div>

<div class="modal-body">
		<div class="row">
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
                        <tr>
	                      	<th>Employees</th><td>
                               <?php
                               $names = '';
                               if(!empty($data->employee_id)) {
                                    $employees = explode(',', $data->employee_id);
                                    $name_arr = array();
                                    foreach($employees as $value) {
                                        $name = $this->employees_model->get_emp_name($value);
                                        $name_arr[] = $name->first_name.' '.$name->last_name; 
                                    } 
                                    if(!empty($name_arr)) {
                                            $names = implode(',', $name_arr);
                                    }
                                }
                               echo $names;
                               ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Meeting Title</th>
                            <td>
                                <?= $data->meeting_title ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Meeting Date</th><td><?= date('d M, Y', strtotime($data->meeting_date));  ?></td>
	                      </tr>
						  <tr>
	                      	<th>Meeting Time</th><td><?= $data->meeting_time;  ?></td>
	                      </tr>
						  <tr>
	                      	<th>Meeting Room</th><td><?= $data->meeting_room;  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Meeting Note</th><td><?= $data->meeting_note ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		</div>
</div>
<div class="modal-footer">
    <button class="delete_event btn btn-danger" data-id="<?= $data->meeting_id ?>" type="button" >Delete</button>
</div>

<script> 
    $(document).ready(function(){

        $(document).on('click', '.delete_event', function(){
            var id = $(this).data('id');
            $.ajax({
                url: site.base_url + 'core_hr/delete_event/' + id,
                method: 'GET',
                dataType: 'text',
                success : function() {
                    $('.cal_modal').modal('hide');
                    location.reload();
                }
            });
        });
    });
</script>
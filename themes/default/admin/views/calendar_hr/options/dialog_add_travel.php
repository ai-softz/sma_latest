<?php
$fdate = $_GET['event_date'];
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_travel'); ?></h4>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-12"> 
            <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form', 'id' => 'add_travel_cal']; ?>
            
        <?php echo admin_form_open_multipart("core_hr/add_travel", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="leave_employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
				<label for="start_date"><?php echo  lang('start_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('start_date');?>" name="start_date" type="text" value="<?=  date('d-m-Y', strtotime($fdate)); ?>" required="required" autocomplete="off">
			</div> 
			
	   </div>
	   <div class="form-group row">
              
            <div class="col-md-6">
				<label for="end_date"><?php echo  lang('end_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('end_date');?>" name="end_date" type="text" value="<?=  date('d-m-Y', strtotime($fdate)); ?>" required="required" autocomplete="off">
			</div> 
            <div class="col-md-6"> 
                <label for="visit_place"><?php echo  lang('visit_place') ?> <span class="text-danger">*</span></label>
                <input type="text" name="visit_place" class="form-control" required="required">
            </div>  
	   </div>
       <div class="form-group row">
              
            <div class="col-md-6">
				<label for="visit_purpose"><?php echo  lang('visit_purpose') ?> <span 	class="text-danger">*</span></label>
				<input type="text" name="visit_purpose" class="form-control" required="required">
			</div> 
            <div class="col-md-6"> 
                <label for="expected_budget"><?php echo  lang('expected_budget') ?> <span class="text-danger"></span></label>
                <input type="text" name="expected_budget" class="form-control">
            </div>  
	   </div>
       <div class="form-group row">
            
            <div class="col-md-6"> 
                <label for="actual_budget"><?php echo  lang('actual_budget') ?> <span class="text-danger"></span></label>
                <input type="text" name="actual_budget" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="travel_mode"><?php echo  lang('travel_mode') ?> <span class="text-danger"></span></label>
                <?php
                    $travel_mode = array(
                        1 => "By Bus", 
                        2 => "By Train", 
                        3 => "By Plane", 
                        4 => "By Taxi", 
                        5 => "By Rental Car", 
                    );
                ?>
                <select name="travel_mode" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($travel_mode as $key=>$value) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value;?></option>
                        <?php } ?>
                </select>
	        </div> 
        </div>
       <div class="form-group row"> 
            
            <div class="col-md-6">
                <label for="arrangement_type"><?php echo  lang('arrangement_type') ?> <span class="text-danger"></span></label>
                <?php
                    $arrangement_type = array(
                        1 => "Corporation", 
                        2 => "Guest House", 
                    );
                ?>
                <select name="arrangement_type" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($arrangement_type as $key=>$value) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value;?></option>
                        <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"></textarea>
			</div>            
       </div>
       
	   <div class="form-group row"> 
			 			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('add_travel', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
            </div>
        </div>
    </div>
	
    <script>

$(document).ready(function(){


$('#add_travel_cal').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "core_hr/add_travel_cal",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               console.log(data);
             	  $('.cal_modal').modal('hide');
                //$('#table_working_experience').DataTable().ajax.reload();
                    location.reload();
               
             }
               
      });  
  });



});
</script>
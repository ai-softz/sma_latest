<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/add_award", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="leave_employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
	      <div class="col-md-6">
	         <label for="award_type_id"><?php echo  lang('award_type_id') ?> <span class="text-danger"></span></label>
	         <select name="award_type_id" class="form-control select" id="award_type_id">
	              <option value=""><?php echo 'Select';?></option>
	              <?php foreach($award_type as $row) { ?>
	                <option value="<?php echo $row->award_type_id; ?>"><?php echo $row->award_type;?></option>
	              <?php } ?>
	           </select>
	      </div>
	      
	   </div>
	   <div class="form-group row">
	   	<div class="col-md-6">
	         <label for="award_date"><?php echo  lang('award_date') ?> <span class="text-danger">*</span></label>
	         <input class="form-control date" placeholder="<?php echo lang('award_date');?>" name="award_date" type="text" value="" required="required" autocomplete="off">
	    </div> 
	     <div class="col-md-6">
	         <label for="award_month_year"><?php echo lang('award_month_year') ?> <span class="text-danger">*</span></label>
	         <input class="form-control date" placeholder="<?php echo lang('award_month_year');?>" name="award_month_year" type="text" value="" required="required" autocomplete="off">
	    </div>  
	      
	   </div>

       <div class="form-group row">
	   	<div class="col-md-6">
	         <label for="gift_item"><?php echo  lang('gift_item') ?> <span class="text-danger">*</span></label>
	        <input type="text" name="gift_item" class="form-control" placeholder="<?php echo lang('gift_item');?>"/>
	    </div> 	
	    <div class="col-md-6">
	         <label for="cash_price"><?php echo  lang('cash_price') ?> <span class="text-danger">*</span></label>
	        <input type="text" name="cash_price" class="form-control" placeholder="<?php echo lang('cash_price');?>"/>
	    </div> 
	   </div>

	   <div class="form-group row">
	   	<div class="col-md-6">
	         <label for="award_photo"><?php echo  lang('award_photo') ?> <span class="text-danger">*</span></label>
	        <input type="file" name="award_photo" id="award_photo" size="20" />
	    </div> 	
	    <div class="col-md-6">
	    	
	    </div>
	   </div>
	   <div class="form-group row">
		    <div class="col-md-6">
			   	<label for="award_information"><?php echo  lang('award_information') ?> <span class="text-danger"></span></label>
			   	<textarea name="award_information" class="form-control" required="required"></textarea>
		    </div>
		    <div class="col-md-6">
			   	<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
			   	<textarea name="description" class="form-control"></textarea>
	    	</div>
		</div>

         <div class="form-group">
			 <?php echo form_submit('add_award', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

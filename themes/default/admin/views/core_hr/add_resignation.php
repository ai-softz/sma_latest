<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/add_resignation", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="leave_employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
			<div class="col-md-6">
				<label for="notice_date"><?php echo  lang('notice_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('notice_date');?>" name="notice_date" type="text" value="" required="required" autocomplete="off">
			</div>
	   </div>
	   <div class="form-group row">
	   	<div class="col-md-6">
           <label for="resignation_date"><?php echo  lang('resignation_date') ?> <span 	class="text-danger">*</span></label>
			<input class="form-control date" placeholder="<?php echo lang('resignation_date');?>" name="resignation_date" type="text" value="" required="required" autocomplete="off">
	    </div> 
	    
	   </div>
	   <div class="form-group row"> 
			<div class="col-md-6">
				<label for="reason"><?php echo  lang('reason') ?> <span class="text-danger"></span></label>
				<textarea name="reason" class="form-control"></textarea>
			</div>  			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('add_resignation', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

 

<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_trainer'); ?></h4>
    </div>

    <div class="modal-body">

        <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_trainer']; ?>
		     <?php echo admin_form_open_multipart("core_hr/add_trainer", $attrib) ?>
		     
		    <div class="form-group row">
                <div class="col-md-6">
                    <label for="first_name"><?php echo  lang('first_name') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('first_name');?>" name="first_name" id="first_name" type="text" value="" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="last_name"><?php echo  lang('last_name') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('last_name');?>" name="last_name" id="last_name" type="text" value="" required="required">
                </div>
		      
		   </div>

		   <div class="form-group row">
		    <div class="col-md-6">
		         <label for="contact_number"><?php echo  lang('contact_number') ?> <span class="text-danger">*</span></label>
		         <input class="form-control" placeholder="<?php echo lang('contact_number');?>" name="contact_number" type="text" value="" required="required">
		    </div> 
            <div class="col-md-6">
                <label for="email"><?php echo  lang('email') ?> <span class="text-danger">*</span></label>
		         <input class="form-control " placeholder="<?php echo lang('email');?>" name="email" type="text" value="" required="required">
            </div> 
		   </div>
		   <div class="form-group row">

            <div class="col-md-6">
                <label for="expertise"><?php echo  lang('expertise') ?> <span class="text-danger">*</span></label>
                <input class="form-control " placeholder="<?php echo lang('expertise');?>" name="expertise" type="text" value="">
            </div> 
            <div class="col-md-6">
                <label for="address"><?php echo  lang('address') ?> <span class="text-danger"></span></label>
		         <input class="form-control " placeholder="<?php echo lang('address');?>" name="address" type="text" value="">
            </div> 
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_trainer', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_trainer').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "core_hr/add_trainer",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('#myModal').modal('hide');
                location.reload();
                // $('#table_events').DataTable().ajax.reload();
             }
               
      });  
  });


});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/add_transfer", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="leave_employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
			<div class="col-md-6">
				<label for="transfer_date"><?php echo  lang('transfer_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('transfer_date');?>" name="transfer_date" type="text" value="" required="required" autocomplete="off">
			</div>
	   </div>
	   <div class="form-group row">
	   	<div class="col-md-6">
		   	<label for="transfer_department"><?php echo  lang('transfer_to_department') ?> <span class="text-danger"></span></label>
            <select name="transfer_department" class="form-control select" id="">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_departments as $row) { ?>
                        <option value="<?php echo $row->department_id; ?>"><?php echo $row->department_name;?></option>
                    <?php } ?>
            </select>
	    </div> 
	    
	   </div>
	   <div class="form-group row"> 
			<div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"></textarea>
			</div>  			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('add_transfer', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

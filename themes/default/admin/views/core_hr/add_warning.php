<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/add_warning", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="warning_to"><?php echo  lang('warning_to') ?> <span class="text-danger"></span></label>
                <select name="warning_to" class="form-control select" id="warning_to">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
                <label for="warning_type_id"><?php echo  lang('warning_type_id') ?> <span class="text-danger"></span></label>
                <select name="warning_type_id" class="form-control select" id="warning_type_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($warning_type as $row) { ?>
                        <option value="<?php echo $row->warning_type_id; ?>"><?php echo $row->type ;?></option>
                    <?php } ?>
                </select>
	        </div> 
			
	   </div>
	   <div class="form-group row">
            <div class="col-md-6">
                <label for="subject"><?php echo  lang('subject') ?> <span class="text-danger"></span></label>
                <input name="subject" type="text" value="" class="form-control" required="required">
            </div>  
            <div class="col-md-6">
				<label for="warning_date"><?php echo  lang('warning_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('warning_date');?>" name="warning_date" type="text" value="" required="required" autocomplete="off">
			</div>
	   </div>
       <div class="form-group row">
            <div class="col-md-6">
                <label for="warning_by"><?php echo  lang('warning_by') ?> <span class="text-danger"></span></label>
                <select name="warning_by" class="form-control select" id="warning_by">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
                <label for="attachment"><?php echo  lang('attachment') ?> <span class="text-danger">*</span></label>
                <input type="file" name="attachment" id="attachment" size="20" />
            </div>
        </div>
	   <div class="form-group row"> 
             
			<div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"></textarea>
			</div>  			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('add_warning', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

<div class="panel">
    <div class="panel-body">
    <h2>Upcoming Birthdays:</h2>
    <br>
    
    <div class="table-responsive">
    <table id="table_birthdays" class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
            <th><?php echo lang('the_number_sign');?></th>
            <th><?php echo lang('employee');?></th>
            <th><?php echo lang('department');?></th>
            <th><?php echo lang('designation');?></th>
            <th><?php echo lang('birthday_date');?></th>
            <th><?php echo lang('age');?></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
</div>

<script>
   $(document).ready(function () {
    // $(document).on('click', '#working_experience', function(){
      oTable = $('#table_birthdays').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('core_hr/getBirthdays') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });

               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    // });

   });

</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<link href='<?= $assets ?>fullcalendar/css/fullcalendar.min.css' rel='stylesheet' />
<link href='<?= $assets ?>fullcalendar/css/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="<?= $assets ?>fullcalendar/css/bootstrap-colorpicker.min.css" rel="stylesheet" />

<style>
    .fc th {
        padding: 10px 0px;
        vertical-align: middle;
        background:#F2F2F2;
        width: 14.285%;
    }
    .fc-content {
        cursor: pointer;
    }
    .fc-day-grid-event>.fc-content {
        padding: 4px;
    }

    .fc .fc-center {
        margin-top: 5px;
    }
    .error {
        color: #ac2925;
        margin-bottom: 15px;
    }
    .event-tooltip {
        width:150px;
        background: rgba(0, 0, 0, 0.85);
        color:#FFF;
        padding:10px;
        position:absolute;
        z-index:10001;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        cursor: pointer;
        font-size: 11px;
    }
    .square {
        height: 22px;
        width:22px;
    }
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('calendar'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
        <div class="col-lg-3">
        <div class="draggable">
        <div class="card-header with-elements">
            <span class="card-header-title mr-2"><strong><?php echo lang('draggable_options');?></strong></span> </div>                              
            <input type="hidden" id="exact_date" value="" />
              <div class="list-group" id="list_group">
              
              <span class="list-group-item calendar-options drag-option" data-record="0"> <i class="fa fa-paper-plane fa-lg text-dark"></i> &nbsp; <?php echo lang('holidays');?> &nbsp; <i class="fa fa-square fa-lg text-dark" style="float: right"></i></span>

              <span class="list-group-item calendar-options drag-option" data-record="1"> <i class="fa fa-calendar text-primary"></i> &nbsp; <?php echo lang('leave_request');?> <i class="fa fa-square fa-lg text-primary" style="color: #26B4FF; float: right"></i></span>
             
              <span class="list-group-item calendar-options drag-option" data-record="2"> <i class="fa fa-plane text-success"></i> &nbsp; <?php echo lang('travel_request');?> <i class="fa fa-square fa-lg" style="color: #02BC77; float: right"></i></span>
             
              <span class="list-group-item calendar-options drag-option" data-record="3"> <i class="fa fa-briefcase text-info"></i> &nbsp; <?php echo lang('tranings');?> <i class="fa fa-square fa-lg text-info" style="float: right"></i></span>
              
              <span class="list-group-item calendar-options drag-option" data-record="6"> <i class="fa fa-calendar text-maroon"></i> &nbsp; <?php echo lang('events');?> <i class="fa fa-square fa-lg" style="color: #d81b60; float: right"></i></span>
             
              <span class="list-group-item calendar-options drag-option" data-record="7"> <i class="fa fa-users text-purple"></i> &nbsp; <?php echo lang('meetings');?> <i class="fa fa-square fa-lg text-purple" style="float: right; color:#605ca8"></i></span>
           
              <span class="list-group-item calendar-options"> <i class="fa fa-gift text-muted"></i> &nbsp; <?php echo lang('upcoming_birthday');?> <i class="fa fa-square text-muted fa-lg" style="float: right"></i></span>
            </div>
        </div>
        </div>
            <div class="col-lg-9">
                <p class="introtext"><?= lang('drag_option_on_date_to_add_events') ?></p>
                <div id='calendar_hr'>

                </div>
            </div>
        </div>
        
        <div class="modal fade cal_modal">
        <div class="modal-dialog">
            <div class="modal-content" id="ajax_modal_view">
                
            </div>
        </div>
        </div>
    </div>

    </div>
</div>
   
<script type="text/javascript">
    var currentLangCode = '<?= $cal_lang; ?>', moment_df = '<?= strtoupper($dateFormats['js_sdate']); ?> HH:mm', cal_lang = {},
    tkname = "<?=$this->security->get_csrf_token_name()?>", tkvalue = "<?=$this->security->get_csrf_hash()?>";
    cal_lang['add_event'] = '<?= lang('add_event'); ?>';
    cal_lang['edit_event'] = '<?= lang('edit_event'); ?>';
    cal_lang['delete'] = '<?= lang('delete'); ?>';
    cal_lang['event_error'] = '<?= lang('event_error'); ?>';
</script>
<script src='<?= $assets ?>fullcalendar/js/moment.min.js'></script>
<script src="<?= $assets ?>fullcalendar/js/fullcalendar.min.js"></script>
<script src="<?= $assets ?>fullcalendar/js/lang-all.js"></script>
<script src='<?= $assets ?>fullcalendar/js/bootstrap-colorpicker.min.js'></script>
<script src='<?= $assets ?>fullcalendar/js/main.js'></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script> 
var fcDelay = 200, fcClicks = 0, fcTimer = null;
$(document).ready(function(){

    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      });
      
    }

    init_events($('#list_group span.drag-option'));
    var date = new Date();
    var startDate, endDate;
    var currentEvent;
   // $('.selectpicker').selectpicker();
    $('#color').colorpicker();
    // Fullcalendar
    $('#calendar_hr').fullCalendar({
        lang: currentLangCode,
        isRTL: (site.settings.user_rtl == 1 ? true : false),
        eventLimit: false,
        timeFormat: 'H:mm',
        height: 550,
        // timezone: site.settings.timezone, // 'local', 'UTC' or timezone
        //ignoreTimezone: false,
        selectable: false,
        editable  : false,
        //selectHelper: true,
        droppable : true,
        // when click on a event show detail by modal
        eventRender: function(event, element) {
		element.attr('title',event.title).tooltip();
		element.attr('href', 'javascript:void(0);');
        element.click(function() {
			if(event.unq==1){
				$.ajax({
					url : site.base_url+"core_hr/read_holiday_record/",
					type: "GET",
					data: 'jd=1&is_ajax=1&mode=modal&data=view_holiday&holiday_id='+event.holiday_id,
					success: function (response) {
						if(response) {
							$('.cal_modal').modal('show');
							$("#ajax_modal_view").html(response);
						}
					}
				});
			} else if(event.unq==2){
				$.ajax({
					url : site.base_url+"core_hr/read_leave_record/",
					type: "GET",
					data: 'jd=1&is_ajax=1&mode=modal&data=view_leave&leave_id='+event.leave_id,
					success: function (response) {
						if(response) {
							$('.cal_modal').modal('show');
							$("#ajax_modal_view").html(response);
						}
					}
				});
			} else if(event.unq==4){
				$.ajax({
					url : site.base_url+"core_hr/read_travel_record/",
					type: "GET",
					data: 'jd=1&is_ajax=1&mode=modal&data=view_travel&travel_id='+event.travel_id,
					success: function (response) {
						if(response) {
							$('.cal_modal').modal('show');
							$("#ajax_modal_view").html(response);
						}
					}
				});
			} else if(event.unq==5){
				$.ajax({
					url : site.base_url+"core_hr/read_training/",
					type: "GET",
					data: 'jd=1&is_ajax=1&mode=modal&data=view_training&training_id='+event.training_id,
					success: function (response) {
						if(response) {
							$('.cal_modal').modal('show');
							$("#ajax_modal_view").html(response);
						}
					}
				});
			}
            else if(event.unq==8){
				$.ajax({
					url : site.base_url+"core_hr/read_event_record/",
					type: "GET",
					data: 'jd=1&is_ajax=1&mode=modal&data=view_event&event_id='+event.event_id,
					success: function (response) {
						if(response) {
							$('.cal_modal').modal('show');
							$("#ajax_modal_view").html(response);
						}
					}
				});
			} 
            else if(event.unq==9){
				$.ajax({
					url : site.base_url+"core_hr/read_meeting_record/",
					type: "GET",
					data: 'jd=1&is_ajax=1&mode=modal&data=view_meeting&meeting_id='+event.meeting_id,
					success: function (response) {
						if(response) {
							$('.cal_modal').modal('show');
							$("#ajax_modal_view").html(response);
						}
					}
				});
			} 
        });
		
		},
        // when element is dropped then modal will be opened 
        drop : function (date, allDay) { 
            //alert('Hello');
            var event_date = date.format();
			$('#exact_date').val(event_date);
			var this_record = $(this).data('record');
			$('.cal_modal').modal('show');
			var ex_date = $('#exact_date').val();
			$.ajax({
			url : site.base_url+"core_hr/add_cal_record/",
			type: "GET",
			data: 'jd=1&is_ajax=1&mode=modal&data=event&event_date='+ex_date+"&record="+this_record,
			success: function (response) {
				if(response) {
					$("#ajax_modal_view").html(response);
				}
                //$('#calendar_hr').fullCalendar("refetchEvents");
			}
		})
        },
        header: {
            left: 'prev, next, today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        // get all events and show them in calendar
        events: [
			<?php 
            foreach($all_holidays->result() as $holiday):
                ?>
			{
				holiday_id: '<?php echo $holiday->holiday_id?>',
				title: "<?php echo $holiday->event_name?>",
				start: '<?php echo $holiday->start_date?>',
				end: '<?php echo $holiday->end_date?>',
				color: 'rgba(24,28,33,0.9) !important',
				unq: '1',
			},
            <?php endforeach;?>
            <?php 
            foreach($leave_applications->result() as $value):

            $leave_type = $this->db->select('type_name')->get_where('leave_type', array('leave_type_id'=>$value->leave_type_id))->row();
            $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $name = $emp_name->first_name.' '.$emp_name->last_name;
            ?>
			{
				leave_id: '<?php echo $value->leave_id ?>',
				title: "<?php echo $leave_type->type_name.' '.lang('by').' '.$name?>",
				start: '<?php echo date('Y-m-d',strtotime($value->from_date)) ?>',
				end: '<?php echo date('Y-m-d',strtotime($value->to_date)) ?>',
				color: '#26B4FF !important',
				unq: '2',
			},
            <?php endforeach;?>
            <?php foreach($all_meetings as $meetings):?>
			{
				meeting_id: '<?php echo $meetings->meeting_id?>',
				title: "<?php echo $meetings->meeting_title?>",
				start: '<?php echo $meetings->meeting_date?>T<?php echo $meetings->meeting_time?>',
				color: '#605ca8 !important',
				unq: '9',
				className: "regular"
			},
			<?php endforeach;?>
            <?php foreach($all_events as $events):?>
			{
				event_id: '<?php echo $events->event_id?>',
				title: "<?php echo $events->event_title?>",
				start: '<?php echo $events->event_date?>T<?php echo $events->event_time?>',
				color: '#d81b60 !important',
				unq: '8',
			},
			<?php endforeach;?>
            <?php foreach($all_upcoming_birthday as $upc_birthday):?>
			{
				title: "<?php echo $upc_birthday->first_name.' '.$upc_birthday->last_name?> - <?php echo lang('calendar_upc_birthday');?>",
				start: '<?php echo $upc_birthday->next_birthday?>',
				color: '#a3a4a6 !important',
				unq: '3',
			},
			<?php endforeach;?>
            <?php foreach($all_travel_request->result() as $travel_request):?>
			<?php $emp_name = $this->employees_model->get_emp_name($value->employee_id);
            $eName = $emp_name->first_name.' '.$emp_name->last_name; ?>
			{
				travel_id: '<?php echo $travel_request->travel_id?>',
				title: "<?php echo $travel_request->visit_purpose.' '.lang('travel_request_by').' '.$eName;?>",
				start: '<?php echo $travel_request->start_date?>',
				end: '<?php echo $travel_request->end_date?>',
				color: '#02BC77 !important',
				unq: '4',
			},
			<?php endforeach;?>
            <?php foreach($all_training->result() as $training):?>
			
			{
				training_id: '<?php echo $training->training_id?>',
				title: "<?php echo $training->training_title;?>",
				start: '<?php echo $training->start_date?>',
				end: '<?php echo $training->finish_date?>',
				color: '#31708f !important',
				unq: '5',
			},
			<?php endforeach;?>
        ],
        
    });

   

});
</script>

<style type="text/css">
.fc-event.fc-draggable, .fc-event[href], .fc-popover .fc-header .fc-close {
    cursor: pointer;
}
.fc-event { line-height: 2.0 !important; }
.drag-option {
	cursor: move !important;
}
</style>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/editPromotion", $attrib) ?>
        <input type="hidden" name="promotion_id" value="<?= $promotion->promotion_id ?>">
        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>" <?= $row->user_id == $promotion->employee_id ? 'Selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
			<div class="col-md-6">
                <label for="designation_id"><?php echo  lang('designation_id') ?> <span class="text-danger"></span></label>
                <div id="designation_select">
                <select name="designation_id" class="form-control select" id="designation_id">
                        <option value=""><?php echo 'Select';?></option>
                        <option value="<?php echo $designation->designation_id; ?>" <?= $promotion->designation_id == $designation->designation_id ? 'Selected' : '' ?>><?php echo $designation->designation_name;?></option>
                </select>
                </div>
				
			</div>
	   </div>
	   <div class="form-group row">
            
            <div class="col-md-6">
                <label for="title"><?php echo  lang('title') ?> <span class="text-danger"></span></label>
                <input name="title" type="text" value="<?= $promotion->title ?>" class="form-control">
            </div>  			
            
            <div class="col-md-6">
            <label for="promotion_date"><?php echo  lang('promotion_date') ?> <span 	class="text-danger">*</span></label>
                    <input class="form-control date" placeholder="<?php echo lang('promotion_date');?>" name="promotion_date" type="text" value="<?= date('d-m-Y', strtotime($promotion->promotion_date)) ?>" required="required" autocomplete="off">
            </div> 
	   </div>
	   <div class="form-group row">

		    <div class="col-md-6">
			   	<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
			   	<textarea name="description" class="form-control"><?= $promotion->description ?></textarea>
	    	</div>
		</div>

         <div class="form-group">
			 <?php echo form_submit('edit_promotion', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

<script>

$(document).ready(function() {
    $(document).on('change', '#employee_id', function(){
        var empId = $(this).val();

        $.ajax({
            url: site.base_url + "core_hr/getDesignation",
            method: 'POST',
            type: 'text',
            data : {empId: empId},
            success : function(data) {
                $('#designation_id').remove();
                $('#designation_select').html(data);
            }
            
        });
    });
});

</script>
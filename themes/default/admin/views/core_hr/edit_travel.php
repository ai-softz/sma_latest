<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/editTravel", $attrib) ?>
        <input type="hidden" name="travel_id" value="<?= $travel->travel_id ?>">
        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="leave_employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>" <?= $row->user_id == $travel->employee_id ? 'Selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
				<label for="start_date"><?php echo  lang('start_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('start_date');?>" name="start_date" type="text" value="<?= date('d-m-Y', strtotime($travel->start_date)) ?>" required="required" autocomplete="off">
			</div> 
			
	   </div>
	   <div class="form-group row">
              
            <div class="col-md-6">
				<label for="end_date"><?php echo  lang('end_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('end_date');?>" name="end_date" type="text" value="<?= date('d-m-Y', strtotime($travel->end_date)) ?>" required="required" autocomplete="off">
			</div> 
            <div class="col-md-6"> 
                <label for="visit_place"><?php echo  lang('visit_place') ?> <span class="text-danger">*</span></label>
                <input type="text" name="visit_place" value="<?= $travel->visit_place ?>" class="form-control" required="required">
            </div>  
	   </div>
       <div class="form-group row">
              
            <div class="col-md-6">
				<label for="visit_purpose"><?php echo  lang('visit_purpose') ?> <span 	class="text-danger">*</span></label>
				<input type="text" name="visit_purpose" value="<?= $travel->visit_purpose ?>" class="form-control" required="required">
			</div> 
            <div class="col-md-6"> 
                <label for="expected_budget"><?php echo  lang('expected_budget') ?> <span class="text-danger"></span></label>
                <input type="text" name="expected_budget" value="<?= $travel->expected_budget ?>" class="form-control">
            </div>  
	   </div>
       <div class="form-group row">
            
            <div class="col-md-6"> 
                <label for="actual_budget"><?php echo  lang('actual_budget') ?> <span class="text-danger"></span></label>
                <input type="text" name="actual_budget" value="<?= $travel->actual_budget ?>" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="travel_mode"><?php echo  lang('travel_mode') ?> <span class="text-danger"></span></label>
                <?php
                    $travel_mode = array(
                        1 => "By Bus", 
                        2 => "By Train", 
                        3 => "By Plane", 
                        4 => "By Taxi", 
                        5 => "By Rental Car", 
                    );
                ?>
                <select name="travel_mode" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($travel_mode as $key=>$value) { ?>
                            <option value="<?php echo $key; ?>" <?= $travel->travel_mode == $key ? 'Selected' : '' ?>><?php echo $value;?></option>
                        <?php } ?>
                </select>
	        </div> 
        </div>
       <div class="form-group row"> 
            
            <div class="col-md-6">
                <label for="arrangement_type"><?php echo  lang('arrangement_type') ?> <span class="text-danger"></span></label>
                <?php
                    $arrangement_type = array(
                        1 => "Corporation", 
                        2 => "Guest House", 
                    );
                ?>
                <select name="arrangement_type" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($arrangement_type as $key=>$value) { ?>
                            <option value="<?php echo $key; ?>" <?= $travel->arrangement_type == $key ? 'Selected' : '' ?>><?php echo $value;?></option>
                        <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class=""><?= $travel->description ?></textarea>
			</div>            
       </div>
       
	   <div class="form-group row"> 
            <div class="col-md-6">
                <label for="status"><?php echo  lang('status') ?> <span class="text-danger"></span></label>
            <select name="status" class="form-control select" id="">
                    <option value=""><?php echo 'Select';?></option>
                    <option value="0" <?= $travel->status == 0 ? 'Selected' : '' ?>> <?= lang('pending') ?> </option>
                    <option value="1" <?= $travel->status == 1 ? 'Selected' : '' ?>> <?= lang('approved') ?> </option>
                    <option value="2" <?= $travel->status == 1 ? 'Selected' : '' ?>> <?= lang('rejected') ?> </option>
            </select>
        </div>  			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('edit_travel', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

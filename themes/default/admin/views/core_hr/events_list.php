<div class="panel">
    <div class="panel-body">

    <?php
    echo "<div class=\"\"><a href='" . admin_url('core_hr/add_event_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_event') . "'><i class=\"fa fa-plus\"></i> Add Event</a></div>";
    ?>
    <h2>Events:</h2>
    <div class="table-responsive">
    <table id="table_events" class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
            <th><?php echo lang('the_number_sign');?></th>
            <th><?php echo lang('employee');?></th>
            <th><?php echo lang('event_title');?></th>
            <th><?php echo lang('event_date');?></th>
            <th><?php echo lang('event_time');?></th>
            <th style="width:100px;"><?= lang('actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
</div>

<script>
   $(document).ready(function () {
    // $(document).on('click', '#working_experience', function(){
      oTable = $('#table_events').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('core_hr/getEvents') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    // });

   });

</script>
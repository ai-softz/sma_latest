<div class="panel">
    <div class="panel-body">

    <?php
    echo "<div class=\"\"><a href='" . admin_url('core_hr/add_meeting_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_meetings') . "'><i class=\"fa fa-plus\"></i> Add Meeting</a></div>";
    ?>
    <h2>Meetings:</h2>
    <div class="table-responsive">
    <table id="table_meetings" class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
            <th><?php echo lang('the_number_sign');?></th>
            <th><?php echo lang('employee');?></th>
            <th><?php echo lang('meeting_title');?></th>
            <th><?php echo lang('meeting_date');?></th>
            <th><?php echo lang('meeting_time');?></th>
            <th><?php echo lang('meeting_room');?></th>
            <th style="width:100px;"><?= lang('actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
    </div>
</div>

<script>
   $(document).ready(function () {
    // $(document).on('click', '#working_experience', function(){
      oTable = $('#table_meetings').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('core_hr/getMeetings') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });


   });

</script>
<span>
<?php
$add = "<a href='" . admin_url('core_hr/add_trainer_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_trainer') . "' style='color: #fff'><i class=\"fa fa-file-plus\"></i>" . lang('Add Trainer') ."</a>";
echo $add;
?>
 <a href="<?= admin_url('core_hr/trainings') ?>" class="btn btn-primary" style="float: right">Training List</a>
</span>
<br>
<h2> Trainers: </h2>

<br>
<div class="table-responsive">
 <table id="table_trainers" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('name');?></th>
          <th><?php echo lang('contact');?></th>
          <th><?php echo lang('email');?></th>
          <th><?php echo lang('expertise');?></th>
          <th><?php echo lang('address');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {
    
      oTable = $('#table_trainers').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('core_hr/getTrainers') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

   

</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_award'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
		  
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Employee</th><td><?= $emp_name->first_name .' '.$emp_name->last_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Award Type</th><td><?= $award_type ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Date</th><td><?= date('d M, Y', strtotime($awards->created_at));  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Month Year</th><td><?= date('d M, Y', strtotime($awards->award_month_year)); ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Gift</th><td><?= $awards->gift_item; ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Cash</th><td><?= $awards->cash_price; ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Award Photo</th>
                              <td> 
                                  <img src="<?= base_url('assets/uploads/awards/'.$awards->award_photo); ?>" width="30%">
                                  <a href="<?php echo base_url('assets/uploads/awards/'.$awards->award_photo); ?>" target="_blank"> <?= 'Download' ?> </a> 
                              </td>
	                      </tr>
	                      <tr>
	                      	<th>Award Information</th><td><?= $awards->award_information ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_award'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
		  
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Employee</th><td><?= $emp_name->first_name .' '.$emp_name->last_name ?></td>
	                      </tr>
	                      
	                      <tr>
	                      	<th>Transfer Date</th><td><?= date('d M, Y', strtotime($transfer->transfer_date));  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Transfer To (Department)</th><td><?= $dept_name; ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Status</th>
							  	<td>
								  <?php
								  
								  if($transfer->status == 0) {
									  echo lang('pending');
								  } 
								  if($transfer->status == 1) {
									  echo lang('approved');
								  } 
								  if($transfer->status == 2) {
									  echo lang('rejected');
								  } 
								  ?>
								</td>
	                      </tr>
	                      
	                      <tr>
	                      	<th> Information</th><td><?= $transfer->description ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


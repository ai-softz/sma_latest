<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_travel'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
		  
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Employee</th><td><?= $emp_name->first_name .' '.$emp_name->last_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Start Date</th><td><?= date('d M, Y', strtotime($travel->start_date));  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>End Date</th><td><?= date('d M, Y', strtotime($travel->end_date));  ?></td>
	                      </tr>
                          <tr>
	                      	<th> Purpose of Visit</th><td><?= $travel->visit_purpose ?></td>
	                      </tr>
                          <tr>
	                      	<th> Visit Place</th><td><?= $travel->visit_place ?></td>
	                      </tr>
                          <tr>
	                      	<th> Travel Mode</th>
                                <td>
                                  <?php 
                                  if($travel->travel_mode == 1) {
                                      echo "By Bus";
                                  } 
                                  if($travel->travel_mode == 2) {
                                      echo "By Train";
                                  } 
                                  if($travel->travel_mode == 3) {
                                      echo "By Plane";
                                  } 
                                  if($travel->travel_mode == 4) {
                                      echo "By Taxi";
                                  } 
                                  if($travel->travel_mode == 5) {
                                      echo "By Rental Car";
                                  } 
                                  ?>
                                </td>
	                      </tr>
                          <tr>
	                      	<th> Arrangement Type</th>
                              <td>
                                  <?php 
                                    if($travel->arrangement_type == 1) {
                                        echo "Corporation";
                                    } 
                                    if($travel->arrangement_type == 2) {
                                        echo "Guest House";
                                    } 
                                  ?>
                            </td>
	                      </tr>
                          <tr>
	                      	<th> Expected Budget</th><td><?= $travel->expected_budget ?></td>
	                      </tr>
                          <tr>
	                      	<th> Actual Budget</th><td><?= $travel->actual_budget ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Status</th>
							  	<td>
								  <?php
								  if($travel->status == 0) {
									  echo lang('pending');
								  } 
								  if($travel->status == 1) {
									  echo lang('approved');
								  } 
								  if($travel->status == 2) {
									  echo lang('rejected');
								  } 
								  ?>
								</td>
	                      </tr>
	                      <tr>
	                      	<th> Description</th><td><?= $travel->description ?></td>
	                      </tr>
	                      
	                    </tbody>
	              	  </table>
	                </div>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


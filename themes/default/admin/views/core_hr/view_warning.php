<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_award'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
		  
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Warning To</th><td><?= $warning_to->first_name .' '.$warning_to->last_name ?></td>
	                      </tr>
                          <tr>
	                      	<th>Warning Subject</th><td><?= $warning->subject; ?></td>
	                      </tr>
                          <tr>
	                      	<th>Warning By</th>
                              <td>
                                <?= $warning_by->first_name .' '.$warning_by->last_name ?>
                              </td>
	                      </tr>
                          
	                      <tr>
	                      	<th>Date</th><td><?= date('d M, Y', strtotime($warning->warning_date));  ?></td>
	                      </tr>
	                      
	                      <tr>
	                      	<th>Attachment</th>
                              <td> 
                                  <img src="<?= base_url('assets/uploads/warnings/'.$warning->attachment); ?>" width="30%">
                                  <a href="<?php echo base_url('assets/uploads/warnings/'.$warning->attachment); ?>" target="_blank"> <?= 'Download' ?> </a> 
                              </td>
	                      </tr>
                          <tr>
	                      	<th>Status</th>
							  	<td>
								  <?php
								  if($warning->status == 0) {
									  echo lang('pending');
								  } 
								  if($warning->status == 1) {
									  echo lang('approved');
								  } 
								  if($warning->status == 2) {
									  echo lang('rejected');
								  } 
								  ?>
								</td>
	                      </tr>
	                      <tr>
	                      	<th>Warning Information</th><td><?= $warning->description ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


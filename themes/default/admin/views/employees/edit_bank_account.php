<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_work_experience'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
		  <h3><b>Add New</b> Work Experience</h3>
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_emp_bank_account']; ?>
		     <?php echo admin_form_open_multipart("employees/edit_emp_bank_account", $attrib) ?>
		     <input type="hidden" name="bankaccount_id" value="<?= $bank_account_info->bankaccount_id ?>">
		    <div class="form-group row">
		      <div class="col-md-6">
		         <label for="account_title"><?php echo  lang('account_title') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('account_title');?>" name="account_title" id="account_title" type="text" value="<?= $bank_account_info->account_title ?>" required="required">
		      </div>
		      <div class="col-md-6">
		         <label for="account_number"><?php echo  lang('account_number') ?> <span class="text-danger">*</span></label>
		         <input class="form-control" placeholder="<?php echo lang('account_number');?>" name="account_number" type="text" value="<?= $bank_account_info->account_number ?>" required="required">
		      </div>
		   </div>

		   <div class="form-group row">
		     <div class="col-md-6">
		         <label for="bank_name"><?php echo  lang('bank_name') ?> <span class="text-danger">*</span></label>
		         <input class="form-control" placeholder="<?php echo lang('bank_name');?>" name="bank_name" type="text" value="<?= $bank_account_info->bank_name ?>" required="required">
		    </div>  
		    <div class="col-md-6">
		         <label for="bank_code"><?php echo  lang('bank_code') ?> <span class="text-danger">*</span></label>
		         <input class="form-control" placeholder="<?php echo lang('bank_code');?>" name="bank_code" type="text" value="<?= $bank_account_info->bank_code ?>" required="required">
		    </div>   
		   </div>
		   <div class="form-group row">
		      <div class="col-md-6">
		         <label for="bank_branch"><?php echo  lang('bank_branch') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('bank_branch');?>" name="bank_branch" type="text" value="<?= $bank_account_info->bank_branch ?>">
		    </div>  
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('edit_emp_bank_account', lang('save'), 'class="btn btn-primary"'); ?>
		         </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


 $('#edit_emp_bank_account').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/EditEmpBankaccount",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
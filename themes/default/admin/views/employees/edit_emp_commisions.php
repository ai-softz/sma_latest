<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_employee_commissions'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

<div class="row">
 
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_employee_commissions']; ?>
     <?php echo admin_form_open_multipart("employees/edit_employee_commissions", $attrib) ?>
     <input type="hidden" name="salary_commissions_id" value="<?= $emp_commissions->salary_commissions_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="is_commission_taxable"><?php echo  lang('is_commission_taxable') ?> <span class="text-danger"></span></label>
         <select name="is_commission_taxable" class="form-control select">
              
              <option value="0" <?= $emp_commissions->is_commission_taxable == 0 ? "Selected" : "" ?>><?php echo lang('not_taxable');?></option>
               <option value="1" <?= $emp_commissions->is_commission_taxable == 1 ? "Selected" : "" ?>><?php echo lang('fully_taxable');?></option>
               <option value="2" <?= $emp_commissions->is_commission_taxable == 2 ? "Selected" : "" ?>><?php echo lang('partially_taxable');?></option>
              
        </select>
      </div>
      <div class="col-md-6">
         <label for="amount_option"><?php echo  lang('amount_option') ?> <span class="text-danger"></span></label>
        <select name="amount_option" class="form-control select" id="wages_type">
               
                 <option value="0" <?= $emp_commissions->amount_option == 0 ? "Selected" : "" ?>><?php echo lang('fixed');?></option>
                 <option value="1" <?= $emp_commissions->amount_option == 1 ? "Selected" : "" ?>><?php echo lang('percentage');?></option>
                
        </select>
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="commission_title"><?php echo  lang('commission_title') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('commission_title');?>" name="commission_title" type="text" value="<?= $emp_commissions->commission_title ?>" required="required">
    </div>  
    <div class="col-md-6">
         <label for="commission_amount"><?php echo  lang('commission_amount') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('commission_amount');?>" name="commission_amount" type="text" value="<?= $emp_commissions->commission_amount ?>" required="required">
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('edit_employee_commissions', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_employee_commissions').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "employees/editEmpCommission",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
              $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
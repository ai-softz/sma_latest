<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_employee_document'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">

  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_employee_document']; ?>
     <?php echo admin_form_open_multipart("employees/edit_employee_document", $attrib) ?>
     <input type="hidden" name="document_id" value="<?= $employee_document->document_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="document_type_id"><?php echo  lang('document_type_id') ?> <span class="text-danger"></span></label>
         <select name="document_type_id" id="language" class="form-control select">
              <option value=""><?php echo 'Select';?></option>
              <?php foreach($document_types as $row) { ?>
                <option value="<?php echo $row->document_type_id; ?>" <?= $row->document_type_id == $employee_document->document_type_id ? 'Selected' : ''; ?> ><?php echo $this->site->getCaption($row->document_type, $row->secondary_document_type);?></option>
              <?php } ?>
           </select>
      </div>
      <div class="col-md-6">
         <label for="title"><?php echo  lang('document_title') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('title');?>" name="title" type="text" value="<?= $employee_document->title ?>" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="date_of_expiry"><?php echo  lang('expiry_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('date_of_expiry');?>" name="date_of_expiry" type="text" value="<?= $employee_document->date_of_expiry ?>" required="required" autocomplete="off">
    </div>  
    <div class="col-md-6">
         <label for="document_file"><?php echo  lang('document_file') ?> <span class="text-danger">*</span></label>
        
       <input type="file" name="document_file" id="document_file" size="20" />
       <p><small>Upload files only: png, jpg, jpeg, gif, txt, pdf, xls, xlsx, doc, docx</small></p>
       <a href="<?= base_url('assets/uploads/documents/'.$employee_document->document_file) ?>" target="_blank"> <?= $employee_document->document_file ?> </a>
    </div>   
   </div>
   <div class="form-group row">
      <div class="col-md-12">
        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
        <textarea name="description" class="form-control" id="description"></textarea>
      </div>
   </div>
   <div class="form-group">
        <?php echo form_submit('edit_employee_document', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_employee_document').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/editEmployeeDocument",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('#myModal').modal('hide');
                // $('#table_employee_document').DataTable().ajax.reload();
                location.reload();
             }
               
      });  
  });

});
</script>
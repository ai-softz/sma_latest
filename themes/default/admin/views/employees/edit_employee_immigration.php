<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_employee_immigration'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>
            <div class="row">
   
    <div class="col-md-12">
      <?php $attrib = [ 'role' => 'form', 'id' => 'edit_employee_immigration']; ?>
       <?php echo admin_form_open_multipart("employees/edit_employee_immigration", $attrib) ?>
     <input type="hidden" name="immigration_id" value="<?= $employee_immigration->immigration_id ?>">
        <div class="form-group row">
          <div class="col-md-6">
             <label for="document_type_id"><?php echo  lang('document_type_id') ?> <span class="text-danger"></span></label>
             <select name="document_type_id" class="form-control select">
                  <option value=""><?php echo 'Select';?></option>
                  <?php foreach($document_types as $row) { ?>
                    <option value="<?php echo $row->document_type_id; ?>" <?= $row->document_type_id == $employee_immigration->document_type_id ? "Selected" : "" ?>><?php echo $this->site->getCaption($row->document_type, $row->secondary_document_type);?></option>
                  <?php } ?>
               </select>
          </div>
          <div class="col-md-6">
             <label for="document_number"><?php echo  lang('document_number') ?> <span class="text-danger">*</span></label>
             <input class="form-control" placeholder="<?php echo lang('document_number');?>" name="document_number" type="text" value="<?= $employee_immigration->document_number ?>" required="required">
          </div>
       </div>

       <div class="form-group row">
        <div class="col-md-6">
             <label for="issue_date"><?php echo  lang('issue_date') ?> <span class="text-danger">*</span></label>
             <input class="form-control date" placeholder="<?php echo lang('issue_date');?>" name="issue_date" type="text" value="<?= $employee_immigration->issue_date ?>" required="required" autocomplete="off">
        </div> 
         <div class="col-md-6">
             <label for="expiry_date"><?php echo  lang('expiry_date') ?> <span class="text-danger">*</span></label>
             <input class="form-control date" placeholder="<?php echo lang('expiry_date');?>" name="expiry_date" type="text" value="<?= $employee_immigration->expiry_date ?>" required="required" autocomplete="off">
        </div>  
      
       </div>
       <div class="form-group row">
          <div class="col-md-6">
               <label for="document_file"><?php echo  lang('document_file') ?> <span class="text-danger">*</span></label>
              
             <input type="file" name="document_file" id="document_file" size="20" />
             <p><small>Upload files only: png, jpg, jpeg, gif, txt, pdf, xls, xlsx, doc, docx</small></p>
             <a href="<?= base_url('assets/uploads/documents/'.$employee_immigration->document_file) ?>" target="_blank"> <?= $employee_immigration->document_file ?> </a>
          </div>  
          
       </div>
        <div class="form-group row">
           <div class="col-md-6">
             <label for="eligible_review_date"><?php echo  lang('eligible_review_date') ?> <span class="text-danger">*</span></label>
             <input class="form-control date" placeholder="<?php echo lang('eligible_review_date');?>" name="eligible_review_date" type="text" value="<?= $employee_immigration->eligible_review_date ?>" required="required" autocomplete="off">
          </div>  
          <div class="col-md-6">
            <label for="country"><?php echo lang('country') ?>   </label>
             <select name="country" class="form-control select";?>">
             <option value=""><?php echo 'Select';?></option>
             <?php foreach($all_countries as $row) { ?>
                <option value="<?php echo $row->country_id; ?>" <?= $employee_immigration->country_id == $row->country_id ? "Selected" : "" ?>><?php echo $row->country_name; ?></option>
            <?php } ?> 
            </select>
          </div>
        </div>
       <div class="form-group">
            <?php echo form_submit('add_employee_immigration', lang('save'), 'class="btn btn-primary"'); ?>
             </div>
        <?php echo form_close() ?>
      </div>
  
</div>

	     </div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_employee_immigration').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/editEmployeeImmigration",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
              $('#myModal').modal('hide');
				      location.reload();
             }
               
      });  
  });

});
</script>
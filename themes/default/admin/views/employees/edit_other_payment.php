<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_other_payment'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

<div class="row">
  <h3><b>Add New</b> Other Payments</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_other_payment']; ?>
     <?php echo admin_form_open_multipart("employees/edit_other_payment", $attrib) ?>
     <input type="hidden" name="other_payments_id" value="<?= $other_payment->other_payments_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="is_otherpayment_taxable"><?php echo  lang('otherpayment_option') ?> <span class="text-danger"></span></label>
         <select name="is_otherpayment_taxable" class="form-control select">
              
              <option value="0" <?= $other_payment->is_otherpayment_taxable == 0 ? "Selected" : "" ?>><?php echo lang('not_taxable');?></option>
               <option value="1" <?= $other_payment->is_otherpayment_taxable == 1 ? "Selected" : "" ?>><?php echo lang('fully_taxable');?></option>
               <option value="2" <?= $other_payment->is_otherpayment_taxable == 2 ? "Selected" : "" ?>><?php echo lang('partially_taxable');?></option>
              
        </select>
      </div>
      <div class="col-md-6">
         <label for="amount_option"><?php echo  lang('amount_option') ?> <span class="text-danger"></span></label>
        <select name="amount_option" class="form-control select" id="wages_type">
                
                 <option value="0" <?= $other_payment->amount_option == 0 ? "Selected" : "" ?>><?php echo lang('fixed');?></option>
                 <option value="1" <?= $other_payment->amount_option == 0 ? "Selected" : "" ?>><?php echo lang('percentage');?></option>
                
        </select>
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="payments_title"><?php echo  lang('payments_title') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('payments_title');?>" name="payments_title" type="text" value="<?= $other_payment->payments_title ?>" required="required">
    </div>  
    <div class="col-md-6">
         <label for="payments_amount"><?php echo  lang('payments_amount') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('payments_amount');?>" name="payments_amount" type="text" value="<?= $other_payment->payments_amount ?>" required="required">
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('edit_other_payment', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_other_payment').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "employees/editOtherPayment",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
              $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
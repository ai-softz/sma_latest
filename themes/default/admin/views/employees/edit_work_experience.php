<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_work_experience'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_work_experience']; ?>
		     <?php echo admin_form_open_multipart("employees/editWorkExperience", $attrib) ?>
		     <input type="hidden" name="work_experience_id" value="<?= $work_experience->work_experience_id ?>">
		    <div class="form-group row">
		      <div class="col-md-6">
		         <label for="company_name"><?php echo  lang('company_name') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('company_name');?>" name="company_name" id="company_name" type="text" value="<?= $work_experience->company_name ?>" required="required">
		      </div>
		      <div class="col-md-6">
		         <label for="post"><?php echo  lang('post') ?> <span class="text-danger">*</span></label>
		         <input class="form-control" placeholder="<?php echo lang('post');?>" name="post" type="text" value="<?= $work_experience->post ?>" required="required">
		      </div>
		   </div>

		   <div class="form-group row">
		     <div class="col-md-6">
		         <label for="from_date"><?php echo  lang('from_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('from_date');?>" name="from_date" type="text" value="<?= date('d-m-Y', strtotime($work_experience->from_date)) ?>" required="required">
		    </div>  
		    <div class="col-md-6">
		         <label for="to_date"><?php echo  lang('to_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('to_date');?>" name="to_date" type="text" value="<?= date('d-m-Y', strtotime($work_experience->to_date)) ?>" required="required">
		    </div>   
		   </div>
		   <div class="form-group row">
		      <div class="col-md-12">
		        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
		        <textarea name="description" class="form-control" id="description"><?= $work_experience->description ?></textarea>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('edit_working_experience', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_work_experience').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "employees/editWorkExperience",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				$('#myModal').modal('hide');
				location.reload();
             }
               
      });  
  });

});
</script>
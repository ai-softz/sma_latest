
<style>
	.success {
		background-color: #00ff00;
		width: fit-content;
	}
	.danger {
		background-color: #ffb9b9;
		width: fit-content;
	}
</style>

<p id="error_msg"></p>

<div class="row">
  <h3><b>Change Password</b></h3>
  <div class="col-md-12">
	    <?php $attrib = [ 'role' => 'form', 'id' => 'update_employee_password']; ?>
	     <?php echo admin_form_open_multipart("employees/update_employee_password", $attrib) ?>
	     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
	    <div class="form-group row">
	      <div class="col-md-4">
	         <label for="old_password"><?php echo  lang('old_password') ?> <span class="text-danger"></span></label>
	         <input class="form-control" placeholder="<?php echo lang('old_password');?>" name="old_password" id="old_password" type="password" value="" required="required">
	      </div>
	      <div class="col-md-4">
	         <label for="new_password"><?php echo  lang('new_password') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('new_password');?>" name="new_password" type="password" value="" required="required">
	      </div>
	      <div class="col-md-4">
	         <label for="new_password_confirm"><?php echo  lang('new_password_confirm') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('new_password_confirm');?>" name="new_password_confirm" type="password" value="" required="required">
	      </div>
	   </div>

	   <div class="form-group">
        <?php echo form_submit('update_employee_password', lang('save'), 'class="btn btn-primary"'); ?>
	    </div>
	    <?php echo form_close() ?>

	</div>
</div>
<script>
   $(document).ready(function () {
  $('#update_employee_password').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/update_employee_password",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               // $('#table_working_experience').DataTable().ajax.reload();
               if(data.error == 0) {
               		$('#error_msg').addClass('success');
               }
               if(data.error == 1) {
               		$('#error_msg').addClass('danger');
               }
               $('#error_msg').text(data.msg);
             }
               
      });  
  });

  });
</script>
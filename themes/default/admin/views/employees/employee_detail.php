<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="box-content">  
<div class="panel">
   <div class="panel-body" style="background-color: #f5f5f5;">
      <h1 style="font-size: 20px">Employee Details</h1> <br>
      <!-- <ul class="nav nav-pills" id="employee_tabs_top"> -->
         <!-- <li class="nav-item active">
            <a class="nav-link" data-toggle="tab" aria-current="page" href="#smartwizard-2-step-1"> 
              <span class="fa fa-users"></span> General<p><small>Basic information</small></p></a>
         </li> -->
         <!-- <li class="nav-item">
            <a class="nav-link" data-toggle="tab" aria-current="page" href="#smartwizard-2-step-2"> 
              <span class="fa fa-edit"></span> Set Salary<p><small>Set up Set Salary</small></p></a>
         </li>
         <li class="nav-item">
            <a class="nav-link" data-toggle="tab" aria-current="page" href="#smartwizard-2-step-3" id="employee_leaves"> 
              <span class="fa fa-newspaper-o"></span> Leaves<p><small>View All Leaves</small></p></a>
         </li>
        
         <li class="nav-item">
            <a class="nav-link" data-toggle="tab" aria-current="page" href="#smartwizard-2-step-5" id="employee_payslips"> 
              <span class="fa fa-sticky-note-o"></span> Payslips<p><small>View All Payslips</small></p></a>
         </li> -->
      <!-- </ul> -->
      <div class="container" id="smartwizard_tabs" style="margin-top: 10px;background-color: #fff;padding: 10px;">
         <div class="tab-content">
            <div class="tab-pane active" id="smartwizard-2-step-1">
                  <div class="row">
                     <div class="col-md-3">
                        <ul class="list-group">
                           <a href="#account-basic_info" data-toggle="tab" class="list-group-item list-group-item-action">Basic Information</a>
                           <a href="#account-profile_picture" data-toggle="tab" class="list-group-item list-group-item-action">Profile Picture</a>
                           <a href="#account-work_experience" data-toggle="tab" class="list-group-item list-group-item-action" id="working_experience">
                           Working Experience
                           </a>
                           <a href="#account-qualification" data-toggle="tab" class="list-group-item list-group-item-action" id="employee_qualification">Qualification</a>
                           <a href="#account-contacts" data-toggle="tab" class="list-group-item list-group-item-action" id="emergency_contact">Emergency Contact</a>
                            <a href="#account-bank_account" data-toggle="tab" class="list-group-item list-group-item-action" id="emp_bank_account">Bank Account</a>
                            <a href="#account-document_type" data-toggle="tab" class="list-group-item list-group-item-action" id="document_type">Document Type</a>
                            <a href="#account-employee_document" data-toggle="tab" class="list-group-item list-group-item-action" id="employee_document">Employee Document</a>
                           <a href="#account-immigration" data-toggle="tab" class="list-group-item list-group-item-action" id="immigration">Immigration</a>
                           
                           <a href="#account-social_network" data-toggle="tab" class="list-group-item list-group-item-action" id="social_network">Social Network</a>
                           
                           <a href="#account-change_password" data-toggle="tab" class="list-group-item list-group-item-action">Change Password</a>
                           <a href="#account-emp_contract" data-toggle="tab" class="list-group-item list-group-item-action" id="emp_contract">Contract</a>
                          
                        </ul>
                     </div>
                     <div class="col-md-9">
                        <div class="tab-content ">
                           <div class="tab-pane active" id="account-basic_info">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_basic_info', $this->data); 
                                 ?>
                           </div> <!-- /#account-basic_info -->
                           <div class="tab-pane" id="account-profile_picture">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_profile_picture', $this->data); 
                              ?>
                           </div> <!-- /#account-basic_info -->
                           <div class="tab-pane" id="account-work_experience">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_work_experience', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-contacts">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_emergency_contact', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-qualification">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->data['education_levels'] = $education_levels;
                                 $this->data['qualification_languages'] = $qualification_languages;
                                 $this->data['qualification_skill'] = $qualification_skill;
                                 $this->load->view($this->theme . 'employees/employee_qualification', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-bank_account">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_bank_account', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-document_type">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->data['document_types'] = $document_types;
                                 $this->load->view($this->theme . 'employees/document_type', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-employee_document">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->data['document_types'] = $document_types;
                                 $this->load->view($this->theme . 'employees/employee_document', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-immigration">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->data['document_types'] = $document_types;
                                 $this->load->view($this->theme . 'employees/employee_immigration', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-social_network">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_social_network', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-change_password">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_change_password', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-emp_contract">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->data['contract_types'] = $contract_types;
                                 $this->data['all_designations'] = $all_designations;
                                 $this->load->view($this->theme . 'employees/employee_contract', $this->data); 
                              ?>
                           </div> 
                           <div class="tab-pane" id="account-immigration">
                              <h3>Immigration area.</h3>
                           </div> 
                           <div class="tab-pane" id="3">
                              <h3>add clearfix to tab-content (see the css)</h3>
                           </div>
                        </div>
                     </div>
                  </div>
            </div> <!-- /#smartwizard-2-step-1 -->
            <div class="tab-pane" id="smartwizard-2-step-2">
               <div class="row">
                     <div class="col-md-3">
                        <ul class="list-group">
                           <a href="#account-update_salary" data-toggle="tab" class="list-group-item list-group-item-action">Update Salary</a>
                           <a href="#account-allowances" data-toggle="tab" class="list-group-item list-group-item-action" id="employee_allowances">Allowances</a>
                           <a href="#account-commissions" data-toggle="tab" class="list-group-item list-group-item-action" id="employee_commisions">Commissions</a>
                           <a href="#account-statutory_deductions" data-toggle="tab" class="list-group-item list-group-item-action" id="statutory_deductions">Statutory deductions</a>
                           <a href="#account-other_payment" data-toggle="tab" class="list-group-item list-group-item-action" id="other_payment">Other Payment</a>
                           <a href="#account-overtime" data-toggle="tab" class="list-group-item list-group-item-action" id="salary_overtime">Overtime</a>
                           <a href="#account-loan" data-toggle="tab" class="list-group-item list-group-item-action" id="employee_loan">Loan</a>
                           
                        </ul>
                     </div>
                     <div class="col-md-9">
                        <div class="tab-content ">
                           <div class="tab-pane active" id="account-update_salary">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_update_salary', $this->data); 
                                 ?>
                           </div>
                           <div class="tab-pane" id="account-allowances">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_allowances', $this->data); 
                              ?>
                           </div>
                           <div class="tab-pane" id="account-commissions">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_commissions', $this->data); 
                              ?>
                           </div>
                           <div class="tab-pane" id="account-statutory_deductions">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_statutory_deductions', $this->data); 
                              ?>
                           </div>
                           <div class="tab-pane" id="account-other_payment">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_other_payment', $this->data); 
                              ?>
                           </div>
                           <div class="tab-pane" id="account-overtime">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_salary_overtime', $this->data); 
                              ?>
                           </div>
                           <div class="tab-pane" id="account-loan">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 $this->load->view($this->theme . 'employees/employee_loan', $this->data); 
                              ?>
                           </div>
                           <div class="tab-pane" id="account-allowances">
                              <?php 
                                 echo "Allowances"; 
                                 ?>
                           </div>
                           
                        </div>
                     </div>
                  </div>
            </div>
            <div class="tab-pane" id="smartwizard-2-step-3">
               <div class="row">
                  <div class="col-md-12">
                  <?php 
                  //print_r($leave_statistics);die;
                     $this->data['emp_info'] = $emp_info;
                     $this->data['leave_statistics'] = $leave_statistics;
                     $this->load->view($this->theme . 'employees/employee_leaves', $this->data); 
                  ?>
                  </div>
               </div>
            </div>
            <div class="tab-pane" id="smartwizard-2-step-4">
               <div class="row">
                  <div class="col-md-3">
                     <ul class="list-group">
                           <a href="#account-update_salary" data-toggle="tab" class="list-group-item list-group-item-action">Update Salary</a>
                           <a href="#account-allowances" data-toggle="tab" class="list-group-item list-group-item-action" id="employee_allowances">Allowances</a>
                           
                           
                     </ul>
                  </div>
                  <div class="col-md-9">
                     <div class="tab-content ">
                           <div class="tab-pane active" id="account-update_salary">
                              <?php 
                                 $this->data['emp_info'] = $emp_info;
                                 //$this->load->view($this->theme . 'employees/', $this->data); 
                                 ?>
                                 </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane" id="smartwizard-2-step-5">
               <div class="row">
                  <div class="col-md-12">
                  <?php 
                  //print_r($leave_statistics);die;
                     $this->data['emp_info'] = $emp_info;
                     $this->load->view($this->theme . 'employees/employee_payslips', $this->data); 
                  ?>
                  </div>
               </div>
            </div>
         </div> <!-- /.tab-content #smartwizard_tabs -->
      </div> <!-- /.container #smartwizard_tabs -->
      
   </div> <!-- /.panel-body -->
</div> <!-- /.panel -->

<style>
  #employee_tabs_top a{
    font-size: 16px;
  }
  #employee_tabs_top ul li {
    margin-right: 10px;
  }
  </style>
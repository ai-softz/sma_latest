

<hr></hr>
<div class="container"><h2>Example tab 2 (using standard nav-tabs)</h2></div>

<div id="exTab2" class="container">	
	<ul class="nav nav-tabs">
			<li class="active">
        	<a  href="#1" data-toggle="tab">Overview</a>
			</li>
			<li><a href="#2" data-toggle="tab">Without clearfix</a>
			</li>
			<li><a href="#3" data-toggle="tab">Solution</a>
			</li>
	</ul>

	<div class="tab-content ">
	  	<div class="tab-pane active" id="1">
  			<h3>Standard tab panel created on bootstrap using nav-tabs</h3>
		</div>
		<div class="tab-pane" id="2">
  			<h3>Notice the gap between the content and tab after applying a background color</h3>
		</div>
		<div class="tab-pane" id="3">
  			<h3>add clearfix to tab-content (see the css)</h3>
		</div>
	</div>
 </div>

<hr></hr>




<!-- Bootstrap core JavaScript
    ================================================== -->


<div class="mb-3 sw-container tab-content">
      <div id="smartwizard-2-step-1" class="card animated fadeIn tab-pane step-content mt-3" style="display: block;">
        <div class="cards-body">
          <div class="card overflow-hidden">
            <div class="row no-gutters row-bordered row-border-light">
              <div class="col-md-3 pt-0">
                <div class="list-group list-group-flush account-settings-links"> <a class="list-group-item list-group-item-action active" data-toggle="list" href="#account-basic_info"> <i class="lnr lnr-user text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_basic');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-profile_picture"> <i class="lnr lnr-picture text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_profile_picture');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-immigration"> <i class="lnr lnr-rocket text-lightest"></i> &nbsp; <?php echo lang('xin_employee_immigration');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-contacts"> <i class="lnr lnr-phone-handset text-lightest"></i> &nbsp; <?php echo lang('xin_employee_emergency_contacts');?></a> <a class="list-group-item  list-group-item-action" data-toggle="list" href="#account-social"> <i class="lnr lnr-earth text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_social');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-document"> <i class="lnr lnr-file-add text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_document');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-qualification"> <i class="lnr lnr-file-empty text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_qualification');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-experience"> <i class="lnr lnr-hourglass text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_w_experience');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-baccount"> <i class="lnr lnr-apartment text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_baccount');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-cpassword"> <i class="lnr lnr-lock text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_cpassword');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-security_level"> <i class="lnr lnr-link text-lightest"></i> &nbsp; <?php echo lang('xin_esecurity_level_title');?></a> <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-contract"> <i class="lnr lnr-pencil text-lightest"></i> &nbsp; <?php echo lang('xin_e_details_contract');?></a> </div>
              </div>
              <div class="col-md-9">
              		<div class="tab-content">
                  		<div class="tab-pane fade show active" id="account-basic_info">
                    		<div class="card-body media align-items-center">
                      <div class="form-group row">          
	                    <div class="col-md-3">
	                    	<label for="customer_name"><?php echo  lang('first_name') ?> <span class="text-danger">*</span></label>
	                       <input class="form-control" placeholder="<?php echo lang('first_name');?>" name="first_name" id="first_name" type="text" value="" required="required">
	                    </div>
	                    <div class="col-md-3">
	                    	<label for="customer_name"><?php echo  lang('last_name') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('last_name');?>" name="last_name" type="text" value="" required="required">
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="customer_name"><?php echo  lang('employee_id') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('employee_id');?>" name="employee_id" type="text" value="<?php //echo $employee_id;?>" required="required">
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="customer_name"><?php echo  lang('employee_doj') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control date" placeholder="<?php echo lang('employee_doj');?>" name="date_of_joining" type="text" value="<?php echo date('d-m-Y');?>" required="required">
	           	 		</div>
               		</div>
                    		</div>
                		</div>
            		</div>
              </div> <!-- ./col-md-9 -->
            </div>

        </div>
    </div>
</div>
</div>

<h2>Work Experiences:</h2>
<div class="table-responsive">
 <table id="table_employee_document" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('document_type');?></th>
          <th><?php echo lang('document_title');?></th>
          <th><?php echo lang('expiry_date');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Document</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_employee_document']; ?>
     <?php echo admin_form_open_multipart("employees/add_employee_document", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="document_type_id"><?php echo  lang('document_type_id') ?> <span class="text-danger"></span></label>
         <select name="document_type_id" id="language" class="form-control select">
              <option value=""><?php echo 'Select';?></option>
              <?php foreach($document_types as $row) { ?>
                <option value="<?php echo $row->document_type_id; ?>">
                <?php //echo $this->site->getCaption($row->document_type, $row->secondary_document_type);
                echo $row->document_type;
                ?></option>
              <?php } ?>
           </select>
      </div>
      <div class="col-md-6">
         <label for="title"><?php echo  lang('document_title') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('title');?>" name="title" type="text" value="" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="date_of_expiry"><?php echo  lang('expiry_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('date_of_expiry');?>" name="date_of_expiry" type="text" value="" required="required" autocomplete="off">
    </div>  
    <div class="col-md-6">
         <label for="document_file"><?php echo  lang('document_file') ?> <span class="text-danger">*</span></label>
        
       <input type="file" name="document_file" id="document_file" size="20" />
       <p><small>Upload files only: png, jpg, jpeg, gif, txt, pdf, xls, xlsx, doc, docx</small></p>
    </div>   
   </div>
   <div class="form-group row">
      <div class="col-md-12">
        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
        <textarea name="description" class="form-control" id="description"></textarea>
      </div>
   </div>
   <div class="form-group">
        <?php echo form_submit('add_employee_document', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
    <?php echo form_close() ?>
  </div>
  
</div>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>

<script>
   $(document).ready(function () {
    $(document).on('click', '#employee_document', function(){
      oTable = $('#table_employee_document').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getEmployeeDocument/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_employee_document').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_employee_document",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               location.reload();
             }
               
      });  
  });

    // $(document).on('submit', '#add_working_experienc', function(){
      
    //   $.ajax({
    //     url: site.base_url + 'employees/add_working_experience',
    //     method: 'POST',
    //     data: new FormData(this),
    //     dataType: 'text',
    //     success: function(data) {
      
    //     }
    //   });
    // });

   });

</script>
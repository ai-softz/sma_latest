<div class="row">
<h3 style="margin-bottom: 30px"> Leave Statistics</h3>
<?php foreach($leave_statistics as $row) { ?>
    <div class="col-md-3">
        <p><label><?= $row['leave_name'] ?> (<?= $row['total_leave_taken'] ?>/<?= $row['total_leave_days'] ?>) </label></p>
        <?php $to = ($row['total_leave_taken'] / $row['total_leave_days']) * 100;  ?>
        <div class="progress">
        <div class="progress-bar" style="width: <?php echo $to; ?>%"></div>
        </div>
    </div>
<?php } ?>
</div>
<br><br>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
        <table id="table_emp_leaves" class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th><?php echo lang('the_number_sign');?></th>
                <th><?php echo lang('leave_type');?></th>
                <th><?php echo lang('department');?></th>
                <th><?php echo lang('request_duration');?></th>
                <th><?php echo lang('applied_on');?></th>
                <th style="width:100px;"><?= lang('actions'); ?></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
</div>
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#employee_leaves', function(){
      oTable = $('#table_emp_leaves').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('timesheets/getAllLeavesOfEmp/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });
});
</script>
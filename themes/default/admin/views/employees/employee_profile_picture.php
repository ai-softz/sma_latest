
<div class="row">
	 <div class="col-md-12">

	 	<?php $attrib = ['role' => 'form', 'id' => 'profile_pic_form']; ?>
    	 <?php echo admin_form_open_multipart("employees/add_employee", $attrib) ?>
    	 <label>Browse*</label>
    	 <input type="file" name="profile_pic" id="profile_pic" size="20" />
    	 <p><small>Upload only jpg, jpeg, png, gif format </small></p>
    	 <br>
    	 <div id="profile_picture_area">
    	 <?php //var_dump($emp_info->profile_picture) ;
    	 	if(!empty($emp_info->profile_picture)) { ?>
    	 		<img id="profile_picture" src="<?= base_url('assets/uploads/profile/'.$emp_info->profile_picture) ?>" style="width: 80px">
    	 	<?php } else {
    	  ?>
	    <img id="profile_picture" src="<?= base_url('assets/images/male.png') ?>" style="width: 80px">
		<?php } ?>
		</div>
	    <hr class="border-light m-0">
	    <input type="hidden" name="emp_id" value="<?php echo $emp_info->user_id ?>"> <br>
	    <?php echo form_submit('add_image', lang('save'), 'class="btn btn-primary"'); ?>
	    <?php echo form_close(); ?>

	 </div>
	 
</div>

<script>
$('#profile_pic_form').on('submit', function(e){  
           e.preventDefault();  
           
            var formdata = new FormData(this);
            console.log(formdata);

             $.ajax({  
                     url: site.base_url + "employees/add_profile_picture",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
                         //fetch_comments(); 
                         $('#profile_picture').remove();
                         $('#profile_picture_area').html(data);
                     }  
                });  
           
      });  

</script>
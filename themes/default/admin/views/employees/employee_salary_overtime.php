<h2>Other Payments:</h2>
<div class="table-responsive">
 <table id="table_salary_overtime" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('overtime_type');?></th>
          <th><?php echo lang('no_of_days');?></th>
          <th><?php echo lang('overtime_hours');?></th>
          <th><?php echo lang('overtime_rate');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Other Payments</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_salary_overtime']; ?>
     <?php echo admin_form_open_multipart("employees/add_salary_overtime", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">

   <div class="form-group row">
     <div class="col-md-6">
         <label for="overtime_type"><?php echo  lang('overtime_type') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('overtime_type');?>" name="overtime_type" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="no_of_days"><?php echo  lang('no_of_days') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('no_of_days');?>" name="no_of_days" type="text" value="" required="required">
    </div>   
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="overtime_hours"><?php echo  lang('overtime_hours') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('overtime_hours');?>" name="overtime_hours" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="overtime_rate"><?php echo  lang('overtime_rate') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('overtime_rate');?>" name="overtime_rate" type="text" value="" required="required">
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('add_salary_overtime', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#salary_overtime', function(){
      oTable = $('#table_salary_overtime').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getSalaryOvertime/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
              // $('td:eq(4)', nRow).html(amount_option(nRow, aData));
              // return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_salary_overtime').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_salary_overtime",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#table_salary_overtime').DataTable().ajax.reload();
             }
               
      });  
  });


   });

</script>
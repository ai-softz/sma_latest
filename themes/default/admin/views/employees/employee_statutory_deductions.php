<h2>Statutory Deduction:</h2>
<div class="table-responsive">
 <table id="table_statutory_deductions" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('title');?></th>
          <th><?php echo lang('amount');?></th>
          <th><?php echo lang('amount_option');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Statutory Deduction</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_statutory_deductions']; ?>
     <?php echo admin_form_open_multipart("employees/add_statutory_deductions", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    <div class="form-group row">
      
      <div class="col-md-6">
         <label for="statutory_options"><?php echo  lang('statutory_options') ?> <span class="text-danger"></span></label>
        <select name="statutory_options" class="form-control select" id="wages_type">
               
                 <option value="0"><?php echo lang('fixed');?></option>
                 <option value="1"><?php echo lang('percentage');?></option>
                
        </select>
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="deduction_title"><?php echo  lang('deduction_title') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('deduction_title');?>" name="deduction_title" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="deduction_amount"><?php echo  lang('deduction_amount') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('deduction_amount');?>" name="deduction_amount" type="text" value="" required="required">
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('add_statutory_deductions', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#statutory_deductions', function(){
      oTable = $('#table_statutory_deductions').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getStatutoryDeduction/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              $('td:eq(3)', nRow).html(amount_option(nRow, aData));
             
              //return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

    function amount_option(nRow, aData) {
       if(aData[3] == 0) {
          var returnData='Fixed';
       } else if(aData[3] == 1) {
          var returnData='Parcentage';
       } 
       return returnData;
    }

     $('#add_statutory_deductions').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_statutory_deductions",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#table_statutory_deductions').DataTable().ajax.reload();
             }
               
      });  
  });


   });

</script>
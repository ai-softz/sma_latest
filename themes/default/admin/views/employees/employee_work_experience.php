<h2>Work Experiences:</h2>
<div class="table-responsive">
 <table id="table_working_experience" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('company_name');?></th>
          <th><?php echo lang('from_date');?></th>
          <th><?php echo lang('to_date');?></th>
          <th><?php echo lang('post');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Work Experience</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_working_experience']; ?>
     <?php echo admin_form_open_multipart("employees/add_working_experience", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="company_name"><?php echo  lang('company_name') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('company_name');?>" name="company_name" id="company_name" type="text" value="" required="required">
      </div>
      <div class="col-md-6">
         <label for="post"><?php echo  lang('post') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('post');?>" name="post" type="text" value="" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="from_date"><?php echo  lang('from_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('from_date');?>" name="from_date" type="text" value="" required="required" autocomplete="off">
    </div>  
    <div class="col-md-6">
         <label for="to_date"><?php echo  lang('to_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('to_date');?>" name="to_date" type="text" value="" required="required" autocomplete="off">
    </div>   
   </div>
   <div class="form-group row">
      <div class="col-md-12">
        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
        <textarea name="description" class="form-control" id="description"></textarea>
      </div>
   </div>
   <div class="form-group">
        <?php echo form_submit('add_working_experience', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#working_experience', function(){
      oTable = $('#table_working_experience').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getWorkingExperience/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_working_experience').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_working_experience",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               location.reload();
             }
               
      });  
  });

    // $(document).on('submit', '#add_working_experienc', function(){
      
    //   $.ajax({
    //     url: site.base_url + 'employees/add_working_experience',
    //     method: 'POST',
    //     data: new FormData(this),
    //     dataType: 'text',
    //     success: function(data) {
      
    //     }
    //   });
    // });

   });

</script>
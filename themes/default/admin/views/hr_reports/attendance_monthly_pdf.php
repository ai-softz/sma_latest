<html>
    <head>
        <style>
            body {font-family: sans-serif;
                font-size: 10pt;
            }
            p {	margin: 0pt; }
            table.items {
                border: 0.1mm solid #000000;
            }
            td { vertical-align: top; }
            .items td {
                border-left: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
            }
            table thead td { background-color: #EEEEEE;
                text-align: center;
                border: 0.1mm solid #000000;
                font-variant: small-caps;
            }
            .items td.blanktotal {
                background-color: #EEEEEE;
                border: 0.1mm solid #000000;
                background-color: #FFFFFF;
                border: 0mm none #000000;
                border-top: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
            }
            .items td.totals {
                text-align: right;
                border: 0.1mm solid #000000;
            }
            .items td.cost {
                text-align: "." center;
            }
            .header {
                display: flex;
                
            }
            .emp_info {
                float: left;
                width: 50%;
            }
            .address {
                float: right;
                width: 50%;
                text-align: right;
            }
        </style>
    </head>
    <body style="overflow-x: scroll;">
        <div class="header">
            <div class="emp_info">
                <h2> Monthly Attendance Report </h2>
            </div>
            <div class="address">
                <address> Address: Eshtri </address>
            </div>
        </div>
        <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; overflow-x: scroll; " cellpadding="8">
            <thead>
              
                <tr>
                    <td width="">SL</td>
                    <td width="">Employee ID</td>
                    <td width="">Name</td>
                    <td width="">Department</td>
                    <td width="">Days of Month</td>
                    <td width="">Present</td>
                    <td width="">Absent</td>
                    <td width="">Holidays</td>
                    <td width="">Leaves</td>
                    <td width="">Working Days</td>
                    <td width="">Late In</td>
                    <td width="">Early Leave</td>
                    <td width="">Total Hours</td>
                    
                </tr>
            </thead>
            <tbody>
                <?php 
                $serial = 1;
                // echo '<pre>'; print_r($data); die;
                foreach($data as $row) { 
                ?>
                <tr>
                    <td> <?= $serial; ?> </td>
                    <td> <?= $row['employee_id']; ?> </td>
                    <td> <?= $row['emp_name']; ?> </td>
                    <td> <?= $row['department_name']; ?> </td>
                    <td> <?= $row['daysInMonth']; ?> </td>
                    <td> <?= $row['present_count']; ?> </td>
                    <td> <?= $row['absent_count']; ?> </td>
                    <td> <?= $row['holiday_count']; ?> </td>
                    <td> <?= $row['leave_count']; ?> </td>
                    <td> <?= $row['working_days']; ?> </td>
                    <td> <?= $row['total_late_in']; ?> </td>
                    <td> <?= $row['total_early_leave']; ?> </td>
                    <td> <?= $row['total_hours_monthly']; ?> </td>
                    
                </tr>
                <?php 
                $serial++;
                } 
                ?>
                
            </tbody>
        </table>

    <div style="margin-top: 20px; font-style: italic; ">Footer information</div>
    </body>
</html>
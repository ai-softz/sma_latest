<div class="row">
    <div class="col-sm-12 col-md-12">
	<div class="panel panel-bd lobidrag">
		<div class="panel-body">
			<div class="panel-heading" style="border-bottom: 1px solid #e4e5e7; margin-bottom: 5px;">
				<div class="panel-title">
						<h2>Monthly Attendance Report:</h2>
					
                </div>
				
				<div class="row">
					<div class="col-md-6">
						<input name="date" type="month" id="dateSearch" class="form-control" value="<?= date('Y-m'); ?>">
					</div>
					<div class="col-md-3">
						<a id="btnSearch" type="submit" class="btn btn-primary">Search</a>
					</div>
					<div class="col-md-3">
						<a href="<?= admin_url('hr_reports/payslip_list/?pdf=1') ?>" target="_blank" class="btn btn-primary" style="margin-bottom:5px; float: right"> <i class="fa fa-download"> </i>  Download Pdf</a>
					</div>
				</div>
				
				
	        </div>
            
			 <div class="table-responsive" width="95%" style="overflow-x: scroll;">
			 <table id="table_attendance_monthly_report" class="table table-bordered table-hover table-striped" >
			    <thead>
			       <tr>
			          <th><?php echo lang('the_number_sign');?></th>
			          <th><?php echo lang('name');?></th>
			          <th><?php echo lang('employee_code');?></th>
			          <th><?php echo lang('department');?></th>
			          <th><?php echo lang('date');?></th>
			          <th><?php echo lang('month_of_days');?></th>
			          <th><?php echo lang('present');?></th>
			          <th><?php echo lang('absent');?></th>
			          <th><?php echo lang('holidays');?></th>
			          <th><?php echo lang('leaves');?></th>
			          <th><?php echo lang('working_days');?></th>
			          <th><?php echo lang('late_in');?></th>
					  <th><?php echo lang('early_leave');?></th>
					  <th><?php echo lang('total_hours');?></th>

			       </tr>
			    </thead>
			    <tbody>
			    </tbody>
			 </table>
			</div>
		</div>
	</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		
		fill_datatable();
		function fill_datatable(dateSearch='') {
		oTable = $('#table_attendance_monthly_report').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "sScrollX": '100%',
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 
		   'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hr_reports/attendance_monthly_list') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
				//$('td:eq(7)', nRow).html(paymentStatus(nRow, aData));
				//return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>",
               });
			   aoData.push({
                   "name": "dateSearch",
                   "value": dateSearch,
               });
               
			//    console.log(aoData);
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false} , {"bSortable": false} , {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}
           ]
       });
	}
	$("#btnSearch").on("click", function(e){
		e.preventDefault();
		var dateSearch = $("#dateSearch").val();
		fill_datatable(dateSearch);
	});
});
	
	
</script>


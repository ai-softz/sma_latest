<html>
    <head>
        <style>
            body {font-family: sans-serif;
                font-size: 10pt;
            }
            p {	margin: 0pt; }
            table.items {
                border: 0.1mm solid #000000;
            }
            td { vertical-align: top; }
            .items td {
                border-left: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
            }
            table thead td { background-color: #EEEEEE;
                text-align: center;
                border: 0.1mm solid #000000;
                font-variant: small-caps;
            }
            .items td.blanktotal {
                background-color: #EEEEEE;
                border: 0.1mm solid #000000;
                background-color: #FFFFFF;
                border: 0mm none #000000;
                border-top: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
            }
            .items td.totals {
                text-align: right;
                border: 0.1mm solid #000000;
            }
            .items td.cost {
                text-align: "." center;
            }
            .header {
                display: flex;
                
            }
            .emp_info {
                float: left;
                width: 50%;
            }
            .address {
                float: right;
                width: 50%;
                text-align: right;
            }
        </style>
    </head>
    <body style="overflow-x: scroll;">
        <div class="header">
            <div class="emp_info">
                <h2> Salary Report </h2>
            </div>
            <div class="address">
                <address> Address: Eshtri </address>
            </div>
        </div>
        <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; overflow-x: scroll;" cellpadding="8">
            <thead>
                <tr >
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="4">Allowances</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="2">Deductions</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="">SL</td>
                    <td width="">Employee ID</td>
                    <td width="">Name</td>
                    <td width="">Department</td>
                    <td width="">Payroll Type</td>
                    <td width="">Basic Salary</td>
                    <td width="">House Rent</td>
                    <td width="">Medical</td>
                    <td width="">Travel</td>
                    <td width="">Other</td>
                    <td width="">Total Allowances</td>
                    <td width="">Overtime</td>
                    <td width="">Commissions</td>
                    <td width="">Other Payment</td>
                    <td width="">Loan</td>
                    <td width="">Statutory Deduction</td>
                    <td width="">Net Salary</td>
                    <td width="">Status</td>
                    
                </tr>
            </thead>
            <tbody>
                <?php 
                $serial = 1;
                // echo '<pre>'; print_r($data); die;
                foreach($data as $row) { 
                ?>
                <tr>
                    <td> <?= $serial; ?> </td>
                    <td> <?= $row['employee_id']; ?> </td>
                    <td> <?= $row['name']; ?> </td>
                    <td> <?= $row['department_name']; ?> </td>
                    <td> <?= $row['wage_type']; ?> </td>
                    <td> <?= $row['basic_salary']; ?> </td>
                    <td> <?= $row['house_rent']; ?> </td>
                    <td> <?= $row['medical_allowance']; ?> </td>
                    <td> <?= $row['travel_allowance']; ?> </td>
                    <td> <?= $row['other_allowance']; ?> </td>
                    <td> <?= $row['allowance_amount']; ?> </td>
                    <td> <?= $row['overtime_amount']; ?> </td>
                    <td> <?= $row['commissions_amount']; ?> </td>
                    <td> <?= $row['other_payments_amount']; ?> </td>
                    <td> <?= $row['loan_de_amount']; ?> </td>
                    <td> <?= $row['statutory_deductions_amount']; ?> </td>
                    <td> <?= $row['total_net_salary']; ?> </td>
                    <td> <?= $row['e_status']; ?> </td>
                    
                </tr>
                <?php 
                $serial++;
                } 
                ?>
                
            </tbody>
        </table>

    <div style="margin-top: 20px; font-style: italic; ">Footer information</div>
    </body>
</html>
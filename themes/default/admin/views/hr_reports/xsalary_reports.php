<div class="row">
    <div class="col-sm-12 col-md-12">
	<div class="panel panel-bd lobidrag">
		<div class="panel-body">
			<div class="panel-heading" style="border-bottom: 1px solid #e4e5e7; margin-bottom: 5px;">
				<div class="panel-title">
					<h2>Payroll:</h2>
					
                </div>
				
				<div class="row">
					<div class="col-md-6">
						<select name="emp_id" class="select" id="empId" style="width:100%"> 
							<option value="">Select Employee</option>
							<?php foreach($all_employees as $row) { ?>
								<option value="<?= $row->user_id ?>"><?= $row->first_name.' '.$row->last_name; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-3">
						<input name="date" type="text" id="salaryMonth" class="form-control date">
					</div>
					<div class="col-md-3">
						<a id="btnSearch" type="submit" class="btn btn-primary">Search</a>
					</div>
				</div>
				
	        </div>
	         
			 <div class="table-responsive" width="95%" style="overflow-x: scroll;">
			 <table id="table_payslips" class="table table-bordered table-hover table-striped" >
			    <thead>
			       <tr>
			          <th><?php echo lang('the_number_sign');?></th>
			          <th><?php echo lang('name');?></th>
			          <th><?php echo lang('employee_code');?></th>
			          <th><?php echo lang('department');?></th>
			          <th><?php echo lang('payroll_type');?></th>
			          <th><?php echo lang('basic_salary');?></th>
			          <th><?php echo lang('house_rent');?></th>
			          <th><?php echo lang('medical');?></th>
			          <th><?php echo lang('travel');?></th>
			          <th><?php echo lang('other');?></th>
			          <th style=""><?php echo lang('total_allowances');?></th>
			          <th><?php echo lang('overtime');?></th>
			          <th><?php echo lang('commissions');?></th>
			          <th style=""><?php echo lang('other_payment');?></th>
			          <th><?php echo lang('loan');?></th>
			          <th style=""><?php echo lang('statutory_deduction');?></th>
			          <th><?php echo lang('net_salary');?></th>
			          <th><?php echo lang('status');?></th>
			       </tr>
			    </thead>
			    <tbody>
			    </tbody>
			 </table>
			</div>
		</div>
	</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		fill_datatable();
		function fill_datatable(empid='', salaryMonth='') {
		var month_year = $('#month_year').val();
		oTable = $('#table_payslips').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
		//    "scrollX": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 
		   'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hr_reports/payslip_list?month_year=') ?>' + month_year,
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
				//$('td:eq(7)', nRow).html(paymentStatus(nRow, aData));
				//return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>",
               });
               aoData.push({
                   "name": "empid",
                   "value": empid,
               });
               aoData.push({
                   "name": "salaryMonth",
                   "value": salaryMonth,
               });
			//    console.log(aoData);
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
	}
	$("#btnSearch").on("click", function(e){
		e.preventDefault();
		var empid = $("#empId").val();
		var salaryMonth = $("#salaryMonth").val();
		console.log(empid);
		fill_datatable(empid, salaryMonth);
	});
});
	
	function paymentStatus(nRow, aData) 
	{
		var status = '';
		if(aData[7] == 0) {
			status = 'Unpaid';
		} else if(aData[7] == 1) {
			status = 'Partially Paid';
		}
		else if(aData[7] == 2) {
			status = 'Paid';
		}
		return status;
	}
</script>


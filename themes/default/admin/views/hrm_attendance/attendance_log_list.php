<br>
    <div class="search_area">
            <div class="row">
                <div class="col-md-3">
				   	<input type="text" id="departmentId" name="department" placeholder="Type department name..." class="form-control" autocomplete="off">
		      	</div> 
                <div class="col-md-3">
                    <input type="text" id="empid" placeholder="Type employee name..." class="form-control">
                </div>
                <div class="col-md-3">
					   <input name="" type="text" id="startDate" class="date form-control" value="" placeholder="dd-mm-yyyy" autocomplete="off">
				</div>
                <div class="col-md-3">
					   <input name="" type="text" id="endDate" class="date form-control" value="" placeholder="dd-mm-yyyy" autocomplete="off">
				</div>
			</div>
            <br>
            <div class="row">
				<div class="col-md-3">
					<a id="btnSearch" type="submit" class="btn btn-primary">Search</a>
				</div>
			</div>
    </div>
    <br>
<div class="table-responsive">
 <table id="table_hrm_attendance" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('log_day');?></th>
          <th><?php echo lang('shift');?></th>
          <th><?php echo lang('entry_roster_time');?></th>
          <th><?php echo lang('exit_roster_time');?></th>
          <th><?php echo lang('entry_punch');?></th>
          <th><?php echo lang('exit_punch');?></th>
          <th><?php echo lang('present_status');?></th>
          <th><?php echo lang('leave_status');?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

    fill_datatable();
    function fill_datatable(empid, departmentId, startDate, endDate) {
      oTable = $('#table_hrm_attendance').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_attendance/getAttendanceLog') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push(
                   {
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
                    },
                    { "name": "empid", "value": empid },
                    { "name": "departmentId", "value": departmentId },
                    { "name": "startDate", "value": startDate },
                    { "name": "endDate", "value": endDate },
               );
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    }

    $("#btnSearch").on("click", function(e){
         e.preventDefault();
         
         var empid = $("#empid").val().split('-');
         var departmentId = $("#departmentId").val().split('-');
         var startDate = $("#startDate").val();
         var endDate = $("#endDate").val();
         
         fill_datatable(empid[0], departmentId[0], startDate, endDate);
      });   
    

    $( "#empid").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

        $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
    });
</script>
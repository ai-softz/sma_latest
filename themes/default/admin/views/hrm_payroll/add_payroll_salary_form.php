<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payroll_emp_salary'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'add_payroll_emp_salary']; ?>
                <?php echo admin_form_open_multipart("hrm_payroll/add_payroll_emp_salary", $attrib) ?>

            <div class="form-group row">
                <div class="col-md-6">
				   <label for="salary_year"><?php echo  lang('salary_year') ?> <span class="text-danger"></span></label>
				   	<input type="month" id="salary_year" name="salary_year" class="form-control" value="<?= date('Y-m'); ?>">
		      	</div> 
                  <div class="col-md-6">
                    
                </div>  
            </div>    

            <div class="form-group row">
                <div class="col-md-6">
				   <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
				   	<input type="text" id="departmentId" name="department" placeholder="Type department name..." class="form-control">
                    
		      	</div> 
				<div class="col-md-6">
				   <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <div id="emp_area">
						<input type="text" name="employee" id="empid2" placeholder="Type employee name..." class="form-control">
                    </div>
		      	</div> 
            </div>
            
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="checked_by"><?php echo  lang('checked_by') ?> <span class="text-danger"></span></label>
                    <select name="checked_by" class="form-control select2" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($staffs as $row) { ?>
						    <option value="<?= $row->id ?>"><?php echo $row->first_name. ' ' . $row->last_name;?></option>
                        <?php } ?>
                    </select>
                </div>  
                <div class="col-md-6">
                    <label for="approved_by"><?php echo  lang('approved_by') ?> <span class="text-danger"></span></label>
                    <select name="approved_by" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($staffs as $row) { ?>
						    <option value="<?= $row->id ?>"><?php echo $row->first_name. ' ' . $row->last_name;?></option>
                        <?php } ?>
                    </select>
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                    <textarea name="remark" class="form-control"></textarea>
                </div>  
            </div>
            

            <div class="form-group">
                    <?php echo form_submit('add_income_tax', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script>
$(document).ready(function(){

    $('#add_payroll_emp_salary').on('submit', function(e){  
        e.preventDefault();       
        var formdata = new FormData(this);
        // console.log(formdata);
        $.ajax({  
                url: site.base_url + "hrm_payroll/add_payroll_emp_salary",
                method:"POST",  
                data:new FormData(this),  
                contentType: false,  
                cache: false,  
                processData:false,  
                success:function(data)  
                {  
                        $('#myModal').modal('hide');
                        location.reload();
                }
        });  
    });
    
        function floatConverter(value) {
            if (value == null || value == 'null' || value == '' || value == 'NULL') {
                return parseFloat('0');
            } else {
                return parseFloat(value);
            }
        }    
        function percentage(num, per)
        {
            var Perc=(num/100)*per;
            return Perc.toFixed(2);
        }

        $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#empid2").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid2').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

});
</script>
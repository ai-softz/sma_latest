<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style> 
    .till_end {
        width: 22px;
        height: 22px;
        cursor: pointer;
    }
    .table_tax_dtl > thead:first-child > tr:first-child > th{
        background: #fff;
        color: #000;
        border-color: #ccc;
        border-top: #ccc;
    }
    .addRow {
        cursor: pointer;
        font-size: 22px;
    }
    .deleteRow {
        cursor: pointer;
        font-size: 22px;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_income_tax'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_income_tax']; ?>
                <?php echo admin_form_open_multipart("hrm_payroll/edit_income_tax", $attrib) ?>                
            <input type="hidden" name="income_tax_id" value="<?= $income_tax->id ?>">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="<?= $income_tax->caption ?>" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="<?= $income_tax->caption_alt ?>">
                </div>  
            </div>
            
            <div class="form-group row">
                <div class="col-md-12">
                        <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger">*</span></label>
                        <textarea name="remark" class="form-control"><?= $income_tax->remark ?></textarea>
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <table id="table_tax_dtl" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr> 
                            <th>SL</th>
                            <th>Start Amount</th>
                            <th>End Amount</th>
                            <th>Tax Percent(%)</th>
                            <th>Till End</th>
                            <th>Delete</th>
                            <th>Add More</th>
                        </tr>
                        </thead>
                        <tbody id="addItem">
                        <?php 
                        $sl = 0;
                        foreach($income_tax_dtls as $row) { 
                            $sl++;
                        ?>
                        <tr>
                            <td> 
                                <?= $sl ?> 
                                <input type="hidden" name="row_no[]">
                                <input type="hidden" name="incomeTaxDtlId[]" id="incomeTaxDtlId" value="<?= $row->id ?>"> 
                            </td>
                            <td> 
                                <input type="text" name="start_amount[]" value="<?= $row->start_amount ?>" class="form-control">
                            </td>
                            <td> 
                                <input type="text" name="end_amount[]" value="<?= $row->end_amount ?>" class="form-control">
                            </td>
                            <td> 
                                <input type="text" name="tax_percent[]" value="<?= $row->tax_percent ?>" class="form-control">
                            </td>
                            <td> 
                                <?php if(empty($row->end_amount)) { ?>
                                    <input type="checkbox" name="is_close" class="form-control till_end" <?= empty($row->end_amount) ? 'Checked' : '' ?>>
                                <?php } ?>
                            </td>
                            <td> 
                                <?php if($sl == sizeof($income_tax_dtls)) { ?>
                                    <a class="deleteRow"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </td>
                            <td> 
                                <?php if($sl == sizeof($income_tax_dtls)) { ?>
                                    <a class="addRow"><i class="fa fa-plus"></i></a>
                                <?php } ?>
                                
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            <div class="form-group">
                    <?php echo form_submit('add_income_tax', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	


<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

    $('#edit_income_tax').on('submit', function(e){  
        e.preventDefault();       
        var formdata = new FormData(this);
        // console.log(formdata);
        $.ajax({  
                url: site.base_url + "hrm_payroll/edit_income_tax",
                method:"POST",  
                data:new FormData(this),  
                contentType: false,  
                cache: false,  
                processData:false,  
                success:function(data)  
                {  
                        $('#myModal').modal('hide');
                        location.reload();
                }
        });  
    });
    let sl = <?= $sl ?>;
    $(document).on("click", ".addRow", function(){

        sl++;
        localStorage.setItem('sl', sl);
        sl = localStorage.getItem('sl');

        var x = document.getElementById('table_tax_dtl');
        var len = x.rows.length;
        var previousrowlen = parseInt(len) - 1;
        
        // x.rows[len].cells[6].innerHTML = "";
        var previousEndAmount = x.rows[previousrowlen].cells[2].getElementsByTagName('input')[0].value;
        var previousStartAmount = x.rows[previousrowlen].cells[1].getElementsByTagName('input')[0].value;
        
            x.rows[previousrowlen].cells[4].innerHTML = "";
            x.rows[previousrowlen].cells[5].innerHTML = "";
            x.rows[previousrowlen].cells[6].innerHTML = "";
            // $(this).closest('tr').find('.till_end').hide();
            //var newstartAmount = parseInt(previousEndAmount) + 1;
            const html = `
            <tr>
                            <td> ${sl} <input type="hidden" name="row_no[]"> </td>
                            <td> 
                                <input type="text" name="start_amount[]" class="form-control startAmount">
                            </td>
                            <td> 
                                <input type="text" name="end_amount[]" id="" class="form-control endAmount">
                            </td>
                            <td> 
                                <input type="text" name="tax_percent[]" class="form-control">
                            </td>
                            <td> 
                                <input type="checkbox" name="is_close" class="form-control till_end">
                            </td>
                            <td> 
                                <a class="deleteRow"><i class="fa fa-trash"></i></a>
                            </td>
                            <td> 
                                <a class="addRow"><i class="fa fa-plus"></i></a>
                            </td>
                        </tr>
            `;
            $('#addItem').append(html);
        
        
    });

    $(document).on("click", ".deleteRow", function(){
        //$(this).closest('tr').remove();
        var x = document.getElementById('table_tax_dtl');
        var i = this.parentNode.parentNode.rowIndex;
        console.log(x, i);
        var previousrowlen = parseInt(i) - 1;
        if (i > 1) {
            //x.deleteRow(i);
            $(this).closest('tr').remove();
            x.rows[previousrowlen].cells[4].innerHTML = '<input type="checkbox" name="is_close" class="form-control till_end">';
            x.rows[previousrowlen].cells[5].innerHTML = '<a class="deleteRow"><i class="fa fa-trash"></i></a>';
            x.rows[previousrowlen].cells[6].innerHTML = '<a class="addRow"><i class="fa fa-plus"></i></a>';
        }
    });

    $(document).on("ifChanged", ".till_end", function(){
        console.log('hello');
        if($(this).closest('tr').find('.till_end').prop('checked') == true) {
            $(this).closest('tr').find('.endAmount').attr('readonly', true);
            $(this).closest('tr').find('.addRow').hide();
            $(this).closest('tr').find('.deleteRow').hide();
        } else {
            $(this).closest('tr').find('.endAmount').attr('readonly', false);
            $('.addRow').show();
            $('.deleteRow').show();
        }
    });

    $(document).on("click", ".till_end", function(){
        console.log('hello');
        if($(this).closest('tr').find('.till_end').prop('checked') == true) {
            $(this).closest('tr').find('.endAmount').attr('readonly', true);
            $(this).closest('tr').find('.addRow').hide();
            $(this).closest('tr').find('.deleteRow').hide();
        } else {
            $(this).closest('tr').find('.endAmount').attr('readonly', false);
            $('.addRow').show();
            $('.deleteRow').show();
        }
    });

});
</script>
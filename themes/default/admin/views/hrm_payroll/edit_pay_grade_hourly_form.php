<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_pay_grade'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_pay_grade']; ?>
                <?php echo admin_form_open_multipart("hrm_payroll/edit_pay_grade", $attrib) ?>
            <input type="hidden" name="pay_grade_id" value="<?= $pay_grade->id ?>">    
            <input type="hidden" name="payment_frequency_type" value="hourly">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="<?= $pay_grade->caption ?>" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="<?= $pay_grade->caption_alt ?>">
                </div>  
            </div>
            
            <div class="form-group row">
                 
                <div class="col-md-6">
                    <label for="hourly_rate"><?php echo  lang('hourly_rate') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('hourly_rate');?>" name="hourly_rate" type="text" value="<?= $pay_grade->hourly_rate ?>" required="required">   
                </div> 
                <div class="col-md-6">
                    <label for="overtime_rate"><?php echo  lang('overtime_rate') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('overtime_rate');?>" name="overtime_rate" type="text" value="<?= $pay_grade->overtime_rate ?>">   
                </div> 
            </div>
            <div class="form-group row">
                
                <div class="col-md-12">
                    <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                    <textarea name="remark" class="form-control"><?= $pay_grade->remark ?></textarea>
                </div>  
            </div>

            <div class="form-group">
                    <?php echo form_submit('add_income_tax', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script>
$(document).ready(function(){

    $('#edit_pay_grade').on('submit', function(e){  
        e.preventDefault();       
        var formdata = new FormData(this);
        // console.log(formdata);
        $.ajax({  
                url: site.base_url + "hrm_payroll/edit_pay_grade",
                method:"POST",  
                data:new FormData(this),  
                contentType: false,  
                cache: false,  
                processData:false,  
                success:function(data)  
                {  
                        $('#myModal').modal('hide');
                        location.reload();
                }
        });  
    });
});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_payroll_policy'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_payroll_policy']; ?>
                <?php echo admin_form_open_multipart("hrm_payroll/edit_payroll_policy", $attrib) ?>
                <input type="hidden" name="payroll_policy_id" value="<?= $payroll_policy->id ?>">

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="<?= $payroll_policy->caption ?>" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="<?= $payroll_policy->caption_alt ?>">
                </div>  
            </div>
            
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="salary_status"><?php echo  lang('salary_status') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('salary_status');?>" name="salary_status" type="text" value="<?= $payroll_policy->salary_status ?>" required="required">
                </div>
                <div class="col-md-6">
                    <label for="quantity"><?php echo  lang('quantity') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('quantity');?>" name="quantity" type="text" value="<?= $payroll_policy->quantity ?>" required="required">   
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                    <textarea name="remark" class="form-control"><?= $payroll_policy->remark ?></textarea>
                </div>  
            </div>

            <div class="form-group">
                    <?php echo form_submit('add_income_tax', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script>
$(document).ready(function(){

    $('#edit_payroll_policy').on('submit', function(e){  
        e.preventDefault();       
        var formdata = new FormData(this);
        // console.log(formdata);
        $.ajax({  
                url: site.base_url + "hrm_payroll/edit_payroll_policy",
                method:"POST",  
                data:new FormData(this),  
                contentType: false,  
                cache: false,  
                processData:false,  
                success:function(data)  
                {  
                        $('#myModal').modal('hide');
                        location.reload();
                }
        });  
    });
});
</script>
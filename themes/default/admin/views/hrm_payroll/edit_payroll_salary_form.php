<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payroll_emp_salary'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_payroll_emp_salary']; ?>
                <?php echo admin_form_open_multipart("hrm_payroll/edit_payroll_emp_salary", $attrib) ?>
            <input type="hidden" name="payroll_salary_id" value="<?= $payroll_salary->id ?>">
            <div class="form-group row">
                <div class="col-md-6">
				   <label for="salary_year"><?php echo  lang('salary_year') ?> <span class="text-danger"></span></label>
				   	<input type="month" id="salary_year" name="salary_year" class="form-control" value="<?= date('Y-m'); ?>">
		      	</div> 
                  <div class="col-md-6">
                    <label for="pay_grade_id"><?php echo  lang('pay_grade_id') ?> <span class="text-danger"></span></label>
                    <select name="pay_grade_id" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($all_pay_grade as $row) { ?>
						    <option value="<?= $row->id ?>" <?= $row->id == $payroll_salary->pay_grade_id ? 'Selected' : '' ?>><?php echo $row->caption;?></option>
                        <?php } ?>
						
                    </select> 
                </div>  
            </div>    

            <div class="form-group row">
                <div class="col-md-6">
				   <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
                   <?php
                        $dept = $this->shift_model->read_department_information($payroll_salary->department_id);
                   ?>
				   	<input type="text" id="departmentId" name="department" value="<?= $dept->department_id. ' - ' . $dept->department_name ?>" class="form-control">
                    
		      	</div> 
				<div class="col-md-6">
				   <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                   <?php
                        $emp_name = $this->employees_model->get_emp_name($payroll_salary->employee_id);
                        $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                   ?>
                    <div id="emp_area">
						<input type="text" name="employee" id="empid2" value="<?= $payroll_salary->employee_id. ' - ' . $emp_fullname ?>"  class="form-control">
                    </div>
		      	</div> 
            </div>
            
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="hourly_rate"><?php echo  lang('hourly_rate') ?> <span class="text-danger"></span></label>
                    <input type="text" name="hourly_rate" id="hourly_rate" class="form-control" value="<?= $payroll_salary->hourly_rate ?>">
                </div>  
                <div class="col-md-6">
                    <label for="overtime_rate"><?php echo  lang('overtime_rate') ?> <span class="text-danger"></span></label>
                    <input type="text" name="overtime_rate" id="overtime_rate" class="form-control" value="<?= $payroll_salary->overtime_rate ?>">
                </div>  
                
            </div>
            <div class="form-group row">
            <div class="col-md-6">
                    <label for="basic_salary"><?php echo  lang('basic_salary') ?> <span class="text-danger"></span></label>
                    <input type="text" name="basic_salary" id="basic_salary" class="form-control" value="<?= $payroll_salary->basic_salary ?>">
                </div> 
                <div class="col-md-6">
                    <label for="gross_salary"><?php echo  lang('gross_salary') ?> <span class="text-danger"></span></label>
                    <input type="text" name="gross_salary" id="gross_salary" class="form-control" value="<?= $payroll_salary->gross_salary ?>">
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="net_salary"><?php echo  lang('net_salary') ?> <span class="text-danger"></span></label>
                    <input type="text" name="net_salary" id="net_salary" class="form-control" value="<?= $payroll_salary->net_salary ?>">
                </div>  
                <div class="col-md-6">
                    <label for="tax_amount"><?php echo  lang('tax_amount') ?> <span class="text-danger"></span></label>
                    <input type="text" name="tax_amount" id="tax_amount" class="form-control" value="<?= $payroll_salary->tax_amount ?>">
                </div> 
            </div>
            
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="advance_deduct"><?php echo  lang('advance_deduct') ?> <span class="text-danger"></span></label>
                    <input type="text" name="advance_deduct" class="form-control" value="<?= $payroll_salary->advance_deduct ?>">
                </div> 
                <div class="col-md-6">
                    <label for="pf_amount"><?php echo  lang('pf_amount') ?> <span class="text-danger"></span></label>
                    <input type="text" name="pf_amount" class="form-control" value="<?= $payroll_salary->pf_amount ?>">
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="checked_by"><?php echo  lang('checked_by') ?> <span class="text-danger"></span></label>
                    <select name="checked_by" class="form-control select2" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($staffs as $row) { ?>
						    <option value="<?= $row->id ?>" <?= $row->id == $payroll_salary->checked_by ? 'Selected' : '' ?>><?php echo $row->first_name. ' ' . $row->last_name;?></option>
                        <?php } ?>
                    </select>
                </div>  
                <div class="col-md-6">
                    <label for="approved_by"><?php echo  lang('approved_by') ?> <span class="text-danger"></span></label>
                    <select name="approved_by" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($staffs as $row) { ?>
						    <option value="<?= $row->id ?>" <?= $row->id == $payroll_salary->approved_by ? 'Selected' : '' ?>><?php echo $row->first_name. ' ' . $row->last_name;?></option>
                        <?php } ?>
                    </select>
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                    <textarea name="remark" class="form-control"></textarea>
                </div>  
            </div>

            <div class="form-group">
                    <?php echo form_submit('add_income_tax', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script>
$(document).ready(function(){

    $('#edit_payroll_emp_salary').on('submit', function(e){  
        e.preventDefault();       
        var formdata = new FormData(this);
        // console.log(formdata);
        $.ajax({  
                url: site.base_url + "hrm_payroll/edit_payroll_emp_salary",
                method:"POST",  
                data:new FormData(this),  
                contentType: false,  
                cache: false,  
                processData:false,  
                success:function(data)  
                {  
                        //$('#myModal').modal('hide');
                        //location.reload();
                }
        });  
    });
    $(document).on('change', '#pay_grade_id', function(){
        const payGradeId = $(this).val();
        $.ajax({

            url: site.base_url + 'hrm_payroll/getPayGradeDetail',
            method: 'POST',
            data: { payGradeId : payGradeId },
            success: function(data) {
                $('#hrmPrlSalaryDtls-table > tbody').html('');
                var rowNumber = 1;
                // console.log(data.payGradeDetail);
                // console.log(data.payGrade);
                if (data.payGrade.payment_frequency_type == "Monthly") {
                    // if (data.payGradeDetail != null) {
                    //     $.each(data.payGradeDetail, function (key, value) {
                    //         addDetailInTable(rowNumber++, '', value.payroll_head_id, value.payroll_head_text, value.pay_nature, value.addition_deduction, value.allowance_type, value.actual_amount, value.max_limit, value.percentage_of_basic);
                    //     });
                    // }
                    $("#gross_salary").val(data.payGrade.gross_salary);
                    $("#basic_percent").val(data.payGrade.basic_percent);
                    $("#basic_salary").val(data.payGrade.basic_salary);
                    $("#overtime_rate").val(data.payGrade.overtime_rate);
                } else if (data.payGrade.payment_frequency_type == "Hourly") {
                    $("#hourly_rate").val(data.payGrade.hourly_rate);
                }
                totalAmount();
            }
        });
    });
    function addDetailInTable(rowNumber, id, payrollHeadId, payrollHeadText, payNature, additionDeduction, allowanceType, actualAmt, maxLimit, percentageOfBasic, additionDeductionKey='', allowanceTypeKey='') {
        // <td><input type="hidden" class="form-control" name="id" value="${id}"></td>
        const html = `
        <tr>
            
            <td> 
                ${rowNumber} 
                <input type="hidden" name="row_no[]"> 
                <input type="hidden" name="payroll_head_id[]" value="${payrollHeadId}">
                
            </td>
            <td><input type="text" class="form-control payroll_head_text" name="payroll_head_text[]" value="${payrollHeadText}" readonly></td>
            <td><input type="text" class="form-control pay_nature" name="pay_nature[]" value="${payNature}" readonly></td>
            <td><input type="text" class="form-control addition_deduction" name="addition_deduction[]" value="${additionDeduction}" readonly></td>
            <td><input type="text" class="form-control allowance_type" name="allowance_type[]" value="${allowanceType}" readonly></td>
            <td><input type="text" class="form-control actual_amount" name="actual_amount[]" value="${actualAmt}" ></td>
            <td><input type="text" class="form-control max_limit" name="max_limit[]" value="${maxLimit}"></td>
        </tr>
        `;
        $('#hrmPrlSalaryDtls-table > tbody').append(html);
    }

    function totalAmount() {
            var lineTotalSum = 0;
            var additionSum = 0;
            var deductionSum = 0;
            var lineTotalSumText = '';
            var basicSalary = 0;
            $("#hrmPrlSalaryDtls-table tbody tr").each(function () {
            
                if ($(this).find(".addition_deduction").val() == "Addition") {
                    additionSum += floatConverter($(this).find(".actual_amount").val());
                } else if ($(this).find(".addition_deduction").val() == "Deduction}") {
                    deductionSum += floatConverter($(this).find(".actual_amount").val());
                }
                lineTotalSum = additionSum - deductionSum;
            });
            console.log(lineTotalSum, additionSum, deductionSum);
            basicSalary = floatConverter($("#basic_salary").val());
            lineTotalSum+=basicSalary;
            additionSum+=basicSalary;
            lineTotalSumText = additionSum + ' - ' + deductionSum + ' = ' + lineTotalSum;
            $('#gross_salary').val(additionSum);
            $('#net_salary').val(additionSum-deductionSum);
        }
        function floatConverter(value) {
            if (value == null || value == 'null' || value == '' || value == 'NULL') {
                return parseFloat('0');
            } else {
                return parseFloat(value);
            }
        }    
        function percentage(num, per)
        {
            var Perc=(num/100)*per;
            return Perc.toFixed(2);
        }

        $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#empid2").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid2').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

});
</script>
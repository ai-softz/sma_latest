<?php
    echo "<a href='" . admin_url('hrm_payroll/add_pay_grade_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_pay_grade') . "'><i class=\"fa fa-plus\"></i>" . lang('add_pay_grade') . " </a>";
?>
&nbsp; &nbsp;
<?php
    echo "<a href='" . admin_url('hrm_payroll/add_pay_grade_hourly_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_pay_grade') . "'><i class=\"fa fa-plus\"></i>" . lang('add_hourly_pay_grade') . " </a>";
?>

<h2>Pay Grade:</h2>

<div class="table-responsive">
 <table id="table_pay_grade" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('caption');?></th>
          <th><?php echo lang('caption_alt');?></th>    
          <th><?php echo lang('payment_frequency_type');?></th>    
          <th><?php echo lang('gross_salary');?></th>    
          <th><?php echo lang('basic_percent');?></th>    
          <th><?php echo lang('basic_salary');?></th>    
          <th><?php echo lang('hourly_rate');?></th>    
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

      oTable = $('#table_pay_grade').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_payroll/getPayGrade') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });

    });

    </script>
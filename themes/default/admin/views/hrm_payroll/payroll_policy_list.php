<?php
    echo "<a href='" . admin_url('hrm_payroll/add_payroll_policy_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_payroll_policy') . "'><i class=\"fa fa-plus\"></i> " . lang('add_payroll_policy') . " </a>";
?>
&nbsp; &nbsp;
<?php
    echo "<a href='#' class='btn btn-primary' id='add_default_payroll_policy' title='" . lang('add_default_payroll_policy') . "'><i class=\"fa fa-plus\"></i> " . lang('add_default_payroll_policy') . "</a>";
?>
<h2>Income Tax:</h2>

<div class="table-responsive">
 <table id="table_payroll_policy" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('caption');?></th>
          <th><?php echo lang('caption_alt');?></th>    
          <th><?php echo lang('quantity');?></th>    
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

      oTable = $('#table_payroll_policy').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_payroll/getPayrollPolicy/') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });

       $(document).on('click', '#add_addition_default', function(){
            $.ajax({  
                url: site.base_url + "hrm_payroll/add_addition_default",
                method:"GET",  
                success:function(data)  {  
                    location.reload();
                }
            });  
       });

       $(document).on('click', '#add_default_payroll_policy', function(){
            $.ajax({  
                url: site.base_url + "hrm_payroll/add_default_payroll_policy",
                method:"GET",  
                success:function(data)  {  
                    location.reload();
                }
            });  
       });

    });

    </script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_bonus_setup'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'add_bonus_setup']; ?>
                <?php echo admin_form_open_multipart("hrm_payslip/add_bonus_setup", $attrib) ?>                

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="">
                </div>  
                
            </div>
            <div class="form-group row"> 
            
                <div class="col-md-6">
                    <label for="allowance_type"><?php echo  lang('allowance_type') ?> <span class="text-danger"></span></label>
                    <?php
                        $allowance_types = array(
                        'Percent' => 'Percent',
                        'Fixed' => 'Fixed',
                        );
                    ?>
                    <select name="allowance_type" class="form-control select"  required="required">
                            <option value="">Select</option>
                            <?php foreach($allowance_types as $key => $value) {?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
                
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                        <label for="percentage_of_bonus"><?php echo  lang('percentage_of_bonus') ?> <span class="text-danger">*</span></label>
                        <input class="form-control" placeholder="<?php echo lang('percentage_of_bonus');?>" name="percentage_of_bonus" type="text" value="" required="required">
                </div>  
                <div class="col-md-6">
                        <label for="max_limit"><?php echo  lang('max_limit') ?> <span class="text-danger">*</span></label>
                        <input class="form-control" placeholder="<?php echo lang('max_limit');?>" name="max_limit" type="text" value="" >
                </div> 
            </div>
            <div class="form-group row">
                 
                <div class="col-md-12">
                        <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger">*</span></label>
                        <textarea name="remark" class="form-control"></textarea>
                </div>  
            </div>
            <div class="form-group">
                    <?php echo form_submit('add_employee_allowances', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_bonus_setup').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_payslip/add_bonus_setup",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

  $(document).on('change', '#department', function(){
        var deptId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_roster/get_dept_employees',
            method: 'POST',
            type: 'text',
            data: { dept_id: deptId },
            success: function(data) {
                $('#employee_id').remove();
                $('#emp_area').html(data);
            }
        });
  });

});
</script>
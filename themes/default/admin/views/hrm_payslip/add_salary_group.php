<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_salary_group'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'add_salary_group']; ?>
                <?php echo admin_form_open_multipart("hrm_payslip/add_salary_group", $attrib) ?>                

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="">
                </div>  
                
            </div>
            <div class="form-group row"> 
            
                <div class="col-md-6">
                    <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
                    <input type="text" id="departmentId" name="department" placeholder="Type department name..." class="form-control" autocomplete="off">
                </div>
                
            </div>

            <div class="form-group row">
                 
                <div class="col-md-12">
                        <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger">*</span></label>
                        <textarea name="remark" class="form-control"></textarea>
                </div>  
            </div>
            <div class="form-group">
                    <?php echo form_submit('add_employee_allowances', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_salary_group').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_payslip/add_salary_group",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

  $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.label + ' | ' + ui.item.value);                
                return false;
            }
        });	

});
</script>
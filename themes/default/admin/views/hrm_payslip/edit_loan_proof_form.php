<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_salary_group'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_loan_proof']; ?>
                <?php echo admin_form_open_multipart("hrm_payslip/edit_loan_proof", $attrib) ?>                
<input type="hidden" name="loan_proof_id" value="<?= $loan_proof->id ?>">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="<?= $loan_proof->caption ?>" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="<?= $loan_proof->caption_alt ?>">
                </div>  
                
            </div>
            <div class="form-group row"> 
            
                <div class="col-md-6">
                    <label for="document_type"><?php echo  lang('document_type') ?> <span class="text-danger"></span></label>
                    <?php
                        $document_types = array(
                        'Education' => 'Education',
                        'Driving Licence' => 'Driving Licence',
                        'Passport' => 'Passport',
                        'Visa' => 'Visa',
                        'Iqama' => 'Iqama',
                        'Medical Insurance' => 'Medical Insurance',
                        );
                    ?>
                    <select name="document_type" class="form-control select"  required="required">
                            <option value="">Select</option>
                            <?php foreach($document_types as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $loan_proof->document_type == $key ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-md-6">
                        <label for="is_mandatory"><?php echo  lang('is_mandatory') ?> <span class="text-danger"></span></label> <br>
                        <input name="is_mandatory" type="checkbox" class="form-control" <?= $loan_proof->is_mandatory == 1 ? 'Checked' : '' ?>>
                </div>  
                
            </div>

            <div class="form-group row">
                 
                <div class="col-md-12">
                        <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger">*</span></label>
                        <textarea name="remark" class="form-control"><?= $loan_proof->remark ?></textarea>
                </div>  
            </div>
            <div class="form-group">
                    <?php echo form_submit('edit_loan_proof', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#edit_loan_proof').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_payslip/edit_loan_proof",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
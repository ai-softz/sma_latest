<?php
    echo "<a href='" . admin_url('hrm_payslip/add_loan_proof_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_payroll_loan_proof') . "'><i class=\"fa fa-plus\"></i>" . lang('add_payroll_loan_proof') . " </a>";
?>
&nbsp;&nbsp;

<h2>Loan Type:</h2>

<div class="table-responsive">
 <table id="table_payroll_loan_proof" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
       <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('caption');?></th>
          <th><?php echo lang('caption_alt');?></th>
          <th><?php echo lang('document_type');?></th>     
          <th><?php echo lang('is_mandatory');?></th>    
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

      oTable = $('#table_payroll_loan_proof').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_payslip/get_payroll_loan_proof/') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });

      

    });

    </script>
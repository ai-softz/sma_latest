<?php
    echo "<a href='" . admin_url('hrm_payslip/add_loan_advance_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_payroll_loan_advance') . "'><i class=\"fa fa-plus\"></i> Add Loan Advance</a>";
?>
&nbsp;&nbsp;

<h2>Loan Advance:</h2>

<div class="table-responsive">
 <table id="table_payroll_loan_advance" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('loan_type');?></th>     
          <th><?php echo lang('principle_amount');?></th>  
          <th><?php echo lang('instalments');?></th>      
          <th><?php echo lang('interest_rate');?></th>     
          <th><?php echo lang('interest_amount');?></th>     
          <th><?php echo lang('net_amount');?></th>     
          <th><?php echo lang('apply_date');?></th>     
          <th><?php echo lang('first_settlement_date');?></th>      
             
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

      oTable = $('#table_payroll_loan_advance').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_payslip/get_payroll_loan_advance/') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false},{"bSortable": false}, {"bSortable": false}, {"bSortable": false},{"bSortable": false}, {"bSortable": false} ]
       });

    });

    </script>
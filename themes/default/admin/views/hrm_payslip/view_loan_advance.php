

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_loan_advance'); ?></h4>
        </div>

        <div class="modal-body">
        <h2> Loan Advance Detail </h2>
        <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
                          <?php
                                    $emp_name = $this->employees_model->get_emp_name($loan_advance->employee_id);
                                    $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                            ?>
	                      	<th>Employee</th><td><?= $emp_fullname ?></td>
                            <th>Loan Type</th><td><?= $loan_advance->loan_type ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Principle Amount</th><td><?= number_format($loan_advance->principle_amount, 2) ?></td>
                            <th>Instalments</th><td><?= $loan_advance->instalments ?></td>
	                      </tr>
                          <tr>
	                      	<th>Interest Rate</th><td><?= number_format($loan_advance->interest_rate, 2) ?></td>
                            <th>Interest Amount</th><td><?= $loan_advance->interest_amount ?></td>
	                      </tr>
                          <tr>
                            <th>Net Amount</th><td><?= number_format($loan_advance->net_amount, 2) ?></td>
	                      </tr>
                          <tr>
	                      	<th>Apply Date</th><td><?= date('d-m-Y',strtotime($loan_advance->transaction_date)) ?></td>
                            <th>First Settlement Date</th><td><?= date('d-m-Y', strtotime($loan_advance->first_settlement_date)) ?></td>
	                      </tr>
                          <tr>
	                      	<th>Salary Rules</th><td><?= $loan_advance->salary_rules ?></td>
                            <th>Repayment Method</th><td><?= $loan_advance->repayment_method ?></td>
	                      </tr>
                          <tr>
                          <?php
                                if($loan_advance->guarantor1_id != 0) {
                                    $guarantor1 = $this->employees_model->get_emp_name($loan_advance->guarantor1_id);
                                    $guarantor1_fullname = $guarantor1->first_name. ' '.$guarantor1->last_name;
                                } else {
                                    $guarantor1_fullname = '';
                                }
                            ?>
	                      	<th>Guarantor 1</th><td><?= $guarantor1_fullname ?></td>
                              <?php
                                if($loan_advance->guarantor2_id != 0) {
                                    $guarantor2 = $this->employees_model->get_emp_name($loan_advance->guarantor2_id);
                                    $guarantor2_fullname = $guarantor2->first_name. ' '.$guarantor2->last_name;
                                } else {
                                    $guarantor2_fullname = '';
                                }
                            ?>
                            <th>Guarantor 1</th><td><?= $guarantor2_fullname ?></td>
	                      </tr>
                          <tr>
                            <th>Remark</th><td><?= $loan_advance->remark ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>

        <div>
            <h2> Loan Advance Detail </h2>
        <table id="hrmLoanAdvanceDtls-table" border="0" class="table table-stripped table-bordered">
                    <tbody>
                        <tr style="text-align: center;">
                            <td>SL</td>
                            <td>Transaction Date</td>
                            <td>Settlement Date</td>
                            <td>Interest Amount</td>
                            <td>Principle Amount</td>
                            <td>Instalment Amount</td>
                            <td>Paid Amount</td>
                        </tr>
                        <?php 
                        $i = 1;
                        foreach($loan_advance_dtls as $value) 
                        { ?>
                            <tr>
                                <td> <?= $i ?> </td>
                                <td> <?= $value->transaction_date ?> </td>
                                <td> <?= $value->settlement_date ?> </td>
                                <td> <?= number_format($value->interest_amount, 2) ?> </td>
                                <td> <?= number_format($value->principle_amount, 2) ?> </td>
                                <td> <?= number_format($value->instalment_amount, 2) ?> </td>
                                <td> <?= number_format($value->paid_amount, 2) ?> </td>
                        <?php 
                        $i++;
                        } 
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

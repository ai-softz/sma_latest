<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_roster'); ?></h4>
    </div>

    <div class="modal-body">
	<p class="error_msg text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_roster_details']; ?>
		     <?php echo admin_form_open_multipart("hrm_roster_details/add_roster_details", $attrib) ?>
		     
           <div class="form-group row">
                <div class="col-md-6">
					<label for="roster_date"><?php echo  lang('roster_date') ?> <span class="text-danger"></span></label>
					<input type="text" name="roster_date" class="date form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="card_no"><?php echo  lang('card_no') ?> <span class="text-danger"></span></label>
					<input type="text" name="card_no" class="form-control">
		        </div> 
		   </div>
		   <div class="form-group row">
                <div class="col-md-6">
					<label for="roster_id2"><?php echo  lang('roster') ?> <span class="text-danger"></span></label>
					<input type="text" id="rosterId2" name="roster_id" placeholder="Type roster name..." class="form-control">
					
		        </div>
				<div class="col-md-6">
					<label for="period_id"><?php echo  lang('period_master') ?> <span class="text-danger"></span></label>
					<input type="text" id="period_id2" name="period_id" placeholder="Type period name..." class="form-control">
					
		        </div>
				
		   </div>
		   
		   <div class="form-group row">
		   		<div class="col-md-6">
				   <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
				   	<input type="text" id="departmentId" name="department" placeholder="Type department name..." class="form-control">
                    
		      	</div> 
				<div class="col-md-6">
				   <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <div id="emp_area">
						<input type="text" name="employee" id="empid2" placeholder="Type employee name..." class="form-control">
                    </div>
		      	</div> 
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
				   <label for="shift_id"><?php echo  lang('shift_id') ?> <span class="text-danger"></span></label>
                    <select name="shift_id" class="form-control select" id="shift_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($shift_masters as $row) { ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->name;?></option>
                    <?php } ?>
                    </select>
		      	</div> 			
		   		
		   </div>
		   
           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_roster_time"><?php echo  lang('entry_roster_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_roster_time" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="exit_roster_time"><?php echo  lang('exit_roster_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="exit_roster_time" class="form-control" autocomplete="off">
		        </div> 
		   </div>
		   
           
           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_roster_time_tol"><?php echo  lang('entry_roster_time_tol') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_roster_time_tol" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="half_day_time"><?php echo  lang('half_day_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="half_day_time" class="form-control" autocomplete="off">
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_time"><?php echo  lang('entry_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_time" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="exit_time"><?php echo  lang('exit_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="exit_time" class="form-control" autocomplete="off">
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="present_status"><?php echo  lang('present_status') ?> <span class="text-danger"></span></label>
                    <select name="present_status" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="Present"><?php echo 'Present'?></option>
						<option value="Absent"><?php echo 'Absent'?></option>
                    </select>
		        </div>
				<div class="col-md-6">
					<label for="leave_status"><?php echo  lang('leave_status') ?> <span class="text-danger"></span></label>
                    <select name="leave_status" class="form-control select" id="">
						<option value=""><?php echo 'Select'?></option>
						<option value="Annual Leave"><?php echo lang('anuual_leave') ?></option>
						<option value="Casual Leave"><?php lang('casual_leave') ?></option>
                    </select>
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="petrol_log"><?php echo  lang('petrol_log') ?> <span class="text-danger"></span></label>
					<input type="text" name="petrol_log" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="overtime_enable"><?php echo  lang('is_overtime') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="overtime_enable">
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="overtime_minute"><?php echo  lang('overtime_minute') ?> <span class="text-danger"></span></label>
					<input type="text" name="overtime_minute" class="form-control">
		        </div>
				<div class="col-md-6">
					<label for="overtime_after"><?php echo  lang('overtime_after_minute') ?> <span class="text-danger"></span></label> <br>
					<input type="datetime-local" name="overtime_after" class="form-control" autocomplete="off">
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="duty_type"><?php echo  lang('duty_type') ?> <span class="text-danger"></span></label>
                    <select name="duty_type" class="form-control select" id="">
						<option value=""><?php echo lang('select') ;?></option>
						<option value="Regular"><?php echo lang('regular') ?></option>
						<option value="Paid Overtime"><?php echo lang('paid_overtime') ?></option>
						<option value="Unpaid Overtime"><?php echo lang('unpaid_overtime') ?></option>
                    </select>
		        </div>
				<div class="col-md-6">
					<label for="day_duration"><?php echo  lang('day_duration') ?> <span class="text-danger"></span></label>
                    <select name="day_duration" class="form-control select" id="">
						<option value=""><?php echo lang('select');?></option>
						<option value="DayShift"><?php echo lang('day_shift') ; ?></option>
						<option value="NightShift"><?php echo lang('night_shift') ; ?></option>
                    </select>
		        </div> 
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
					<label for="is_weekend"><?php echo  lang('is_weekend') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_weekend">
		        </div> 
                <div class="col-md-6">
					<label for="holiday_caption"><?php echo  lang('holiday_caption') ?> <span class="text-danger"></span></label>
					<input type="text" name="holiday_caption" class="form-control">
		        </div>
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
					<label for="is_consider"><?php echo  lang('is_consider') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_consider" class="form-control">
		        </div> 
                <div class="col-md-6">
					<label for="pay_type"><?php echo  lang('pay_type') ?> <span class="text-danger"></span></label>
					<select name="pay_type" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="In Salary"><?php echo lang('in_salary') ;?></option>
						<option value="Frequently"><?php echo lang('frequently') ;?></option>
                    </select>		        
				</div>
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
					<label for="is_edited"><?php echo  lang('is_edited') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_edited" class="form-control">
		        </div> 
                <div class="col-md-6">
					<label for="is_approved"><?php echo  lang('is_approved') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_approved" class="form-control">
		        </div> 
		   </div>
		   <div class="form-group row">
                <div class="col-md-6">
					<label for="salary_status"><?php echo lang('salary_status') ?> <span class="text-danger"></span></label>
                    <select name="salary_status" class="form-control select" id="">
						<option value=""><?php echo lang('select');?></option>
						<option value="Regular"><?php echo lang('regular')?></option>
						<option value="Unauthorized Leave"><?php echo lang('unauthorized_leave') ?></option>
						<option value="Late Present"><?php echo lang('late_present') ?></option>
						<option value="Half Day Leave"><?php echo lang('half_day_leave') ?></option>
						<option value="Early Leave"><?php echo lang('early_leave') ?></option>
						<option value="In Leave"><?php echo lang('in_leave') ?></option>
						<option value="In Tour"><?php echo lang('in_tour') ?></option>
                    </select>
		        </div>
				<div class="col-md-6">
					<label for="confirm_status"><?php echo  lang('confirm_status') ?> <span class="text-danger"></span></label>
                    <select name="confirm_status" class="form-control select" id="">
						<option value=""><?php echo lang('select') ;?></option>
						<option value="YES"><?php echo lang('yes') ;?></option>
						<option value="NO"><?php echo lang('no') ;?></option>
                    </select>
		        </div> 
		   </div>
           
		   <div class="form-group">
		        <?php echo form_submit('add_shift_master', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_roster_details').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_roster_details/add_roster_details",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    // $('#myModal').modal('hide');
				    // location.reload();
             }
               
      });  
  });

  $(document).on('change', '#department', function(){
        var deptId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_roster/get_dept_employees',
            method: 'POST',
            type: 'text',
            data: { dept_id: deptId },
            success: function(data) {
                $('#employee_id').remove();
                $('#emp_area').html(data);
            }
        });
  });

  $( "#rosterId2").autocomplete({
		source: function( request, response ) {

			// Fetch data
			$.ajax({
				url: site.base_url + "hrm_roster_details/get_roster_select2",
				type: 'post',
				dataType: "json",
				data: {
					roster_name: request.term,
					_token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
				},
				success: function( data ) {
					response( data );
				}
			});
		},
		select: function (event, ui) {
			// Set selection
			$('#rosterId2').val(ui.item.value + ' - ' + ui.item.label);                
			return false;
		}
	});

	$( "#period_id2").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_period_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#period_id2').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });
		$( "#empid2").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid2').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
		$( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

});
</script>
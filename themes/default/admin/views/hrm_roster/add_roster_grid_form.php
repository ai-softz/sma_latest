<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_roster_grid'); ?></h4>
    </div>

    <div class="modal-body">
	<p class="error_msg text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_roster_grid']; ?>
		     <?php echo admin_form_open_multipart("hrm_roster_grid/add_roster_grid", $attrib) ?>
		     
             <div class="form-group row">
		   		
				<div class="col-md-6">
				   <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <div id="emp_area">
						<input type="text" name="employee" id="empid" placeholder="Type employee name..." class="form-control">
                    </div>
		      	</div> 
                <div class="col-md-6">
					<label for="card_no"><?php echo  lang('card_no') ?> <span class="text-danger"></span></label>
					<input type="text" name="card_no" class="form-control">
		        </div>   
		   </div>
		   
           <div class="form-group row">
                <div class="col-md-6">
					<label for="roster_date"><?php echo  lang('roster_date') ?> <span class="text-danger"></span></label>
					<input type="text" name="roster_date" value="" class="date form-control" autocomplete="off">
		        </div>
		   		<div class="col-md-6">
				   <label for="day_duration"><?php echo  lang('day_duration') ?> <span class="text-danger"></span></label>
                    <select name="day_duration" class="form-control select">
                        <option>Select</option>
                            <option value="DayShift"><?php echo lang('day_shift'); ?></option>
                            <option value="NightShift"><?php echo lang('night_shift'); ?></option>
                    </select>
		      	</div> 
		   </div>

           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_roster_time"><?php echo  lang('entry_roster_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_roster_time" value="" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="exit_roster_time"><?php echo  lang('exit_roster_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="exit_roster_time" value="" class="form-control" autocomplete="off">
		        </div> 
		   </div>

           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_roster_time_tol"><?php echo  lang('entry_roster_time_tol') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_roster_time_tol" value="" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
                    <label for="duty_type"><?php echo  lang('duty_type') ?> <span class="text-danger"></span></label>
                    <select name="duty_type" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="Regular"><?php echo lang('regular')?></option>
						<option value="Paid Overtime"><?php echo lang('paid_overtime')?></option>
						<option value="Unpaid Overtime"><?php echo lang('unpaid_overtime')?></option>
                    </select>
		        </div> 
		   </div>

           <div class="form-group row">
                
				<div class="col-md-6">
					<label for="overtime_enable"><?php echo  lang('is_overtime') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="overtime_enable">
		        </div> 
                <div class="col-md-6">
					<label for="is_weekend"><?php echo  lang('is_weekend') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_weekend">
		        </div> 
		   </div>
		   
           <div class="form-group row">
		      <div class="col-md-12">
				<label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
				<textarea name="remark" class="form-control"> </textarea>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_shift_master', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_roster_grid').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_roster_grid/add_roster_grid",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                 if(data.error == 0) {
				    $('#myModal').modal('hide');
				    location.reload();
                 } 
                 else {
                     $('.error_msg').text(data.msg);
                 }
             }
               
      });  
  });

  $( "#empid").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

  $(document).on('change', '#department', function(){
        var deptId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_roster/get_dept_employees',
            method: 'POST',
            type: 'text',
            data: { dept_id: deptId },
            success: function(data) {
                $('#employee_id').remove();
                $('#emp_area').html(data);
            }
        });
  });

});
</script>
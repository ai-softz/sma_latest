<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_roster'); ?></h4>
    </div>
<?php //echo '<pre>'; print_r($roster_detail) ?> 
    <div class="modal-body">
	<p class="error_msg text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_roster_details']; ?>
		     <?php echo admin_form_open_multipart("hrm_roster_details/edit_roster_details", $attrib) ?>
		     <input type="hidden" name="roster_detail_id" value="<?= $roster_detail->id ?>" class="form-control">
           <div class="form-group row">
                <div class="col-md-6">
					<label for="roster_date"><?php echo  lang('roster_date') ?> <span class="text-danger"></span></label>
					<input type="text" name="roster_date" value="<?= date('d-m-Y', strtotime($roster_detail->roster_date)) ?>" class="date form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="card_no"><?php echo  lang('card_no') ?> <span class="text-danger"></span></label>
					<input type="text" name="card_no" value="<?= $roster_detail->card_no ?>" class="form-control">
		        </div> 
		   </div>
		   <div class="form-group row">
                <div class="col-md-6">
					<label for="roster_id"><?php echo  lang('roster') ?> <span class="text-danger"></span></label>
					<select name="roster_id" class="form-control select" id="roster_id">
						<option value=""><?php echo 'Select';?></option>
					<?php foreach($rosters as $row) { ?>
						<option value="<?php echo $row->id; ?>" <?= $row->id == $roster_detail->roster_id ? 'Selected' : '' ?>><?php echo $row->caption;?></option>
					<?php } ?>
					</select>
		        </div>
				<div class="col-md-6">
					<label for="period_id"><?php echo  lang('period_master') ?> <span class="text-danger"></span></label>
					<select name="period_id" class="form-control select" id="period_id">
						<option value=""><?php echo 'Select';?></option>
						<?php foreach($period_masters as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?= $row->id == $roster_detail->period_id ? 'Selected' : '' ?>><?php echo $row->name;?></option>
						<?php } ?>
					</select>
		        </div>
				
		   </div>
		   
		   <div class="form-group row">
		   		<div class="col-md-6">
				   <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
                    <select name="department" class="form-control select" id="department">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_departments as $row) { ?>
                        <option value="<?php echo $row->department_id; ?>" <?= $row->department_id == $roster_detail->department_id ? 'Selected' : '' ?>><?php echo $row->department_name;?></option>
                    <?php } ?>
                    </select>
		      	</div> 
				<div class="col-md-6">
				   <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <div id="emp_area">
                        <?php
                            $emp_name = $this->employees_model->get_emp_name($roster_detail->employee_id);
                            $emp_full_name = $emp_name->first_name.' '.$emp_name->last_name;
                        ?>
                        <select name="employee" class="form-control select" id="employee_id">
						<option value="<?= $roster_detail->employee_id ?>"><?php echo $emp_full_name;?></option>
                        </select>
                    </div>
		      	</div> 
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
				   <label for="shift_id"><?php echo  lang('shift_id') ?> <span class="text-danger"></span></label>
                    <select name="shift_id" class="form-control select" id="shift_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($shift_masters as $row) { ?>
                        <option value="<?php echo $row->id; ?>" <?= $row->id == $roster_detail->shift_id ? 'Selected' : '' ?>><?php echo $row->name;?></option>
                    <?php } ?>
                    </select>
		      	</div> 			
		   		
		   </div>
		   
           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_roster_time"><?php echo  lang('entry_roster_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_roster_time" value="<?= date('Y-m-d', strtotime($roster_detail->entry_roster_time)) . 'T' . date('H:i:s', strtotime($roster_detail->entry_roster_time)) ?>" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="exit_roster_time"><?php echo  lang('exit_roster_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="exit_roster_time" value="<?= date('Y-m-d', strtotime($roster_detail->exit_roster_time)) . 'T' . date('H:i:s', strtotime($roster_detail->exit_roster_time)) ?>" class="form-control" autocomplete="off">
		        </div> 
		   </div>
		   
           
           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_roster_time_tol"><?php echo  lang('entry_roster_time_tol') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_roster_time_tol" value="<?= date('Y-m-d', strtotime($roster_detail->entry_roster_time_tol)) . 'T' . date('H:i:s', strtotime($roster_detail->entry_roster_time_tol)) ?>" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="half_day_time"><?php echo  lang('half_day_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="half_day_time" value="<?= $roster_detail->half_day_time ? date('Y-m-d', strtotime($roster_detail->half_day_time)) . 'T' . date('H:i:s', strtotime($roster_detail->half_day_time)) : '' ?>" class="form-control" autocomplete="off">
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="entry_time"><?php echo  lang('entry_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="entry_time" value="<?= $roster_detail->entry_time ? date('Y-m-d', strtotime($roster_detail->entry_time)) . 'T' . date('H:i:s', strtotime($roster_detail->entry_time)) : ''?>" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="exit_time"><?php echo  lang('exit_time') ?> <span class="text-danger"></span></label>
					<input type="datetime-local" name="exit_time" value="<?= $roster_detail->exit_time ? date('Y-m-d', strtotime($roster_detail->exit_time)) . 'T' . date('H:i:s', strtotime($roster_detail->exit_time)) : '' ?>" class="form-control" autocomplete="off">
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="present_status"><?php echo  lang('present_status') ?> <span class="text-danger"></span></label>
                    <select name="present_status" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="Present" <?= $roster_detail->present_status == 'Present' ? 'Selected' : '' ?>><?php echo 'Present'?></option>
						<option value="Absent" <?= $roster_detail->present_status == 'Absent' ? 'Selected' : '' ?>><?php echo 'Absent'?></option>
                    </select>
		        </div>
				<div class="col-md-6">
					<label for="leave_status"><?php echo  lang('leave_status') ?> <span class="text-danger"></span></label>
                    <select name="leave_status" class="form-control select" id="">
						<option value=""><?php echo 'Select'?></option>
						<option value="Annual Leave" <?= $roster_detail->leave_status == 'Annual Leave' ? 'Selected' : '' ?>><?php echo 'Annual Leave'?></option>
						<option value="Casual Leave" <?= $roster_detail->leave_status == 'Casual Leave' ? 'Selected' : '' ?>><?php echo 'Casual Leave'?></option>
                    </select>
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="petrol_log"><?php echo  lang('petrol_log') ?> <span class="text-danger"></span></label>
					<input type="text" name="petrol_log" value="<?= $roster_detail->petrol_log ? $roster_detail->petrol_log : '' ?>" class="form-control" autocomplete="off">
		        </div>
				<div class="col-md-6">
					<label for="overtime_enable"><?php echo  lang('is_overtime') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="overtime_enable" <?= $roster_detail->overtime_enable == 1 ? 'Checked' : '' ?>>
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="overtime_minute"><?php echo  lang('overtime_minute') ?> <span class="text-danger"></span></label>
					<input type="text" name="overtime_minute" value="<?= $roster_detail->overtime_minute ?>" class="form-control">
		        </div>
				<div class="col-md-6">
					<label for="overtime_after"><?php echo  lang('overtime_after') ?> <span class="text-danger"></span></label> <br>
					<input type="datetime-local" name="overtime_after" value="<?= $roster_detail->overtime_after ? date('Y-m-d', strtotime($roster_detail->overtime_after)) . 'T' . date('H:i:s', strtotime($roster_detail->overtime_after)) : '' ?>" class="form-control" autocomplete="off">
		        </div> 
		   </div>
           <div class="form-group row">
                <div class="col-md-6">
					<label for="duty_type"><?php echo  lang('duty_type') ?> <span class="text-danger"></span></label>
                    <select name="duty_type" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="Regular" <?= $roster_detail->duty_type == 'Regular' ? 'Selected' : '' ?>><?php echo 'Regular'?></option>
						<option value="Paid Overtime" <?= $roster_detail->duty_type == 'Paid Overtime' ? 'Selected' : '' ?>><?php echo 'Paid Overtime'?></option>
						<option value="Unpaid Overtime" <?= $roster_detail->duty_type == 'Unpaid Overtime' ? 'Selected' : '' ?>><?php echo 'Unpaid Overtime'?></option>
                    </select>
		        </div>
				<div class="col-md-6">
					<label for="day_duration"><?php echo  lang('day_duration') ?> <span class="text-danger"></span></label>
                    <select name="day_duration" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="DayShift" <?= $roster_detail->day_duration == 'DayShift' ? 'Selected' : '' ?>><?php echo 'Day Shift'; ?></option>
						<option value="NightShift" <?= $roster_detail->day_duration == 'NightShift' ? 'Selected' : '' ?>><?php echo 'Night Shift'; ?></option>
                    </select>
		        </div> 
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
					<label for="is_weekend"><?php echo  lang('is_weekend') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" <?= $roster_detail->is_weekend == 1 ? 'Checked' : '' ?> name="is_weekend">
		        </div> 
                <div class="col-md-6">
					<label for="holiday_caption"><?php echo  lang('holiday_caption') ?> <span class="text-danger"></span></label>
					<input type="text" name="holiday_caption" value="<?= $roster_detail->holiday_caption ?>"  class="form-control">
		        </div>
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
					<label for="is_consider"><?php echo  lang('is_consider') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_consider" <?= $roster_detail->is_consider == 1 ? 'Checked' : '' ?> class="form-control">
		        </div> 
                <div class="col-md-6">
					<label for="pay_type"><?php echo  lang('pay_type') ?> <span class="text-danger"></span></label>
					<select name="pay_type" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="In Salary" <?= $roster_detail->pay_type == 'In Salary' ? 'Selected' : '' ?>><?php echo 'In Salary';?></option>
						<option value="Frequently" <?= $roster_detail->pay_type == 'Frequently' ? 'Selected' : '' ?>><?php echo 'Frequently';?></option>
                    </select>		        
				</div>
		   </div>
		   <div class="form-group row">
		   		<div class="col-md-6">
					<label for="is_edited"><?php echo  lang('is_edited') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_edited" <?= $roster_detail->is_edited == 1 ? 'Checked' : '' ?> class="form-control">
		        </div> 
                <div class="col-md-6">
					<label for="is_approved"><?php echo  lang('is_approved') ?> <span class="text-danger"></span></label> <br>
					<input type="checkbox" name="is_approved" <?= $roster_detail->is_approved == 1 ? 'Checked' : '' ?> class="form-control">
		        </div> 
		   </div>
		   <div class="form-group row">
                <div class="col-md-6">
					<label for="salary_status"><?php echo  lang('salary_status') ?> <span class="text-danger"></span></label>
                    <select name="salary_status" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="Regular" <?= $roster_detail->salary_status == 'Regular' ? 'Selected' : '' ?>><?php echo 'Regular'?></option>
						<option value="Unauthorized Leave" <?= $roster_detail->salary_status == 'Unauthorized Leave' ? 'Selected' : '' ?>><?php echo 'Unauthorized Leave'?></option>
						<option value="Late Present"><?php echo 'Late Present'?></option>
						<option value="Half Day Leave"><?php echo 'Half Day Leave'?></option>
						<option value="Early Leave"><?php echo 'Early Leave'?></option>
						<option value="In Leave"><?php echo 'In Leave'?></option>
						<option value="In Tour"><?php echo 'In Tour'?></option>
                    </select>
		        </div>
				<div class="col-md-6">
					<label for="confirm_status"><?php echo  lang('confirm_status') ?> <span class="text-danger"></span></label>
                    <select name="confirm_status" class="form-control select" id="">
						<option value=""><?php echo 'Select';?></option>
						<option value="YES" <?= $roster_detail->confirm_status == 'YES' ? 'Selected' : '' ?>><?php echo 'YES';?></option>
						<option value="NO" <?= $roster_detail->confirm_status == 'NO' ? 'Selected' : '' ?>><?php echo 'NO';?></option>
                    </select>
		        </div> 
		   </div>
           
		   <div class="form-group">
		        <?php echo form_submit('add_shift_master', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#edit_roster_details').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_roster_details/edit_roster_details",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

  $(document).on('change', '#department', function(){
        var deptId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_roster/get_dept_employees',
            method: 'POST',
            type: 'text',
            data: { dept_id: deptId },
            success: function(data) {
                $('#employee_id').remove();
                $('#emp_area').html(data);
            }
        });
  });

});
</script>

<?php
    echo "<div class=\"\"><a href='" . admin_url('hrm_roster_details/add_roster_details_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_roster_details') . "'><i class=\"fa fa-plus\"></i> Add Roster Details</a></div>";
?>
    <br>
    <div class="search_area">
            <div class="row">
                <div class="col-md-3">
					   <input name="" type="text" id="salaryDate" class="date form-control" value="" placeholder="dd-mm-yyyy" autocomplete="off">
				</div>
                <div class="col-md-3">
                    <input type="text" id="empid" placeholder="Type employee name..." class="form-control">
                    
                </div>
                <div class="col-md-3">
                    <input type="text" id="rosterId" placeholder="Type roster name..." class="form-control">
                </div>
			</div>
            <br>
            <div class="row">
               <div class="col-md-3">
                    <input type="text" id="period_id" placeholder="Type period name..." class="form-control">
                     
				</div>
				<div class="col-md-3">
					<a id="btnSearch" type="submit" class="btn btn-primary">Search</a>
				</div>
			</div>
    </div>
<br>

<div class="table-responsive">
 <table id="table_roster_details" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('card_no');?></th>
          <th><?php echo lang('roster_date');?></th>
          <th><?php echo lang('period');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('entry_time');?></th>
          <th><?php echo lang('exit_time');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>



<script>
   $(document).ready(function () {

    fill_datatable();
    function fill_datatable(empid, rosterId, period_id, salaryDate) {
      oTable = $('#table_roster_details').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_roster_details/getRosterDetails') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               aoData.push({
                   "name": "empid",
                   "value": empid,
               });
               aoData.push({
                   "name": "salaryDate",
                   "value": salaryDate,
               });
               aoData.push({
                   "name": "rosterId",
                   "value": rosterId,
               });
               aoData.push({
                   "name": "period_id",
                   "value": period_id,
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false} ,{"bSortable": false}]
       });
    }

       $("#btnSearch").on("click", function(e){
         e.preventDefault();
         
         var empid = $("#empid").val().split('-');
         var period_id = $("#period_id").val().split('-');
         var rosterId = $("#rosterId").val().split('-');
         var salaryDate = $("#salaryDate").val();
         fill_datatable(empid[0], rosterId, period_id, salaryDate);
      });

        $( "#rosterId").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_roster_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        roster_name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#rosterId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });
        $( "#empid").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });
        $( "#period_id").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_period_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#period_id').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });

     /* $('#rosterId').select2({
                ajax: {
                    url: site.base_url + "hrm_roster_details/get_roster_select",
                    dataType: 'JSON',
                    delay: 250,
                    tags: true,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            guestSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "Roster...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 0,
                templateResult: function (repo) {
                    if (repo.loading) {
                        return repo.text;
                    }
                    return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'>" + repo.imgsrc + "</div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.code + '</span><span class="sc-phn rit">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-capt lft">' + repo.caption + '</span><span class="sc-city rit">' + repo.city + '</span></span><span class="scsm-gp"><span class="sc-eml lft">' + repo.email + '</span><span class="sc-state rit">' + repo.state + "</span></span></div></div>";
                },
                templateSelection: function (repo) {
                    if (repo.id === '') {
                        return "Roster...";
                    }
                    return repo.code + ' ' + repo.caption;
                }
            });*/


    });

   

</script>
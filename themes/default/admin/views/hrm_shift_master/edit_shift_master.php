<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_shift_master'); ?></h4>
    </div>

  <div class="modal-body">
	<p class="error_msg2 text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'editShiftMaster']; ?>
		     <?php echo admin_form_open_multipart("hrm_shift_master/editShiftMaster", $attrib); ?>
		     <input type="hidden" name="shift_master_id" value="<?= $shift_master->id ?>">
        <div class="form-group row">
		   		<div class="col-md-6">
            <label for="name"><?php echo  lang('name') ?> <span class="text-danger"></span></label>
            <input class="form-control" placeholder="<?php echo lang('name');?>" name="name" type="text" value="<?= $shift_master->name ?>">
		      </div> 
          <div class="col-md-6">
                <label for="name_alt"><?php echo  lang('alternate_name') ?> <span class="text-danger"></span></label>
                <input class="form-control" placeholder="<?php echo lang('alternate_name');?>" name="name_alt" type="text" value="<?= $shift_master->name_alt ?>">
		      </div> 
		   </div>
		   <div class="form-group row">
          <div class="col-md-6">
		         <label for="start_time"><?php echo  lang('start_time') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('start_time');?>" name="start_time" type="time" value="<?= $shift_master->start_time ?>" autocomplete="off">
		      </div> 
		      <div class="col-md-6">
		         <label for="end_time"><?php echo  lang('end_time') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('end_time');?>" name="end_time" type="time" value="<?= $shift_master->end_time ?>" autocomplete="off">
		      </div> 
		   </div>
        <div class="form-group row">
		   		<div class="col-md-6">
            <label for="late_tolerance"><?php echo  lang('late_tolerance') ?> <span class="text-danger"></span></label>
            <input class="form-control" placeholder="<?php echo lang('late_tolerance');?>" name="late_tolerance" type="text" value="<?= $shift_master->late_tolerance ?>">
		      </div> 
          <div class="col-md-6">
              <label for="half_day_leave"><?php echo  lang('half_day_leave') ?> <span class="text-danger"></span></label>
              <input class="form-control" placeholder="<?php echo lang('half_day_leave');?>" name="half_day_leave" type="text" value="<?= $shift_master->half_day_leave ?>">
		      </div> 
		   </div>
        <div class="form-group row">
		   		<div class="col-md-6">
            <label for="start_punch"><?php echo  lang('start_punch') ?> <span class="text-danger"></span></label>
            <input class="form-control" placeholder="<?php echo lang('start_punch');?>" name="start_punch" type="text" value="<?= $shift_master->start_punch ?>">
		      </div> 
          <div class="col-md-6">
              <label for="end_punch"><?php echo  lang('end_punch') ?> <span class="text-danger"></span></label>
              <input class="form-control" placeholder="<?php echo lang('end_punch');?>" name="end_punch" type="text" value="<?= $shift_master->end_punch ?>">
		      </div> 
		   </div>
		   <div class="form-group row">
		      <div class="col-md-12">
            <label for="day_duration"><?php echo  lang('day_duration') ?> <span class="text-danger"></span></label>
            <select name="day_duration" class="form-control select">
                <option>Select</option>
                <option value="DayShift" <?= $shift_master->day_duration == 'DayShift' ? 'Selected' : '' ?>><?php echo 'Day Shift'; ?></option>
                <option value="NightShift" <?= $shift_master->day_duration == 'NightShift' ? 'Selected' : '' ?>><?php echo 'Night Shift'; ?></option>
            </select>
		      </div>
		    </div>
        <div class="form-group row">
		      <div class="col-md-12">
            <label for="weekly_holiday"><?php echo  lang('weekly_holiday') ?> <span class="text-danger"></span></label>
            <?php
                $days = array('FRIDAY', 'SATURDAY', 'SUNDAY', 'MONDAY', 'THUESDAY', 'WEDNESDAY', 'THURSDAY');
                $weekly_holidays = explode(',', $shift_master->weekly_holiday);
                ?>
                <?php foreach($days as $day) { ?>
                    &nbsp;&nbsp;<input type="checkbox" name="weekly_holiday[]" value="<?= $day ?>" <?= in_array($day, $weekly_holidays) ? 'Checked' : ''  ?>>  &nbsp;&nbsp;<?= $day ?>
                <?php } ?>
		      </div>
		   </div>
        <div class="form-group row">
		      <div class="col-md-12">
            <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
            <textarea name="remark" class="form-control"> <?= $shift_master->remark ?> </textarea>
		      </div>
		   </div>

        <div class="form-group">
		        <?php echo form_submit('edit_shift_master', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close(); ?>
		  </div>

        <div class="form-group row">
		      <div class="col-md-12">
              <div class="table-responsive" data-pattern="priority-columns">
              <p class="error_msg text-danger" style="font-size: 16px;"> </p>
	                <table class="table table-striped m-md-b-0">
                        <tr>    
                            <th><?php echo lang('day');?></th>
                            <th><?php echo lang('day_duration');?></th>
                            <th><?php echo lang('start_time');?></th>
                            <th><?php echo lang('end_time');?></th>
                            <th><?php echo lang('holiday');?></th>
                            <th><?php echo lang('action');?></th>
                        </tr>
                        <?php foreach($shift_details as $key=>$row) { ?> 
	                      <tr>
	                      	<td><?php echo $row->date_day ?></td>
	                      	<td>
                              <input type="radio" name="<?= 'dayDuration'.$row->id ?>" value="DayShift" <?= $row->day_duration === 'DayShift' ? 'Checked' : '' ?>> Day &nbsp;&nbsp;
                              <input type="radio" name="<?= 'dayDuration'.$row->id ?>" value="NightShift" <?= $row->day_duration === 'NightShift' ? 'Checked' : '' ?>> Night 
                          </td>
	                      	<td>
                              <input class="form-control start_time" id="startTimedtl<?=$row->id?>" type="time" value="<?= $row->start_time ?>" autocomplete="off">
                          </td>
                          <td>
                              <input class="form-control" id="endTimedtl<?=$row->id?>" type="time" value="<?= $row->end_time ?>" autocomplete="off">
                          </td>
	                      	<td>
                              <input type="checkbox" id="isHolidaydtl<?=$row->id?>" <?= $row->is_holiday == 1 ? 'Checked' : '' ?>>
                          </td>
	                      	<td>
                              <button type="button" referenceId="<?=$row->id?>" class="btn btn-primary update_shift_detail">update</button>
                          </td>
	                      </tr>	 
                        <?php } ?>       
	              	</table>   
	              </div>
		      </div>
		   </div>         
		   
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

  $('#editShiftMaster').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "hrm_shift_master/editShiftMaster",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                if (data.isError == false) {
                  $('.error_msg2').text(data.msg);
                } else {
                  $('.error_msg2').text(data.msg);
                }
             }
      });  
  });

  $(".update_shift_detail").click(function (e) {
          var referenceId = $(this).attr('referenceId');
          var dayDuration = $("input[name='dayDuration" + referenceId + "']:checked").val();
          var startTime = $("#startTimedtl" + referenceId).val();
          var endTime = $("#endTimedtl" + referenceId).val();
          var isHolidayChecked = $("#isHolidaydtl" + referenceId).is(":checked");
          if(isHolidayChecked) {
            var isHoliday = 1;
          } else {
            var isHoliday = 0;
          }
          jQuery.ajax({
              type: 'POST',
              dataType: 'JSON',
              data: {
                  id: referenceId,
                  dayDuration: dayDuration,
                  startTime: startTime,
                  endTime: endTime,
                  isHoliday: isHoliday
              },
              url: site.base_url + "hrm_shift_master/edit_shift_detail",
              success: function (data, textStatus) {
                
                  if (data.isError == false) {
                    $('.error_msg').text(data.msg);
                     // toastr.success(data.message, 'Success...');
                  } else {
                    $('.error_msg').text(data.msg);
                     // toastr.warning(data.message, 'Failed...');
                  }
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                 // serverErrorToast(errorThrown);
              }
          });
          e.preventDefault();
      });

});
</script>
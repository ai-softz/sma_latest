<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_notification'); ?></h4>
        </div>
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form'];
        echo admin_form_open('notifications/add', $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('from', 'from_date'); ?>
                        <div class="controls">
                            <?php echo form_input('from_date', '', 'class="form-control datetime" id="from_date" required="required"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('till', 'to_date'); ?>
                        <div class="controls">
                            <?php echo form_input('to_date', '', 'class="form-control datetime" id="to_date" required="required"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?php echo lang('title', 'title'); ?>
                <div class="controls">
                    <?php echo form_input('title', '', 'class="form-control" id="title" required="required"'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo lang('comment', 'comment'); ?>
                <div class="controls">
                    <?php echo form_textarea($comment); ?>
                </div>
            </div>

           <div class="form-group">
                <input type="radio" class="checkbox" name="scope" value="1" id="customer"><label for="customer"
                                                                                                 class="padding05"><?= lang('for_customers_only') ?></label>
                <input type="radio" class="checkbox" name="scope" value="2" id="staff_scope"><label for="staff"
                                                                                              class="padding05"><?= lang('for_staff_only') ?></label>
                <input type="radio" class="checkbox" name="scope" value="3" id="both" checked="checked"><label
                    for="both" class="padding05"><?= lang('for_both') ?></label>
            </div>
            <div id="select_staff_user" style="display: none;">
            <div class="row" >
                <div class="col-md-8">
                    <div class="form-group ">
                       <input type="radio" name="notify_user" value="all_staff"> <label> All Staffs </label>
                    </div>
                </div>
                
            </div>    

            <div class="row" >
                <div class="col-md-6">
                    <input type="radio" id="specific_user" name="notify_user"> <label> Specific Users</label>
                </div>
            </div>
           
            <div class="row" id="select_user" style="display: none;">
                 <br>
                <div class="col-md-4">
                    <label style="margin-left: 25px;">Select User</label>
                </div>
                <div class="col-md-8">
                    <div class="form-group ">
                       <select name="user_ids[]" class="selectpicker" multiple data-live-search="true" style="width: 100%">
                         <?php foreach($staffs as $staff) { ?> 
                          <option value="<?= $staff['id'] ?>"><?= $staff['first_name'] ?></option>
                          <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <br>

            <div class="row" >
                <div class="col-md-6">
                    <input type="radio" id="warehouse" name="notify_user"> <label>Warehouse</label>
                </div>
            </div>
             <br>
            <div class="row" id="select_warehouse" style="display: none;">
                <div class="col-md-4">
                    <label style="margin-left: 25px;">Select Warehouse</label>
                </div>
                <div class="col-md-8">
                    <div class="form-group ">
                       <select name="warehouse_id" class="select2" style="width: 100%">
                          <option value="">Select warehouse</option>
                          <?php foreach($warehouses as $warehouse) { ?> 
                          <option value="<?= $warehouse['id'] ?>"><?= $warehouse['name'] ?></option>
                          <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_notification', lang('add_notification'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>

<script>



$(document).ready(function(){

    $(document).on('ifChanged', '#specific_user', function(){
        if($('#specific_user').prop('checked') === true) {
            $('#select_user').show();
        }
        else {
            $('#select_user').hide();
        }
    });
    $(document).on('ifChanged', '#warehouse', function(){
        if($('#warehouse').prop('checked') === true) {
            $('#select_warehouse').show();
        }
        else {
            $('#select_warehouse').hide();
        }
    });

    $(document).on('ifChanged', '#staff_scope', function(){
    if($('#staff_scope').prop('checked') === true) {
        $('#select_staff_user').show();
    } 
    else {
        $('#select_staff_user').hide();
    }
});

});

</script>

<h2>Departments:</h2>
<div class="table-responsive">
 <table id="table_departments" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('#');?></th>
          <th><?php echo lang('department');?></th>
          <th><?php echo lang('warehouse');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b><?php echo lang('add_new');?> </b> <?php echo lang('department');?></h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_department']; ?>
     <?php echo admin_form_open_multipart("employees/add_department", $attrib) ?>
     
    <div class="form-group row">
      <div class="col-md-6">
         <label for="department_name"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('department_name');?>" name="department_name" id="department_name" type="text" value="" required="required">
      </div>
      <div class="col-md-6">
         <label for="secondary_name"><?php echo  lang('caption_alt') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('secondary_name');?>" name="secondary_name" id="secondary_name" type="text" value="" required="required">
      </div>
      
   </div>

   <div class="form-group row">
    <div class="col-md-6">
         <label for="warehouse"><?php echo  lang('warehouse') ?> <span class="text-danger">*</span></label>
          <select name="warehouse_id" id="warehouse" class="form-control select">
        <option value=""><?php echo 'Select';?></option>
        <?php foreach($all_warehouses as $warehouse) { ?>
          <option value="<?php echo $warehouse->id; ?>"><?php echo $warehouse->name; ?></option>
        <?php } ?>
      </select>
      </div>
     <div class="col-md-6">
         <label for="employee_id"><?php echo  lang('department') . lang('head') ?> <span class="text-danger"></span></label>
	       <select name="employee_id" class="form-control select";?>">
	       <option value=""><?php echo 'Select';?></option>
	       <?php foreach($all_employees as $row) { ?>
	  				<option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name.' '.$row->last_name; ?></option>
	  		<?php } ?> 
	  		</select>
    </div>  
     
   </div>
   
   <div class="form-group">
        <?php echo form_submit('add_department', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    
      oTable = $('#table_departments').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('departments/getAllDepartments') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_department').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "departments/add_department",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               //  $('#table_departments').DataTable().ajax.reload();
                  $('#myModal').modal('hide');
				      location.reload();
             }
               
      }); 

  });


</script>
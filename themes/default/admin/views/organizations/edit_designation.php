<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edir_department'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
 
		  <h3><b>Add New</b> Department</h3>
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_designation']; ?>
		     <?php echo admin_form_open_multipart("designations/edit_designation", $attrib) ?>
		     <input type="hidden" name="designation_id" value="<?= $designation->designation_id ?>">
		    <div class="form-group row">
		      <div class="col-md-6">
		         <label for="designation_name"><?php echo  lang('designation_name') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('designation_name');?>" name="designation_name" id="designation_name" type="text" value="<?= $designation->designation_name ?>" required="required">
		      </div>
		      <div class="col-md-6">
		         <label for="secondary_name"><?php echo  lang('secondary_name') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('secondary_name');?>" name="secondary_name" id="secondary_name" type="text" value="<?= $designation->secondary_name ?>" required="required">
		      </div>
		   </div>

		   <div class="form-group row">
		    <div class="col-md-6">
		         <label for="department_id"><?php echo  lang('department') ?> <span class="text-danger">*</span></label>
		          <select name="department_id" id="department_id" class="form-control select">
		        <option value=""><?php echo 'Select';?></option>
		        <?php foreach($all_departments as $row) { ?>
		          <option value="<?php echo $row->department_id; ?>" <?php echo $designation->department_id == $row->department_id ? "Selected" : "" ?>><?php echo $row->department_name; ?></option>
		        <?php } ?>
		      </select>
		      </div>
		     <div class="col-md-6">
		         <label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('description');?>" name="description" id="description" type="text" value="<?= $designation->description ?>">
		    </div>  
     
		   </div>
		   
		   <div class="form-group">
		        <?php echo form_submit('edit_designation', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_designation').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "designations/editDesignation",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
					$('#myModal').modal('hide');
				    location.reload();
                // $('#table_designations').DataTable().ajax.reload();
             }
               
      });  
  });

});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_office_shift'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
 
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_office_shift']; ?>
     <?php echo admin_form_open_multipart("timesheets/edit_office_shift", $attrib) ?>
     <input type="hidden" name="office_shift_id" value="<?= $office_shift->office_shift_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="shift_name"><?php echo  lang('shift_name') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('shift_name');?>" name="shift_name" id="shift_name" type="text" value="<?= $office_shift->shift_name ?>" required="required">
      </div>
      <div class="col-md-6">
         <label for="secondary_name"><?php echo  lang('secondary_name') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('secondary_name');?>" name="secondary_name" id="secondary_name" type="text" value="<?= $office_shift->secondary_name ?>" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="monday_in_time"><?php echo  lang('monday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('monday_in_time');?>" name="monday_in_time" id="monday_in_time" type="text" value="<?= $office_shift->monday_in_time ?>" >
      </div>
     <div class="col-md-6">
         <label for="monday_out_time"><?php echo  lang('monday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('monday_out_time');?>" name="monday_out_time" id="monday_out_time" type="text" value="<?= $office_shift->monday_out_time ?>" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="tuesday_in_time"><?php echo  lang('tuesday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('tuesday_in_time');?>" name="tuesday_in_time" type="text" value="<?= $office_shift->tuesday_in_time ?>" >
      </div>
     <div class="col-md-6">
         <label for="tuesday_out_time"><?php echo  lang('tuesday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('tuesday_out_time');?>" name="tuesday_out_time" type="text" value="<?= $office_shift->tuesday_out_time ?>" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="wednesday_in_time"><?php echo  lang('wednesday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('wednesday_in_time');?>" name="wednesday_in_time" type="text" value="<?= $office_shift->wednesday_in_time ?>" >
      </div>
     <div class="col-md-6">
         <label for="wednesday_out_time"><?php echo  lang('wednesday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('wednesday_out_time');?>" name="wednesday_out_time" type="text" value="<?= $office_shift->wednesday_out_time ?>" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="thursday_in_time"><?php echo  lang('thursday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('thursday_in_time');?>" name="thursday_in_time" type="text" value="<?= $office_shift->thursday_in_time ?>">
      </div>
     <div class="col-md-6">
         <label for="thursday_out_time"><?php echo  lang('thursday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('thursday_out_time');?>" name="thursday_out_time" type="text" value="<?= $office_shift->thursday_out_time ?>" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="friday_in_time"><?php echo  lang('friday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('friday_in_time');?>" name="friday_in_time" type="text" value="<?= $office_shift->friday_in_time ?>" >
      </div>
     <div class="col-md-6">
         <label for="friday_out_time"><?php echo  lang('friday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('friday_out_time');?>" name="friday_out_time" type="text" value="<?= $office_shift->friday_out_time ?>" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="saturday_in_time"><?php echo  lang('saturday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('saturday_in_time');?>" name="saturday_in_time" type="text" value="<?= $office_shift->saturday_in_time ?>" >
      </div>
     <div class="col-md-6">
         <label for="saturday_out_time"><?php echo  lang('saturday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('saturday_out_time');?>" name="saturday_out_time" type="text" value="<?= $office_shift->saturday_out_time ?>" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="sunday_in_time"><?php echo  lang('sunday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('sunday_in_time');?>" name="sunday_in_time" type="text" value="<?= $office_shift->sunday_in_time ?>" >
      </div>
     <div class="col-md-6">
         <label for="sunday_out_time"><?php echo  lang('sunday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('sunday_out_time');?>" name="sunday_out_time" type="text" value="<?= $office_shift->sunday_out_time ?>" >
      </div>
   </div>
   
   <div class="form-group">
        <?php echo form_submit('edit_office_shift', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_office_shift').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "timesheets/editOfficeShift",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('#myModal').modal('hide');
                $('#table_office_shift').DataTable().ajax.reload();
             }
               
      });  
  });

});
</script>
<h2>Office Shifts:</h2>
<div class="table-responsive">
 <table id="table_office_shift" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('day');?></th>
          <th><?php echo lang('monday');?></th>
          <th><?php echo lang('tuesday');?></th>
          <th><?php echo lang('wednesday');?></th>
          <th><?php echo lang('thursday');?></th>
          <th><?php echo lang('friday');?></th>
          <th><?php echo lang('saturday');?></th>
          <th><?php echo lang('sunday');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Office Shift</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_office_shift']; ?>
     <?php echo admin_form_open_multipart("timesheets/add_office_shift", $attrib) ?>
     
    <div class="form-group row">
      <div class="col-md-6">
         <label for="shift_name"><?php echo  lang('shift_name') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('shift_name');?>" name="shift_name" id="shift_name" type="text" value="" required="required">
      </div>
      <div class="col-md-6">
         <label for="secondary_name"><?php echo  lang('secondary_name') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('secondary_name');?>" name="secondary_name" id="secondary_name" type="text" value="" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="monday_in_time"><?php echo  lang('monday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('monday_in_time');?>" name="monday_in_time" id="monday_in_time" type="text" value="" >
      </div>
     <div class="col-md-6">
         <label for="monday_out_time"><?php echo  lang('monday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('monday_out_time');?>" name="monday_out_time" id="monday_out_time" type="text" value="" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="tuesday_in_time"><?php echo  lang('tuesday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('tuesday_in_time');?>" name="tuesday_in_time" type="text" value="" >
      </div>
     <div class="col-md-6">
         <label for="tuesday_out_time"><?php echo  lang('tuesday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('tuesday_out_time');?>" name="tuesday_out_time" type="text" value="" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="wednesday_in_time"><?php echo  lang('wednesday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('wednesday_in_time');?>" name="wednesday_in_time" type="text" value="" >
      </div>
     <div class="col-md-6">
         <label for="wednesday_out_time"><?php echo  lang('wednesday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('wednesday_out_time');?>" name="wednesday_out_time" type="text" value="" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="thursday_in_time"><?php echo  lang('thursday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('thursday_in_time');?>" name="thursday_in_time" type="text" value="">
      </div>
     <div class="col-md-6">
         <label for="thursday_out_time"><?php echo  lang('thursday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('thursday_out_time');?>" name="thursday_out_time" type="text" value="" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="friday_in_time"><?php echo  lang('friday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('friday_in_time');?>" name="friday_in_time" type="text" value="" >
      </div>
     <div class="col-md-6">
         <label for="friday_out_time"><?php echo  lang('friday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('friday_out_time');?>" name="friday_out_time" type="text" value="" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="saturday_in_time"><?php echo  lang('saturday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('saturday_in_time');?>" name="saturday_in_time" type="text" value="" >
      </div>
     <div class="col-md-6">
         <label for="saturday_out_time"><?php echo  lang('saturday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('saturday_out_time');?>" name="saturday_out_time" type="text" value="" >
      </div>
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="sunday_in_time"><?php echo  lang('sunday_in_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('sunday_in_time');?>" name="sunday_in_time" type="text" value="" >
      </div>
     <div class="col-md-6">
         <label for="sunday_out_time"><?php echo  lang('sunday_out_time') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('sunday_out_time');?>" name="sunday_out_time" type="text" value="" >
      </div>
   </div>
   
   <div class="form-group">
        <?php echo form_submit('add_office_shift', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#office_shift_table', function(){
      oTable = $('#table_office_shift').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('timesheets/officeShifts') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false},]
       });
    });
});
     $('#add_office_shift').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "timesheets/add_office_shift",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#table_office_shift').DataTable().ajax.reload();
             }
               
      }); 



  });



    


</script>

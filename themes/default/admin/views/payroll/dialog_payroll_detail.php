<?php

// ___get total hours > worked > employee___			
				//$pay_date = $this->input->get("month_year") ? $this->input->get("month_year") : date('Y-m');
				$emp = $this->payroll_model->read_user_info($user_id);
				$value = $emp[0];
				

				$total_hours_worked = $this->payroll_model->total_hours_worked($value->user_id,$pay_date);
				// echo "<pre>"; print_r($total_hours_worked->result()); die;
				$hrs_old_int1 = 0;
				$pcount = 0;
				$Trest = 0;
				$total_time_rs = 0;
				$hrs_old_int_res1 = 0;
				foreach ($total_hours_worked->result() as $hour_work){
					// total work			
					$clock_in =  new DateTime($hour_work->clock_in);
					$clock_out =  new DateTime($hour_work->clock_out);
					$interval_late = $clock_in->diff($clock_out);
					$hours_r  = $interval_late->format('%h');
					$minutes_r = $interval_late->format('%i');			
					$total_time = $hours_r .":".$minutes_r.":".'00';
					
					$str_time = $total_time;
					$str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
					sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
					$hrs_old_seconds = $hours * 3600 + $minutes * 60 + $seconds;
					$hrs_old_int1 += $hrs_old_seconds;
					
					$pcount = gmdate("H", $hrs_old_int1);			
				}
				$pcount = $pcount;
				// end ___get total hours > worked > employee___

				// 1: salary type
				if($value->wages_type==1){
					$wages_type = $this->lang->line('payroll_basic_salary');
					$basic_salary = $value->basic_salary;
					$p_class = 'emo_monthly_pay';
					$view_p_class = 'payroll_template_modal';
				} else if($value->wages_type==2){
					$wages_type = lang('employee_daily_wages');
					if($pcount > 0){
						$basic_salary = $pcount * $value->basic_salary;
					} else {
						$basic_salary = $pcount;
					}
					$p_class = 'emo_hourly_pay';
					$view_p_class = 'hourlywages_template_modal';
				} else {
					$wages_type = lang('payroll_basic_salary');
					$basic_salary = $value->basic_salary;
					$p_class = 'emo_monthly_pay';
					$view_p_class = 'payroll_template_modal';
					
				}	

				// 2: all allowances
				$salary_allowances = $this->payroll_model->read_salary_allowances($value->user_id);
				$count_allowances = $this->payroll_model->count_employee_allowances($value->user_id);
				$allowance_amount = 0;
				if($count_allowances > 0) {
					foreach($salary_allowances as $sl_allowances){
						if($this->Settings->is_half_monthly==1){
					  	 if($this->Settings->half_deduct_month==2){
							 $eallowance_amount = $sl_allowances->allowance_amount/2;
						 } else {
							 $eallowance_amount = $sl_allowances->allowance_amount;
						 }
						  $allowance_amount += $eallowance_amount;
                      } else {
						  //$eallowance_amount = $sl_allowances->allowance_amount;
						  if($sl_allowances->is_allowance_taxable == 1) {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount;
							  } else {
								  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
							  }
							 $allowance_amount -= $iallowance_amount; 
						  } else if($sl_allowances->is_allowance_taxable == 2) {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount / 2;
							  } else {
								  $iallowance_amount = ($basic_salary / 100) / 2 * $sl_allowances->allowance_amount;
							  }
							 $allowance_amount -= $iallowance_amount; 
						  } else {
							  if($sl_allowances->amount_option == 0) {
								  $iallowance_amount = $sl_allowances->allowance_amount;
							  } else {
								  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
							  }
							  $allowance_amount += $iallowance_amount;
						  }
                      }
					 
					}
				} else {
					$allowance_amount = 0;
				}

				// 3: all loan/deductions
				$salary_loan_deduction = $this->payroll_model->read_salary_loan_deductions($value->user_id);
				$count_loan_deduction = $this->payroll_model->count_employee_deductions($value->user_id);
				$loan_de_amount = 0;
				if($count_loan_deduction > 0) {
					foreach($salary_loan_deduction as $sl_salary_loan_deduction){
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount/2;
						  } else {
							  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
						  }
                      } else {
						  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
                      }
					  $loan_de_amount += $er_loan;
					}
				} else {
					$loan_de_amount = 0;
				}

				// commissions
				$count_commissions = $this->payroll_model->count_employee_commissions($value->user_id);
				$commissions = $this->payroll_model->set_employee_commissions($value->user_id);
				$commissions_amount = 0;
				if($count_commissions > 0) {
					foreach($commissions->result() as $sl_salary_commissions){
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $ecommissions_amount = $sl_salary_commissions->commission_amount/2;
						  } else {
							  $ecommissions_amount = $sl_salary_commissions->commission_amount;
						  }
						  $commissions_amount += $ecommissions_amount;
                      } else {
						 // $ecommissions_amount = $sl_salary_commissions->commission_amount;
						 if($sl_salary_commissions->is_commission_taxable == 1) {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount;
							  } else {
								  $ecommissions_amount = $basic_salary / 100 * $sl_salary_commissions->commission_amount;
							  }
							 $commissions_amount -= $ecommissions_amount; 
						  } else if($sl_salary_commissions->is_commission_taxable == 2) {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount / 2;
							  } else {
								  $ecommissions_amount = ($basic_salary / 100) / 2 * $sl_salary_commissions->commission_amount;
							  }
							 $commissions_amount -= $ecommissions_amount; 
						  } else {
							  if($sl_salary_commissions->amount_option == 0) {
								  $ecommissions_amount = $sl_salary_commissions->commission_amount;
							  } else {
								  $ecommissions_amount = $basic_salary / 100 * $sl_salary_commissions->commission_amount;
							  }
							  $commissions_amount += $ecommissions_amount;
						  }
                      }
					  
					}
				} else {
					$commissions_amount = 0;
				}

				// otherpayments
				$count_other_payments = $this->payroll_model->count_employee_other_payments($value->user_id);
				$other_payments = $this->payroll_model->set_employee_other_payments($value->user_id);
				$other_payments_amount = 0;
				if($count_other_payments > 0) {
					foreach($other_payments->result() as $sl_other_payments) {
						if($this->Settings->is_half_monthly==1){
					  	  if($this->Settings->half_deduct_month==2){
							  $epayments_amount = $sl_other_payments->payments_amount/2;
						  } else {
							  $epayments_amount = $sl_other_payments->payments_amount;
						  }
						  $other_payments_amount += $epayments_amount;
                      } else {
						 // $epayments_amount = $sl_other_payments->payments_amount;
						  if($sl_other_payments->is_otherpayment_taxable == 1) {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount;
							  } else {
								  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
							  }
							 $other_payments_amount -= $epayments_amount; 
						  } else if($sl_other_payments->is_otherpayment_taxable == 2) {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount / 2;
							  } else {
								  $epayments_amount = ($basic_salary / 100) / 2 * $sl_other_payments->payments_amount;
							  }
							 $other_payments_amount -= $epayments_amount; 
						  } else {
							  if($sl_other_payments->amount_option == 0) {
								  $epayments_amount = $sl_other_payments->payments_amount;
							  } else {
								  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
							  }
							  $other_payments_amount += $epayments_amount;
						  }
                      }
					  
					}
				} else {
					$other_payments_amount = 0;
				}

				// statutory_deductions
				$count_statutory_deductions = $this->payroll_model->count_employee_statutory_deductions($value->user_id);
				$statutory_deductions = $this->payroll_model->set_employee_statutory_deductions($value->user_id);
				$statutory_deductions_amount = 0;
				if($count_statutory_deductions > 0) {
					foreach($statutory_deductions->result() as $sl_salary_statutory_deductions){
						if($this->Settings->is_half_monthly==1){
							  if($this->Settings->half_deduct_month==2){
								  $single_sd = $sl_salary_statutory_deductions->deduction_amount/2;
							  } else {
								   $single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  }
							  $statutory_deductions_amount += $single_sd;
						  } else {
							  //$single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  if($sl_salary_statutory_deductions->statutory_options == 0) {
								  $single_sd = $sl_salary_statutory_deductions->deduction_amount;
							  } else {
								  $single_sd = $basic_salary / 100 * $sl_salary_statutory_deductions->deduction_amount;
							  }
							  $statutory_deductions_amount += $single_sd;
						  }
						  
					}
				} else {
					$statutory_deductions_amount = 0;
				}	

				// 5: overtime
				$salary_overtime = $this->payroll_model->read_salary_overtime($value->user_id);
				$count_overtime = $this->payroll_model->count_employee_overtime($value->user_id);
				$overtime_amount = 0;
				if($count_overtime > 0) {
					foreach($salary_overtime as $sl_overtime){
						if($this->Settings->is_half_monthly==1){
							if($this->Settings->half_deduct_month==2){
								$eovertime_hours = $sl_overtime->overtime_hours/2;
								$eovertime_rate = $sl_overtime->overtime_rate/2;
							} else {
								$eovertime_hours = $sl_overtime->overtime_hours;
								$eovertime_rate = $sl_overtime->overtime_rate;
							}
						} else {
							$eovertime_hours = $sl_overtime->overtime_hours;
							$eovertime_rate = $sl_overtime->overtime_rate;
						}
						$overtime_total = $eovertime_hours * $eovertime_rate;
						//$overtime_total = $sl_overtime->overtime_hours * $sl_overtime->overtime_rate;
						$overtime_amount += $overtime_total;
					}
				} else {
					$overtime_amount = 0;
				}

	
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo 'Employee Salary Details
'; ?></h4>
        </div>

        <div class="modal-body">
        	
        	<div class="container">
        	<div class="row">
		<h2>Payment Details</h2>
		<div class="col-md-12 box" style="background-color: #fff;">
			<div class="payment_detail">
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#basic_salary" aria-expanded="false"> <span>Monthly Payslip</span> </a></h2>
					<div id="basic_salary" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Monthly Payslip</th> <td><?= $basic_salary ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_allowances" aria-expanded="false"> <span>Allowance</span> </a></h2>
					<div id="set_allowances" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Allowances</th> <td><?= $allowance_amount ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_commission" aria-expanded="false"> <span>Commission</span> </a></h2>
					<div id="set_commission" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Commissions</th> <td><?= $commissions_amount ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_loan" aria-expanded="false"> <span>Loan</span> </a></h2>
					<div id="set_loan" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Loan</th> <td><?= $loan_de_amount ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_statutory_deduc" aria-expanded="false"> <span>Statutory Deductions</span> </a></h2>
					<div id="set_statutory_deduc" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Statutory Deductions</th> <td><?= $statutory_deductions_amount ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_other_payment" aria-expanded="false"> <span>Other Payment</span> </a></h2>
					<div id="set_other_payment" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Other Payment</th> <td><?= $other_payments_amount ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_over_time" aria-expanded="false"> <span>Over Time</span> </a></h2>
					<div id="set_over_time" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Over Time</th> <td><?= $other_payments_amount ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			
			</div>
			
		</div>
	</div>
	</div>
		</div>
		
           
		
	</div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-12">
	<div class="panel panel-bd">
		<div class="panel-body">
			<div class="panel-heading" style="border-bottom: 1px solid #e4e5e7; margin-bottom: 5px;">
				<div class="panel-title">
					<h2>Payroll:</h2>
                </div>
				
				<div class="row">
					<div class="col-md-6">
						<select name="emp_id" class="select" id="empId" style="width:100%"> 
							<option value="">Select Employee</option>
							<?php foreach($all_employees as $row) { ?>
								<option value="<?= $row->user_id ?>"><?= $row->first_name.' '.$row->last_name; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-3">
					<input name="" type="month" id="salaryMonth" class="form-control" value="<?= date('Y-m'); ?>">
					</div>
					<div class="col-md-3">
						<a id="btnSearch" type="submit" class="btn btn-primary">Search</a>
					</div>
				</div>
				<br>
				
				<div class="row">
				<?php $attrib = [ 'role' => 'form', 'id' => 'bulkPayment']; ?>
				   <?php echo admin_form_open_multipart("", $attrib) ?>
					<div class="col-md-3">
						<select name="warehouse_id" class="select" id="warehouseId" style="width:100%"> 
							<option value="0">All</option>
							<?php foreach($all_warehouses as $row) { ?>
								<option value="<?php echo $row->id ?>"><?php echo $row->name; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-3" id="deptSelect"> 
						<select name="department_id" class="form-control select" id="departmentId" style="width:100%"> 
							<option value="0">All</option>
						</select>		
					</div>
					<div class="col-md-3">
						<input name="pay_date" type="month" value="<?= date('Y-m'); ?>" id="pay_date" class="form-control date">
					</div>
					<div class="col-md-3">
						<?php echo form_submit('bulkPayment', lang('bulk_payment'), 'class="btn btn-primary"'); ?>
					</div>
					
				</div>
				
	        </div>
	         <input type="hidden" id="month_year" class="date" name="" value="<?php echo date('Y-m');?>" >
			
			 <div class="table-responsive">
			 <table id="table_payslips" class="table table-bordered table-hover table-striped">
			    <thead>
			       <tr>
			          <th><?php echo lang('the_number_sign');?></th>
			          <th><?php echo lang('name');?></th>
			          <th><?php echo lang('name');?></th>
			          <th><?php echo lang('payroll_type');?></th>
			          <th><?php echo lang('salary');?></th>
			          <th><?php echo lang('net_salary');?></th>
			          <th><?php echo lang('status');?></th>
			          <th style="width:100px;"><?= lang('actions'); ?></th>
			       </tr>
			    </thead>
			    <tbody>
			    </tbody>
			 </table>
			</div>
		</div>
	</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		fill_datatable();
		function fill_datatable(empid='', salaryMonth='') {
		var month_year = $('#month_year').val();
		oTable = $('#table_payslips').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 
		   'bServerSide': true,
           'sAjaxSource': '<?= admin_url('payroll/payslip_list') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
				$('td:eq(6)', nRow).html(paymentStatus(nRow, aData));
				return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>",
               });
               aoData.push({
                   "name": "empid",
                   "value": empid,
               });
               aoData.push({
                   "name": "salaryMonth",
                   "value": salaryMonth,
               });
			//    console.log(aoData);
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, null, null, {"bSortable": false}, null, null, {"bSortable": false}, {"bSortable": false}]
       });
	}
	$("#btnSearch").on("click", function(e){
		e.preventDefault();
		var empid = $("#empId").val();
		var salaryMonth = $("#salaryMonth").val();
		console.log(empid);
		fill_datatable(empid, salaryMonth);
	});

	$(document).on('change', '#warehouseId', function(){
		var warehoue_id = $(this).val();
		$.ajax({
			url: site.base_url + 'payroll/getDeptSelect',
			method: 'POST',
			dataType: 'text',
			data: {warehoue_id: warehoue_id},
			
			success: function(data){
				$('#departmentId').remove();
				$('#deptSelect').html(data);
			}
		});
	});

	$('#bulkPayment').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "payroll/bulkPayment",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#myModal').modal('hide');
             }
               
      });  
  });
});
	
	function paymentStatus(nRow, aData) 
	{
		var status = '';
		if(aData[6] == 0) {
			status = 'Unpaid';
		} else if(aData[6] == 1) {
			status = 'Partially Paid';
		}
		else if(aData[6] == 2) {
			status = 'Paid';
		}
		return status;
	}
</script>


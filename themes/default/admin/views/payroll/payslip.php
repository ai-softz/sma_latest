<style>
.box {
	border: 1px solid #dbdee0;
    margin-bottom: 15px;
}
.payment_detail {
	display: flex; 
	flex-direction: column;
}
.payment_detail a:visited {
	text-decoration: none;
}
.payment_detail a:hover {
	text-decoration: none;
}
.payment_detail a:active {
	text-decoration: none;
}
</style>
<div class="container">
<h2 class="fs-3"> Employee Payslip</h2>
	<div class="row">
		<div class="col-md-9 box" style="background-color: #fff">
			<div class="table-responsive" data-pattern="priority-columns">
				<h3> <b> Payslip - </b> <?= $payslip_info['payment_date'] ?></h3>
	            <table class="table table-striped m-md-b-0">
	            	
                    <tbody>
                      	<tr>
	                      	<td><strong><?= lang('employee_id').': '?></strong> <?= $payslip_info['employee_id'] ?></td>
	                      	<td><strong><?= lang('employee_name').': '; ?></strong><?= $payslip_info['first_name']. ' '. $payslip_info['last_name'] ?></td>
	                      	<td><strong><?= lang('payslip_no').': '; ?></strong><?= $payslip_info['make_payment_id'] ?></td>
                      	</tr>
                      	<tr>
	                      	<td><strong><?= lang('phone').': '?></strong> <?= $payslip_info['contact_no'] ?></td>
	                      	<td><strong><?= lang('joining_date').': '; ?></strong><?= $payslip_info['date_of_joining'] ?></td>
	                      	<td></td>
                      	</tr>
                      	<tr>
	                      	<td><strong><?= lang('department').': '?></strong> <?= $payslip_info['department_name'] ?></td>
	                      	<td><strong><?= lang('Designation').': '; ?></strong><?= $payslip_info['designation_name'] ?></td>
	                      	<td></td>
                      	</tr>
                      	
	                </tbody>
	            </table>
	          </div>
		</div>
		<div class="col-md-3 box" style="background: #fff">
			<h3 style="margin-bottom: 30px"> Update Status</h3>
        			<div class="table-responsive" data-pattern="priority-columns">
        			<?php $attrib = [ 'role' => 'form', 'id' => 'change_payroll_status']; ?>	
        			<?php echo admin_form_open_multipart('payroll/change_payroll_status', $attrib) ?>
					<input type="hidden" name="payslip_id" value="<?= $payslip_info['make_payment_id'] ?>">	 
        			<div class="form-group row">
      					<div class="col-md-12">
	                  		<label for="status"><?php echo  lang('status') ?> <span class="text-danger"></span></label>
                      		<select name="status" class="form-control">
                      			<option value="0" <?= $payslip_info['approval_status'] == 0 ? "Selected" : "" ?>>Not Approved</option>
                      			<option value="1" <?= $payslip_info['approval_status'] == 1 ? "Selected" : "" ?>>First Level Approval</option>
                      			<option value="2" <?= $payslip_info['approval_status'] == 2 ? "Selected" : "" ?>>Final Approval</option>
                      			
                      		</select> 
                      		
                      	</div>
                       </div>
	                	<div class="form-group">
		              	  <?php 
		              	  	echo form_submit('change_payroll_status', lang('save'), 'class="btn btn-primary"');
		              	  	echo form_close(); 
		              	  ?>
	              		</div>
	                </div>
		</div>
		
	</div>

	<div class="row">
		<h2>Payment Details</h2>
		<div class="col-md-6 box" style="background-color: #fff;">
			<div class="payment_detail">
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#basic_salary" aria-expanded="false"> <span>Monthly Payslip</span> </a></h2>
					<div id="basic_salary" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Monthly Payslip</th> <td><?= $payslip_info['basic_salary'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_allowances" aria-expanded="false"> <span>Allowance</span> </a></h2>
					<div id="set_allowances" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Allowances</th> <td><?= $payslip_info['total_allowances'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_commission" aria-expanded="false"> <span>Commission</span> </a></h2>
					<div id="set_commission" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Commissions</th> <td><?= $payslip_info['total_commissions'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_loan" aria-expanded="false"> <span>Loan</span> </a></h2>
					<div id="set_loan" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Loan</th> <td><?= $payslip_info['total_loan'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_statutory_deduc" aria-expanded="false"> <span>Statutory Deductions</span> </a></h2>
					<div id="set_statutory_deduc" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Statutory Deductions</th> <td><?= $payslip_info['total_statutory_deductions'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_other_payment" aria-expanded="false"> <span>Other Payment</span> </a></h2>
					<div id="set_other_payment" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Other Payment</th> <td><?= $payslip_info['total_other_payments'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_over_time" aria-expanded="false"> <span>Over Time</span> </a></h2>
					<div id="set_over_time" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Over Time</th> <td><?= $payslip_info['total_overtime'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_over_time" aria-expanded="false"> <span>Advance Deducted Salary</span> </a></h2>
					<div id="set_over_time" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Advance Deducted Salary</th> <td><?= $payslip_info['advance_salary_amount'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			</div>
			
		</div>
	</div>
</div>

<script>
$(document).ready(function(){

	$('#change_payroll_status').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "payroll/change_payroll_status",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                
             }
               
      });  
  	});
});
</script>
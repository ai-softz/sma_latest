<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->lang->line('product_price_report'); ?></title>
    <link href="<?= $assets ?>styles/pdf/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets ?>styles/pdf/pdf.css" rel="stylesheet">

    <style type="text/css">
    </style>
    </head>

<body>
<div class="box">
    <div class="box-header">
        
    </div>
    <div class="box-content">
        <div class="row" style="width: 100%; margin-bottom: 20px">
            <div class="col-lg-3"> </div>
            <div class="col-lg-6 text-center" style="width: 50%; margin: auto">
                
                <?= !empty($biller->logo) ? '<img src="' . base_url('assets/uploads/logos/' . $biller->logo) . '" alt="" width="50%">' : ''; ?>
                <hr>
                <h3 style="text-transform:uppercase;"><?=$biller->company && $biller->company != '-' ? $biller->company : $biller->name;?></h3>
                <?php
                echo '<p>' . $biller->address . ' ' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state . ' ' . $biller->country .
                '<br>' . lang('tel') . ': ' . $biller->phone .
                '<br>' . lang('رقم الضريبي') . ': ' . $biller->vat_no;
                echo '</p>';
                ?>
            </div>
            <div class="col-lg-3"> </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            
            <h2 class="blue text-center"><i class="fa-fw fa fa-calendar"></i><?= ('Product Price List'); ?></h2>
            <?php
                            if (!empty($results)) {
                                foreach($results as $key => $products) { 
                        ?>
                            <div class="table-responsive">
                            <?php

                            $category = $this->db->get_where('categories', ['id' => $key])->row();

                            ?>
                            <h2> <?= $category ? $category->name . ':' : '' ?> </h2>
                                <table class="table table-bordered table-striped dfTable reports-table">
                                <thead>
                                    <tr class="bold text-center">
                                        <th> English Name </th>
                                        <th> Name </th>
                                        <th> Price </th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php   
                                foreach ($products as $value) {
                            ?>   
                                <tr>
                                    <td> <?= $value->second_name ?> </td>
                                    <td> <?= $value->name ?> </td>
                                    <td> <?= number_format($value->price, 2) ?> </td>
                                </tr>
                                <?php  
                                } 
                                ?>
                            </tbody>
                        </table>
                        </div>
                        <?php
                            } 
                        } 
                        ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>


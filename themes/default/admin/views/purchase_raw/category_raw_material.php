<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php
    echo "<a href='" . admin_url('purchases_raw/add_categories_raw_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_category') . "'><i class=\"fa fa-plus\"></i> " . lang('add_category') . "</a>";
?>


<h2> <?= lang('category'); ?> :</h2>

    <div class="table-responsive">
        <table id="productsRawTable" class="table table-bordered table-hover table-striped reports-table">
            <thead>
                <tr>
                    <th><?php echo lang('#');?></th>
                    <th><?= lang('name'); ?></th>
                    <th><?= lang('caption_alt'); ?></th>
                   
                    <th style="width:100px;"><?= lang('actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="7" class="dataTables_empty">
                        <?= lang('loading_data_from_server') ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

<script>
    $(document).ready(function () {
        oTable = $('#productsRawTable').dataTable({
            "aaSorting": [[3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('purchases_raw/getRawMaterialCategories') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
 
            },
            "aoColumns": [null,{"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
        });
    });
    
</script>

<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
.purchase_link_raw {
    cursor: pointer;
}
    </style>
<?php
    echo "<a href='" . admin_url('purchases_raw/add_purchase_raw') . "' class='tip btn btn-primary' title='" . lang('add_purchase_raw') . "'><i class=\"fa fa-plus\"></i> " . lang('add_purchase_raw') . "</a>";
?>

<h2> <?= lang('raw_material_purchases'); ?> : </h2>

    <div class="table-responsive">
        <table id="purchaseRawTable" class="table table-bordered table-hover table-striped reports-table">
            <thead>
                <tr>
                    <th><?php echo lang('#');?></th>
                    <th><?= lang('date'); ?></th>
                    <th><?= lang('reference_no'); ?></th>
                    <th><?= lang('status'); ?></th>
                    <th><?= lang('grand_total'); ?></th>
                    <th><?= lang('paid'); ?></th>
                    <th><?= lang('payment_status'); ?></th>
                    <th style="width:100px;"><?= lang('actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="7" class="dataTables_empty">
                        <?= lang('loading_data_from_server') ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

<script>
    $(document).ready(function () {
        oTable = $('#purchaseRawTable').dataTable({
            "aaSorting": [[3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('purchases_raw/getPurchaseRawTable') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "purchase_link_raw";
                return nRow;
            },
            "aoColumns": [null,{"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
        });

        $('body').on('click', '.purchase_link_raw td:not(:first-child,:last-child)', function () {
            $('#myModal').modal({
                remote: site.base_url + 'purchases_raw/modal_view_raw/' + $(this).parent('.purchase_link_raw').attr('id'),
            });
            $('#myModal').modal('show');
            //window.location.href = site.base_url + 'purchases/view/' + $(this).parent('.purchase_link').attr('id');
        });
    });
    
</script>

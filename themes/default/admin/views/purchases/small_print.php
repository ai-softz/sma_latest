<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if (false) {
    ?>
<div class="modal-dialog no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
    <?php
} else {
        ?><!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . ' ' . lang('no') . ' ' . $inv->id; ?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <link href="<?= base_url('assets/custom/pos.css') ?>" rel="stylesheet"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>

    <body>
        <?php
    } ?>
    <div id="wrapper">
        <div id="receiptData" style="height: fit-content">
        <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <?php if ($logo) {
    ?>
                <div class="text-center" style="margin-bottom:20px;">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>"
                         alt="<?= $Settings->site_name; ?>">
                </div>
            <?php
} ?>
            <div class="no-print">
                
            </div>
            <div id="receipt-data">
                <div class="text-center">
                    <?= !empty($biller->logo) ? '<img src="' . base_url('assets/uploads/logos/' . $biller->logo) . '" alt="">' : ''; ?>
                    <hr>
                </div>
                <div class="row bold text-center">
                    <div class="col-xs-12">
                    <p class="bold">
                        <?= lang('date'); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang('ref'); ?>: <?= $inv->reference_no; ?><br>
                        <?= lang('status'); ?>: <?= lang($inv->status); ?><br>
                        <?= lang('payment_status'); ?>: <?= lang($inv->payment_status); ?>
                    </p>
                    </div>
                </div>
                <hr>
                <div class="row bold text-center" style="">
                    <div class="col-xs-6" style="border-right:1px solid #bbb;">
                        <h4 style="margin-top:5px;"><?= $Settings->site_name; ?></h4>
                        <?= $warehouse->name ?>
                        <?php
                        echo $warehouse->address;
                        echo($warehouse->phone ? lang('tel') . ': ' . $warehouse->phone . '<br>' : '') . ($warehouse->email ? lang('email') . ': ' . $warehouse->email : '');
                        ?>
                    </div>
                    <div class="col-xs-6">
                       
                        <h4 style="margin-top:5px;"><?= $supplier->company && $supplier->company != '-' ? $supplier->company : $supplier->name; ?></h4>
                        <?= $supplier->company                              && $supplier->company != '-' ? '' : 'Attn: ' . $supplier->name ?>

                        <?php
                        echo $supplier->address . '<br />' . $supplier->city . ' ' . $supplier->postal_code . ' ' . $supplier->state . '<br />' . $supplier->country;

                        echo '<p>';

                        if ($supplier->vat_no != '-' && $supplier->vat_no != '') {
                            echo '<br>' . lang('vat_no') . ': ' . $supplier->vat_no;
                        }
                        if ($supplier->gst_no != '-' && $supplier->gst_no != '') {
                            echo '<br>' . lang('gst_no') . ': ' . $supplier->gst_no;
                        }
                        if ($supplier->cf1 != '-' && $supplier->cf1 != '') {
                            echo '<br>' . lang('scf1') . ': ' . $supplier->cf1;
                        }
                        if ($supplier->cf2 != '-' && $supplier->cf2 != '') {
                            echo '<br>' . lang('scf2') . ': ' . $supplier->cf2;
                        }
                        if ($supplier->cf3 != '-' && $supplier->cf3 != '') {
                            echo '<br>' . lang('scf3') . ': ' . $supplier->cf3;
                        }
                        if ($supplier->cf4 != '-' && $supplier->cf4 != '') {
                            echo '<br>' . lang('scf4') . ': ' . $supplier->cf4;
                        }
                        if ($supplier->cf5 != '-' && $supplier->cf5 != '') {
                            echo '<br>' . lang('scf5') . ': ' . $supplier->cf5;
                        }
                        if ($supplier->cf6 != '-' && $supplier->cf6 != '') {
                            echo '<br>' . lang('scf6') . ': ' . $supplier->cf6;
                        }

                        echo '</p>';
                        echo lang('tel') . ': ' . $supplier->phone . '<br />' . lang('email') . ': ' . $supplier->email;
                        ?>
                    </div>
                </div>
<hr>
                <div style="clear:both;"></div>
                <table class="table">
                    <thead> 
                        <tr style="border-bottom: 1px solid #000">
                        <th class="no-border"> QTY </th>
                        <th colspan="2" class="no-border"> ITEM </th>
                        <th style="text-align: right;" class="no-border"> Price </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $r           = 1; $category           = 0;$totalQTY           = 0;
                        $tax_summary = [];
                        foreach ($rows as $row) {
                            $totalQTY +=$row->unit_quantity;
                            echo '<tr>';
                            echo '<td class="no-border">'. $row->unit_quantity  . '</td>';
                            
                            echo '<td colspan="2" class="no-border">' . '' . $row->product_name . '</span><br>';
                            if (!empty($row->second_name)) {
                                echo '' . $row->second_name . '</td>';
                            }
                            echo '<td class="no-border text-right">' . $this->sma->formatMoney($row->subtotal) . '</td>';
                            echo '</tr>';
                            $r++;
                        }
                        

                        ?>
                        
                    </tbody>
                    <tfoot>
                    
                    <?php if ($inv->grand_total != $inv->total) {
                        ?>
                        <tr style="border-top: 1px solid #000">
                            <td colspan="3" class="text-right no-border"><?= lang('total'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <?php
                            
                         ?>
                            <td class="text-right no-border"><?= $this->sma->formatMoney($return_purchase ? (($inv->total + $inv->product_tax) + ($return_purchase->total + $return_purchase->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                        </tr>
                        <?php
                        } ?>
                        <tr>
                        <td colspan="3" class="no-border"
                            style="text-align:right; font-weight:bold;"><?= lang('total_amount'); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;" class="no-border"><?= $this->sma->formatMoney($return_purchase ? ($inv->grand_total + $return_purchase->grand_total) : $inv->grand_total); ?></td>
                        </tr>
                        <tr>
                        <td colspan="3"
                            style="text-align:right; font-weight:bold;" class="no-border"><?= lang('paid'); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;" class="no-border"><?= $this->sma->formatMoney($return_purchase ? ($inv->paid + $return_purchase->paid) : $inv->paid); ?></td>
                    </tr>
                    <tr style="border-top: 1px solid #000">
                        <td colspan="3"
                            style="text-align:right; font-weight:bold;" class="no-border"><?= lang('balance'); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;" class="no-border"><?= $this->sma->formatMoney(($return_purchase ? ($inv->grand_total + $return_purchase->grand_total) : $inv->grand_total) - ($return_purchase ? ($inv->paid + $return_purchase->paid) : $inv->paid)); ?></td>
                    </tr>
                        
                    </tfoot>
                </table>
                <?php
                

                ?>

                <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax + $return_sale->product_tax : $inv->product_tax)) : ''; ?>

                
            </div>
<hr>
            <div class="order_barcodes text-center">
                <img src="<?= admin_url('misc/barcode/' . $this->sma->base64url_encode($inv->reference_no) . '/code128/74/0/1'); ?>" alt="<?= $inv->reference_no; ?>" class="bcimg" />
                <?= $this->sma->qrcode('link', urlencode(admin_url('purchases/view/' . $inv->id)), 2); ?>
               
            </div>
            <div style="clear:both;"></div>
        </div>

        <div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
            <hr>
            
            <?php
            
            ?>
            <div style="clear:both;"></div>
        </div>
    </div>

    <?php
   
        ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
        <?php
    
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            
        });
        function printDot() {
            var mywindow = window.open('', 'sma_pos_print', 'height=400,width=250');
            mywindow.document.write('<html><head><title>CashDrawer</title>');
            mywindow.document.write('</head><body>.</body></html>');
            mywindow.print();
            mywindow.close();
            return true;
        }

        // $(window).load(function () {
        //         window.print();
        //         return false;
        //     });
    </script>
    <?php /* include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'pos'.DIRECTORY_SEPARATOR.'remote_printing.php'; */ ?>
    
    <?php
   
    if (false) {
        ?>
    </div>
</div>
</div>
<?php
    } else {
        ?>
</body>
</html>
<?php
    }
?>

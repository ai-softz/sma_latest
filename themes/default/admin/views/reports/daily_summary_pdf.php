<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->lang->line('purchase'); ?></title>
    <link href="<?= $assets ?>styles/pdf/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets ?>styles/pdf/pdf.css" rel="stylesheet">

    <style type="text/css">
    </style>

    </head>

<body>
<div class="box">
   
            <?php
                $month_arr = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            ?>
   
    <div class="box-content">

        <div class="row" style="width: 100%; margin-bottom: 20px">
            <div class="col-lg-3"> </div>
            <div class="col-lg-6 text-center" style="width: 50%; margin: auto">
                
                <?= !empty($biller->logo) ? '<img src="' . base_url('assets/uploads/logos/' . $biller->logo) . '" alt="" width="50%" >' : ''; ?>
               
                <h3 style="text-transform:uppercase; border-top: 1px solid #bbb; padding-top: 5px"><?=$biller->company && $biller->company != '-' ? $biller->company : $biller->name;?></h3>
                
                <?php
                echo '<p>' . $biller->address . ' ' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state . ' ' . $biller->country .
                '<br>' . lang('tel') . ': ' . $biller->phone .
                '<br>' . lang('vat_no') . ': ' . $biller->vat_no;
                echo '</p>';
                ?>
            </div>
            <div class="col-lg-3"> </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            
            <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= ('Daily Sales Summary') . '' . (isset($sel_warehouse) ? $sel_warehouse->name : lang('')) . ' (' . $year . ' - ' . $month_arr[$month+0] .')'; ?></h2>

                <div class="table-responsive">
                    <table width="100%" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr class="bold text-center">
                                <th style="width: 25%"> Days / <?= $month_arr[$month+0] ?> </th>
                                <th style="width: 25%"> Total Without VAT </th>
                                <th style="width: 25%"> Total VAT </th>
                                <th style="width: 25%"> Subtotal </th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        <?php
                            if (!empty($sales)) {

                                $grand_total_sum = 0;
                                $total_sum = 0;
                                $total_tax_sum = 0;

                                foreach ($sales as $value) {  
                            ?>   
                                <tr>
                                    <td> <b> 
                                        <?php
                                        $d = mktime(0, 0 , 0, $month, $value->date, $year);
                                        echo date('d M, Y', $d); 
                                        ?> 
                                    </b> </td>
                                    <td> <?= number_format($value->total, 2) ?> </td>
                                    <td> <?= number_format($value->total_tax, 2) ?> </td>
                                    <td> <?= number_format($value->grand_total, 2) ?> </td>
                                </tr>
                            <?php   
                                    $total_sum += $value->total;
                                    $total_tax_sum += $value->total_tax;
                                    $grand_total_sum += $value->grand_total;
                                } 
                            } 
                            ?>
                        </tbody>
                        <tfoot>
                                <tr>
                                    <th>  Total </th>
                                    <th> <?= $this->sma->formatMoney($total_sum) ?> </th>
                                    <th> <?= $this->sma->formatMoney($total_tax_sum) ?> </th>
                                    <th> <?= $this->sma->formatMoney($grand_total_sum) ?> </th>
                                </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>


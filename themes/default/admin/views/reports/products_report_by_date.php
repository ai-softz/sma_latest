<style>
#product_reports_date table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 95%;
}

#product_reports_date td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 5px;
}
#product_reports_date td:last-child, #product_reports_date th:last-child { display: block; }
</style>

<div class="box">
<div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-barcode"></i><?= lang('products_report'); ?> <?php
            if ($this->input->post('start_date')) {
                echo 'From ' . $this->input->post('start_date') . ' to ' . $this->input->post('end_date');
            }
            ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                        <i class="icon fa fa-file-excel-o"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" id="downloadPdf" class="btn btn-secondary tip" title="<?= lang('download_pdf') ?>"style="margin-top:5px;"> <i class="fa fa-download"> </i> </a> 
                    <!-- <a href="<?= admin_url('reports/products_report_by_date_pdf') ?>" class="tip btn btn-primary" title="<?= lang('download_pdf') ?>">
                                <i class="fa fa-download"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                    </a> -->
                </li>
            </ul>
        </div>
    </div>
</div>

<?php 
// print_r($this->input->post());
?>
<div id="search_form" style="border: 1px solid #ccc; padding: 8px">
<?php echo admin_form_open_multipart("", ['role' => 'form', 'id' => 'get_data']); ?>
            <div class="row">
                <div class="col-md-3">
					   <input type="text" id="startDate" name="startDate" class="date form-control" value="" placeholder="Start Date" autocomplete="off">
				</div>
                <div class="col-md-3">
					   <select name="product_id" id="product_id" class="form-control">
                            <option value=""> Select Product</option>
                            <?php foreach($products as $value) { ?>
                                <option value="<?= $value->id ?>"> <?= $value->name ?> </option>
                            <?php } ?>
                       </select>
				</div>
                <div class="col-md-3">
					   <select name="category_id" id="category_id" class="form-control">
                            <option value=""> Select Category</option>
                            <?php foreach($categories as $value) { ?>
                                <option value="<?= $value->id ?>"> <?= $value->name ?> </option>
                            <?php } ?>
                       </select>
				</div>
                <div class="col-md-3">
                        <select name="created_by" id="created_by" class="form-control">
                            <option value=""> Select User</option>
                            <?php foreach($users as $value) { ?>
                                <option value="<?= $value->id ?>"> <?= $value->first_name. ' '. $value->last_name ?> </option>
                            <?php } ?>
                       </select>
				</div>
			</div>
            <br>
            <div class="row">
                
				<div class="col-md-3">
					<a id="dataSearch" type="submit" class="btn btn-primary">Search</a>
				</div>
                
			</div>
        <?php echo form_close(); ?>
    </div>
<br>

<div id="dataGrid" style="overflow-x:auto; width: 95%;">
</div>


<script>
   $(document).ready(function () {

        $('#search_form').hide();
        $('.toggle_down').click(function () {
            $("#search_form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#search_form").slideUp();
            return false;
        });

        $.ajax({
            type: "POST",
            url: site.base_url + "reports/get_products_report_by_date_default",
            success: function(data) {
                console.log(data);
                $("#dataGrid").html(data);
            }
        });

      $("#dataSearch").on("click", function(){
        $.ajax({
            type: "POST",
            url: site.base_url + "reports/get_products_report_by_date",
            data: $('#get_data').serialize(),
            success: function(data) {
                $("#dataGrid").html(data);
            }
        });
      });
      
      $('#downloadPdf').click(function (event) {
            event.preventDefault();
            
            const startDate = $('#startDate').val();
            const product_id = $('#product_id').val();
            const category_id = $('#category_id').val();
            const created_by = $('#created_by').val();
            
            window.open("<?=admin_url('reports/products_report_by_date_pdf/?start_date=')?>" + startDate + "&product_id=" + product_id + "&category_id=" + category_id + "&created_by=" + created_by , '_blank');
            return false;
            
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            const startDate = $('#startDate').val();
            const product_id = $('#product_id').val();
            const category_id = $('#category_id').val();
            const created_by = $('#created_by').val();
            window.location.href = "<?=admin_url('reports/products_report_by_date_excels?start_date=')?>" + startDate + "&product_id=" + product_id + "&category_id=" + category_id + "&created_by=" + created_by;
            return false;
        });

      $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#empid").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });
        

    });

   

</script>
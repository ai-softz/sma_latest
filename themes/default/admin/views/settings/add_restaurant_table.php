<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_restaurant_table'); ?></h4>
        </div>
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; //print_r($floors);
        echo admin_form_open_multipart('system_settings/add_restaurant_table', $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            

            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang('capacity', 'capacity'); ?>
                <?= form_input('capacity', '', 'class="form-control" id="capacity" required="required"'); ?>
            </div>

            <div class="form-group all">
                <?= lang('floor', 'floor'); ?>
                
                <select name="floor" class="form-control" id="floor">
                	<option value="">Select Floor</option>
                	<?php foreach($floors as $floor) { ?>
                		<option value="<?php echo $floor->id ?>"> <?php echo $floor->name; ?></option>
                	<?php } ?>
                </select>
            </div>

            

        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_restaurant_table', lang('add_restaurant_table'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
   
</script>

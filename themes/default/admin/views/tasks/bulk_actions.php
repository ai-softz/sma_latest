<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo lang('bulk_actions'); ?></h4>
         </div>
         <div class="modal-body">

            
               <div class="checkbox checkbox-danger">
                  <input type="checkbox" name="mass_delete" id="mass_delete">
                  <label for="mass_delete"><?php echo lang('mass_delete'); ?></label>
               </div>
               <hr class="mass_delete_separator" />
           
            <div id="bulk_change">
            	
		               <div class="form-group row">
		               	  <div class="col-md-3">	
		                  	<?php echo lang('task_status'); ?>
		              	  </div>
		              	  <div class="col-md-9">
		                  <?php
		                     $task_statuses= array(
		                                      array('id' => '1', 'name' => 'Not Started'),
		                                      array('id' => '2', 'name' => 'Awaiting Feedback'),
		                                      array('id' => '3', 'name' => 'Testing'),
		                                      array('id' => '4', 'name' => 'In Progress'),
		                                      array('id' => '5', 'name' => 'Complete'),
		                                    );    
		                     ?>
		                  <select name="move_to_status_tasks_bulk_action" id="move_to_status_tasks_bulk_action"  style="width: 50%">
		                     <option value="">Select Status</option>
		                     <?php foreach($task_statuses as $status){ ?>
		                        <option value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
		                     <?php } ?>
		                  </select>
		                  </div>
		               </div>
	            	
              
	       		
	                  <div class="form-group row">
	                  	<div class="col-md-3">	
	                     <?php echo lang('priority'); ?>
	                 	</div>
	                 	<div class="col-md-9">	
	                     <select name="task_bulk_priority" class="select2" id="task_bulk_priority" style="width: 50%">
	                        <option value="">Select Priority</option>
	                        <option value="1"><?php echo lang('task_priority_low'); ?></option>
	                        <option value="2"><?php echo lang('task_priority_medium'); ?></option>
	                        <option value="3"><?php echo lang('task_priority_high'); ?></option>
	                        <option value="4"><?php echo lang('task_priority_urgent'); ?></option>
	                     </select>
	                 	</div>
	                  </div>

           
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('close'); ?></button>
         <a href="#" class="btn btn-info" onclick="tasks_bulk_action(this); return false;"><?php echo lang('confirm'); ?></a>
      </div>
   </div>
   <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<!-- /.modal -->
<?= $modal_js ?>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="d-flex flex-column">
    <?php 
    if(!empty($checklists)) {
    foreach($checklists as $list){ 
      ?>
        
            <div class="checklist" data-checklist-id="<?php echo $list['id']; ?>">
               <div class="d-flex">
                <div class="checkbox checkbox-success checklist-checkbox" data-toggle="tooltip" title="">
                    <input type="checkbox"<?php if($list['finished'] == 1){echo ' disabled';} ?> name="checklist-box" <?php if($list['finished'] == 1){echo 'checked';}; ?>>
                    <label for=""><span class="hide"><?php echo $list['description']; ?></span></label>
                </div>
                <div class="flex-grow-1">
                  <input data-taskid="<?php echo $task_id; ?>" name="checklist-description" rows="1"<?php if(0){echo ' disabled';} ?>  value="<?php echo ($list['description']); ?>">
              	</div>
                 
               </div>
        	</div>
       
<?php }
} ?>
</div>
<script>



</script>

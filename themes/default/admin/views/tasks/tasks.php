<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    .dropdown_status_task {
        border: 1px solid #868686;
        padding: 2px;
        text-align: center;
    }
    .dropdown_status_task a:hover{
        text-decoration: none;
    }
    .dropdown_status_task a:focused{
        text-decoration: none;
    }

</style>
<script>
    $(document).ready(function () {
        oTable = $('#TasksTable').dataTable({
            "aaSorting": [[3, "asc"], [1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('tasks/getTasks') ?>',
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                     $('td:eq(2)', nRow).html(addPopUpModal(nRow, aData));
                     $('td:eq(7)', nRow).html(changeStatus(nRow, aData));
                     $('td:eq(8)', nRow).html(changePriority(nRow, aData));
                    return nRow;
                },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false, "mRender": checkbox}, null, null,  null, null, null, null, {"bSortable": false}, null, {"bSortable": false}]
        });
    });
    function addPopUpModal(nRow, aData) {
       // console.log(nRow);
      // console.log(aData);
       var returnData='<a href="<?=admin_url('tasks/view_task/')?>'+aData[1]+'" data-toggle="modal" data-target="#myModal" class="tip" title="">'+aData[2]+'</a>';
       return returnData;
    }
    function changeStatus(nRow, aData) {

        
                var outputStatus = '<div class="dropdown dropdown_status_task">';
                var task_status = [
                      { "id": 1, "name": "Not Started", },
                      { "id": 2, "name": "Awaiting Feedback", },
                      { "id": 3, "name": "Testing", },
                      { "id": 4, "name": "In Progress", },
                      { "id": 5, "name": "Complete", },
                    ];         
                    var Len = task_status.length;
                    for(var i=0; i < Len; i++) {
                              if (task_status[i].id == aData[7]) {
                         outputStatus += '<a class="dropdown-toggle" data-toggle="dropdown" href="#" >' + task_status[i].name + ' <i class="fa fa-caret-down"></i></a> ';
                       
                              }
                          }
                          
                   outputStatus += '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
                            for(var i=0; i < Len; i++) {
                             if(aData[7] != task_status[i].id){ 
                            outputStatus += '<li  class="border-bottom"><a href="admin/tasks/changeTaskStatus/?id='+aData[0]+'&status_id='+ task_status[i].id+'">'+task_status[i].name+'</a></li>';
                             } 
                             } 
                   outputStatus += '</ul></div>';

                   return outputStatus; 
    }
    function changePriority(nRow, aData) {
                var outputPriority = '<div class="dropdown dropdown_status_task">';
                var task_priority = [
                      { "id": 1, "name": "Low", },
                      { "id": 2, "name": "Medium"},
                      { "id": 3, "name": "High", },
                      { "id": 4, "name": "Urgent", },
                     
                    ];         
                    var Len = task_priority.length;
                    for(var i=0; i < Len; i++) {
                              if (task_priority[i].id == aData[8]) {
                         outputPriority += '<a class="dropdown-toggle" data-toggle="dropdown" href="#" >' + task_priority[i].name + ' <i class="fa fa-caret-down"></i></a>';
                       
                              }
                          }
                          
                   outputPriority += '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
                            for(var i=0; i < Len; i++) {
                             if(aData[8] != task_priority[i].id){ 
                            outputPriority += '<li  class="border-bottom"><a href="admin/tasks/changeTaskPriority/?id='+aData[0]+'&priority_id='+ task_priority[i].id+'">'+task_priority[i].name+'</a></li>';
                             } 
                             } 
                   outputPriority += '</ul></div>';

                   return outputPriority; 
    }
</script>
<?php admin_form_open('tasks/task_actions', 'id="action-form-task"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('tasks'); ?></h2>
        <a class="btn btn-primary right" href="<?php echo admin_url('tasks/add_task'); ?>" data-toggle="modal" data-target="#myModal" style="margin-left: 10px;">
            <i class="fa fa-plus"></i> <?= lang('add_task') ?>
        </a>
        
        <a class="btn btn-primary right" id="bulk_delete" style="margin-left: 10px;">
            <?= lang('bulk_delete') ?>
        </a>
         <span style="margin-left: 10px;">
              <?php
                 $task_statuses= array(
                                  array('id' => '1', 'name' => 'Not Started'),
                                  array('id' => '2', 'name' => 'Awaiting Feedback'),
                                  array('id' => '3', 'name' => 'Testing'),
                                  array('id' => '4', 'name' => 'In Progress'),
                                  array('id' => '5', 'name' => 'Complete'),
                                );    
                 ?>
              <select name="tasks_status_bulk_action" id="tasks_status_bulk_action"  style="width: 20%" >
                 <option>Change Bulk Status</option>
                 <?php foreach($task_statuses as $status){ ?>
                    <option value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
                 <?php } ?>
              </select>
        </span>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang('actions') ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo admin_url('tasks/add_task'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_task') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?=lang('delete_tasks')?></b>" data-content="<p><?=lang('r_u_sure')?></p><button type='button' class='btn btn-danger' id='delete_tasks' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>" data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?=lang('delete_tasks')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>
                <div class="table-responsive">
                    <table id="TasksTable" class="table table-bordered table-hover table-striped reports-table">
                        <thead>
                            <tr>
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox delete_checkbox" type="checkbox" name="check"/>
                                </th>
                                <th><?= lang('the_number_sign'); ?></th>
                                <th><?= lang('tasks_dt_name'); ?></th>
                               
                                <th><?= lang('tasks_dt_datestart'); ?></th>
                                <th><?= lang('task_duedate'); ?></th>
                                <th><?= lang('task_assigned'); ?></th>
                                <th><?= lang('tags'); ?></th>
                                
                                <th style="width:100px;"><?= lang('status'); ?></th>
                                <th><?= lang('priority'); ?></th>
                                <th style="width:100px;"><?= lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7" class="dataTables_empty">
                                    <?= lang('loading_data_from_server') ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action_task" value="" id="form_action_task"/>
     <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
</div>
<?php form_close() ?>

<script language="javascript">
    $(document).ready(function () {

        // $('#myModal').on('hidden.bs.modal', function (e) {
        //   $(".modal-content").html(""); 
        //   $(".modal-content").empty();
        //   $(this).removeData('bs.modal');
        // });

        $(document).on('click', '#bulk_delete', function(){
            
                var taskTable = $('#TasksTable');
                var rows = $(taskTable).find('tbody tr');
                var ids = [];
                $.each(rows, function () {
                        var checkbox = $($(this).find('td').eq(0)).find('input');
                        if (checkbox.prop('checked') === true) {
                            ids.push(checkbox.val());
                        }
                });
                if(ids != '') 
                {
                    if (confirm('Are you sure?')) {
                    var table = $('#TasksTable').DataTable();
                    $.ajax({
                            url: site.base_url + 'tasks/bulk_delete',
                            method: 'POST',
                            dataType: 'text',
                            data: {ids: ids},
                            success: function(data){
                                //table.ajax.reload();
                                location.reload();
                            }

                    });
                     }
                }
                else
                {
                    alert('No task is selected!');
                }
           

        });

        $(document).on('change', '#tasks_status_bulk_action', function(){

                var status = $(this).val();
                var taskTable = $('#TasksTable');
                var rows = $(taskTable).find('tbody tr');
                var ids = [];
                $.each(rows, function () {
                        var checkbox = $($(this).find('td').eq(0)).find('input');
                        if (checkbox.prop('checked') === true) {
                            ids.push(checkbox.val());
                        }
                });
                if(ids != '' && status != '') 
                {
                    var table = $('#TasksTable').DataTable();
                    $.ajax({
                            url: site.base_url + 'tasks/tasks_status_bulk_action',
                            method: 'POST',
                            dataType: 'text',
                            data: {status: status, ids: ids},
                            success: function(data){
                                // table.ajax.reload();
                                location.reload();
                            }

                    });
                }
                else
                {
                    alert('No task is selected!');
                }

        });

        function task_mark_as(status, taskid)
        {
            //console.log(status);
            //console.log(taskid);
            $.ajax({
                url: site.base_url + "tasks/task_mark_as2",
                method: 'POST',
                dataType: 'text',
                data: {status: status, taskid: taskid},
                success: function(data){
                  //alert(data);
                  $('.dropdown_status').html(data);
                }
              });
        }

        $('#delete_t').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

    });
</script>

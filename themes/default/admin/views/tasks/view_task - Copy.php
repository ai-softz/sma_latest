<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $task->name; ?></h4>
        </div>
        
        <div class="modal-body">
            <div class="row">
        			<div class="col-md-8 task-single-col-left">
        				<?php
        				if(!empty($task->description)){
  		            echo '<div class="tc-content"><div id="task_view_description">' .($task->description) .'</div></div>';
  		            } 
        				?>
             <hr />

             <div id="checklist_area">  
               <a id="add" style="cursor: pointer; margin-bottom: 5px;">
               <span class="new-checklist-item"><i class="fa fa-plus-circle"></i>
               <?php echo ('Add Checklist Item'); ?>
               </span>
               </a> 

               <!-- //Add Checklist Form -->
               <form id="add_checklist" name="add_checklist">
                 <button id="submit_btn" class="btn btn-primary btn-sm" type="button" style="display: none;margin-top: 5px">Save</button> 
                 <button id="edit_btn" class="btn btn-warning btn-sm" type="button" style="margin-top: 5px">Edit</button>
                 <input type="hidden" name="taskid" id="taskid" value="<?php echo $task->id ?>"> 
                 <table id="dynamic_field" class="table" border="0" style="margin-top: 5px;width: 100%">
                 </table>
                </form>
                <!-- //show checklist items -->
                <div id="checklist_item_list">
                  <form id="edit_checklist" name="edit_checklist">
                  <table id="checklist_ul" class="table" border="0" width="100%">
                    <tr> 
                      <td></td>
                      <td> <button id="edit_submit_btn" class="btn btn-primary btn-sm" type="button" style="display: none;margin-top: 5px">Update Checklist</button>  </td> 
                      <td></td>
                      
                    </tr>
                   <?php
                   
                   if(!empty($checklists)) {
                      $sl = 0;
                      foreach($checklists as $list){ 
                        $sl++;
                   ?>     
                      <tr>
                        <td><input class="checklist_checkbox" name="checkbox_ok" value="<?php echo $list['id'] ?>" type="checkbox" ></td>
                        <td>
                          <input type="hidden" name="checklist_id" class="checklist_id" value="<?php echo $list['id'] ?>">
                          <input type="text" name="checklist_des[]" class="form-control checklist_input_edit" value="<?php echo $list['description'];?>" disabled>
                        </td>
                        <td>   
                          <!-- <a href="#" class="pull-right text-muted remove-checklist" onclick="delete_checklist_item(<?php echo $list['id']; ?>,this); return false;"><i class="fa fa-remove"></i>
                          </a> -->
                          <a class="pull-right text-muted remove-checklist" id="delete_checklist_item" style="cursor: pointer;"><i class="fa fa-remove"></i>
                          </a>
                        </td>
                      </tr>
                   <?php  
                      }
                    }
                   ?>
                  </table>
                  </form>
                </div>
                <!-- //End #checklist_item_list -->
              </div>
        		 <div class="col-md-4">
        		 </div>
      		</div>
          </div>
          <!-- Right side -->
          <div class="modal-footer">
          </div>
    </div>
    <?php echo form_close(); ?>
</div>
<style>
  .checklist_item {
    margin-bottom: 5px;
  }
  .checklist_input {
    margin-bottom: 5px;
  }
</style>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){
    var i =0;
    $(document).on('click', '#add', function(){
        console.log('add btn clicked!');
        $('#submit_btn').show();
        $('#edit_submit_btn').hide();
        $('.checklist_input_edit').prop('disabled', true);
        i++;
        var html = 
        '<tr id="row'+i+'">'+
        '<td><input type="text" name="checklist_des[]" class="form-control checklist_input"></td>' + 
        '<td><button id="row'+i+'" name="remove" class="btn_remove btn btn-danger"> x</button></td>' + 
        '</tr>'
        ;
        $('#dynamic_field').append(html);
        // $('.checklist_area').reload();

   });

   $(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr('id');
      $('#row'+i+'').remove();
   });

   $(document).on('click', '#submit_btn', function(){
      console.log('submit btn clicked!');
      var checklist_data = $('#add_checklist').serialize(); 
      var taskid = $('#taskid').val();

      $.ajax({
          url: site.base_url + "tasks/add_checklist_item",
          method: "POST",
          data: checklist_data,
          success:function(data) {

            var obj = JSON.parse(data);
            $('.checklist_input').remove();
            $('.btn_remove').remove();
            $('#submit_btn').hide();
            $('#edit_btn').show();
            $('#checklist_ul').remove();

            var len = obj.checklist_items.length;
            var i;
            $('#checklist_item_list').append('<table class="table" id="checklist_ul" width="100%">');
            for(i = 0; i< len; i++) {
                $('#checklist_ul').append('<tr><td><input type="text" name="checklist_des[]" class="form-control checklist_input" value="'+obj.checklist_items[i].description+'" disabled></td></tr>');
            }
            $('#checklist_item_list').append('</table>');

          } 
      })
   });

   $(document).on('click', '#edit_submit_btn', function(){
      //$('#edit_btn').hide();
      var form_data = $('#edit_checklist').serialize();
      console.log(form_data);
   });

   $(document).on('click', '#edit_btn', function(){
      $('.checklist_input_edit').prop('disabled', false);
      $('#edit_submit_btn').show();
      $('#edit_btn').hide();
   });

   $(document).on('ifChanged', '.checklist_checkbox', function(){
    console.log('checkbox clicked!');
       if($('.checklist_checkbox').prop('checked', true)) {
          var checklist_id = $('.checklist_checkbox').val();
          console.log(checklist_id);
       } 
   });

   function delete_checklist_item($checklist_id, $field) {
      console.log($checklist_id);
      console.log($this);
   }

   $(document).on('click', '.delete_checklist_item', function(){
      //$('#edit_btn').hide();
      var checklist_id = $('.checklist_id').val();
      console.log(checklist_id);
   });

  });
</script>

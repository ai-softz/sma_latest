<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header" style="background-color: #b5d9e6">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $task->name; ?></h4>
        </div>
        
        <div class="modal-body">
            <div class="row">
        			<div class="col-md-8 task-single-col-left">

                <div class="clearfix"></div>
                
               <div class="clearfix"></div>
         <?php if($task->status != Tasks_model::STATUS_COMPLETE){ ?>
         <p class="no-margin pull-left" id="mark_complete_area" style="<?php //echo 'margin-'.(is_rtl() ? 'left' : 'right').':5px !important'; ?>">
            <a class="btn btn-info" id="mark-complete" data-taskid="<?php echo $task->id ?>" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_single_mark_as_complete'); ?>">
            <i class="fa fa-check"></i>
            </a>
         </p>
         <?php } else if($task->status == Tasks_model::STATUS_COMPLETE){ ?>
         <p class="no-margin pull-left"  id="unmark_complete_area" style="<?php //echo 'margin-'.(is_rtl() ? 'left' : 'right').':5px !important'; ?>">
            <a class="btn btn-default" data-taskid="<?php echo $task->id ?>" id="unmark-complete" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_unmark_as_complete'); ?>">
            <i class="fa fa-check"></i>
            </a>
         </p>
         <?php } ?>

               <!-- <p class="no-margin pull-left" style="margin-right:5px<?php //echo 'margin-'.(is_rtl() ? 'left' : 'right').':5px !important'; ?>">
                <?php 
                  
                  if($task->status != Tasks_model::STATUS_COMPLETE){ 
                      $mark_completestyle='style="display:block"';
                      $unmark_completestyle='style="display:none"';
                  } else if($task->status == Tasks_model::STATUS_COMPLETE){ 
                      $mark_completestyle='style="display:none"';
                      $unmark_completestyle='style="display:block"';
                  } 
                  ?>
                  <a id="mark-complete" <?=$mark_completestyle?>  data-taskid="<?php echo $task->id; ?>" class="btn btn-info" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_single_mark_as_complete'); ?>">
                  <i class="fa fa-check"></i>
                  </a>
                  
                  <a id="unmark-complete" <?=$unmark_completestyle?> data-taskid="<?php echo $task->id; ?>" class="btn btn-default" id="unmark-complete" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_unmark_as_complete'); ?>">
                  <i class="fa fa-check"></i>
                  </a>
                 
               </p> -->
              

               <?php 
                //if(!$this->tasks_model->is_timer_started($task->id)) { 
               if(1) { 
                ?>
                <p class="no-margin pull-left" data-toggle="tooltip" data-title="<?php echo lang('task_start_timer_only_assignee'); ?>">
                   <a href="#" class="mbot10 btn<?php if($task->status == Tasks_model::STATUS_COMPLETE){echo ' disabled btn-default';}else {echo ' btn-success';} ?>" onclick="timer_action(this, <?php echo $task->id; ?>); return false;">
                   <i class="fa fa-clock-o"></i> <?php echo ' ' . lang('task_start_timer'); ?>
                   </a>
                </p>
               <?php } else { ?>
               <p class="no-margin pull-left">
                  <a href="#" data-toggle="popover" data-placement="<?php echo is_mobile() ? 'bottom' : 'right'; ?>" data-html="true" data-trigger="manual" data-title="<?php echo lang('note'); ?>" data-content='<?php echo render_textarea('timesheet_note'); ?><button type="button" onclick="timer_action(this, <?php echo $task->id; ?>, <?php echo $this->tasks_model->get_last_timer($task->id)->id; ?>);" class="btn btn-info btn-xs"><?php echo lang('save'); ?></button>' class="btn mbot10 btn-danger<?php if(!$is_assigned){echo ' disabled';} ?>" onclick="return false;">
                  <i class="fa fa-clock-o"></i> <?php echo lang('task_stop_timer'); ?>
                  </a>
               </p>
               <?php } ?>
               

               <div class="clearfix"></div>
               <hr />
               <div class="clearfix"></div>
               <h4 class="th font-medium mbot15 pull-left"><?php echo lang('task_view_description'); ?></h4>
               <a href="#" onclick="edit_task_inline_description(this,<?php echo $task->id; ?>); return false;" class="pull-left mtop10 mleft5 font-medium-xs"><i class="fa fa-pencil-square-o"></i></a>
                <div class="clearfix"></div>
                 <?php if(!empty($task->description)){
                    echo '<div class="tc-content"><div id="task_view_description">' .$task->description .'</div></div>';
                    } else {
                    echo '<div class="no-margin tc-content task-no-description" id="task_view_description"><span class="text-muted">' . lang('task_no_description') . '</span></div>';
                    } ?>
                 <div class="clearfix"></div>
                 
                <hr />
             <!-- Checklists items add/edit/delete -->
               <div id="checklist_area">  
                  <div class="container">  
                     <h4 class="bold chk-heading th font-medium pull-left">Checklist Items</h4>
                      <div class="table-responsive">  
                           <input type="hidden" name="taskid" id="taskid" value="<?php echo $task->id ?>">
                           <div id="live_data"></div>                 
                      </div>  
                 </div>  
                </div>

                    <hr />
         <a href="#" id="taskCommentSlide" onclick="slideToggle('.tasks-comments'); return false;">
            <h4 class="mbot20 font-medium"><?php echo lang('task_comments'); ?></h4>
         </a>
         <div class="tasks-comments inline-block full-width simple-editor"<?php if(count($task_comments) == 0){echo ' style="display:none"';} ?>>
            <?php echo form_open_multipart(admin_url('tasks/add_task_comment'),array('id'=>'task-comment-form','class'=>'dropzone dropzone-manual','style'=>'min-height:auto;background-color:#fff;')); ?>
            <textarea name="comment" placeholder="<?php echo lang('task_single_add_new_comment'); ?>" id="task_comment" rows="3" class="form-control ays-ignore"></textarea>
            <div id="dropzoneTaskComment" class="dropzoneDragArea dz-default dz-message hide task-comment-dropzone">
               <span><?php echo lang('drop_files_here_to_upload'); ?></span>
            </div>
            <div class="dropzone-task-comment-previews dropzone-previews"></div>
            <button type="button" class="btn btn-info mtop10 pull-right hide" id="addTaskCommentBtn" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" onclick="add_task_comment('<?php echo $task->id; ?>');" data-comment-task-id="<?php echo $task->id; ?>">
            <?php echo lang('task_single_add_new_comment'); ?>
            </button>
            <?php echo form_close(); ?>
            <div class="clearfix"></div>
            <?php if(count($task_comments) > 0){echo '<hr />';} ?>
            <div id="task-comments" class="mtop10">
               <?php
                  $comments = '';
                  $len = count($task_comments);
                  $i = 0;
                  foreach ($task_comments as $comment) {
                    $comments .= '<div id="comment_'.$comment['id'].'" data-commentid="' . $comment['id'] . '" data-task-attachment-id="'.$comment['file_id'].'" class="tc-content task-comment'.(strtotime($comment['dateadded']) >= strtotime('-16 hours') ? ' highlight-bg' : '').'">';
                    
                    if($comment['staffid'] != 0){
                     $comments .= '<a href="' . $comment['first_name'].' '.$comment['last_name']. '" target="_blank">'
                      
                   . '</a>';
                  } 
                  // elseif($comment['contact_id'] != 0) {
                  //    $comments .= '<img src="'.contact_profile_image_url($comment['contact_id']).'" class="client-profile-image-small media-object img-circle pull-left mright10">';
                  // }
                  

                  if ($comment['staffid'] == $this->session->userdata('user_id') ) {
                     $comment_added = strtotime($comment['dateadded']);
                     $minus_1_hour = strtotime('-1 hours');
                     if(get_option('client_staff_add_edit_delete_task_comments_first_hour') == 0 || (get_option('client_staff_add_edit_delete_task_comments_first_hour') == 1 && $comment_added >= $minus_1_hour) || is_admin()){
                       $comments .= '<span class="pull-right"><a href="#" onclick="remove_task_comment(' . $comment['id'] . '); return false;"><i class="fa fa-times text-danger"></i></span></a>';
                       $comments .= '<span class="pull-right mright5"><a href="#" onclick="edit_task_comment(' . $comment['id'] . '); return false;"><i class="fa fa-pencil-square-o"></i></span></a>';
                    }
                  }

                  $comments .= '<div class="media-body comment-wrapper">';
                  $comments .= '<div class="mleft40">';

                  if($comment['staffid'] != 0){
                   $comments .= $comment['first_name'].' '.$comment['last_name'] . '<br />';
                  } elseif($comment['contact_id'] != 0) {
                   $comments .= '<span class="label label-info mtop5 mbot5 inline-block">'.lang('is_customer_indicator').'</span><br /><a href="' . admin_url('clients/client/'.get_user_id_by_contact_id($comment['contact_id']) .'?contactid='.$comment['contact_id'] ) . '" class="pull-left" target="_blank">' . ($comment['contact_id']) . '</a> <br />';
                  }

                  $comments .= '<small class="border-bottom">' . date('d M, Y h:i:sa',strtotime($comment['dateadded'])) . '</small>';

                  $comments .= '<div data-edit-comment="'.$comment['id'].'" class="hide edit-task-comment"><textarea rows="5" id="task_comment_'.$comment['id'].'" class="ays-ignore form-control">'.str_replace('[task_attachment]', '', $comment['content']).'</textarea>
                  <div class="clearfix mtop20"></div>
                  <button type="button" class="btn btn-info pull-right" onclick="save_edited_comment('.$comment['id'].','.$task->id.')">'.lang('submit').'</button>
                  <button type="button" class="btn btn-default pull-right mright5" onclick="cancel_edit_comment('.$comment['id'].')">'.lang('cancel').'</button>
                  </div>';
                  if($comment['file_id'] != 0){
                  $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$attachments_data[$comment['file_id']],$comment['content']);
                  // Replace lightbox to prevent loading the image twice
                  $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-attachment-comment-'.$comment['id'].'"',$comment['content']);
                  } 
                  /*else if(count($comment['attachments']) > 0 && isset($comments_attachments[$comment['id']])) {
                   $comment_attachments_html = '';
                   foreach($comments_attachments[$comment['id']] as $comment_attachment) {
                       $comment_attachments_html .= trim($comment_attachment);
                   }
                   $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$comment_attachments_html,$comment['content']);
                   // Replace lightbox to prevent loading the image twice
                   $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-comment-files-'.$comment['id'].'"',$comment['content']);
                   $comment['content'] .='<div class="clearfix"></div>';
                   $comment['content'] .='<div class="text-center download-all">
                   <hr class="hr-10" />
                   <a href="'.admin_url('tasks/download_files/'.$task->id.'/'.$comment['id']).'" class="bold">'.lang('download_all').' (.zip)
                   </a>
                   </div>';
                  }*/
                  $comments .= '<div class="comment-content mtop10">'.($comment['content']) . '</div>';
                  $comments .= '</div>';
                  if ($i >= 0 && $i != $len - 1) {
                     $comments .= '<hr class="task-info-separator" />';
                  }
                  $comments .= '</div>';
                  $comments .= '</div>';
                  $i++;
                  }
                  echo $comments;
                  ?>
            </div>
         </div>


      		    </div> 
              <!-- End .col-md-8 -->
              <div class="col-md-4"  style="background-color: #f0f5f7">
                <h4 class="task-info-heading"><?php echo lang('task_info'); ?>
                  <?php
                     if($task->recurring == 1){
                        echo '<span class="label label-info inline-block mleft5">'.lang('recurring_task').'</span>';
                     }
                     ?>
                </h4>
                <div class="clearfix"></div>
                 <h5 class="no-mtop task-info-created">
                    <small class="text-dark"><?php echo lang('task_created_at').'<span class="text-dark">: '.($task->dateadded).'</span>'; ?></small>
                 </h5>
                 <hr class="task-info-separator" />

                <div class="task-info task-status task-info-status">
                  <h5>
                    <div class="dropdown">
                     <i class="fa fa-<?php if($task->status == Tasks_model::STATUS_COMPLETE){echo 'star';} else if($task->status == 1){echo 'star-o';} else {echo 'star-half-o';} ?> pull-left task-info-icon fa-fw fa-lg"></i><?php echo lang('task_status'); ?>:
                     <?php
                     $task_status= array(
                                      array('id' => '1', 'name' => 'Not Started'),
                                      array('id' => '2', 'name' => 'Awaiting Feedback'),
                                      array('id' => '3', 'name' => 'Testing'),
                                      array('id' => '4', 'name' => 'In Progress'),
                                      array('id' => '5', 'name' => 'Complete'),
                                    );    
                     ?>
                     <?php if($task->status != Tasks_model::STATUS_COMPLETE) { ?> 
                     <?php 
                     //echo $task->status; 
                     foreach ($task_status as $status) {
                              if ($status['id'] == $task->status) {
                          ?>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $status['name']; ?> </a>
                          <?php
                              }
                          }
                          ?>

                       <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <?php
                                // show priority dropdown
                                foreach($task_status as $status){ ?>
                                <?php if($task->status != $status['id']){ ?>
                                <li  class="border-bottom">
                                   <a href="#" onclick="task_mark_as(<?php echo $status['id']; ?>,<?php echo $task->id; ?>); return false;">
                                   <?php echo $status['name']; ?>
                                   </a>
                                </li>
                                <?php } ?>
                                <?php } ?>
                           </ul>   
                           
                    </div>
                     <?php } else { ?>
                     
                     <?php 
                     foreach ($task_status as $status) {
                              if ($status['id'] == $task->status) {
                          ?>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $status['name']; ?> </a>
                          <?php
                              }
                          }
                     ?>

                     <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <?php
                                // show priority dropdown
                                foreach($task_status as $status){ ?>
                                <?php if($task->status != $status['id']){ ?>
                                <li  class="border-bottom">
                                   <a href="#" onclick="task_mark_as(<?php echo $status['id']; ?>,<?php echo $task->id; ?>); return false;">
                                   <?php echo $status['name']; ?>
                                   </a>
                                </li>
                                <?php } ?>
                                <?php } ?>
                           </ul>
                    
                     <?php } ?>
                    
                    </h5>
                  </div>

                  <div class="task-info task-single-inline-wrap task-info-start-date">
                    <h5><i class="fa task-info-icon fa-fw fa-lg fa-calendar-plus-o pull-left fa-margin"></i>
                       <?php echo lang('task_single_start_date'); ?>:
                       <?php if($task->status !=5) { ?>
                       <input type="" name="startdate" class="date pointer task-single-inline-field" id="start_date" value="<?php echo date('d-m-Y',strtotime($task->startdate)); ?>" autocomplete="off"> 
                       <?php } else { ?>
                       <?php echo ($task->startdate); ?>
                       <?php } ?>
                    </h5>
                  </div>
                  <div class="task-info task-single-inline-wrap task-info-due-date">
                    <h5>
                     <i class="fa fa-calendar-check-o task-info-icon fa-fw fa-lg pull-left"></i>
                     <?php echo lang('task_single_due_date'); ?>:
                     <?php if($task->status !=5) { ?>
                     <input type="" name="startdate" class="date pointer task-single-inline-field" id="start_date" value="<?php echo date('d-m-Y',strtotime($task->duedate)); ?>" autocomplete="off"> 
                     <?php } else { ?>
                     <?php echo date('d-m-Y',strtotime($task->duedate)); ?>
                     <?php } ?>
                  </h5>
                  </div>

                  <div class="task-info task-status task-info-status">
                  <h5>
                    <div class="dropdown">
                     <i class="fa task-info-icon fa-fw fa-lg pull-left fa-bolt"></i>
                       <?php 
                        $task_priorities  = array(
                            array('id' => '1', 'name' => lang('task_priority_low')),
                            array('id' => '2', 'name' => lang('task_priority_medium')),
                            array('id' => '3', 'name' => lang('task_priority_high')),
                            array('id' => '4', 'name' => lang('task_priority_urgent')),
                          );
                        ?>
                       <?php echo lang('task_single_priority'); ?>: 
                       <?php if($task->status != Tasks_model::STATUS_COMPLETE) { ?>
                          <?php 
                          // show priority name
                          
                          foreach ($task_priorities as $priority) {
                              if ($priority['id'] == $task->priority) {
                          ?>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $priority['name']; ?> </a>
                          <?php
                              }
                          }
                          ?>
                           <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <?php
                                // show priority dropdown
                                foreach($task_priorities as $priority){ ?>
                                <?php if($task->priority != $priority['id']){ ?>
                                <li  class="border-bottom">
                                   <a href="#" onclick="task_change_priority(<?php echo $priority['id']; ?>,<?php echo $task->id; ?>); return false;">
                                   <?php echo $priority['name']; ?>
                                   </a>
                                </li>
                                <?php } ?>
                                <?php } ?>
                           </ul>
                     </div>
                     <?php } else { ?>
                     <span style="">
                       <?php 
                       foreach ($task_priorities as $priority) {
                                if ($priority['id'] == $task->priority) {
                            ?>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $priority['name']; ?> </a>
                            <?php
                                }
                        }
                       ?>
                       </span>
                     <?php } ?>
                    </h5>
                  </div>

                  <?php 
                  $query = $this->db->get_where('taskstimers', array('task_id'=>$task->id,'staff_id'=>$this->session->userdata('user_id')));
                  if($query->num_rows() > 0){ ?>
                   <div class="task-info task-info-user-logged-time">
                      <h5>
                         <i class="fa fa-asterisk task-info-icon fa-fw fa-lg" aria-hidden="true"></i><?php echo lang('task_user_logged_time'). ': '; ?>
                         <?php  ?>
                      </h5>
                   </div>
                   <?php } ?>

                   <div class="task-info task-info-total-logged-time">
                      <h5>
                         <i class="fa task-info-icon fa-fw fa-lg fa-clock-o"></i><?php echo lang('task_total_logged_time'). ': '; ?>
                         <span class="text-success">
                         <?php  ?>
                         </span>
                      </h5>
                   </div>

                   <div class="mtop5 clearfix"></div>
                     <div id="inputTagsWrapper">
                        <?php 
                        //$this->db->get_where('tags', array('ta'))
                        ?>
                        <input type="text" class="tagsinput" id="tags" name="tags" value="" data-role="tagsinput">
                     </div>
                   <div class="clearfix"></div>

                   <hr class="task-info-separator" />
                   <div class="clearfix"></div>
                   <h4 class="task-info-heading font-normal font-medium-xs"><i class="fa fa-user" aria-hidden="true"></i> <?php echo lang('task_single_assignees'); ?></h4>
                    <div class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" ><?php echo lang('assign_task_to'); ?><span class="caret right"></span></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        
                        <?php foreach ($staff as $assignee) {
                                         if (!in_array($assignee['id'],$assignees_ids)) { ?>
                            <li class="border-bottom">
                              <a class="dropdown-item" href="#"><?php echo $assignee['first_name'] . ' ' . $assignee['last_name']; ?></a>
                            </li>

                        <?php } } ?>
                      </ul>
                    </div>

                    
      <hr class="task-info-separator" />
         <div class="clearfix"></div>
          <h4 class="task-info-heading font-normal font-medium-xs">
            <i class="fa fa-bell-o" aria-hidden="true"></i>
            <?php echo lang('reminders'); ?>
         </h4>
         <a href="#" onclick="new_task_reminder(<?php echo $task->id; ?>); return false;">
         <?php echo lang('create_reminder'); ?>
         </a>
         <?php if(count($reminders) == 0) { ?>
         <div class="display-block text-muted mtop10">
            <?php echo lang('no_reminders_for_this_task'); ?>
         </div>
         <?php } else { ?>
         <ul class="mtop10">
            <?php foreach($reminders as $rKey => $reminder) {
               ?>
            <li class="<?php if($reminder['isnotified'] == '1'){echo 'text-throught';} ?>" data-id="<?php echo $reminder['id']; ?>">
               <div class="mbot15">
                  <div>
                     <p class="bold">
                        <?php 
                        //echo lang('reminder_for', [
                        //    ($reminder['staff']),
                        //    ($reminder['date'])
                        // ]); 
                        echo lang('reminder_for'). ' ' . $reminder['staff'] . ' ' . $reminder['date'];
                        ?>
                        <?php if ($reminder['creator'] == $this->session->userdata('user_id')) { ?>
                        <?php if($reminder['isnotified'] == 0){ ?>
                        <a href="#" class="  text-muted" onclick="edit_reminder(<?php echo $reminder['id']; ?>, this); return false;">
                        <i class="fa fa-edit"></i>
                        </a>
                        <?php } ?>
                        <a href="<?php echo admin_url('tasks/delete_reminder/' . $task->id . '/' . $reminder['id']); ?>" class="text-danger delete-reminder"><i class="fa fa-remove"></i></a>
                        <?php } ?>
                     </p>
                     <?php
                        if(!empty($reminder['description'])) {
                           echo $reminder['description'];
                        } else {
                           echo '<p class="text-muted no-mbot">'._l('no_description_provided').'</p>';
                        }
                        ?>
                  </div>
                  <?php if(count($reminders) -1 != $rKey) { ?>
                  <hr class="hr-10" />
                  <?php } ?>
               </div>
            </li>
            <?php } ?>
         </ul>
         <?php } ?>
         <div class="clearfix"></div>
         <div id="newTaskReminderToggle" class="mtop15" style="display:none;">
            <?php echo form_open('', array('id'=>'form-reminder-task')); ?>
            <?php //$this->load->view('admin/includes/reminder_fields',['members'=>$staff_reminders, 'id'=>$task->id, 'name'=>'task']); ?>
            <button class="btn btn-info btn-xs pull-right" type="submit" id="taskReminderFormSubmit">
            <?php echo lang('create_reminder'); ?>
            </button>
            <div class="clearfix"></div>
            <?php echo form_close(); ?>
         </div>
         <hr class="task-info-separator" />
         <div class="clearfix"></div>
                    

               </div>
            </div>
          <!-- Right side -->
          <div class="modal-footer">
          </div>
    </div>
    <?php echo form_close(); ?>
</div>
<style>
  .checklist_item {
    margin-bottom: 5px;
  }
  .checklist_input {
    margin-bottom: 5px;
  }
  #description{
    background-color: #f1f1f1;
  }
  .date {
    width: 30%;
  }
  .dropdown a:hover{
    text-decoration: none;
  }
  .dropdown a{
    color: #8c8d8e;
  }
  .dropdown ul{
    width: 80%;
  }
  .dropdown li:last-child {
    border-bottom: none !important; 
  }
</style>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>



<script>  
 $(document).ready(function(){  
      function fetch_data()  
      {  
           $.ajax({  
                url: site.base_url + "tasks/select_checklist_item",
                method:"POST",  
                data : {taskid: $('#taskid').val()},
                success:function(data){  
                     $('#live_data').html(data);  
                }  
           });  
      }  
      fetch_data();  
      $(document).on('click', '#btn_add', function(){  
           var taskid = $('#taskid').val();  
           var description = $('#description').text();  
           if(description == '')  
           {  
                alert("Enter description");  
                return false;  
           }  

           $.ajax({  
                url: site.base_url + "tasks/add_checklist_item",
                method:"POST",  
                data:{ taskid: taskid, description: description },  
                dataType:"text",  
                success:function(data)  
                {  
                     //alert(data);  
                     fetch_data();  
                }  
           })  
      });  
      function edit_data(checklist_id, description)  
      {  
           $.ajax({  
                url: site.base_url + "tasks/edit_checklist_item",
                method:"POST",  
                data:{ checklist_id:checklist_id, description:description },  
                dataType:"text",  
                success:function(data){  
                     //alert(data);  
                }  
           });  
      }  
      $(document).on('blur', '.description', function(){  
           var checklist_id = $(this).data("checklist_id");  
           var description = $(this).text();  
           edit_data(checklist_id, description);  
      });  
      $(document).on('click', '.btn_delete', function(){  
           var checklist_id=$(this).data("checklist_id");  
           if(confirm("Are you sure you want to delete this?"))  
           {  
                $.ajax({  
                     url: site.base_url + "tasks/delete_checklist_item",
                     method:"POST",  
                     data:{ checklist_id:checklist_id },  
                     dataType:"text",  
                     success:function(data){  
                          //alert(data);  
                          fetch_data();  
                     }  
                });  
           }  
      });  

      $(document).on('change', '.checklist_checkbox', function(){
          var checklist_id = $(this).data('checklist_id');
          console.log(checklist_id);
          if ($('.checklist_checkbox').is(":checked")) {
            var finished = 1;
          } 
          else {
            var finished = 0;
          }
          $.ajax({
            url: site.base_url + "tasks/finish_checklist_item",
            method: "POST",
            data: { checklist_id: checklist_id, finished: finished },
            dataType: "text",
            success: function(){

            }
          });

       });

      // Assign task to staff member
      $("body").on('change', 'select[name="select-assignees"]', function () {
          $("body").append('<div class="dt-loader"></div>');
          var data = {};
          data.assignee = $('select[name="select-assignees"]').val();
          if (data.assignee !== '') {
              data.taskid = $(this).attr('data-task-id');
              $.post(admin_url + 'tasks/add_task_assignees', data).done(function (response) {
                  $("body").find('.dt-loader').remove();
                  response = JSON.parse(response);
                  reload_tasks_tables();
                  _task_append_html(response.taskHtml);
              });
          }
      });

      // Marking task as complete
      $(document).on('click', '#mark-complete', function(){
          
          var taskid = $(this).data("taskid");
          var status = 5; // for complete 

          $.ajax({
            url: site.base_url + "tasks/task_mark_as",
            method: 'POST',
            dataType: 'text',
            data: {status: status, taskid: taskid},
            success: function(data){
              //$('#unmark-complete').css({"background-color": "#fff","display":"block"});
              //$('#mark-complete').css({"display":"none"});
              $('#mark-complete').remove();
              $('#mark_complete_area').html('<a class="btn btn-default" id="unmark-complete" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_unmark_as_complete'); ?>"><i class="fa fa-check"></i></a>');
            }
          });
      });

      $(document).on('click', '#unmark-complete', function(){
          
          var taskid = $(this).data("taskid");
          var status = 4; // for complete 

          $.ajax({
            url: site.base_url + "tasks/task_mark_as",
            method: 'POST',
            dataType: 'text',
            data: {status: status, taskid: taskid},
            success: function(data){
              //alert(data);
              // $('#unmark-complete').css({"display":"none"});
              // $('#mark-complete').css({"background-color": "#008ece", "display":"block"});
              $('#unmark-complete').remove();
              $('#unmark_complete_area').html('<a class="btn btn-info" id="mark-complete" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_single_mark_as_complete'); ?>"><i class="fa fa-check"></i></a>');

            }
          });
      });

      

 });  
function task_mark_as(status, taskid)
      {
        $.ajax({
            url: site.base_url + "tasks/task_mark_as",
            method: 'POST',
            dataType: 'text',
            data: {status: status, taskid: taskid},
            success: function(data){
              //alert(data);
              
            }
          });
      }
 </script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("timesheets/add_leave_application", $attrib) ?>

        <div class="form-group row">
	      <div class="col-md-6">
	         <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
	         <select name="employee_id" class="form-control select" id="leave_employee_id">
	              <option value=""><?php echo 'Select';?></option>
	              <?php foreach($all_employees as $row) { ?>
	                <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
	              <?php } ?>
	           </select>
	      </div>
	      <div class="col-md-6">
	         <label for="leave_type_id"><?php echo  lang('leave_type') ?> <span class="text-danger"></span></label>
	         <div id="leave_type_ajax">
	         <select name="leave_type_id" class="form-control select" id="leave_type_id">
	              <option value=""><?php echo 'Select';?></option>
	         </select>
	     	</div>
	      </div>
	   </div>
	   <div class="form-group row">
	   	<div class="col-md-6">
	         <label for="from_date"><?php echo  lang('from_date') ?> <span class="text-danger">*</span></label>
	         <input class="form-control date" placeholder="<?php echo lang('from_date');?>" name="from_date" type="text" value="" required="required" autocomplete="off">
	    </div> 
	     <div class="col-md-6">
	         <label for="to_date"><?php echo  lang('to_date') ?> <span class="text-danger">*</span></label>
	         <input class="form-control date" placeholder="<?php echo lang('to_date');?>" name="to_date" type="text" value="" required="required" autocomplete="off">
	    </div>  
	      
	   </div>

	   <div class="form-group row">
	   	<div class="col-md-6">
	         <label for="leave_attachment"><?php echo  lang('leave_attachment') ?> <span class="text-danger">*</span></label>
	        <input type="file" name="leave_attachment" id="leave_attachment" size="20" />
	        <p><small>Upload files only: png, jpg, jpeg, gif, txt, pdf, xls, xlsx, doc, docx</small></p>
	    </div> 	
	    <div class="col-md-6">
	    	<input type="checkbox" name="is_half_day">
	    	<label for="is_half_day"><?php echo  lang('half_day') ?> <span class="text-danger"></span></label>
	    	
	    </div>
	   </div>
	   <div class="form-group row">
		    <div class="col-md-6">
			   	<label for="reason"><?php echo  lang('reason') ?> <span class="text-danger"></span></label>
			   	<textarea name="reason" class="form-control" required="required"></textarea>
		    </div>
		    <div class="col-md-6">
			   	<label for="remarks"><?php echo  lang('remarks') ?> <span class="text-danger"></span></label>
			   	<textarea name="remarks" class="form-control"></textarea>
	    	</div>
		</div>

         <div class="form-group">
			 <?php echo form_submit('add_leave_application', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){

	$(document).on('change', '#leave_employee_id', function(){
		var empid = $(this).val();
		console.log(empid);
		$.ajax({
			url: site.base_url + "timesheets/getEmpLeaves",
			method: 'POST',
			dataType: 'text',
			data: {empid: empid},
			success: function(data){
				// alert(data);
				$('#leave_type_id').remove();
				$('#leave_type_ajax').html(data);
			}

		});
	});

});

    </script>

<div class="row">
    <div class="col-sm-12 col-md-12">
	<div class="panel panel-bd lobidrag">
		<div class="panel-body">
			<div class="panel-heading" style="border-bottom: 1px solid #e4e5e7; margin-bottom: 5px;">
				<div class="panel-title">
					<h2>Attendances:</h2>
                </div>
				<div class="row">
					<div class="col-md-6">
						<input name="date" type="text" id="dateSearch" class="form-control date" value="<?= date('d-m-Y'); ?>">
					</div>
					<div class="col-md-3">
						<a id="btnSearch" type="submit" class="btn btn-primary">Search</a>
					</div>
					<div class="col-md-3">
						<a id="add_attendance" href="<?= admin_url('timesheets/add_attendance') ?>" class="btn btn-primary">Add Employee Attendance</a>
					</div>
				</div>
				</div>
	        </div>
	         <input type="hidden" name="attendance_date" value="<?php echo date('Y-m-d');?>" id="attendance_date">
			
			 <div class="table-responsive">
			 <table id="table_attendances" class="table table-bordered table-hover table-striped">
			    <thead>
			       <tr>
			          <th><?php echo lang('the_number_sign');?></th>
			          <th><?php echo lang('name');?></th>
			          <th><?php echo lang('id');?></th>
			          <th><?php echo lang('date');?></th>
			          <th><?php echo lang('status');?></th>
			          <th><?php echo lang('clock_in');?></th>
			          <th><?php echo lang('clock_out');?></th>
			          <th><?php echo lang('late');?></th>
			          <th><?php echo lang('early_leaving');?></th>
			          <th><?php echo lang('overtime');?></th>
			          <th><?php echo lang('total_work');?></th>
			          <th><?php echo lang('total_rest');?></th>
			       </tr>
			    </thead>
			    <tbody>
			    </tbody>
			 </table>
			</div>
		</div>
	</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		fill_datatable();
		function fill_datatable(dateSearch='') {
		var attendance_date = $('#attendance_date').val();
		oTable = $('#table_attendances').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 
		   'bServerSide': true,
           'sAjaxSource': '<?= admin_url('timesheets/attendance_list/?attendance_date=') ?>' + attendance_date,
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
				// $('td:eq(6)', nRow).html(paymentStatus(nRow, aData));
				// return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>",
               });
               aoData.push({
                   "name": "dateSearch",
                   "value": dateSearch,
               });
			   //console.log(aoData);
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, null, null, {"bSortable": false}, null, null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
	}
	$("#btnSearch").on("click", function(e){
		e.preventDefault();
		var dateSearch = $("#dateSearch").val();
		fill_datatable(dateSearch);
	});
});
	
</script>
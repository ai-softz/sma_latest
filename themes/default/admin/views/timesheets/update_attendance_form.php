<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_events'); ?></h4>
    </div>

    <div class="modal-body">
    <h3> Update Attendance of  <b><?= $emp_name ?></b> </h3>

    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'updateAttendance']; ?>
		     <?php echo admin_form_open_multipart("timesheets/updateAttendance", $attrib) ?>
		     <input type="hidden" name="time_attendance_id" value="<?= $attendance->time_attendance_id ?>">
		     <input type="hidden" name="emp_id" value="<?= $emp_id ?>">


		   <div class="form-group row">
		    <div class="col-md-6">
		         <label for="attendance_date"><?php echo  lang('attendance_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('attendance_date');?>" name="attendance_date" type="text" value="<?= date('d-m-Y', strtotime($attendance->attendance_date)) ?>" required="required">
		    </div> 
            
		   </div>
		   <div class="form-group row">
           <div class="col-md-6">
               <?php sscanf($attendance->clock_in, '%d-%d-%d %0d:%d', $years, $months, $days, $hours, $minutes); ?>
                <label for="clock_in"><?php echo  lang('clock_in') ?> <span class="text-danger">*</span></label>
		         <input class="form-control " placeholder="<?php echo lang('clock_in');?>" name="clock_in" type="text" value="<?= $hours.':'.$minutes ?>" required="required">
            </div> 
            <div class="col-md-6">
                <?php sscanf($attendance->clock_out, '%d-%d-%d %0d:%d', $years, $months, $days, $hours, $minutes); ?>
                <label for="clock_out"><?php echo  lang('clock_out') ?> <span class="text-danger">*</span></label>
		         <input class="form-control " placeholder="<?php echo lang('clock_out');?>" name="clock_out" type="text" value="<?= $hours.':'.$minutes ?>" required="required">
            </div> 
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('update_attendance', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#updateAttendance').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "timesheets/updateAttendance",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('#myModal').modal('hide');
                location.reload();
                // $('#table_events').DataTable().ajax.reload();
             }
               
      });  
  });


});
</script>
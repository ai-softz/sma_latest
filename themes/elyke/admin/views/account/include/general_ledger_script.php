<script type="text/javascript">
    $(document).ready(function(){
    	"use strict";
        $('#cmbGLCode').on('change',function(){
           var Headid=$(this).val();
            $.ajax({
                 url: '<?php echo site_url('accounts/accounts/general_led'); ?>',
                type: 'POST',
                data: {
                    Headid: Headid
                },
                success: function (data) {
                   $("#ShowmbGLCode").html(data);
                }
            });

        });
    });

     $(function(){
     	"use strict";
        $(".datepicker").datepicker({ dateFormat:'yy-mm-dd' });
       
    });

</script>
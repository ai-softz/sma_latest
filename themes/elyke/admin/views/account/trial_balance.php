<?php $this->load->view('./admin/views/includes/head') ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            
            <div class="panel-body">
                <?php echo  form_open_multipart('admin/account/trial_balance_report') ?>
                <div class="row" id="">
                    <div class="col-sm-6">
     
                        <div class="form-group row">
                            <label for="date" class="col-sm-4 col-form-label"><?php echo $this->lang->line('from_date') ?><span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="dtpFromDate" value="<?php date('Y-m-d')?>" placeholder="<?php echo $this->lang->line('from_date') ?>" class="datepicker form-control" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date" class="col-sm-4 col-form-label"><?php echo $this->lang->line('to_date') ?><span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  name="dtpToDate" value="<?php date('Y-m-d')?>" placeholder="<?php echo $this->lang->line('to_date') ?>" class="datepicker form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <input type="checkbox" id="chkWithOpening" name="chkWithOpening" size="40"/>&nbsp;&nbsp;&nbsp;<label for="chkWithOpening"><?php echo $this->lang->line('with_details') ?></label>
                            </div>
                        </div>

                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5"><?php echo $this->lang->line('find') ?></button>
                        </div>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
<!--start new-->
<?php $this->load->view('./admin/views/includes/js') ?>
<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_holiday'); ?></h4>
</div>

<div class="modal-body">
		<div class="row">
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Event Name</th><td><?= $data->event_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Start Date</th><td><?= date('d M, Y', strtotime($data->start_date));  ?></td>
	                      </tr>
						  <tr>
	                      	<th>End Date</th><td><?= date('d M, Y', strtotime($data->end_date));  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Status</th><td><?= $data->is_publish == 1 ? 'Published' : 'Un Published' ?></td>
	                      </tr>
	                      <tr>
	                      	<th>description</th><td><?= $data->description ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		  </div>
</div>
<div class="modal-footer">
    <button class="delete_event btn btn-danger" data-id="<?= $data->holiday_id ?>" type="button">Delete</button>
</div>

<script> 
    $(document).ready(function(){

        $(document).on('click', '.delete_event', function(){
            var id = $(this).data('id');
            $.ajax({
                url: site.base_url + 'timesheets/delete_holiday/' + id,
                method: 'GET',
                dataType: 'text',
                success : function() {
                    $('.cal_modal').modal('hide');
                    location.reload();
                }
            });
        });
    });
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>


<?php
$fdate = $_GET['event_date'];
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_events'); ?></h4>
    </div>

    <div class="modal-body">

        <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_events']; ?>
		     <?php echo admin_form_open_multipart("core_hr/add_events", $attrib) ?>
		     
		    <div class="form-group row">
                <div class="col-md-6">
                    <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger">*</span></label>
                    <select name="user_ids[]" class="js-example-basic-multiple" multiple="multiple" style="width: 100%" id="" required>
                        <?php foreach($all_employees as $row) { ?>
                            <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                        <?php } ?>
                    </select>
                </div>  
                <div class="col-md-6">
                    <label for="event_title"><?php echo  lang('event_title') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('event_title');?>" name="event_title" id="event_title" type="text" value="" required="required">
                </div>
		      
		   </div>

		   <div class="form-group row">
		    <div class="col-md-6">
		         <label for="event_date"><?php echo  lang('event_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('event_date');?>" name="event_date" type="text" value="<?= date('d-m-Y', strtotime($fdate)); ?>" required="required">
		    </div> 
            <div class="col-md-6">
                <label for="event_time"><?php echo  lang('event_time') ?> <span class="text-danger">*</span></label>
		         <input class="form-control " placeholder="<?php echo lang('event_time');?>" name="event_time" type="text" value="" required="required">
            </div> 
		   </div>
		   <div class="form-group row">

		      <div class="col-md-12">
		        <label for="event_note"><?php echo  lang('event_note') ?> <span class="text-danger"></span></label>
		        <textarea name="event_note" class="form-control" id="event_note"></textarea>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_events', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>
	



<script>
$(document).ready(function(){


$('#add_events').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "core_hr/add_events",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('.cal_modal').modal('hide');
                location.reload();
             }
               
      });  
  });

  $('.js-example-basic-multiple').select2();

});
</script>
<?php
$fdate = $_GET['event_date'];
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_holiday'); ?></h4>
    </div>

    <div class="modal-body">

        <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_holiday']; ?>
		     <?php echo admin_form_open_multipart("core_hr/add_holiday", $attrib) ?>
		     
		    <div class="form-group row">
		      <div class="col-md-6">
		         <label for="event_name"><?php echo  lang('event_name') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('event_name');?>" name="event_name" id="event_name" type="text" value="" required="required">
		      </div>
		      <div class="col-md-6">
		         <label for="start_date"><?php echo  lang('start_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('start_date');?>" name="start_date" type="text" value="<?= date('d-m-Y', strtotime($fdate)); ?>" required="required">
		    </div> 
		   </div>

		   <div class="form-group row">
		    <div class="col-md-6">
		         <label for="end_date"><?php echo  lang('end_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('end_date');?>" name="end_date" type="text" value="<?= date('d-m-Y', strtotime($fdate)); ?>" required="required">
		    </div> 
            <div class="col-md-6">
                <label for="is_publish"><?php echo  lang('is_publish') ?> <span class="text-danger"></span></label>
                <select name="is_publish" class="form-control select" id="">
                        <option value="1"> <?= lang('published') ?> </option>
                        <option value="0"> <?= lang('un_published') ?> </option>
                </select>
            </div> 
		   </div>
		   <div class="form-group row">

		      <div class="col-md-12">
		        <label for="post"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
		        <textarea name="description" class="form-control" id="description"></textarea>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_holiday', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>
	
    <script>

$(document).ready(function(){

$('#add_holiday').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "core_hr/add_holiday",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	  $('.cal_modal').modal('hide');
                location.reload();
             }
               
      });  
  });


});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/add_complaint", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="complaint_from"><?php echo  lang('complaint_from') ?> <span class="text-danger"></span></label>
                <select name="complaint_from" class="form-control select" id="complaint_from">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
                <label for="complaint_against"><?php echo  lang('complaint_against') ?> <span class="text-danger"></span></label>
                <select name="complaint_against[]" class="selectpicker" multiple data-live-search="true" style="width: 100%" id="complaint_against" required>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div> 
			
	   </div>
	   <div class="form-group row">
            <div class="col-md-6">
                <label for="title"><?php echo  lang('title') ?> <span class="text-danger"></span></label>
                <input name="title" type="text" value="" class="form-control" required="required">
            </div>  
            <div class="col-md-6">
				<label for="complaint_date"><?php echo  lang('complaint_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('complaint_date');?>" name="complaint_date" type="text" value="" required="required" autocomplete="off">
			</div>
	   </div>
	   <div class="form-group row"> 
            <div class="col-md-6">
                <label for="attachment"><?php echo  lang('attachment') ?> <span class="text-danger">*</span></label>
                <input type="file" name="attachment" id="attachment" size="20" />
            </div> 
			<div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"></textarea>
			</div>  			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('add_complaint', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/add_employee_exit", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee_id') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
                <label for="exit_type"><?php echo  lang('exit_type') ?> <span class="text-danger"></span></label>
                <select name="exit_type" class="form-control select" id="exit_type">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($employee_exit_type as $row) { ?>
                        <option value="<?php echo $row->exit_type_id; ?>"><?php echo $row->type ;?></option>
                    <?php } ?>
                </select>
	        </div> 
			
	   </div>
	   <div class="form-group row">
            <div class="col-md-6">
				<label for="exit_date"><?php echo  lang('exit_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('exit_date');?>" name="exit_date" type="text" value="" required="required" autocomplete="off">
			</div>
            <div class="col-md-6">
                <label for="exit_interview"><?php echo  lang('exit_interview') ?> <span class="text-danger"></span></label>
                <select name="exit_interview" class="form-control select" id="exit_interview">
                    <option value="1"> <?= lang('yes') ?> </option>
                    <option value="0"> <?= lang('no') ?> </option>
                </select>
	        </div> 
	   </div>
       <div class="form-group row">
            
            <div class="col-md-6">
                <label for="is_inactivate_account"><?php echo  lang('disable_account') ?> <span class="text-danger"></span></label>
                <select name="is_inactivate_account" class="form-control select" id="is_inactivate_account">
                    <option value="1"> <?= lang('yes') ?> </option>
                    <option value="0"> <?= lang('no') ?> </option>
                </select>
	        </div> 
            <div class="col-md-6">
				<label for="reason"><?php echo  lang('reason') ?> <span class="text-danger"></span></label>
				<textarea name="reason" class="form-control"></textarea>
			</div> 
        </div>
	   <div class="form-group row"> 

	   </div>

         <div class="form-group">
			 <?php echo form_submit('add_employee_exit', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

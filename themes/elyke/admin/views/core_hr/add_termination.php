<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/add_termination", $attrib) ?>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee_id') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
                <label for="termination_type"><?php echo  lang('termination_type') ?> <span class="text-danger"></span></label>
                <select name="termination_type_id" class="form-control select" id="termination_type">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($termination_type as $row) { ?>
                        <option value="<?php echo $row->termination_type_id; ?>"><?php echo $row->type ;?></option>
                    <?php } ?>
                </select>
	        </div> 
			
	   </div>
	   <div class="form-group row">
            <div class="col-md-6">
				<label for="notice_date"><?php echo  lang('notice_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('notice_date');?>" name="notice_date" type="text" value="" required="required" autocomplete="off">
			</div>
            <div class="col-md-6">
				<label for="termination_date"><?php echo  lang('termination_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('termination_date');?>" name="termination_date" type="text" value="" required="required" autocomplete="off">
			</div>
	   </div>
       <div class="form-group row">
            <div class="col-md-6">
                <label for="attachment"><?php echo  lang('attachment') ?> <span class="text-danger">*</span></label>
                <input type="file" name="attachment" id="attachment" size="20" />
            </div>
            <div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"></textarea>
			</div> 
        </div>
	   <div class="form-group row"> 	 			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('add_termination', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

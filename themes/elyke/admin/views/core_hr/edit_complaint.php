<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/editComplaint", $attrib) ?>
        <input type="hidden" name="complaint_id" value="<?= $complaint->complaint_id ?>">
        <div class="form-group row">
            <div class="col-md-6">
                <label for="complaint_from"><?php echo  lang('complaint_from') ?> <span class="text-danger"></span></label>
                <select name="complaint_from" class="form-control select" id="complaint_from">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>" <?= $row->user_id == $complaint->complaint_from ? 'Selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
                <label for="complaint_against"><?php echo  lang('complaint_against') ?> <span class="text-danger"></span></label>
                <select name="complaint_against[]" class="selectpicker" multiple data-live-search="true" style="width: 100%" id="complaint_against" required>
                <?php $employee_arr = explode(',', $complaint->complaint_against) ?>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>" <?= in_array($row->user_id, $employee_arr) ? 'selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div> 
			
	   </div>
	   <div class="form-group row">
            <div class="col-md-6">
                <label for="title"><?php echo  lang('title') ?> <span class="text-danger"></span></label>
                <input name="title" type="text" value="<?= $complaint->title ?>" class="form-control" required="required">
            </div>  
            <div class="col-md-6">
				<label for="complaint_date"><?php echo  lang('complaint_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('complaint_date');?>" name="complaint_date" type="text" value="<?= date('d-m-Y', strtotime($complaint->complaint_date)) ?>" required="required" autocomplete="off">
			</div>
	   </div>
	   <div class="form-group row"> 
            <div class="col-md-6">
                <label for="status"><?php echo  lang('status') ?> <span class="text-danger"></span></label>
                <select name="status" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <option value="0" <?= $complaint->status == 0 ? 'Selected' : '' ?>> <?= lang('pending') ?> </option>
                        <option value="1" <?= $complaint->status == 1 ? 'Selected' : '' ?>> <?= lang('approved') ?> </option>
                        <option value="2" <?= $complaint->status == 1 ? 'Selected' : '' ?>> <?= lang('rejected') ?> </option>
                </select>
            </div> 
			<div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"><?= $complaint->description ?></textarea>
			</div>  			
	   </div>
       <div class="form-group row"> 
            <div class="col-md-6">
                <label for="attachment"><?php echo  lang('attachment') ?> <span class="text-danger">*</span></label>
                <input type="file" name="attachment" id="attachment" size="20" />
            </div> 	
            <div class="col-md-2">
                <img src="<?= base_url('assets/uploads/complaints/'.$complaint->attachment) ?>" width="50%">
            </div>
        </div>                
         <div class="form-group">
			 <?php echo form_submit('edit_complaint', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

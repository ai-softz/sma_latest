<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_meetings'); ?></h4>
    </div>

    <div class="modal-body">

    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_meetings']; ?>
		     <?php echo admin_form_open_multipart("core_hr/edit_meetings", $attrib) ?>
             <input type="hidden" name="meeting_id" value="<?= $meeting->meeting_id ?>">
		     
		    <div class="form-group row">
                <div class="col-md-6">
                    <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger">*</span></label>
                    <select name="user_ids[]" class="js-example-basic-multiple" multiple="multiple" style="width: 100%" id="" required>
                    <?php $employee_arr = explode(',', $meeting->employee_id) ?>
                        <?php foreach($all_employees as $row) { ?>
                            <option value="<?php echo $row->user_id; ?>" <?= in_array($row->user_id, $employee_arr) ? 'selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                        <?php } ?>
                    </select>
                </div>  
                
                <div class="col-md-6">
                    <label for="meeting_title"><?php echo  lang('meeting_title') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('meeting_title');?>" name="meeting_title" id="meeting_title" type="text" value="<?= $meeting->meeting_title ?>" required="required">
                </div>
		      
		   </div>

		   <div class="form-group row">
		    <div class="col-md-6">
		         <label for="meeting_date"><?php echo  lang('meeting_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('meeting_date');?>" name="meeting_date" type="text" value="<?= date('d-m-Y', strtotime($meeting->meeting_date)) ?>" required="required">
		    </div> 
            <div class="col-md-6">
                <label for="meeting_time"><?php echo  lang('meeting_time') ?> <span class="text-danger">*</span></label>
		         <input class="form-control " placeholder="<?php echo lang('meeting_time');?>" name="meeting_time" type="text" value="<?= $meeting->meeting_time ?>" required="required">
            </div> 
		   </div>
		   <div class="form-group row">
           
                <div class="col-md-6">
                    <label for="meeting_room"><?php echo  lang('meeting_room') ?> <span class="text-danger">*</span></label>
                    <input class="form-control " placeholder="<?php echo lang('meeting_room');?>" name="meeting_room" type="text" value="<?= $meeting->meeting_room ?>" >
                </div>
		      <div class="col-md-12">
		        <label for="meeting_note"><?php echo  lang('meeting_note') ?> <span class="text-danger"></span></label>
		        <textarea name="meeting_note" class="form-control" id="meeting_note"><?= $meeting->meeting_note ?></textarea>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_meetings', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#edit_meetings').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "core_hr/edit_meetings",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('#myModal').modal('hide');
                location.reload();
                // $('#table_events').DataTable().ajax.reload();
             }
               
      });  
  });


});
</script>
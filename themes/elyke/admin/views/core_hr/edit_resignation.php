<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/editResignation", $attrib) ?>
        <input type="hidden" name="resignation_id" value="<?= $resignation->resignation_id ?>">
        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="leave_employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>" <?= $row->user_id == $resignation->employee_id ? 'Selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
			<div class="col-md-6">
				<label for="notice_date"><?php echo  lang('notice_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('notice_date');?>" name="notice_date" type="text" value="<?= date('d-m-Y', strtotime($resignation->notice_date)) ?>" required="required" autocomplete="off">
			</div>
	   </div>
	   <div class="form-group row">
	   	<div class="col-md-6">
           <label for="resignation_date"><?php echo  lang('resignation_date') ?> <span 	class="text-danger">*</span></label>
			<input class="form-control date" placeholder="<?php echo lang('resignation_date');?>" name="resignation_date" type="text" value="<?= date('d-m-Y', strtotime($resignation->resignation_date)) ?>" required="required" autocomplete="off">
	    </div> 
        <div class="col-md-6">
                <label for="status"><?php echo  lang('status') ?> <span class="text-danger"></span></label>
                <select name="status" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <option value="0" <?= $resignation->status == 0 ? 'Selected' : '' ?>> <?= lang('pending') ?> </option>
                        <option value="1" <?= $resignation->status == 1 ? 'Selected' : '' ?>> <?= lang('approved') ?> </option>
                        <option value="2" <?= $resignation->status == 1 ? 'Selected' : '' ?>> <?= lang('rejected') ?> </option>
                </select>
        </div> 
	    
	   </div>
	   <div class="form-group row"> 
            
			<div class="col-md-6">
				<label for="reason"><?php echo  lang('reason') ?> <span class="text-danger"></span></label>
				<textarea name="reason" class="form-control"> <?= $resignation->reason ?> </textarea>
			</div>  			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('edit_resignation', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

 

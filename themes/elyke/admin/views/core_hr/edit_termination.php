<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/editTermination", $attrib) ?>
        <input type="hidden" name="termination_id" value="<?= $termination->termination_id ?>">
        <div class="form-group row">
            <div class="col-md-6">
                <label for="employee_id"><?php echo  lang('employee_id') ?> <span class="text-danger"></span></label>
                <select name="employee_id" class="form-control select" id="employee_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>" <?= $row->user_id == $termination->employee_id ? 'Selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6">
                <label for="termination_type"><?php echo  lang('termination_type') ?> <span class="text-danger"></span></label>
                <select name="termination_type_id" class="form-control select" id="termination_type">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($termination_type as $row) { ?>
                        <option value="<?php echo $row->termination_type_id; ?>" <?= $row->termination_type_id == $termination->termination_type_id ? 'Selected' : '' ?>><?php echo $row->type ;?></option>
                    <?php } ?>
                </select>
	        </div> 
			
	   </div>
	   <div class="form-group row">
            <div class="col-md-6">
				<label for="notice_date"><?php echo  lang('notice_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('notice_date');?>" name="notice_date" type="text" value="<?= date('d-m-Y', strtotime($termination->notice_date)) ?>" required="required" autocomplete="off">
			</div>
            <div class="col-md-6">
				<label for="termination_date"><?php echo  lang('termination_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('termination_date');?>" name="termination_date" type="text" value="<?= date('d-m-Y', strtotime($termination->termination_date)) ?>" required="required" autocomplete="off">
			</div>
	   </div>
       <div class="form-group row">
            <div class="col-md-6">
                <label for="attachment"><?php echo  lang('attachment') ?> <span class="text-danger">*</span></label>
                <input type="file" name="attachment" id="attachment" size="20" />
            </div>
            <div class="col-md-2">
                <img src="<?= base_url('assets/uploads/terminations/'.$termination->attachment) ?>" width="50%">
            </div>	
             
            
        </div>
	   <div class="form-group row"> 
       <div class="col-md-6">
            <label for="status"><?php echo  lang('status') ?> <span class="text-danger"></span></label>
                <select name="status" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <option value="0" <?= $termination->status == 0 ? 'Selected' : '' ?>> <?= lang('pending') ?> </option>
                        <option value="1" <?= $termination->status == 1 ? 'Selected' : '' ?>> <?= lang('approved') ?> </option>
                        <option value="2" <?= $termination->status == 1 ? 'Selected' : '' ?>> <?= lang('rejected') ?> </option>
                </select>
            </div>
            <div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"> <?= $termination->description ?></textarea>
			</div> 	 			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('edit_termination', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){


    // $('.date-picker').datepicker( {
    //     changeMonth: true,
    //     changeYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) { 
    //         $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    //     }
    // });

});

    </script>

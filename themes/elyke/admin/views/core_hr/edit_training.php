<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

    <div class="panel">
        
        <div class="panel-body">
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
        <?php echo admin_form_open_multipart("core_hr/editTraining", $attrib) ?>
        <input type="hidden" name="training_id" value="<?= $training->training_id ?>">
        <div class="form-group row">
            <div class="col-md-6"> 
                <label for="training_title"><?php echo  lang('training_title') ?> <span class="text-danger">*</span></label>
                <input type="text" name="training_title" class="form-control" required="required" value="<?= $training->training_title ?>">
            </div>  
            <div class="col-md-6">
            <label for="employee_id"><?php echo  lang('employee_id') ?> <span class="text-danger"></span></label>
                <select name="user_ids[]" class="selectpicker" multiple data-live-search="true" style="width: 100%" id="employee_id" required>
                <?php $employee_arr = explode(',', $training->employee_id) ?>
                    <?php foreach($all_employees as $row) { ?>
                        <option value="<?php echo $row->user_id; ?>" <?= in_array($row->user_id, $employee_arr) ? 'selected' : '' ?>><?php echo $row->first_name. ' '.$row->last_name ;?></option>
                    <?php } ?>
                </select>
	        </div>  
            
			
	   </div>
	   <div class="form-group row">
            <div class="col-md-6">
				<label for="start_date"><?php echo  lang('start_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('start_date');?>" name="start_date" type="text" value="<?= date('d-m-Y', strtotime($training->start_date)) ?>" required="required" autocomplete="off">
			</div>   
            <div class="col-md-6">
				<label for="finish_date"><?php echo  lang('finish_date') ?> <span 	class="text-danger">*</span></label>
				<input class="form-control date" placeholder="<?php echo lang('finish_date');?>" name="finish_date" type="text" value="<?= date('d-m-Y', strtotime($training->finish_date)) ?>" required="required" autocomplete="off">
			</div> 
            
	   </div>
       
       <div class="form-group row">
       <div class="col-md-6">
                <label for="trainer_id"><?php echo  lang('trainer_id') ?> <span class="text-danger"></span></label>
                <select name="trainer_id" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($trainers as $value) { ?>
                            <option value="<?php echo $value->trainer_id; ?>" <?= $value->trainer_id == $training->trainer_id ? 'Selected' : '' ?>><?php echo $value->first_name.' '.$value->last_name;?></option>
                        <?php } ?>
                </select>
	        </div>  
            <div class="col-md-6"> 
                <label for="training_cost"><?php echo  lang('training_cost') ?> <span class="text-danger"></span></label>
                <input type="text" name="training_cost" class="form-control" value="<?= $training->training_cost ?>">
            </div>
            
        </div>
       <div class="form-group row"> 
       <div class="col-md-6">
                <label for="trainer_option"><?php echo  lang('trainer_option') ?> <span class="text-danger"></span></label>
                <?php
                    $trainer_option = array(
                        1 => "Internal", 
                        2 => "External",
                    );
                ?>
                <select name="trainer_option" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($trainer_option as $key=>$value) { ?>
                            <option value="<?php echo $key; ?>" <?= $key == $training->trainer_option ? 'Selected' : '' ?>><?php echo $value;?></option>
                        <?php } ?>
                </select>
	        </div> 
            
            <div class="col-md-6">
				<label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
				<textarea name="description" class="form-control"><?= $training->description ?></textarea>
			</div>            
       </div>
       
	   <div class="form-group row"> 
			 			
	   </div>

         <div class="form-group">
			 <?php echo form_submit('edit_training', lang('save'), 'class="btn btn-primary"'); ?>

         </div>
         <?php echo form_close() ?>
        </div>
    </div>

    <script>

$(document).ready(function(){

});

    </script>

<a href="<?= admin_url('core_hr/add_employee_exit') ?>" class="btn btn-primary">Add Employee Exit</a>
<br>
<div class="table-responsive">
 <table id="table_employee_exit" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('exit_type');?></th>
          <th><?php echo lang('exit_date');?></th>
          <th><?php echo lang('exit_interview');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {
    
      oTable = $('#table_employee_exit').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('core_hr/getEmployeeExit') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
                $('td:eq(4)', nRow).html(type(nRow, aData));
                return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

    function type(nRow, aData) {
       var status ='';
       if(aData[4] == 0) {
          status = 'No';
       } else if(aData[4] == 1) {
          status = 'Yes';
       } 
       return status;
    }

</script>
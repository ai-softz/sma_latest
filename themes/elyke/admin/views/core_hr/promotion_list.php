<a href="<?= admin_url('core_hr/add_promotion') ?>" class="btn btn-primary">Add Promotion</a>
<br>
<div class="table-responsive">
 <table id="table_transfers" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('name');?></th>
          <th><?php echo lang('promotion_title');?></th>
          <th><?php echo lang('promoted_to');?></th>
          <th><?php echo lang('promotion_date');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {
    
      oTable = $('#table_transfers').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('core_hr/getPromotions') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            //    $('td:eq(3)', nRow).html(transfer_status(nRow, aData));
            //    return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

    function transfer_status(nRow, aData) {
       var status ='';
       if(aData[3] == 0) {
          status = 'Pending';
       } else if(aData[3] == 1) {
          status = 'Approved';
       } else if(aData[3] == 2) {
          status = 'Rejected';
       }
       return status;
    }
   

</script>
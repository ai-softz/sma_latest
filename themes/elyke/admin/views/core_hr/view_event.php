<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_event'); ?></h4>
        </div>
        <div class="modal-body">

		<div class="row">
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
                        <tr>
	                      	<th>Employees</th><td>
                               <?= 
                               $data->event_title 
                               ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Event Title</th>
                            <td>
                                <?= $data->event_title ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Event Date</th><td><?= date('d M, Y', strtotime($data->event_date));  ?></td>
	                      </tr>
						  <tr>
	                      	<th>End Time</th><td><?= $data->event_time;  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Event Note</th><td><?= $data->event_note ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		  </div>

	</div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


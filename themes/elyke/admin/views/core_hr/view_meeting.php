<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_meeting'); ?></h4>
        </div>
        <div class="modal-body">

		<div class="row">
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
                        <tr>
	                      	<th>Employees</th><td>
                               <?php
                               $names = '';
                               if(!empty($data->employee_id)) {
                                    $employees = explode(',', $data->employee_id);
                                    $name_arr = array();
                                    foreach($employees as $value) {
                                        $name = $this->employees_model->get_emp_name($value);
                                        $name_arr[] = $name->first_name.' '.$name->last_name; 
                                    } 
                                    if(!empty($name_arr)) {
                                            $names = implode(',', $name_arr);
                                    }
                                }
                               echo $names;
                               ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Meeting Title</th>
                            <td>
                                <?= $data->meeting_title ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Meeting Date</th><td><?= date('d M, Y', strtotime($data->meeting_date));  ?></td>
	                      </tr>
						  <tr>
	                      	<th>Meeting Time</th><td><?= $data->meeting_time;  ?></td>
	                      </tr>
						  <tr>
	                      	<th>Meeting Room</th><td><?= $data->meeting_room;  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Meeting Note</th><td><?= $data->meeting_note ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		</div>

	</div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_termination'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
		  
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Employee Terminated</th><td><?= $employee->first_name .' '.$employee->last_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Notice Date</th><td><?= date('d M, Y', strtotime($termination->notice_date));  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Termination Date</th><td><?= date('d M, Y', strtotime($termination->termination_date));  ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Termination Type</th><td><?= $termination_type->type; ?></td>
	                      </tr>
                          <tr>
	                      	<th>Status</th>
							  	<td>
								  <?php
								  if($termination->status == 0) {
									  echo lang('pending');
								  } 
								  if($termination->status == 1) {
									  echo lang('approved');
								  } 
								  if($termination->status == 2) {
									  echo lang('rejected');
								  } 
								  ?>
								</td>
	                      </tr>
	                      <tr>
	                      	<th>Attachment</th>
                              <td> 
                                  <img src="<?= base_url('assets/uploads/terminations/'.$termination->attachment); ?>" width="30%">
                                  <a href="<?php echo base_url('assets/uploads/terminations/'.$termination->attachment); ?>" target="_blank"> <?= 'Download' ?> </a> 
                              </td>
	                      </tr>
                          
	                      <tr>
	                      	<th>Description</th><td><?= $termination->description ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


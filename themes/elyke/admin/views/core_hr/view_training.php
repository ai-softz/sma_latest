<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('view_award'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

            <div class="row">
		  <div class="col-md-12">
          <div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
                        
	                      <tr>
	                      	<th>Training Title</th>
                            <td>
                                <?= $data->training_title ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Start Date</th><td><?= date('d M, Y', strtotime($data->start_date));  ?></td>
	                      </tr>
						  <tr>
	                      	<th>End Date</th><td><?= date('d M, Y', strtotime($data->finish_date));  ?></td>
	                      </tr>
						  <tr>
	                      	<th>Employees</th><td>
                               <?php
                               
                               echo $emp_names;
                               ?>
                            </td>
	                      </tr>
	                      <tr>
	                      	<th>Description</th><td><?= $data->description ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


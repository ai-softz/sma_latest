<a href="<?= admin_url('core_hr/add_warning') ?>" class="btn btn-primary">Add Warning</a>
<br>
<div class="table-responsive">
 <table id="table_warnings" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('warning_to');?></th>
          <th><?php echo lang('warning_type_id');?></th>
          <th><?php echo lang('subject');?></th>
          <th><?php echo lang('warning_date');?></th>
          <th><?php echo lang('warning_by');?></th>
          <th><?php echo lang('status');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {
    
      oTable = $('#table_warnings').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('core_hr/getWarnings') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
               $('td:eq(6)', nRow).html(transfer_status(nRow, aData));
                return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

    function transfer_status(nRow, aData) {
       var status ='';
       if(aData[6] == 0) {
          status = 'Pending';
       } else if(aData[6] == 1) {
          status = 'Approved';
       } else if(aData[6] == 2) {
          status = 'Rejected';
       }
       return status;
    }

</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel">
			<?php echo validation_errors(); ?>
                <div class="panel-body">
                <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; ?>
                <?php echo admin_form_open_multipart("employees/add_employee", $attrib) ?>
              <div class="form-group row">          
	                    <div class="col-md-3">
	                    	<label for="customer_name"><?php echo  lang('first_name') ?> <span class="text-danger">*</span></label>
	                       <input class="form-control" placeholder="<?php echo lang('first_name');?>" name="first_name" id="first_name" type="text" value="" required="required">
	                    </div>
	                    <div class="col-md-3">
	                    	<label for="customer_name"><?php echo  lang('last_name') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('last_name');?>" name="last_name" type="text" value="" required="required">
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="customer_name"><?php echo  lang('employee_id') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('employee_id');?>" name="employee_id" type="text" value="<?php //echo $employee_id;?>" required="required">
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="customer_name"><?php echo  lang('employee_doj') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control date" placeholder="<?php echo lang('employee_doj');?>" name="date_of_joining" type="text" value="<?php echo date('d-m-Y');?>" required="required">
	           	 		</div>
               </div>

               <div class="form-group row">          
	                    <div class="col-md-3">
	                    	<label for="warehouse"><?php echo  lang('warehouse') ?> <span class="text-danger">*</span></label>
	                       <select name="warehouse_id" id="warehouse" class="form-control select">
                      			<option value=""><?php echo 'Select';?></option>
                      			<?php foreach($all_warehouses as $warehouse) { ?>
                      				<option value="<?php echo $warehouse->id; ?>"><?php echo $warehouse->name; ?></option>
                      			<?php } ?>
                   			</select>
	                    </div>
	                    <div class="col-md-3">
	                    	<label for="department_id"><?php echo  lang('department_id') ?> <span class="text-danger">*</span></label>
	                        <select name="department_id" id="department" class="form-control select" required="required">
                      			<option value=""><?php echo 'Select';?></option>
                      			<?php foreach($all_departments as $row) { ?>
                      				<option value="<?php echo $row->department_id; ?>"><?php echo $row->department_name; ?></option>
                      			<?php } ?> 
                   			</select>
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="designation"><?php echo  lang('designation') ?> <span class="text-danger">*</span></label>
	           	 			<div id="designation_area">
	                        <select name="designation_id" id="designation" class="form-control select" required="required">
                      			<option value=""><?php echo 'Select';?></option>
                   			</select>
                   			</div>
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="date_of_birth"><?php echo  lang('date_of_birth') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control date" placeholder="<?php echo lang('date_of_birth');?>" name="date_of_birth" type="text" value="">
	           	 		</div>
               </div>

               <div class="form-group row">          
	                    <div class="col-md-3">
	                    	<label for="username"><?php echo  lang('username') ?> <span class="text-danger">*</span></label>
	                       <input class="form-control" placeholder="<?php echo lang('username');?>" name="username" type="text" value="">
	                    </div>
	                    <div class="col-md-3">
	                    	<label for="email"><?php echo  lang('email') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('email');?>" name="email" type="text" value="">
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="password"><?php echo  lang('password') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('password');?>" name="password" type="text" value="">
	           	 		</div>
	           	 		<div class="col-md-3">
	           	 			<label for="confirm_password"><?php echo  lang('confirm_password') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('confirm_password');?>" name="confirm_password" type="text" value="">
	           	 		</div>
               </div>

               <div class="form-group row">  
               			<div class="col-md-3">
	           	 			<label for="contact_no"><?php echo  lang('contact_no') ?> <span class="text-danger">*</span></label>
	                        <input class="form-control" placeholder="<?php echo lang('contact_no');?>" name="contact_no" type="text" value="">
	           	 		</div> 

                     	<div class="col-md-3">
	                    	<label for="role"><?php echo  lang('role') ?> <span class="text-danger">*</span></label>
	                    	<select name="role" class="form-control select";?>">
	                       <option value=""><?php echo 'Select';?></option>
	                       <?php foreach($all_groupemp as $row) { ?>
                      				<option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                      		<?php } ?> 
                      		</select>
	                    </div>

	                    <div class="col-md-3">
	                    	<label for="gender"><?php echo  lang('gender') ?> <span class="text-danger"></span></label>
	                    	<select name="gender" class="form-control select">
	                       <option value="Male"><?php echo lang('gender_male');?></option>
                      		<option value="Female"><?php echo lang('gender_female');?></option>
                      		</select>
	                    </div>
	                    <!-- <div class="col-md-3">
	                    	<label for="office_shift"><?php echo  lang('office_shift') ?> <span class="text-danger"></span></label>
	                       <select name="office_shift_id" class="form-control select">
	                       <option value=""><?php echo 'Select';?></option>
	                       <?php foreach($all_office_shifts as $row) { ?>
                      				<option value="<?php echo $row->office_shift_id; ?>"><?php echo $row->shift_name; ?></option>
                      		<?php } ?> 
                      		</select>
	           	 		</div> -->
						<div class="col-md-3">
	           	 			<label for="address"><?php echo  lang('address') ?> <span class="text-danger"></span></label>
	                        <input type="text" class="form-control" placeholder="<?php echo lang('address');?>" name="address">
	           	 		</div>	
	           	 		
               </div>

               <div class="form-group row">          
	                    <div class="col-md-3">
	                    	<label for="reports_to"><?php echo  lang('reports_to') ?> <span class="text-danger"></span></label>
	                       <select name="reports_to" class="form-control select";?>">
	                       <option value=""><?php echo 'Select';?></option>
	                       <?php foreach($all_employees as $row) { ?>
                      				<option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name.' '.$row->last_name; ?></option>
                      		<?php } ?> 
                      		</select>
	                    </div>
	                    <!-- <div class="col-md-3">
	                    	<label for="leave_categories"><?php echo  lang('leave_categories') ?> <span class="text-danger"></span></label>
		                       <select name="leave_categories[]" class="selectpicker" multiple data-live-search="true" style="width: 100%">
		                         <?php foreach($leave_categories as $row) { ?> 
		                          <option value="<?= $row['leave_type_id'] ?>"><?= $row['type_name'] ?></option>
		                          <?php } ?>
		                        </select>
		                   
	           	 		</div> -->
	           	 		<div class="col-md-3">
	           	 			<label for="pin_code"><?php echo  lang('pin_code') ?> <span class="text-danger"></span></label>
	                        <input class="form-control" placeholder="<?php echo $this->lang->line('pin_code');?>" name="pin_code" type="text" value="<?php //echo $pin_code;?>">
	           	 		</div>
	           	 		
               </div>

	            <div class="form-group">
	    			 <?php echo form_submit('add_employee', lang('add_employee'), 'class="btn btn-primary"'); ?>

	             </div>
	             <?php echo form_close() ?>

            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){

	$(document).on('change', '#department', function(){
		var dept = $(this).val();
		$.ajax({
			url: site.base_url + 'employees/getDesignations',
			method: 'POST',
			data: {dept: dept},
			dataType: 'text',
			success: function(data) {
				$('#designation').remove();
				$('#designation_area').html(data);
			}
		});
	});
});

</script>
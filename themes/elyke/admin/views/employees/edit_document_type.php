<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_document_type'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">

  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_document_type']; ?>
     <?php echo admin_form_open_multipart("employees/edit_document_type", $attrib) ?>
     <input type="hidden" name="document_type_id" value="<?= $document_type->document_type_id ?>">
     <div class="form-group row">
      
      <div class="col-md-6">
         <label for="title"><?php echo  lang('document_type') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('title');?>" name="document_type" type="text" value="<?= $document_type->document_type ?>" required="required">
      </div>
      <div class="col-md-6">
         <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="secondary_document_type" type="text" value="<?= $document_type->secondary_document_type ?>" required="required">
      </div>
   </div>

   <div class="form-group">
        <?php echo form_submit('edit_document_type', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_document_type').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/editDocumentType",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('#myModal').modal('hide');
                // $('#table_document_type').DataTable().ajax.reload();
                location.reload();
             }
               
      });  
  });

});
</script>
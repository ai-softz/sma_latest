<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_emergency_contact'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

          <div class="row">

		  <hr class="border-light">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_emergen_contact']; ?>
		     <?php echo admin_form_open_multipart("employees/add_working_experience", $attrib) ?>
		     <input type="hidden" name="contact_id" value="<?= $contact_info->contact_id ?>">
		    <div class="form-group row">
		      <div class="col-md-6">
		         <div class="row">
		          <div class="col-md-12">
		           <label for="relation"><?php echo  lang('relation') ?> <span class="text-danger"></span></label>
           <?php
              $relations = array(
                  'Self' => 'Self',
                  'Parent' => 'Parent',
                  'Spouse' => 'Spouse',
                  'Child' => 'Child',
                  'Sibling' => 'Sibling',
                  'in Laws' => 'in Laws',
              );
            ?>
           <select name="relation" id="language" class="form-control select">
              <option value=""><?php echo 'Select';?></option>
              <?php foreach($relations as $key => $value) { ?>
                <option value="<?php echo $key; ?>" <?= $contact_info->relation == $key ? 'Selected' : '' ?>><?php echo $value; ?></option>
              <?php } ?>
           </select>
            </div>
          </div>
          <div class="row" style="margin-top: 15px;">
            <div class="col-md-12">
              <input type="checkbox" name="is_primary" <?= $contact_info->is_primary == 1 ? "checked" : "" ?>>  Primary Contact
              <input type="checkbox" name="is_dependent" <?= $contact_info->is_dependent == 1 ? "checked" : "" ?>>  Dependent
            </div>
          </div>
	      </div>
		      <div class="col-md-6">
		         <label for="contact_name"><?php echo  lang('contact_name') ?> <span class="text-danger">*</span></label>
		         <input class="form-control" placeholder="<?php echo lang('contact_name');?>" name="contact_name" type="text" value="<?= $contact_info->contact_name ?>" required="required">
		      </div>
		   </div>

	   	<div class="form-group row">
	    	 <div class="col-md-6">
	         <label for="phone"><?php echo  lang('phone') ?> <span class="text-danger">*</span></label>
	            <div class="row">
	              <div class="col-md-9">
	                <input class="form-control" placeholder="<?php echo lang('work_phone');?>" name="work_phone" type="text" value="<?= $contact_info->work_phone ?>" required="required">
	              </div>
	              <div class="col-md-3">
	                <input class="form-control" placeholder="<?php echo lang('ext');?>" name="work_phone_extension" type="text" value="<?= $contact_info->work_phone_extension ?>">
	              </div>
	            </div>
	            
	    </div>  
	    <div class="col-md-6">
	         <label for="home_phone"><?php echo  lang('home_phone') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('home_phone');?>" name="home_phone" type="text" value="<?= $contact_info->home_phone ?>">
	    </div>   
	   </div>
	   <div class="form-group row">
	      <div class="col-md-6">
	        <label for="mobile_phone"><?php echo  lang('mobile_phone') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('mobile_phone');?>" name="mobile_phone" type="text" value="<?= $contact_info->mobile_phone ?>">
	      </div>
	   </div>

	   <div class="form-group row">
	     <div class="col-md-6">
	         <label for="work_email"><?php echo  lang('work_email') ?> <span class="text-danger">*</span></label>
	          <input class="form-control" placeholder="<?php echo lang('work_email');?>" name="work_email" type="text" value="<?= $contact_info->work_email ?>" required="required">
	    </div>  
	    <div class="col-md-6">
	         <label for="personal_email"><?php echo  lang('personal_email') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('personal_email');?>" name="personal_email" type="text" value="<?= $contact_info->personal_email ?>">
	    </div>   
	   </div>

	   <div class="form-group row">
	      <div class="col-md-6">
	        <label for="address_1"><?php echo lang('address_line1') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('address_1');?>" name="address_1" type="text" value="<?= $contact_info->address_1 ?>">
	      </div>
	      <div class="col-md-6">
	        <label for="address_2"><?php echo lang('address_line2') ?>   </label>
	         <input class="form-control" placeholder="<?php echo lang('address_2');?>" name="address_2" type="text" value="<?= $contact_info->address_2 ?>">
	      </div>
	   </div>
	   <div class="form-group row">
	      <div class="col-md-6">
	        <label for="city"><?php echo lang('city') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('city');?>" name="city" type="text" value="<?= $contact_info->city ?>">
	      </div>
	      <div class="col-md-6">
	        <label for="state"><?php echo lang('state') ?>   </label>
	         <input class="form-control" placeholder="<?php echo lang('state');?>" name="state" type="text" value="<?= $contact_info->state ?>">
	      </div>
	   </div>
	   <div class="form-group row">
	      <div class="col-md-6">
	        <label for="zipcode"><?php echo lang('zipcode') ?> <span class="text-danger"></span></label>
	         <input class="form-control" placeholder="<?php echo lang('zipcode');?>" name="zipcode" type="text" value="<?= $contact_info->zipcode ?>">
	      </div>
	      <div class="col-md-6">
	        <label for="country"><?php echo lang('country') ?>   </label>
	         <select name="country" class="form-control select";?>">
	         <option value=""><?php echo 'Select';?></option>
	         <?php foreach($all_countries as $row) { ?>
	            <option value="<?php echo $row->country_id; ?>" <?= $contact_info->country == $row->country_id ? "Selected" : "" ?>><?php echo $row->country_name; ?></option>
	        <?php } ?> 
	        </select>
	      </div>
	   </div>
	   <div class="form-group">
	        <?php echo form_submit('add_working_experience', lang('save'), 'class="btn btn-primary"'); ?>
	         </div>
	    <?php echo form_close() ?>
	  </div>
	  
	</div>

		</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){

	$('#edit_emergen_contact').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/editEmergencyContact",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				$('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
    });

});
</script>
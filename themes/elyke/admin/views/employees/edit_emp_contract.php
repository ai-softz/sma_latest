<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_employee_document'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

            <div class="row">

		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_emp_contract']; ?>
		     <?php echo admin_form_open_multipart("employees/edit_emp_contract", $attrib) ?>
		     <input type="hidden" name="contract_id" value="<?= $emp_contract->contract_id ?>">
		    <div class="form-group row">
		      <div class="col-md-6">
		         <label for="contract_type_id"><?php echo  lang('document_type') ?> <span class="text-danger"></span></label>
		         <select name="contract_type_id" class="form-control select">
		          <option value=""><?php echo 'Select';?></option>
		          <?php foreach($contract_types as $row) { ?>
		            <option value="<?php echo $row->contract_type_id; ?>" <?= $row->contract_type_id == $emp_contract->contract_type_id ? "Selected" : "" ?>><?php echo $this->site->getCaption($row->name, $row->name);?></option>
		          <?php } ?>
		       </select>
		      </div>
		      <div class="col-md-6">
		         <label for="title"><?php echo  lang('title') ?> <span class="text-danger">*</span></label>
		         <input class="form-control" placeholder="<?php echo lang('title');?>" name="title" type="text" value="<?= $emp_contract->title ?>" required="required">
		      </div>
		   </div>

	   <div class="form-group row">
	     <div class="col-md-6">
	         <label for="from_date"><?php echo  lang('from_date') ?> <span class="text-danger">*</span></label>
	         <input class="form-control date" placeholder="<?php echo lang('from_date');?>" name="from_date" type="text" value="<?= $emp_contract->from_date ?>" required="required" autocomplete="off">
	    </div>  
	    <div class="col-md-6">
	         <label for="to_date"><?php echo  lang('to_date') ?> <span class="text-danger">*</span></label>
	         <input class="form-control date" placeholder="<?php echo lang('to_date');?>" name="to_date" type="text" value="<?= $emp_contract->to_date ?>" required="required" autocomplete="off">
	    </div>   
	   </div>
	   <div class="form-group row">
	   		<div class="col-md-6">
	         <label for="designation_id"><?php echo  lang('designation') ?> <span class="text-danger"></span></label>
	         <select name="designation_id" class="form-control select">
	          <option value=""><?php echo 'Select';?></option>
	          <?php foreach($all_designations as $row) { ?>
	            <option value="<?php echo $row->designation_id; ?>" <?= $row->designation_id == $emp_contract->designation_id ? "Selected" : "" ?>><?php echo $this->site->getCaption($row->designation_name, $row->designation_name);?></option>
	          <?php } ?>
	       </select>
	      </div>
	      <div class="col-md-6">
	        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
	        <textarea name="description" class="form-control" id="description"><?= $emp_contract->description ?></textarea>
	      </div>
	   </div>
	   <div class="form-group">
	        <?php echo form_submit('add_emp_contract', lang('save'), 'class="btn btn-primary"'); ?>
	    </div>
	    <?php echo form_close() ?>
	  </div>
	  
	</div>
		</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_emp_contract').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/editEmpContract",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
					$('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_employee_loan'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

<div class="row">
 
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_employee_loan']; ?>
     <?php echo admin_form_open_multipart("employees/edit_employee_loan", $attrib) ?>
     <input type="hidden" name="loan_deduction_id" value="<?= $employee_loan->loan_deduction_id ?>">

   <div class="form-group row">
     <div class="col-md-6">
         <label for="loan_options"><?php echo  lang('loan_options') ?> <span class="text-danger"></span></label>
         <select name="loan_options" class="form-control select">
              <option value="">Select</option>
               <option value="1" <?= $employee_loan->loan_options == 1 ? "Selected" : "" ?>><?php echo lang('loan_ssc_title');?></option>
              <option value="2" <?= $employee_loan->loan_options == 2 ? "Selected" : "" ?>><?php echo lang('loan_hdmf_title');?></option>
              <option value="0" <?= $employee_loan->loan_options == 0 ? "Selected" : "" ?>><?php echo lang('loan_other_sd_title');?></option>
        </select>
      </div>
    <div class="col-md-6">
         <label for="loan_deduction_title"><?php echo  lang('loan_deduction_title') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('loan_deduction_title');?>" name="loan_deduction_title" type="text" value="<?= $employee_loan->loan_deduction_title ?>" required="required">
    </div>   
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="monthly_installment"><?php echo  lang('loan_amount') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('loan_amount');?>" name="monthly_installment" type="text" value="<?= $employee_loan->monthly_installment ?>" required="required">
    </div>  
      
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="start_date"><?php echo  lang('start_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('start_date');?>" name="start_date" type="text" value="<?= date('d-m-Y', strtotime($employee_loan->start_date))  ?>" required="required">
    </div>  
    <div class="col-md-6">
         <label for="end_date"><?php echo  lang('end_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('end_date');?>" name="end_date" type="text" value="<?= date('d-m-Y', strtotime($employee_loan->end_date)) ?>" required="required">
    </div>   
   </div>
	<div class="form-group row">
    <div class="col-md-12">
         <label for="reason"><?php echo  lang('reason') ?> <span class="text-danger">*</span></label>
         <textarea name="reason" class="form-control"><?= $employee_loan->reason ?></textarea>
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('edit_employee_loan', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_employee_loan').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "employees/editEmployeeLoan",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
              $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
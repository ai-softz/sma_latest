<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_qualification'); ?></h4>
        </div>
        

        <div class="modal-body">
            <p> </p>

		<div class="row">
  
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_qualification']; ?>
		     <?php echo admin_form_open_multipart("employees/editQualification", $attrib) ?>
		     <input type="hidden" name="qualification_id" value="<?= $qualification->qualification_id ?>">
		    <div class="form-group row">
		      <div class="col-md-6">
		         <label for="school"><?php echo  lang('school') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('school');?>" name="school" id="school" type="text" value="<?php echo $qualification->name; ?>" required="required">
		      </div>
		      <div class="col-md-6">
		         <label for="education_level_id"><?php echo  lang('education_level') ?> <span class="text-danger">*</span></label>
		         <select name="education_level_id" id="language" class="form-control select">
		  			<option value=""><?php echo 'Select';?></option>
		  			<?php foreach($education_levels as $row) { ?>
		  				<option value="<?php echo $row->education_level_id; ?>"><?php echo $row->name; ?></option>
		  			<?php } ?>
				 </select>
		      </div>
		   </div>

		   <div class="form-group row">
		     <div class="col-md-6">
		         <label for="from_year"><?php echo  lang('from_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('from_year');?>" name="from_year" type="text" value="<?php echo $qualification->from_year; ?>" required="required">
		    </div>  
		    <div class="col-md-6">
		         <label for="to_year"><?php echo  lang('to_date') ?> <span class="text-danger">*</span></label>
		         <input class="form-control date" placeholder="<?php echo lang('to_year');?>" name="to_year" type="text" value="" required="required">
		    </div>   
		   </div>
		   	<div class="form-group row">
		   		<div class="col-md-6">
		   			<label for="language"><?php echo  lang('language') ?> <span class="text-danger"></span></label>
		      		 <select name="language_id" id="language" class="form-control select">
		  			<option value=""><?php echo 'Select';?></option>
		  			<?php foreach($qualification_languages as $row) { ?>
		  				<option value="<?php echo $row->language_id; ?>"><?php echo $row->name; ?></option>
		  			<?php } ?>
					</select>
				 </div>   
				 <div class="col-md-6">
		   			<label for="skill_id"><?php echo  lang('skill') ?> <span class="text-danger"></span></label>
		      		 <select name="skill_id" id="skill_id" class="form-control select">
		  			<option value=""><?php echo 'Select';?></option>
		  			<?php foreach($qualification_skill as $row) { ?>
		  				<option value="<?php echo $row->skill_id; ?>"><?php echo $row->name; ?></option>
		  			<?php } ?>
					</select>
				 </div>  
			   </div>
			   <div class="form-group row">
			      <div class="col-md-12">
			        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
			        <textarea name="description" class="form-control" id="description"></textarea>
			      </div>
			   </div>
			   <div class="form-group">
			        <?php echo form_submit('edit_qualification', lang('save'), 'class="btn btn-primary"'); ?>
			         </div>
			    <?php echo form_close() ?>
			  </div>
  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){

	$('#edit_qualification').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/editQualification",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });
});
	</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_salary_overtime'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

<div class="row">

  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_salary_overtime']; ?>
     <?php echo admin_form_open_multipart("employees/edit_salary_overtime", $attrib) ?>
     <input type="hidden" name="salary_overtime_id" value="<?= $salary_overtime->salary_overtime_id ?>">

   <div class="form-group row">
     <div class="col-md-6">
         <label for="overtime_type"><?php echo  lang('overtime_type') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('overtime_type');?>" name="overtime_type" type="text" value="<?= $salary_overtime->overtime_type ?>" required="required">
    </div>  
    <div class="col-md-6">
         <label for="no_of_days"><?php echo  lang('no_of_days') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('no_of_days');?>" name="no_of_days" type="text" value="<?= $salary_overtime->no_of_days ?>" required="required">
    </div>   
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="overtime_hours"><?php echo  lang('overtime_hours') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('overtime_hours');?>" name="overtime_hours" type="text" value="<?= $salary_overtime->overtime_hours ?>" required="required">
    </div>  
    <div class="col-md-6">
         <label for="overtime_rate"><?php echo  lang('overtime_rate') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('overtime_rate');?>" name="overtime_rate" type="text" value="<?= $salary_overtime->overtime_rate ?>" required="required">
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('add_salary_overtime', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_salary_overtime').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "employees/editSalaryOvertime",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
              $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
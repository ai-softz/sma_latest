<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_statutory_deductions'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

<div class="row">
  
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_statutory_deductions']; ?>
     <?php echo admin_form_open_multipart("employees/edit_statutory_deductions", $attrib) ?>
     <input type="hidden" name="statutory_deductions_id" value="<?= $statutory_deductions->statutory_deductions_id ?>">
    <div class="form-group row">
      
      <div class="col-md-6">
         <label for="statutory_options"><?php echo  lang('statutory_options') ?> <span class="text-danger"></span></label>
        <select name="statutory_options" class="form-control select">
               
                 <option value="0" <?= $statutory_deductions->statutory_options == 0 ? "Selected" : "" ?>><?php echo lang('fixed');?></option>
                 <option value="1" <?= $statutory_deductions->statutory_options == 1 ? "Selected" : "" ?>><?php echo lang('percentage');?></option>
                
        </select>
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="deduction_title"><?php echo  lang('deduction_title') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('deduction_title');?>" name="deduction_title" type="text" value="<?= $statutory_deductions->deduction_title ?>" required="required">
    </div>  
    <div class="col-md-6">
         <label for="deduction_amount"><?php echo  lang('deduction_amount') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('deduction_amount');?>" name="deduction_amount" type="text" value="<?= $statutory_deductions->deduction_amount ?>" required="required">
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('add_statutory_deductions', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_statutory_deductions').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "employees/editStatutoryDeductions",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
              $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
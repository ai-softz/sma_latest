<h2>Allowances:</h2>
<div class="table-responsive">
 <table id="table_allowances" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('title');?></th>
          <th><?php echo lang('amount');?></th>
          <th><?php echo lang('allowances_option');?></th>
          <th><?php echo lang('amount_option');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Allowances</h3>
  <p class="text-danger" class="error_input"> </p>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_employee_allowances']; ?>
     <?php echo admin_form_open_multipart("employees/add_employee_allowances", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    

   <div class="form-group row">
     <div class="col-md-6">
         <label for="allowance_title"><?php echo  lang('allowance_title') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('allowance_title');?>" name="allowance_title" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="allowance_type"><?php echo  lang('allowance_type') ?> <span class="text-danger"></span></label>
         <?php
            $allowance_types = array(
               'House Rent' => 'House Rent',
               'Meidcal' => 'Medical',
               'Travel' => 'Travel',
               'Other' => 'Other',
            );
         ?>
        <select name="allowance_type" class="form-control select"  required="required">
                <option value="">Select</option>
                <?php foreach($allowance_types as $key => $value) {?>
                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                <?php } ?>
        </select>
      </div>
     
   </div>
   <div class="form-group row"> 
      <div class="col-md-6">
            <label for="allowance_amount"><?php echo  lang('allowance_amount') ?> <span class="text-danger">*</span></label>
            <input class="form-control" placeholder="<?php echo lang('allowance_amount');?>" name="allowance_amount" type="text" value="" required="required">
      </div>  
      <div class="col-md-6">
         <label for="is_allowance_taxable"><?php echo  lang('allowances_option') ?> <span class="text-danger"></span></label>
         <select name="is_allowance_taxable" class="form-control select" required="required">
              <option value="">Select</option>
              <option value="0"><?php echo lang('not_taxable');?></option>
               <option value="1"><?php echo lang('fully_taxable');?></option>
               <option value="2"><?php echo lang('partially_taxable');?></option>
              
        </select>
      </div>
   </div>

   <div class="form-group row">
      
      <div class="col-md-6">
         <label for="amount_option"><?php echo  lang('amount_option') ?> <span class="text-danger"></span></label>
        <select name="amount_option" class="form-control select" id="wages_type">
                <option value="">Select</option>
                 <option value="0"><?php echo lang('fixed');?></option>
                 <option value="1"><?php echo lang('percentage');?></option>
                
        </select>
      </div>
   </div>

   <div class="form-group">
        <?php echo form_submit('add_employee_allowances', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#employee_allowances', function(){
      oTable = $('#table_allowances').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getAllowances/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              $('td:eq(3)', nRow).html(allowances_option(nRow, aData));
              $('td:eq(4)', nRow).html(amount_option(nRow, aData));
              return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

    function allowances_option(nRow, aData) {
       if(aData[3] == 0) {
          var returnData='No Taxable';
       } else if(aData[3] == 1) {
          var returnData='Fully Taxable';
       } else {
          var returnData='Partianly Taxable';
       }
       return returnData;
    }
    function amount_option(nRow, aData) {
       if(aData[4] == 0) {
          var returnData='Fixed';
       } else if(aData[4] == 1) {
          var returnData='Parcentage';
       } 
       return returnData;
    }

     $('#add_employee_allowances').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      var allowance_amount = $('#allowance_amount').val();
      // if(allowance_amount == null) {
      //    $('#error_input').text('Please enter amount.');
      //    return false;
      // }
      $.ajax({  
             url: site.base_url + "employees/add_employee_allowances",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });


   });

</script>
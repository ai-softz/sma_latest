<h2>Work Experiences:</h2>
<div class="table-responsive">
 <table id="table_emp_bank_account" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('account_title');?></th>
          <th><?php echo lang('account_number');?></th>
          <th><?php echo lang('bank_name');?></th>
          <th><?php echo lang('bank_code');?></th>
          <th><?php echo lang('bank_branch');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Work Experience</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_emp_bank_account']; ?>
     <?php echo admin_form_open_multipart("employees/add_emp_bank_account", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="account_title"><?php echo  lang('account_title') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('account_title');?>" name="account_title" id="account_title" type="text" value="" required="required">
      </div>
      <div class="col-md-6">
         <label for="account_number"><?php echo  lang('account_number') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('account_number');?>" name="account_number" type="text" value="" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="bank_name"><?php echo  lang('bank_name') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('bank_name');?>" name="bank_name" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="bank_code"><?php echo  lang('bank_code') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('bank_code');?>" name="bank_code" type="text" value="" required="required">
    </div>   
   </div>
   <div class="form-group row">
      <div class="col-md-6">
         <label for="bank_branch"><?php echo  lang('bank_branch') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('bank_branch');?>" name="bank_branch" type="text" value="">
    </div>  
   </div>
   <div class="form-group">
        <?php echo form_submit('add_emp_bank_account', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function() {
    $(document).on('click', '#emp_bank_account', function(){
      oTable = $('#table_emp_bank_account').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getEmpBankAccount/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_emp_bank_account').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_emp_bank_account",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    location.reload();
             }
               
      });  
  });

    // $(document).on('submit', '#add_working_experienc', function(){
      
    //   $.ajax({
    //     url: site.base_url + 'employees/add_working_experience',
    //     method: 'POST',
    //     data: new FormData(this),
    //     dataType: 'text',
    //     success: function(data) {
      
    //     }
    //   });
    // });

   });

</script>
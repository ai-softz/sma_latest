<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
     	<div class="col-md-2">
     		<?php if(!empty($emp_info->profile_picture)) { ?>
    	 		<img src="<?= base_url('assets/uploads/profile/'.$emp_info->profile_picture) ?>" style="width: 60px">
    	 	<?php } else { ?>
	        <img src="<?= base_url('assets/images/male.png') ?>" style="width: 100px">
	    	<?php } ?>
	     </div>
	     <div class="col-md-4">
	        <h2><p class=""><?= $emp_info->first_name.' '.$emp_info->last_name ?> </p></h2>
	        <!-- <p class="text-muted">Shift - Morning Shift <?= $emp_info->office_shift_id ?> </p> -->
	     </div>
	  	</div>
	  <hr class="border-light m-0">

	<?php $attrib = ['data-toggle' => 'validator', 'role' => 'form', 'id' => 'add_basic_info']; ?>
     <?php echo admin_form_open_multipart("employees/add_basic_info", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
	<div class="form-group row">
      <div class="col-md-4">
         <label for="customer_name"><?php echo  lang('first_name') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('first_name');?>" name="first_name" id="first_name" type="text" value="<?= $emp_info->first_name ?>" required="required">
      </div>
      <div class="col-md-4">
         <label for="customer_name"><?php echo  lang('last_name') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('last_name');?>" name="last_name" type="text" value="<?= $emp_info->last_name ?>" required="required">
      </div>
      <div class="col-md-4">
         <label for="customer_name"><?php echo  lang('employee_id') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('employee_id');?>" name="employee_id" type="text" value="<?= $emp_info->employee_id ?>" required="required">
      </div>
      
   </div>
   <div class="form-group row">   
    <div class="col-md-4">
         <label for="customer_name"><?php echo  lang('employee_doj') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('employee_doj');?>" name="date_of_joining" type="text" value="<?php echo date('d-m-Y', strtotime($emp_info->date_of_joining));?>" required="required">
    </div>       
    <div class="col-md-4">
    	<label for="warehouse"><?php echo  lang('warehouse') ?> <span class="text-danger">*</span></label>
       <select name="warehouse_id" id="warehouse" class="form-control select">
  			<option value=""><?php echo 'Select';?></option>
  			<?php foreach($all_warehouses as $warehouse) { ?>
  				<option value="<?php echo $warehouse->id; ?>"  <?= $warehouse->id == $emp_info->warehouse_id ? 'Selected' : '' ?>><?php echo $warehouse->name; ?></option>
  			<?php } ?>
			</select>
    </div>
    <div class="col-md-4">
    	<label for="department_id"><?php echo  lang('department_id') ?> <span class="text-danger">*</span></label>
        <select name="department_id" id="department" class="form-control select" required="required">
  			<option value=""><?php echo 'Select';?></option>
  			<?php foreach($all_departments as $row) { ?>
  				<option value="<?php echo $row->department_id; ?>" <?= $row->department_id == $emp_info->department_id ? 'Selected' : ''; ?>><?php echo $row->department_name; ?></option>
  			<?php } ?> 
			</select>
 		</div>
 		
	</div>
	<div class="form-group row">   
		<div class="col-md-4">
 			<label for="designation"><?php echo  lang('designation') ?> <span class="text-danger">*</span></label>
 		<div id="designation_area">	
 		<?php 
 		if(!empty($emp_info->designation_id)) { 
 			$desig = $this->db->get_where('designations', array('designation_id' => $emp_info->designation_id))->row();
 			?>	
 			<select name="designation_id" id="designation" class="form-control select" required="required">
  				<option value="<?php echo $emp_info->designation_id; ?>"><?php echo $desig->designation_name; ?></option>
			</select>
 		<?php } ?>
        
		</div>
 		</div>
 		<div class="col-md-4">
 			<label for="date_of_birth"><?php echo  lang('date_of_birth') ?> <span class="text-danger">*</span></label>
        <input class="form-control date" placeholder="<?php echo lang('date_of_birth');?>" name="date_of_birth" type="text" value="<?= date('d-m-Y', strtotime($emp_info->date_of_birth)); ?>">
 		</div>
 		<div class="col-md-4">
        	<label for="username"><?php echo  lang('username') ?> <span class="text-danger">*</span></label>
           <input class="form-control" placeholder="<?php echo lang('username');?>" name="username" type="text" value="<?= $emp_info->username ?>">
	    </div>
	</div>

	<div class="form-group row">   
		<div class="col-md-4">
    	<label for="email"><?php echo  lang('email') ?> <span class="text-danger">*</span></label>
        <input class="form-control" placeholder="<?php echo lang('email');?>" name="email" type="text" value="<?= $emp_info->email ?>">
 		</div>
 		<div class="col-md-4">
 		<label for="contact_no"><?php echo  lang('contact_no') ?> <span class="text-danger">*</span></label>
        <input class="form-control" placeholder="<?php echo lang('contact_no');?>" name="contact_no" type="text" value="<?= $emp_info->contact_no ?>">
 		</div> 
	 	<div class="col-md-4">
	    	<label for="role"><?php echo  lang('role') ?> <span class="text-danger">*</span></label>
	    	<select name="role" class="form-control select";?>">
	       <option value=""><?php echo 'Select';?></option>
	       <?php foreach($all_groupemp as $row) { ?>
	  				<option value="<?php echo $row->id; ?>" <?= $row->id == $user_info->group_id ? 'Selected' : '' ?>><?php //echo $this->site->getCaption($row->name, $row->name); 
					  echo $row->name; ?></option>
	  		<?php } ?> 
	  		</select>
	    </div>

	</div>

	<div class="form-group row">   
		<div class="col-md-4">
	    	<label for="gender"><?php echo  lang('gender') ?> <span class="text-danger"></span></label>
	    	<select name="gender" class="form-control select">
	       <option value="Male"><?php echo lang('gender_male');?></option>
	  		<option value="Female"><?php echo lang('gender_female');?></option>
	  		</select>
	    </div>
	    <!-- <div class="col-md-4">
    		<label for="office_shift"><?php echo  lang('office_shift') ?> <span class="text-danger"></span></label>
	        <select name="office_shift_id" class="form-control select">
	        <option value=""><?php echo 'Select';?></option>
	        <?php foreach($all_office_shifts as $row) { ?>
	  				<option value="<?php echo $row->office_shift_id; ?>" <?= $row->office_shift_id == $emp_info->office_shift_id ? 'Selected' : '' ?>><?php echo $row->shift_name; ?></option>
	  		<?php } ?> 
	  		</select>
 		</div> -->
 		<div class="col-md-4">
	    	<label for="reports_to"><?php echo  lang('reports_to') ?> <span class="text-danger"></span></label>
	       <select name="reports_to" class="form-control select";?>">
	       <option value=""><?php echo 'Select';?></option>
	       <?php foreach($all_employees as $row) { ?>
	  				<option value="<?php echo $row->user_id; ?>" <?= $row->user_id == $emp_info->reports_to ? 'Selected' : '' ?>><?php echo $row->first_name.' '.$row->last_name; ?></option>
	  		<?php } ?> 
	  		</select>
	    </div>
		<div class="col-md-4">
 			<label for="pin_code"><?php echo  lang('pin_code') ?> <span class="text-danger"></span></label>
        <input class="form-control" placeholder="<?php echo $this->lang->line('pin_code');?>" name="pin_code" type="text" value="<?php //echo $pin_code;?>">
 		</div>
	</div>

	<div class="form-group row">   
          
    <div class="col-md-4">
    	<label for="status"><?php echo  lang('status') ?> <span class="text-danger"></span></label>
       <select name="status" id="status" class="form-control select">
  			<option value=""><?php echo 'Select';?></option>
  			<option value="1" <?= $emp_info->is_active == 1 ? 'Selected' : ''; ?>>Active</option>
  			<option value="0" <?= $emp_info->is_active == 0 ? 'Selected' : ''; ?>>Inactive</option>
			</select>
    </div>
    <div class="col-md-4">
    	<label for="marital_status"><?php echo  lang('marital_status') ?> <span class="text-danger"></span></label>
        <select name="marital_status" id="marital_status" class="form-control select">
  			<option value=""><?php echo 'Select';?></option>
  			<option value="Single" <?= $emp_info->marital_status == 'Single' ? 'Selected' : '' ?>>Single</option>
  			<option value="Married" <?= $emp_info->marital_status == 'Married' ? 'Selected' : '' ?>>Married</option>
  			<option value="Widowed" <?= $emp_info->marital_status == 'Widowed' ? 'Selected' : '' ?>>Widowed</option>
  			<option value="Divorced or Seperated" <?= $emp_info->marital_status == 'Divorced or Seperated' ? 'Selected' : '' ?>>Divorced or Seperated</option>
			</select>
 		</div>
 		<div class="col-md-4">
         <label for="date_of_leaving"><?php echo  lang('date_of_leaving') ?> <span class="text-danger"></span></label>
         <input class="form-control date" placeholder="<?php echo lang('date_of_leaving');?>" name="date_of_leaving" type="text" value="<?php echo date('d-m-Y', strtotime($emp_info->date_of_leaving));?>">
    </div> 
	</div>

	<div class="form-group row">
		<!-- <div class="col-md-4">
			<?php  ?>
    	<label for="leave_categories"><?php echo  lang('leave_categories') ?> <span class="text-danger"></span></label>
        	<select name="leave_categories[]" class="selectpicker" multiple data-live-search="true" style="width: 100%">
             <?php foreach($leave_categories as $row) { ?> 
              <option value="<?= $row['leave_type_id'] ?>" <?= in_array($row['leave_type_id'], $cat_ids) ? 'selected' : '' ?>><?= $row['type_name'] ?></option>
              <?php } ?>
            </select>
 		</div> -->
 		
 		
 		
	</div>

	<div class="form-group row">
      <div class="col-md-4">
         <label for="address"><?php echo  lang('address') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('address');?>" name="address" id="address" type="text" value="<?= $emp_info->address ?>">
      </div>
      <div class="col-md-4">
         <label for="city"><?php echo  lang('city') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('city');?>" name="city" type="text" value="<?= $emp_info->city ?>">
      </div>
      <div class="col-md-4">
         <label for="state"><?php echo  lang('state') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('state');?>" name="state" type="text" value="<?= $emp_info->state ?>">
      </div>
      
   </div>

   <div class="form-group row">
      <div class="col-md-4">
         <label for="zipcode"><?php echo  lang('zipcode') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('zipcode');?>" name="zipcode" id="zipcode" type="text" value="<?= $emp_info->zipcode ?>" >
      </div>
      <div class="col-md-4">
         <label for="nationality_id"><?php echo  lang('nationality_id') ?> <span class="text-danger"></span></label>
         <select name="nationality_id" class="form-control select";?>">
	       <option value=""><?php echo 'Select';?></option>
	       <?php foreach($all_countries as $row) { ?>
	  				<option value="<?php echo $row->country_id; ?>" <?= $row->country_id == $emp_info->nationality_id ? 'Selected' : ''; ?>><?php echo $row->country_name; ?></option>
	  		<?php } ?> 
	  	 </select>
      </div>
      <div class="col-md-4">
         <label for="citizenship_id"><?php echo  lang('citizenship_id') ?> <span class="text-danger"></span></label>
         <select name="citizenship_id" class="form-control select";?>">
	       <option value=""><?php echo 'Select';?></option>
	       <?php foreach($all_countries as $row) { ?>
	  				<option value="<?php echo $row->country_id; ?>" <?= $row->country_id == $emp_info->citizenship_id ? 'Selected' : ''; ?>><?php echo $row->country_name; ?></option>
	  		<?php } ?> 
	  	  </select>
      </div>
      
   </div>

   <div class="form-group row">   
    <div class="col-md-4">
    	<label for="blood_group"><?php echo  lang('blood_group') ?> <span class="text-danger"></span></label>
       <select name="blood_group" id="blood_group" class="form-control select">
  			<option value=""><?php echo 'Select';?></option>
  			<option value="A+" <?= $emp_info->blood_group == 'A+' ? 'Selected' : ''; ?>>A+</option>
  			<option value="A-" <?= $emp_info->blood_group == 'A-' ? 'Selected' : ''; ?>>A-</option>
  			<option value="B+" <?= $emp_info->blood_group == 'B+' ? 'Selected' : ''; ?>>B+</option>
  			<option value="B-" <?= $emp_info->blood_group == 'B-' ? 'Selected' : ''; ?>>B-</option>
  			<option value="AB+" <?= $emp_info->blood_group == 'AB+' ? 'Selected' : ''; ?>>AB+</option>
  			<option value="AB-" <?= $emp_info->blood_group == 'AB-' ? 'Selected' : ''; ?>>AB-</option>
  			<option value="O+" <?= $emp_info->blood_group == 'O+' ? 'Selected' : ''; ?>>O+</option>
  			<option value="O-" <?= $emp_info->blood_group == 'O-' ? 'Selected' : ''; ?>>O-</option>
		</select>
    </div>
	</div>



	<div class="form-group">
	    	<?php echo form_submit('add_employee', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
   	<?php echo form_close() ?>

<script>

$(document).ready(function(){

	$('#add_basic_info').on('submit', function(e){  
	    e.preventDefault();       
    	var formdata = new FormData(this);

	    $.ajax({  
             url: site.base_url + "employees/add_basic_info",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	location.reload();
             }
	             
	    });  
	});  

	$(document).on('change', '#department', function(){
		var dept = $(this).val();
		$.ajax({
			url: site.base_url + 'employees/getDesignations',
			method: 'POST',
			data: {dept: dept},
			dataType: 'text',
			success: function(data) {
				$('#designation').remove();
				$('#designation_area').html(data);
			}
		});
	});

});

</script>
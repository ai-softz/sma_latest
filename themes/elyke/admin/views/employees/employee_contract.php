<h2>Contract:</h2>
<div class="table-responsive">
 <table id="table_emp_contract" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('duration');?></th>
          <th><?php echo lang('designation');?></th>
          <th><?php echo lang('contract_type');?></th>
          <th><?php echo lang('title');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Contract</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_emp_contract']; ?>
     <?php echo admin_form_open_multipart("employees/add_emp_contract", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="contract_type_id"><?php echo  lang('document_type') ?> <span class="text-danger"></span></label>
         <select name="contract_type_id" class="form-control select">
          <option value=""><?php echo 'Select';?></option>
          <?php foreach($contract_types as $row) { ?>
            <option value="<?php echo $row->contract_type_id; ?>"><?php echo $this->site->getCaption($row->name, $row->name);?></option>
          <?php } ?>
       </select>
      </div>
      <div class="col-md-6">
         <label for="title"><?php echo  lang('title') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('title');?>" name="title" type="text" value="" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="from_date"><?php echo  lang('from_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('from_date');?>" name="from_date" type="text" value="" required="required" autocomplete="off">
    </div>  
    <div class="col-md-6">
         <label for="to_date"><?php echo  lang('to_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('to_date');?>" name="to_date" type="text" value="" required="required" autocomplete="off">
    </div>   
   </div>
   <div class="form-group row">
   		<div class="col-md-6">
         <label for="designation_id"><?php echo  lang('designation') ?> <span class="text-danger"></span></label>
         <select name="designation_id" class="form-control select">
          <option value=""><?php echo 'Select';?></option>
          <?php foreach($all_designations as $row) { ?>
            <option value="<?php echo $row->designation_id; ?>"><?php echo $this->site->getCaption($row->designation_name, $row->designation_name);?></option>
          <?php } ?>
       </select>
      </div>
      <div class="col-md-6">
        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
        <textarea name="description" class="form-control" id="description"></textarea>
      </div>
   </div>
   <div class="form-group">
        <?php echo form_submit('add_emp_contract', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#emp_contract', function(){
      oTable = $('#table_emp_contract').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getEmployeeContract/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_emp_contract').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_emp_contract",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    location.reload();
             }
               
      });  
  });

    // $(document).on('submit', '#add_working_experienc', function(){
      
    //   $.ajax({
    //     url: site.base_url + 'employees/add_working_experience',
    //     method: 'POST',
    //     data: new FormData(this),
    //     dataType: 'text',
    //     success: function(data) {
      
    //     }
    //   });
    // });

   });

</script>
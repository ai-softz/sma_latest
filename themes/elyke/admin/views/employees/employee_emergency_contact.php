<h2>Contacts</h2>
<div class="table-responsive">
 <table id="table_emergen_contact" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('name');?></th>
          <th><?php echo lang('relation');?></th>
          <th><?php echo lang('email');?></th>
          <th><?php echo lang('mobile');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Contact</h3>
  <hr class="border-light">
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_emergen_contact']; ?>
     <?php echo admin_form_open_multipart("employees/add_working_experience", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <div class="row">
          <div class="col-md-12">
           <label for="relation"><?php echo  lang('relation') ?> <span class="text-danger"></span></label>
           <?php
              $relations = array(
                  'Self' => 'Self',
                  'Parent' => 'Parent',
                  'Spouse' => 'Spouse',
                  'Child' => 'Child',
                  'Sibling' => 'Sibling',
                  'in Laws' => 'in Laws',
              );
            ?>
           <select name="relation" id="language" class="form-control select">
              <option value=""><?php echo 'Select';?></option>
              <?php foreach($relations as $key => $value) { ?>
                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
              <?php } ?>
           </select>
            </div>
          </div>
          <div class="row" style="margin-top: 15px;">
            <div class="col-md-12">
              <input type="checkbox" name="is_primary">  Primary Contact
              <input type="checkbox" name="is_dependent" style="margin-left: 10px;">  Dependent
            </div>
          </div>
      </div>
      <div class="col-md-6">
         <label for="contact_name"><?php echo  lang('contact_name') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('contact_name');?>" name="contact_name" type="text" value="" required="required">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="phone"><?php echo  lang('phone') ?> <span class="text-danger">*</span></label>
            <div class="row">
              <div class="col-md-9">
                <input class="form-control" placeholder="<?php echo lang('work_phone');?>" name="work_phone" type="text" value="" required="required">
              </div>
              <div class="col-md-3">
                <input class="form-control" placeholder="<?php echo lang('ext');?>" name="work_phone_extension" type="text" value="">
              </div>
            </div>
            
    </div>  
    <div class="col-md-6">
         <label for="home_phone"><?php echo  lang('home_phone') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('home_phone');?>" name="home_phone" type="text" value="">
    </div>   
   </div>
   <div class="form-group row">
      <div class="col-md-6">
        <label for="mobile_phone"><?php echo  lang('mobile_phone') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('mobile_phone');?>" name="mobile_phone" type="text" value="">
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="work_email"><?php echo  lang('work_email') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('work_email');?>" name="work_email" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="personal_email"><?php echo  lang('personal_email') ?> <span class="text-danger">*</span></label>
         <input class="form-control" placeholder="<?php echo lang('personal_email');?>" name="personal_email" type="text" value="">
    </div>   
   </div>

   <div class="form-group row">
      <div class="col-md-6">
        <label for="address_1"><?php echo lang('address_line1') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('address_1');?>" name="address_1" type="text" value="">
      </div>
      <div class="col-md-6">
        <label for="address_2"><?php echo lang('address_line2') ?>   </label>
         <input class="form-control" placeholder="<?php echo lang('address_2');?>" name="address_2" type="text" value="">
      </div>
   </div>
   <div class="form-group row">
      <div class="col-md-6">
        <label for="city"><?php echo lang('city') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('city');?>" name="city" type="text" value="">
      </div>
      <div class="col-md-6">
        <label for="state"><?php echo lang('state') ?>   </label>
         <input class="form-control" placeholder="<?php echo lang('state');?>" name="state" type="text" value="">
      </div>
   </div>
   <div class="form-group row">
      <div class="col-md-6">
        <label for="zipcode"><?php echo lang('zipcode') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('zipcode');?>" name="zipcode" type="text" value="">
      </div>
      <div class="col-md-6">
        <label for="country"><?php echo lang('country') ?>   </label>
         <select name="country" class="form-control select";?>">
         <option value=""><?php echo 'Select';?></option>
         <?php foreach($all_countries as $row) { ?>
            <option value="<?php echo $row->country_id; ?>"><?php echo $row->country_name; ?></option>
        <?php } ?> 
        </select>
      </div>
   </div>
   <div class="form-group">
        <?php echo form_submit('add_working_experience', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#emergency_contact', function(){
      oTable = $('#table_emergen_contact').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getEmergencyContact/'.$emp_info->user_id) ?>',
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [ null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_emergen_contact').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_emergency_contact",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				      location.reload();
             }
               
      });  
    });

    // $(document).on('submit', '#add_working_experienc', function(){
      
    //   $.ajax({
    //     url: site.base_url + 'employees/add_working_experience',
    //     method: 'POST',
    //     data: new FormData(this),
    //     dataType: 'text',
    //     success: function(data) {
      
    //     }
    //   });
    // });

   });

</script>
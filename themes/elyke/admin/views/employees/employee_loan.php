<h2>Loan:</h2>
<div class="table-responsive">
 <table id="table_employee_loan" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('loan_deduction_title');?></th>
          <th><?php echo lang('loan_options');?></th>
          <th><?php echo lang('time_period');?></th>
          <th><?php echo lang('loan_deduction_amount');?></th>
          <th><?php echo lang('loan_time');?></th>
          
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Loan</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_employee_loan']; ?>
     <?php echo admin_form_open_multipart("employees/add_employee_loan", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">

   <div class="form-group row">
     <div class="col-md-6">
         <label for="loan_options"><?php echo  lang('loan_options') ?> <span class="text-danger"></span></label>
         <select name="loan_options" class="form-control select">
              <option value="">Select</option>
               <option value="1"><?php echo lang('loan_ssc_title');?></option>
              <option value="2"><?php echo lang('loan_hdmf_title');?></option>
              <option value="0"><?php echo lang('loan_other_sd_title');?></option>
        </select>
      </div>
    <div class="col-md-6">
         <label for="loan_deduction_title"><?php echo  lang('loan_deduction_title') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('loan_deduction_title');?>" name="loan_deduction_title" type="text" value="" required="required">
    </div>   
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="monthly_installment"><?php echo  lang('monthly_installment') ?> <span class="text-danger">*</span></label>
          <input class="form-control" placeholder="<?php echo lang('monthly_installment');?>" name="monthly_installment" type="text" value="" required="required">
    </div>  
      
   </div>
   <div class="form-group row">
     <div class="col-md-6">
         <label for="start_date"><?php echo  lang('start_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('start_date');?>" name="start_date" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="end_date"><?php echo  lang('end_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('end_date');?>" name="end_date" type="text" value="" required="required">
    </div>   
   </div>
	<div class="form-group row">
    <div class="col-md-6">
         <label for="reason"><?php echo  lang('reason') ?> <span class="text-danger">*</span></label>
         <textarea name="reason" class="form-control"></textarea>
    </div>   
   </div>

   <div class="form-group">
        <?php echo form_submit('add_salary_overtime', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#employee_loan', function(){
      oTable = $('#table_employee_loan').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getEmployeeLoan/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
              $('td:eq(2)', nRow).html(loan_option(nRow, aData));
              return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });
    function loan_option(nRow, aData) {
       if(aData[2] == 0) {
          var returnData='Others Loan';
       } else if(aData[2] == 1) {
          var returnData='Social Security System Loan';
       } else if(aData[2] == 2) {
          var returnData='Home Development Mutual Fund Loan';
       } 
       return returnData;
    }

     $('#add_employee_loan').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_employee_loan",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#table_employee_loan').DataTable().ajax.reload();
             }
               
      });  
  });


   });

</script>
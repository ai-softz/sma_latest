<a href="<?= admin_url('timesheets/add_leave_application') ?>" class="btn btn-primary"> Add New Leave </a>
<h2>Departments:</h2>
<div class="table-responsive">
 <table id="table_emp_payslips" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('payslip_key');?></th>
          <th><?php echo lang('payroll_net_payable');?></th>
          <th><?php echo lang('salary_month');?></th>
          <th><?php echo lang('payroll_date_title');?></th>
          <th><?php echo lang('status');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {
       $(document).on('click', '#employee_payslips', function(){
      oTable = $('#table_emp_payslips').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('payroll/getAllPayslipsOfEmp/'.$emp_info->user_id) ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
                $('td:eq(4)', nRow).html(paymentStatus(nRow, aData));
				return nRow;
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });
    function paymentStatus(nRow, aData) 
	{
		var status = '';
		if(aData[4] == 2) {
			status = 'Paid';
		}
		return status;
	}
});
</script>
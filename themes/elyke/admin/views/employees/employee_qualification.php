<h2>Qualifications:</h2>
<div class="table-responsive">
 <table id="table_employee_qualification" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
       	
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('school');?></th>
          <th><?php echo lang('time_period');?></th>
          <th><?php echo lang('education_level');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b>Add New</b> Qualification</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_qualification']; ?>
     <?php echo admin_form_open_multipart("employees/add_qualification", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
    <div class="form-group row">
      <div class="col-md-6">
         <label for="school"><?php echo  lang('school') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('school');?>" name="school" id="school" type="text" value="" required="required">
      </div>
      <div class="col-md-6">
         <label for="education_level_id"><?php echo  lang('education_level') ?> <span class="text-danger">*</span></label>
         <select name="education_level_id" id="language" class="form-control select">
  			<option value=""><?php echo 'Select';?></option>
  			<?php foreach($education_levels as $row) { ?>
  				<option value="<?php echo $row->education_level_id; ?>"><?php echo $row->name; ?></option>
  			<?php } ?>
		 </select>
      </div>
   </div>

   <div class="form-group row">
     <div class="col-md-6">
         <label for="from_year"><?php echo  lang('from_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('from_year');?>" name="from_year" type="text" value="" required="required">
    </div>  
    <div class="col-md-6">
         <label for="to_year"><?php echo  lang('to_date') ?> <span class="text-danger">*</span></label>
         <input class="form-control date" placeholder="<?php echo lang('to_year');?>" name="to_year" type="text" value="" required="required">
    </div>   
   </div>
   <div class="form-group row">
   		<div class="col-md-6">
   			<label for="language"><?php echo  lang('language') ?> <span class="text-danger"></span></label>
      		 <select name="language_id" id="language" class="form-control select">
  			<option value=""><?php echo 'Select';?></option>
  			<?php foreach($qualification_languages as $row) { ?>
  				<option value="<?php echo $row->language_id; ?>"><?php echo $row->name; ?></option>
  			<?php } ?>
			</select>
		 </div>   
		 <div class="col-md-6">
   			<label for="skill_id"><?php echo  lang('skill') ?> <span class="text-danger"></span></label>
      		 <select name="skill_id" id="skill_id" class="form-control select">
  			<option value=""><?php echo 'Select';?></option>
  			<?php foreach($qualification_skill as $row) { ?>
  				<option value="<?php echo $row->skill_id; ?>"><?php echo $row->name; ?></option>
  			<?php } ?>
			</select>
		 </div>  
   </div>
   <div class="form-group row">
      <div class="col-md-12">
        <label for="post"><?php echo  lang('description') ?> <span class="text-danger">*</span></label>
        <textarea name="description" class="form-control" id="description"></textarea>
      </div>
   </div>
   <div class="form-group">
        <?php echo form_submit('add_qualification', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
    $(document).on('click', '#employee_qualification', function(){
      oTable = $('#table_employee_qualification').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('employees/getEmployeeQualification/'.$emp_info->user_id) ?>',
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [ null, null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

     $('#add_qualification').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_qualification",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               location.reload();
             }
               
      });  
  });

   });

</script>
<div class="row">
  <h3><b>Add New</b> Work Experience</h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_social_network']; ?>
     <?php echo admin_form_open_multipart("employees/add_social_network", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">

	    <div class="form-group row">
	      <div class="col-md-6">
	         <label for="facebook_profile"><?php echo  lang('facebook_profile') ?> <span class="text-danger"></span></label>
	         <input class="form-control" placeholder="<?php echo lang('facebook_profile');?>" name="facebook_profile" id="facebook_profile" type="text" value="<?= $emp_info->facebook_link ?>">
	      </div>
	      <div class="col-md-6">
	         <label for="twitter_profile"><?php echo  lang('twitter_profile') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('twitter_profile');?>" name="twitter_profile" type="text" value="<?= $emp_info->twitter_link ?>" >
	      </div>
	   </div>

	    <div class="form-group row">
	      <div class="col-md-6">
	         <label for="blogger_profile"><?php echo  lang('blogger_profile') ?> <span class="text-danger"></span></label>
	         <input class="form-control" placeholder="<?php echo lang('blogger_profile');?>" name="blogger_profile" id="blogger_profile" type="text" value="<?= $emp_info->blogger_link ?>" >
	      </div>
	      <div class="col-md-6">
	         <label for="linkedin_profile"><?php echo  lang('linkedin_profile') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('linkedin_profile');?>" name="linkedin_profile" type="text" value="<?= $emp_info->linkdedin_link ?>">
	      </div>
	   </div>

	    <div class="form-group row">
	      <div class="col-md-6">
	         <label for="google_plus_profile"><?php echo  lang('google_plus_profile') ?> <span class="text-danger"></span></label>
	         <input class="form-control" placeholder="<?php echo lang('google_plus_profile');?>" name="google_plus_profile" id="google_plus_profile" type="text" value="<?= $emp_info->google_plus_link ?>">
	      </div>
	      <div class="col-md-6">
	         <label for="instagram_profile"><?php echo  lang('instagram_profile') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('instagram_profile');?>" name="instagram_profile" type="text" value="<?= $emp_info->instagram_link ?>" >
	      </div>
	   </div>
	    <div class="form-group row">
	      <div class="col-md-6">
	         <label for="pinterest_profile"><?php echo  lang('pinterest_profile') ?> <span class="text-danger"></span></label>
	         <input class="form-control" placeholder="<?php echo lang('pinterest_profile');?>" name="pinterest_profile" id="pinterest_profile" type="text" value="<?= $emp_info->pinterest_link ?>" >
	      </div>
	      <div class="col-md-6">
	         <label for="youtube_profile"><?php echo  lang('youtube_profile') ?> <span class="text-danger">*</span></label>
	         <input class="form-control" placeholder="<?php echo lang('youtube_profile');?>" name="youtube_profile" type="text" value="<?= $emp_info->youtube_link ?>" >
	      </div>
	   </div>
	   <div class="form-group">
        <?php echo form_submit('add_working_experience', lang('save'), 'class="btn btn-primary"'); ?>
	    </div>
	    <?php echo form_close() ?>

	</div>
</div>

<script>
$(document).ready(function(){

	$('#add_social_network').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "employees/add_social_network",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                location.reload();
             }
               
      });  
  });

});
</script>
<div class="row">
    <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'update_employee_salary']; ?>
     <?php echo admin_form_open_multipart("employees/update_employee_salary", $attrib) ?>
     <input type="hidden" name="emp_id" value="<?= $emp_info->user_id ?>">
      <div class="form-group row">
         <div class="col-md-6">
            <label for="wages_type"><?php echo  lang('wages_type') ?> <span class="text-danger">*</span></label>
            
            <select name="wages_type" class="form-control select" id="wages_type">
            	<option value="">Select</option>
               <option value="1" <?= $emp_info->wages_type == 1 ? "Selected" : "" ?>><?php echo lang('payroll_basic_salary');?></option>
               <option value="2" <?= $emp_info->wages_type == 2 ? "Selected" : "" ?>><?php echo lang('employee_daily_wages');?></option>
            	
            </select>
         </div>
         <div class="col-md-6">
            <label for="basic_salary"><?php echo  lang('basic_salary') ?> <span class="text-danger">*</span></label>
            <input class="form-control" placeholder="<?php echo lang('basic_salary');?>" name="basic_salary" type="text" value="<?= $emp_info->basic_salary ?>" required="required">
         </div>
         
      </div>  
      <div class="form-group">
        <?php echo form_submit('update_employee_salary', lang('save'), 'class="btn btn-primary"'); ?>
         </div>
         <?php echo form_close() ?>
      </div>    
 </div>

 <script>
$(document).ready(function () {
   $('#update_employee_salary').on('submit', function(e){  
         e.preventDefault();       
         var formdata = new FormData(this);
         $.ajax({  
                url: site.base_url + "employees/update_employee_salary",
                method:"POST",  
                data:new FormData(this),  
                contentType: false,  
                cache: false,  
                processData:false,  
                success:function(data)  
                {  
                   
                }
                  
         });  
     });
});
 </script>
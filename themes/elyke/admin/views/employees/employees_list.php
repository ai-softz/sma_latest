<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script>
    $(document).ready(function () {
        oTable = $('#employee_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('employees/employees_list') ?>',
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false, "mRender": checkbox}, null, null,  null, null, null, null, {"bSortable": false}]
        });
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo admin_form_open('products/product_actions', 'id="action-form"');
} ?>
<div class="box">
<div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-barcode"></i><?= lang('employees'); ?>
        </h2>
        
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang('actions') ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('employees/add_employee') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_employee') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('employees/employee_excel')?>">
                            <i class="fa fa-file-excel-o"></i> <?= lang('download_excel') ?>
                            </a>
                        </li>
                        
                        <li>
                            <a href="<?= admin_url('employees/employee_pdf')?>">
                            <i class="fa fa-file-excel-o"></i> <?= lang('download_pdf') ?>
                            </a>
                        </li>
                        
                         
                    </ul>
                </li>
                
            </ul>
        </div>
    </div>


<div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>
                <!-- <a href="<?= admin_url('employees/add_employee') ?>" class="btn btn-primary" style="margin-bottom:5px"> Add Employee </a>
                <a href="<?= admin_url('employees/employee_excel') ?>" class="btn btn-primary" style="margin-bottom:5px; float: right"> <i class="fa fa-download"> </i>  Download Excel</a>
                <a href="<?= admin_url('employees/employee_pdf') ?>" target="_blank" class="btn btn-primary" style="margin-bottom:5px; float: right"> <i class="fa fa-download"> </i>  Download Pdf</a>  -->
                
                <div class="table-responsive">
                    <table id="employee_table" class="table table-bordered table-hover table-striped reports-table">
                        <thead>
                            <tr>
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th><?= lang('#'); ?></th>
                                <th><?= lang('name'); ?></th>
                                <th><?= lang('employee_code'); ?></th>
                                <th><?= lang('warehouse'); ?></th>
                                <th><?= lang('contact'); ?></th>
                                
                                <th><?= lang('date_of_joining'); ?></th>
                                <th style="width:100px;"><?= lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7" class="dataTables_empty">
                                    <?= lang('loading_data_from_server') ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $GP['bulk_actions']) {
                                ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php
                            } ?>
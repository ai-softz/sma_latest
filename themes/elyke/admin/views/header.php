<?php defined('BASEPATH') or exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $page_title ?> - <?= $Settings->site_name ?></title>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/custom.css" rel="stylesheet"/>
    <link href="<?= base_url('assets/custom/custom.css') ?>" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/jquery.js"></script>
    <![endif]-->
    <noscript><style type="text/css">#loading { display: none; }</style></noscript>
    <?php if ($Settings->user_rtl) {
        ?>
        <link href="<?= $assets ?>styles/helpers/bootstrap-rtl.min.css" rel="stylesheet"/>
        <link href="<?= $assets ?>styles/style-rtl.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () { $('.pull-right, .pull-left').addClass('flip'); });
        </script>
        <?php
    } ?>
    <script type="text/javascript">
        $(window).load(function () {
            $("#loading").fadeOut("slow");
        });
    </script>
</head>

<body>
<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>
<div id="loading"></div>
<div id="app_wrapper">
    <header id="header" class="navbar">
        <div class="container">
            <a class="navbar-brand" href="<?= admin_url() ?>"><span class="logo"><?= $Settings->site_name ?></span></a>

            <div class="btn-group visible-xs pull-right btn-visible-sm">
                <button class="navbar-toggle btn" type="button" data-toggle="collapse" data-target="#sidebar_menu">
                    <span class="fa fa-bars"></span>
                </button>
                <?php if (SHOP) {
                    ?>
                <a href="<?= site_url('/') ?>" class="btn">
                    <span class="fa fa-shopping-cart"></span>
                </a>
                    <?php
                } ?>
                <?php if (POS) {
                    ?>
                <?php if (POSTYPE==2) {
                        ?>
                        <a href="<?= admin_url('poswholesale') ?>" class="btn">
                            <span class="fa fa-th-large"></span>
                        </a>
                        <?php
                    }else if (POSTYPE==3) {
                        ?>
                        <a href="<?= admin_url('pos_resturant_two') ?>" class="btn">
                            <span class="fa fa-th-large"></span>
                        </a>
                        <?php
                    }else if (POSTYPE ==1) {
                        ?>
                        <a href="<?= admin_url('posgrocery') ?>" class="btn">
                            <span class="fa fa-th-large"></span>
                        </a>
                        <?php
                    }else{ ?>
                        <a href="<?= admin_url('pos') ?>" class="btn">
                            <span class="fa fa-th-large"></span>
                        </a>
                        <?php
                    } ?>
                    <?php
                } ?>
                <a href="<?= admin_url('calendar') ?>" class="btn">
                    <span class="fa fa-calendar"></span>
                </a>
                <a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id')); ?>" class="btn">
                    <span class="fa fa-user"></span>
                </a>
                <a href="<?= admin_url('logout'); ?>" class="btn">
                    <span class="fa fa-sign-out"></span>
                </a>
            </div>
            <div class="header-nav">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" src="<?= $this->session->userdata('avatar') ? base_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : base_url('assets/images/' . $this->session->userdata('gender') . '.png'); ?>" class="mini_avatar img-rounded">

                            <div class="user">
                                <span><?= lang('welcome') ?> <?= $this->session->userdata('username'); ?></span>
                            </div>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id')); ?>">
                                    <i class="fa fa-user"></i> <?= lang('profile'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>"><i class="fa fa-key"></i> <?= lang('change_password'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= admin_url('logout'); ?>">
                                    <i class="fa fa-sign-out"></i> <?= lang('logout'); ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown hidden-xs"><a class="btn tip" title="<?= lang('dashboard') ?>" data-placement="bottom" href="<?= admin_url('welcome') ?>"><i class="fa fa-dashboard"></i></a></li>
                    <?php if (SHOP) {
                        ?>
                    <li class="dropdown hidden-xs"><a class="btn tip" title="<?= lang('shop') ?>" data-placement="bottom" href="<?= base_url() ?>"><i class="fa fa-shopping-cart"></i></a></li>
                        <?php
                    } ?>
                    <?php if (POS) {
                        ?>
                    <?php if (POSTYPE==2) {
                            ?>
                            <a href="<?= admin_url('poswholesale') ?>" class="btn">
                                <span class="fa fa-th-large"></span>
                            </a>
                            <?php
                        }else if (POSTYPE==3) {
                            ?>
                            <a href="<?= admin_url('pos_resturant_two') ?>" class="btn">
                                <span class="fa fa-th-large"></span>
                            </a>
                            <?php
                        }else if (POSTYPE ==1) {
                            ?>
                            <a href="<?= admin_url('posgrocery') ?>" class="btn">
                                <span class="fa fa-th-large"></span>
                            </a>
                            <?php
                        }else{ ?>
                            <a href="<?= admin_url('pos') ?>" class="btn">
                                <span class="fa fa-th-large"></span>
                            </a>
                    <?php
                    } ?>
                        <?php
                    } ?>
                    <?php if ($Owner) {
                        ?>
                    <li class="dropdown hidden-sm">
                        <a class="btn tip" title="<?= lang('settings') ?>" data-placement="bottom" href="<?= admin_url('system_settings') ?>">
                            <i class="fa fa-cogs"></i>
                        </a>
                    </li>
                        <?php
                    } ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('calculator') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                            <i class="fa fa-calculator"></i>
                        </a>
                        <ul class="dropdown-menu pull-right calc">
                            <li class="dropdown-content">
                                <span id="inlineCalc"></span>
                            </li>
                        </ul>
                    </li>
                    <?php if ($info) {
                        ?>
                        <li class="dropdown hidden-sm">
                            <a class="btn tip" title="<?= lang('notifications') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                                <i class="fa fa-info-circle"></i>
                                <span class="number blightOrange black"><?= sizeof($info) ?></span>
                            </a>
                            <ul class="dropdown-menu pull-right content-scroll">
                                <li class="dropdown-header"><i class="fa fa-info-circle"></i> <?= lang('notifications'); ?></li>
                                <li class="dropdown-content">
                                    <div class="scroll-div">
                                        <div class="top-menu-scroll">
                                            <ol class="oe">
                                                <?php foreach ($info as $n) {
                                                    echo '<li>' . $n->comment . '</li>';
                                                } ?>
                                            </ol>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <?php
                    } ?>
                    <?php if ($events) {
                        ?>
                        <li class="dropdown hidden-xs">
                            <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                                <i class="fa fa-calendar"></i>
                                <span class="number blightOrange black"><?= sizeof($events) ?></span>
                            </a>
                            <ul class="dropdown-menu pull-right content-scroll">
                                <li class="dropdown-header">
                                <i class="fa fa-calendar"></i> <?= lang('upcoming_events'); ?>
                                </li>
                                <li class="dropdown-content">
                                    <div class="top-menu-scroll">
                                        <ol class="oe">
                                            <?php foreach ($events as $event) {
                                                echo '<li>' . date($dateFormats['php_ldate'], strtotime($event->start)) . ' <strong>' . $event->title . '</strong><br>' . $event->description . '</li>';
                                            } ?>
                                        </ol>
                                    </div>
                                </li>
                                <li class="dropdown-footer">
                                    <a href="<?= admin_url('calendar') ?>" class="btn-block link">
                                        <i class="fa fa-calendar"></i> <?= lang('calendar') ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php
                    } else {
                        ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="<?= admin_url('calendar') ?>">
                            <i class="fa fa-calendar"></i>
                        </a>
                    </li>
                        <?php
                    } ?>
                    <li class="dropdown hidden-sm">
                        <a class="btn tip" title="<?= lang('styles') ?>" data-placement="bottom" data-toggle="dropdown"
                           href="#">
                            <i class="fa fa-css3"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="bwhite noPadding">
                                <a href="#" id="fixed" class="">
                                    <i class="fa fa-angle-double-left"></i>
                                    <span id="fixedText">Fixed</span>
                                </a>
                                <a href="#" id="cssLight" class="grey">
                                    <i class="fa fa-stop"></i> Grey
                                </a>
                                <a href="#" id="cssBlue" class="blue">
                                    <i class="fa fa-stop"></i> Blue
                                </a>
                                <a href="#" id="cssBlack" class="black">
                                   <i class="fa fa-stop"></i> Black
                               </a>
                           </li>
                        </ul>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('language') ?>" data-placement="bottom" data-toggle="dropdown"
                           href="#">
                            <img src="<?= base_url('assets/images/' . $Settings->user_language . '.png'); ?>" alt="">
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <?php $scanned_lang_dir = array_map(function ($path) {
        return basename($path);
                            }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));
                                                  foreach ($scanned_lang_dir as $entry) {
                                                        ?>
                                <li>
                                    <a href="<?= admin_url('welcome/language/' . $entry); ?>">
                                        <img src="<?= base_url('assets/images/' . $entry . '.png'); ?>" class="language-img">
                                        &nbsp;&nbsp;<?= ucwords($entry); ?>
                                    </a>
                                </li>
                                                      <?php
                                                  } ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= admin_url('welcome/toggle_rtl') ?>">
                                    <i class="fa fa-align-<?=$Settings->user_rtl ? 'right' : 'left';?>"></i>
                                    <?= lang('toggle_alignment') ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php /* if ($Owner && $Settings->update) { ?>
                    <li class="dropdown hidden-sm">
                        <a class="btn blightOrange tip" title="<?= lang('update_available') ?>"
                            data-placement="bottom" data-container="body" href="<?= admin_url('system_settings/updates') ?>">
                            <i class="fa fa-download"></i>
                        </a>
                    </li>
                        <?php } */ ?>
                    <?php if (($Owner || $Admin || $GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts']) && ($qty_alert_num > 0 || $exp_alert_num > 0 || $shop_sale_alerts)) {
                        ?>
                        <li class="dropdown hidden-sm">
                            <a class="btn blightOrange tip" title="<?= lang('alerts') ?>"
                                data-placement="left" data-toggle="dropdown" href="#">
                                <i class="fa fa-exclamation-triangle"></i>
                                <span class="number bred black"><?= $qty_alert_num + (($Settings->product_expiry) ? $exp_alert_num : 0) + $shop_sale_alerts + $shop_payment_alerts; ?></span>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <?php if ($qty_alert_num > 0) {
                                    ?>
                                <li>
                                    <a href="<?= admin_url('reports/quantity_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('quantity_alerts') ?></span>
                                    </a>
                                </li>
                                    <?php
                                } ?>
                                <?php if ($Settings->product_expiry) {
                                    ?>
                                <li>
                                    <a href="<?= admin_url('reports/expiry_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $exp_alert_num; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('expiry_alerts') ?></span>
                                    </a>
                                </li>
                                    <?php
                                } ?>
                                <?php if ($shop_sale_alerts) {
                                    ?>
                                <li>
                                    <a href="<?= admin_url('sales?shop=yes&delivery=no') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $shop_sale_alerts; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('sales_x_delivered') ?></span>
                                    </a>
                                </li>
                                    <?php
                                } ?>
                                <?php if ($shop_payment_alerts) {
                                    ?>
                                <li>
                                    <a href="<?= admin_url('sales?shop=yes&attachment=yes') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $shop_payment_alerts; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('manual_payments') ?></span>
                                    </a>
                                </li>
                                    <?php
                                } ?>
                            </ul>
                        </li>
                        <?php
                    } ?>
                    <?php if (POS) {
                        ?>
                    <li class="dropdown hidden-xs">
                        <?php if (POSTYPE ==2) {
                            ?>
                            <a class="btn bdarkGreen tip" title="<?= lang('pos') ?>" data-placement="bottom" href="<?= admin_url('poswholesale') ?>">
                                <i class="fa fa-th-large"></i> <span class="padding05"><?= lang('pos') ?></span>
                            </a>
                        <?php
                        }else if (POSTYPE==3) {
                        ?>
                        <a class="btn bdarkGreen tip" title="<?= lang('pos') ?>" data-placement="bottom" href="<?= admin_url('pos_resturant_two') ?>">
                            <i class="fa fa-th-large"></i> <span class="padding05"><?= lang('pos') ?></span>
                        </a>
                        <?php
                        }else if (POSTYPE==1) {
                            ?>
                            <a class="btn bdarkGreen tip" title="<?= lang('pos') ?>" data-placement="bottom" href="<?= admin_url('posgrocery') ?>">
                                <i class="fa fa-th-large"></i> <span class="padding05"><?= lang('pos') ?></span>
                            </a>
                            <?php
                        }else{ ?>
                            <a class="btn bdarkGreen tip" title="<?= lang('pos') ?>" data-placement="bottom" href="<?= admin_url('pos') ?>">
                                <i class="fa fa-th-large"></i> <span class="padding05"><?= lang('pos') ?></span>
                            </a>
                            <?php
                        } ?>
                    </li>
                        <?php
                    } ?>
                    <?php if ($Owner) {
                        ?>
                        <li class="dropdown">
                            <a class="btn bdarkGreen tip" id="today_profit" title="<span><?= lang('today_profit') ?></span>"
                                data-placement="bottom" data-html="true" href="<?= admin_url('reports/profit') ?>"
                                data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-hourglass-2"></i>
                            </a>
                        </li>
                        <?php
                    } ?>
                    <?php if ($Owner || $Admin) {
                        ?>
                        <?php if (POS) {
                            ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn bblue tip" title="<?= lang('list_open_registers') ?>" data-placement="bottom" href="<?= admin_url('pos/registers') ?>">
                            <i class="fa fa-list"></i>
                        </a>
                    </li>
                            <?php
                        } ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn bred tip" title="<?= lang('clear_ls') ?>" data-placement="bottom" id="clearLS" href="#">
                            <i class="fa fa-eraser"></i>
                        </a>
                    </li>
                        <?php
                    } ?>
                </ul>
            </div>
        </div>
    </header>

    <div class="container" id="container">
        <div class="row" id="main-con">
        <table class="lt"><tr><td class="sidebar-con">
            <div id="sidebar-left">
                <div class="sidebar-nav nav-collapse collapse navbar-collapse" id="sidebar_menu">
                    <ul class="nav main-menu">
                        <li class="mm_welcome">
                            <a href="<?= admin_url() ?>">
                                <i class="fa fa-dashboard"></i>
                                <span class="text"> <?= lang('dashboard'); ?></span>
                            </a>
                        </li>

                        <?php
                        if ($Owner || $Admin) {
                            ?>

                            <li class="mm_products">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-barcode"></i>
                                    <span class="text"> <?= lang('products'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="products_index">
                                        <a class="submenu" href="<?= admin_url('products'); ?>">
                                            <i class="fa fa-barcode"></i>
                                            <span class="text"> <?= lang('list_products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_add">
                                        <a class="submenu" href="<?= admin_url('products/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_product'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_import_csv">
                                        <a class="submenu" href="<?= admin_url('products/import_csv'); ?>">
                                            <i class="fa fa-file-text"></i>
                                            <span class="text"> <?= lang('import_products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_print_barcodes">
                                        <a class="submenu" href="<?= admin_url('products/print_barcodes'); ?>">
                                            <i class="fa fa-tags"></i>
                                            <span class="text"> <?= lang('print_barcode_label'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_quantity_adjustments">
                                        <a class="submenu" href="<?= admin_url('products/quantity_adjustments'); ?>">
                                            <i class="fa fa-filter"></i>
                                            <span class="text"> <?= lang('quantity_adjustments'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_add_adjustment">
                                        <a class="submenu" href="<?= admin_url('products/add_adjustment'); ?>">
                                            <i class="fa fa-filter"></i>
                                            <span class="text"> <?= lang('add_adjustment'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_stock_counts">
                                        <a class="submenu" href="<?= admin_url('products/stock_counts'); ?>">
                                            <i class="fa fa-list-ol"></i>
                                            <span class="text"> <?= lang('stock_counts'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_count_stock">
                                        <a class="submenu" href="<?= admin_url('products/count_stock'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('count_stock'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'sales' ? 'mm_pos' : '' ?>">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-heart"></i>
                                    <span class="text"> <?= lang('sales'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= admin_url('sales'); ?>">
                                            <i class="fa fa-heart"></i>
                                            <span class="text"> <?= lang('list_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php if (POS) {
                                        ?>
                                    <li id="pos_sales">
                                        <a class="submenu" href="<?= admin_url('pos/sales'); ?>">
                                            <i class="fa fa-heart"></i>
                                            <span class="text"> <?= lang('pos_sales'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <li id="sales_add">
                                        <a class="submenu" href="<?= admin_url('sales/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_sale'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_sale_by_csv">
                                        <a class="submenu" href="<?= admin_url('sales/sale_by_csv'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_sale_by_csv'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_deliveries">
                                        <a class="submenu" href="<?= admin_url('sales/deliveries'); ?>">
                                            <i class="fa fa-truck"></i>
                                            <span class="text"> <?= lang('deliveries'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_gift_cards">
                                        <a class="submenu" href="<?= admin_url('sales/gift_cards'); ?>">
                                            <i class="fa fa-gift"></i>
                                            <span class="text"> <?= lang('list_gift_cards'); ?></span>
                                        </a>
                                    </li>
                                    <li id="return_sale_barcode">
                                        <a class="submenu" href="<?= admin_url('sales/return_sale_form'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('return_sale_barcode'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_quotes">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-heart-o"></i>
                                    <span class="text"> <?= lang('quotes'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="quotes_index">
                                        <a class="submenu" href="<?= admin_url('quotes'); ?>">
                                            <i class="fa fa-heart-o"></i>
                                            <span class="text"> <?= lang('list_quotes'); ?></span>
                                        </a>
                                    </li>
                                    <li id="quotes_add">
                                        <a class="submenu" href="<?= admin_url('quotes/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_quote'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_purchases">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="text"> <?= lang('purchases'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="purchases_index">
                                        <a class="submenu" href="<?= admin_url('purchases'); ?>">
                                            <i class="fa fa-star"></i>
                                            <span class="text"> <?= lang('list_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_add">
                                        <a class="submenu" href="<?= admin_url('purchases/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_purchase'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_purchase_by_csv">
                                        <a class="submenu" href="<?= admin_url('purchases/purchase_by_csv'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_purchase_by_csv'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_expenses">
                                        <a class="submenu" href="<?= admin_url('purchases/expenses'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('list_expenses'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_add_expense">
                                        <a class="submenu" href="<?= admin_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_expense'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_purchase_amount">
                                        <a class="submenu" href="<?= admin_url('purchases/purchase_amount'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('purchase_amount_list'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_add_purchase_amount">
                                        <a class="submenu" href="<?= admin_url('purchases/add_purchase_amount'); ?>"
                                            data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_purchase_amount'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_purchase_return_form">
                                        <a class="submenu" href="<?= admin_url('purchases/purchase_return_form'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('purchase_return_barcode'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_purchases_raw">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-barcode"></i>
                                    <span class="text"> <?= lang('purchases_raw'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    
                                    <li id="purchases_raw_index">
                                        <a class="submenu" href="<?= admin_url('purchases_raw'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('list_purchases_raw'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_add_purchase_raw">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/add_purchase_raw'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_purchase_raw'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_make_products">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/make_products'); ?>">
                                            <i class="fa fa-credit-card"></i>
                                            <span class="text"> <?= lang('make_products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_products_raw_material">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/products_raw_material'); ?>">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="text"> <?= lang('raw_products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_category_raw_material">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/category_raw_material'); ?>">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="text"> <?= lang('raw_categories'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_daily_purchases_raw">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/daily_purchases_raw'); ?>">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="text"> <?= lang('daily_purchases_raw'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_monthly_purchases_raw">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/monthly_purchases_raw'); ?>">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="text"> <?= lang('monthly_purchases_raw'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_purchase_raw_product">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/purchase_raw_product'); ?>">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="text"> <?= lang('raw_product_purchase'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_raw_purchase_make_raw_product">
                                        <a class="submenu" href="<?= admin_url('purchases_raw/purchase_make_raw_product'); ?>">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="text"> <?= lang('make_product_report'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_transfers">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star-o"></i>
                                    <span class="text"> <?= lang('transfers'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="transfers_index">
                                        <a class="submenu" href="<?= admin_url('transfers'); ?>">
                                            <i class="fa fa-star-o"></i><span class="text"> <?= lang('list_transfers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="transfers_add">
                                        <a class="submenu" href="<?= admin_url('transfers/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_transfer'); ?></span>
                                        </a>
                                    </li>
                                    <li id="transfers_purchase_by_csv">
                                        <a class="submenu" href="<?= admin_url('transfers/transfer_by_csv'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_transfer_by_csv'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_returns">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-random"></i>
                                    <span class="text"> <?= lang('returns'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="returns_index">
                                        <a class="submenu" href="<?= admin_url('returns'); ?>">
                                            <i class="fa fa-random"></i><span class="text"> <?= lang('list_returns'); ?></span>
                                        </a>
                                    </li>
                                    <li id="returns_add">
                                        <a class="submenu" href="<?= admin_url('returns/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_return'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_auth mm_customers mm_suppliers mm_billers">
                                <a class="dropmenu" href="#">
                                <i class="fa fa-users"></i>
                                <span class="text"> <?= lang('people'); ?> </span>
                                <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <?php if ($Owner) {
                                        ?>
                                    <li id="auth_users">
                                        <a class="submenu" href="<?= admin_url('users'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_users'); ?></span>
                                        </a>
                                    </li>
                                    <li id="auth_create_user">
                                        <a class="submenu" href="<?= admin_url('users/create_user'); ?>">
                                            <i class="fa fa-user-plus"></i><span class="text"> <?= lang('new_user'); ?></span>
                                        </a>
                                    </li>
                                    <li id="billers_index">
                                        <a class="submenu" href="<?= admin_url('billers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_billers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="billers_index">
                                        <a class="submenu" href="<?= admin_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_biller'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= admin_url('customers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_customers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= admin_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_customer'); ?></span>
                                        </a>
                                    </li>
                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= admin_url('suppliers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_suppliers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= admin_url('suppliers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_supplier'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_notifications">
                                <a class="submenu" href="<?= admin_url('notifications'); ?>">
                                    <i class="fa fa-info-circle"></i><span class="text"> <?= lang('notifications'); ?></span>
                                </a>
                            </li>
                            <li class="mm_calendar">
                                <a class="submenu" href="<?= admin_url('calendar'); ?>">
                                    <i class="fa fa-calendar"></i><span class="text"> <?= lang('calendar'); ?></span>
                                </a>
                            </li>

                            <li class="mm_account">
                                <a class="dropmenu" href="#"><i class="fa fa-credit-card" aria-hidden="true"></i><span class="text"> <?= lang('accounts'); ?> </span><span class="chevron closed"></span></a>
                                <ul>
                                    <li id="account_coa"><a class="submenu" href="<?= admin_url('account/coa'); ?>"><i class="fa fa-random"></i><span class="text"> <?= lang('chart_of_account'); ?></span></a></li>
                                    <li id="account_opening_balance"><a class="submenu" href="<?= admin_url('account/opening_balance'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('opening_balance'); ?></span></a></li>
                                    <li id="account_supplier_payment"><a class="submenu" href="<?= admin_url('account/supplier_payment'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Supplier_Payment'); ?></span></a></li>
                                    <li id="account_customer_receive"><a class="submenu" href="<?= admin_url('account/customer_receive'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Customer_Receive'); ?></span></a></li>
                                    <li id="account_cash_adjustment"><a class="submenu" href="<?= admin_url('account/cash_adjustment'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Cash_Adjustment'); ?></span></a></li>
                                    <li id="account_debit_voucher"><a class="submenu" href="<?= admin_url('account/debit_voucher'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Debit_Voucher'); ?></span></a></li>
                                    <li id="account_credit_voucher"><a class="submenu" href="<?= admin_url('account/credit_voucher'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Credit_Voucher'); ?></span></a></li>
                                    <li id="account_contra_voucher"><a class="submenu" href="<?= admin_url('account/contra_voucher'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Contra_Voucher'); ?></span></a></li>
                                    <li id="account_journal_voucher"><a class="submenu" href="<?= admin_url('account/journal_voucher'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Journal_Voucher'); ?></span></a></li>
                                    <li id="account_voucher_list"><a class="submenu" href="<?= admin_url('account/voucher_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Vouchar_Approval'); ?></span></a></li>
                                    <li class="mm_account_report">
                                        <a class="dropmenu" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i><span class="text"> <?= lang('accounts_report'); ?> </span><span class="chevron closed"></span></a>
                                        <ul>
                                            <li id="account_voucher_report" class=""><a class="submenu" href="<?= admin_url('account/voucher_report'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Voucher_Report'); ?></span></a></li>
                                            <li id="account_inventory_ledger" class=""><a class="submenu" href="<?= admin_url('account/inventory_ledger'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Inventory_Ledger'); ?></span></a></li>
                                            <li id="account_cash_book" class=""><a class="submenu" href="<?= admin_url('account/cash_book'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Cash_Book'); ?></span></a></li>
                                            <li id="account_bank_book" class=""><a class="submenu" href="<?= admin_url('account/bank_book'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Bank_Book'); ?></span></a></li>
                                            <li id="account_general_ledger" class=""><a class="submenu" href="<?= admin_url('account/general_ledger'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('General_Ledger'); ?></span></a></li>
                                            <li id="account_trial_balance" class=""><a class="submenu" href="<?= admin_url('account/trial_balance'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Trial_Balance'); ?></span></a></li>
                                            <li id="account_profit_loss_report" class=""><a class="submenu" href="<?= admin_url('account/profit_loss_report'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Profit_Loss'); ?></span></a></li>
                                            <li id="account_cash_flow_report" class=""><a class="submenu" href="<?= admin_url('account/cash_flow_report'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Cash_Flow'); ?></span></a></li>
                                            <li id="account_coa_print" class=""><a class="submenu" href="<?= admin_url('account/coa_print'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('COA_Print'); ?></span></a></li>
                                            <li id="account_balance_sheet" class=""><a class="submenu" href="<?= admin_url('account/balance_sheet'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Balance_Sheet'); ?></span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_bank">
                                <a class="dropmenu" href="#"><i class="fa fa-briefcase" aria-hidden="true"></i><span class="text"> <?= lang('bank'); ?> </span><span class="chevron closed"></span></a>
                                <ul>
                                    <li id="bank_bank_form"><a class="submenu" href="<?= admin_url('bank/bank_form'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Add_New_Bank'); ?></span></a></li>
                                    <li id="bank_bank_list"><a class="submenu" href="<?= admin_url('bank/bank_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Manage_Bank'); ?></span></a></li>
                                    <li id="bank_bank_transaction"><a class="submenu" href="<?= admin_url('bank/bank_transaction'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Bank_Transaction'); ?></span></a></li>
                                    <li id="bank_bank_ledger"><a class="submenu" href="<?= admin_url('bank/bank_ledger'); ?>"><i class="fa fa-plus-circle"></i><span class="text"><?= lang('Bank_Ledger'); ?></span></a></li>
                                </ul>
                            </li>
                            <li class="mm_tasks">
                            <a class="dropmenu" href="#"><i class="fa fa-credit-card" aria-hidden="true"></i><span class="text"> <?= lang('task_manager'); ?> </span><span class="chevron closed"></span></a>
                            <ul>
                                <li id="tasks_list_tasks"><a class="submenu" href="<?= admin_url('tasks/list_tasks'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('task_list'); ?></span></a></li>
                            </ul>
                            </li>
                           
                            <li class="mm_hrm">
                            <a class="dropmenu" href="#"><i class="fa fa-users" aria-hidden="true"></i><span class="text"> <?= lang('hrm'); ?> </span><span class="chevron closed"></span></a>
                            <ul>

                            <li class="mm_organizationss">
                                <a class="dropmenu" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i><span class="text"> <?= lang('hrm_organization'); ?> </span><span class="chevron closed"></span></a>
                                <ul>
                                    <li id="hrm_payroll_employees"><a class="submenu" href="<?= admin_url('employees'); ?>"><i class="fa fa-user"></i><span class="text"> <?= lang('employees'); ?></span></a></li>
                                    <li id="hrm_payroll_departments_list" class=""><a class="submenu" href="<?= admin_url('organizations/departments_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('departments'); ?></span></a></li>
                                    <li id="hrm_payroll_designations_list" class=""><a class="submenu" href="<?= admin_url('organizations/designations_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('designations'); ?></span></a></li>
                                    
                                </ul>
                            </li>


                            <li class="mm_hr_roster">
                            <a class="dropmenu" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i><span class="text"> <?= lang('hrm_roster'); ?> </span><span class="chevron closed"></span></a>
                            <ul>
                                <li id="hrm_payroll_period_master" class=""><a class="submenu" href="<?= admin_url('hrm_period_masters/period_master'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('period_master'); ?></span></a></li>
                                <li id="hrm_payroll_shift_master"><a class="submenu" href="<?= admin_url('hrm_shift_master/shift_master'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('shift_master'); ?></span></a></li>
                                <li id="hrm_payroll_employee_shift_mapping"><a class="submenu" href="<?= admin_url('hrm_shift_master/employee_shift_mapping'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('employee_shift_mapping'); ?></span></a></li>
                                <li id="hrm_payroll_rosters_list"><a class="submenu" href="<?= admin_url('hrm_roster/rosters_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('hrm_roster_process'); ?></span></a></li>
                                <li id="hrm_payroll_roster_details_list"><a class="submenu" href="<?= admin_url('hrm_roster_details/roster_details_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('hrm_roster_details'); ?></span></a></li>
                                <li id="hrm_payroll_roster_grid_list"><a class="submenu" href="<?= admin_url('hrm_roster_grid/roster_grid_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('roster_grid'); ?></span></a></li>
                                <li id="hrm_payroll_attendance_log_list"><a class="submenu" href="<?= admin_url('hrm_attendance/attendance_log_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('attendance_log'); ?></span></a></li>
                                <li id="hrm_payroll_punch_history"><a class="submenu" href="<?= admin_url('hrm_attendance/punch_history'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('punch_history'); ?></span></a></li>
                                
                            </ul>
                            </li>

                            <li class="mm_hr_payrolldd">
                            <a class="dropmenu" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i><span class="text"> <?= lang('hrm_payroll'); ?> </span><span class="chevron closed"></span></a>
                                <ul>
                                    <li id="hrm_payroll_add_deduct_list"><a class="submenu" href="<?= admin_url('hrm_payroll/add_deduct_list'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('addition_deduction'); ?></span></a></li>
                                    <li id="hrm_payroll_income_tax"><a class="submenu" href="<?= admin_url('hrm_payroll/income_tax'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('income_tax'); ?></span></a></li>
                                    <li id="hrm_payroll_payroll_policy"><a class="submenu" href="<?= admin_url('hrm_payroll/payroll_policy'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_policy'); ?></span></a></li>
                                    <li id="hrm_payroll_pay_grade"><a class="submenu" href="<?= admin_url('hrm_payroll/pay_grade'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('pay_grade'); ?></span></a></li>
                                    
                                    <li id="hrm_payroll_payroll_setup"><a class="submenu" href="<?= admin_url('hrm_payroll/payroll_setup'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_setup'); ?></span></a></li>
                                    <li id="hrm_payroll_payroll_salary_mst"><a class="submenu" href="<?= admin_url('hrm_payroll/payroll_salary_mst'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_salary_mst'); ?></span></a></li>
                                    
                                    <li id="hrm_payroll_payroll_bonus_setup"><a class="submenu" href="<?= admin_url('hrm_payslip/payroll_bonus_setup'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_bonus_setup'); ?></span></a></li>
                                    <li id="hrm_payroll_payroll_salary_group"><a class="submenu" href="<?= admin_url('hrm_payslip/payroll_salary_group'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_salary_group'); ?></span></a></li>
                                    <li id="hrm_payroll_payroll_loan_proof"><a class="submenu" href="<?= admin_url('hrm_payslip/payroll_loan_proof'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_loan_proof'); ?></span></a></li>
                                    <li id="hrm_payroll_payroll_loan_type"><a class="submenu" href="<?= admin_url('hrm_payslip/payroll_loan_type'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_loan_type'); ?></span></a></li>
                                    <li id="hrm_payroll_payroll_loan_request"><a class="submenu" href="<?= admin_url('hrm_payslip/payroll_loan_request'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_loan_request'); ?></span></a></li>
                                    <li id="hrm_payroll_payroll_loan_advance"><a class="submenu" href="<?= admin_url('hrm_payslip/payroll_loan_advance'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('payroll_loan_advance'); ?></span></a></li>
                                    
                                </ul> 
                            </li>

                            <li class="mm_hrm_payrollss">
                            <a class="dropmenu" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i><span class="text"> <?= lang('hrm_payslip'); ?> </span><span class="chevron closed"></span></a>
                                <ul>
                                <li id="hrm_payroll_index"><a class="submenu" href="<?= admin_url('hrm_payslip/index'); ?>"><i class="fa fa-plus-circle"></i><span class="text"> <?= lang('generate_payslip'); ?></span></a></li>
                                
                                </ul> 
                            </li> 
                        

                            </ul> 
                            </li>
                            <?php if ($Owner) {
                                ?>
                                <li class="mm_system_settings <?= strtolower($this->router->fetch_method()) == 'sales' ? '' : 'mm_pos' ?>">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-cog"></i><span class="text"> <?= lang('settings'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="system_settings_index">
                                            <a href="<?= admin_url('system_settings') ?>">
                                                <i class="fa fa-cogs"></i><span class="text"> <?= lang('system_settings'); ?></span>
                                            </a>
                                        </li>
                                        <?php if (POS) {
                                            ?>
                                        <li id="pos_settings">
                                            <a href="<?= admin_url('pos/settings') ?>">
                                                <i class="fa fa-th-large"></i><span class="text"> <?= lang('pos_settings'); ?></span>
                                            </a>
                                        </li>
                                        <li id="promos_index">
                                            <a href="<?= admin_url('promos') ?>">
                                                <i class="fa fa-cogs"></i><span class="text"> <?= lang('promos'); ?></span>
                                            </a>
                                        </li>
                                        <li id="pos_printers">
                                            <a href="<?= admin_url('pos/printers') ?>">
                                                <i class="fa fa-print"></i><span class="text"> <?= lang('list_printers'); ?></span>
                                            </a>
                                        </li>
                                        <li id="pos_add_printer">
                                            <a href="<?= admin_url('pos/add_printer') ?>">
                                                <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_printer'); ?></span>
                                            </a>
                                        </li>
                                            <?php
                                        } ?>
                                        <li id="system_settings_restaurant_table">
                                            <a href="<?= admin_url('system_settings/restaurant_tables') ?>" >
                                                <i class="fa fa-th-large"></i><span class="text"> <?= lang('restaurant_table'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_change_logo">
                                            <a href="<?= admin_url('system_settings/change_logo') ?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-upload"></i><span class="text"> <?= lang('change_logo'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_currencies">
                                            <a href="<?= admin_url('system_settings/currencies') ?>">
                                                <i class="fa fa-money"></i><span class="text"> <?= lang('currencies'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_customer_groups">
                                            <a href="<?= admin_url('system_settings/customer_groups') ?>">
                                                <i class="fa fa-chain"></i><span class="text"> <?= lang('customer_groups'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_price_groups">
                                            <a href="<?= admin_url('system_settings/price_groups') ?>">
                                                <i class="fa fa-dollar"></i><span class="text"> <?= lang('price_groups'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_categories">
                                            <a href="<?= admin_url('system_settings/categories') ?>">
                                                <i class="fa fa-folder-open"></i><span class="text"> <?= lang('categories'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_expense_categories">
                                            <a href="<?= admin_url('system_settings/expense_categories') ?>">
                                                <i class="fa fa-folder-open"></i><span class="text"> <?= lang('expense_categories'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_units">
                                            <a href="<?= admin_url('system_settings/units') ?>">
                                                <i class="fa fa-wrench"></i><span class="text"> <?= lang('units'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_brands">
                                            <a href="<?= admin_url('system_settings/brands') ?>">
                                                <i class="fa fa-th-list"></i><span class="text"> <?= lang('brands'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_variants">
                                            <a href="<?= admin_url('system_settings/variants') ?>">
                                                <i class="fa fa-tags"></i><span class="text"> <?= lang('variants'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_tax_rates">
                                            <a href="<?= admin_url('system_settings/tax_rates') ?>">
                                                <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('tax_rates'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_warehouses">
                                            <a href="<?= admin_url('system_settings/warehouses') ?>">
                                                <i class="fa fa-building-o"></i><span class="text"> <?= lang('warehouses'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_email_templates">
                                            <a href="<?= admin_url('system_settings/email_templates') ?>">
                                                <i class="fa fa-envelope"></i><span class="text"> <?= lang('email_templates'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_user_groups">
                                            <a href="<?= admin_url('system_settings/user_groups') ?>">
                                                <i class="fa fa-key"></i><span class="text"> <?= lang('group_permissions'); ?></span>
                                            </a>
                                        </li>
                                        <li id="site_logs_index">
                                            <a href="<?= admin_url('site_logs') ?>">
                                                <i class="fa fa-file-text"></i><span class="text"> <?= lang('site_logs'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_backups">
                                            <a href="<?= admin_url('system_settings/backups') ?>">
                                                <i class="fa fa-database"></i><span class="text"> <?= lang('backups'); ?></span>
                                            </a>
                                        </li>
                                        <!-- <li id="system_settings_updates">
                                            <a href="<?= admin_url('system_settings/updates') ?>">
                                                <i class="fa fa-upload"></i><span class="text"> <?= lang('updates'); ?></span>
                                            </a>
                                        </li> -->
                                    </ul>
                                </li>
                                <?php
                            } ?>
                            <li class="mm_reports">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <span class="text"> <?= lang('reports'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="reports_index">
                                        <a href="<?= admin_url('reports') ?>">
                                            <i class="fa fa-bars"></i><span class="text"> <?= lang('overview_chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_warehouse_stock">
                                        <a href="<?= admin_url('reports/warehouse_stock') ?>">
                                            <i class="fa fa-building"></i><span class="text"> <?= lang('warehouse_stock'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_best_seller_products">
                                        <a href="<?= admin_url('reports/best_seller_products') ?>">
                                            <i class="fa fa-barcode"></i><span class="text"> <?= ('Best Selling Products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_best_sellers">
                                        <a href="<?= admin_url('reports/best_sellers') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= lang('best_sellers'); ?></span>
                                        </a>
                                    </li>
                                    <!-- <li id="reports_order_time_charts">
                                        <a href="<?= admin_url('reports/order_time_charts') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Daily Order Time Charts'); ?></span>
                                        </a>
                                    </li> -->
                                    <li id="reports_order_time_charts_drill">
                                        <a href="<?= admin_url('reports/order_time_charts_drill') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Daily Order Traffic Time'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_order_time_chart_dine">
                                        <a href="<?= admin_url('reports/order_time_chart_dine') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Daily Order Traffic Dine'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_best_selling_products_qty">
                                        <a href="<?= admin_url('reports/best_selling_products_qty') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Best Selling Product Qty'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_best_selling_products_amount">
                                        <a href="<?= admin_url('reports/best_selling_products_amount') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Best Selling Product Amount'); ?></span>
                                        </a>
                                    </li>
                                    <!-- <li id="reports_best_selling_products_pie">
                                        <a href="<?= admin_url('reports/best_selling_products_pie') ?>">
                                            <i class="fa fa-pie-chart"></i><span class="text"> <?= ('Best Selling Product Pie'); ?></span>
                                        </a>
                                    </li> -->
                                    <li id="reports_daily_best_products_chart">
                                        <a href="<?= admin_url('reports/daily_best_products_chart') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Daily Best Selling Product'); ?></span>
                                        </a>
                                    </li>
                                    
                                    <li id="reports_order_chart_items_percent">
                                        <a href="<?= admin_url('reports/order_chart_items_percent') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Products Percentage Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_top_sales_chart">
                                        <a href="<?= admin_url('reports/top_sales_chart') ?>">
                                            <i class="fa fa fa-bar-chart"></i><span class="text"> <?= ('Top 10 Sales Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_sales_chart">
                                        <a href="<?= admin_url('reports/daily_sales_chart') ?>">
                                            <i class="fa fa fa-bar-chart"></i><span class="text"> <?= ('Daily Sales Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_sales_chart">
                                        <a href="<?= admin_url('reports/monthly_sales_chart') ?>">
                                            <i class="fa fa-bar-chart"></i><span class="text"> <?= ('Monthly Sales Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_sales_summary_chart">
                                        <a href="<?= admin_url('reports/monthly_sales_summary_chart') ?>">
                                            <i class="fa fa-bar-chart"></i><span class="text"> <?= ('Monthly Sales Summary Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_yearly_sales_chart">
                                        <a href="<?= admin_url('reports/yearly_sales_chart') ?>">
                                            <i class="fa fa-bar-chart"></i><span class="text"> <?= ('Yearly Sales Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_sales_vs_purchases_chart">
                                        <a href="<?= admin_url('reports/sales_vs_purchases_chart') ?>">
                                            <i class="fa fa-area-chart "></i><span class="text"> <?= ('Purchases vs Sales Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_sales_vs_purchases_yearly">
                                        <a href="<?= admin_url('reports/sales_vs_purchases_yearly') ?>">
                                            <i class="fa fa-line-chart"></i><span class="text"> <?= ('Purchases vs Sales Yearly'); ?></span>
                                        </a>
                                    </li>
                                    
                                    <li id="reports_categories_sales_chart">
                                        <a href="<?= admin_url('reports/categories_sales_chart') ?>">
                                            <i class="fa fa fa-bar-chart-o"></i><span class="text"> <?= ('Categories Sales Chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_categories_sales_pie">
                                        <a href="<?= admin_url('reports/categories_sales_pie') ?>">
                                            <i class="fa fa fa-pie-chart"></i><span class="text"> <?= ('Categories Sales Qty'); ?></span>
                                        </a>
                                    </li>
                                    
                                    <?php if (POS) {
                                        ?>
                                    <li id="reports_register">
                                        <a href="<?= admin_url('reports/register') ?>">
                                            <i class="fa fa-th-large"></i><span class="text"> <?= lang('register_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <li id="reports_quantity_alerts">
                                        <a href="<?= admin_url('reports/quantity_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_quantity_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($Settings->product_expiry) {
                                        ?>
                                    <li id="reports_expiry_alerts">
                                        <a href="<?= admin_url('reports/expiry_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_expiry_alerts'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <li id="reports_products">
                                        <a href="<?= admin_url('reports/products') ?>">
                                            <i class="fa fa-barcode"></i><span class="text"> <?= lang('products_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_products_sales_yearly">
                                        <a href="<?= admin_url('reports/products_sales_yearly') ?>">
                                            <i class="fa fa-barcode"></i><span class="text"> <?= ('Products Sales Yearly'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_products_report_by_date">
                                        <a href="<?= admin_url('reports/products_report_by_date') ?>">
                                            <i class="fa fa-barcode"></i><span class="text"> <?= lang('products_report_by_date'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_adjustments">
                                        <a href="<?= admin_url('reports/adjustments') ?>">
                                            <i class="fa fa-filter"></i><span class="text"> <?= lang('adjustments_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_categories">
                                        <a href="<?= admin_url('reports/categories') ?>">
                                            <i class="fa fa-folder-open"></i><span class="text"> <?= lang('categories_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_brands">
                                        <a href="<?= admin_url('reports/brands') ?>">
                                            <i class="fa fa-cubes"></i><span class="text"> <?= lang('brands_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_sales">
                                        <a href="<?= admin_url('reports/daily_sales') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('daily_sales'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_sales_summary">
                                        <a href="<?= admin_url('reports/daily_sales_summary') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= ('Daily Sales Summary'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_sales">
                                        <a href="<?= admin_url('reports/monthly_sales') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('monthly_sales'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_sales_summary">
                                        <a href="<?= admin_url('reports/monthly_sales_summary') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= ('Monthly Sales Summary'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_yearly_summary">
                                        <a href="<?= admin_url('reports/daily_yearly_summary') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= ('Yearly Sales Summary'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_sales">
                                        <a href="<?= admin_url('reports/sales') ?>">
                                            <i class="fa fa-heart"></i><span class="text"> <?= lang('sales_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_payments">
                                        <a href="<?= admin_url('reports/payments') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('payments_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_tax">
                                        <a href="<?= admin_url('reports/tax') ?>">
                                            <i class="fa fa-area-chart"></i><span class="text"> <?= lang('tax_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_profit_loss">
                                        <a href="<?= admin_url('reports/profit_loss') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('profit_and_loss'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_purchases">
                                        <a href="<?= admin_url('reports/daily_purchases') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('daily_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_purchases">
                                        <a href="<?= admin_url('reports/monthly_purchases') ?>">
                                            <i class="fa fa-calendar"></i><span class="text"> <?= lang('monthly_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_purchases">
                                        <a href="<?= admin_url('reports/purchases') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('purchases_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_expenses">
                                        <a href="<?= admin_url('reports/expenses') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('expenses_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_customer_report">
                                        <a href="<?= admin_url('reports/customers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('customers_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_supplier_report">
                                        <a href="<?= admin_url('reports/suppliers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('suppliers_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_staff_report">
                                        <a href="<?= admin_url('reports/users') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('staff_report'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php if ($Owner && file_exists(APPPATH . 'controllers' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'Shop.php')) {
                                ?>
                            <li class="mm_shop_settings mm_api_settings">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-shopping-cart"></i><span class="text"> <?= lang('front_end'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="shop_settings_index">
                                        <a href="<?= admin_url('shop_settings') ?>">
                                            <i class="fa fa-cog"></i><span class="text"> <?= lang('shop_settings'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_slider">
                                        <a href="<?= admin_url('shop_settings/slider') ?>">
                                            <i class="fa fa-file"></i><span class="text"> <?= lang('slider_settings'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($Settings->apis) {
                                        ?>
                                    <li id="api_settings_index">
                                        <a href="<?= admin_url('api_settings') ?>">
                                            <i class="fa fa-key"></i><span class="text"> <?= lang('api_keys'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <li id="shop_settings_pages">
                                        <a href="<?= admin_url('shop_settings/pages') ?>">
                                            <i class="fa fa-file"></i><span class="text"> <?= lang('list_pages'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_pages">
                                        <a href="<?= admin_url('shop_settings/add_page') ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_page'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_sms_settings">
                                        <a href="<?= admin_url('shop_settings/sms_settings') ?>">
                                            <i class="fa fa-cogs"></i><span class="text"> <?= lang('sms_settings'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_send_sms">
                                        <a href="<?= admin_url('shop_settings/send_sms') ?>">
                                            <i class="fa fa-send"></i><span class="text"> <?= lang('send_sms'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_sms_log">
                                        <a href="<?= admin_url('shop_settings/sms_log') ?>">
                                            <i class="fa fa-file-text-o"></i><span class="text"> <?= lang('sms_log'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                                <?php
                            }
                        } else {
                            if ($GP['products-index'] || $GP['products-add'] || $GP['products-barcode'] || $GP['products-adjustments'] || $GP['products-stock_count']) {
                                ?>
                            <li class="mm_products">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-barcode"></i>
                                    <span class="text"> <?= lang('products'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="products_index">
                                        <a class="submenu" href="<?= admin_url('products'); ?>">
                                            <i class="fa fa-barcode"></i><span class="text"> <?= lang('list_products'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['products-add']) {
                                        ?>
                                    <li id="products_add">
                                        <a class="submenu" href="<?= admin_url('products/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_product'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <?php if ($GP['products-barcode']) {
                                        ?>
                                    <li id="products_sheet">
                                        <a class="submenu" href="<?= admin_url('products/print_barcodes'); ?>">
                                            <i class="fa fa-tags"></i><span class="text"> <?= lang('print_barcode_label'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <?php if ($GP['products-adjustments']) {
                                        ?>
                                    <li id="products_quantity_adjustments">
                                        <a class="submenu" href="<?= admin_url('products/quantity_adjustments'); ?>">
                                            <i class="fa fa-filter"></i><span class="text"> <?= lang('quantity_adjustments'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_add_adjustment">
                                        <a class="submenu" href="<?= admin_url('products/add_adjustment'); ?>">
                                            <i class="fa fa-filter"></i>
                                            <span class="text"> <?= lang('add_adjustment'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <?php if ($GP['products-stock_count']) {
                                        ?>
                                    <li id="products_stock_counts">
                                        <a class="submenu" href="<?= admin_url('products/stock_counts'); ?>">
                                            <i class="fa fa-list-ol"></i>
                                            <span class="text"> <?= lang('stock_counts'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_count_stock">
                                        <a class="submenu" href="<?= admin_url('products/count_stock'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('count_stock'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php if ($GP['sales-index'] || $GP['sales-add'] || $GP['sales-deliveries'] || $GP['sales-gift_cards']) {
                                ?>
                            <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'sales' ? 'mm_pos' : '' ?>">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-heart"></i>
                                    <span class="text"> <?= lang('sales'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= admin_url('sales'); ?>">
                                            <i class="fa fa-heart"></i><span class="text"> <?= lang('list_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php if (POS && $GP['pos-index']) {
                                        ?>
                                    <li id="pos_sales">
                                        <a class="submenu" href="<?= admin_url('pos/sales'); ?>">
                                            <i class="fa fa-heart"></i><span class="text"> <?= lang('pos_sales'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <?php if ($GP['sales-add']) {
                                        ?>
                                    <li id="sales_add">
                                        <a class="submenu" href="<?= admin_url('sales/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_sale'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['sales-deliveries']) {
                                        ?>
                                    <li id="sales_deliveries">
                                        <a class="submenu" href="<?= admin_url('sales/deliveries'); ?>">
                                            <i class="fa fa-truck"></i><span class="text"> <?= lang('deliveries'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['sales-gift_cards']) {
                                        ?>
                                    <li id="sales_gift_cards">
                                        <a class="submenu" href="<?= admin_url('sales/gift_cards'); ?>">
                                            <i class="fa fa-gift"></i><span class="text"> <?= lang('gift_cards'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php if ($GP['quotes-index'] || $GP['quotes-add']) {
                                ?>
                            <li class="mm_quotes">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-heart-o"></i>
                                    <span class="text"> <?= lang('quotes'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= admin_url('quotes'); ?>">
                                            <i class="fa fa-heart-o"></i><span class="text"> <?= lang('list_quotes'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['quotes-add']) {
                                        ?>
                                    <li id="sales_add">
                                        <a class="submenu" href="<?= admin_url('quotes/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_quote'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php if ($GP['purchases-index'] || $GP['purchases-add'] || $GP['purchases-expenses']) {
                                ?>
                            <li class="mm_purchases">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="text"> <?= lang('purchases'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="purchases_index">
                                        <a class="submenu" href="<?= admin_url('purchases'); ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('list_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['purchases-add']) {
                                        ?>
                                    <li id="purchases_add">
                                        <a class="submenu" href="<?= admin_url('purchases/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_purchase'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                    <?php if ($GP['purchases-expenses']) {
                                        ?>
                                    <li id="purchases_expenses">
                                        <a class="submenu" href="<?= admin_url('purchases/expenses'); ?>">
                                            <i class="fa fa-dollar"></i><span class="text"> <?= lang('list_expenses'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_add_expense">
                                        <a class="submenu" href="<?= admin_url('purchases/add_expense'); ?>"
                                            data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_expense'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php if ($GP['transfers-index'] || $GP['transfers-add']) {
                                ?>
                            <li class="mm_transfers">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star-o"></i>
                                    <span class="text"> <?= lang('transfers'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="transfers_index">
                                        <a class="submenu" href="<?= admin_url('transfers'); ?>">
                                            <i class="fa fa-star-o"></i><span class="text"> <?= lang('list_transfers'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['transfers-add']) {
                                        ?>
                                    <li id="transfers_add">
                                        <a class="submenu" href="<?= admin_url('transfers/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_transfer'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php if ($GP['returns-index'] || $GP['returns-add']) {
                                ?>
                            <li class="mm_returns">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-random"></i>
                                    <span class="text"> <?= lang('returns'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="returns_index">
                                        <a class="submenu" href="<?= admin_url('returns'); ?>">
                                            <i class="fa fa-random"></i><span class="text"> <?= lang('list_returns'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['returns-add']) {
                                        ?>
                                    <li id="returns_add">
                                        <a class="submenu" href="<?= admin_url('returns/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_return'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php if ($GP['customers-index'] || $GP['customers-add'] || $GP['suppliers-index'] || $GP['suppliers-add']) {
                                ?>
                            <li class="mm_auth mm_customers mm_suppliers mm_billers">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-users"></i>
                                    <span class="text"> <?= lang('people'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <?php if ($GP['customers-index']) {
                                        ?>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= admin_url('customers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_customers'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['customers-add']) {
                                        ?>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= admin_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_customer'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['suppliers-index']) {
                                        ?>
                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= admin_url('suppliers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_suppliers'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['suppliers-add']) {
                                        ?>
                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= admin_url('suppliers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_supplier'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php if ($GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts'] || $GP['reports-products'] || $GP['reports-monthly_sales'] || $GP['reports-sales'] || $GP['reports-payments'] || $GP['reports-purchases'] || $GP['reports-customers'] || $GP['reports-suppliers'] || $GP['reports-staff'] || $GP['reports-expenses']) {
                                ?>
                            <li class="mm_reports">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <span class="text"> <?= lang('reports'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <?php if ($GP['reports-quantity_alerts']) {
                                        ?>
                                    <li id="reports_quantity_alerts">
                                        <a href="<?= admin_url('reports/quantity_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_quantity_alerts'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-expiry_alerts']) {
                                        ?>
                                        <?php if ($Settings->product_expiry) {
                                            ?>
                                    <li id="reports_expiry_alerts">
                                        <a href="<?= admin_url('reports/expiry_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_expiry_alerts'); ?></span>
                                        </a>
                                    </li>
                                            <?php
                                        } ?>
                                        <?php
                                    }
                                    if ($GP['reports-products']) {
                                        ?>
                                    <li id="reports_products">
                                        <a href="<?= admin_url('reports/products') ?>">
                                            <i class="fa fa-filter"></i><span class="text"> <?= lang('products_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_adjustments">
                                        <a href="<?= admin_url('reports/adjustments') ?>">
                                            <i class="fa fa-barcode"></i><span class="text"> <?= lang('adjustments_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_categories">
                                        <a href="<?= admin_url('reports/categories') ?>">
                                            <i class="fa fa-folder-open"></i><span class="text"> <?= lang('categories_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_brands">
                                        <a href="<?= admin_url('reports/brands') ?>">
                                            <i class="fa fa-cubes"></i><span class="text"> <?= lang('brands_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-daily_sales']) {
                                        ?>
                                    <li id="reports_daily_sales">
                                        <a href="<?= admin_url('reports/daily_sales') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('daily_sales'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-monthly_sales']) {
                                        ?>
                                    <li id="reports_monthly_sales">
                                        <a href="<?= admin_url('reports/monthly_sales') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('monthly_sales'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-sales']) {
                                        ?>
                                    <li id="reports_sales">
                                        <a href="<?= admin_url('reports/sales') ?>">
                                            <i class="fa fa-heart"></i><span class="text"> <?= lang('sales_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-payments']) {
                                        ?>
                                    <li id="reports_payments">
                                        <a href="<?= admin_url('reports/payments') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('payments_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-tax']) {
                                        ?>
                                    <li id="reports_tax">
                                        <a href="<?= admin_url('reports/tax') ?>">
                                            <i class="fa fa-area-chart"></i><span class="text"> <?= lang('tax_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-daily_purchases']) {
                                        ?>
                                    <li id="reports_daily_purchases">
                                        <a href="<?= admin_url('reports/daily_purchases') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('daily_purchases'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-monthly_purchases']) {
                                        ?>
                                    <li id="reports_monthly_purchases">
                                        <a href="<?= admin_url('reports/monthly_purchases') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('monthly_purchases'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-purchases']) {
                                        ?>
                                    <li id="reports_purchases">
                                        <a href="<?= admin_url('reports/purchases') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('purchases_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-expenses']) {
                                        ?>
                                    <li id="reports_expenses">
                                        <a href="<?= admin_url('reports/expenses') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('expenses_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-customers']) {
                                        ?>
                                    <li id="reports_customer_report">
                                        <a href="<?= admin_url('reports/customers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('customers_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-suppliers']) {
                                        ?>
                                    <li id="reports_supplier_report">
                                        <a href="<?= admin_url('reports/suppliers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('suppliers_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    }
                                    if ($GP['reports-staff']) {
                                        ?>
                                    <li id="reports_staff_report">
                                        <a href="<?= admin_url('reports/users') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('staff_report'); ?></span>
                                        </a>
                                    </li>
                                        <?php
                                    } ?>
                                </ul>
                            </li>
                                <?php
                            } ?>

                            <?php
                        } ?>
                    </ul>
                </div>
                <a href="#" id="main-menu-act" class="full visible-md visible-lg">
                    <i class="fa fa-angle-double-left"></i>
                </a>
            </div>
            </td><td class="content-con">
            <div id="content">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <ul class="breadcrumb">
                            <?php
                            foreach ($bc as $b) {
                                if ($b['link'] === '#') {
                                    echo '<li class="active">' . $b['page'] . '</li>';
                                } else {
                                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                                }
                            }
                            ?>
                            <li class="right_log hidden-xs">
                                <?= lang('your_ip') . ' ' . $ip_address . " <span class='hidden-sm'>( " . lang('last_login_at') . ': ' . date($dateFormats['php_ldate'], $this->session->userdata('old_last_login')) . ' ' . ($this->session->userdata('last_ip') != $ip_address ? lang('ip:') . ' ' . $this->session->userdata('last_ip') : '') . ' )</span>' ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($message) {
                            ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= $message; ?>
                            </div>
                            <?php
                        } ?>
                        <?php if ($error) {
                            ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= $error; ?>
                            </div>
                            <?php
                        } ?>
                        <?php if ($warning) {
                            ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= $warning; ?>
                            </div>
                            <?php
                        } ?>
                        <?php
                        if ($info) {
                            foreach ($info as $n) {
                                if (!$this->session->userdata('hidden' . $n->id)) {
                                    ?>
                                    <div class="alert alert-info">
                                        <a href="#" id="<?= $n->id ?>" class="close hideComment external"
                                           data-dismiss="alert">&times;</a>
                                        <?= $n->comment; ?>
                                    </div>
                                    <?php
                                }
                            }
                        } ?>
                        <div class="alerts-con"></div>

<script>
$(document).ready(function(){

    let url = window.location.href;
    let urlSplit = url.split("/");
    let urlLastSegment, controllerSegment; 
    urlLastSegment = urlSplit[urlSplit.length - 1];
    
    let urlLastSegmentInt = parseInt(urlLastSegment);
    if (isNaN(urlLastSegmentInt)) {
        urlLastSegment = urlSplit[urlSplit.length - 1];
        controllerSegment = urlSplit[urlSplit.length - 2];
    } else {
        urlLastSegment = urlSplit[urlSplit.length - 2];
        controllerSegment = urlSplit[urlSplit.length - 3];
    }
    
    let elements = document.querySelectorAll('.mm_hrm > ul > li > ul > li');

    for(element of elements) {
        if(controllerSegment = 'employees' && urlLastSegment == 'detail') {
            urlLastSegment = 'employees';
        }
        if(element.id == 'hrm_payroll_'+urlLastSegment) {
            $('.mm_hrm').addClass('active');
            $(element).parent().show();
            $('.mm_hrm').find("ul").first().slideToggle();
            $('#hrm_payroll_'+urlLastSegment).addClass('active');
            $('.mm_hrm a .chevron').removeClass("closed").addClass("opened");
        }
    }
});
</script>
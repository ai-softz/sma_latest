<html>
    <head>
        <style>
            body {font-family: sans-serif;
                font-size: 10pt;
            }
            p {	margin: 0pt; }
            table.items {
                border: 0.1mm solid #000000;
            }
            td { vertical-align: top; }
            .items td {
                border-left: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
            }
            table thead td { background-color: #EEEEEE;
                text-align: center;
                border: 0.1mm solid #000000;
                font-variant: small-caps;
            }
            .items td.blanktotal {
                background-color: #EEEEEE;
                border: 0.1mm solid #000000;
                background-color: #FFFFFF;
                border: 0mm none #000000;
                border-top: 0.1mm solid #000000;
                border-right: 0.1mm solid #000000;
            }
            .items td.totals {
                text-align: right;
                border: 0.1mm solid #000000;
            }
            .items td.cost {
                text-align: "." center;
            }
            .header {
                display: flex;
                
            }
            .emp_info {
                float: left;
                width: 50%;
            }
            .address {
                float: right;
                width: 50%;
                text-align: right;
            }
        </style>
    </head>
    <body>
        <div class="header">
            <div class="emp_info">
                <h2> Employee Information </h2>
            </div>
            <div class="address">
                <address> Address: Eshtri </address>
            </div>
        </div>
        <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
            <thead>
                <tr>
                    <td width="10%">SL</td>
                    <td width="">Employee ID</td>
                    <td width="">Name</td>
                    <td width="">Department</td>
                    <td width="">Designation</td>
                    <td width="">Warehouse</td>
                    <td width="">Contact</td>
                    <td width="">Email</td>
                </tr>
            </thead>
            <tbody>
                <?php 
                $serial = 1;
                foreach($employees as $row) { 
                ?>
                <tr>
                    <td> <?= $serial; ?> </td>
                    <td> <?= $row->employee_id; ?> </td>
                    <td> <?= $row->whole_name; ?> </td>
                    <td> <?= $row->department_name; ?> </td>
                    <td> <?= $row->designation_name; ?> </td>
                    <td> <?= $row->warehouse_name; ?> </td>
                    <td> <?= $row->contact_no; ?> </td>
                    <td> <?= $row->email; ?> </td>
                </tr>
                <?php 
                $serial++;
                } 
                ?>
                
            </tbody>
        </table>

    <div style="margin-top: 20px; font-style: italic; ">Footer information</div>
    </body>
</html>
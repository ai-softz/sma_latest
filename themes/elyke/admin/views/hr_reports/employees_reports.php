<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script>
    $(document).ready(function () {
        oTable = $('#employee_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('hr_reports/employees_list') ?>',
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [ null, null,  null, null, null, null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false} ]
        });
    });

</script>


<div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>
                
                <a href="<?= admin_url('hr_reports/employee_excel') ?>" class="btn btn-primary" style="margin-bottom:5px; float: right"> <i class="fa fa-download"> </i>  Download Excel</a>
                <a href="<?= admin_url('hr_reports/employee_pdf') ?>" target="_blank" class="btn btn-primary" style="margin-bottom:5px; float: right"> <i class="fa fa-download"> </i>  Download Pdf</a> 
                
                <div class="table-responsive">
                    <table id="employee_table" class="table table-bordered table-hover table-striped reports-table">
                        <thead>
                            <tr>
                                
                                <th><?= lang('the_number_sign'); ?></th>
                                <th><?= lang('name'); ?></th>
                                <th><?= lang('employee_id'); ?></th>
                                <th><?= lang('department'); ?></th>
                                <th><?= lang('designation'); ?></th>
                                <th><?= lang('warehouse'); ?></th>
                                <th><?= lang('contact'); ?></th>
                                <th><?= lang('email'); ?></th>
                                <th><?= lang('date_of_joining'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7" class="dataTables_empty">
                                    <?= lang('loading_data_from_server') ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
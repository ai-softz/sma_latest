<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payroll_setup'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'add_payroll_setup']; ?>
                <?php echo admin_form_open_multipart("hrm_payroll/add_payroll_setup", $attrib) ?>

            <div class="form-group row">
                <div class="col-md-6">
				   <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
				   	<input type="text" id="departmentId" name="department" placeholder="Type department name..." class="form-control">
                    
		      	</div> 
				<div class="col-md-6">
				   <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <div id="emp_area">
						<input type="text" name="employee" id="empid2" placeholder="Type employee name..." class="form-control">
                    </div>
		      	</div> 
            </div>
            
            <div class="form-group row">
                <div class="col-md-6">
					<label for="payment_frequency_type"><?php echo  lang('payment_frequency_type') ?> <span class="text-danger"></span></label>
                    <select name="payment_frequency_type" class="form-control select" id="payment_frequency_type">
						<option value=""><?php echo 'Select';?></option>
						<option value="Monthly"><?php echo 'Monthly'?></option>
						<option value="Hourly"><?php echo 'Hourly'?></option>
                    </select>
		        </div>
                <div class="col-md-6">
                    <label for="pay_grade_id"><?php echo  lang('pay_grade_id') ?> <span class="text-danger"></span></label>
                    <select name="pay_grade_id" class="form-control select" id="pay_grade_id">
                        <option value=""><?php echo 'Select';?></option>
                        <!-- <?php foreach($all_pay_grade as $row) { ?>
						    <option value="<?= $row->id ?>"><?php echo $row->caption;?></option>
                        <?php } ?> -->
						
                    </select> 
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-6">
					<label for="bonus_eligibile"><?php echo  lang('bonus_eligibile') ?> <span class="text-danger"></span></label>
                    <select name="bonus_eligibile[]" class="js-example-basic-multiple" multiple="multiple" style="width: 100%" id="" tabindex="-1">
                        <?php foreach($payroll_bonuses as $row) { ?> 
                        <option value="<?= $row->id ?>"><?= $row->caption?></option>
                        <?php } ?>
                    </select>
		        </div>
                <div class="col-md-6">
                    <label for="payment_type"><?php echo  lang('payment_type') ?> <span class="text-danger"></span></label>
                    <select name="payment_type" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <option value="Bank"><?php echo 'Bank'?></option>
						<option value="Cash"><?php echo 'Cash'?></option>
                        <option value="Cheque"><?php echo 'Cheque'?></option>
						
                    </select> 
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="hourly_rate"><?php echo  lang('hourly_rate') ?> <span class="text-danger"></span></label>
                    <input type="text" name="hourly_rate" id="hourly_rate" class="form-control" value="">
                </div>  
                <div class="col-md-6">
                    <label for="basic_percent"><?php echo  lang('basic_percent') ?> <span class="text-danger"></span></label>
                    <input type="text" name="basic_percent" id="basic_percent" class="form-control" value="">
                </div> 
                
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="gross_salary"><?php echo  lang('gross_salary') ?> <span class="text-danger"></span></label>
                    <input type="text" name="gross_salary" id="gross_salary" class="form-control" value="">
                </div>  
                <div class="col-md-6">
                    <label for="basic_salary"><?php echo  lang('basic_salary') ?> <span class="text-danger"></span></label>
                    <input type="text" name="basic_salary" id="basic_salary" class="form-control" value="">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="net_salary"><?php echo  lang('net_salary') ?> <span class="text-danger"></span></label>
                    <input type="text" name="net_salary" id="net_salary" class="form-control" value="">
                </div>  
                <div class="col-md-6">
                    <label for="tax_amount"><?php echo  lang('tax_amount') ?> <span class="text-danger"></span></label>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <input type="checkbox" name="tax_effect">
                        </div> 
                        <div class="col-md-9">
                            <input type="text" name="tax_amount" id="tax_amount" class="form-control" value="">
                        </div> 
                    </div> 
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="income_tax_id"><?php echo  lang('income_tax_id') ?> <span class="text-danger"></span></label>
                    <select name="income_tax_id" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($all_income_tax as $row) { ?>
						    <option value="<?= $row->id ?>"><?php echo $row->caption_alt;?></option>
                        <?php } ?>
                    </select>
		        </div>
                <div class="col-md-6">
                    <label for="overtime_rate"><?php echo  lang('overtime_rate') ?> <span class="text-danger"></span></label>
                    <input type="text" name="overtime_rate" id="overtime_rate" class="form-control" value="">
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="account_id"><?php echo  lang('account_id') ?> <span class="text-danger"></span></label>
                    <select name="account_id" class="form-control select" id="">
                        <option value=""><?php echo 'Select';?></option>
                        <?php foreach($all_bank_accounts as $row) { ?>
						    <option value="<?= $row->id ?>"><?php echo $row->caption;?></option>
                        <?php } ?>
                    </select>
		        </div>
                <div class="col-md-6">
                    <label for="iban_number"><?php echo  lang('iban_number') ?> <span class="text-danger"></span></label>
                    <input type="text" name="iban_number" class="form-control" value="">
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="account_number"><?php echo  lang('account_number') ?> <span class="text-danger"></span></label>
                    <input type="text" name="account_number" class="form-control" value="">
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                    <textarea name="remark" class="form-control"></textarea>
                </div>  
            </div>
            <div class="form-group row">
            
                <div class="col-md-12">
                    <table id="hrmPrlSetupDtls-table" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr> 
                                <th>SL</th>
                                <th>Payroll Head</th>
                                <th>Pay Nature</th>
                                <th>Addiction Deduction</th>
                                <th>Allowance Type</th>
                                <th>Actual Amount</th>
                                <th>Max Limit</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-group">
                    <?php echo form_submit('add_income_tax', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script>
$(document).ready(function(){

    $('#add_payroll_setup').on('submit', function(e){  
        e.preventDefault();       
        var formdata = new FormData(this);
        // console.log(formdata);
        $.ajax({  
                url: site.base_url + "hrm_payroll/add_payroll_setup",
                method:"POST",  
                data:new FormData(this),  
                contentType: false,  
                cache: false,  
                processData:false,  
                success:function(data)  
                {  
                        $('#myModal').modal('hide');
                        location.reload();
                }
        });  
    });
    $(document).on('change', '#pay_grade_id', function(){
        const payGradeId = $(this).val();
        $.ajax({

            url: site.base_url + 'hrm_payroll/getPayGradeDetail',
            method: 'POST',
            data: { payGradeId : payGradeId },
            success: function(data) {
                $('#hrmPrlSetupDtls-table > tbody').html('');
                var rowNumber = 1;
                // console.log(data.payGradeDetail);
                if (data.payGrade.payment_frequency_type == "Monthly") {
                    if (data.payGradeDetail != null) {
                        $.each(data.payGradeDetail, function (key, value) {
                            addDetailInTable(rowNumber++, '', value.payroll_head_id, value.payroll_head_text, value.pay_nature, value.addition_deduction, value.allowance_type, value.actual_amount, value.max_limit, value.percentage_of_basic);
                        });
                    }
                    $("#gross_salary").val(data.payGrade.gross_salary);
                    $("#basic_percent").val(data.payGrade.basic_percent);
                    $("#basic_salary").val(data.payGrade.basic_salary);
                    $("#overtime_rate").val(data.payGrade.overtime_rate);
                } else if (data.payGrade.payment_frequency_type == "Hourly") {
                    $("#hourly_rate").val(data.payGrade.hourly_rate);
                }
                totalAmount();
            }
        });
    });
    function addDetailInTable(rowNumber, id, payrollHeadId, payrollHeadText, payNature, additionDeduction, allowanceType, actualAmt, maxLimit, percentageOfBasic, additionDeductionKey='', allowanceTypeKey='') {
        // <td><input type="hidden" class="form-control" name="id" value="${id}"></td>
        const html = `
        <tr>
            
            <td> 
                ${rowNumber} 
                <input type="hidden" name="row_no[]"> 
                <input type="hidden" name="payroll_head_id[]" value="${payrollHeadId}">
                
            </td>
            <td><input type="text" class="form-control payroll_head_text" name="payroll_head_text[]" value="${payrollHeadText}" readonly></td>
            <td><input type="text" class="form-control pay_nature" name="pay_nature[]" value="${payNature}" readonly></td>
            <td><input type="text" class="form-control addition_deduction" name="addition_deduction[]" value="${additionDeduction}" readonly></td>
            <td><input type="text" class="form-control allowance_type" name="allowance_type[]" value="${allowanceType}" readonly></td>
            <td><input type="text" class="form-control actual_amount" name="actual_amount[]" value="${actualAmt}" ></td>
            <td><input type="text" class="form-control max_limit" name="max_limit[]" value="${maxLimit}"></td>
        </tr>
        `;
        $('#hrmPrlSetupDtls-table > tbody').append(html);
    }

    $(document).on('change', '#payment_frequency_type', function(){
        const paymentFrequencyType = $(this).val();
        $.ajax({

            url: site.base_url + 'hrm_payroll/changePaymentFrequencyType',
            method: 'POST',
            data: { paymentFrequencyType : paymentFrequencyType },
            success: function(data) {
                $('#pay_grade_id option').remove();
                $('#pay_grade_id').append(`<option value="">Select</option>`);
                $.each(data.dataReturn, function(key, item){
                    console.log(item);
                    $('#pay_grade_id').append(`<option value="${item.id}"> ${item.value} </option>`);
                });

                if($('#payment_frequency_type').val() == 'Monthly') {

                    $("#hourly_rate").prop('required',false);
                    $('#hourly_rate').prop('disabled', true);

                    $("#gross_salary").prop('required',true);
                    $("#net_salary").prop('required',true);
                    $("#basic_salary").prop('required',true);
                    $("#basic_percent").prop('required',true);
                    $("#tax_amount").prop('required',true);
                    $("#overtime_rate").prop('required',true);
                }
                else if($('#payment_frequency_type').val() == 'Hourly') {
                    //$('#hourly_rate').rules('add', {required: true});
                    $("#hourly_rate").prop('required',true);
                    $('#hourly_rate').prop("disabled", false);

                    $("#gross_salary").prop('required',false);
                    $("#net_salary").prop('required',false);
                    $("#basic_salary").prop('required',false);
                    $("#basic_percent").prop('required',false);
                    $("#tax_amount").prop('required',false);
                    $("#overtime_rate").prop('required',false);
                }
            }
        });
    });

        $("#basic_percent").on('change', function (e) {
            var basic_percent = floatConverter($("#basic_percent").val());
            var gross_salary = floatConverter($("#gross_salary").val());
            $("#basic_salary").val(percentage(gross_salary, basic_percent));
            totalAmount();
           e.preventDefault();
        });

        function totalAmount() {
            var lineTotalSum = 0;
            var additionSum = 0;
            var deductionSum = 0;
            var lineTotalSumText = '';
            var basicSalary = 0;
            $("#hrmPrlSetupDtls-table tbody tr").each(function () {
            
                if ($(this).find(".addition_deduction").val() == "Addition") {
                    additionSum += floatConverter($(this).find(".actual_amount").val());
                } else if ($(this).find(".addition_deduction").val() == "Deduction}") {
                    deductionSum += floatConverter($(this).find(".actual_amount").val());
                }
                lineTotalSum = additionSum - deductionSum;
            });
            console.log(lineTotalSum, additionSum, deductionSum);
            basicSalary = floatConverter($("#basic_salary").val());
            lineTotalSum+=basicSalary;
            additionSum+=basicSalary;
            lineTotalSumText = additionSum + ' - ' + deductionSum + ' = ' + lineTotalSum;
            $('#gross_salary').val(additionSum);
            $('#net_salary').val(additionSum-deductionSum);
        }

        function floatConverter(value) {
            if (value == null || value == 'null' || value == '' || value == 'NULL') {
                return parseFloat('0');
            } else {
                return parseFloat(value);
            }
        }    
        function percentage(num, per)
        {
            var Perc=(num/100)*per;
            return Perc.toFixed(2);
        }

    $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#empid2").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid2').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

        // $("#gross_sal").on('change', function (e) {
        //     console.log('hello');
        //     var basic_percent = floatConverter($("#basic_percent").val());
        //     var gross_salary = floatConverter($("#gross_salary").val());
        //     $("#basic_salary").val(percentage(gross_salary, basic_percent));
        //     grossChange();
        //     totalAmount();
        //     e.preventDefault();
        // });
        // function grossChange() {
        //     $("#hrmPrlSetupDtls-table  tbody tr").each(function () {
                
        //         if ($(this).find(".allowance_type").val() == "Percent" && $(this).find(".addition_deduction").val() == "Addition") {
        //             var basicPercent = floatConverter($(this).find(".percentageOfBasic").val());
        //             var grossSalary = floatConverter($("#gross_salary").val());
        //             var basicSalary = floatConverter($("#basic_salary").val());
        //             $(this).find(".actual_amount").val(percentage(basicSalary, basicPercent));
        //         }
        //     });
        // }

        $('.js-example-basic-multiple').select2();

});
</script>
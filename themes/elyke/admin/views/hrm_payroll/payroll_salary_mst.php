<?php
    echo "<a href='" . admin_url('hrm_payroll/add_payroll_salary_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_payroll_salary_mst') . "'><i class=\"fa fa-plus\"></i> " . lang('add_payroll_salary_mst') . " </a>";
?>
&nbsp; &nbsp;

<h2>Payroll Salary:</h2>

<div class="table-responsive">
 <table id="table_payroll_salary_mst" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('salary_month');?></th>
          <th><?php echo lang('salary_year');?></th>
          <th><?php echo lang('employee');?></th>    
          <th><?php echo lang('pay_grade_id');?></th>    
          <th><?php echo lang('basic_salary');?></th>    
          <th><?php echo lang('gross_salary');?></th>    
          <th><?php echo lang('net_salary');?></th>    
          <th><?php echo lang('tax_amount');?></th>       
          <th><?php echo lang('payment_type');?></th>       
          <th><?php echo lang('account');?></th>       
          <th><?php echo lang('account_no');?></th>       
          <th><?php echo lang('approved_by');?></th>       
          <th><?php echo lang('voucher_no');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

      oTable = $('#table_payroll_salary_mst').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_payroll/get_payroll_salary_mst') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false} ,{"bSortable": false}]
       });

    });

    </script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_loan_type'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'add_loan_type']; ?>
                <?php echo admin_form_open_multipart("hrm_payslip/add_loan_type", $attrib) ?>                

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="">
                </div>  
                
            </div>
            <div class="form-group row"> 
                <div class="col-md-6">
                    <label for="salary_rules"><?php echo  lang('salary_rules') ?> <span class="text-danger"></span></label>
                    <?php
                        $salary_ruless = array(
                        'Gross' => 'Gross',
                        'Basic' => 'Basic',
                        'NetAmount' => 'NetAmount',
                        );
                    ?>
                    <select name="salary_rules" class="form-control select"  required="required">
                            <option value="">Select</option>
                            <?php foreach($salary_ruless as $key => $value) {?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="loan_percentage"><?php echo  lang('loan_percentage') ?> <span class="text-danger"></span></label> <br>
                    <input class="form-control" placeholder="<?php echo lang('loan_percentage');?>" name="loan_percentage" type="text" value="">
                </div>  
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="interest_rate"><?php echo  lang('interest_rate') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('interest_rate');?>" name="interest_rate" type="text" value="">
                </div>  
                <div class="col-md-6">
                    <label for="is_interest_payable"><?php echo  lang('is_interest_payable') ?> <span class="text-danger"></span></label> <br>
                    <input name="is_interest_payable" type="checkbox" class="form-control">
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="instalments"><?php echo  lang('instalments') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('instalments');?>" name="instalments" type="text" value="">
                </div>  
                <div class="col-md-6">
                    <label for="disburse_method"><?php echo  lang('disburse_method') ?> <span class="text-danger"></span></label>
                    <?php
                        $disburse_methods = array(
                        'Bank' => 'Bank',
                        'Cash' => 'Cash',
                        'Cheque' => 'Cheque',
                        );
                    ?>
                    <select name="disburse_method" class="form-control select"  required="required">
                            <option value="">Select</option>
                            <?php foreach($disburse_methods as $key => $value) {?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
                
            </div>
            <div class="form-group row">
                
                <div class="col-md-6">
                    <label for="repayment_method"><?php echo  lang('repayment_method') ?> <span class="text-danger"></span></label>
                    <?php
                        $repayment_methods = array(
                        'Bank' => 'Bank',
                        'Cash' => 'Cash',
                        'Cheque' => 'Cheque',
                        'InSalary' => 'InSalary',
                        );
                    ?>
                    <select name="repayment_method" class="form-control select"  required="required">
                            <option value="">Select</option>
                            <?php foreach($repayment_methods as $key => $value) {?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                        <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                        <textarea name="remark" class="form-control"></textarea>
                </div>  
            </div>      
            <div class="form-group">
                    <?php echo form_submit('add_loan_type', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_loan_type').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_payslip/add_loan_type",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
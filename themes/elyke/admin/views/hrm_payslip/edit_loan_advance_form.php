<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_loan_advance'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_loan_advance']; ?>
                <?php echo admin_form_open_multipart("hrm_payslip/edit_loan_advance", $attrib) ?>                
                <input type="hidden" name="loan_advance_id" id="loan_advance_id" value="<?= $loan_advance->id ?>">

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <?php
                        $emp_name = $this->employees_model->get_emp_name($loan_advance->employee_id);
                        $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                   ?>
                    <div id="emp_area">
						<input type="text" id="empid" name="employee_id" value="<?= $loan_advance->employee_id. ' - ' . $emp_fullname ?>"  class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="loan_type"><?php echo  lang('loan_type') ?> <span class="text-danger"></span></label>
                    <?php
                        $loan_types = array(
                        'Loan' => 'Loan',
                        'Advance' => 'Advance',
                        );
                    ?>
                    <select name="loan_type" class="form-control select"  required="required">
                            <option value="">Select</option>
                            <?php foreach($loan_types as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $loan_advance->loan_type == $key ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="principle_amount"><?php echo  lang('principle_amount') ?> <span class="text-danger">*</span></label> <br>
                    <input class="form-control" placeholder="<?php echo lang('principle_amount');?>" name="principle_amount" id="hrmLoanAdvanceDtls_principleAmount" type="text" value="<?= $loan_advance->principle_amount ?>">
                </div> 
                <div class="col-md-6">
                    <label for="instalments"><?php echo  lang('instalments') ?> <span class="text-danger">*</span></label> <br>
                    <input class="form-control" placeholder="<?php echo lang('instalments');?>" name="instalments" id="hrmLoanAdvanceDtls_instalmentAmount" type="text" value="<?= $loan_advance->instalments ?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="interest_rate"><?php echo  lang('interest_rate') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('interest_rate');?>" name="interest_rate" type="text" value="<?= $loan_advance->instalments ?>">
                </div> 
                <div class="col-md-6">
                    <label for="interest_amount"><?php echo  lang('interest_amount') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('interest_amount');?>" name="interest_amount" id="hrmLoanAdvanceDtls_interestAmount" type="text" value="<?= $loan_advance->interest_amount ?>">
                </div> 
                
            </div>
            
            
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="net_amount"><?php echo  lang('net_amount') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('net_amount');?>" name="net_amount" type="text" value="<?= $loan_advance->net_amount ?>">
                </div>   
                <div class="col-md-6">
                    
                </div> 
                            
            </div>
            <div class="form-group row"> 
                <div class="col-md-6">
                    <label for="disburse_method"><?php echo  lang('disburse_method') ?> <span class="text-danger"></span></label>
                    <?php
                        $disburse_methods = array(
                        'Bank' => 'Bank',
                        'Cash' => 'Cash',
                        'Cheque' => 'Cheque',
                        );
                    ?>
                    <select name="disburse_method" class="form-control select">
                            <option value="">Select</option>
                            <?php foreach($disburse_methods as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $loan_advance->disburse_method == $key ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>   
                <div class="col-md-6">
                    <label for="loan_proof"><?php echo  lang('loan_proof') ?> <span class="text-danger"></span></label>
                    <input type="text" id="loan_proof" name="loan_proof" placeholder="" class="form-control">
                </div>              
            </div>
            <div class="form-group row"> 
                <div class="col-md-6">
                    <label for="salary_rules"><?php echo  lang('salary_rules') ?> <span class="text-danger"></span></label>
                    <?php
                        $salary_ruless = array(
                        'Gross' => 'Gross',
                        'Basic' => 'Basic',
                        'NetAmount' => 'NetAmount',
                        );
                    ?>
                    <select name="salary_rules" class="form-control select" >
                            <option value="">Select</option>
                            <?php foreach($salary_ruless as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $loan_advance->salary_rules == $key ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="repayment_method"><?php echo  lang('repayment_method') ?> <span class="text-danger"></span></label>
                    <?php
                        $repayment_methods = array(
                        'Bank' => 'Bank',
                        'Cash' => 'Cash',
                        'Cheque' => 'Cheque',
                        'InSalary' => 'InSalary',
                        );
                    ?>
                    <select name="repayment_method" class="form-control select">
                            <option value="">Select</option>
                            <?php foreach($repayment_methods as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $loan_advance->repayment_method == $key ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </div>
         
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="guarantor1_id"><?php echo  lang('guarantor1_id') ?> <span class="text-danger"></span></label>
                    <?php
                    if($loan_advance->guarantor1_id != 0) {
                        $emp_name = $this->employees_model->get_emp_name($loan_advance->guarantor1_id);
                        $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                    } else {
                        $emp_fullname = '';
                    }
                   ?>
                    <input type="text" id="guarantor1_id" name="guarantor1_id" value="<?php $loan_advance->guarantor1_id ? $loan_advance->guarantor1_id . ' - ' . $emp_fullname : '' ?>" class="form-control">
                </div> 
                <div class="col-md-6">
                    <label for="guarantor2_id"><?php echo  lang('guarantor2_id') ?> <span class="text-danger"></span></label>
                    <?php
                        if($loan_advance->guarantor2_id != 0) {
                            $emp_name = $this->employees_model->get_emp_name($loan_advance->guarantor2_id);
                            $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                        } else {
                            $emp_fullname = '';
                        }
                   ?>
                    <input type="text" id="guarantor2_id" name="guarantor2_id" value="<?php $loan_advance->guarantor2_id ? $loan_advance->guarantor2_id . ' - ' . $emp_fullname : '' ?>" class="form-control">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="apply_date"><?php echo  lang('apply_date') ?> <span class="text-danger"></span></label>
                    <input name="transaction_date" type="text" id="hrmLoanAdvanceDtls_transactionDate" class="date form-control" value="<?= date('d-m-Y', strtotime($loan_advance->transaction_date)) ?>" placeholder="dd-mm-yyyy" autocomplete="off">
                </div>  
                <div class="col-md-6">
                    <label for="first_settlement_date"><?php echo  lang('first_settlement_date') ?> <span class="text-danger"></span></label>
                    <input name="first_settlement_date" type="text" id="hrmLoanAdvanceDtls_settlementDate" class="date form-control" value="<?= date('d-m-Y', strtotime($loan_advance->first_settlement_date)) ?>" placeholder="dd-mm-yyyy" autocomplete="off">
                </div>  
            </div>

            <div class="form-group row">
                
                <div class="col-md-6">
                    <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                    <input type="text" id="remark" name="remark" value="<?= $loan_advance->remark ?>" placeholder="" class="form-control">    
                </div> 
            </div>
            <div>
            <button type="button" class="btn btn-primary"
                                        data-toggle="tooltip"
                                        id="add-hrmLoanAdvanceDtls-btn"><i
                                        class="fa fa-plus"></i> Add
            </button>
            </div>   
            <br>  
            <div>                       
            
            <div class="form-group">
                    <?php echo form_submit('edit_loan_advance', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
    var rowNumber = 1;
$(document).ready(function(){

$('#edit_loan_advance').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_payslip/edit_loan_advance",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    // $('#myModal').modal('hide');
				    // location.reload();
             }
               
      });  
  });

  

  $( "#empid").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#guarantor1_id").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#guarantor1_id').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#guarantor2_id").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#guarantor2_id').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

        $(document).on('click', '.addNewRow', function(){
            const loan_proof = $('#loan_proof').val();
            var loanProof = $('#loan_proof').select2('data');
            console.log(loanProof);
            addAppend(loan_proof);
        });
        $(document).on("click", ".deleteRow", function(){
            $(this).closest('tr').remove();
        });

        localStorage.setItem('serial', '0');

        $("#add-hrmLoanAdvanceDtls-btn").on("click", function (e) {
            // var code = $('#hrmLoanAdvanceDtls_code').val();
            var transactionDate = $('#hrmLoanAdvanceDtls_transactionDate').val();
            var settlementDate = $('#hrmLoanAdvanceDtls_settlementDate').val();
            var principleAmount = $('#hrmLoanAdvanceDtls_principleAmount').val();
            var interestAmount = $('#hrmLoanAdvanceDtls_interestAmount').val();
            var instalmentAmount = $('#hrmLoanAdvanceDtls_instalmentAmount').val();
            var paidAmount = $('#hrmLoanAdvanceDtls_paidAmount').val();
            var remark = $('#hrmLoanAdvanceDtls_remark').val();
            if (transactionDate == '' || settlementDate == '' || principleAmount == '' || interestAmount == '' || instalmentAmount == '') {
                alert("Please select all mandatory fields");
                return false
            }
            if (checkDuplicate(transactionDate) == true) {
                alert(transactionDate + " Data already exist");
                return false;
            }
            addDetailInTable(rowNumber++, '',  transactionDate, settlementDate, principleAmount, interestAmount, instalmentAmount, paidAmount, remark);
            onchangebind();
            //resetDetailForm();
            // totalAmount();
            //e.preventDefault();
        });
});

function addDetailInTable(rowNumber, id,  transactionDate, settlementDate, principleAmount, interestAmount, instalmentAmount, paidAmount, paidStatus, remark) {
        var idCol = '<input type="hidden" class="id" name="loanAdvanceDtlsId[]" value="' + id + '">';
        //var codeCol = '<input type="text"  name="hrmLoanAdvanceDtlsSet[' + rowNumber + '].code" class="form-control input-sm code" value="' + nullCheck(code) + '">';
        var transactionDateCol = '<input type="text"  name="transactionDate[]" class="form-control input-sm transactionDate" value="' + transactionDate + '">';
        var settlementDateCol = '<input type="text"  name="settlementDate[]" class="form-control input-sm settlementDate" value="' + settlementDate + '">';
        var principleAmountCol = '<input type="text"  name="principleAmount[]" class="form-control input-sm principleAmount" value="' + nullCheck(principleAmount) + '">';
        var interestAmountCol = '<input type="text"  name="interestAmount[]" class="form-control input-sm interestAmount" value="' + nullCheck(interestAmount) + '">';
        var instalmentAmountCol = '<input type="text"  name="instalmentAmount[]" class="form-control input-sm instalmentAmount" value="' + nullCheck(instalmentAmount) + '">';
        var paidAmountCol = '<input type="text"  name="paidAmount[]" class="form-control input-sm paidAmount" value="' + nullCheck(paidAmount) + '">';
        var actionCol = '<div class="btn-group"><button class="btn btn-white hrmLoanAdvanceDtlsDeleteDtlBtn" ><i class="fa fa-trash text-danger"></i></button>';
        $('#hrmLoanAdvanceDtls-table > tbody').append('<tr><td>' + rowNumber + idCol + '</td><td>' + transactionDateCol + '</td><td>' + settlementDateCol + '</td><td>' + interestAmountCol + '</td><td>' + principleAmountCol + '</td><td>' + instalmentAmountCol + '</td>' + '<td>' + paidAmountCol + '</td><td>' + actionCol + '</td></tr>');
        //    $('[name*="hrmLoanAdvanceDtls[' + rowNumber + '].quantityReg"]').rules('add', {required: true});
    }

    function onchangebind() {

        $("button.hrmLoanAdvanceDtlsDeleteDtlBtn").on("click", function (e) {
            var selectRow = $(this).parents('tr');
            var rowId = selectRow.find(".id").val();
            if (!(rowId == '')) {
                var idCol = '<input type="hidden" class="id" name="hrmLoanAdvanceDtlsDeleteSet[' + rowNumber + '].id" value="' + rowId + '">';
                $('#hrmLoanAdvanceDtls-table > tbody').append('<tr style="visibility:collapse"><td>' + (rowNumber++) + idCol + '</td></tr>');
            }
            selectRow.remove();
            // totalAmount();
            e.preventDefault();
        });
    }

    function nullCheck(checkvalue) {
        checkvalue = (checkvalue == null || checkvalue == "null") ? '' : checkvalue
        return checkvalue;
    }

    function checkDuplicate(newItemId) {
        var existingItem;
        var isError = false;
        $("#hrmLoanAdvanceDtls-table tbody tr").each(function () {
            existingItem = $(this).find(".transactionDate").val();
            if (newItemId == existingItem) {
                isError = true;
            }
        });
        return isError;
    }


    function addAppend(loan_proof){

      let serial = parseInt(localStorage.getItem('serial'));
      serial++;
      localStorage.setItem('serial', serial);

      const loan_proof_arr = loan_proof.split('|');  
      const loan_proof_id = loan_proof_arr[0];
      const loan_proof_text = loan_proof_arr[1];

      var newHtml = 
      '<tr class="itemRowlist">'+

        '<td>'+ serial + '</td>' + 

        '<td>'+ loan_proof_text +'<input type="hidden" name="loan_proof_id[]" value="'+loan_proof_id+'"></td>' + 
        
        '<td><input type="file" name="document_path_'+serial+'" class="form-control document_path"></td>'+

        '<td><input type="text" name="remark_doc[]" class="form-control remark"></td>'+

        '<td><span class="changeAction"><a class="deleteRow" attr=""><i class="fa fa-trash" style="font-size: 25px;padding-right: 5px;color: #d43c3c;"></i></a>'+

      '</tr>';

      $('#AddItem').append(newHtml);

     }
</script>
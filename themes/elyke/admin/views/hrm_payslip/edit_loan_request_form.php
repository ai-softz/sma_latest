<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_loan_request'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_loan_request']; ?>
                <?php echo admin_form_open_multipart("hrm_payslip/edit_loan_request", $attrib) ?>                
<input type="hidden" name="loan_request_id" id="loan_request_id" value="<?= $loan_request->id ?>">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    
                    <?php
                        $emp_name = $this->employees_model->get_emp_name($loan_request->employee_id);
                        $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                   ?>
                    <div id="emp_area">
						<input type="text" id="empid" name="employee_id" value="<?= $loan_request->employee_id. ' - ' . $emp_fullname ?>"  class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="loan_type"><?php echo  lang('loan_type') ?> <span class="text-danger"></span></label>
                    <?php
                        $loan_types = array(
                        'Loan' => 'Loan',
                        'Advance' => 'Advance',
                        );
                    ?>
                    <select name="loan_type" class="form-control select">
                            <option value="">Select</option>
                            <?php foreach($loan_types as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $key == $loan_request->loan_type ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="apply_date"><?php echo  lang('apply_date') ?> <span class="text-danger"></span></label>
                    <input name="transaction_date" type="text" id="transaction_date" class="date form-control" value="<?= date('d-m-Y', strtotime($loan_request->transaction_date)) ?>" placeholder="dd-mm-yyyy" autocomplete="off">
                </div>  
                <div class="col-md-6">
                    <label for="first_settlement_date"><?php echo  lang('first_settlement_date') ?> <span class="text-danger"></span></label>
                    <input name="first_settlement_date" type="text" id="first_settlement_date" class="date form-control" value="<?= date('d-m-Y', strtotime($loan_request->first_settlement_date)) ?>" placeholder="dd-mm-yyyy" autocomplete="off">
                </div>  
            </div>
            <div class="form-group row"> 
                <div class="col-md-6">
                    <label for="salary_rules"><?php echo  lang('salary_rules') ?> <span class="text-danger"></span></label>
                    <?php
                        $salary_ruless = array(
                        'Gross' => 'Gross',
                        'Basic' => 'Basic',
                        'NetAmount' => 'NetAmount',
                        );
                    ?>
                    <select name="salary_rules" class="form-control select">
                            <option value="">Select</option>
                            <?php foreach($salary_ruless as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $key == $loan_request->salary_rules ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="repayment_method"><?php echo  lang('repayment_method') ?> <span class="text-danger"></span></label>
                    <?php
                        $repayment_methods = array(
                        'Bank' => 'Bank',
                        'Cash' => 'Cash',
                        'Cheque' => 'Cheque',
                        'InSalary' => 'InSalary',
                        );
                    ?>
                    <select name="repayment_method" class="form-control select"  required="required">
                            <option value="">Select</option>
                            <?php foreach($repayment_methods as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $key == $loan_request->repayment_method ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </div>
         
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="disburse_method"><?php echo  lang('disburse_method') ?> <span class="text-danger"></span></label>
                    <?php
                        $disburse_methods = array(
                        'Bank' => 'Bank',
                        'Cash' => 'Cash',
                        'Cheque' => 'Cheque',
                        );
                    ?>
                    <select name="disburse_method" class="form-control select">
                            <option value="">Select</option>
                            <?php foreach($disburse_methods as $key => $value) {?>
                            <option value="<?php echo $key; ?>" <?= $key == $loan_request->disburse_method ? 'Selected' : '' ?>><?php echo $value; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="maximum_amount"><?php echo  lang('maximum_amount') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('maximum_amount');?>" name="maximum_amount" type="text" value="">
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="interest_rate"><?php echo  lang('interest_rate') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('interest_rate');?>" name="interest_rate" type="text" value="<?= $loan_request->interest_rate ?>">
                </div> 
                <div class="col-md-6">
                    <label for="principle_amount"><?php echo  lang('principle_amount') ?> <span class="text-danger"></span></label> <br>
                    <input class="form-control" placeholder="<?php echo lang('principle_amount');?>" name="principle_amount" type="text" value="<?= $loan_request->principle_amount ?>">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="interest_amount"><?php echo  lang('interest_amount') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('interest_amount');?>" name="interest_amount" type="text" value="<?= $loan_request->interest_amount ?>">
                </div> 
                <div class="col-md-6">
                    <label for="instalments"><?php echo  lang('instalments') ?> <span class="text-danger"></span></label> <br>
                    <input class="form-control" placeholder="<?php echo lang('instalments');?>" name="instalments" type="text" value="<?= $loan_request->interest_amount ?>">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="guarantor1_id"><?php echo  lang('guarantor1_id') ?> <span class="text-danger"></span></label>
                    <?php
                    if($loan_request->guarantor1_id != 0) {
                        $emp_name = $this->employees_model->get_emp_name($loan_request->guarantor1_id);
                        $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                    } else {
                        $emp_fullname = '';
                    }
                   ?>
                    <input type="text" id="guarantor1_id" name="guarantor1_id" value="<?php $loan_request->guarantor1_id ? $loan_request->guarantor1_id . ' - ' . $emp_fullname : '' ?>" class="form-control">
                </div> 
                <div class="col-md-6">
                    <label for="guarantor2_id"><?php echo  lang('guarantor2_id') ?> <span class="text-danger"></span></label>
                    <?php
                        if($loan_request->guarantor2_id != 0) {
                            $emp_name = $this->employees_model->get_emp_name($loan_request->guarantor2_id);
                            $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
                        } else {
                            $emp_fullname = '';
                        }
                   ?>
                    <input type="text" id="guarantor2_id" name="guarantor2_id" value="<?php $loan_request->guarantor2_id ? $loan_request->guarantor2_id . ' - ' . $emp_fullname : '' ?>" class="form-control">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="net_amount"><?php echo  lang('net_amount') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('net_amount');?>" name="net_amount" type="text" value="<?= $loan_request->net_amount; ?>">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                        <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                        <textarea name="remark" class="form-control"><?= $loan_request->remark; ?></textarea>
                </div>  
            </div>     
            <h2> Loan Proof </h2>
            <?php if(!empty($loan_proof_docs)) { ?>
            <table border="0" id="loanProofDocu" class="table table-stripped table-bordered">
                <tbody id="loanProofBody">
                        <tr style="text-align: center;">
                            <td>SL</td>
                            <td>Loan Proof</td>
                            <td>Document Path</td>
                            <td>Remark</td>
                            <td style="padding:5px">Delete?</td>
                        </tr>
                <?php 
                // print_r($loan_proof_docs); 
                        $i = 1;
                        foreach($loan_proof_docs as $value) {  
                            $loan_proof = $this->db->get_where('hrm_loan_proof', ['id' => $value->loan_proof_id])->row();    
                        ?>
                        <tr style="text-align: center;">
                            <td> <?php echo $i; ?> </td>
                            <td> <?php echo $loan_proof->caption; ?> </td>
                            <td> <?php echo '<a href="'.base_url('assets/uploads/loan_proof/').$value->document_path.'">'. $value->document_path .'</a>'; ?> </td>
                            <td> <?php echo $value->remark; ?> </td>
                            <td>
                                <span  class="delete_proof_doc" data-id="<?= $value->id ?>">
                                <a  ><i class="fa fa-times" style="font-size: 25px;padding-right: 5px;color: #d43c3c;"></i></a> </span>
                            </td>
                        </tr>
                        <?php
                            $i++; 
                        } ?>
                        </tbody>
            </table>
            <?php } ?>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="loan_proof"><?php echo  lang('add_loan_proof') ?> <span class="text-danger"></span></label>
                    <select name="loan_proof" id="loan_proof" class="form-control select">
                            <option value="">Select</option>
                            <?php foreach($loan_proof as $value) {?>
                            <option value="<?php echo $value->id . '|' . $value->caption; ?>"><?php echo $value->caption; ?></option>
                            <?php } ?>
                    </select>
                </div> 
                <div class="col-md-6" style="margin-top: 30px;">
                
                    <button type="button" class="addNewRow btn btn-success">
                              <i class="fa fa-plus-square" style=""></i> Add
                            </button>
                </div>
            </div>                   
            <div>
                
            <table id="AddItem" border="0" class="table table-stripped table-bordered">
                    <tbody>
                        <tr style="text-align: center;">
                            <td>SL</td>
                            <td>Loan Proof</td>
                            <td>Document Path</td>
                            <td>Remark</td>
                            <td style="padding:5px">Delete?</td>
                        </tr>
                        
   
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                    <?php echo form_submit('add_loan_request', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#edit_loan_request').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_payslip/edit_loan_request",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

  $(document).on("click", ".delete_proof_doc", function() {

      var loanProofId = $(this).data('id');
      const loanRequestId = $('#loan_request_id').val();
      console.log(loanProofId);

      $.ajax({
            url: site.base_url + "hrm_payslip/delete_proof_doc",
            method: "POST",
            dataType: "text",
            data: {loanProofId: loanProofId, loanRequestId: loanRequestId },
            success: function(data) {
                const dataObj = JSON.parse(data)
                const loanProofDocs = dataObj.loanProofDocs;
                let html = ``;
                if(loanProofDocs.length != 0) {
                    
                    html += `
                        <tbody id="loanProofBody">
                        <tr style="text-align: center;">
                            <td>SL</td>
                            <td>Loan Proof</td>
                            <td>Document Path</td>
                            <td>Remark</td>
                            <td style="padding:5px">Delete?</td>
                        </tr>`;
                $.each(loanProofDocs, function(index, item){
                    html += `
                    
                        <tr style="text-align: center;">
                            <td> ${index+1} </td>
                            <td> ${item.loan_advance_id} </td>
                            <td> ${item.document_path} </td>
                            <td> ${item.remark} </td>
                            <td>
                                <span  class="delete_proof_doc" data-id="${item.id}">
                                <a  ><i class="fa fa-times" style="font-size: 25px;padding-right: 5px;color: #d43c3c;"></i></a> </span>
                            </td>
                        </tr>
                    </tbody>
                    `; 
                });
                
                }
                $('#loanProofBody').remove();
                $('#loanProofDocu').html(html);
            }
      });

  });

  $( "#empid").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#guarantor1_id").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#guarantor1_id').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#guarantor2_id").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#guarantor2_id').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	

        $(document).on('click', '.addNewRow', function(){
            const loan_proof = $('#loan_proof').val();
            var loanProof = $('#loan_proof').select2('data');
            console.log(loanProof);
            addAppend(loan_proof);
        });
        $(document).on("click", ".deleteRow", function(){
            $(this).closest('tr').remove();
        });

        localStorage.setItem('serial', '0');
});


    function addAppend(loan_proof){

      let serial = parseInt(localStorage.getItem('serial'));
      serial++;
      localStorage.setItem('serial', serial);

      const loan_proof_arr = loan_proof.split('|');  
      const loan_proof_id = loan_proof_arr[0];
      const loan_proof_text = loan_proof_arr[1];

      var newHtml = 
      '<tr class="itemRowlist">'+

        '<td>'+ serial + '</td>' + 

        '<td>'+ loan_proof_text +'<input type="hidden" name="loan_proof_id[]" value="'+loan_proof_id+'"></td>' + 
        
        '<td><input type="file" name="document_path_'+serial+'" class="form-control document_path"></td>'+

        '<td><input type="text" name="remark_doc[]" class="form-control remark"></td>'+

        '<td><span class="changeAction"><a class="deleteRow" attr=""><i class="fa fa-trash" style="font-size: 25px;padding-right: 5px;color: #d43c3c;"></i></a></td>'+

      '</tr>';

      $('#AddItem').append(newHtml);

     }
</script>
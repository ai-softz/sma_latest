<?php
    echo "<a href='" . admin_url('hrm_payslip/add_loan_request_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_payroll_loan_request') . "'><i class=\"fa fa-plus\"></i> Add Loan Request</a>";
?>
&nbsp;&nbsp;

<h2>Loan Request:</h2>

<div class="table-responsive">
 <table id="table_payroll_loan_request" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('guarantor_1');?></th>     
          <th><?php echo lang('guarantor_2');?></th>     
          <th><?php echo lang('transaction_date');?></th>     
          <th><?php echo lang('first_settlement_date');?></th>     
          <th><?php echo lang('salary_rules');?></th>     
          <th><?php echo lang('loan_type');?></th>     
          <th><?php echo lang('repayment_method');?></th>      
          <th><?php echo lang('disburse_method');?></th>      
          <th><?php echo lang('instalments');?></th>      
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

      oTable = $('#table_payroll_loan_request').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_payslip/get_payroll_loan_request/') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false},{"bSortable": false}, {"bSortable": false}, {"bSortable": false},{"bSortable": false}, {"bSortable": false}, {"bSortable": false} ]
       });

    });

    </script>
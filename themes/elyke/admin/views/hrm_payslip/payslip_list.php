<div class="error" style="color: #ff0000"> </div>
<div class="search_area">
            <div class="row">
                <div class="col-md-3">
            <input type="text" id="departmentId" name="department" placeholder="Type department name..." class="form-control" autocomplete="off">
            </div> 
                <div class="col-md-3">
                    <input type="text" id="empid" placeholder="Type employee name..." class="form-control">
                </div>
                <div class="col-md-3">
             <!-- <input name="" type="text" id="startDate" class="date form-control" value="" placeholder="dd-mm-yyyy" autocomplete="off"> -->
                       <input type="month" id="startDate" name="" class="form-control" value="<?= date('Y-m'); ?>" autocomplete="off">
        </div>
                
      </div>
            <br>
            <div class="row">
        <div class="col-md-3">
          <a id="btnSearch" type="submit" class="btn btn-primary">Search</a>
        </div>
                <div class="col-md-3">
                <a id="btnPdf" type="submit" class="btn btn-primary">Download Pdf</a>
                    <!-- <a href="<?= admin_url('hrm_payslip/payslip_pdf') ?>" target="_blank" class="btn btn-primary" style="margin-bottom:5px; float: right"> <i class="fa fa-download"> </i>  Download Pdf</a>  -->
        </div>
      </div>
    </div>

<h2>Payslip:</h2>

<div class="table-responsive">
 <table id="table_payslip" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>
          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('pay_grade');?></th>
          <th><?php echo lang('salary_year');?></th>
          <th><?php echo lang('salary_month');?></th>
          <th><?php echo lang('basic_salary');?></th>          
          <th><?php echo lang('gross_salary');?></th>          
          <th><?php echo lang('net_salary');?></th>          
          <th><?php echo lang('draft_status');?></th>          
          <th><?php echo lang('approved_by');?></th>          
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {

    fill_datatable();
    function fill_datatable(empid, departmentId, startDate, pdf) {
      oTable = $('#table_payslip').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_payslip/getPayslip/') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
              
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push(
                {
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               },
                    { "name": "empid", "value": empid },
                    { "name": "departmentId", "value": departmentId },
                    { "name": "startDate", "value": startDate },
                    { "name": "pdf", "value": pdf },
               
               );
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    }

       $("#btnSearch").on("click", function(e){
         e.preventDefault();
         
         var empid = $("#empid").val().split('-');
         var departmentId = $("#departmentId").val().split('-');
         var startDate = $("#startDate").val();
         var pdf = $("#pdf").val();
         
         fill_datatable(empid[0], departmentId[0], startDate, pdf);
      }); 

      $("#btnPdf").on("click", function(e){
         e.preventDefault();
         
         var empid = $("#empid").val().split('-');
         var departmentId = $("#departmentId").val().split('-');
         var startDate = $("#startDate").val();

         if( (empid == '' && departmentId == '') || startDate == '') {
             $(".error").html('<p>Please fill date or employee or department fields.</p>')
             return false;
         } else {
            window.location.replace(site.base_url + "hrm_payslip/payslip_pdf/?empid=" + empid[0] + "&departmentId=" + departmentId[0] + "&startDate=" + startDate);
         }
        //  $.ajax({
        //     url: site.base_url + "hrm_payslip/payslip_pdf",
        //     method: "POST",
        //     data: {
        //         empid: empid,
        //         departmentId: departmentId,
        //         startDate: startDate
        //     },
        //     success: function() {

        //     }
        //  });
      });  
    
      
    $( "#empid").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        }); 

        $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        }); 

    });

    </script>
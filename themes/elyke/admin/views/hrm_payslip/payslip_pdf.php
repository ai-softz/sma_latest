<?php defined('BASEPATH') or exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="<?= $assets ?>styles/pdf/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets ?>styles/pdf/pdf.css" rel="stylesheet">

    <title> Payslip Pdf </title>
</head>

<body>
    
<style>
#payslip_pdf table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 10px;
  margin: 10px;
}
#company_name {
  float: left;
  width: 50%;
}
#company_name h2{
  color: #0000ff;
}
#payslip_date {
  float: right;
  width: 50%;
  text-align: right;
}
#payslip_date h2 {
  color: #ff0000; 
}
</style>

<?php foreach($data as $emp_id => $value) { ?>

<?php 
if(!empty($data[$emp_id])) {

if(isset($data[$emp_id][0]->payment_frequency_type)) {

  if($data[$emp_id][0]->payment_frequency_type == 'Hourly') {

    $emp_name = $this->employees_model->get_emp_name($emp_id);
    $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
    $dept_name = $this->hrm_payroll_model->get_dept_name($data[$emp_id][0]->department_id);
  ?>
         <div class="header">
            <div id="company_name">
                <h2> Company Name: <?= $site_name ?></h2>
            </div>
            <div id="payslip_date">
                <h2> Pay Slip <?php echo $month . '-' . $year ?> </h2>
            </div>
        </div>
      <p> <b>Created date: <?= date('d-m-Y') ?><b> </p>
      <br> <br>
      <p> Employee Name: <b><?php echo $emp_fullname ?></b> </p>
      <p> Department: <b><?php echo $dept_name->department_name ?></b> </p>
      <p> Month & Year: <b><?php echo $month . '-' . $year ?></b> </p>

    <table id="payslip_pdf" style="width:100%; margin-top: 20px">
    <tr>
        <th>Hourly Rate</th>
        <th>Gross Salary</th> 
        <th>Net Salary</th>
      </tr>
    <tr>
      <td> <?php echo $data[$emp_id][0]->hourly_rate ?> </td>
      <td> <?php echo number_format($data[$emp_id][0]->gross_salary, 2) ?> </td>
      <td> <?php echo number_format($data[$emp_id][0]->net_salary, 2) ?> </td>
    </tr>

  </table>
  <hr>
  <?php } 
} else { ?>
<?php
    $emp_name = $this->employees_model->get_emp_name($emp_id);
    $emp_fullname = $emp_name->first_name. ' '.$emp_name->last_name;
?>
        <div class="header">
            <div id="company_name">
                <h2> Company Name: <?= $site_name ?></h2>
            </div>
            <div id="payslip_date">
                <h2> Pay Slip <?php echo $month . '-' . $year ?> </h2>
            </div>
        </div>
<p> <b>Created date: <?= date('d-m-Y') ?><b> </p>
<br> <br>
<p> Employee Name: <b><?php echo $emp_fullname ?></b> </p>
<p> Department: <b><?php echo $value[0]->department_name ?></b> </p>
<p> Month & Year: <b><?php echo $month . '-' . $year ?></b> </p>

<table id="payslip_pdf" style="width:100%; margin-top: 20px">
  <tr>
    <th>head_name</th>
    <th>addition_deduction</th> 
    <th>Amount</th>
  </tr>
<?php 
arsort($value);
$total_actual_amt = 0;
foreach($value as $row) { ?>
  <tr>
    <td> <?php echo $row->head_name ?> </td>
    <td> <?php echo $row->addition_deduction ?> </td>
    <td> <?php echo number_format($row->actual_amt, 2) ?> </td>
  </tr>
<?php 
  if($row->addition_deduction == 'Addition') {
    $total_actual_amt += $row->actual_amt;
  } else {
    $total_actual_amt -= $row->actual_amt;
  }
  
} 
?>
<tr>
<th colspan="2">  Total </th>

<th> <?php echo number_format($total_actual_amt, 2) ?> </th>
</tr>
</table>
<hr>
<?php 
  } 
  echo '<pagebreak />';
}

}
?>
</body>
</html>
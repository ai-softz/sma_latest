<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_roster'); ?></h4>
    </div>

    <div class="modal-body">
	<p class="error_msg text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_roster']; ?>
		     <?php echo admin_form_open_multipart("hrm_roster/add_roster", $attrib) ?>
		     
           <div class="form-group row">
		   		
                <div class="col-md-6">
					<label for="period_mst_id"><?php echo  lang('period_master') ?> <span class="text-danger"></span></label>
					<select name="period_mst_id" class="form-control select" id="period_mst_id">
						<option value=""><?php echo 'Select';?></option>
						<?php foreach($period_masters as $row) { ?>
							<option value="<?php echo $row->id; ?>"><?php echo $row->name;?></option>
						<?php } ?>
					</select>
		        </div>
				<div class="col-md-6">
				   <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
                    <select name="department" class="form-control select" id="department">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_departments as $row) { ?>
                        <option value="<?php echo $row->department_id; ?>"><?php echo $row->department_name;?></option>
                    <?php } ?>
                    </select>
		      	</div> 
		   </div>
		   
           <div class="form-group row">
		   		<div class="col-md-6">
				   <label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <div id="emp_area">
                        <select name="employee" class="form-control select" id="employee_id">
						<option value=""><?php echo 'Select';?></option>
                        </select>
                    </div>
		      	</div> 
                
		   </div>
		   
           
           <div class="form-group row">
		      <div class="col-md-12">
				<label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
				<textarea name="remark" class="form-control"> </textarea>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_shift_master', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_roster').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_roster/add_roster",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                 if(data.error == 0) {
				    $('#myModal').modal('hide');
				    location.reload();
                 } 
                 else {
                     $('.error_msg').text(data.msg);
                 }
             }
               
      });  
  });

  $(document).on('change', '#department', function(){
        var deptId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_roster/get_dept_employees',
            method: 'POST',
            type: 'text',
            data: { dept_id: deptId },
            success: function(data) {
                $('#employee_id').remove();
                $('#emp_area').html(data);
            }
        });
  });

});
</script>
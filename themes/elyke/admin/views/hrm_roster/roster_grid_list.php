
<?php
    echo "<div class=\"\"><a href='" . admin_url('hrm_roster_grid/add_roster_grid_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_roster_grid') . "'><i class=\"fa fa-plus\"></i> Add Roster</a></div>";
?>
<br>
<div class="search_area">
<?php echo admin_form_open_multipart("", ['role' => 'form', 'id' => 'get_roster_details']); ?>
            <div class="row">
                
                <div class="col-md-3">
                    <input type="text" id="departmentId" name="department" placeholder="Type department name..." class="form-control">
                </div>
                <div class="col-md-3">
                    <input type="text" id="empid" name="empid" placeholder="Type employee name..." class="form-control">
                </div>
                
			</div>
            <br>
            <div class="row">
                <div class="col-md-3">
					   <input type="text" id="startDate" name="startDate" class="date form-control" value="" placeholder="Start Date" autocomplete="off">
				</div>
                <div class="col-md-3">
                    <input type="text" id="endDate" name="endDate" class="date form-control" value="" placeholder="End Date" autocomplete="off">	
				</div>
				<div class="col-md-3">
					<a id="renderRoster" type="submit" class="btn btn-primary">Search</a>
				</div>
			</div>
        <?php echo form_close(); ?>
    </div>
<br>

<div id="rosterGrid" style="overflow-x:auto; width: 95%;">
</div>


<script>
   $(document).ready(function () {

        $.ajax({
            type: "POST",
            url: site.base_url + "hrm_roster_grid/get_roster_grid_default",
            success: function(data) {
                $("#rosterGrid").html(data);
            }
        });

      $("#renderRoster").on("click", function(){

        $.ajax({
            type: "POST",
            url: site.base_url + "hrm_roster_grid/get_roster_grid",
            data: $('#get_roster_details').serialize(),
            success: function(data) {
                $("#rosterGrid").html(data);
            }
        });
      });


      $( "#departmentId").autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_department_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#departmentId').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });	
        $( "#empid").autocomplete({
            source: function( request, response ) {

                // Fetch data
                $.ajax({
                    url: site.base_url + "hrm_roster_details/get_employee_select",
                    type: 'post',
                    dataType: "json",
                    data: {
                        name: request.term,
                        _token: "7XJ9pLvj44OyrEfThIkuRV6MowMvuuXcrHhoaVtf"
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#empid').val(ui.item.value + ' - ' + ui.item.label);                
                return false;
            }
        });
        

    });

   

</script>
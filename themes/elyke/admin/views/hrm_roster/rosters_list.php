
<?php
    echo "<a href='" . admin_url('hrm_roster/add_roster_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_roster') . "'><i class=\"fa fa-plus\"></i>" . lang('add_roster') . " </a>";
?>

<?php
    echo "<a href='" . admin_url('hrm_roster/roster_process_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_roster') . "'><i class=\"fa fa-plus\"></i>" . lang('roster_process') . "  </a>";
?>

<?php
    echo " <a href='" . admin_url('cron/hr_add_roster_default') . "' class='tip btn btn-primary' title='" . ('Add rosters for all employees.') . "'> <i class=\"fa fa-plus\"></i>" . ('Add Roster Default') . "  </a>";
?>
<?php
    echo " <a href='" . admin_url('cron/hr_auto_roster_process') . "' class='tip btn btn-primary' title='" . ('Auto rostering for all employees last 30 days.') . "'> <i class=\"fa fa-plus\"></i>" . ('Auto Roster Process') . "  </a>";
?>
<br>
<div class="table-responsive">
 <table id="table_hrm_roster" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('time_period');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('department');?></th>
          <th><?php echo lang('caption');?></th>
          <th><?php echo lang('active_status');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {
    
      oTable = $('#table_hrm_roster').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_roster/getRosters') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

   

</script>
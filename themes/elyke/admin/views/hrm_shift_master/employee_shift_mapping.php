
<?php
    echo "<div class=\"\"><a href='" . admin_url('Hrm_shift_master/shift_mapping_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_shift_mapping') . "'><i class=\"fa fa-plus\"></i> " . lang('add_shift_mapping') . " </a></div>";
    ?>
<br><br>
<div class="table-responsive">
 <table id="table_shift_master" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('the_number_sign');?></th>
          <th><?php echo lang('employee');?></th>
          <th><?php echo lang('department');?></th>
          <th><?php echo lang('shift');?></th>
          <th><?php echo lang('shift_name');?></th>
          <th><?php echo lang('punch_required');?></th>
          <th><?php echo lang('start_punch');?></th>
          <th><?php echo lang('end_punch');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<script>
   $(document).ready(function () {
    
      oTable = $('#table_shift_master').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('hrm_shift_master/getEmployeeShiftMap') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
    });

   

</script>
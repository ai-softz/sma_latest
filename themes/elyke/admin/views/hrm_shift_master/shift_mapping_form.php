<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_emp_shift_map'); ?></h4>
    </div>

    <div class="modal-body">
	<p class="error_msg text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_emp_shift_map']; ?>
		     <?php echo admin_form_open_multipart("hrm_shift_master/add_emp_shift_map", $attrib) ?>
		     <?php
// print_r($all_employees); echo "<br>"; print_r($all_departments);
             ?>
           <div class="form-group row">
                <div class="col-md-6">
                    <label for="department"><?php echo  lang('department') ?> <span class="text-danger"></span></label>
                    <select name="department" class="form-control select" id="department">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($all_departments as $row) { ?>
                        <option value="<?php echo $row->department_id; ?>"><?php echo $row->department_name;?></option>
                    <?php } ?>
                    </select>
		        </div> 
		   		<div class="col-md-6">
					<label for="employee"><?php echo  lang('employee') ?> <span class="text-danger"></span></label>
                    <div id="emp_area">
                        <select name="employee" class="form-control select" id="employee_id">
                        <option value=""><?php echo 'Select';?></option>
                        </select>
                    </div>
		      	</div> 
                
		   </div>
		   <div class="form-group row">
                <div class="col-md-6">
					<label for="shift_mst_id"><?php echo  lang('shift_master') ?> <span class="text-danger"></span></label>
					<select name="shift_mst_id" class="form-control select" id="shift_mst_id">
                    <option value=""><?php echo 'Select';?></option>
                    <?php foreach($shift_masters as $row) { ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->name;?></option>
                    <?php } ?>
                </select>
		      	</div>
                <div class="col-md-6">
                    <label for="card_number"><?php echo  lang('card_number') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('card_number');?>" name="card_number" id="card_no" type="text" value="">
                </div> 
		    
		   </div>
           <br>
           <div class="form-group row">   
            <div class="col-md-6">          
                <table class="table table-striped m-md-b-0">
                    <tr>    
                        <th><?php echo lang('day');?></th>
                        <th><?php echo lang('start_time');?></th>
                        <th><?php echo lang('end_time');?></th>
                        <th><?php echo lang('holiday');?></th>
                    </tr>
                    <tr>
                        <td><?php echo lang('friday') ?></td>
                        <td>
                            <input class="form-control" name="fri_start_time" id="fri_start_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input class="form-control" name="fri_end_time" id="fri_end_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="fri_off" id="fri_off">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo lang('saturday')  ?></td>
                        <td>
                            <input class="form-control" name="sat_start_time" id="sat_start_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input class="form-control" name="sat_end_time" id="sat_end_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input type="checkbox" name="sat_off" id="sat_off">
                        </td>
                    </tr>	  
                    <tr>
                        <td><?php echo lang('sunday')  ?></td>
                        <td>
                            <input class="form-control" name="sun_start_time" id="sun_start_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input class="form-control" name="sun_end_time" id="sun_end_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input type="checkbox" name="sun_off" id="sun_off">
                        </td>
                    </tr>	
                    <tr>
                        <td><?php echo lang('monday')  ?></td>
                        <td>
                            <input class="form-control" name="mon_start_time" id="mon_start_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input class="form-control" name="mon_end_time" id="mon_end_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input type="checkbox" name="mon_off" id="mon_off">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo lang('tuesday') ?></td>
                        <td>
                            <input class="form-control" name="tues_start_time" id="tues_start_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input class="form-control" name="tues_end_time" id="tues_end_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input type="checkbox" name="tues_off" id="tues_off">
                        </td>
                    </tr> 
                    <tr>
                        <td><?php echo lang('wednesday')  ?></td>
                        <td>
                            <input class="form-control" name="wed_start_time" id="wed_start_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input class="form-control" name="wed_end_time" id="wed_end_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input type="checkbox" name="wed_off" id="wed_off">
                        </td>
                    </tr> 
                    <tr>
                        <td><?php echo lang('thursday')  ?></td>
                        <td>
                            <input class="form-control" name="thurs_start_time" id="thurs_start_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input class="form-control" name="thurs_end_time" id="thurs_end_time" type="time" value="" autocomplete="off">
                        </td>
                        <td>
                            <input type="checkbox" name="thurs_off" id="thurs_off">
                        </td>
                    </tr>     
                </table>
            </div>
            <div class="col-md-6">
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="auto_rostering"><?php echo  lang('auto_rostering') ?> <span class="text-danger"></span></label>
                    </div>    
                    <div class="col-md-6">
                        <input type="checkbox" name="auto_rostering" id="auto_rostering" class="form-control"> 
                    </div> 
                </div> 
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="punch_required"><?php echo  lang('punch_required') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                        <input type="checkbox" name="log_required" id="log_required" class="form-control">
                    </div> 
                </div> 
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="logout_required"><?php echo  lang('logout_required') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                        <input type="checkbox" name="logout_required" id="logout_required" class="form-control">
                    </div>
                </div> 
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="half_day_absent"><?php echo  lang('half_day_absent') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                    <input type="checkbox" name="half_day_absent" id="half_day_absent" class="form-control">
                    </div> 
                </div> 
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="late_tolerance"><?php echo  lang('late_tolerance_minute') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                        <input class="form-control" placeholder="<?php echo lang('late_tolerance_minute');?>" name="late_tolerance" id="late_tolerance" type="text" value="">
                    </div> 
                </div> 
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="half_day_leave"><?php echo  lang('half_day_leave_minute') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                        <input class="form-control" placeholder="<?php echo lang('half_day_leave_minute');?>" name="half_day_leave" id="half_day_leave" type="text" value="">
                    </div> 
                </div>
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="start_punch"><?php echo  lang('start_punch_minute') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                        <input class="form-control" placeholder="<?php echo lang('start_punch_minute');?>" name="start_punch" id="start_punch" type="text" value="">
                    </div> 
                </div>
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="end_punch"><?php echo  lang('end_punch_minute') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                        <input class="form-control" placeholder="<?php echo lang('end_punch_minute');?>" name="end_punch" id="end_punch" type="text" value="">
                    </div> 
                </div>
                <div class="form-group row"> 
                    <div class="col-md-6">
                        <label for="overtime_after"><?php echo  lang('overtime_after_minute') ?> <span class="text-danger"></span></label>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group row"> 
                            <div class="col-md-4">
                                <input type="checkbox" name="overtime_enable" id="overtime_enable" class="form-control">
                            </div> 
                            <div class="col-md-8"> 
                                <input type="text" name="overtime_after" id="overtime_after" class="form-control">
                            </div> 
                        </div> 
                    </div> 
                </div>
            </div>  
           </div>
		   <div class="form-group row">
                <div class="col-md-6"> 
                    <input type="hidden" id="work_start_time" name="work_start_time">        
                    <input type="hidden" id="work_end_time" name="work_end_time">        
                        
                    <?php echo form_submit('add_shift_master', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
           </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_emp_shift_map').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_shift_master/add_emp_shift_map",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                 if(data.error == 1) {
                     $('.error_msg').text(data.message);
                 } 
                 else {
                    $('#myModal').modal('hide');
				    location.reload();
                 }
				
             }
               
      });  
  });

  $(document).on('change', '#shift_mst_id', function(){
        var shiftMstId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_shift_master/get_shift_details',
            method: 'POST',
            type: 'json',
            data: { shift_mst_id: shiftMstId },
            success: function(data) {
                var objArr = $.parseJSON(data);
                
                if(objArr.isError == 0) {

                var obj = (objArr.shift_data);
                $('#fri_start_time').val(obj.fri_start_time);
                $('#fri_end_time').val(obj.fri_end_time);
                $('#sat_start_time').val(obj.sat_start_time);
                $('#sat_end_time').val(obj.sat_end_time);
                $('#sun_start_time').val(obj.sun_start_time);
                $('#sun_end_time').val(obj.sun_end_time);
                $('#mon_start_time').val(obj.mon_start_time);
                $('#mon_end_time').val(obj.mon_end_time);
                $('#tues_start_time').val(obj.tues_start_time);
                $('#tues_end_time').val(obj.tues_end_time);
                $('#wed_start_time').val(obj.wed_start_time);
                $('#wed_end_time').val(obj.wed_end_time);
                $('#thurs_start_time').val(obj.thurs_start_time);
                $('#thurs_end_time').val(obj.thurs_end_time);

                (obj.fri_off == 1) ? $('#fri_off').attr('checked', true) : $('#fri_off').attr('checked', false);
                (obj.sat_off == 1) ? $('#sat_off').attr('checked', true) : $('#sat_off').attr('checked', false);
                (obj.sun_off == 1) ? $('#sun_off').attr('checked', true) : $('#sun_off').attr('checked', false);
                (obj.mon_off == 1) ? $('#mon_off').attr('checked', true) : $('#mon_off').attr('checked', false);
                (obj.tues_off == 1) ? $('#tues_off').attr('checked', true) : $('#tues_off').attr('checked', false);
                (obj.wed_off == 1) ? $('#wed_off').attr('checked', true) : $('#wed_off').attr('checked', false);
                (obj.thurs_off == 1) ? $('#thurs_off').attr('checked', true) : $('#thurs_off').attr('checked', false);

                $('#log_required').attr('checked', true);
                $('#overtime_enable').attr('checked', false);

                if($("input[type='checkbox']:checked")) {
                    $("input[type='checkbox']:checked").css({"opacity": "1"});
                    $("input[type='checkbox']:checked").css({"width": "100%"});
                    $("input[type='checkbox']:checked").css({"margin": "0px 4px"});
                } 
                else {
                    $("input[type='checkbox']:checked").css({"opacity": "0"});
                    $("input[type='checkbox']:checked").css({"width": "100%"});
                    $("input[type='checkbox']:checked").css({"margin": "0px 4px"});
                }

                $('#late_tolerance').val(obj.late_tolerance);
                $('#half_day_leave').val(obj.half_day_leave);
                $('#start_punch').val(obj.start_punch);
                $('#end_punch').val(obj.end_punch);
                $('#work_start_time').val(obj.work_start_time);
                $('#work_end_time').val(obj.work_end_time);
                $('#overtime_after').val('');

                } else {

                $('#fri_start_time').val('');
                $('#fri_end_time').val('');
                $('#sat_start_time').val('');
                $('#sat_end_time').val('');
                $('#sun_start_time').val('');
                $('#sun_end_time').val('');
                $('#mon_start_time').val('');
                $('#mon_end_time').val('');
                $('#tues_start_time').val('');
                $('#tues_end_time').val('');
                $('#wed_start_time').val('');
                $('#wed_end_time').val('');
                $('#thurs_start_time').val('');
                $('#thurs_end_time').val('');

                $('#late_tolerance').val('');
                $('#half_day_leave').val('');
                $('#start_punch').val('');
                $('#end_punch').val('');
                $('#overtime_after').val('');

                $('#fri_off').attr('checked', false);
                $('#sat_off').attr('checked', false);
                $('#sun_off').attr('checked', false);
                $('#mon_off').attr('checked', false);
                $('#tues_off').attr('checked', false);
                $('#wed_off').attr('checked', false);
                $('#thurs_off').attr('checked', false);
                $('#log_required').attr('checked', false);
                $('#overtime_enable').attr('checked', false);

                }

            } 

        });
  });

  $(document).on('change', '#department', function(){
        var deptId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_shift_master/get_dept_employees',
            method: 'POST',
            type: 'text',
            data: { dept_id: deptId },
            success: function(data) {
                $('#employee_id').remove();
                $('#emp_area').html(data);
            }
        });
  });
  $(document).on('change', '#employee_id', function(){
        var employeeId = $(this).val();

        $.ajax({
            url: site.base_url + 'hrm_shift_master/get_employees_card',
            method: 'POST',
            type: 'text',
            data: { employee_id: employeeId },
            success: function(data) {
                $('#card_no').val(data);
            }
        });
  });

});
</script>
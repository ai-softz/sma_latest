<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_shift_master'); ?></h4>
    </div>

    <div class="modal-body">
	<p class="error_msg text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_shift_master']; ?>
		     <?php echo admin_form_open_multipart("hrm_shift_master/add_shift_master", $attrib) ?>
		     
           <div class="form-group row">
		   		<div class="col-md-6">
					<label for="name"><?php echo  lang('name') ?> <span class="text-danger"></span></label>
					<input class="form-control" placeholder="<?php echo lang('name');?>" name="name" type="text" value="">
		      	</div> 
                <div class="col-md-6">
                    <label for="name_alt"><?php echo  lang('alternate_name') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('alternate_name');?>" name="name_alt" type="text" value="">
		        </div> 
		   </div>
		   <div class="form-group row">
            <div class="col-md-6">
		         <label for="start_time"><?php echo  lang('start_time') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('start_time');?>" name="start_time" type="time" value="" autocomplete="off">
		    </div> 
		    <div class="col-md-6">
		         <label for="end_time"><?php echo  lang('end_time') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('end_time');?>" name="end_time" type="time" value="" autocomplete="off">
		    </div>
		   </div>
           <div class="form-group row">
		   		<div class="col-md-6">
					<label for="late_tolerance"><?php echo  lang('late_tolerance_minute') ?> <span class="text-danger"></span></label>
					<input class="form-control" placeholder="<?php echo lang('late_tolerance_minute');?>" name="late_tolerance" type="text" value="">
		      	</div> 
                <div class="col-md-6">
                    <label for="half_day_leave"><?php echo  lang('half_day_leave_minute') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('half_day_leave_minute');?>" name="half_day_leave" type="text" value="">
		        </div> 
		   </div>
           <div class="form-group row">
		   		<div class="col-md-6">
					<label for="start_punch"><?php echo  lang('start_punch_minute') ?> <span class="text-danger"></span></label>
					<input class="form-control" placeholder="<?php echo lang('start_punch_minute');?>" name="start_punch" type="text" value="">
		      	</div> 
                <div class="col-md-6">
                    <label for="end_punch"><?php echo  lang('end_punch_minute') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('end_punch_minute');?>" name="end_punch" type="text" value="">
		        </div> 
		   </div>
		   <div class="form-group row">
		      <div class="col-md-12">

				<label for="day_duration"><?php echo  lang('day_duration') ?> <span class="text-danger"></span></label>
				<select name="day_duration" class="form-control select">
					<option>Select</option>
						<option value="DayShift"><?php echo lang('day_shift') ; ?></option>
						<option value="NightShift"><?php echo lang('night_shift') ; ?></option>
				</select>
		      </div>
		   </div>
           <div class="form-group row">
		      <div class="col-md-12">
				<label for="weekly_holiday"><?php echo  lang('weekly_holiday') ?> <span class="text-danger"></span></label>
				<?php
                $days = array('FRIDAY', 'SATURDAY', 'SUNDAY', 'MONDAY', 'THUESDAY', 'WEDNESDAY', 'THURSDAY');
                ?>
                <?php foreach($days as $day) { ?>
                    &nbsp;&nbsp;<input type="checkbox" name="weekly_holiday[]" value="<?= $day ?>">  &nbsp;&nbsp;<?= $day ?>
                <?php } ?>
		      </div>
		   </div>
           <div class="form-group row">
		      <div class="col-md-12">
				<label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
				<textarea name="remark" class="form-control"> </textarea>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_shift_master', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_shift_master').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "hrm_shift_master/add_shift_master",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				$('#myModal').modal('hide');
					location.reload();
             }
               
      });  
  });


});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_period_master'); ?></h4>
    </div>

    <div class="modal-body">
	<p class="error_msg text-danger" style="font-size: 16px"> </p>
    <div class="row">
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'add_period_master']; ?>
		     <?php echo admin_form_open_multipart("Hrm_period_masters/add_period_master", $attrib) ?>
		     
		    <div class="form-group row">
			<div class="col-md-12">
		         <label for="period_type"><?php echo  lang('period_type') ?> <span class="text-danger">*</span></label>
				 <select name="period_type" class="form-control select">
					<option> Select Period Type </option>
					<option value="customize"> <?= lang('customize') ?> </option>
					<option value="monthly"> <?= lang('monthly') ?> </option>
					<option value="weekly"> <?= lang('weekly') ?> </option>
				</select>
		      </div>
		      
		   </div>
           <div class="form-group row" style="display: none">
		   		<div class="col-md-6">
					<label for="name"><?php echo  lang('name') ?> <span class="text-danger"></span></label>
					<input class="form-control" placeholder="<?php echo lang('name');?>" name="name" type="text" value="">
		      	</div> 
                <div class="col-md-6">
                    <label for="name_alt"><?php echo  lang('alternate_name') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('alternate_name');?>" name="name_alt" type="text" value="">
		        </div> 
		      
		      
		   </div>
		   <div class="form-group row">
            <div class="col-md-6">
		         <label for="start_date"><?php echo  lang('start_date') ?> <span class="text-danger"></span></label>
		         <input class="form-control date" placeholder="<?php echo lang('start_date');?>" name="start_date" type="text" value="" autocomplete="off">
		    </div> 
		    <div class="col-md-6">
		         <label for="end_date"><?php echo  lang('end_date') ?> <span class="text-danger"></span></label>
		         <input class="form-control date" placeholder="<?php echo lang('end_date');?>" name="end_date" type="text" value="" autocomplete="off">
		    </div> 
            
		   </div>
		   <div class="form-group row">
		      <div class="col-md-12">
		        <?php $years = range(2020, date('Y') + 20);  ?>

				<label for="year"><?php echo  lang('year') ?> <span class="text-danger"></span></label>
				<select name="year" class="form-control select">
					<option>Select Year</option>
					<?php foreach($years as $year) : ?>
						<option value="<?php echo $year; ?>" <?php //echo $year == date('Y') ? 'Selected' : ''; ?>><?php echo $year; ?></option>
					<?php endforeach; ?>
				</select>
		      </div>
		   </div>
		   <div class="form-group">
		        <?php echo form_submit('add_period_master', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>
    </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#add_period_master').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "Hrm_period_masters/add_period_master",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				if(data.error == 1) {
					$('.error_msg').text(data.msg);
				}
				else {
					$('#myModal').modal('hide');
					location.reload();
				}
             }
               
      });  
  });


});
</script>
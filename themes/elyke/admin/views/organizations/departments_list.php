<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
  #employee_tabs_top a{
    font-size: 16px;
  }
  #employee_tabs_top ul li {
    margin-right: 10px;
  }
  </style>
<div class="panel">
   <div class="panel-body" style="background-color: #f5f5f5;">
      <ul class="nav nav-pills" id="employee_tabs_top">
         <li class="nav-item active">
            <a class="nav-link" data-toggle="tab" aria-current="page" href="#smartwizard-2-step-1"> 
              <span class="fa fa-users"></span> <?= lang('department') ?></a>
         </li>

      </ul>

      <div class="container" id="smartwizard_tabs" style="margin-top: 30px;background-color: #fff;padding: 10px;">
         <div class="tab-content">
         	<div class="tab-pane active" id="smartwizard-2-step-1">
               <div class="row">
                     <div class="col-md-12">
                        <?php 
                        // print_r($departments);
                         $this->data['all_warehouses'] = $all_warehouses;
                         $this->data['all_employees'] = $all_employees;
                         $this->load->view($this->theme . 'organizations/departments', $this->data); 
                         ?>
                     </div>
                  </div>
            </div>
            
            <div class="tab-pane" id="smartwizard-2-step-3">
               <div class="row">
                     <div class="col-md-12">
                        <?php 
                        // print_r($departments);
                         $this->data['all_warehouses'] = $all_warehouses;
                         $this->data['all_employees'] = $all_employees;
                         $this->load->view($this->theme . 'organizations/office_shift', $this->data); 
                         ?>
                     </div>
                  </div>
            </div>
          </div> <!-- /.tab-content #smartwizard_tabs -->
      </div> <!-- /.container #smartwizard_tabs -->
      
   </div> <!-- /.panel-body -->
</div> <!-- /.panel -->

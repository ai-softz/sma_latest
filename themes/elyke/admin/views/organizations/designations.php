<h2><?php echo lang('designation');?>:</h2>
<div class="table-responsive">
 <table id="table_designations" class="table table-bordered table-hover table-striped">
    <thead>
       <tr>

          <th><?php echo lang('#');?></th>
          <th><?php echo lang('designation');?></th>
          <th><?php echo lang('department');?></th>
          <th style="width:100px;"><?= lang('actions'); ?></th>
       </tr>
    </thead>
    <tbody>
    </tbody>
 </table>
</div>

<div class="row">
  <h3><b><?php echo lang('add_new');?></b> <?php echo lang('designation');?></h3>
  <div class="col-md-12">
    <?php $attrib = [ 'role' => 'form', 'id' => 'add_designation']; ?>
     <?php echo admin_form_open_multipart("designations/add_designation", $attrib) ?>
     
    <div class="form-group row">
      <div class="col-md-6">
         <label for="designation_name"><?php echo  lang('name') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('designation_name');?>" name="designation_name" id="designation_name" type="text" value="" required="required">
      </div>
      <div class="col-md-6">
         <label for="secondary_name"><?php echo  lang('caption_alt') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('secondary_name');?>" name="secondary_name" id="secondary_name" type="text" value="" required="required">
      </div>
   </div>

   <div class="form-group row">
    <div class="col-md-6">
         <label for="department_id"><?php echo  lang('department') ?> <span class="text-danger">*</span></label>
          <select name="department_id" id="department_id" class="form-control select">
        <option value=""><?php echo 'Select';?></option>
        <?php foreach($all_departments as $row) { ?>
          <option value="<?php echo $row->department_id; ?>"><?php echo $row->department_name; ?></option>
        <?php } ?>
      </select>
      </div>
     <div class="col-md-6">
         <label for="description"><?php echo  lang('description') ?> <span class="text-danger"></span></label>
         <input class="form-control" placeholder="<?php echo lang('description');?>" name="description" id="description" type="text" value="">
    </div>  
     
   </div>
   
   <div class="form-group">
        <?php echo form_submit('add_designation', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

<script>
   $(document).ready(function () {
   //  $(document).on('click', '#designations_table', function(){
      oTable = $('#table_designations').dataTable({
           "aaSorting": [[1, "asc"]],
           "bDestroy": true,
           "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
           "iDisplayLength": <?= $Settings->rows_per_page ?>,
           'bProcessing': true, 'bServerSide': true,
           'sAjaxSource': '<?= admin_url('designations/getAllDesignations') ?>',
           "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //console.log(aData);
            },
           'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                   "name": "<?= $this->security->get_csrf_token_name() ?>",
                   "value": "<?= $this->security->get_csrf_hash() ?>"
               });
               $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
           },
           "aoColumns": [null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
       });
   //  });

     $('#add_designation').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "designations/add_designation",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
               //  $('#table_designations').DataTable().ajax.reload();
                  $('#myModal').modal('hide');
				      location.reload();
             }
               
      }); 

  });

});
</script>
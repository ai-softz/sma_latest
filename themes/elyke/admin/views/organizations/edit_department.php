<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edir_department'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
 
		  <div class="col-md-12">
		    <?php $attrib = [ 'role' => 'form', 'id' => 'edit_department']; ?>
		     <?php echo admin_form_open_multipart("departments/edit_department", $attrib) ?>
		     <input type="hidden" name="department_id" value="<?= $department->department_id ?>">
		    <div class="form-group row">
		      <div class="col-md-6">
		         <label for="department_name"><?php echo  lang('department_name') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('department_name');?>" name="department_name" id="department_name" type="text" value="<?= $department->department_name ?>" required="required">
		      </div>
		      <div class="col-md-6">
		         <label for="secondary_name"><?php echo  lang('secondary_name') ?> <span class="text-danger"></span></label>
		         <input class="form-control" placeholder="<?php echo lang('secondary_name');?>" name="secondary_name" id="secondary_name" type="text" value="<?= $department->secondary_name ?>" required="required">
		      </div>
		      
		   </div>

		   <div class="form-group row">
		   	<div class="col-md-6">
		         <label for="warehouse"><?php echo  lang('warehouse') ?> <span class="text-danger">*</span></label>
		       		<select name="warehouse_id" id="warehouse" class="form-control select">
		  			<option value=""><?php echo 'Select';?></option>
		  			<?php foreach($all_warehouses as $warehouse) { ?>
		  				<option value="<?php echo $warehouse->id; ?>" <?php echo $department->location_id == $warehouse->id ? "Selected" : "" ?>><?php echo $warehouse->name; ?></option>
		  			<?php } ?>
					</select>
		      </div>
		     <div class="col-md-6">
		         <label for="employee_id"><?php echo  lang('department_head') ?> <span class="text-danger"></span></label>
			       <select name="employee_id" class="form-control select";?>">
			       <option value=""><?php echo 'Select';?></option>
			       <?php foreach($all_employees as $row) { ?>
			  				<option value="<?php echo $row->user_id; ?>" <?php echo $department->employee_id == $row->user_id ? "Selected" : "" ?>><?php echo $row->first_name.' '.$row->last_name; ?></option>
			  		<?php } ?> 
			  		</select>
		    </div>  
		     
		   </div>
		   
		   <div class="form-group">
		        <?php echo form_submit('edir_department', lang('save'), 'class="btn btn-primary"'); ?>
		    </div>
		    <?php echo form_close() ?>
		  </div>
		  
		</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_department').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "departments/editDepartment",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
					$('#myModal').modal('hide');
				    location.reload();
                // $('#table_departments').DataTable().ajax.reload();
             }
               
      });  
  });

});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php

if($wages_type ==1) {

$payment_month = strtotime($this->input->get('pay_date'));
$p_month = date('F Y',$payment_month);
if($wages_type==1){
	if($this->Settings->is_half_monthly==1){
		//if($half_deduct_month==2){
			$basic_salary = $basic_salary / 2;
		//} else {
			//$basic_salary = $basic_salary;
		//}
	} else {
		$basic_salary = $basic_salary;
	}
} else {
	$basic_salary = $daily_wages;
}
?>
<?php
$salary_allowances = $this->payroll_model->read_salary_allowances($user_id);
$count_allowances = $this->payroll_model->count_employee_allowances($user_id);
$allowance_amount = 0;
$eallowance_amount = 0;
$iallowance_amount = 0;
if($count_allowances > 0) {
	foreach($salary_allowances as $sl_allowances){
		//$allowance_amount += $sl_allowances->allowance_amount;
	  if($this->Settings->is_half_monthly==1){
		 if($this->Settings->half_deduct_month==2){
			 $eallowance_amount = $sl_allowances->allowance_amount/2;
		 } else {
			 $eallowance_amount = $sl_allowances->allowance_amount;
		 }
		 $allowance_amount += $eallowance_amount;
	  } else {
		  if($sl_allowances->is_allowance_taxable == 1) {
			  if($sl_allowances->amount_option == 0) {
				  $iallowance_amount = $sl_allowances->allowance_amount;
			  } else {
				  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
			  }
			 $allowance_amount -= $iallowance_amount; 
		  } else if($sl_allowances->is_allowance_taxable == 2) {
			  if($sl_allowances->amount_option == 0) {
				  $iallowance_amount = $sl_allowances->allowance_amount / 2;
			  } else {
				  $iallowance_amount = ($basic_salary / 100) / 2 * $sl_allowances->allowance_amount;
			  }
			 $allowance_amount -= $iallowance_amount; 
		  } else {
			  if($sl_allowances->amount_option == 0) {
				  $iallowance_amount = $sl_allowances->allowance_amount;
			  } else {
				  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
			  }
			  $allowance_amount += $iallowance_amount;
		  }
	  }
	  
	}
} else {
	$allowance_amount = 0;
}  
$salary_loan_deduction = $this->payroll_model->read_salary_loan_deductions($user_id);
$count_loan_deduction = $this->payroll_model->count_employee_deductions($user_id);
$loan_de_amount = 0;
if($count_loan_deduction > 0) {
	foreach($salary_loan_deduction as $sl_salary_loan_deduction){
		if($this->Settings->is_half_monthly==1){
			  if($this->Settings->half_deduct_month==2){
				  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount/2;
			  } else {
				  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
			  }
		  } else {
			  $er_loan = $sl_salary_loan_deduction->loan_deduction_amount;
		  }
		  $loan_de_amount += $er_loan;
	}
} else {
	$loan_de_amount = 0;
}
// 4: other payment
$other_payments = $this->payroll_model->set_employee_other_payments($user_id);
$other_payments_amount = 0;
if(!is_null($other_payments)):
	foreach($other_payments->result() as $sl_other_payments) {
		if($this->Settings->is_half_monthly==1){
		  if($this->Settings->half_deduct_month==2){
			  $epayments_amount = $sl_other_payments->payments_amount/2;
		  } else {
			  $epayments_amount = $sl_other_payments->payments_amount;
		  }
		  $other_payments_amount += $epayments_amount;
	  } else {
		  //$epayments_amount = $sl_other_payments->payments_amount;
		  if($sl_other_payments->is_otherpayment_taxable == 1) {
			  if($sl_other_payments->amount_option == 0) {
				  $epayments_amount = $sl_other_payments->payments_amount;
			  } else {
				  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
			  }
			 $other_payments_amount -= $epayments_amount; 
		  } else if($sl_other_payments->is_otherpayment_taxable == 2) {
			  if($sl_other_payments->amount_option == 0) {
				  $epayments_amount = $sl_other_payments->payments_amount / 2;
			  } else {
				  $epayments_amount = ($basic_salary / 100) / 2 * $sl_other_payments->payments_amount;
			  }
			 $other_payments_amount -= $epayments_amount; 
		  } else {
			  if($sl_other_payments->amount_option == 0) {
				  $epayments_amount = $sl_other_payments->payments_amount;
			  } else {
				  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
			  }
			  $other_payments_amount += $epayments_amount;
		  }
	  }
	  
	}
endif;
// all other payment
$all_other_payment = $other_payments_amount;
// 5: commissions
$commissions = $this->payroll_model->set_employee_commissions($user_id);
if(!is_null($commissions)):
	$commissions_amount = 0;
	foreach($commissions->result() as $sl_commissions) {
		 if($this->Settings->is_half_monthly==1){
			  if($this->Settings->half_deduct_month==2){
				  $ecommissions_amount = $sl_commissions->commission_amount/2;
			  } else {
				  $ecommissions_amount = $sl_commissions->commission_amount;
			  }
			  $commissions_amount += $ecommissions_amount;
		  } else {
			  //$ecommissions_amount = $sl_commissions->commission_amount;
			  if($sl_commissions->is_commission_taxable == 1) {
				  if($sl_commissions->amount_option == 0) {
					  $ecommissions_amount = $sl_commissions->commission_amount;
				  } else {
					  $ecommissions_amount = $basic_salary / 100 * $sl_commissions->commission_amount;
				  }
				 $commissions_amount -= $ecommissions_amount; 
			  } else if($sl_commissions->is_commission_taxable == 2) {
				  if($sl_commissions->amount_option == 0) {
					  $ecommissions_amount = $sl_commissions->commission_amount / 2;
				  } else {
					  $ecommissions_amount = ($basic_salary / 100) / 2 * $sl_commissions->commission_amount;
				  }
				 $commissions_amount -= $ecommissions_amount; 
			  } else {
				  if($sl_commissions->amount_option == 0) {
					  $ecommissions_amount = $sl_commissions->commission_amount;
				  } else {
					  $ecommissions_amount = $basic_salary / 100 * $sl_commissions->commission_amount;
				  }
				  $commissions_amount += $ecommissions_amount;
			  }
		  }
		  //$commissions_amount += $ecommissions_amount;
		  //$commissions_amount += $sl_commissions->commission_amount;
	}
endif;
// 6: statutory deductions
$statutory_deductions = $this->payroll_model->set_employee_statutory_deductions($user_id);
$statutory_deductions_amount = 0;
if(!is_null($statutory_deductions)):
	$statutory_deductions_amount = 0;
	foreach($statutory_deductions->result() as $sl_statutory_deductions) {
		if($this->Settings->is_half_monthly==1){
			 if($this->Settings->half_deduct_month==2){
				$single_sd = $sl_statutory_deductions->deduction_amount/2;
			 } else {
				$single_sd = $sl_statutory_deductions->deduction_amount;
			 }
			 $statutory_deductions_amount += $single_sd;
		  } else {
			  //$single_sd = $sl_statutory_deductions->deduction_amount;
			 if($sl_statutory_deductions->statutory_options == 0) {
				  $single_sd = $sl_statutory_deductions->deduction_amount;
			  } else {
				  $single_sd = $basic_salary / 100 * $sl_statutory_deductions->deduction_amount;
			  }
			 $statutory_deductions_amount += $single_sd; 
		  }
		//$statutory_deductions_amount += $single_sd;
	}
endif;
// 7: overtime
$salary_overtime = $this->payroll_model->read_salary_overtime($user_id);
$count_overtime = $this->payroll_model->count_employee_overtime($user_id);
$overtime_amount = 0;
if($count_overtime > 0) {
	foreach($salary_overtime as $sl_overtime){
		if($this->Settings->is_half_monthly==1){
			if($this->Settings->half_deduct_month==2){
				$eovertime_hours = $sl_overtime->overtime_hours/2;
				$eovertime_rate = $sl_overtime->overtime_rate/2;
			} else {
				$eovertime_hours = $sl_overtime->overtime_hours;
				$eovertime_rate = $sl_overtime->overtime_rate;
			}
		} else {
			$eovertime_hours = $sl_overtime->overtime_hours;
			$eovertime_rate = $sl_overtime->overtime_rate;
		}
		$overtime_amount += $eovertime_hours * $eovertime_rate;
		//$overtime_total = $sl_overtime->overtime_hours * $sl_overtime->overtime_rate;
		//$overtime_amount += $overtime_total;
	}
} else {
	$overtime_amount = 0;
}

// add amount
$add_salary = $allowance_amount + $basic_salary + $overtime_amount + $all_other_payment + $commissions_amount ;
// add amount
$net_salary_default = $add_salary - $loan_de_amount - $statutory_deductions_amount;
$sta_salary = $allowance_amount + $basic_salary;

$estatutory_deductions = $statutory_deductions_amount;
// net salary + statutory deductions
$net_salary = $net_salary_default;
$net_salary = number_format((float)$net_salary, 2, '.', '');
// print_r($allowance_amount);

// get advance salary
$advance_salary = $this->payroll_model->advance_salary_by_employee_id($user_id);
$emp_value = $this->payroll_model->get_paid_salary_by_employee_id($user_id);

if(!is_null($advance_salary)){
	$monthly_installment = $advance_salary[0]->monthly_installment;
	$advance_amount = $advance_salary[0]->advance_amount;
	$total_paid = $advance_salary[0]->total_paid;
	//check ifpaid
	$em_advance_amount = $advance_salary[0]->advance_amount;
	$em_total_paid = $advance_salary[0]->total_paid;
	
	if($em_advance_amount > $em_total_paid){
		if($monthly_installment=='' || $monthly_installment==0) {
			
			$ntotal_paid = $emp_value[0]->total_paid;
			$nadvance = $emp_value[0]->advance_amount;
			$total_net_salary = $nadvance - $ntotal_paid;
			$pay_amount = $net_salary - $total_net_salary;
			$advance_amount = $total_net_salary;
		} else {
			//
			$re_amount = $em_advance_amount - $em_total_paid;
			if($monthly_installment > $re_amount){
				$advance_amount = $re_amount;
				$total_net_salary = $net_salary - $re_amount;
				$pay_amount = $net_salary - $re_amount;
			} else {
				$advance_amount = $monthly_installment;
				$total_net_salary = $net_salary - $monthly_installment;
				$pay_amount = $net_salary - $monthly_installment;
			}
		}
		
	} else {
		$total_net_salary = $net_salary - 0;
		$pay_amount = $net_salary - 0;
		$advance_amount = 0;
	}
} else {
	$pay_amount = $net_salary - 0;
	$total_net_salary = $net_salary - 0;	
	$advance_amount = 0;
}
$net_salary = $net_salary - $advance_amount;
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('payment'); ?></h4>
        </div>

        <div class="modal-body">
        	<div class="row">
  				<div class="col-md-12">
		           <?php $attrib = [ 'role' => 'form', 'id' => 'add_pay_monthly']; ?>
				   <?php echo admin_form_open_multipart("payroll/add_pay_monthly", $attrib) ?>
				   <input type="hidden" name="department_id" value="<?php echo $department_id;?>" />
		          <input type="hidden" name="designation_id" value="<?php echo $designation_id;?>" />
		          <input type="hidden" name="location_id" value="<?php echo $location_id;?>" />
		          
		          	<div class="form-group row">
						<div class="col-md-6">
						<label for="name"><?php echo $this->lang->line('monthly_payslip');?></label>		  
						<input type="text" name="gross_salary" class="form-control" value="<?php echo $basic_salary;?>"> 
						</div>
						<div class="col-md-6">	
						<label for="name"><?php echo $this->lang->line('month');?></label>		 
						<input type="text" class="date form-control" value="<?php echo $this->input->get('pay_date');?>" readonly>
						</div>
					</div>
		          <input type="hidden" id="emp_id" value="<?php echo $user_id?>" name="emp_id">
		          <input type="hidden" value="<?php echo $user_id;?>" name="u_id">
		          <input type="hidden" value="<?php echo $basic_salary;?>" name="basic_salary">
		          <input type="hidden" value="<?php echo $wages_type;?>" name="wages_type">
		          <input type="hidden" value="<?php echo $pay_date;?>" name="pay_date" id="pay_date">
		           <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo $this->lang->line('payroll_total_allowance');?></label>
		          		<input type="text" name="total_allowances" class="form-control" value="<?= $allowance_amount ?>" readonly="readonly">
				    </div>  
				    <div class="col-md-6">
				         <label for="name"><?php echo $this->lang->line('hr_commissions');?></label>
		          		<input type="text" name="total_commissions" class="form-control" value="<?= $commissions_amount ?>" readonly="readonly">
				    </div>   
				   </div>
				   <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo $this->lang->line('payroll_total_loan');?></label>
		          		<input type="text" name="total_loan" class="form-control" value="<?= $loan_de_amount ?>" readonly="readonly">
				     </div>  
				     <div class="col-md-6">
				         <label for="name"><?php echo $this->lang->line('payroll_total_overtime');?></label>
		          		<input type="text" name="total_overtime" class="form-control" value="<?= $overtime_amount ?>" readonly="readonly">
				     </div>   
				   </div>
				   <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo lang('statutory_deductions');?></label>
		          		<input type="text" name="total_statutory_deductions" class="form-control" value="<?= $statutory_deductions_amount ?>" readonly="readonly">
				     </div>  
				     <div class="col-md-6">
				         <label for="name"><?php echo lang('other_payment');?></label>
		          		<input type="text" name="total_other_payments" class="form-control" value="<?= $other_payments_amount ?>" readonly="readonly">
				     </div>   
				   </div>
				   <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo lang('payroll_net_salary');?></label>
         			 	<input type="text" readonly="readonly" name="net_salary" class="form-control" value="<?php echo $net_salary;?>">
				     </div>  
				     <div class="col-md-6">
				         <label for="name"><?php echo lang('payroll_payment_amount');?></label>
          				<input type="text" readonly="readonly" name="payment_amount" class="form-control" value="<?php echo $net_salary;?>">
				     </div>   
				   </div>
				    <div class="form-group">
				        <?php echo form_submit('add_pay_monthly', lang('save'), 'class="btn btn-primary"'); ?>
				    </div>
				    <?php echo form_close() ?>
				</div>
			</div>
		</div>
		
           
		
	</div>
</div>
<script>
$('#add_pay_monthly').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "payroll/add_pay_monthly",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#myModal').modal('hide');
             }
               
      });  
  });
</script>
<?php
} else if($wages_type == 2)
{

$payment_month = strtotime($this->input->get('pay_date'));
$p_month = date('F Y',$payment_month);
$basic_salary = $basic_salary;

$salary_allowances = $this->payroll_model->read_salary_allowances($user_id);
$count_allowances = $this->payroll_model->count_employee_allowances($user_id);
$allowance_amount = 0;
if($count_allowances > 0) {
	foreach($salary_allowances as $sl_allowances){
		if($sl_allowances->is_allowance_taxable == 1) {
		  if($sl_allowances->amount_option == 0) {
			  $iallowance_amount = $sl_allowances->allowance_amount;
		  } else {
			  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
		  }
		 $allowance_amount -= $iallowance_amount; 
	  } else if($sl_allowances->is_allowance_taxable == 2) {
		  if($sl_allowances->amount_option == 0) {
			  $iallowance_amount = $sl_allowances->allowance_amount / 2;
		  } else {
			  $iallowance_amount = ($basic_salary / 100) / 2 * $sl_allowances->allowance_amount;
		  }
		 $allowance_amount -= $iallowance_amount; 
	  } else {
		  if($sl_allowances->amount_option == 0) {
			  $iallowance_amount = $sl_allowances->allowance_amount;
		  } else {
			  $iallowance_amount = $basic_salary / 100 * $sl_allowances->allowance_amount;
		  }
		  $allowance_amount += $iallowance_amount;
	  }
		  //$allowance_amount += $sl_allowances->allowance_amount;
	}
} else {
	$allowance_amount = 0;
} 
// 3: all loan/deductions
$salary_loan_deduction = $this->payroll_model->read_salary_loan_deductions($user_id);
$count_loan_deduction = $this->payroll_model->count_employee_deductions($user_id);
$loan_de_amount = 0;
if($count_loan_deduction > 0) {
	foreach($salary_loan_deduction as $sl_salary_loan_deduction){
		$loan_de_amount += $sl_salary_loan_deduction->loan_deduction_amount;
	}
} else {
	$loan_de_amount = 0;
}
// 4: other payment
$other_payments = $this->payroll_model->set_employee_other_payments($user_id);
$other_payments_amount = 0;
if(!is_null($other_payments)):
	foreach($other_payments->result() as $sl_other_payments) {
		//$other_payments_amount += $sl_other_payments->payments_amount;
		if($sl_other_payments->is_otherpayment_taxable == 1) {
			  if($sl_other_payments->amount_option == 0) {
				  $epayments_amount = $sl_other_payments->payments_amount;
			  } else {
				  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
			  }
			 $other_payments_amount -= $epayments_amount; 
		  } else if($sl_other_payments->is_otherpayment_taxable == 2) {
			  if($sl_other_payments->amount_option == 0) {
				  $epayments_amount = $sl_other_payments->payments_amount / 2;
			  } else {
				  $epayments_amount = ($basic_salary / 100) / 2 * $sl_other_payments->payments_amount;
			  }
			 $other_payments_amount -= $epayments_amount; 
		  } else {
			  if($sl_other_payments->amount_option == 0) {
				  $epayments_amount = $sl_other_payments->payments_amount;
			  } else {
				  $epayments_amount = $basic_salary / 100 * $sl_other_payments->payments_amount;
			  }
			  $other_payments_amount += $epayments_amount;
		  }
	}
endif;
// all other payment
$all_other_payment = $other_payments_amount;
// 5: commissions
$commissions = $this->payroll_model->set_employee_commissions($user_id);
if(!is_null($commissions)):
	$commissions_amount = 0;
	foreach($commissions->result() as $sl_commissions) {
		//$commissions_amount += $sl_commissions->commission_amount;
		if($sl_commissions->is_commission_taxable == 1) {
			  if($sl_commissions->amount_option == 0) {
				  $ecommissions_amount = $sl_commissions->commission_amount;
			  } else {
				  $ecommissions_amount = $basic_salary / 100 * $sl_commissions->commission_amount;
			  }
			 $commissions_amount -= $ecommissions_amount; 
		  } else if($sl_commissions->is_commission_taxable == 2) {
			  if($sl_commissions->amount_option == 0) {
				  $ecommissions_amount = $sl_commissions->commission_amount / 2;
			  } else {
				  $ecommissions_amount = ($basic_salary / 100) / 2 * $sl_commissions->commission_amount;
			  }
			 $commissions_amount -= $ecommissions_amount; 
		  } else {
			  if($sl_commissions->amount_option == 0) {
				  $ecommissions_amount = $sl_commissions->commission_amount;
			  } else {
				  $ecommissions_amount = $basic_salary / 100 * $sl_commissions->commission_amount;
			  }
			  $commissions_amount += $ecommissions_amount;
		  }
	}
endif;
// 6: statutory deductions
$statutory_deductions = $this->payroll_model->set_employee_statutory_deductions($user_id);
if(!is_null($statutory_deductions)):
	$statutory_deductions_amount = 0;
	foreach($statutory_deductions->result() as $sl_statutory_deductions) {
		/*if($system[0]->statutory_fixed!='yes'):
			$sta_salary = $basic_salary;
			$st_amount = $sta_salary / 100 * $sl_statutory_deductions->deduction_amount;
			$statutory_deductions_amount += $st_amount;
		else:
			$statutory_deductions_amount += $sl_statutory_deductions->deduction_amount;
		endif;*/
		if($sl_statutory_deductions->statutory_options == 0) {
			  $single_sd = $sl_statutory_deductions->deduction_amount;
		  } else {
			  $single_sd = $basic_salary / 100 * $sl_statutory_deductions->deduction_amount;
		  }
		 $statutory_deductions_amount += $single_sd; 
	}
endif;
// 7: overtime
$salary_overtime = $this->payroll_model->read_salary_overtime($user_id);
$count_overtime = $this->payroll_model->count_employee_overtime($user_id);
$overtime_amount = 0;
if($count_overtime > 0) {
	foreach($salary_overtime as $sl_overtime){
		$overtime_total = $sl_overtime->overtime_hours * $sl_overtime->overtime_rate;
		$overtime_amount += $overtime_total;
	}
} else {
	$overtime_amount = 0;
}
//overtime request
$overtime_count = $this->overtime_request_model->get_overtime_request_count($user_id,$this->input->get('pay_date'));
$re_hrs_old_int1 = 0;
$re_hrs_old_seconds =0;
$re_pcount = 0;
foreach ($overtime_count as $overtime_hr){
	// total work			
	$request_clock_in =  new DateTime($overtime_hr->request_clock_in);
	$request_clock_out =  new DateTime($overtime_hr->request_clock_out);
	$re_interval_late = $request_clock_in->diff($request_clock_out);
	$re_hours_r  = $re_interval_late->format('%h');
	$re_minutes_r = $re_interval_late->format('%i');			
	$re_total_time = $re_hours_r .":".$re_minutes_r.":".'00';
	
	$re_str_time = $re_total_time;

	$re_str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $re_str_time);
	
	sscanf($re_str_time, "%d:%d:%d", $hours, $minutes, $seconds);
	
	$re_hrs_old_seconds = $hours * 3600 + $minutes * 60 + $seconds;
	
	$re_hrs_old_int1 += $re_hrs_old_seconds;
	
	$re_pcount = gmdate("H", $re_hrs_old_int1);			
}

// add amount
$add_salary = $allowance_amount + $overtime_amount + $all_other_payment + $commissions_amount ;
// add amount
$net_salary_default = $add_salary - $loan_de_amount - $statutory_deductions_amount;
$sta_salary = $allowance_amount + $basic_salary;

$estatutory_deductions = $statutory_deductions_amount;
// net salary + statutory deductions
$pay_date = $_GET['pay_date'];
$result = $this->payroll_model->total_hours_worked($user_id,$pay_date);
$hrs_old_int1 = 0;
$pcount = 0;
$Trest = 0;
$total_time_rs = 0;
$hrs_old_int_res1 = 0;
foreach ($result->result() as $hour_work){
	// total work			
	$clock_in =  new DateTime($hour_work->clock_in);
	$clock_out =  new DateTime($hour_work->clock_out);
	$interval_late = $clock_in->diff($clock_out);
	$hours_r  = $interval_late->format('%h');
	$minutes_r = $interval_late->format('%i');			
	$total_time = $hours_r .":".$minutes_r.":".'00';
	
	$str_time = $total_time;

	$str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
	
	sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
	
	$hrs_old_seconds = $hours * 3600 + $minutes * 60 + $seconds;
	
	$hrs_old_int1 += $hrs_old_seconds;
	
	$pcount = gmdate("H", $hrs_old_int1);			
}
$pcount = $pcount + $re_pcount;
if($pcount > 0){
	$total_count = $pcount * $basic_salary;
	$fsalary = $total_count + $net_salary_default;
} else {
	$fsalary = $pcount;
}
$net_salary = $fsalary;
$net_salary = number_format((float)$net_salary, 2, '.', '');
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('payment'); ?></h4>
        </div>

        <div class="modal-body">
        	<div class="row">
  				<div class="col-md-12">
		           <?php $attrib = [ 'role' => 'form', 'id' => 'add_pay_hourly']; ?>
				   <?php echo admin_form_open_multipart("payroll/add_pay_hourly", $attrib) ?>
				   <input type="hidden" name="department_id" value="<?php echo $department_id;?>" />
		          <input type="hidden" name="designation_id" value="<?php echo $designation_id;?>" />
		          
		          <input type="hidden" name="location_id" value="<?php echo $location_id;?>" />
		          
				  	<div class="form-group row">
						<div class="col-md-6">	 
						<label for="name"><?php echo $this->lang->line('payroll_basic_salary');?></label>	 
						<input type="text" name="gross_salary" class="form-control" value="<?php echo $basic_salary;?>"> 
						</div>
						<div class="col-md-6">	 
						<label for="name"><?php echo $this->lang->line('month');?></label>		
						<input type="text" class="form-control date" value="<?php echo $this->input->get('pay_date');?>" readonly>
						</div>
					</div>
		          <input type="hidden" id="emp_id" value="<?php echo $user_id?>" name="emp_id">
		          <input type="hidden" value="<?php echo $user_id;?>" name="u_id">
		          <input type="hidden" value="<?php echo $basic_salary;?>" name="basic_salary">
		          <input type="hidden" value="<?php echo $wages_type;?>" name="wages_type">
		          <input type="hidden" value="<?php echo $this->input->get('pay_date');?>" name="pay_date" id="pay_date">
		           <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo $this->lang->line('payroll_total_allowance');?></label>
		          		<input type="text" name="total_allowances" class="form-control" value="<?= $allowance_amount ?>" readonly="readonly">
				    </div>  
				    <div class="col-md-6">
				         <label for="name"><?php echo $this->lang->line('hr_commissions');?></label>
		          		<input type="text" name="total_commissions" class="form-control" value="<?= $commissions_amount ?>" readonly="readonly">
				    </div>   
				   </div>
				   <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo $this->lang->line('payroll_total_loan');?></label>
		          		<input type="text" name="total_loan" class="form-control" value="<?= $loan_de_amount ?>" readonly="readonly">
				     </div>  
				     <div class="col-md-6">
				         <label for="name"><?php echo $this->lang->line('payroll_total_overtime');?></label>
		          		<input type="text" name="total_overtime" class="form-control" value="<?= $overtime_amount ?>" readonly="readonly">
				     </div>   
				   </div>
				   <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo lang('statutory_deductions');?></label>
		          		<input type="text" name="total_statutory_deductions" class="form-control" value="<?= $statutory_deductions_amount ?>" readonly="readonly">
				     </div>  
				     <div class="col-md-6">
				         <label for="name"><?php echo lang('other_payment');?></label>
		          		<input type="text" name="total_other_payments" class="form-control" value="<?= $other_payments_amount ?>" readonly="readonly">
				     </div>   
				   </div>
				   <div class="form-group row">
				     <div class="col-md-6">
				        <label for="name"><?php echo lang('payroll_net_salary');?></label>
         			 	<input type="text" readonly="readonly" name="net_salary" class="form-control" value="<?php echo $net_salary;?>">
				     </div>  
				     <div class="col-md-6">
				         <label for="name"><?php echo lang('payroll_payment_amount');?></label>
          				<input type="text" readonly="readonly" name="payment_amount" class="form-control" value="<?php echo $net_salary;?>">
				     </div>   
				   </div>
				    <div class="form-group">
				        <?php echo form_submit('add_pay_hourly', lang('save'), 'class="btn btn-primary"'); ?>
				    </div>
				    <?php echo form_close() ?>
				</div>
			</div>
		</div>
		
           
		
	</div>
</div>
<script>
$('#add_pay_hourly').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "payroll/add_pay_hourly",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                $('#myModal').modal('hide');
             }
               
      });  
  });
</script>
<?php
}
?>

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

</script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo 'Employee Salary Details
'; ?></h4>
        </div>

        <div class="modal-body">
        	<h2><?= $payslip_info['first_name']. ' '.$payslip_info['last_name'] ?></h2>
        	<p class="text-mute"> <?= $payslip_info['department_name'] ?> </p>
        	<div class="container">
        	<div class="row">
		<h2>Payment Details</h2>
		<div class="col-md-12 box" style="background-color: #fff;">
			<div class="payment_detail">
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#basic_salary" aria-expanded="false"> <span>Monthly Payslip</span> </a></h2>
					<div id="basic_salary" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Monthly Payslip</th> <td><?= $payslip_info['basic_salary'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_allowances" aria-expanded="false"> <span>Allowance</span> </a></h2>
					<div id="set_allowances" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Allowances</th> <td><?= $payslip_info['total_allowances'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_commission" aria-expanded="false"> <span>Commission</span> </a></h2>
					<div id="set_commission" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Commissions</th> <td><?= $payslip_info['total_commissions'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_loan" aria-expanded="false"> <span>Loan</span> </a></h2>
					<div id="set_loan" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Loan</th> <td><?= $payslip_info['total_loan'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_statutory_deduc" aria-expanded="false"> <span>Statutory Deductions</span> </a></h2>
					<div id="set_statutory_deduc" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Statutory Deductions</th> <td><?= $payslip_info['total_statutory_deductions'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_other_payment" aria-expanded="false"> <span>Other Payment</span> </a></h2>
					<div id="set_other_payment" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Other Payment</th> <td><?= $payslip_info['total_other_payments'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_over_time" aria-expanded="false"> <span>Over Time</span> </a></h2>
					<div id="set_over_time" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Total Over Time</th> <td><?= $payslip_info['total_overtime'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			<div class="payslip_section border-bottom">
					<h2><a class="collapsed text-decoration-none" data-toggle="collapse" href="#set_over_time" aria-expanded="false"> <span>Advance Deducted Salary</span> </a></h2>
					<div id="set_over_time" class="collapse">
						<div class="table-responsive" data-pattern="priority-columns">
				            <table class="table table-striped m-md-b-0">
				            	<th>Advance Deducted Salary</th> <td><?= $payslip_info['advance_salary_amount'] ?></td>
				            </table>
			        	</div>
		        	</div>
			</div>
			</div>
			
		</div>
	</div>
	</div>
		</div>
		
           
		
	</div>
</div>
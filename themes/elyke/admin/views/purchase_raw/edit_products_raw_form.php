<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_raw_products'); ?></h4>
        </div>

        <div class="modal-body">
            <p class="error_msg text-danger" style="font-size: 16px"> </p>
            <div class="row">
            <p class="text-danger" class="error_input"> </p>
            <div class="col-md-12">
                <?php $attrib = [ 'role' => 'form', 'id' => 'edit_products_raw']; ?>
                <?php echo admin_form_open_multipart("purchases_raw/edit_products_raw", $attrib) ?>                
            <input type="hidden" name="products_raw_id" value="<?= $products_raw->id ?>">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="caption"><?php echo  lang('caption') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption');?>" name="caption" type="text" value="<?= $products_raw->caption ?>" required="required">
                </div>  
                <div class="col-md-6">
                    <label for="caption_alt"><?php echo  lang('caption_alt') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('caption_alt');?>" name="caption_alt" type="text" value="<?= $products_raw->caption_alt ?>">
                </div>  
                
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="category_id"><?php echo  lang('category_id') ?> <span class="text-danger">*</span></label>
                    <select name="category_id" class="form-control select"  required="required">
                        <option value="">Select</option>
                        <?php foreach($categories_raw as $value) {?>
                            <option value="<?= $value->id ?>" <?= $products_raw->category_id == $value->id ? 'Selected' : '' ?>><?= $value->caption; ?></option>
                        <?php } ?>
                    </select>
                </div>  
                <div class="col-md-6">
                    <label for="price"><?php echo  lang('price') ?> <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="<?php echo lang('price');?>" name="price" type="text" value="<?= $products_raw->price ?>">
                </div>   
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="unit"><?php echo  lang('unit') ?> <span class="text-danger">*</span></label>
                    <select name="unit" class="form-control select"  required="required">
                        <option value="">Select</option>
                        <?php foreach($base_units as $value) {?>
                            <option value="<?= $value->id ?>" <?= $products_raw->unit == $value->id ? 'Selected' : '' ?>><?= $value->name; ?></option>
                        <?php } ?>
                    </select>
                </div>  
                <div class="col-md-6">
                    <label for="code"><?php echo  lang('code') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('code');?>" name="code" type="text" value="<?= $products_raw->code ?>">
                </div> 
            </div>                
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="slug"><?php echo  lang('slug') ?> <span class="text-danger"></span></label>
                    <input class="form-control" placeholder="<?php echo lang('slug');?>" name="slug" type="text" value="<?= $products_raw->slug ?>">
                </div>  
            </div>
            
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="remark"><?php echo  lang('remark') ?> <span class="text-danger"></span></label>
                    <textarea name="remark" class="form-control"><?= $products_raw->remark ?></textarea>
                </div>    
            </div>
            <div class="form-group">
                    <?php echo form_submit('edit_products_raw', lang('save'), 'class="btn btn-primary"'); ?>
                </div>
                <?php echo form_close() ?>
            </div>
            </div>
        </div>

    </div>
</div>	

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
$(document).ready(function(){

$('#edit_products_raw').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "purchases_raw/edit_products_raw",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
				    $('#myModal').modal('hide');
				    location.reload();
             }
               
      });  
  });

});
</script>
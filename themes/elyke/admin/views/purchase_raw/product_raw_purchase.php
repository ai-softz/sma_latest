
<?php
$v = '';
if(($product)) {
    $v .= '&product='.$product;
}
if (isset($start_date)) {
    $v .= '&start_date=' . $start_date;
}
if (isset($end_date)) {
    $v .= '&end_date=' . $end_date;
}
?>

<div class="box">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-barcode"></i><?= lang('raw_products_purchase_report'); ?> <?php
                if ($this->input->post('start_date')) {
                    echo 'From ' . $this->input->post('start_date') . ' to ' . $this->input->post('end_date');
                }
                ?></h2>
            <div class="box-icon">
                <ul class="btn-tasks">
                    <li class="dropdown">
                        <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                            <i class="icon fa fa-toggle-up"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                            <i class="icon fa fa-toggle-down"></i>
                        </a>
                    </li>
                </ul>
            </div>    
        </div>
        <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
<?php //print_r($_POST); ?>
                <p class="introtext"><?= lang('customize_report'); ?></p>

                <div id="form">
                <?php echo admin_form_open('purchases_raw/purchase_raw_product', 'autocomplete="off"'); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                            <?= lang('product', 'suggest_product'); ?>
                                <?php echo form_input('sproduct', ($_POST['sproduct'] ?? ''), 'class="form-control" id="suggest_product3"'); ?>
                                <input type="hidden" name="product" value="<?=  $product ? $product : '' ?>" id="report_product_id"/>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang('start_date', 'start_date'); ?>
                                <?php echo form_input('start_date', ($_POST['start_date'] ?? ''), 'class="form-control date" id="start_date"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang('end_date', 'end_date'); ?>
                                <?php echo form_input('end_date', ($_POST['end_date'] ?? ''), 'class="form-control date" id="end_date"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div
                            class="controls"> <?php echo form_submit('submit_report', $this->lang->line('submit'), 'class="btn btn-primary"'); ?> </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        
            

            <div class="table-responsive">
                    <table id="PrRawReportData"
                           class="table table-striped table-bordered table-condensed table-hover dfTable reports-table"
                           style="margin-bottom:5px;">
                        <thead>
                        <tr class="active">
                            <th><?= lang('product_code'); ?></th>
                            <th><?= lang('product_name'); ?></th>
                            <th><?= lang('purchased'); ?></th>
                            
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                            <th><?= lang('purchased'); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
</div>

<script>
    $(document).ready(function () {
        
        oTable = $('#PrRawReportData').dataTable({
            "aaSorting": [[3, "desc"], [2, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('purchases_raw/getPrRawPurchaseReport/?v=1'  . $v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                
            },
            "aoColumns": [null, null, {"bSortable": false},],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                // console.log(aaData);
                var pq = 0, pa = 0;
                for (var i = 0; i < aaData.length; i++) {
                    p = (aaData[aiDisplay[i]][2]).split(' ');
                    pq += parseFloat(p[0].match(/\(([^)]+)\)/)[1]);
                    pa += parseFloat(p[1]);
                    
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[2].innerHTML = '<div class="text-left">('+formatQuantity2(pq)+') '+formatMoney(pa)+'</div>';
                
            }
        });

        
        $('#suggest_product3').on('keyup', function(){
            if($('#suggest_product3').val().length <= 0) {
                $('#report_product_id').val('');
            }
        });

        $('#suggest_product3').autocomplete({
        source: site.base_url + 'purchases_raw/get_products_raw',
        select: function (event, ui) {
            $('#report_product_id').val(ui.item.id);
        },
        minLength: 1,
        autoFocus: false,
        delay: 250,
        response: function (event, ui) {
            if (ui.content.length == 1 && ui.content[0].id != 0) {
                ui.item = ui.content[0];
                $(this).val(ui.item.label);
                $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                $(this).autocomplete('close');
                $(this).removeClass('ui-autocomplete-loading');
            }
            },
        });


        
        $('#form').hide();
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php
    echo "<a href='" . admin_url('purchases_raw/add_products_raw_form') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary' title='" . lang('add_products') . "'><i class=\"fa fa-plus\"></i> " . lang('add_products') . "</a>";
?>


<h2> <?= lang('raw_material_products'); ?> :</h2>

            <div class="row">
					<div class="col-md-6">
						
                        <select name="" id="category_id" class="form-control select">
                            <option value="">Select</option>
                            <?php foreach($categories_raw as $value) { ?>
                                <option value="<?= $value->id ?>"> <?= $value->caption ?> </option>
                            <?php } ?>
                        </select>
					</div>
					<div class="col-md-3">
						<a id="btnSearch" onclick="" class="btn btn-primary">Search</a>
					</div>
					
			</div>
            <hr>
               

    <div class="table-responsive">
        <table id="productsRawTable" class="table table-bordered table-hover table-striped reports-table">
            <thead>
                <tr>
                    <th><?php echo lang('#');?></th>
                    <th><?= lang('name'); ?></th>
                    <th><?= lang('caption_alt'); ?></th>
                    <th><?= lang('category'); ?></th>
                    <th><?= lang('price'); ?></th>
                    <th><?= lang('unit'); ?></th>
                    <th style="width:100px;"><?= lang('actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="7" class="dataTables_empty">
                        <?= lang('loading_data_from_server') ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

<script>
    $(document).ready(function () {

        fill_datatable();

        function fill_datatable(categoryId = '') {
        oTable = $('#productsRawTable').dataTable({
            "aaSorting": [[3, "desc"]],
            "bDestroy": true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('purchases_raw/getProductsRawTable') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                aoData.push({
                    "name": "categoryId",
                    "value": categoryId
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
 
            },
            "aoColumns": [null,{"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}, {"bSortable": false}]
        });
        }

        $("#btnSearch").on("click", function(e){
		e.preventDefault();
        var categoryId = $("#category_id").val();
		fill_datatable(categoryId);
	});
    });
    
</script>

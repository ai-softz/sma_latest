<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-minus-circle"></i><?= lang('return_purchase'); ?></h2>
    </div>
    <div class="box-content">
        <?php if($this->session->flashdata('error')) { ?>
            <div class="alert alert-warning">
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-6">

                
                <?php
                $attrib = ['data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-resl-form'];
                echo admin_form_open_multipart('purchases/purchase_return_form/', $attrib)
                ?>
                <div class="row">
                    <div class="col-lg-12 border-bottom" style="width: 50%;">
                    <h2 class=""><?php echo ('Return By Barcode'); ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('reference_no', 'reref'); ?>
                                <?php echo form_input('reference_no', ($_POST['reference_no'] ?? ''), 'class="form-control input-tip" id="reref"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div
                            class="fprom-group"><?php echo form_submit('purchase_return_barcode', $this->lang->line('submit'), 'id="" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?></div>
                    </div>
                </div>

                <?php echo form_close(); ?>

            </div> 
            <div class="col-lg-6">

                
                <?php
                $attrib = ['data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-resl-form'];
                echo admin_form_open_multipart('purchases/purchase_return_form/', $attrib)
                ?>
                <div class="row">
                    <div class="col-lg-12 border-bottom" style="width: 50%;">
                    <h2 class=""><?php echo ('Return By Product List'); ?></h2>
                    </div>
                </div>
                <div class="row">
                
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('reference_no', 'reref'); ?>
                                <?php echo form_input('reference_no', ($_POST['reference_no'] ?? ''), 'class="form-control input-tip" id="reref"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div
                            class="fprom-group"><?php echo form_submit('purchase_return', $this->lang->line('submit'), 'id="" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?></div>
                    </div>
                </div>

                <?php echo form_close(); ?>

            </div> 
        </div>  
    </div>    
    
</div>
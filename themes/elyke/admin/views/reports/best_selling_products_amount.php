<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script src="<?= $assets; ?>js/hc/exporting.js"></script>

<script type="text/javascript">
    $(function () {
        Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                stops: [[0, color], [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]
            };
        });
        <?php if ($best_products_amount) {
    ?>
        $('#best_item_amount').highcharts({
            chart: {type: 'column'},
            title: {text: ''},
            credits: {enabled: false},
            xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
            yAxis: {min: 0, title: {text: 'sold Amount (SAR)'}},
            legend: {enabled: false},
            // tooltip: {
		        
		        // pointFormat:  '<b>{point.y:.2f} SAR</b>',
		        
		    // },
            series: [{
                name: '<?=lang('sold Amount'); ?>',
                colorByPoint: true,
                data: [<?php
                foreach ($best_products_amount as $r) {
                        echo "['" . $r->product_name . '<br>(' . $r->product_code . ")', " . $r->subtotal . '],';
                } ?>],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'right',
                    y: -25,
                    style: {fontSize: '12px'}
                }
            }]
        });
        <?php
}  
?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Best Selling Products Amount') ; ?>
        </h2>
        <?php if (!empty($warehouses)) {
                    ?>
        <?php
                } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php echo admin_form_open('reports/best_selling_products_amount', []) ?>
                    <div class="col-sm-4">
                        <input type="text" name="stdate" value="<?= $_POST['stdate'] ?? '' ?>" class="datetime form-control" autocomplete="off" placeholder="start date">
                    </div>
                    <div class="col-sm-4">
                        <input type="text" name="enddate" value="<?= $_POST['enddate'] ?? '' ?>" class="datetime form-control"  autocomplete="off" placeholder="end date">
                    </div>
                    <div class="col-sm-4">
                            <?php echo form_submit('submit_report', $this->lang->line('submit'), 'class="btn btn-primary"'); ?> 
                    </div>
            <?php echo form_close(); ?>
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($best_products_amount)) {
                        
                        echo 'Date: '.($_POST['stdate'] ?? date('d-m-Y').' 00:00:00') . ' To ' . ($_POST['enddate'] ?? date('d-m-Y').' 23:59:59');
                        echo '. No data to show the chart! ';
                    } else {
                        echo "All time products sale.";
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="best_item_amount" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



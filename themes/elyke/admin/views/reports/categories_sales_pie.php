<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script src="<?= $assets; ?>js/hc/exporting.js"></script>

<script type="text/javascript">
    $(function () {
        
        <?php if($categories_sales) { ?>
        $('#categories_sales').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                    },
                title: {text: 'Categories Sales'},
                credits: {enabled: false},
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.2f} %'
                        }
                    }
                },
                series: [{
                    name: '<?=lang('sold'); ?>',
                    data: [<?php
                            foreach ($categories_sales as $r) {
                                // if ($r['soldQty'] > 0) {
                                echo "{ name:'" . addSlashes($r['category_name'])  . "', y: "  . $r['soldQty'] . '},';
                                // }
                            } ?>],
                }]
            });
            <?php } ?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Categories Sales') ; ?>
        </h2>
        <?php if (!empty($warehouses)) { ?>

        <?php } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php
            // print_r($_POST['year']);
                echo admin_form_open_multipart('reports/categories_sales_pie', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-3"> 
                <input type="month" id="" name="date" class="form-control" value="<?= $_POST['date'] ? $_POST['date'] : date('Y-m'); ?>" autocomplete="off">
            </div>
            <div class="col-md-3"> 
                <input type="submit" id="searchYear" value="search" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
            
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($categories_sales)) {
                        echo 'No data to show the chart!';
                    } else {
                        echo 'Year: '. ($_POST['date'] ? date('M, Y', strtotime($_POST['date'])) : date('M, Y'));
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="categories_sales" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



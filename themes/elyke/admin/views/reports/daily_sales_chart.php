<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                stops: [[0, color], [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]
            };
        });
        <?php if($daily_sales) {  ?>
        $('#monthly_sales_chart').highcharts({
            chart: {type: 'column'},
            title: {text: 'Daily Sales'},
            credits: {enabled: false},
            xAxis: {
                categories: [
                   <?php
                    for($i=1; $i<=$days_of_month; $i++) {
                    	echo "'".$i."',";
                    }
                    ?>
                ]
            },
            yAxis: {
                min: 0, 
                title: {
                    text: 'Sales Amount (SAR)'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: ' +
                    '<b>{point.y:.2f} SAR</b>',
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {enabled: false},
            series: [{
                name: '<?=lang('sales_amount'); ?>',
                colorByPoint: true,
                data: [<?php
                foreach($daily_sales as $key => $r) {
                        echo $r . ', ';
                } ?>],
                
            }]
        });
        <?php 
    } 
    ?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Daily Sales Chart') ; ?>
        </h2>
        <?php if (!empty($warehouses)) {
                    ?>
        <?php
                } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php
            // print_r($_POST['year']);
                echo admin_form_open_multipart('reports/daily_sales_chart', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-3"> 
                <select id="month" name="month" class="form-control"> 
                	<?php 
                	$months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
 					?>
 					<option value="">Select Month</option>
                    <?php foreach($months as $key=>$r) { ?>
                        <option value="<?= $key ?>" <?= ($_POST['month'] == $key) ? 'Selected' : '' ?>> <?= $r ?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-3"> 
                <input type="submit" id="searchYear" value="submit" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
            
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($daily_sales)) {
                        echo 'No data to show the chart!';
                    } else {
                        echo 'Month: '. ($_POST['month'] ? date('M',strtotime($_POST['month'])) : date('M'));
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="monthly_sales_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



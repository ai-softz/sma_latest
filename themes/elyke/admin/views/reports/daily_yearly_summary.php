<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style type="text/css">
    .dfTable th, .dfTable td {
        text-align: center;
        vertical-align: middle;
    }

    .dfTable td {
        padding: 2px;
    }

    .data tr:nth-child(odd) td {
        color: #2FA4E7;
    }

    .data tr:nth-child(even) td {
        text-align: right;
    }
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('daily_sales_summary'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <?php if (!empty($warehouses) && !$this->session->userdata('warehouse_id')) {
                ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?=lang('warehouses')?>"></i></a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?=admin_url('reports/monthly_sales_summary/0/' . $year)?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                            <li class="divider"></li>
                            <?php
                                foreach ($warehouses as $warehouse) {
                                    echo '<li><a href="' . admin_url('reports/monthly_sales_summary/' . $warehouse->id . '/' . $year) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                } ?>
                        </ul>
                    </li>
                <?php
                } ?>
                <li class="dropdown">
                    <a href="#" id="pdf_daily_yearly" class="tip" title="<?= lang('download_pdf') ?>">
                        <i class="icon fa fa-file-pdf-o"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                        <i class="icon fa fa-file-picture-o"></i>
                    </a>
                </li>
            </ul>
        </div>
        
    </div>
    <div class="box-content">
        <div class="row">
            <?php
                echo admin_form_open_multipart('', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-6"> 
                <select id="year" name="year" class="form-control"> 
                    <?php for($i = date('Y')-20; $i <= date('Y'); $i++) { ?>
                        <option value="<?= $i ?>" <?= $year == $i ? 'Selected' : '' ?>> <?= $i ?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-6"> 
                <input type="submit" id="searchYear" value="submit" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                
                <?php
                $month_arr = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                ?>
                        <?php
                            if (!empty($results)) {
                                foreach($results as $key => $sales) { 
                        ?>
                            <div class="table-responsive">
                            <h2> <?= $month_arr[$key] . ':' ?> </h2>
                                <table class="table table-bordered table-striped dfTable reports-table">
                                <thead>
                                    <tr class="bold text-center">
                                    <th style="width: 25%"> Days </th>
                                    <th style="width: 25%"> Total Without VAT </th>
                                    <th style="width: 25%"> Total VAT </th>
                                    <th style="width: 25%"> Subtotal </th>
                                    </tr>
                                </thead>
                                <tbody>

                            <?php    
                                $grand_total_sum = 0;
                                $total_sum = 0;
                                $total_tax_sum = 0;

                                foreach ($sales as $value) {
                            ?>   
                                <tr>
                                    <td> <b> 
                                        <?php
                                        echo date('d M, Y', strtotime($value->sale_date)); 
                                        ?> 
                                    </b> </td>
                                    <td> <?= number_format($value->total, 2) ?> </td>
                                    <td> <?= number_format($value->total_tax, 2) ?> </td>
                                    <td> <?= number_format($value->grand_total, 2) ?> </td>
                                </tr>
                                <?php   
                                    $total_sum += $value->total;
                                    $total_tax_sum += $value->total_tax;
                                    $grand_total_sum += $value->grand_total;
                                } 
                                ?>
                            </tbody>
                            <tfoot>
                                    <tr>
                                        <th>  Total </th>
                                        <th> <?= isset($total_sum) ? $this->sma->formatMoney($total_sum) : 0.00 ?> </th>
                                        <th> <?= isset($total_tax_sum) ? $this->sma->formatMoney($total_tax_sum) : 0.00 ?> </th>
                                        <th> <?= isset($grand_total_sum) ? $this->sma->formatMoney($grand_total_sum) : 0.00 ?> </th>
                                    </tr>
                            </tfoot>
                        </table>
                        </div>
                        <?php
                            } 
                        } 
                        ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#searchYear').click(function (event) {
            event.preventDefault();
            let year = $('#year').val();
            let url = '<?= admin_url("reports/daily_yearly_summary/". ($warehouse_id ? $warehouse_id : 0))?>' + '/' + year;
            window.location.href = url;
            return false;
        });
        $('#pdf_daily_yearly').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/daily_yearly_summary/' . ($warehouse_id ? $warehouse_id : 0) . '/' . $year . '/pdf')?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });
</script>

<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                stops: [[0, color], [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]
            };
        });
        <?php if($purchases_raw) {  ?>
        $('#purchases_raw').highcharts({
            chart: {type: 'column'},
            title: {text: 'Monthly Raw Purchases'},
            credits: {enabled: false},
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec',
                ]
            },
            yAxis: {
                min: 0, 
                title: {
                    text: 'Amount (SAR)'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: ' +
                    '<b>{point.y:.2f} SAR</b>',
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {enabled: false},
            series: [{
                name: '<?=lang('raw_purchases_amount'); ?>',
                colorByPoint: true,
                data: [<?php
                foreach ($purchases_raw as $key => $r) {
                        echo $r . ', ';
                } ?>],
                
            }]
        });
        <?php 
    } 
    ?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Monthly Raw Purchases') ; ?>
        </h2>
        <?php if (!empty($warehouses)) {
                    ?>
        <?php
                } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php
            // print_r($_POST['year']);
                echo admin_form_open_multipart('reports/monthly_raw_purchase_chart', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-6"> 
                <select id="year" name="year" class="form-control"> 
                    <?php for($i = date('Y'); $i >= date('Y')-20; $i--) { ?>
                        <option value="<?= $i ?>" <?= ($_POST['year'] == $i) ? 'Selected' : '' ?>> <?= $i ?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-6"> 
                <input type="submit" id="searchYear" value="submit" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
            
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($purchases_raw)) {
                        echo 'No data to show the chart!';
                    } else {
                        echo 'Year: '. ($_POST['year'] ? $_POST['year'] : date('Y'));
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="purchases_raw" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Monthly Sales Chart') ; ?>
        </h2>
        <?php if (!empty($warehouses)) {
                    ?>
        <?php
                } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php
            // print_r($_POST['year']);
                echo admin_form_open_multipart('reports/monthly_sales_chart', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-6"> 
                <select id="year" name="year" class="form-control"> 
                    <?php for($i = date('Y'); $i >= date('Y')-20; $i--) { ?>
                        <option value="<?= $i ?>" <?= ($_POST['year'] == $i) ? 'Selected' : '' ?>> <?= $i ?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-6"> 
                <input type="submit" id="searchYear" value="submit" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
            
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($monthly_sales)) {
                        echo 'No data to show the chart!';
                    } else {
                        echo 'Year: '. ($_POST['year'] ? $_POST['year'] : date('Y'));
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="monthly_sales_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                    		<div id="sliders">
						        <table>
						            <tr>
						                <td><label for="alpha">Alpha Angle</label></td>
						                <td><input id="alpha" type="range" min="0" max="45" value="15"/> <span id="alpha-value" class="value"></span></td>
						            </tr>
						            <tr>
						                <td><label for="beta">Beta Angle</label></td>
						                <td><input id="beta" type="range" min="-45" max="45" value="15"/> <span id="beta-value" class="value"></span></td>
						            </tr>
						            <tr>
						                <td><label for="depth">Depth</label></td>
						                <td><input id="depth" type="range" min="20" max="100" value="50"/> <span id="depth-value" class="value"></span></td>
						            </tr>
						        </table>
					    </div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>


<script type="text/javascript">
	// Set up the chart
const chart = new Highcharts.Chart({
    chart: {
        renderTo: 'monthly_sales_chart',
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
	    },
	    credits: {enabled: false},
	    xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec',
                ]
            },
	    title: {
	        text: 'Monthly Sales Report'
	    },
	    subtitle: {
	        text: 'Test options by dragging the sliders below'
	    },
	    plotOptions: {
	        column: {
	            depth: 25
	        }
	    },
	    series: [{
	    	colorByPoint: true,
	        data: [
	        	<?php
	                foreach ($monthly_sales as $key => $r) {
	                        echo $r . ', ';
	            } ?>
	        ]
	    }]
	});

	function showValues() {
	    document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
	    document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
	    document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
	}

	// Activate the sliders
	document.querySelectorAll('#sliders input').forEach(input => input.addEventListener('input', e => {
	    chart.options.chart.options3d[e.target.id] = parseFloat(e.target.value);
	    showValues();
	    chart.redraw(false);
	}));

	showValues();
</script>
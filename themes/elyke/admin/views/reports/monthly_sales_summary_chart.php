<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script src="<?= $assets; ?>js/hc/exporting.js"></script>

<script type="text/javascript">
    $(function () {
        
        <?php if($grand_total) { ?>
        $('#best_selling_products').highcharts({
                chart: {
			        type: 'column'
			    },
			    title: {
			        text: 'Monthly Sales Summary'
			    },
    			credits: {enabled: false},
			    xAxis: {
			        categories: [
			            'Jan',
			            'Feb',
			            'Mar',
			            'Apr',
			            'May',
			            'Jun',
			            'Jul',
			            'Aug',
			            'Sep',
			            'Oct',
			            'Nov',
			            'Dec'
			        ],
			        crosshair: true
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Amount (SAR)'
			        }
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			            '<td style="padding:0"><b>{point.y:.2f} SAR</b></td></tr>',
			        footerFormat: '</table>',
			        shared: true,
			        useHTML: true
			    },
			    plotOptions: {
			        column: {
			            pointPadding: 0.2,
			            borderWidth: 0
			        }
			    },
			    series: [{
			        name: 'Total Without VAT',
			        data: [
				        <?php 
				        foreach($total_without_vat as $r) {
				        	echo $r . ",";
				        } 
				        ?>
			        ]

				    }, 
				    {
			        name: 'Total VAT',
			        data: [
			        	<?php foreach($total_vat as $r) {
			        		echo $r . ",";
				        } 
				        ?>
			        	]

				    }, 
				    {
			        name: 'Subtotal',
			        data: [
						<?php foreach($grand_total as $r) {
			        		echo $r . ",";
				        } 
				        ?>
			        ]

		    	}]
            });
            <?php } ?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Monthly Sales Summary') ; ?>
        </h2>
        <?php if (!empty($warehouses)) { ?>

        <?php } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php
            // print_r($_POST['year']);
                echo admin_form_open_multipart('reports/monthly_sales_summary_chart', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-3"> 
                <select id="year" name="year" class="form-control"> 
                    <?php for($i = date('Y'); $i >= date('Y')-20; $i--) { ?>
                        <option value="<?= $i ?>" <?= ($_POST['year'] == $i) ? 'Selected' : '' ?>> <?= $i ?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-3"> 
                <input type="submit" id="searchYear" value="search" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
            
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($grand_total)) {
                        echo 'No data to show the chart!';
                    } else {
                        echo 'Year: '. ($_POST['date'] ? date('M, Y', strtotime($_POST['date'])) : date('M, Y'));
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="best_selling_products" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style type="text/css">
    .dfTable th, .dfTable td {
        text-align: center;
        vertical-align: middle;
    }

    .dfTable td {
        padding: 2px;
    }

    .data tr:nth-child(odd) td {
        color: #2FA4E7;
    }

    .data tr:nth-child(even) td {
        text-align: right;
    }
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= ('Monthly Sales Summary') . ' (' . (isset($sel_warehouse) ? $sel_warehouse->name : lang('all_warehouses')) . ')'; ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <?php if (!empty($warehouses) && !$this->session->userdata('warehouse_id')) {
    ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?=lang('warehouses')?>"></i></a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?=admin_url('reports/monthly_sales_summary/0/' . $year)?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                            <li class="divider"></li>
                            <?php
                                foreach ($warehouses as $warehouse) {
                                    echo '<li><a href="' . admin_url('reports/monthly_sales_summary/' . $warehouse->id . '/' . $year) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                } ?>
                        </ul>
                    </li>
                <?php
} ?>
                <li class="dropdown">
                    <a href="#" id="pdf" class="tip" title="<?= lang('download_pdf') ?>">
                        <i class="icon fa fa-file-pdf-o"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                        <i class="icon fa fa-file-picture-o"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('reports_calendar_text') ?></p>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped dfTable reports-table">
                        <thead>
                        <tr class="year_roller">
                            <th><a class="white" href="<?= admin_url('reports/monthly_sales_summary/' . (isset($warehouse_id) ? $warehouse_id : 0) . '/' . ($year - 1)); ?>">&lt;&lt;</a></th>
                            <th colspan="10"> <?php echo $year; ?></th>
                            <th><a class="white" href="<?= admin_url('reports/monthly_sales_summary/' . (isset($warehouse_id) ? $warehouse_id : 0) . '/' . ($year + 1)); ?>">&gt;&gt;</a></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="bold text-center">
                                <td> Months </td>
                                <td> Total Without VAT </td>
                                <td> Total VAT </td>
                                <td> Subtotal </td>
                            </td>
                            <?php
                            if (!empty($sales)) {

                                $grand_total_sum = 0;
                                $total_sum = 0;
                                $total_tax_sum = 0;

                                foreach ($sales as $value) {

                                $month_arr = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];    
                            ?>   
                                <tr>
                                    <td> <b> <?= $month_arr[$value->date] ?> </b> </td>
                                    <td> <?= number_format($value->total, 2) ?> </td>
                                    <td> <?= number_format($value->total_tax, 2) ?> </td>
                                    <td> <?= number_format($value->grand_total, 2) ?> </td>
                                </tr>
                            <?php   
                                    $total_sum += $value->total;
                                    $total_tax_sum += $value->total_tax;
                                    $grand_total_sum += $value->grand_total;
                                } 
                            } 
                            ?>
                        </tbody>
                        <tfoot>
                                <tr>
                                    <th>  Total </th>
                                    <th> <?= $this->sma->formatMoney($total_sum) ?> </th>
                                    <th> <?= $this->sma->formatMoney($total_tax_sum) ?> </th>
                                    <th> <?= $this->sma->formatMoney($grand_total_sum) ?> </th>
                                </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/monthly_sales_summary/' . ($warehouse_id ? $warehouse_id : 0) . '/' . $year . '/pdf')?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });
</script>

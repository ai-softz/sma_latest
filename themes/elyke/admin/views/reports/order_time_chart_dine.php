<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?php echo 'Order Traffic Dine In / Take Away'; ?>
        </h2>
        <?php if (!empty($warehouses)) {
                    ?>
        <div class="box-icon">
            <ul class="btn-tasks">
                
            </ul>
        </div>
        <?php
                } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        	<div class="row">
        		<?php echo admin_form_open('reports/order_time_chart_dine', []) ?>
                        <div class="col-sm-4">
                            <input type="text" name="date" value="<?= $_POST['date'] ?? date('d-m-Y') ?>" class="date form-control" id="date" autocomplete="off">
                        </div>
                        <div class="col-sm-4">
                             <?php echo form_submit('submit_report', $this->lang->line('submit'), 'class="btn btn-primary"'); ?> 
                        </div>
                <?php echo form_close(); ?>
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?= ($_POST['date'] ?? date('d-m-Y')) ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="order_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">

        
        <?php if ($record_products) {
    ?>
        Highcharts.chart('order_chart', {
            chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Order Traffic Dine-In/Take Away'
		    },
    		credits: {
                enabled: false
            },
		    xAxis: {
		        categories: [
		            <?php 
		            	foreach ($record_products as $r) {
		            		echo "'".$r['time']."',";
		            	}
		            ?>
		        ],
		        crosshair: true
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Numbers'
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y} </b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.2,
		            borderWidth: 0
		        }
		    },
		    series: [
		    	{
		        name: 'Dine-In',
		        data: [
			        <?php 
			        foreach($record_products as $r) {
			        	echo $r['dine_in'] . ",";
			        } 
			        ?>
		        ]
		    	}, 
		    	{
		        name: 'Take Away',
		        data: [
		        	<?php foreach($record_products as $r) {
		        		echo $r['take_away'] . ",";
			        } 
			        ?>
		        	]
		    	}
		    ]
        });
        <?php
		} 
        ?>
       
   
</script>



<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?php echo 'Order Traffic Time'; ?>
        </h2>
        <?php if (!empty($warehouses)) {
                    ?>
        <div class="box-icon">
            <ul class="btn-tasks">
                
            </ul>
        </div>
        <?php
                } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        	<div class="row">
        		<?php echo admin_form_open('reports/order_time_charts_drill', []) ?>
                        <div class="col-sm-4">
                            <input type="text" name="date" value="<?= $_POST['date'] ?? date('d-m-Y') ?>" class="date form-control" id="date" autocomplete="off">
                        </div>
                        <div class="col-sm-4">
                             <?php echo form_submit('submit_report', $this->lang->line('submit'), 'class="btn btn-primary"'); ?> 
                        </div>
                <?php echo form_close(); ?>
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?= ($_POST['date'] ?? date('d-m-Y')) ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="order_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">

        
        <?php if ($records_hours) {
    ?>
        // $('#order_chart').highcharts({
        Highcharts.chart('order_chart', {
            chart: {
            type: 'column'
            },
            credits: {
                enabled: false
            },
            title: {
                align: 'left',
                text: 'Daily Order Traffic'
            },
            subtitle: {
                align: 'left',
                text: 'Click the columns to view the top ordered items.'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total orders'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
            },
            series: [{
                name: '<?=lang('orders'); ?>',
                colorByPoint: true,
                data: [
                <?php
                foreach ($records_hours as $r) {
                        echo "{ name:'" . $r['time'] . "', y: " . intval($r['quantity']) .", ";
                        if($r['quantity'] > 0) {
                            echo "drilldown: '" . $r['time'] . "'},";
                        } else {
                            echo "drilldown: null". "},";
                        }
                } ?>
                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'right',
                    y: -25,
                    style: {fontSize: '12px'}
                }
            }],
            drilldown: {
	        breadcrumbs: {
	            position: {
	                align: 'right'
	            }
	        },
	        series: [
	        	<?php
                foreach ($record_products as $r) {
                        echo "{ 
                            name:'" . $r['time'] . "',". 
                        	"id: '" . ($r['time']) ."', ";
                        if(!empty($r['products'])) {
                        	echo  "data:[";
                        foreach ($r['products'] as $value) {
                        		echo "[". '"' . $value['product_name'] . "(" . $value['product_code'] . ")" . '"' . "," . $value['quantity']. "],";
                        	}
                            echo  "]";
                        }	
                        echo "},";	
                } ?>	
	        ]

	       }
        });
        <?php
		} 
        ?>
       
   
</script>



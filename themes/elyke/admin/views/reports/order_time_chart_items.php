<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                stops: [[0, color], [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]
            };
        });
        <?php if ($records_hours) {
    ?>
        $('#order_chart').highcharts({
            chart: {type: 'column'},
            title: {text: ''},
            credits: {enabled: false},
            xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
            yAxis: {min: 0, title: {text: ''}},
            legend: {enabled: false},
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <br/>'
            },
            series: [{
                name: '<?=lang('orders'); ?>',
                colorByPoint: true,
                data: [<?php
                foreach ($records_hours as $r) {
                        echo "['" . $r['time'] . "<br>(" . $r['product_name'] . ")', " . $r['quantity'] . '],';
                } ?>],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'right',
                    y: -25,
                    style: {fontSize: '12px'}
                }
            }]
        });
        <?php
			} 
        ?>
       
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?php echo 'Order Traffic Time'; ?>
        </h2>
        <?php if (!empty($warehouses)) {
                    ?>
        <div class="box-icon">
            <ul class="btn-tasks">
                
            </ul>
        </div>
        <?php
                } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        	<div class="row">
        		<?php echo admin_form_open('reports/order_time_chart_items', []) ?>
                        <div class="col-sm-4">
                            <input type="text" name="date" value="<?= $_POST['date'] ?? date('d-m-Y') ?>" class="date form-control" id="date" autocomplete="off">
                        </div>
                        <div class="col-sm-4">
                             <?php echo form_submit('submit_report', $this->lang->line('submit'), 'class="btn btn-primary"'); ?> 
                        </div>
                <?php echo form_close(); ?>
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?= ($_POST['date'] ?? date('d-m-Y')) ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="order_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



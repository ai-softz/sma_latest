<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        
        <?php if($monthly_purchases && $monthly_sales) { 
            
            ?>
        $('#monthly_sales_chart_area').highcharts({
            chart: {
                type: 'areaspline'
            },
            title: {
                text: 'Monthly Purchases vs Sales'
            },
            
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                plotBands: [{ // visualize the weekend
                    from: 4.5,
                    to: 6.5,
                    color: 'rgba(68, 170, 213, .2)'
                }]
            },
            yAxis: {
                title: {
                    text: 'Sales'
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ' amount'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            series: [
                {
	                name: 'Purchases',
	                data: [
                        <?php
                        foreach ($monthly_purchases as $r) {
                            echo  $r . ',';
                        } ?>
	                ]
                },
                {
                    name: 'Sales',
                    data: [
                        <?php
                        foreach ($monthly_sales as $r) {
                            echo  $r . ',';
                        } ?>
                    ]
                }
            ]
        });
        <?php } ?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Monthly Purchases vs Sales') ; ?>
        </h2>
        <?php if (!empty($warehouses)) { ?>

        <?php } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php
            // print_r($_POST['year']);
                echo admin_form_open_multipart('reports/sales_vs_purchases_chart', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-6"> 
                <select id="year" name="year" class="form-control"> 
                    <?php for($i = date('Y'); $i >= date('Y')-20; $i--) { ?>
                        <option value="<?= $i ?>" <?= ($_POST['year'] == $i) ? 'Selected' : '' ?>> <?= $i ?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-6"> 
                <input type="submit" id="searchYear" value="submit" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
            
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($monthly_sales) && empty($monthly_purchases)) {
                        echo 'No data to show the chart!';
                    } else {
                        echo 'Year: '. $_POST['year'];
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="monthly_sales_chart_area" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



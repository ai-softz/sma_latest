<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        
        <?php 
        if($yearly_sales) { 
        ?>
        $('#yearly_sales_chart').highcharts({
        title: {
            text: 'Yearly Sales and Purchases'
        },

        yAxis: {
            title: {
                text: 'Amount'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: 'Range:'
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: <?= date('Y') - 7 ?>
            }
        },

        series: [{
            name: 'Sales',
            data: [
                <?php 
                    foreach($yearly_sales as $r) {
                        echo $r . ", ";
                    }
                ?>
            ]
        }, {
            name: 'Purchases',
            data: [
                <?php 
                    foreach($yearly_purchases as $r) {
                        echo $r . ", ";
                    }
                ?>
            ]
        }, ],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

        });
        <?php 
    } 
    ?>

    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Yearly Sales vs Purchases') ; ?>
        </h2>
        <?php if (!empty($warehouses)) { ?>

        <?php } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            
            
        </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($best_sell_products)) {
                        echo 'No data to show the chart!';
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="yearly_sales_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



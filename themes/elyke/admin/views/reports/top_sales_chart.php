<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        
        <?php 
        if($top_sales) { 
        ?>
        $('#top_sales_chart').highcharts({
            chart: {
            type: 'bar'
            },
            title: {
                text: 'Top 10 Sales Report'
            },
            xAxis: {
                categories: [
                    <?php
                    foreach ($top_sales as $r) {
                        echo "'".addSlashes($r->date). "'" . ",";
                    } 
                    ?>
                ],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Sales Amount (SAR)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' SAR'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
    
            credits: {
                enabled: false
            },
            series: [{
                colorByPoint: true,
                data: [
                    <?php
                        foreach ($top_sales as $r) {
                            echo  $r->grand_total . ',';
                    } ?>

                ]
            }, ]
        });
        <?php 
    } 
    ?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Top 10 Sales') ; ?>
        </h2>
        <?php if (!empty($warehouses)) { ?>

        <?php } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        <div class="row">
            <?php
            // print_r($_POST['year']);
                echo admin_form_open_multipart('reports/top_sales_chart', ['role' => 'form', 'id' => '']); 
            ?> 
            <div class="col-md-3"> 
                <input type="month" id="" name="date" class="form-control" value="<?= $_POST['date'] ? $_POST['date'] : date('Y-m'); ?>" autocomplete="off">
            </div>
            <div class="col-md-3"> 
                <input type="submit" id="searchYear" value="search" class="btn btn-primary">
            </div>
            <?php echo form_close(); ?>
            
            </div>
            <br>
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($top_sales)) {
                        echo 'No data to show the chart!';
                    } else {
                    	if($_POST['date'] && $_POST['date'] != '1970-01-01') {
                        	echo 'Year: '. date('M, Y', strtotime($_POST['date']));
                    	} else {
                    		echo 'All time sales';
                    	}

                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="top_sales_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



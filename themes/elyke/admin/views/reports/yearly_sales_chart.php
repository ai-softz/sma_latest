<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        
        <?php 
        if($yearly_sales) { 
        ?>
        $('#yearly_sales_chart').highcharts({
            chart: {
            type: 'bar'
            },
            title: {
                text: 'Yearly Sales Report'
            },
            xAxis: {
                categories: [
                    <?php
                    foreach ($yearly_sales as $key=>$r) {
                        echo "'".$key. "'" . ",";
                    } 
                    ?>
                ],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Sales Amount (SAR)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' SAR'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
    
            credits: {
                enabled: false
            },
            series: [{
                colorByPoint: true,
                data: [
                    <?php
                        foreach ($yearly_sales as $r) {
                            echo  $r . ',';
                    } ?>

                ]
            }, ]
        });
        <?php 
    } 
    ?>
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-line-chart"></i>
            <?= ('Categories Sales ') ; ?>
        </h2>
        <?php if (!empty($warehouses)) { ?>

        <?php } ?>
    </div>
    <div class="box-content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
        
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><?php 
                    if(empty($yearly_sales)) {
                        echo 'No data to show the chart!';
                    } else {
                    } 
                    ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="yearly_sales_chart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>



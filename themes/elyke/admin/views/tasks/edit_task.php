<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>


<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_task'); ?></h4>
        </div>
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form']; 
        echo admin_form_open_multipart('tasks/edit_task', $attrib); ?>

        <div class="modal-body">
            <p> </p>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang('subject', 'subject'); ?>
                        <?= form_input('name', $task->name, 'class="form-control" id="name" required="required"'); ?>
                         <input type="hidden" name="task_id" value="<?php echo $task->id; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('start_date', 'startdate'); ?>
                        <input type="" name="startdate" class="form-control input-tip date" id="start_date" value="<?php echo date('d-m-Y', strtotime($task->startdate)) ?>" placeholder="dd-mm-YY" autocomplete="off"> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('due_date', 'duedate'); ?>
                        <input type="" name="duedate" class="form-control input-tip date" id="due_date" value="<?php echo date('d-m-Y', strtotime($task->duedate)) ?>" placeholder="dd-mm-YY" autocomplete="off"> 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('priority', 'priority'); ?>
                        <?php
                        $task_priorities  = array(
                            array('id' => '1', 'name' => lang('task_priority_low')),
                            array('id' => '2', 'name' => lang('task_priority_medium')),
                            array('id' => '3', 'name' => lang('task_priority_high')),
                            array('id' => '4', 'name' => lang('task_priority_urgent')),
                            
                        );
                        $priorities_drop[''] = lang('select') . ' ' . lang('price_group');
                        foreach ($task_priorities as $row) {
                            $priorities_drop[$row['id']] = $row['name'];
                        }
                        ?>
                        <select name="priority" class="form-control select">
                            <option value="">Select Priority</option>
                                <?php foreach ($task_priorities as $row) { ?>
                                <option value="<?php echo $row['id'] ?>" <?php echo $task->priority == $row['id'] ? 'Selected' : ''?>> <?php echo $row['name']; ?> </option>
                                <?php } ?>
                           
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('task_repeat_every', 'task_repeat_every'); ?>
                        <select name="repeat_every" id="repeat_every" class="form-control select" style="width:100%;">
                           <option value="">Nothing Selected</option>
                           <option value="1-day" <?php if(isset($task) && $task->repeat_every == 1 && $task->recurring_type == 'day'){echo 'selected';} ?>><?php echo lang('day'); ?></option>
                           <option value="1-week" <?php if(isset($task) && $task->repeat_every == 1 && $task->recurring_type == 'week'){echo 'selected';} ?>><?php echo lang('week'); ?></option>
                           <option value="2-week" <?php if(isset($task) && $task->repeat_every == 2 && $task->recurring_type == 'week'){echo 'selected';} ?>>2 <?php echo lang('weeks'); ?></option>
                           <option value="1-month" <?php if(isset($task) && $task->repeat_every == 1 && $task->recurring_type == 'month'){echo 'selected';} ?>>1 <?php echo lang('month'); ?></option>
                           <option value="2-month" <?php if(isset($task) && $task->repeat_every == 2 && $task->recurring_type == 'month'){echo 'selected';} ?>>2 <?php echo lang('months'); ?></option>
                           <option value="3-month" <?php if(isset($task) && $task->repeat_every == 3 && $task->recurring_type == 'month'){echo 'selected';} ?>>3 <?php echo lang('months'); ?></option>
                           <option value="6-month" <?php if(isset($task) && $task->repeat_every == 6 && $task->recurring_type == 'month'){echo 'selected';} ?>>6 <?php echo lang('months'); ?></option>
                           <option value="1-year" <?php if(isset($task) && $task->repeat_every == 1 && $task->recurring_type == 'year'){echo 'selected';} ?>>1 <?php echo lang('year'); ?></option>
                           
                        </select>
                    </div>
                </div>
            </div>
           
           <div id="cycles_wrapper" class="<?php if(!isset($task) || (isset($task) && $task->recurring == 0)){echo ' hide';}?>">
              <?php $value = (isset($task) ? $task->cycles : 0); ?>
              <div class="form-group recurring-cycles">
                 <label for="cycles"><?php echo lang('recurring_total_cycles'); ?>
                 <?php if(isset($task) && $task->total_cycles > 0){
                    echo '<small>' . ('cycles_passed') . '</small>'; //
                    }
                    ?>
                 </label>
                 <div class="input-group">
                    <input type="number" class="form-control"<?php if($value == 0){echo ' disabled'; } ?> name="cycles" id="cycles" value="<?php echo $value; ?>" <?php if(isset($task) && $task->total_cycles > 0){echo 'min="'.($task->total_cycles).'"';} ?>>
                    <div class="input-group-addon">
                       <div class="">
                          <input type="checkbox"<?php if($value == 0){echo ' checked';} ?> id="unlimited_cycles">
                          <label for="unlimited_cycles"><?php echo ('Infinity'); ?></label>
                       </div>
                    </div>
                 </div>
              </div>
           </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo ('tags'); ?></label>
                        <div id="inputTagsWrapper">
                         
                        <input type="text" class="tagsinput" id="tags" name="tags"  value="<?php echo $tags; ?>" data-role="tagsinput">
                      </div>
                   </div>
              
                </div>
            </div>
            <hr />

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang('description', 'description'); ?>
                        <?= form_textarea('description', $task->description, 'class="form-control" id="description"'); ?>
                    </div>
                </div>
            </div>
            
        <div class="modal-footer">
            <?php echo form_submit('add_task', lang('edit_task'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script src="<?php echo base_url() ?>assets/js/typeahead.bundle.js"></script>

<script>
  $(document).ready(function(){
  $(document).on('', '', function(){
        
        console.log('clicked!');
          $.ajax({
            url: site.base_url + "tasks/tags_list",
            method: 'POST',
            // data: { taskid: taskid },
            success: function(data){
              //alert(data);
              $('#inputTagsWrapper').html(data);
            }

          });
      });

    // $('#tags').tagsinput('input').typeahead({
    //         local: colors,
    //         prefetch: site.base_url + "tasks/tags_list"
    //     }).bind('typeahead:selected', $.proxy(function (obj, datum) {
    //         this.tagsinput('add', datum.value);
    //         this.tagsinput('input').typeahead('setQuery', '');
    //     }, elt));


  // var countries = new Bloodhound({
  //   datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  //   queryTokenizer: Bloodhound.tokenizers.whitespace,
  //   prefetch: {
  //   url: site.base_url + "tasks/tags_list",
  //   filter: function(list) {
  //     return $.map(list, function(name) {
  //     return { name: name }; });
  //   }
  //   }
  // });
  // countries.initialize();

  // $('#tags-input').tagsinput({
  //   typeaheadjs: {
  //   name: 'countries',
  //   displayKey: 'name',
  //   valueKey: 'name',
  //   source: countries.ttAdapter()
  //   }
  // });




  });
</script>
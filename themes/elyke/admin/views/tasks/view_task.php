<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header" style="background-color: #b5d9e6">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $task->name; ?></h4>
        </div>
        
        <div class="modal-body">
            <div class="row">
        			<div class="col-md-8 task-single-col-left">

              <div class="clearfix"></div>
               <p class="no-margin pull-left" style="margin-right:5px<?php //echo 'margin-'.(is_rtl() ? 'left' : 'right').':5px !important'; ?>">
                <?php 
                  
                  if($task->status != Tasks_model::STATUS_COMPLETE){ 
                      $mark_completestyle='style="display:block"';
                      $unmark_completestyle='style="display:none"';
                  } else if($task->status == Tasks_model::STATUS_COMPLETE){ 
                      $mark_completestyle='style="display:none"';
                      $unmark_completestyle='style="display:block"';
                  } 
                  ?>
                  <a id="mark-complete" <?=$mark_completestyle?>  data-taskid="<?php echo $task->id; ?>" class="btn btn-info" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_single_mark_as_complete'); ?>">
                  <i class="fa fa-check"></i>
                  </a>
                  
                  <a id="unmark-complete" <?=$unmark_completestyle?> data-taskid="<?php echo $task->id; ?>" class="btn btn-default" id="unmark-complete" autocomplete="off" data-loading-text="<?php echo lang('wait_text'); ?>" data-toggle="tooltip" title="<?php echo lang('task_unmark_as_complete'); ?>">
                  <i class="fa fa-check"></i>
                  </a>
               </p>
              
                <p class="no-margin pull-left" data-toggle="tooltip" data-title="<?php echo lang('task_start_timer_only_assignee'); ?>">
                   <?php
                    $task_timer=$this->db->get_where('taskstimers', array('task_id'=>$task->id, 'status'=>'incomplete'))->row();
                    $timer_status = '';
                    if(!empty($task_timer)) {
                       if($task_timer->status == 'incomplete') {
                        $timer_status = 'true';
                      }
                    }  
                   ?>
                   <?php 
                    if($timer_status != 'true') {
                        $start_timerstyle='style="display:block"';
                        $stop_timerstyle='style="display:none"';
                    } else { 
                        $start_timerstyle='style="display:none"';
                        $stop_timerstyle='style="display:block"';
                    } 
                    ?>

                   <a id="start_timer" <?= $start_timerstyle ?> class="mbot10 btn<?php if($task->status == Tasks_model::STATUS_COMPLETE){echo ' disabled btn-default';}else {echo ' btn-success';} ?>" onclick="timerFunction(<?php echo $task->id; ?>); return false;">
                   <i class="fa fa-clock-o"></i> <?php echo ' ' . ('Start Clock'); ?>
                   </a>
                   <a id="stop_timer" <?= $stop_timerstyle ?> class="mbot10 btn<?php if($task->status == Tasks_model::STATUS_COMPLETE){echo ' disabled btn-default';}else {echo ' btn-danger';} ?>" onclick="stopTimerFunction(<?php echo $task->id; ?>); return false;">
                   <i class="fa fa-clock-o"></i> <?php echo ' ' . ('Stop Clock'); ?>
                   </a>
                </p>
                <p class="no-margin pull-left bg-primary" style="margin:  5px 5px; padding:2px; font-weight: 100" id="clock_time"> 
                  <?php 
                    if($task_timer) {
                      echo 'Clock Started: ' . date('d-m-Y H:i:s', strtotime($task_timer->in_time));
                    } 
                  ?>
                </p>
               <div class="clearfix"></div>
               <hr />
               <div class="clearfix"></div>
               <h4 class="th font-medium mbot15 pull-left"><?php echo lang('task_view_description'); ?></h4>
               <a href="#" onclick="edit_task_inline_description(); return false;" class="pull-left mtop10 mleft5 font-medium-xs"><i class="fa fa-pencil-square-o"></i></a>
                <div class="clearfix"></div>
                 <?php if(!empty($task->description)){
                    echo '<div id="description_view">' . $task->description . '</div>';
                    echo '<div id="task_description"  style="display:none"><textarea  class="form-control" rows="3">' .$task->description .'</textarea></div>';
                    echo '<input id="taskid_description" type="hidden" value="'.$task->id.'">';
                    } else {
                    echo '<div class="no-margin tc-content task-no-description" id="task_view_description"><span class="text-muted">' . lang('task_no_description') . '</span></div>';
                    } ?>
                 <div class="clearfix"></div>
                 
                <hr />
             <!-- Checklists items add/edit/delete -->
               <div id="checklist_area">  
                  <div class="container">  
                     <h4 class="bold chk-heading th font-medium pull-left">Checklist Items</h4>
                      <div class="table-responsive">  
                           <input type="hidden" name="taskid" id="taskid" value="<?php echo $task->id ?>">
                          <div id="live_data">

                          </div> 

                      </div>  
                 </div>  
                </div>

                    <hr />
         <a href="#" id="taskCommentSlide" onclick="slideToggle('.tasks-comments'); return false;">
            <h4 class="mbot20 font-medium"><?php echo lang('task_comments'); ?></h4>
         </a>
         <div class="tasks-comments inline-block full-width simple-editor"<?php //if(count($task_comments) == 0){echo ' style="display:block"';} ?>>
            <?php echo form_open_multipart(admin_url('tasks/add_task_comment'),array('id'=>'task-comment-form','class'=>'dropzone dropzone-manual','style'=>'min-height:auto;background-color:#fff;')); ?>
 
            <textarea name="comment" placeholder="<?php echo lang('task_single_add_new_comment'); ?>" id="task_comment" rows="3" class="form-control ays-ignore"></textarea><br>
            <input type="file" name="userfile" id="userfile" size="20" />
            <div class="dropzone-task-comment-previews dropzone-previews"></div>
            
            <input type="hidden" name="taskid" value="<?php echo $task->id ?>"> <br>
             <input type="submit" name="upload" id="upload" value="Add Comment" class="btn btn-info" /> 
            <?php echo form_close(); ?>
            <div class="clearfix"></div>
            <br>
            <?php if(count($task_comments) > 0){echo '<hr />';} ?>
            <div id="task-comments" class="mtop10">
               <?php
                  $comments = '';
                  $len = count($task_comments);
                  $i = 0;
                  foreach ($task_comments as $comment) {
                    $comments .= '<div id="comment_'.$comment['id'].'" data-commentid="' . $comment['id'] . '" data-task-attachment-id="'.$comment['file_id'].'" class="tc-content task-comment'.(strtotime($comment['dateadded']) >= strtotime('-16 hours') ? ' highlight-bg' : '').'">';
                    
                    if($comment['staffid'] != 0){
                     $comments .= '<a href="' . $comment['first_name'].' '.$comment['last_name']. '" target="_blank">'
                      
                   . '</a>';
                  } 

                  if ($comment['staffid'] == $this->session->userdata('user_id') ) {
                     $comment_added = strtotime($comment['dateadded']);
                     $minus_1_hour = strtotime('-1 hours');
                     if($this->Admin){
                       $comments .= '<span class="pull-right"><a href="#" onclick="remove_task_comment(' . $comment['id'] . '); return false;"><i class="fa fa-times text-danger"></i></span></a>';
                       $comments .= '<span class="pull-right mright5"><a href="#" onclick="edit_task_comment(' . $comment['id'] . '); return false;"><i class="fa fa-pencil-square-o"></i></span></a>';
                    }
                  }

                  $comments .= '<div class="media-body comment-wrapper">';
                  $comments .= '<div class="mleft40">';

                  if($comment['staffid'] != 0){
                   $comments .= $comment['first_name'].' '.$comment['last_name'] . '<br />';
                  } 
                  $comments .= '<small class="border-bottom">' . date('d M, Y h:i:sa',strtotime($comment['dateadded'])) . '</small>';

                  $comments .= '<div data-edit-comment="'.$comment['id'].'" class="edit-task-comment" style="display:none"><textarea rows="5" id="task_comment_'.$comment['id'].'" class="ays-ignore form-control">'.str_replace('[task_attachment]', '', $comment['content']).'</textarea>
                  <div class="clearfix mtop20"></div>
                  <button type="button" class="btn btn-info pull-right" onclick="save_edited_comment('.$comment['id'].','.$task->id.')">'.lang('submit').'</button>
                  <button type="button" class="btn btn-default pull-right mright5" onclick="cancel_edit_comment('.$comment['id'].')">'.lang('cancel').'</button>
                  </div>';
                  if($comment['file_id'] != 0){
                  $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$attachments_data[$comment['file_id']],$comment['content']);
                  // Replace lightbox to prevent loading the image twice
                  $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-attachment-comment-'.$comment['id'].'"',$comment['content']);
                  } 
                  /*else if(count($comment['attachments']) > 0 && isset($comments_attachments[$comment['id']])) {
                   $comment_attachments_html = '';
                   foreach($comments_attachments[$comment['id']] as $comment_attachment) {
                       $comment_attachments_html .= trim($comment_attachment);
                   }
                   $comment['content'] = str_replace('[task_attachment]','<div class="clearfix"></div>'.$comment_attachments_html,$comment['content']);
                   // Replace lightbox to prevent loading the image twice
                   $comment['content'] = str_replace('data-lightbox="task-attachment"','data-lightbox="task-comment-files-'.$comment['id'].'"',$comment['content']);
                   $comment['content'] .='<div class="clearfix"></div>';
                   $comment['content'] .='<div class="text-center download-all">
                   <hr class="hr-10" />
                   <a href="'.admin_url('tasks/download_files/'.$task->id.'/'.$comment['id']).'" class="bold">'.lang('download_all').' (.zip)
                   </a>
                   </div>';
                  }*/
                  $comments .= '<div id="comment_view' . $comment['id'] .'" class="comment-content mtop10">'.($comment['content']) . '</div>';
                  $comments .= '<div id="" class=""> <a target="_blank" href="'.base_url().'assets/uploads/task_files/'.($comment['file_name']) . '">' .($comment['file_name']) . '</a></div>';
                  $comments .= '</div>';
                  if ($i >= 0 && $i != $len - 1) {
                     $comments .= '<hr class="task-info-separator" />';
                  }
                  $comments .= '</div>';
                  $comments .= '</div>';
                  $i++;
                  }
                  echo $comments;
                  
                  ?>
            </div>
         </div>


      		    </div> 
              <!-- End .col-md-8 -->
              <div class="col-md-4"  style="background-color: #f0f5f7">
                <h4 class="task-info-heading"><?php echo lang('task_info'); ?>
                  <?php
                     if($task->recurring == 1){
                        echo '<span class="label label-info inline-block mleft5">'.lang('recurring_task').'</span>';
                     }
                     ?>
                </h4>
                <div class="clearfix"></div>
                 <h5 class="no-mtop task-info-created">
                    <small class="text-dark"><?php echo lang('task_created_at').'<span class="text-dark">: '.($task->dateadded).'</span>'; ?></small>
                 </h5>
                 <hr class="task-info-separator" />

                <div class="task-info task-status task-info-status">
                  <h5>
                    <div class="dropdown dropdown_status">
                     <i class="fa fa-<?php if($task->status == Tasks_model::STATUS_COMPLETE){echo 'star';} else if($task->status == 1){echo 'star-o';} else {echo 'star-half-o';} ?> pull-left task-info-icon fa-fw fa-lg"></i><?php echo lang('task_status'); ?>:
                     <?php
                     $task_status= array(
                                      array('id' => '1', 'name' => 'Not Started'),
                                      array('id' => '2', 'name' => 'Awaiting Feedback'),
                                      array('id' => '3', 'name' => 'Testing'),
                                      array('id' => '4', 'name' => 'In Progress'),
                                      array('id' => '5', 'name' => 'Complete'),
                                    );    
                     ?>
                     <?php if($task->status != Tasks_model::STATUS_COMPLETE) { ?> 
                     <?php 
                     //echo $task->status; 
                     foreach ($task_status as $status) {
                              if ($status['id'] == $task->status) {
                          ?>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $status['name']; ?> </a>
                          <?php
                              }
                          }
                          ?>

                       <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <?php
                                // show priority dropdown
                                foreach($task_status as $status){ ?>
                                <?php if($task->status != $status['id']){ ?>
                                <li  class="border-bottom">
                                   <a href="#" onclick="task_mark_as(<?php echo $status['id']; ?>,<?php echo $task->id; ?>); return false;">
                                   <?php echo $status['name']; ?>
                                   </a>
                                </li>
                                <?php } ?>
                                <?php } ?>
                           </ul>   
                           
                    </div>
                     <?php } else { ?>
                     
                     <?php 
                     foreach ($task_status as $status) {
                              if ($status['id'] == $task->status) {
                          ?>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $status['name']; ?> </a>
                          <?php
                              }
                          }
                     ?>

                     <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <?php
                                // show priority dropdown
                                foreach($task_status as $status){ ?>
                                <?php if($task->status != $status['id']){ ?>
                                <li  class="border-bottom">
                                   <a href="#" onclick="task_mark_as(<?php echo $status['id']; ?>,<?php echo $task->id; ?>); return false;">
                                   <?php echo $status['name']; ?>
                                   </a>
                                </li>
                                <?php } ?>
                                <?php } ?>
                           </ul>
                    
                     <?php } ?>
                    
                    </h5>
                  </div>

                  <div class="task-info task-single-inline-wrap task-info-start-date">
                    <h5><i class="fa task-info-icon fa-fw fa-lg fa-calendar-plus-o pull-left fa-margin"></i>
                       <?php echo lang('task_single_start_date'); ?>:
                       <?php if($task->status !=5) { ?>
                       <input type="" id="task-single-startdate" data-taskid="<?php echo $task->id ?>" name="startdate" class="date pointer task-single-inline-field"value="<?php echo date('d-m-Y',strtotime($task->startdate)); ?>" autocomplete="off"> 
                       <?php } else { ?>
                       <?php echo date('d-m-Y',strtotime($task->startdate)); ?>
                       <?php } ?>
                    </h5>
                  </div>
                  <div class="task-info task-single-inline-wrap task-info-due-date">
                    <h5>
                     <i class="fa fa-calendar-check-o task-info-icon fa-fw fa-lg pull-left"></i>
                     <?php echo lang('task_single_due_date'); ?>:
                     <?php if($task->status !=5) { ?>
                     <input type="" id="task-single-duedate" data-taskid="<?php echo $task->id ?>" name="duedate" class="date pointer task-single-inline-field" value="<?php echo date('d-m-Y',strtotime($task->duedate)); ?>" autocomplete="off"> 
                     <?php } else { ?>
                     <?php echo date('d-m-Y',strtotime($task->duedate)); ?>
                     <?php } ?>
                  </h5>
                  </div>

                  <div class="task-info task-status task-info-status">
                  <h5>
                    <div class="dropdown dropdown_priority">
                     <i class="fa task-info-icon fa-fw fa-lg pull-left fa-bolt"></i>
                       <?php 
                        $task_priorities  = array(
                            array('id' => '1', 'name' => lang('task_priority_low')),
                            array('id' => '2', 'name' => lang('task_priority_medium')),
                            array('id' => '3', 'name' => lang('task_priority_high')),
                            array('id' => '4', 'name' => lang('task_priority_urgent')),
                          );
                        ?>
                       <?php echo lang('task_single_priority'); ?>: 
                       <?php if($task->status != Tasks_model::STATUS_COMPLETE) { ?>
                          <?php 
                          // show priority name
                          
                          foreach ($task_priorities as $priority) {
                              if ($priority['id'] == $task->priority) {
                          ?>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $priority['name']; ?> </a>
                          <?php
                              }
                          }
                          ?>
                           <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <?php
                                // show priority dropdown
                                foreach($task_priorities as $priority){ ?>
                                <?php if($task->priority != $priority['id']){ ?>
                                <li  class="border-bottom">
                                   <a href="#" onclick="task_change_priority(<?php echo $priority['id']; ?>,<?php echo $task->id; ?>); return false;">
                                   <?php echo $priority['name']; ?>
                                   </a>
                                </li>
                                <?php } ?>
                                <?php } ?>
                           </ul>
                     </div>
                     <?php } else { ?>
                     <span style="">
                       <?php 
                       foreach ($task_priorities as $priority) {
                                if ($priority['id'] == $task->priority) {
                            ?>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" > <?php echo $priority['name']; ?> </a>
                            <?php
                                }
                        }
                       ?>
                       </span>
                     <?php } ?>
                    </h5>
                  </div>
                  
                  <?php 
                  $query = $this->db->get_where('taskstimers', array('task_id'=>$task->id,'status'=>'complete'));
                  $results_time = $query->result();

                  $seconds = 0;
                  foreach($results_time as $r) {
                      $time_diff = strtotime($r->out_time) - strtotime($r->in_time);
                      $seconds += $time_diff;
                  }
                  ?>

                  <div class="task-info task-info-user-logged-time">
                      <h5>
                         <i class="fa fa-asterisk task-info-icon fa-fw fa-lg" aria-hidden="true"></i> <?php echo lang('task_user_logged_time'). ': '; ?>
                         <span id="user_logged_time">
                         <?php 
                         ?>
                         </span>
                      </h5>
                   </div>

                   <div class="task-info task-info-total-logged-time">
                      <h5>
                         <i class="fa task-info-icon fa-fw fa-lg fa-clock-o"></i> <?php echo lang('task_total_logged_time'). ': '; ?>
                         <span class="text-success">
                         <?php
                            $hours = floor($seconds / 3600);
                            $minutes = floor(($seconds / 60) % 60);
                            $seconds = $seconds % 60;
                            $hours = ($hours < 10) ? ('0'.$hours) : $hours;
                            $minutes = ($minutes < 10) ? ('0'.$minutes) : $minutes;
                            $seconds = ($seconds < 10) ? ('0'.$seconds) : $seconds;
                            $show_time = $hours . ' : ' . $minutes . ' : ' . $seconds;
                            echo "$hours:$minutes:$seconds"; 
                           ?>
                         </span>
                      </h5>
                   </div>

                   <div class="mtop5 clearfix"></div>
                     <div id="inputTagsWrapper">
                        <?php 
                        //$this->db->get_where('tags', array('ta'))
                        ?>
                        <input type="text" data-taskid="<?php echo $task->id ?>" class="tagsinput" id="tags" name="tags" value="<?php echo $tags; ?>" data-role="tagsinput">
                     </div>
                   <div class="clearfix"></div>

                   <hr class="task-info-separator" />
                   <div class="clearfix"></div>
                   <h4 class="task-info-heading font-normal font-medium-xs"><i class="fa fa-user" aria-hidden="true"></i> <?php echo lang('task_single_assignees'); ?></h4>
                    <div class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" ><?php echo lang('assign_task_to'); ?><span class="caret right"></span></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <?php foreach ($staff as $assignee) {
                                         if (!in_array($assignee['id'],$assignees_ids)) { ?>
                            <li class="border-bottom">
                              <a id="add_task_assignees" onclick="add_task_assignees(<?php echo $assignee['id'] ?>, <?php echo $task->id ?>); return false;" class="dropdown-item" href="#"><?php echo $assignee['first_name'] . ' ' . $assignee['last_name']; ?></a>
                            </li>

                        <?php } } ?>
                      </ul>
                    </div>
            <div class="clearfix"></div>        
            <div class="task_users_wrapper">
            <?php
               $_assignees = '';
               foreach ($task_assignees as $assignee) {
                $_remove_assigne = '';
                
                  $_remove_assigne = ' <a href="#" class="remove-task-user text-danger" onclick="remove_assignee(' . $assignee['id'] . ',' . $task->id . '); return false;"><i class="fa fa-remove"></i></a>';
               
               $_assignees .= '
               <div class="task-user"  data-toggle="tooltip" data-title="'.html_escape($assignee['first_name']).'">
               '. $assignee['first_name'] . ' ' . $assignee['last_name'] . $_remove_assigne . '</span>
               </div>';
               }
               if ($_assignees == '') {
               $_assignees = '<div class="text-danger display-block">'.lang('task_no_assignees').'</div>';
               }
               echo $_assignees;
               ?>
         </div>
                    
      <hr class="task-info-separator" />
         <div class="clearfix"></div>
          <h4 class="task-info-heading font-normal font-medium-xs">
            <i class="fa fa-bell-o" aria-hidden="true"></i>
            <?php echo lang('reminders'); ?>
         </h4>
         <a href="#" onclick="new_task_reminder(<?php echo $task->id; ?>); return false;" >
         <?php echo lang('create_reminder'); ?>
         </a>
         <div id="reminder_area">
         <?php if(count($reminders) == 0) { ?>
         <div class="display-block text-muted mtop10">
            <?php echo lang('no_reminders_for_this_task'); ?>
         </div>
         <?php } else { ?>
         <ul class="mtop10">
            <?php foreach($reminders as $rKey => $reminder) {
               ?>
            <li class="<?php if($reminder['isnotified'] == '1'){echo 'text-throught';} ?>" data-id="<?php echo $reminder['id']; ?>">
               <div class="mbot15">
                  <div>
                     <p class="bold">
                        <?php 
                        echo lang('reminder_for'). ' ' . $reminder['first_name'] . ' ' . $reminder['last_name'] . ' ' . $reminder['date'];
                        ?>
                        <?php if ($reminder['creator'] == $this->session->userdata('user_id')) { ?>
                        <a href="#" onclick="remove_task_reminder(<?php echo $reminder['id']; ?>); return false;" class="text-danger delete-reminder"><i class="fa fa-remove"></i></a>
                        <?php } ?>
                     </p>
                     <?php
                        if(!empty($reminder['description'])) {
                           echo $reminder['description'];
                        } else {
                           echo '<p class="text-muted no-mbot">'.lang('no_description_provided').'</p>';
                        }
                        ?>
                  </div>
                  <?php if(count($reminders) -1 != $rKey) { ?>
                  <hr class="hr-10" />
                  <?php } ?>
               </div>
            </li>
            <?php } ?>
         </ul>
         <?php } ?>
        </div>
         <div class="clearfix"></div>
         <div id="newTaskReminderToggle" class="mtop15" style="display:none;">
           
            <div class="form-group">
            <label>Date to notify</label> <br>
            <input type="" id="reminder_date" data-taskid="" name="reminder_date" class="datetime pointer task-single-inline-field" value="" autocomplete="off"> 
            </div>
            <div class="form-group">
              <select class="select2" style="width: 100%" name="reminder_staff" id="reminder_staff">
                <?php foreach($staff as $member) { ?>
                    <option value="<?php echo $member['id'] ?>"><?php echo $member['first_name']. ' '. $member['last_name']; ?></option>>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
            <label>Description</label>
            <input type="text" id="reminder_description" name="reminder_description" class="form-control"> 
            </div>
              <div class="form-group">
                <div class="checkbox checkbox-primary">
                  <input type="checkbox" name="notify_by_email" id="notify_by_email">
                  <label for="notify_by_email"><?php echo lang('reminder_notify_me_by_email'); ?></label>
                </div>
            </div>
            <button class="btn btn-info btn-xs pull-right"  onclick="add_task_reminder('<?php echo $task->id; ?>');"  id="">
            <?php echo lang('create_reminder'); ?>
            </button>
            <div class="clearfix"></div>
          </div>

          <hr class="task-info-separator" />
          <div class="clearfix"></div>    

          </div>
          </div>
          <!-- Right side -->
          <div class="modal-footer">
          </div>
    </div>
    <?php echo form_close(); ?>
</div>
<style>
  .checklist_item {
    margin-bottom: 5px;
  }
  .checklist_input {
    margin-bottom: 5px;
  }
  #description{
    background-color: #f1f1f1;
    border: 2px solid #6d6a6a;
    border-radius: 5px;
  }
  .date {
    width: 30%;
  }
  .dropdown a:hover{
    text-decoration: none;
  }
  .dropdown a{
    color: #8c8d8e;
  }
  .dropdown ul{
    width: 80%;
  }
  .dropdown li:last-child {
    border-bottom: none !important; 
  }
</style>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>


<script>  

 

 $(document).ready(function(){ 

      $('#myModal').on('hidden.bs.modal', function (e) {
          //  console.log("openCloseded");
           $(".modal-content").empty();
          //  $(this).removeData('bs.modal');
          //  $(".modal-content").html("");
      });

      fetch_data();  

      $(document).on('click', '#btn_add', function(event){
        event.preventDefault();
           var taskid = $('#taskid').val();  
           var description = $('#description').text();  
           if(description == '')  
           {  
                alert("Enter description");  
                return false;  
           }  
           $.ajax({  
                url: site.base_url + "tasks/add_checklist_item",
                method:"POST",  
                data:{ taskid: taskid, description: description },  
                dataType:"text",
                // async: false,
                // contentType: false,
                // cache: false,
                // processData: false,
                success : function(data)  
                {  
                     fetch_data();  
                }  
           });  
      });
       
      $(document).on('click', '.btn_delete', function(event){
        event.preventDefault();
        
        var checklist_id=$(this).data("checklist_id");  
          //  if(confirm("Are you sure you want to delete this?"))  
          //  {  
                $.ajax({  
                     url: site.base_url + "tasks/delete_checklist_item",
                     method:"POST",  
                     data:{ checklist_id:checklist_id },  
                     dataType:"json",
                     success:function(data){  
                      $this.prop('disabled', false);
                          fetch_data();  
                     }  
                });  
           //}  
      });
      function edit_data(checklist_id, description)  
      {  
           $.ajax({  
                url: site.base_url + "tasks/edit_checklist_item",
                method:"POST",  
                data:{ checklist_id:checklist_id, description:description },  
                dataType:"text",  
                success:function(data){  
                     //alert(data);  
                }  
           });  
      }  
      $(document).on('blur', '.description', function(){  
           var checklist_id = $(this).data("checklist_id");  
           var description = $(this).text();  
           edit_data(checklist_id, description);  
      });  
      
     

      $(document).on('change', '.checklist_checkbox', function(){
          var checklist_id = $(this).data('checklist_id');
          console.log(checklist_id);
          if ($('.checklist_checkbox').is(":checked")) {
            var finished = 1;
          } 
          else {
            var finished = 0;
          }
          $.ajax({
            url: site.base_url + "tasks/finish_checklist_item",
            method: "POST",
            data: { checklist_id: checklist_id, finished: finished },
            dataType: "text",
            success: function(){

            }
          });

       });

      // Marking task as complete
      $(document).on('click', '#mark-complete', function(){
          
          var taskid = $(this).data("taskid");
          var status = 5; // for complete 

          $.ajax({
            url: site.base_url + "tasks/task_mark_as",
            method: 'POST',
            dataType: 'text',
            data: {status: status, taskid: taskid},
            success: function(data){
              $('#unmark-complete').css({"background-color": "#fff","display":"block"});
              $('#mark-complete').css({"display":"none"});
            }
          });
      });

      $(document).on('click', '#unmark-complete', function(){
          
          var taskid = $(this).data("taskid");
          var status = 4; // for complete 

          $.ajax({
            url: site.base_url + "tasks/task_mark_as",
            method: 'POST',
            dataType: 'text',
            data: {status: status, taskid: taskid},
            success: function(data){
              //alert(data);
              $('#unmark-complete').css({"display":"none"});
              $('#mark-complete').css({"background-color": "#008ece", "display":"block"});
            }
          });
      });

      $(document).on('change', '#task-single-startdate', function(){
        var startdate = $('#task-single-startdate').val();
        var taskid = $(this).data('taskid');
        console.log(site.base_url);
          $.ajax({
            url: site.base_url + "tasks/update_startdate",
            method: 'POST',
            data: { startdate: startdate , taskid: taskid },
            success: function(data){
              //alert(data);
            }

          });
      });

      $(document).on('change', '#task-single-duedate', function(){
        var duedate = $('#task-single-duedate').val();
        var taskid = $(this).data('taskid');
        console.log(duedate);
          $.ajax({
            url: site.base_url + "tasks/update_duedate",
            method: 'POST',
            data: { duedate: duedate , taskid: taskid },
            success: function(data){
              //alert(data);
            }

          });
      });

      $(document).on('blur', '#inputTagsWrapper input', function(){
       
        var tags = $('#tags').val();
        var taskid = $('#taskid').val();
        console.log(tags);
          $.ajax({
            url: site.base_url + "tasks/update_tags",
            method: 'POST',
            dataType: 'text',
            data: { tags: tags , taskid: taskid },
            success: function(data){
              //alert(data);
            }

          });
      });

      $('#task-comment-form').on('submit', function(e){  
           e.preventDefault();  
           
            var formdata = new FormData(this);
            console.log(formdata);

             $.ajax({  
                     url: site.base_url + "tasks/add_task_comment",
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
                         fetch_comments(); 
                     }  
                });  
           
      });  



 });  

  function fetch_data()  
    {  
          $.ajax({  
              url: site.base_url + "tasks/select_checklist_item",
              method:"GET",  
              data : {taskid: $('#taskid').val()},
              success:function(data){  
                //$('#live_data').remove();
                    $('#live_data').html(data);  
              }  
          });  
    }  
      
      sessionStorage.setItem('timer', 'false');
      var timer = sessionStorage.getItem('timer');
      if(timer === 'true') {
            //$('#stop_timer').show();
            alert('Timer is on! First stop the timer & then try.');
      } else {
          //$('#start_timer').hide();
      }

      function timerFunction(taskid) {
        $('#start_timer').hide();
        $('#stop_timer').show();

        $.ajax({
            url: site.base_url + "tasks/start_stop_timer",
            method: 'POST',
            data: {taskid: taskid},
            success: function(data) {
              $('#clock_time').text(data);
            }
        }); 
      }

      function stopTimerFunction(taskid) {
        $('#stop_timer').hide();
        $('#start_timer').show();

        $.ajax({
            url: site.base_url + "tasks/start_stop_timer",
            method: 'POST',
            data: {taskid: taskid},
            success: function(data) {
              $('#clock_time').text('Clock Stopped!');
              $('#user_logged_time').text(data);
            }
        });
      }

      function xtimerFunction(taskid) {
        $('#start_timer').hide();
        $('#stop_timer').show();

        var timer = sessionStorage.getItem('timer');
        if(timer === 'true') {
            //$('#stop_timer').show();
            $('#start_timer').show();
            $('#stop_timer').hide();
            alert('Timer is on! First stop the timer & then try.');
        } else {
          
          myVar = setInterval(function(){
           sessionStorage.setItem('timer', 'true'); 
           $.ajax({
              url: site.base_url + "tasks/count_time",
              method: 'POST',
              data: {taskid: taskid},
              beforeSend: function(){
                  $('#ajaxCall').hide();
              },
              complete: function(){
                  $('#ajaxCall').hide();
              },
              success: function(data) {
                $('#show_time').text(data);
              }
            }); 
          }, 1000);
        }
      }
      function xstopTimerFunction(taskid) {

        $('#stop_timer').hide();
        $('#start_timer').show();
        clearInterval(myVar);
        sessionStorage.setItem('timer', 'false');
        
        $.ajax({
          url: site.base_url + "tasks/count_time",
          method: 'POST',
          data: {taskid: taskid, time: '0'},
          success: function(data) {
            $('#show_time').text(data);
          }
        }); 
      }

function task_mark_as(status, taskid)
{
    $.ajax({
        url: site.base_url + "tasks/task_mark_as2",
        method: 'POST',
        dataType: 'text',
        data: {status: status, taskid: taskid},
        success: function(data){
          //alert(data);
          $('.dropdown_status').html(data);
        }
      });
}


function task_change_priority(priority, taskid) 
{
  console.log(priority);
    $.ajax({
        url: site.base_url + "tasks/task_change_priority",
        method: 'POST',
        dataType: 'text',
        data: {priority: priority, taskid: taskid},
        success: function(data){
          $('.dropdown_priority').html(data);
        }
    });
}

function add_task_assignees(assignee_id, taskid)
{
  console.log(assignee_id);
  $.ajax({
        url: site.base_url + "tasks/add_task_assignees",
        method: 'POST',
        dataType: 'text',
        data: {assignee_id: assignee_id, taskid: taskid},
        success: function(data){
          //alert(data);
          var obj = JSON.parse(data);
          var taskid = obj.taskid;
          console.log(data);
          console.log(obj);
          fetch_task_assignee(taskid); 
        }
    });
}

function fetch_task_assignee(taskid)
{
    $.ajax({
        url: site.base_url + "tasks/get_task_assignees",
        method: 'POST',
        dataType: 'text',
        data: {taskid: taskid},
        success: function(data){
            $('.task_users_wrapper').html(data);
        }
    });
}

function remove_assignee(assignee_id, taskid)
{
    $.ajax({
        url: site.base_url + "tasks/delete_task_assignees",
        method: 'POST',
        dataType: 'text',
        data: {assignee_id: assignee_id, taskid: taskid},
        success: function(data){
          // alert(data);
          fetch_task_assignee(taskid); 
        }
    });
}

function edit_task_inline_description()
{
  $('#description_view').hide();
  $('#task_description').show();
}

$(document).on('blur', '#task_description .redactor_editor', function(){
    var description = $('#task_description .redactor_editor').text();
    var taskid = $('#taskid_description').val();

      $.ajax({
          url: site.base_url + "tasks/update_description",
          method: 'POST',
          dataType: 'text',
          data: {description: description, taskid: taskid},
          success: function(data){
            //alert(data);
            $('#task_description').hide();
            $('#description_view').show();
            $('#description_view').text(data);
          }
      });
  });

function fetch_comments()
{
  $.ajax({
      url: site.base_url + "tasks/fetch_comments",
      method: 'GET',
      dataType: 'text',
      data: {taskid: $('#taskid').val()},
      success: function(data){
          $('#task-comments').html(data);
      }
    });
}

function add_task_comment(taskid) {

  var comment = $('#task_comment').val();

 // var userfile = $('#userfile').val();
var userfile = document.getElementById("userfile").files[0];

  // console.log(userfile);

  $.ajax({
          url: site.base_url + "tasks/add_task_comment",
          method: 'POST',
          //dataType: 'text',
          data: {comment: comment, taskid: taskid, userfile: userfile},
          success: function(){
            //$('#task-comments').html(data);
            fetch_comments();
          }
    });
}


function edit_task_comment(comment_id)
{

    var edit_wrapper = $('[data-edit-comment="' + comment_id + '"]');
    edit_wrapper.show();
    $('#comment_view'+comment_id).hide();

    
}

function save_edited_comment(comment_id, taskid)
{

    var comment_content = $('#task_comment_'+comment_id).val();

    console.log(comment_content);

    $.ajax({
      url: site.base_url + "tasks/update_task_comment",
      method: 'POST',
      dataType: 'text',
      data: {comment_id: comment_id, taskid: taskid, comment_content: comment_content},
      success: function(data){
        //$('#task-comments').html(data);
        fetch_comments();
      }
    });
}

function cancel_edit_comment(comment_id)
{
   var edit_wrapper = $('[data-edit-comment="' + comment_id + '"]');
    edit_wrapper.hide();
    $('#comment_view'+comment_id).show();
}

function remove_task_comment(comment_id)
{

  $.ajax({
      url: site.base_url + "tasks/remove_task_comment",
      method: 'POST',
      dataType: 'text',
      data: {comment_id: comment_id},
      success: function(data){
        //$('#task-comments').html(data);
        fetch_comments();
      }
    });

}
function remove_task_checklist(checklist_id)
{

  $.ajax({
      url: site.base_url + "tasks/delete_checklist_item",
      method: 'POST',
      dataType: 'text',
      data: {checklist_id: checklist_id},
      success: function(data){
        //$('#task-comments').html(data);
        fetch_data();
      }
    });

}





function new_task_reminder(taskid)
{
  $('#newTaskReminderToggle').toggle();
}
function add_task_reminder(taskid) 
{
  var reminder_date = $('#reminder_date').val();
  var reminder_description = $('#reminder_description').val();
  var reminder_staff = $("#reminder_staff option:selected").val();
 
  $.ajax({
      url: site.base_url + "tasks/add_task_reminder",
      method: 'POST',
      dataType: 'text',
      data: {taskid: taskid, reminder_date: reminder_date, reminder_staff: reminder_staff, reminder_description: reminder_description},
      success: function(data){
        //alert(data);
        //$('#task-comments').html(data);
        fetch_reminders();
      }
    });
}

function fetch_reminders()
{
  $.ajax({
      url: site.base_url + "tasks/fetch_reminders",
      method: 'POST',
      dataType: 'text',
      data: {taskid: $('#taskid').val()},
      success: function(data){
       //alert(data);
        $('#reminder_area').html(data);
      }
    });
}
   

function remove_task_reminder(reminder_id)
{

  $.ajax({
      url: site.base_url + "tasks/remove_task_reminder",
      method: 'POST',
      dataType: 'text',
      data: {reminder_id: reminder_id},
      success: function(data){
        //$('#task-comments').html(data);
        fetch_reminders();
      }
    });

}

 </script>


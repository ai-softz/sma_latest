<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
	.box {
	}
</style>
<div class="container">
        	<div class="row">
        		<h2 class="" style="font-size: 18px">Leave Details</h2>
        		<div class="col-md-4 box" style="background-color: #fff">
        			<h3 style="margin-bottom: 30px"> Leave Application Detail</h3>
        			<div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Employee</th><td><?= $emp_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Department</th><td><?= $department_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Leave Type</th><td><?= $leave_type_name ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Applied On</th><td><?= date('d M, Y', strtotime($leave_application->applied_on)); ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Start Date</th><td><?= date('d M, Y', strtotime($leave_application->from_date)); ?></td>
	                      </tr>
	                      <tr>
	                      	<th>End Date</th><td><?= date('d M, Y', strtotime($leave_application->to_date)); ?></td>
	                      </tr>
	                      <tr>
	                      	<th>Attachment</th><td> <a href="<?= base_url('assets/uploads/leave_documents/'.$leave_application->leave_attachment) ?>" target="_blank"> <?= $leave_application->leave_attachment ?> </a> </td>
	                      </tr>
	                      <tr>
	                      	<th>Total Days</th><td><?= $leave_application->leave_days ?></td>
	                      </tr>
	                    </tbody>
	              	  </table>
	                </div>
        		</div>
        		<div class="col-md-4 box" style="background-color: #fff">
        			<h3 style="margin-bottom: 30px"> Update Status</h3>
        			<div class="table-responsive" data-pattern="priority-columns">
        			<?php $attrib = [ 'role' => 'form', 'id' => 'change_leave_status']; ?>	
        			<?php echo admin_form_open_multipart('timesheets/change_leave_status', $attrib) ?><input type="hidden" name="leave_id" value="<?= $leave_application->leave_id ?>">	 
        			<div class="form-group row">
      					<div class="col-md-12">
	                  		<label for="status"><?php echo  lang('status') ?> <span class="text-danger"></span></label>
                      		<select name="status" class="form-control">
                      			<option value="1" <?= $leave_application->status == 1 ? "Selected" : "" ?>>Pending</option>
                      			<option value="2" <?= $leave_application->status == 2 ? "Selected" : "" ?>>Approved</option>
                      			<option value="3" <?= $leave_application->status == 3 ? "Selected" : "" ?>>Rejected</option>
                      			
                      		</select> 
                      	</div>
                       </div>
	                   <div class="form-group row">
      						<div class="col-md-12"> 
      						<label for="remarks"><?php echo  lang('remarks') ?> <span class="text-danger"></span></label>  	
	                      	<textarea name="remarks" class="form-control"><?= $leave_application->remarks ?></textarea>
	                    	</div>
	                	</div>
	                	<div class="form-group">
		              	  <?php 
		              	  	echo form_submit('change_leave_status', lang('save'), 'class="btn btn-primary"');
		              	  	echo form_close(); 
		              	  ?>
	              		</div>
	                </div>
        		</div>
        		<div class="col-md-4 box" style="background-color: #fff">
        			<h3 style="margin-bottom: 30px"> Last Taken Leave</h3>
        			<div class="table-responsive" data-pattern="priority-columns">
	                  <table class="table table-striped m-md-b-0">
	                    <tbody>
	                      <tr>
	                      	<th>Leave Type</th> <td><?= $last_leave_type_name ?></td>

	                      </tr>
	                      <tr>
	                      	<th>Applied On</th> <td><?= date('d M, Y', strtotime($last_leave->applied_on)); ?></td>
	                      </tr>
	                      <th>Total Days</th> <td><?= $last_leave->leave_days ?></td>
	                    </tbody>
	              	  </table>
	                </div>
	                <hr>
	                <h3 style="margin-bottom: 30px"> Leave Statistics</h3>
	                <?php foreach($leave_statistics as $row) { ?>
	                	<p><label><?= $row['leave_name'] ?> (<?= $row['total_leave_taken'] ?>/<?= $row['total_leave_days'] ?>) </label></p>
	                	<?php $to = ($row['total_leave_taken'] / $row['total_leave_days']) * 100;  ?>
	                	<div class="progress">
						<div class="progress-bar" style="width: <?php echo $to; ?>%"></div>
						</div>
	            	<?php } ?>
        		</div>

    		</div>

</div>

<script>
$(document).ready(function(){

	$('#change_leave_status').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      $.ajax({  
             url: site.base_url + "timesheets/change_leave_status",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
                
             }
               
      });  
  	});
});
</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_leave_application'); ?></h4>
        </div>

        <div class="modal-body">
            <p> </p>

		<div class="row">
      <div class="col-md-12">
        <?php $attrib = [ 'role' => 'form', 'id' => 'edit_leave_application']; ?>
        <?php echo admin_form_open_multipart("timesheets/edit_leave_application", $attrib) ?>
        <input type="hidden" name="leave_id" value="<?= $leave_application->leave_id ?>">

        
        <div class="form-group row">
          <div class="col-md-12">
            <label for="reason"><?php echo  lang('reason') ?> <span class="text-danger"></span></label>
            <textarea name="reason" class="form-control" required="required"><?= $leave_application->reason ?></textarea>
          </div>
		    
		    </div>
        <div class="form-group row">
            <div class="col-md-12">
              <label for="remarks"><?php echo  lang('remarks') ?> <span class="text-danger"></span></label>
              <textarea name="remarks" class="form-control"><?= $leave_application->remarks ?></textarea>
            </div>
        </div>
   <div class="form-group">
        <?php echo form_submit('edit_leave_application', lang('save'), 'class="btn btn-primary"'); ?>
    </div>
    <?php echo form_close() ?>
  </div>
  
</div>

	</div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>

$(document).ready(function(){


$('#edit_leave_application').on('submit', function(e){  
      e.preventDefault();       
      var formdata = new FormData(this);
      // console.log(formdata);
      $.ajax({  
             url: site.base_url + "timesheets/editLeaveApplication",
             method:"POST",  
             data:new FormData(this),  
             contentType: false,  
             cache: false,  
             processData:false,  
             success:function(data)  
             {  
             	$('#myModal').modal('hide');
                //$('#table_allowances').DataTable().ajax.reload();
             }
               
      });  
  });

});
</script>